object dlg_Wizard: Tdlg_Wizard
  Left = 408
  Top = 108
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Custom Wizard'
  ClientHeight = 358
  ClientWidth = 497
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel2: TBevel
    Left = 0
    Top = 60
    Width = 497
    Height = 3
    Align = alTop
    Shape = bsTopLine
    Visible = False
  end
  object pn_Buttons: TPanel
    Left = 0
    Top = 323
    Width = 497
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 0
    DesignSize = (
      497
      35)
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 497
      Height = 2
      Align = alTop
      Shape = bsTopLine
    end
    object btn_Ok: TButton
      Left = 334
      Top = 8
      Width = 75
      Height = 23
      Action = act_Ok
      Anchors = [akTop, akRight]
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object btn_Cancel: TButton
      Left = 418
      Top = 8
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      OnClick = act_CancelExecute
    end
  end
  object pn_Header: TPanel
    Left = 0
    Top = 0
    Width = 497
    Height = 60
    Align = alTop
    BevelOuter = bvLowered
    BorderWidth = 6
    Color = clInfoBk
    Constraints.MaxHeight = 60
    Constraints.MinHeight = 60
    TabOrder = 1
    object lb_Action: TLabel
      Left = 8
      Top = 8
      Width = 44
      Height = 13
      Caption = 'lb_Action'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
  end
  object ActionList1: TActionList
    Left = 440
    Top = 4
    object act_Ok: TAction
      Category = 'Main'
      Caption = 'Ok'
      OnExecute = act_OkExecute
    end
    object act_Cancel: TAction
      Category = 'Main'
      Caption = #1054#1090#1084#1077#1085#1072
      OnExecute = act_CancelExecute
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredValues = <>
    Left = 412
    Top = 4
  end
end
