object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Form3'
  ClientHeight = 453
  ClientWidth = 769
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 382
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 0
  end
  object Edit1: TEdit
    Left = 8
    Top = 168
    Width = 433
    Height = 21
    TabOrder = 1
    Text = 'Edit1'
  end
  object ComboBox1: TComboBox
    Left = 8
    Top = 128
    Width = 145
    Height = 21
    Style = csDropDownList
    TabOrder = 2
  end
  object ButtonedEdit1: TButtonedEdit
    Left = 32
    Top = 248
    Width = 353
    Height = 21
    TabOrder = 3
    Text = 'ButtonedEdit1'
  end
  object CategoryButtons1: TCategoryButtons
    Left = 304
    Top = 312
    ButtonFlow = cbfVertical
    Categories = <>
    RegularButtonColor = 14410210
    SelectedButtonColor = 12502986
    TabOrder = 4
  end
  object CategoryPanelGroup1: TCategoryPanelGroup
    Left = 560
    Top = 0
    Width = 209
    Height = 453
    VertScrollBar.Tracking = True
    Align = alRight
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'Tahoma'
    HeaderFont.Style = []
    TabOrder = 5
    object CategoryPanel2: TCategoryPanel
      Top = 200
      Caption = 'CategoryPanel2'
      TabOrder = 0
    end
    object CategoryPanel1: TCategoryPanel
      Top = 0
      Caption = 'CategoryPanel1'
      TabOrder = 1
    end
  end
  object OpenTextFileDialog1: TOpenTextFileDialog
    Left = 48
    Top = 320
  end
end
