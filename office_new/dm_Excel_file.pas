unit dm_Excel_file;

interface

uses
  Windows, SysUtils, Classes, Forms, Variants, ExcelXP, Dialogs,

  u_func,
 // u_Worksheet,

  OleServer;


  //http://basicsprog.ucoz.ru/blog/2011-09-30-8

  //http://www.delphikingdom.com/asp/viewitem.asp?catalogid=1274#04

type
  TdmExcel_file = class(TDataModule)
    ExcelApplication1: TExcelApplication;
    ExcelWorksheet1: TExcelWorksheet;
    ExcelWorkbook1: TExcelWorkbook;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
//    FWorksheet: TWorksheet;

    FArrData: Variant;

    FStrArr: array of array of string;

//            s:=VarToStr(dmExcel.Cells[iRow,iCol]);


    FvWorkbook: _Workbook;

    function Get_CellValue_1(ARow, ACol: Cardinal): Variant;
    function Get_CellValue_0(ARow, ACol: Cardinal): Variant;
    procedure Log(aMsg : string);

  public
    Active : Boolean;

    RowCount: Integer;
    ColCount: Integer;

//    class procedure Init;


    function Open(aFileName: string): Boolean;
    procedure Close;
    procedure GetSheets(aFileName: string; aStrings: TStrings);

    procedure LoadWorksheetNames(aStrings: TStrings);

    function LoadWorksheetByName(aName: string): Boolean;
    function LoadWorksheetByIndex_0(aIndex: Integer): Boolean;

    property Cells_1[ARow: Cardinal; ACol: Cardinal]: Variant read Get_CellValue_1;
    property Cells_0[ARow: Cardinal; ACol: Cardinal]: Variant read Get_CellValue_0;

  end;



  function dmExcel_file: TdmExcel_file;


implementation
{$R *.dfm}

var
  FdmExcel_file: TdmExcel_file;

// ---------------------------------------------------------------
function dmExcel_file: TdmExcel_file;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmExcel_file) then
    Application.CreateForm(TdmExcel_file, FdmExcel_file);

  Result := FdmExcel_file;
end;



procedure TdmExcel_file.DataModuleDestroy(Sender: TObject);
begin
//  FreeAndNil(FWorksheet);
end;

// ---------------------------------------------------------------
procedure TdmExcel_file.GetSheets(aFileName: string; aStrings: TStrings);
// ---------------------------------------------------------------
begin
 // TdmExcel_file.Init;

  Open(aFileName);

  LoadWorksheetNames(aStrings);
  Close;

 // OpenFileQuery (aFileName);
end;




procedure TdmExcel_file.DataModuleCreate(Sender: TObject);
begin
//  FWorksheet:=TWorksheet.Create;

 // Log('DataModuleCreate');
end;


procedure TdmExcel_file.Log(aMsg : string);
begin
 // ShowMessage(aMsg);
end;


//------------------------------------------------------------------------------
function TdmExcel_file.Get_CellValue_1(ARow, ACol: Cardinal): Variant;
//------------------------------------------------------------------------------
// ARow, ACol  0..count-1
begin
 // Inc(ARow);
 // Inc(ACol);

  Result:=Null;

  if not VarIsEmpty(FArrData) then
    if (ARow<=RowCount) and (ACol<=ColCount) then
      Result:=FArrData[ARow,ACol];
end;


//------------------------------------------------------------------------------
function TdmExcel_file.Get_CellValue_0(ARow, ACol: Cardinal): Variant;
//------------------------------------------------------------------------------
// ARow, ACol  0..count-1
begin
 // Inc(ARow);
 // Inc(ACol);

  Result:=Null;

  if not VarIsEmpty(FArrData) then
    if (ARow<RowCount) and (ACol<ColCount) then
      Result:=FArrData[ARow+1,ACol+1];
end;


//------------------------------------------------------------------------------
function TdmExcel_file.LoadWorksheetByIndex_0(aIndex: Integer): Boolean;
//------------------------------------------------------------------------------
var
  c: Integer;
  I: Integer;
  r: Integer;
//  vNewWorkbook: _Workbook;
  vExcelRange: ExcelRange;

  v: Variant;
begin    
// vNewWorkbook := ExcelApplication1.Workbooks.Add (aFileName, GetUserDefaultLCID);
//  ExcelWorkbook1.ConnectTo(vNewWorkbook);
  Result:=False;

  if aIndex<0 then
    Exit;


  if not Active then
    Exit;


 // Inc(aIndex);

  if aIndex<ExcelWorkbook1.Worksheets.count then
  begin
    ExcelWorksheet1.ConnectTo(ExcelWorkbook1.Worksheets[aIndex+1] as _Worksheet);

    ColCount:=ExcelWorksheet1.UsedRange[0].Columns.Count;
    RowCount:=ExcelWorksheet1.UsedRange[0].Rows.Count;

//    ColCount:=ExcelWorksheet1.UsedRange[GetUserDefaultLCID].Columns.Count;
//    RowCount:=ExcelWorksheet1.UsedRange[GetUserDefaultLCID].Rows.Count;


    vExcelRange:=ExcelWorksheet1.UsedRange[0];
//    vExcelRange:=ExcelWorksheet1.UsedRange[GetUserDefaultLCID];
    FArrData:=vExcelRange.Value2;

    Result := not VarIsEmpty(FArrData);

    SetLength(FStrArr, RowCount);
    for r := 0 to RowCount - 1 do
      SetLength(FStrArr[r], ColCount);


    for r := 0 to RowCount - 1 do
     for c := 0 to ColCount - 1 do
       try
         FStrArr[r,c] := VarToStr(FArrData[r+1,c+1]);
       except
       end;

//            s:=VarToStr(dmExcel.Cells[iRow,iCol]);

 //   v:=FArrData[5,1];
  end;

//  vNewWorkbook.Close(false, EmptyParam, EmptyParam, GetUserDefaultLCID);
end;

//------------------------------------------------------------------------------
function TdmExcel_file.LoadWorksheetByName(aName: string): Boolean;
//------------------------------------------------------------------------------
var
  iIndex: Integer;
  c: Integer;
  I: Integer;
  r: Integer;
  s: string;
//  vNewWorkbook: _Workbook;
  vExcelRange: ExcelRange;

  v: Variant;
begin
  Assert(aName<>'');

// vNewWorkbook := ExcelApplication1.Workbooks.Add (aFileName, GetUserDefaultLCID);
//  ExcelWorkbook1.ConnectTo(vNewWorkbook);
  Result:=False;

 // if aIndex<0 then
  //  Exit;


  if not Active then
    Exit;

  iIndex:=-1;

  for i := 1 to ExcelWorkbook1.Worksheets.count do
  begin
    ExcelWorksheet1.ConnectTo(ExcelWorkbook1.Worksheets[i] as _Worksheet);

    if Eq(ExcelWorksheet1.Name, aName) then
    begin
      iIndex:=i;
      Break;
    end;
  end;



//    ExcelWorksheet1.ConnectTo(ExcelWorkbook1.Worksheets[i] as _Worksheet);

  if iIndex<0 then
    Exit;
    

 // Inc(aIndex);

  if iIndex<=ExcelWorkbook1.Worksheets.count then
  begin
//    ExcelWorksheet1.ConnectTo(ExcelWorkbook1.Worksheets[aIndex+1] as _Worksheet);
    ExcelWorksheet1.ConnectTo(ExcelWorkbook1.Worksheets[iIndex] as _Worksheet);

    ColCount:=ExcelWorksheet1.UsedRange[0].Columns.Count;
    RowCount:=ExcelWorksheet1.UsedRange[0].Rows.Count;

//    ColCount:=ExcelWorksheet1.UsedRange[GetUserDefaultLCID].Columns.Count;
//    RowCount:=ExcelWorksheet1.UsedRange[GetUserDefaultLCID].Rows.Count;

    vExcelRange:=ExcelWorksheet1.UsedRange[0];
//    vExcelRange:=ExcelWorksheet1.UsedRange[GetUserDefaultLCID];
    FArrData:=vExcelRange.Value2;

    Result := not VarIsEmpty(FArrData);

    SetLength(FStrArr, RowCount);
    for r := 0 to RowCount - 1 do
      SetLength(FStrArr[r], ColCount);


    for r := 0 to RowCount - 1 do
     for c := 0 to ColCount - 1 do
       try
         v:=FArrData[r+1,c+1];
         s:=VarToStr( v);

         FStrArr[r,c] := VarToStr( FArrData[r+1,c+1] );
       except
       end;

//            s:=VarToStr(dmExcel.Cells[iRow,iCol]);

 //   v:=FArrData[5,1];
  end;

//  vNewWorkbook.Close(false, EmptyParam, EmptyParam, GetUserDefaultLCID);
end;



//------------------------------------------------------------------------------
procedure TdmExcel_file.LoadWorksheetNames(aStrings: TStrings);
//------------------------------------------------------------------------------
var
  i: Integer;
begin
//  Log('TdmExcel_file.LoadWorksheetNames');


  aStrings.Clear;

  if not Active then
    Exit;
    

//zz  FvWorkbook := ExcelApplication1.Workbooks.Add (aFileName, GetUserDefaultLCID);
//  ExcelWorkbook1.ConnectTo(FvWorkbook);

  for i := 1 to ExcelWorkbook1.Worksheets.count do
  begin
    ExcelWorksheet1.ConnectTo(ExcelWorkbook1.Worksheets[i] as _Worksheet);
    aStrings.Add(ExcelWorksheet1.Name);
  end;

//  FvWorkbook.Close(false, EmptyParam, EmptyParam, GetUserDefaultLCID);

end;

// ---------------------------------------------------------------
function TdmExcel_file.Open(aFileName: string): Boolean;
// ---------------------------------------------------------------
begin
//..  Result:=FWorksheet.LoadFromFile(aFileName);
//..  Exit;


  if FileExists(aFileName) then
  begin
//    Log('TdmExcel_file.Open');

    // ������ EXCEL
  //  vExcel := CreateOleObject('Excel.Application');

    // ���� �� ������� ������ � ���������� ���������
   // vExcel.DisplayAlerts := false;
    // ����� ��������
   // excel.WorkBooks.Add;

    try
      FvWorkbook := ExcelApplication1.Workbooks.Add (aFileName, 0);
    except
      ShowMessage ('File open error: ' + aFileName);
    end;


//    FvWorkbook := ExcelApplication1.Workbooks.Add (aFileName, GetUserDefaultLCID);

  //  Log('ExcelWorkbook1.ConnectTo(FvWorkbook);');


    ExcelWorkbook1.ConnectTo(FvWorkbook);
    Active := True;

//    Assert();
  end else
    Active := False;

  Result:=Active;
end;


procedure TdmExcel_file.Close;
begin
 //  Log('TdmExcel_file.Close');


  if Active then
  begin
    FvWorkbook.Close(false, EmptyParam, EmptyParam, 0);
//    FvWorkbook.Close(false, EmptyParam, EmptyParam, GetUserDefaultLCID);
  //  FvWorkbook:=nil;
    Active := False;
  end;
end;


end.
