unit d_geo_Coordinates;

interface

uses Windows, Classes, Forms, Controls, StdCtrls, Math,

  u_func,
  fr_geo_Coordinates,

  u_Geo

  ;

type
  Tdlg_GEO_Coordinates = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    GroupBox1: TGroupBox;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);

  private

    Fframe_geo_Coordinates: Tframe_geo_Coordinates;

  public
    //���������� � ��������        //; aIsRandom: boolean=False
    class function ExecDlg (var aPoint: TBLPoint; aCoordSys: integer): boolean;
  end;


  function geo_Dlg_Coordinates (var aPoint: TBLPoint; aCoordSys: integer): Boolean;
//                                aIsRandom: boolean=False): boolean;


//================================================================
implementation {$R *.DFM}
//================================================================
const
  REGISTRY_COMMON_FORMS  = 'Software\Onega\Common\Forms\';



function geo_RandomBLPoint: TBLPoint;
begin
  Result.B:=geo_EncodeDegree(RandomRange(30,50),  RandomRange(0,59), RandomRange(0,59));
  Result.L:=geo_EncodeDegree(RandomRange(30,100), RandomRange(0,59), RandomRange(0,59));
end;



function geo_Dlg_Coordinates(var aPoint: TBLPoint; aCoordSys: integer): boolean;
begin
  Result:=Tdlg_GEO_Coordinates.ExecDlg (aPoint,aCoordSys);
end;



//----------------------------------------------------------------
class function Tdlg_GEO_Coordinates.ExecDlg (var aPoint: TBLPoint; aCoordSys: integer): boolean;
//----------------------------------------------------------------
begin
 // if LApplication=nil then
  //  raise Exception.Create('LApplication=nil');


  with Tdlg_GEO_Coordinates.Create(Application) do
  try
   // if aIsRandom then
  //    aPoint:=geo_RandomBLPoint();


{

    if aCoordSys=EK_WGS84 then begin
      aPoint:=geo_BL_to_BL (aPoint, EK_KRASOVSKY42, EK_WGS84 );
      cb_CoordSys.ItemIndex:=1
    end else
     cb_CoordSys.ItemIndex:=0;
}

//    Fframe_geo_Coordinates.BLPoint_Pulkovo:=aPoint;
//    Fframe_geo_Coordinates.DisplayCoordSys:=aCoordSys;

    Fframe_geo_Coordinates.Set_BLPoint_and_CoordSys(aPoint,aCoordSys);

    Result:=(ShowModal=mrOk);


    if Result then //begin
      aPoint:=Fframe_geo_Coordinates.Get_BLPoint_Pulkovo;

 //   if (cb_CoordSys.ItemIndex=1) then //and (aCoordSys<>EK_WGS84) then
   //   aPoint:=geo_BL_to_BL (aPoint, EK_WGS84, EK_KRASOVSKY42);


  finally
    Free;
  end;
end;

//----------------------------------------------
procedure Tdlg_GEO_Coordinates.FormCreate(Sender: TObject);
//----------------------------------------------
var P: TPoint;
begin
  Caption:='����������';

  CreateChildForm(Tframe_geo_Coordinates, Fframe_geo_Coordinates, GroupBox1);

//  Fframe_geo_Coordinates:=Tframe_geo_Coordinates.CreateChildForm (GroupBox1);


//  cb_CoordSys.ItemIndex:=0;

  GetCursorPos (P);
  Left:=P.x - Width;
  Top :=P.y;//  - Height;


{  FRegPath:= REGISTRY_COMMON_FORMS + ClassName +'\';

  FormStorage1.IniFileName:=FRegPath;
  FormStorage1.IniSection:='Window';
  FormStorage1.Active:=True;
}

end;


procedure Tdlg_GEO_Coordinates.FormDestroy(Sender: TObject);
begin
//  Fframe_geo_Coordinates.Free;
  inherited;
end;



end.


{


procedure geo_Init(aApplication: TApplication);
begin
  LApplication:=aApplication;
end;
}

{
  procedure geo_Init (aApplication: TApplication);
}

{
exports
//  geo_Init,
  geo_Dlg_Coordinates;
}

{

var
  LApplication: TApplication;

}

