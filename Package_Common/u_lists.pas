unit u_lists;

interface
uses
  Classes,

  u_Func_arrays,
  u_func

  ;


type
  TDoubleListItem = class(TCollectionItem)
  public
    ID : integer;
    Value : Double;

    Value2 : Double;

  end;

  TDoubleList = class(TCollection)
  private
    function AddItem: TDoubleListItem;
    function GetItems(Index: Integer): TDoubleListItem;
  public  
    constructor Create;
    procedure AddValue(aValue: double; aID: integer=0);

    function MakeDoubleArray: TDoubleArray;

    function ValueExists(aValue: double): boolean;

    property Items[Index: Integer]: TDoubleListItem read GetItems; default;
  end;


implementation


constructor TDoubleList.Create;
begin
  inherited Create (TDoubleListItem);
end;

function TDoubleList.AddItem: TDoubleListItem;
begin
  Result := TDoubleListItem(inherited Add);
end;

procedure TDoubleList.AddValue(aValue: double; aID: integer=0);
begin
  with TDoubleListItem(inherited Add) do
  begin
    Value:=aValue;
    ID:=aID;
  end
end;

function TDoubleList.GetItems(Index: Integer): TDoubleListItem;
begin
  Result := TDoubleListItem(inherited Items[Index]);
end;


function TDoubleList.MakeDoubleArray: TDoubleArray;
var I: Integer;
begin
  SetLength(Result, Count);

  for I := 0 to Count - 1 do
    Result[i] := Items[i].Value;

  DoubleArraySort (Result);
end;


function TDoubleList.ValueExists(aValue: double): boolean;
var
  I: Integer;
begin
  Result := false;

  for I := 0 to Count - 1 do
    if Abs(Items[i].Value - aValue) < 0.001 then
    begin
      Result:=True;
      exit;
    end;

end;


end.
