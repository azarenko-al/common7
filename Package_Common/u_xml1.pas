unit u_xml1;

//============================================================================
interface
//============================================================================
uses SysUtils,ComObj,Variants, ActiveX, XMLIntf,XMLDoc,

     u_func,
     u_files
     ;


type
  TxmlAttrib = record
    Name: string;
    Value: Variant;
  end;

  function xml_GetIntAttr    (aNode: IXMLNode; aAttrName: string; aDefault:Integer=0): Integer;
  function xml_GetFloatAttr  (aNode: IXMLNode; aAttrName: string; aDefault: Double = 0): Double;
  function xml_GetStringAttr (aNode: IXMLNode; aAttrName: string; aDefault:string=''): string;


  function xml_Att (aName: string; aValue: Variant): TxmlAttrib;
  function xml_Par (aName: string; aValue: Variant): TxmlAttrib;


  function xml_AddNode      (aParentNode: IXMLNode; aTagName: string; aAttribs: array of TxmlAttrib) : IXMLNode;
  function xml_GetAttr     (aNode: IXMLNode; aAttrName: string; aDefault:string=''): Variant;

//  procedure xml_SetAttr     (aNode: IXMLNode; aAttrName: string; aAttrValue: Variant);

  function xml_GetTextByPath (aNode: IXMLNode; aPathArr: array of string): string;

  function xml_GetNodeByPath       (aNode: IXMLNode; aPathArr: array of string): IXMLNode;
  function xml_GetAttrByTagName    (aNode: IXMLNode; aTagName,aAttrName: string): string;
  function xml_GetIntAttrByTagName (aNode: IXMLNode; aTagName,aAttrName: string): integer;

  function xml_FindNode(aParentNode: IXMLNode; aNodeName, aAttrName, aAttrValue:
      string): IXMLNode;

  function xml_GetAttrByPath (aElement: IXMLNode; aTagName,aAttrName: string): string;

  function xml_IsElement(aNode: IXMLNode): boolean;
  function xml_NodeName(aNode: IXMLNode): string;

  procedure xml_SetAttribs (aNode: IXMLNode; aAttribs: array of TxmlAttrib);
//  end;



type

  //------------------------------------------------
  TXMLDoc = class
  //------------------------------------------------
  private

    FIsLevelsInserted : Boolean;

//    XSLFilename : string;
    FXMLDOM:  IXMLDocument;

//    function LoadFromText(aText: string): boolean;
//    function SaveToText: string;
//    FXMLDOM:  IXMLDocument;//
  //  FXMLDOM: IDOMDocument; //Variant;
    function GetDocumentElement: IXMLNode;
//    function GetXMLDOM: IXMLNode;
    procedure SetXMLText (aValue: string);
    function  GetXMLText: string;
    procedure InsertNodeLevels(aParentNode: IXMLNode = nil; aLevel: Integer = 1);

  protected

  public
    constructor Create (aXSLFilename: string='');

    procedure TestData;

    procedure SaveToHTML (sXSLFilename, sFilename: string);
    procedure SaveToFile (aFilename : string);
    function  LoadFromFile (aFileName: string): boolean;

    procedure Clear;

  //  property XMLDOM: IXMLNode read GetXMLDOM;
    property DocumentElement : IXMLNode read GetDocumentElement;
    property XMLText: string read GetXMLText write SetXMLText;
  end;



//============================================================================//
implementation
//============================================================================//


function xml_Att (aName: string; aValue: Variant): TxmlAttrib;
begin
  Result.Name:=aName;
  Result.Value:=aValue;
end;


function xml_Par (aName: string; aValue: Variant): TxmlAttrib;
begin
  Result.Name:=aName;
  Result.Value:=aValue;
end;


//-----------------------------------------------------
// TXMLDoc
//-----------------------------------------------------

//-----------------------------------------------------
constructor TXMLDoc.Create (aXSLFilename: string='');
//-----------------------------------------------------
var sInitXML : string;
begin
  CoInitialize(nil);

  FXMLDOM := NewXMLDocument();

  with FXMLDOM do
  begin
    Encoding := 'windows-1251';
    Version := '1.0';
  end;

  FXMLDOM.AddChild('Document');

end;



//-----------------------------------------------------
function TXMLDoc.LoadFromFile (aFileName: string): boolean;
//-----------------------------------------------------
begin
  if FileExists (aFileName) then begin
    FXMLDOM.LoadFromFile(aFileName);
    Result:=True;
  end else
    Result:=false;
end;

//-----------------------------------------------------
procedure TXMLDoc.Clear;
//-----------------------------------------------------
//var /sInitXML : string;
 //   pi : Variant;
begin
//  sInitXML := '<Document></Document>';

  FXMLDOM.DocumentElement.ChildNodes.Clear;

//  XML.Text:='<Document></Document>';

{  sInitXML := ReplaceStr('type="text/xsl" href=":xsl-filename"', ':xsl-FileName', XSLFilename);
  pi := FXMLDOM.createProcessingInstruction('xml:stylesheet', sInitXML);
  FXMLDOM.insertBefore (pi, FXMLDOM.childNodes.item(0));

  sInitXML := 'version="1.0" encoding="windows-1251"';
  pi := FXMLDOM.createProcessingInstruction('xml', sInitXML);
  FXMLDOM.insertBefore (pi, FXMLDOM.childNodes.item(0));
}
end;


//===========================================
function TXMLDoc.GetDocumentElement: IXMLNode;
begin
//  FXMLDOM.

 // Result := FXMLDOM.documentElement;
  Result := FXMLDOM.ChildNodes.FindNode('Document');
end;

{
function TXMLDoc.GetXMLDOM: IXMLNode;
begin
  Result := FXMLDOM;
end;}

//-------------------------------------------
procedure TXMLDoc.SaveToHTML(sXSLFilename, sFilename : string);
//-------------------------------------------
var xsl,result,pi : Variant;
begin
  // load style sheet
{
  xsl := CreateOleObject('Microsoft.XMLDOM');
  xsl.async := false;
  xsl.load(sXSLFilename);

  // set up the resulting document
  result := CreateOleObject('Microsoft.XMLDOM');
  result.async := false;
  result.validateOnParse := true;

  FXMLDOM.transformNodeToObject(xsl, result);
  pi := result.createProcessingInstruction('xml', ' version="1.0" encoding="windows-1251"');
  result.insertBefore (pi, result.childNodes.item(0));

  result.save(sFilename);
}
end;


//===========================================
procedure TXMLDoc.SaveToFile (aFileName : string);
begin
  aFileName:=ChangeFileExt(aFileName, '.xml');
  ForceDirByFileName(aFileName);

  FXMLDOM.SaveToFile(aFileName);
end;


procedure TXMLDoc.TestData;
var
  v,vNode1,vNode2,vNode3: IXMLNode;
  vNode4: IXMLNode;
 // sSpace: string;
begin
 // vNode1:=FXMLDOM.DocumentElement;

  vNode1:=FXMLDOM.DocumentElement.AddChild('Node1');

  vNode2:=FXMLDOM.DocumentElement.AddChild('Node2');


  vNode2:=vNode1.AddChild('Node3');
  vNode2.Attributes['khjkhj']:='asdasdas';
  vNode2.Attributes['khjkdasdhj']:='asdasdas';


  vNode3:=vNode1.AddChild('Node4');
  vNode3.Attributes['khjkhj']:='asdasdas';
  vNode3.Attributes['khjkdasdhj']:='asdasdas';


 {

  vNode4:=vNode1.AddChild('Node1');
  vNode4.Attributes['khjkhj']:='asdasdas';
  vNode4.Attributes['khjkdasdhj']:='asdasd34534as';
  vNode4.Attributes['kdddhjkdasdhj']:='asdasddfghas';
  vNode4.Attributes['khjkghhfdasdhj']:='asdadfghsdas';

}

  InsertNodeLevels;

end;

//-------------------------------------------------------------------
procedure TXMLDoc.InsertNodeLevels(aParentNode: IXMLNode = nil; aLevel: Integer
    = 1);
//-------------------------------------------------------------------
var
  I: Integer;
  sSpace: string;
  sSpace2: string;
  b: boolean;
  s: string;
begin
  if aParentNode=nil  then
    aParentNode:=FXMLDOM.DocumentElement;

  sSpace   := chr(10) + StringOfChar(' ', 2*aLevel);
  sSpace2  := chr(10) + StringOfChar(' ', 2*(aLevel-1));

  for I := aParentNode.ChildNodes.Count - 1 downto 0 do
  begin
    InsertNodeLevels (aParentNode.ChildNodes[i], aLevel+1);

    aParentNode.ChildNodes.Insert(i, FXMLDOM.CreateNode(sSpace, ntText));
  end;

  if aParentNode.ChildNodes.Count>0 then
    aParentNode.ChildNodes.Add(FXMLDOM.CreateNode(sSpace2, ntText));
end;


procedure TXMLDoc.SetXMLText (aValue: string);
begin
  FXMLDOM.XML.Text:=aValue;
  FIsLevelsInserted :=False;
end;

function TXMLDoc.GetXMLText: string;
begin
  Result:=FXMLDOM.XML.Text;
end;


function  xml_GetIntAttr  (aNode: IXMLNode; aAttrName: string; aDefault:Integer=0):  Integer;
begin
  Result := AsInteger(xml_GetAttr(aNode,aAttrName,IntToStr(aDefault)));
end;

function xml_GetFloatAttr(aNode: IXMLNode; aAttrName: string; aDefault: double
    = 0): Double;
begin
  Result := AsFloat(xml_GetStringAttr(aNode,aAttrName,'0'));
end;

function xml_GetStringAttr  (aNode: IXMLNode; aAttrName: string; aDefault:string=''):  string ;
begin
  Result := AsString(xml_GetAttr(aNode,aAttrName,aDefault));
end;

//--------------------------------------------------------------------
function xml_AddNodeTag(aParentNode: IXMLNode; aTagName: string): IXMLNode;
//--------------------------------------------------------------------
begin
  Result := aParentNode.AddChild(aTagName);
end;


//--------------------------------------------------------------------
function xml_AddTextNode(aParentNode: IXMLNode; aText: string): IXMLNode;
//--------------------------------------------------------------------
var vDoc : IXMLDocument;
begin
  vDoc:=aParentNode.OwnerDocument;
  Result := vDoc.CreateNode  (aText,ntText);
  aParentNode.ChildNodes.Add(Result);
end;


//--------------------------------------------------------------------
function xml_AddNode (aParentNode: IXMLNode; aTagName: string;
                      aAttribs: array of TxmlAttrib) : IXMLNode;
//--------------------------------------------------------------------
var i : integer;
begin
  Result := aParentNode.AddChild(aTagName);

//  Result := xml_AddNodeTag(aParentNode, aTagName);
  xml_SetAttribs (Result, aAttribs);
{
  for i := 0 to High(aAttribs) do
   if not VarIsNull (aAttribs[i].Value) then
   try
       Result.SetAttribute (aAttribs[i].Name, aAttribs[i].Value);
   except end;
}
end;

//--------------------------------------------------------------------
procedure xml_SetAttribs (aNode: IXMLNode; aAttribs: array of TxmlAttrib);
//--------------------------------------------------------------------
var i : integer;
begin
  for i := 0 to High(aAttribs) do
 //  if not VarIsNull (aAttribs[i].Value) then
  // try
     aNode.Attributes [aAttribs[i].Name]:=aAttribs[i].Value;
 //  except end;
end;

{
//--------------------------------------------------------------------
procedure xml_SetAttr (aNode: IXMLNode; aAttrName: string; aAttrValue: Variant);
//--------------------------------------------------------------------
begin
  if VarIsEmpty(aNode) then
    raise Exception.Create('VarIsEmpty(aNode)');

  if Trim(aAttrName)='' then
    raise Exception.Create ('Blank attrbute name');

  try
    aNode.SetAttribute (aAttrName, aAttrValue);
  except end;
end;}



function xml_NodeName(aNode: IXMLNode): string;
begin
  Result:=aNode.NodeName;// TagName;
end;

//--------------------------------------------------------------------
function xml_GetAttr (aNode: IXMLNode; aAttrName: string; aDefault: string=''): Variant;
//--------------------------------------------------------------------
var
    i: integer;
    sName: string;
    vNode: IXMLNode;
    v: Variant;
begin
  if Trim(aAttrName)='' then
    raise Exception.Create ('Blank attrbute name');

  if VarIsEmpty(aNode) then
  begin
    Result := NULL;
    Exit;
  end;


  Result := aNode.Attributes[aAttrName];

  if VarIsNull(Result) then
    Result := aDefault;

end;

//-----------------------------------------------------------
function xml_GetTextByPath (aNode: IXMLNode; aPathArr: array of string): string;
//-----------------------------------------------------------
var vNode: IXMLNode;
begin
  vNode:=xml_GetNodeByPath (aNode, aPathArr);
  if not VarIsNull(vNode) then
    Result:=vNode.Text
  else
    Result:='';
end;

//-----------------------------------------------------------
function xml_GetNodeByPath (aNode: IXMLNode; aPathArr: array of string): IXMLNode;
//-----------------------------------------------------------
var i,j,iCOunt:integer;
    vNode: IXMLNode;
    strArr: array of string;
  sTag: string;
begin
  vNode:=aNode;
  Result:=nil;

  for i:=0 to aNode.childNodes.Count-1 do
  begin
    vNode:= vNode.ChildNodes.FindNode(aPathArr[i]);

    if VarIsNull(vNode) then   Break;
  end;

//    vElement:=aNode.childNodes.Item(i);



{  Result:=null;

  if VarIsNull(aNode) then
    Exit;

  iCOunt:=aNode.childNodes.Length;

  if High(aPathArr)>=0 then

  for i:=0 to aNode.childNodes.Length-1 do
  begin
    vElement:=aNode.childNodes.Item(i);

    if not xml_IsElement(vElement) then
      Continue;

    sTag:=vElement.TagName;

    if sTag='' then Continue;

    if Eq (sTag, aPathArr[0]) then
      if High(aPathArr)=0
       then begin Result:=vElement; Exit; end
       else begin
         SetLength(strArr, High(aPathArr));
         for j:=0 to High(aPathArr)-1 do strArr[j]:=aPathArr[j+1];

         Result:=xml_GetNodeByPath (vElement, strArr);
       end;
  end;

}
end;


function xml_GetAttrByTagName (aNode: IXMLNode; aTagName,aAttrName: string): string;
var vNode: IXMLNode;
begin
   vNode:=xml_GetNodeByPath (aNode, [aTagName]);
   if VarIsNull(vNode) then Result:=''
                       else Result:=xml_GetAttr (vNode, aAttrName);
end;


function xml_GetIntAttrByTagName (aNode: IXMLNode; aTagName,aAttrName: string): integer;
var vNode: IXMLNode;
begin
  Result:=AsInteger(xml_GetAttrByTagName(aNode, aTagName,aAttrName));
end;



//-----------------------------------------------------------------------
function xml_FindNode(aParentNode: IXMLNode; aNodeName, aAttrName, aAttrValue:
    string): IXMLNode;
//-----------------------------------------------------------------------
var i: Integer;
   vRoot,vNode: IXMLNode;   s,sTag,sType: string;
begin
//  Result := aParentNode.ChildNodes.FindNode(aNodeName)

{  for i:=0 to aParentNode.ChildNodes.Count-1 do
  begin
    vNode:=aParentNode.ChildNodes.Item(i);

   // s:=vNode.nodeTypeString;

    if not xml_IsElement(vNode) then
      Continue;

    sTag:=vNode.TagName;

    if sTag='' then Continue;

    if Eq(sTag, aNodeName) then
     if xml_GetAttr (vNode, aAttrName) = aAttrValue then begin
       Result:=vNode; Exit;
     end;
  end;

  Result:=null;
}

end;


function xml_GetAttrByPath (aElement: IXMLNode; aTagName,aAttrName: string): string;
begin
   Result:=xml_GetAttrByTagName (aElement,aTagName,aAttrName);
end;




function xml_IsElement(aNode: IXMLNode): boolean;
begin
  Result:=(aNode.NodeType =  ntElement);// 'element');
end;


var
  obj: TXMLDoc;
begin
  obj:=TXMLDoc.Create();
  obj.TestData;
  obj.SaveToFile('d:\a.xml');

end.


