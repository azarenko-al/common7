unit u_Bmp_file;

interface
uses
  Windows, SysUtils, Classes, Graphics, u_func
  ;

Type

  // ---------------------------------------------------------------
  TBmpFile = class(TObject)
  // ---------------------------------------------------------------
  private
    FBitmapSize: Int64;
    FWidth: Int64;
    FHeight: Int64;

    FLineIndex : Integer;

    FBytesPerCell: Integer;

 //   FCol : Integer;

    FFileStream: TFileStream;
    FMemStream: TMemoryStream;

    procedure LoadHeader(aFileName: string);

    function WriteHeader(AWidth, AHeight: Integer; aBitCount: byte = 24): Boolean;
  public
    constructor Create_Width_Height(AFileName: string; AWidth, AHeight: LongInt;
        aBitCount: byte = 24);

    destructor Destroy; override;

//    procedure CreateFile(aFileName: string; aWidth, aHeight: Longint);

    procedure Write(aColor: Integer);
    procedure Writeln;

    procedure CloseFile;
    procedure Write_Byte(aValue: Byte);
  end;


type
  TBmpInfoRec = record
    Width: LongInt;                  // ������ �����������, pix
    Height: LongInt;                 // ������ �����������, pix
  end;


  {
  function bmp_LoadInfoFromFile(aFileName: string; var aRec: TBmpInfoRec):
      Boolean;
   }
   

//===============================================================
implementation


const
  BMP_MAGIC: Word = $4D42;     // 'BM'
  BMP_SIZE_LIMIT = $FFFFFFFF;  // 4Gb

type



 // Each pixel is a combination of Blue, Green and Red Values
   // In other words, A pixel is uniquely represented by 3 bytes
   // Maximum Value of any of them is $FF (255 in Decimal)
//   TRGB = packed Record
   TRGB = Record
     rgbBlue : BYTE;  //intensity of blue in the color
     rgbGreen: BYTE;  //intensity of green in the color
     rgbRed  : BYTE;  //intensity of red in the color
   End;



  TBitmapFileHeader = packed record  // File Header for Windows/OS2 bitmap file
    Magic: Word;                     // ���������: 'BM'
    Size: LongWord;                  // ������ �����
    Reserved1: Word;                 // -
    Reserved2: Word;                 // -
    Offset: LongWord;                // �������� �� ������ ����� �� ������ �����������
  end;

  TBitmapInfoHeader = packed record  // Info Header for Windows bitmap file
    Size: LongWord;                  // ����� ���������, byte (=40)
    Width: LongInt;                  // ������ �����������, pix
    Height: LongInt;                 // ������ �����������, pix
    Planes: Word;                    // ����� ���������� (=1)
    BitCount: Word;                  // ������� �����, bit/pix (=24)
    Compression: LongWord;           // ��� ������ ��� ������ ����������� (=0)
    SizeImage: LongWord;             // ������ �����������, byte
    XPelsPerMeter: LongInt;          // �������������� ���������� � �������� �� ����
    YPelsPerMeter: LongInt;          // ������������ ���������� � �������� �� ����
    ClrUsed: LongInt;                // ����� ������ (0 - ������������ �����������-����������)
    ClrImportant: LongInt;           // ����� �������� ������
  end;



procedure ColorToRGB_(Value: TColor; var R,G,B: byte); forward;


// ---------------------------------------------------------------
constructor TBmpFile.Create_Width_Height(AFileName: string; AWidth, AHeight:
    LongInt; aBitCount: byte = 24);
// ---------------------------------------------------------------
begin
  inherited Create;

  aFileName:= ChangeFileExt(aFileName, '.bmp');

  ForceDirectories (ExtractFileDir (aFileName));


//  AFileName

  FBytesPerCell:=aBitCount div 8;

  FWidth  := AWidth;
  FHeight := AHeight;

//  FBitmapSize := FWidth * FHeight * 3 + (FWidth mod 4) * FHeight;
  FBitmapSize := FWidth * FHeight * FBytesPerCell + (FWidth mod 4) * FHeight;

  if FBitmapSize < BMP_SIZE_LIMIT then
  begin
    FFileStream := TFileStream.Create(AFileName, fmCreate);
    FFileStream.Size := SizeOf(TBitmapFileHeader) + SizeOf(TBitmapInfoHeader) + FBitmapSize;

    WriteHeader(AWidth, AHeight, aBitCount);
    
  end else
    raise Exception.Create('Image is too big! Maximum size = 4Gb (current size = '
      + IntToStr(FBitmapSize div (1024*1024*1024)) + ' Gb)');

  FMemStream := TMemoryStream.Create();
end;



destructor TBmpFile.Destroy;
begin
  FreeAndNil(FFileStream);
  FreeAndNil(FMemStream);
  inherited Destroy;
end;

// ---------------------------------------------------------------
function TBmpFile.WriteHeader(AWidth, AHeight: Integer; aBitCount: byte = 24):
    Boolean;
// ---------------------------------------------------------------
var
  rFileHeader: TBitmapFileHeader;
  rInfoHeader: TBitmapInfoHeader;
begin
  Result := False;

  Assert((aBitCount=8) or (aBitCount=24));

  ZeroMemory(@rFileHeader, SizeOf(TBitmapFileHeader));
  rFileHeader.Magic := BMP_MAGIC;
  rFileHeader.Size  := SizeOf(TBitmapFileHeader) + SizeOf(TBitmapInfoHeader) + FBitmapSize;
  rFileHeader.Offset:= SizeOf(TBitmapFileHeader) + SizeOf(TBitmapInfoHeader);

  ZeroMemory(@rInfoHeader, SizeOf(TBitmapInfoHeader));
  rInfoHeader.Size := SizeOf(TBitmapInfoHeader);
  rInfoHeader.Width := AWidth;
  rInfoHeader.Height := AHeight;
  rInfoHeader.Planes := 1;
//  rInfoHeader.BitCount := 24;

  rInfoHeader.BitCount := aBitCount;

  rInfoHeader.SizeImage := FBitmapSize;

  if Assigned(FFileStream) then
  begin
    FFileStream.Position := 0;
    if FFileStream.Write(rFileHeader, SizeOf(TBitmapFileHeader)) = SizeOf(TBitmapFileHeader) then
      Result := FFileStream.Write(rInfoHeader, SizeOf(TBitmapInfoHeader)) = SizeOf(TBitmapInfoHeader);
  end;
end;

// ---------------------------------------------------------------
procedure TBmpFile.CloseFile;
// ---------------------------------------------------------------
begin
  FMemStream.SaveToStream(FFileStream);
  FMemStream.Position:=0;

  FreeAndNil(FFileStream);
end;

// ---------------------------------------------------------------
procedure TBmpFile.LoadHeader(aFileName: string);
// ---------------------------------------------------------------
var
  VFileHeader: TBitmapFileHeader;
  VInfoHeader: TBitmapInfoHeader;  


///var
///  rBitmapHeader: TBmpHeader;
begin
  FFileStream:=TFileStream.Create(aFileName, fmOpenRead);

  FFileStream.Read(VFileHeader, SizeOf(TBitmapFileHeader));
  FFileStream.Read(VInfoHeader, SizeOf(TBitmapInfoHeader));


 /////// FFileStream.Read(rBitmapHeader, SizeOf(TBmpHeader));

(*  Width :=  rBitmapHeader.biWidth;
  Height :=  rBitmapHeader.biHeight;*)

  FreeAndNil(FFileStream);
end;

// ---------------------------------------------------------------
procedure TBmpFile.Writeln;
// ---------------------------------------------------------------
const
  bZero: Byte=0;
var
  j: Integer;
  k1: Integer;
  k2: Integer;
begin
//  k1 := FWidth * 3 + (FWidth mod 4);
  k1 := FWidth * FBytesPerCell + (FWidth mod 4);

  k2 := (FHeight - 1) * k1 + SizeOf(TBitmapFileHeader) + SizeOf(TBitmapInfoHeader);

  FFileStream.Position := k2 - FLineIndex * k1;

  Inc(FLineIndex);

  for j := 0 to (FWidth mod 4) - 1 do
    FMemStream.Write(bZero, 1);

  FMemStream.SaveToStream(FFileStream);
  FMemStream.Position:=0;
end;

// ---------------------------------------------------------------
procedure TBmpFile.Write(aColor: Integer);
// ---------------------------------------------------------------
var
  j: Integer;
  k: Integer;
  RGB : TRGB;
  myBuffer : Byte;
begin
  if aColor<0 then
     aColor:=0;

  ColorToRGB_ (aColor, RGB.rgbRed, RGB.rgbGreen, RGB.rgbBlue);

  FMemStream.Write(RGB, 3);

end;


// ---------------------------------------------------------------
procedure TBmpFile.Write_Byte(aValue: Byte);
// ---------------------------------------------------------------
{
var
  j: Integer;
  k: Integer;
  RGB : TRGB;
  myBuffer : Byte;
 }
 
begin
//  if aColor<0 then
//     aColor:=0;

 // ColorToRGB_ (aColor, RGB.rgbRed, RGB.rgbGreen, RGB.rgbBlue);

  FMemStream.Write(aValue, 1);

end;



//--------------------------------------------------------
procedure ColorToRGB_(Value: TColor; var R,G,B: byte);
//--------------------------------------------------------
begin
  r:=GetRValue(Value);
  g:=GetGValue(Value);
  b:=GetBValue(Value);
end;




//--------------------------------------------------------
procedure Test;
//--------------------------------------------------------
const
  DEF_FILE = 'd:\__1111.bmp';

var
  I: Integer;
  j: Integer;
  oFile: TBmpFile;
begin
//
//  oFile:=TBmpFile.Create;
////  oFile.Create_Width_Height_('d:\__1111.bmp',100,100, 24);
//  oFile.Create_Width_Height(DEF_FILE, 100,100, 8);
//
//  for I := 0 to 100 - 1 do    // Iterate
//  begin
//    for j := 0 to 100 - 1 do    // Iterate
//      oFile.Write_Byte(123);
////      oFile.Write(clRed);
//
//    oFile.Writeln;
//  end;
//
//
//  ShellExec(DEF_FILE);
//
//  halt;
//  
//  ShowMessage('');

end;

    {

function bmp_LoadInfoFromFile(aFileName: string; var aRec: TBmpInfoRec):
    Boolean;
begin
  // TODO -cMM: bmp_LoadInfoFromFile default body inserted
//  Result := ;
end;

         }

begin
 // Test;


end.
