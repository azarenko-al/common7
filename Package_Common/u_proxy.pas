unit u_proxy;

interface

uses
  Dialogs, JPEG, PNGImage, ExtCtrls, Graphics,  SysUtils, Classes, Registry, Windows, WinSock,
  StrUtils,
  StdVcl, IdComponent, IdTCPConnection, IdTCPClient, IdBaseComponent, IdHTTP;


function DetectIEProxyServer(var aProxyHost: string; var aProxyPort: integer):
    Boolean;


//function GetIPAddress(NetworkName: String): String;



implementation

function GetIPAddress(NetworkName: String): String; forward;



// ---------------------------------------------------------------
function DetectIEProxyServer(var aProxyHost: string; var aProxyPort: integer):
    Boolean;
// ---------------------------------------------------------------
var
  bProxyEnable: Boolean;
  k: Integer;
//  oReg: TRegIniFile;
  s: string;
  sProxyServer: string;
begin
  Result:=False;

//    
//   oReg:=TRegIniFile.Create('\Software\Microsoft\Windows\CurrentVersion\Internet Settings');
//
//      Result :=oReg.ReadString('','ProxyServer','');
//
//      
//      s:=oReg.ReadString('','ProxyEnable','0');
//
//      
//      bProxyEnable :=oReg.ReadInteger('','ProxyEnable',0) = 1;
//
//      
//      FreeAndNil(oReg);
//
//      
//      Exit;
//
    
    with TRegistry.Create do
    try
        RootKey := HKEY_CURRENT_USER;
            
        if OpenKey('\Software\Microsoft\Windows\CurrentVersion\Internet Settings', False) then 
        begin
          sProxyServer := ReadString('ProxyServer');
 
          k:=LastDelimiter(':',sProxyServer);
          if k>0 then
          begin
            aProxyHost:=LeftStr(sProxyServer,k-1);

            s:= Copy(sProxyServer,k+1, 100);
                
            aProxyPort:=StrToIntDef(s,0);
          end;
 
          k :=ReadInteger('ProxyEnable');
          Result:=k=1;
                                       
          //ProxyEnable

              
          CloseKey;
        end;
       // else
        //  Result := '';
    finally
      Free;
    end;


   if result then
     aProxyHost:=GetIPAddress (aProxyHost);

end;





// ---------------------------------------------------------------
function GetIPAddress(NetworkName: String): String;
// ---------------------------------------------------------------
var
  Error: DWORD;
  HostEntry: PHostEnt;
  Data: WSAData;
  Address: In_Addr;
begin
     Error:=WSAStartup(MakeWord(1, 1), Data);
     if Error = 0 then 
     begin
//          HostEntry:=gethostbyname(PansiChar(NetworkName));
          HostEntry:=gethostbyname(PansiChar(AnsiString(NetworkName)));

          
          Error:=GetLastError();
          if Error = 0 then
          begin
               Address:=PInAddr(HostEntry^.h_addr_list^)^;
               Result:=inet_ntoa(Address);
          end
          else
          begin
               Result:='Unknown!!!!!!!!';
          end;

     end
     else
     begin
          Result:='Error!!!!!!';
     end;
     WSACleanup();
end;



end.
