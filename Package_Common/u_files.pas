unit u_files;

interface
uses
  Dialogs, Windows,SysUtils, Forms, Classes, Registry, ShlObj,
  rxFileUtil, ShellAPI, FileCtrl,

  u_assert,

  u_func

  ;


//  if FileCtrl.SelectDirectory('����� �����...','',sDir) then
//    cx_SetButtonEditText(Sender, sDir);


procedure ShellExec_Notepad_temp1(aText: string; aExt: string = '');

procedure ShellExec(aFileName: string; aFileParams: string = '');

  function dlg_BrowseDirectory(var AFolderName: string; aDlgText: string): Boolean;

  function GetApplicationDir (): string;
  function CorrectFileName(aFileName: string): string;

  function GetTempFileDir (): string;
  function GetTempFileNameWithExt(aExt: string): string;
  function GetTempXmlFileName (): string;

//  procedure FindFilesByMask_11(aDir,aMask: string; aList: Tstrings; aNeedClear:
///      boolean);

//  procedure FindFilesByMaskEx(const aPath, aExtensions: string; var aFiles: TStringList);


  //---------------------------------------------------
  // Directories
  //---------------------------------------------------

//  procedure FindDirectoriesWithoutChilds(aDir: ShortString; var aList: TStringList);
//  procedure FindDirectories (aDir: string; aList: Tstrings);

  function  DirClear           (aPath: string): Boolean;
//  function  ClearDirRecursive  (aPath: string): Boolean;

  procedure ForceDirectories   (aDir: string);
  procedure ForceDirByFileName (aFileName: string);

 // procedure RemoveEmptyDir11111111111(aDirList: TstringList);

//  procedure DirectoryCopy (aSrcDir,aDestDir: string);

  //---------------------------------------------------
  // shell functions
  //---------------------------------------------------
  function ExtractFileNameNoExt (aFileName: string): string;

  procedure MoveFile (aFileName, aDestName: string);

  function GetFileSize(aFileName: string): Int64;

  function  GetFileDate (aFileName: string): TDateTime;

  function GetApplicationFileDateAsStr: string;



  //FileUtil

//  procedure ShellExec       (aFileName,aFileParams: string);


  function  IsDirEmpty      (aDir: string): boolean; export;
//  procedure BuildFileList11111(aFileMask: string; astrings: Tstrings);
//  procedure BuildFileNameNoExtList (aFileMask:  string; astrings: Tstrings);

  function  GetParentFileDir       (aFileName: string): string;
//  function ChangeParentFileDir1111(aFileName,aNewDir: string): string;

  procedure FileCopy_Src_Dest(aSrcFileName, aDestFileName: string);

  procedure StrToTxtFile(aStr: string; aFileName: string);
  function TxtFileToStr(aFileName: string): string;

//  function PathIsLocal11111111111(aFileName: string): boolean;

  function GetFileExt(aFileName: string): string;

  function GetCommonApplicationDataDir_Local_AppData(aApplicationName: string):
      string;

  function RunApp(aFileName, aParamStr: string; var aCode: Integer): boolean;

  function GetFileModifyDate(aFileName: string): TDateTime;

  function mapx_FileSize(aFileName: string): int64;

  function ScanDir1(aPath, aExtensions: string; aFiles: TStrings): Integer;

function GetSpecialFolderPath_AppData: string;

function GetSpecialFolderPath_MyDocuments: string;

procedure Dir_delele(const aDir: String);

procedure FileCopy(aSrcFileName, aDestFileName: string);

function ScanDirToStrArray(aPath, aExtensions: string): TStrArray;

function ReplaceInvalidFileNameChars(const aFileName: string; const
    aReplaceWith: Char = '_'): string;

//function Dlg_SelectDirectory(const aDirName: string): Boolean;



implementation

uses StrUtils;


function SysGetTempFileName(aExt: string = '.tmp'): string; forward;

// ---------------------------------------------------------------
procedure ShellExec_Notepad_temp1(aText: string; aExt: string = '');
// ---------------------------------------------------------------
var
  sFile: string;
begin
  sFile :=SysGetTempFileName(aExt);

  strToTxtFile(aText, sFile);

 // ShellExecute(Handle,
 // zz'open', 'c:\MyDocuments\Letter.doc', nil, nil, SW_SHOWNORMAL);

  ShellExecute(0, 'open',
     'notepad.exe', PChar(sFile), nil, SW_SHOWNORMAL);

 // if aFileName<>'' then
 //   ShellExecute(0,nil, pChar(string(aFileName)), pChar(string(aFileParams)), nil, SW_NORMAL);

//  ShellExecute(0,nil, pChar(aFileName), pChar(aFileParams), nil, SW_NORMAL);
end;


//---------------------------------------------------------------------------
procedure ShellExec(aFileName: string; aFileParams: string = '');
//---------------------------------------------------------------------------
var
  r: Integer;
begin
  Assert(aFileName<>'');
 // Assert(FileExists( aFileName));

//  if aFileName<>'' then

//function ShellExecute(hWnd: HWnd; Operation, FileName, Parameters, Directory: PChar; ShowCmd: Integer): HINST; stdcall;

  r := ShellExecute(0,nil, pChar(string(aFileName)), pChar(string(aFileParams)), nil, SW_NORMAL);
  r:=0;

//  ShellExecute(0,nil, pChar(aFileName), pChar(aFileParams), nil, SW_NORMAL);
end;



//------------------------------------------------
function SysGetTempFileName(aExt: string = '.tmp'): string;
//------------------------------------------------
begin
  Randomize;

  Result:=GetTempFileDir() + IntToStr(Random (1000000)) + aExt;
  if FileExists (Result) then
    Result:=SysGetTempFileName();
end;


function GetApplicationFileDateAsStr: string;
begin
  Result :=FormatDateTime ('yyyy-mm-dd hh:mm', GetFileDate (Application.Exename));
  // TODO -cMM: GetApplicationFileDateAsStr default body inserted
//  Result := ;
end;



//------------------------------------------------
function GetTempFileNameWithExt(aExt: string): string;
//function GetTempFileNameWithExt (aExt: string): string;
//------------------------------------------------
begin
  Result:=SysGetTempFileName();
  if aExt<>'' then
    Result:=ChangeFileExt (Result, '.'+aExt);
end;


function GetTempXmlFileName(): string;
begin
  Result:=GetTempFileNameWithExt('xml');
end;


function ExtractFileNameNoExt (aFileName: string): string;
begin
  Result:=stringReplace (ExtractFileName(aFileName),
                         ExtractFileExt(aFileName), '', [rfReplaceAll]);
end;


function GetParentFileDir (aFileName: string): string;
begin
  Result:=IncludeTrailingBackslash(ExtractFileDir (ExcludeTrailingBackslash(aFileName)));
end;


(*
function ChangeParentFileDir1111(aFileName,aNewDir: string): string;
var
  sDir: string;
begin
  sDir:=ExtractFileDir(ExtractFileDir(aFileName));
  sDir:=IncludeTrailingBackslash(sDir) + aNewDir + '\';
  Result:=sDir + ExtractFileName(aFileName);
end;

*)


procedure ForceDirByFileName (aFileName: string);
//var
 // s: string;
begin

//  s:=ExtractFileDir (aFileName);
  ForceDirectories (ExtractFileDir (aFileName));
end;


procedure ForceDirectories(aDir: string);
begin
  rxFileUtil.ForceDirectories (aDir);
end;

procedure FileCopy(aSrcFileName, aDestFileName: string);
begin
  FileCopy_Src_Dest(aSrcFileName, aDestFileName );
end;


//-------------------------------------------------------------------
procedure FileCopy_Src_Dest(aSrcFileName, aDestFileName: string);
//-------------------------------------------------------------------
var
  b: Boolean;
begin
  AssertFileExists(aSrcFileName);


  b:=DeleteFiles(aDestFileName);
  Assert(not FileExists(aDestFileName), 'FileExists(aDestFileName): '+ aDestFileName);




  if Eq(aSrcFileName,aDestFileName) then Exit;
  if not FileExists(aSrcFileName) then Exit;

  ForceDirByFileName (aDestFileName);

  try
    CopyFile(PChar(aSrcFileName), PChar(aDestFileName), nil);

  except
    raise Exception.Create('');
  end;    // try/finally


//
//{ TODO : ! }
////  MsgDlg('ddddddddddddddd');
//  SysUtils.DeleteFile (aDestFileName);
////  Windows.CopyFile (PChar(aSrcFileName), PChar(aDestFileName), false);
////  Windows.CopyFile (aSrcFileName, PChar(aDestFileName), false);
//
//  rxFileUtil.CopyFile(aSrcFileName, aDestFileName, nil);

end;

function GetApplicationDir (): string;
begin
  Result:=IncludeTrailingBackslash(ExtractFileDir(Application.ExeName));
end;


function GetFileDate (aFileName: string): TDateTime;
begin
  if FileExists(aFileName) then
    Result := rxFileUtil.FileDateTime (aFileName)
  else
    Result := 0;

end;

//-------------------------------------------------------------------
function IsDirEmpty (aDir: string): boolean;
//-------------------------------------------------------------------
var
  i: Integer;
  rInfoRec: TSearchRec;
begin
  aDir:=IncludeTrailingBackslash(aDir);

  i:=0;
  if SysUtils.FindFirst(aDir + '*.*',faAnyFile,rInfoRec)=0 then
  begin
    repeat
      if (rInfoRec.Name<>'.') and (rInfoRec.Name<>'..') then   Inc(i);
    until
       SysUtils.FindNext(rInfoRec)<>0;

    SysUtils.FindClose(rInfoRec);
  end;

  Result:= i=0;
end;


//-------------------------------------------------------------------
function DirClear (aPath: string): Boolean;
//-------------------------------------------------------------------
begin
 Result:=rxFileUtil.ClearDir (aPath, True);

//  Result:=rxFileUtil.ClearDir (aPath, False);

//  Result:=rxFileUtil.DeleteFiles (IncludeTrailingBackslash(aPath) + '*.*');

//  Result:=rxFileUtil.ClearDir (aPath, False);
end;


//-------------------------------------------------------------------
procedure Dir_delele(const aDir: String);
//-------------------------------------------------------------------
var
  sDir: String;
  rRec: TSearchRec;
  
begin
  sDir := IncludeTrailingPathDelimiter(aDir);

  if FindFirst(sDir + '*.*', faAnyFile, rRec) = 0 then
  try
    repeat
      if (rRec.Attr and faDirectory) = faDirectory then
      begin
        if (rRec.Name <> '.') and (rRec.Name <> '..') then
          RemoveDir(sDir + rRec.Name);
      end else
      begin
        DeleteFile(sDir + rRec.Name);
      end;
    until FindNext(rRec) <> 0;
  finally
    FindClose(rRec);
  end;

  RemoveDir(sDir);
end;


//------------------------------------------------
function GetTempFileDir (): string;
//------------------------------------------------
begin
  Result:=GetTempDir();
end;


procedure MoveFile (aFileName, aDestName: string);
begin
  rxFileUtil.MoveFile (aFileName, aDestName);
end;


function dlg_BrowseDirectory(var AFolderName: string; aDlgText: string): Boolean;
var
  S: string;
begin
  s:=AFolderName;
  Result := rxFileUtil.BrowseDirectory(s,aDlgText,0);
  AFolderName:=s;
end;

// ---------------------------------------------------------------
function ScanDirToStrArray(aPath, aExtensions: string): TStrArray;
// ---------------------------------------------------------------
var
  I: Integer;
  oFiles: TStringList;
begin
  oFiles:=TStringList.create;

  ScanDir1(aPath, aExtensions, oFiles);

  SetLength (Result, oFiles.Count);

  for I := 0 to oFiles.Count - 1 do
    Result[i]:=oFiles[i];


  FreeAndNil (oFiles);
end;


// ---------------------------------------------------------------
function ScanDir1(aPath, aExtensions: string; aFiles: TStrings): Integer;
// ---------------------------------------------------------------

   procedure DoScan(aDirPath, aExtension: string);
   var
     rSearchRec: TSearchrec;

     a,i:integer;

     //oExtStrList: TStringList;
     sExt: string;
   begin
     aDirPath := IncludeTrailingBackslash(aDirPath);

   end;


var
  SearchRec: TSearchrec;

  a,i:integer;

  oExtStrList: TStringList;
  sExt: string;

begin
  Assert(Length(aPath)>=3);


  aPath := IncludeTrailingBackslash(aPath);


 // aFiles.Clear;

  oExtStrList:=TStringList.Create;
  oExtStrList.Delimiter:=';';
  oExtStrList.DelimitedText:=aExtensions;

//  aPath := IncludeTrailingBackslash(aPath);

  FindFirst(aPath + '*.*',faDirectory,SearchRec); {. found}
  FindNext(SearchRec);    {.. found}

  a:=FindNext(SearchRec);

  while a=0 do
  begin

    if (SearchRec.Attr and faDirectory)>0 then
      ScanDir1(aPath + SearchRec.Name, aExtensions, aFiles);

    a:=FindNext(SearchRec);

  end;

  FindClose(SearchRec);


  for i:=0 to oExtStrList.Count-1 do
  begin
    sExt :=oExtStrList[i];

    a:=FindFirst(aPath + sExt, faAnyFile, SearchRec);

    while a=0 do
    begin
      if RightStr(SearchRec.Name, Length('.temp.mdb')) <> '.temp.mdb' then
        aFiles.Add(aPath + SearchRec.Name);

      a:=FindNext(SearchRec);
    end;

    FindClose(SearchRec);
  end;

  FreeAndNil(oExtStrList);

  Result := aFiles.Count;

end;

  

procedure StrToTxtFile(aStr: string; aFileName: string);
var list: TstringList;
begin
  ForceDirByFileName(aFileName);

  list:=TstringList.Create;
  list.Text:=aStr;
  list.SaveToFile (aFileName);
  list.Free;
end;


function TxtFileToStr(aFileName: string): string;
var oList: TstringList;
begin
  oList:=TstringList.Create;
  oList.LoadFromFile (aFileName);
  Result:=oList.Text;
  oList.Free;
end;


// ---------------------------------------------------------------
function CorrectFileName(aFileName: string): string;
// ---------------------------------------------------------------
//remove . , : -> _
// ---------------------------------------------------------------

  function ReplaceStr (Value, OldPattern, NewPattern: string): string;
  begin
    Result:=stringReplace (Value, OldPattern, NewPattern, [rfReplaceAll, rfIgnoreCase]);
  end;


begin
  Result := ReplaceStr(aFileName, '/',' ');
  Result := ReplaceStr(Result,    '\',' ');
  Result := ReplaceStr(Result,    '.',' ');
  Result := ReplaceStr(Result,    ':',' ');
  Result := ReplaceStr(Result,    '>',' ');
  Result := ReplaceStr(Result,    '<',' ');
  Result := ReplaceStr(Result,    ',',' ');
end;

function GetFileExt(aFileName: string): string;
begin
  Result := ExtractFileExt (aFileName);
end;


//------------------------------------------------------
function  mapx_FileExists (aFileName: string): boolean;
//------------------------------------------------------
begin
  Result:=
    (FileExists(ChangeFileExt (aFileName, '.dat')) and
     FileExists(ChangeFileExt (aFileName, '.id'))  and
     FileExists(ChangeFileExt (aFileName, '.map'))  and
     FileExists(ChangeFileExt (aFileName, '.tab'))
    )

    or

    (FileExists(ChangeFileExt (aFileName, '.tif')) and
     FileExists(ChangeFileExt (aFileName, '.tab'))
    )


end;



//------------------------------------------------------
function mapx_FileSize(aFileName: string): int64;
//------------------------------------------------------
begin
  Result:= -1;

  if not mapx_FileExists(aFileName) then
    exit;
                   
  Result:= GetFileSize(ChangeFileExt (aFileName, '.dat')) +
           GetFileSize(ChangeFileExt (aFileName, '.id')) +
           GetFileSize(ChangeFileExt (aFileName, '.map')) +
           GetFileSize(ChangeFileExt (aFileName, '.tab'));

  if FileExists(ChangeFileExt (aFileName, '.ind')) then
    Result:= Result +
           GetFileSize(ChangeFileExt (aFileName, '.ind'));
end;


// ---------------------------------------------------------------
function GetFileSize(aFileName: string): Int64;
// ---------------------------------------------------------------
var
  hFind: THandle;
  FindData: TWin32FindData;
begin
  Result:= -1;

  hFind := FindFirstFile(PAnsiChar(aFileName), FindData);
  if hFind <> INVALID_HANDLE_VALUE then
  begin
    // ��� � SDK
    Result := Int64(FindData.nFileSizeHigh) * MAXDWORD + FindData.nFileSizeLow;
    // �������
    Windows.FindClose(hFind);
  end;
end;




// ---------------------------------------------------------------
function GetFileModifyDate(aFileName: string): TDateTime;
// ---------------------------------------------------------------
var
  SR: TSearchRec;
  r : integer;
  LocalTime : tFileTime;
  DOSFileTime : DWord;
begin
  Result := 0;
  if not FileExists(aFileName) then exit;

  r := FindFirst(aFileName, faAnyFile, SR);

  FileTimeToLocalFileTime(SR.FindData.ftLastWriteTime, LocalTime); // Compensate for time zone
  FileTimeToDosDateTime(LocalTime, LongRec(DOSFileTime).Hi,
     LongRec(DOSFileTime).Lo);
  Result := FileDateToDateTime(DOSFileTime);
end;



// ---------------------------------------------------------------
function GetSpecialFolderPath_MyDocuments: string;
// ---------------------------------------------------------------
 var
    r: Bool;
    sPath: array[0..MAX_PATH] of Char;
 begin
//   CSIDL_CSIDL_APPDATA

  r := ShGetSpecialFolderPath(0, sPath, CSIDL_Personal, False) ;
  if not r then
    raise Exception.Create('Could not find MyDocuments folder location.') ;

  Result := IncludeTrailingBackslash( sPath);
end;

// ---------------------------------------------------------------
function GetSpecialFolderPath_AppData: string;
// ---------------------------------------------------------------
const
  CSIDL_LOCAL_APPDATA  = $001c;  // <user name>\Local Settings\Applicaiton Data (non roaming)

var
  r: Bool;
  sPath: array[0..MAX_PATH] of Char;

begin
//   CSIDL_CSIDL_APPDATA

  r := ShGetSpecialFolderPath(0, sPath, CSIDL_LOCAL_APPDATA, False) ;
//  r := ShGetSpecialFolderPath(0, sPath, CSIDL_APPDATA, False) ;
  if not r then
    raise Exception.Create('Could not find APPDATA folder location.') ;

  Result := IncludeTrailingBackslash( sPath);
end;



//-------------------------------------------------------------------
function GetCommonApplicationDataDir_Local_AppData(aApplicationName: string):
    string;
//-------------------------------------------------------------------
var
  sAppName : string;
begin
  Assert(aApplicationName<>'', 'GetCommonApplicationDataDir_Local_AppData - aApplicationName');


  Result:= GetSpecialFolderPath_AppData();


  Result:= IncludeTrailingBackslash(Result)+ aApplicationName+ '\';

 // exit;

  ///////////////////////////
   {
   with TRegistry.Create do
    try
      RootKey := HKEY_CURRENT_USER;

//        REGSTR_PATH_SPECIAL_FOLDERS   = REGSTR_PATH_EXPLORER + '\Shell Folders';

//      if OpenKey('Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders\', False) then

      if OpenKey(REGSTR_PATH_SPECIAL_FOLDERS, False) then
         Result :=ReadString('Local AppData');
      CloseKey;

    finally
      Free;
    end;


  Assert(Result<>'');

  Result:= IncludeTrailingBackslash(Result)+ aApplicationName+ '\';
   }
end;


//----------------------------------------------------------
function RunApp(aFileName, aParamStr: string; var aCode: Integer): boolean;
//----------------------------------------------------------
var
  PI: TProcessInformation;
  SI: TStartupInfo;
  sProgName,sFileName: string;
  i,j: integer;
  iCode: DWORD;

begin

  Result:=False;

  FillChar(SI, SizeOf(SI), 0);
  SI.cb := SizeOf(SI);
  SI.lpTitle:=PChar('');
  SI.dwFlags:=SI.dwFlags or STARTF_USESHOWWINDOW;
  SI.wShowWindow:=SW_SHOW;

  sFileName:=aFileName;

  Application.ProcessMessages;


//  if not FileExists(aFileName) then
 //   aFileName:=

 ///////  MsgDlg ('1-'+aFileName);

  if not FileExists(sFileName) then
  begin
    sFileName:=ExtractFileName(sFileName);
    sFileName:=FileSearch  (sFileName,
                            GetApplicationDir + ';' +
                            GetEnvironmentVariable('path')+ ';' +
                           // GetApplicationDir+'DLL'+ ';' +
                            GetWindowsDir + ';' +
                            GetSystemDir + ';'
                           // 'c:\onega\rpls_db\bin'
                            );

    if not FileExists(sFileName) then
    begin
      ShowMessage('���� �� ������: ' + aFileName);

      Exit;
    end;
  end;



///////   MsgDlg ('1-'+sFileName);

  if CreateProcess(nil, PChar(sFileName + ' ' + aParamStr), nil, nil, False, 0, nil, nil, SI, PI) then
  begin
//    j:=WaitForSingleObject(PI.HProcess, INFINITE);

    repeat
      j:=WaitForSingleObject(PI.HProcess, 100);

     if Assigned(Application.MainForm) then
       Application.MainForm.Refresh;

    //zzzzzzzzzzzzzzzzzzzz  Application.ProcessMessages;
    until
       j=0;

  //  GetExitCodeProcess(PI.hProcess, iCode);
 //   Result:=(iCode=0);

    GetExitCodeProcess(PI.hProcess, iCode);
    Result:=(iCode=0);


    aCode:=iCode;

    CloseHandle(PI.hThread);
    CloseHandle(PI.hProcess);
  end;

 //zzzzzzzzzzzzzzz Application.ProcessMessages;

end;





function ReplaceInvalidFileNameChars(const aFileName: string; const  aReplaceWith: Char = '_'): string;
var
  i: integer;
begin
  Result := aFileName;
  for i := 1 to Length(Result) do
    if (Result[i] in ['/',':','\','<','>']) then
      Result[i] := aReplaceWith;
  end;
end.



function GetSpecialFolderPath(CSIDLFolder: Integer): string;
var
   FilePath: array [0..MAX_PATH] of char;
begin
  SHGetFolderPath(0, CSIDLFolder, 0, 0, FilePath);
  Result := FilePath;

  //http://delphiprogrammingdiary.blogspot.com/2017/08/how-to-get-windows-special-directories.html

//  CSIDL_DESKTOP                   = $0000; { <desktop> }
//  CSIDL_INTERNET                  = $0001; { Internet Explorer (icon on desktop) }
//  CSIDL_PROGRAMS                = $0002; { Start Menu\Programs }
//  CSIDL_CONTROLS                 = $0003; { My Computer\Control Panel }
//  CSIDL_PRINTERS                  = $0004; { My Computer\Printers }
//  CSIDL_PERSONAL                 = $0005; { My Documents.  This is equivalent to
//  CSIDL_MYDOCUMENTS           = $000c; { logical "My Documents" desktop icon }
end;


function GetSpecialFolderPath_MyDocuments: string;
begin
  Result:=GetSpecialFolderPath(CSIDL_MYDOCUMENTS);
end;
      



{
// to get DESKTOP folder location //
procedure TForm1.Button2Click(Sender: TObject);
begin
  ShowMessage(GetSpecialFolderPath(CSIDL_DESKTOP, False));
end;
}

end.





{



//test if a "fileName" is a valid Windows file name
//Dellphi
function IsValidFileName(const fileName : string) : boolean;
const
   InvalidCharacters : set of char = ['\', '/', ':', '*', '?', '"', '', '|'];
var
   cnt : integer;
begin
   result := fileName  '';

   if result then
   begin
     for cnt := 1 to Length(fileName) do
     begin
       result := NOT (fileName[cnt] in InvalidCharacters) ;
       if NOT result then break;
     end;
   end;
end; (* IsValidFileName *)

// for all platforms (Windows\Unix), uses IOUtils.
function ReplaceInvalidFileNameChars(const aFileName: string; const aReplaceWith: Char = '_'): string;
var
  i: integer;
begin
  Result := aFileName;
  for i := Low(Result) to High(Result) do
    if not TPath.IsValidFileNameChar(Result[i]) then
      Result[i] := aReplaceWith;
  end;
end.



function Dlg_SelectDirectory(const aDirName: string): Boolean;
//FileCtrl.pas
var
  sDir: String;
begin
//  Result:=SelectDirectory('Your caption','',sDir);

  //function SelectDirectory(const Caption: string; const Root: WideString;
//  var Directory: string): Boolean; overload;

end;
