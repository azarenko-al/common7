unit u_Mapx_new;

interface
uses
  Variants, adodb, MapXLib_TLB,

  u_MapX
  ;

function mapx_CreateLayerFromDb(aMap: TMap; aDataSet: TCustomAdoDataSet; aFileName:
    string; aIDFld: string = 'ID'; aLatFld: string = 'Lat'; aLonFld: string = 'Lon'):
    CMapxLayer;

implementation


//------------------------------------------------------------------------------
function mapx_CreateLayerFromDb(aMap: TMap; aDataSet: TCustomAdoDataSet; aFileName:
    string; aIDFld: string = 'ID'; aLatFld: string = 'Lat'; aLonFld: string = 'Lon'):
    CMapxLayer;
//------------------------------------------------------------------------------
var
  sTempFileName: string;
  vBindLayerObject: CMapXBindLayer;
  vDataSet: CMapxDataset;
  VLayer: CMapXLayer;
  vFlds: CMapXFields;
  vLayerInfoObject: CMapXLayerInfo;
  iIndex, i: Integer;
  s: string;
  oMap: TMap;
begin
  oMap:=TMap.Create(nil);

  sTempFileName:= 'K:\1.tab';

  aFileName:= 'K:\2.tab';

  Assert(aFileName<>'');

(*
  if Assigned(MapLayer(sTempFileName)) then
    Map1.Layers.remove( MapLayer(sTempFileName) );
  mapx_deleteFile(sTempFileName);

  if Assigned(MapLayer(aFileName)) then
    Map1.Layers.remove( MapLayer(aFileName) );
  mapx_deleteFile(aFileName);*)

  vBindLayerObject := CoBindLayer.Create;
  vBindLayerObject.LayerName := 'Temp_bind_layer';
  vBindLayerObject.RefColumn1 := aLonFld;
  vBindLayerObject.RefColumn2 := aLatFld;
  vBindLayerObject.LayerType := miBindLayerTypeXY;
  vBindLayerObject.FileSpec := sTempFileName;
//  vBindLayerObject.OverwriteFile := true;


  vDataSet := aMap.DataSets.Add(miDatasetADO, aDataSet.Recordset, EmptyParam,
               aIDFld, EmptyParam, vBindLayerObject, EmptyParam, EmptyParam);



  vFlds:= vDataSet.Fields;

  i:=vFlds.Count;

//  s:=vFlds.Item[1].Name;

  vLayer :=mapx_GetLayerByFileName(aMap, sTempFileName);

  vLayer.BeginAccess(miAccessRead);

  vLayerInfoObject := CoLayerInfo.Create;
  vLayerInfoObject.Type_ := miLayerInfoTypeNewTable;
  vLayerInfoObject.AddParameter('FileSpec', aFileName);
  vLayerInfoObject.AddParameter('Fields', vFlds);
  vLayerInfoObject.AddParameter('Features', vLayer.AllFeatures);
  vLayerInfoObject.AddParameter('AutoCreateDataset', 1);
  vLayerInfoObject.AddParameter('OverwriteFile', 1);
  Result:= aMap.Layers.Add(vLayerInfoObject, EmptyParam);

  vLayer.EndAccess(miAccessEnd);


  oMap.Free;


(*
  vLayer:= MapLayer(sTempFileName);
//  iIndex:= LayerIndexByName(sTempFileName);
//  vLayer:= Map1.Layers.Item[iIndex];
  vFlds:= vDataSet.Fields;

  vLayer.BeginAccess(miAccessRead);

  vLayerInfoObject := CoLayerInfo.Create;
  vLayerInfoObject.Type_ := miLayerInfoTypeNewTable;
  vLayerInfoObject.AddParameter('FileSpec', aFileName);
  vLayerInfoObject.AddParameter('Fields', vFlds);
  vLayerInfoObject.AddParameter('Features', vLayer.AllFeatures);
  vLayerInfoObject.AddParameter('AutoCreateDataset', 1);
  vLayerInfoObject.AddParameter('OverwriteFile', 1);
  Result:= Map1.Layers.Add(vLayerInfoObject, 1);

  vLayer.EndAccess(miAccessEnd);

  iIndex:= MapLayerIndex(sTempFileName); // LayerIndexByName('Temp_bind_layer');
  if iIndex>0 then
    Map1.Layers.Remove(iIndex);

  if aDeleteLayerFromMap then begin
    iIndex:= MapLayerIndex(aFileName);
    if iIndex>0 then
      Map1.Layers.Remove(iIndex);

    Result:= nil;
  end;
*)


end;



end.
 