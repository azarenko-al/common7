unit u_MapX_func;

interface
uses SysUtils, Variants, Dialogs,

    MapXLib_TLB
    ;


type
  TMapViewAddRec = record
    LayerName    : string;
    FileName     : string;
//    ObjectName   : string;

    AutoLabel    : boolean;

    IsZoomAllowed: boolean;
    ZoomMin      : integer;
    ZoomMax      : integer;

//    CalcRegionID : integer;

    IsSelectable : boolean;
    IsTransparent: boolean;
    IsEditable   : boolean;

 //   IsFiltered   : boolean;

  //  LabelProperties_RegPath: string;

  //  DisplayIndex : Integer;
    DisplayIndex_from_1 : Integer;

    MapAlignment: (//malNone,
                   malTop, malBottom);//, malMiddle); /malAuto,

  end;


  function map_MapAdd(aMap: TMap; aRec: TMapViewAddRec): CMapXLayer; overload;
  function map_MapAdd(aMap: TMap; aFileName: string): CMapXLayer; overload;

  procedure mapx_Dlg_Ver(aMap: TMap);

  procedure mapx_SetLayerAutoLabel(aLayer: CMapXLayer; aValue: boolean);

implementation


//--------------------------------------------------------------------
function map_MapAdd(aMap: TMap; aRec: TMapViewAddRec): CMapXLayer;
//--------------------------------------------------------------------
var
  vLayer: CMapXLayer;
  sRegPath: string;
    i: Integer;
begin
  Result := nil;

  aRec.FileName:= ChangeFileExt(aRec.FileName, '.tab');

  if not FileExists (aRec.FileName) then
  begin
  //  Log('file not found: '+ aRec.FileName);
    exit;
  end;

//    vLayer:=map1.OleObject.layers.add (aFileName);
  try
    case aRec.MapAlignment of
  //    malAuto:    vLayer:= aMap.Layers.Add (aRec.FileName, EmptyParam);
      malTop:     vLayer:= aMap.Layers.Add (aRec.FileName, 1); //0
//      malMiddle:  vLayer:= aMap.Layers.Add (aRec.FileName, 500);
    //  malBottom:  vLayer:= aMap.Layers.Add (aRec.FileName, aMap.Layers.Count); //1000
      malBottom:  vLayer:= aMap.Layers.Add (aRec.FileName, EmptyParam); //1000
    else
      vLayer:= aMap.Layers.Add (aRec.FileName, 1); //1000
    end;

  except
    ShowMessage('file not found: '+ aRec.FileName);
    Exit;
  end;

  if aRec.LayerName<>'' then
    vLayer.Name:=aRec.LayerName;

  if aRec.ZoomMax>0 then
  begin
    vLayer.ZoomLayer:=aRec.IsZoomAllowed;
    vLayer.ZoomMin:=aRec.ZoomMin;
    vLayer.ZoomMax:=aRec.ZoomMax;
  end;


  Assert(Assigned(vLayer), 'vLayer not assigned');


  case vLayer.Type_ of
    miLayerTypeRaster: begin
      vLayer.ZoomLayer:= False;
    end;

    miLayerTypeNormal: begin
      vLayer.AutoLabel := aRec.AutoLabel;
      vLayer.Selectable:= aRec.IsSelectable;
      vLayer.Editable  := aRec.IsEditable;


{      if aRec.ObjectName<>'' then
        sRegPath:=REGISTRY_MapObjectsLabels +  aRec.ObjectName
      else
        sRegPath:=REGISTRY_MapObjectsLabels + ExtractFileName(aRec.FileName);
}

  //    Assert(aRec.LabelProperties_RegPath<>'');

(*      if aRec.LabelProperties_RegPath<>'' then
        mapx_LabelProperties_LoadFromReg (vLayer.LabelProperties, aRec.LabelProperties_RegPath);
*)

//      dmMap_Desktop.ApplyLabelProperties (vLayer, aRec.FileName);

//      vLayer.Refresh;
    end;
  end;

  Result := vLayer;
end;


procedure mapx_SetLayerAutoLabel(aLayer: CMapXLayer; aValue: boolean);
begin
  if aLayer.Type_ in [miLayerTypeNormal] then
    aLayer.AutoLabel := aValue;

//  aLayer.AutoLabel:=aValue;
end;


procedure mapx_Dlg_Ver(aMap: TMap);
begin
  ShowMessage(Format('������ MapX: %s', [aMap.Version]));
end;


function map_MapAdd(aMap: TMap; aFileName: string): CMapXLayer;
var
  rec: TMapViewAddRec;
begin
  FillChar(rec, SizeOf(rec), 0);

  rec.FileName:=aFileName;
  rec.MapAlignment :=malTop;
//  rec.AutoLabel:=map_rec.AutoLabel;
//   rec.LabelProperties_RegPath:=REGISTRY_MapObjectsLabels + ExtractFileName(rec.FileName);

  Result := map_MapAdd(aMap, rec);
end;


end.
