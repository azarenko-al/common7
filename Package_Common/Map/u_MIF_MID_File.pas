unit u_MIF_MID_File;

interface
uses SysUtils, Classes, Graphics, ComObj,

     //u_Geo_types,

     u_geo,
     u_geo_classes,
     u_MapX,

     u_files,

  //   u_error_log,
     u_func,
 //    u_func_files,
     u_dlg,
//     u_func_img,

     variants;
     //u_FileFormats;

type
  //TMapInfoRegion = class; //����������� ��������� � �����

  //---------------------------------------------------------
  TMIF_MID_File = class
  //---------------------------------------------------------
  private
    FMifFile: TextFile;
    FMidFile: TextFile;

    FMifFileName: string;
    FMidFileName: string;


//    FDataStr    : string;

    FPen   : record width, pattern, color: integer; end;
    FBrush : record pattern, forecolor, backcolor: integer; end;

//    procedure LoadHeaderFromFile (aFileName: string);
    procedure WritePen(aMIStyle: TmiStyleRec);
    procedure WriteBrush(aMIStyle: TmiStyleRec);
///    procedure WriteDataStr(); //  (Value: string);
//    procedure WriteHeader();

  private

  public


 //   TempRegion : TMapInfoRegion;
{
    Header : record
      Version   : string;
      CharSet   : string;
      Delimiter : string;
      FCoordSys  : string;
    end;
}
    constructor Create;
 //   destructor Destroy; override;

    function CreateFile_Pulkovo(aFileName: string; aFields: array of TmiFieldRec): boolean;
        overload;

    function  CreateFile(aFileName: string): boolean; overload;
    procedure Close();

    procedure WritePolyDef(var aBLPoints: TBLPointArray; aMIStyle: TmiStyleRec;
        aValue: string);

    procedure WritePoly(aBLPoints: TBLPointArray; aMIStyle: TmiStyleRec;
        aMiParamArray: array of TmiParam); overload;

    procedure WritePoly(aBLPointList: TBLPointList; aMIStyle: TmiStyleRec;
        aMiParamArray: array of TmiParam); overload;

    procedure WriteRegion(aBLPoints: array of TBLPointArray; aMIStyle: TmiStyleRec;
        aMiParams: array of TmiParam);

    procedure WriteFieldValues(aParams: array of TmiParam);



//    procedure SetDataStr  (Value: string);


 //   procedure WritePolyLine (var aBLPoints: TBLPointArrayF);

//    procedure WritePen1(aWidth, aPattern, aColor: integer);
//    procedure WriteBrush1(aPattern, aForeColor, aBackcolor: integer);


    procedure WriteSymbol(aPoint: TBLPoint; aMIStyle: TmiStyleRec; aMiParamArray:
        array of TmiParam);

{    procedure WriteLine     (b1,l1, b2,l2: double);
    procedure WriteRect     (b1,l1, b2,l2: double);
    procedure WriteText     (b1,l1: double; aCaption,aAlign: string; aBackColor: integer);
}
//    procedure WriteRegion (aRegion: TMapInfoRegion);
//    procedure SetPen      (width, pattern, color: integer);
//    procedure SetBrush    (pattern, forecolor, backcolor: integer);

//    procedure WritePoly (var aBLPoints: TBLPointArrayF; aMIStyle: TmiStyle);

 //   function LoadFromFile   (aFileName: string): boolean; override;

//    property RowCount; //: integer read GetRowCount;
//    property Cells; // [aRow: integer; aColumnName: string]: Variant read GetCellValue;

    function ExportToMapinfoTab: boolean;

  end;

{
  TMapInfoPolygon = class
    Points: TBLPointArrayF;
  end;
}


function mapx_MapInfoInstalled(): boolean;
function mapx_Export_MIFMID_to_TAB(aMifFileName, aTabFileName: string): boolean;


//=====================================================
implementation
//=====================================================
            
{const
  //������� ���������
  MIF_CoordSys_KRASOVSKY = 'Earth Projection 1, 1001';

  MIF_SYMBOL_STAR = 58;

  MIF_BRUSH_PATTERN_BLANK = 1;
  MIF_BRUSH_PATTERN_SOLID = 2;
  MIF_BRUSH_PATTERN_DOTS  = 16;

  // pen pattern styles
  MIF_PEN_PATTERN_BLANK   = 1;
  MIF_PEN_PATTERN_SOLId   = 2;
  MIF_PEN_PATTERN_DASH    = 8;
  MIF_PEN_PATTERN_ARROW   = 59;
}

{  // MapInfo records ----------------------------------
  MIF_HEADER = 'Version 300'               + CRLF +
               'Charset "WindowsCyrillic"' + CRLF +
               'Delimiter ":Delimiter"'    + CRLF +
               'CoordSys   :CoordSys';

}

  function MakeMapInfoColor (aColor: integer): integer; forward;





//------------------------------------------------
constructor TMIF_MID_File.Create;
//------------------------------------------------
begin
  inherited;
//  Header.FCoordSys:=MIF_CoordSys_KRASOVSKY;
 // Header.Delimiter:=';';

//  TempRegion:=TMapInfoRegion.Create(0);

end;


//------------------------------------------------
function TMIF_MID_File.CreateFile_Pulkovo(aFileName: string; aFields: array of
    TmiFieldRec): boolean;
//------------------------------------------------
var
  i, iColumnCount: Integer;
  sFlds: string;
begin
  ForceDirByFileName (aFileName);

  FMifFileName:= ChangeFileExt(aFileName,'.mif');
  FMidFileName:= ChangeFileExt(aFileName,'.mid');

  AssignFile (FMifFile, FMifFileName);
  AssignFile (FMidFile, FMidFileName);

  ReWrite (FMifFile);
  ReWrite (FMidFile);

  iColumnCount:= High(aFields)+1;

//  CoordSys Earth Projection 1, 9999, 3, 23.57, -140.95, -79.8, 0, -0.35, -0.79, -0.22, 0



  Writeln (FMifFile,  'Version 300               '+ CRLF +
                      'Charset "WindowsCyrillic" '+ CRLF +
                      'Delimiter ","             '+ CRLF +
                      'CoordSys Earth Projection 1, 9999, 3, 23.57, -140.95, -79.8, 0, -0.35, -0.79, -0.22, 0 '+ CRLF +
//                      'CoordSys Earth Projection 1, 1001 '+ CRLF +
                      Format('Columns %d', [iColumnCount])     + CRLF
                     );

  sFlds:= '';
  for i:=0 to High(aFields) do
    case aFields[i].Type_ of
      miTypeString : begin
        if aFields[i].Size=0 then
          aFields[i].Size:=200;
        sFlds:= sFlds+ Format('   %s %s(%d) '+ CRLF, [aFields[i].Name, 'Char', aFields[i].Size]);
      end;
      miTypeFloat    : sFlds:= sFlds+ Format('   %s %s '+ CRLF, [aFields[i].Name, 'Float']);
      miTypeInt      ,
      miTypeSmallInt : sFlds:= sFlds+ Format('   %s %s '+ CRLF, [aFields[i].Name, 'Integer']);
    end;


  Writeln (FMifFile,  sFlds);
  Writeln (FMifFile,  'Data                      ');

  Result:=true;
end;


//------------------------------------------------
function TMIF_MID_File.CreateFile(aFileName: string): boolean;
//------------------------------------------------
begin
  ForceDirByFileName (aFileName);

  FMifFileName:= ChangeFileExt(aFileName,'.mif');
  FMidFileName:= ChangeFileExt(aFileName,'.mid');

  AssignFile (FMifFile, FMifFileName);
  AssignFile (FMidFile, FMidFileName);

  ReWrite (FMifFile);
  ReWrite (FMidFile);


  Writeln (FMifFile,  'Version 300               '+ CRLF +
                      'Charset "WindowsCyrillic" '+ CRLF +
                      'Delimiter ","             '+ CRLF +
                      'CoordSys Earth Projection 1, 1001 '+ CRLF + //Bounds (25.6847015111, 58.1118865517) (35.7471482404, 61.4175930606) '+ CRLF +
                      'Columns 1                 '+ CRLF +
                      'Value Char(100)           '+ CRLF +
                      'Data                      '
                     );

/////////////  WriteHeader();
  Result:=true;
end;


procedure TMIF_MID_File.Close();
begin
//  try

  CloseFile(FMifFile);
  CloseFile(FMidFile);

 // except
  // end;
end;

(*
procedure TMIF_MID_File.SetDataStr (Value: string);
begin
  FDataStr:=Value;
end;
*)

{
procedure TMIF_MID_File.SetPen (width, pattern, color: integer);
begin
  FPen.width:=width;  FPen.pattern:=pattern;  FPen.color:=color;
end;

procedure TMIF_MID_File.SetBrush (pattern, forecolor, backcolor: integer);
begin
  FBrush.pattern:=pattern;  FBrush.forecolor:=forecolor;  FBrush.backcolor:=backcolor;
end;
}


procedure TMIF_MID_File.WritePen(aMIStyle: TmiStyleRec);
begin
 // aMIStyle.RegionBorderStyle

  with aMIStyle do
    Writeln (FMifFile, Format('    Pen (%d,%d,%d)',
      [RegionBorderWidth,
       RegionBorderStyle,
       MakeMapinfoColor(RegionBorderColor)]));
end;


procedure TMIF_MID_File.WriteBrush(aMIStyle: TmiStyleRec);
begin
  with aMIStyle do

   // if not aMIStyle.RegionBackEnabled then
//      Writeln (FMifFile, Format('    Brush (%d,%d)',
  //      [RegionPattern, MakeMapinfoColor(RegionBackgroundColor)]))
//    else



      Writeln (FMifFile, Format('    Brush (%d,%d,%d)',
//        [RegionPattern, MakeMapinfoColor(RegionColor),
  //      MakeMapinfoColor(RegionBackColor)]))
         [RegionPattern,
          MakeMapinfoColor(RegionBackgroundColor),
          MakeMapinfoColor(RegionBackgroundColor)
          ]))
end;

//-------------------------------------------------------------------
procedure TMIF_MID_File.WriteRegion(aBLPoints: array of TBLPointArray;
    aMIStyle: TmiStyleRec; aMiParams: array of TmiParam);
//-------------------------------------------------------------------
var i,j,k: integer;
  sL, sB, sDataStr: string;
begin
  Assert(Length(aBLPoints)>0);

  Writeln (FMifFile, Format('Region %d', [Length(aBLPoints)] ));

  for k := 0 to High(aBLPoints) do
  begin
    Assert(Length(aBLPoints[k])>0);

    Writeln (FMifFile, Format('   %d', [Length(aBLPoints[k])] ));

    for j:=0 to High(aBLPoints[k]) do
    begin
      sL:= ReplaceStr(AsString(TruncFloat(aBLPoints[k][j].l,6)), ',', '.');
      sB:= ReplaceStr(AsString(TruncFloat(aBLPoints[k][j].b,6)), ',', '.');

      Writeln (FMifFile, sL+' '+sB); // Format('%1.6f %1.6f', [aBLPoints[j].l, aBLPoints[j].b]));
//      Writeln (FMifFile, Format('%1.6f %1.6f', [aBLPoints[k][j].l, aBLPoints[k][j].b]));
    end;
  end;

  WritePen(aMIStyle);
  WriteBrush(aMIStyle);

  WriteFieldValues(aMiParams);
end;


//-------------------------------------------------------------------
procedure TMIF_MID_File.WriteSymbol(aPoint: TBLPoint; aMIStyle: TmiStyleRec;
    aMiParamArray: array of TmiParam);
//-------------------------------------------------------------------
var
  S: string;
begin
  S:= Format('Point %1.6f %1.6f' + CRLF + '    Symbol (%d,%d,%d,"%s",0,0)',
     [aPoint.l, aPoint.b,
      aMIStyle.Character,
      MakeMapinfoColor(aMIStyle.FontColor),
      aMIStyle.FontSize,
      aMIStyle.FontName ]);

  Writeln (FMifFile, S);

  WriteFieldValues(aMiParamArray);
end;


//-------------------------------------------------------------------
procedure TMIF_MID_File.WriteFieldValues(aParams: array of TmiParam);
//-------------------------------------------------------------------
var
  i: Integer;
  sDataStr: string;
begin
  sDataStr:= '';

  for i := 0 to High(aParams) do
  begin
    case VarType(aParams[i].Value) of
      varString: sDataStr:= sDataStr+'"'+aParams[i].Value+'"';
      else
        sDataStr:= sDataStr+AsString(aParams[i].Value);
    end;

    if i<>High(aParams) then
      sDataStr:= sDataStr+',';
  end;

 // str_DeleteLastChar(sDataStr, ',');

  Writeln (FMidFile, sDataStr);
end;


//-------------------------------------------------------------------
procedure TMIF_MID_File.WritePoly(aBLPoints: TBLPointArray; aMIStyle:
    TmiStyleRec; aMiParamArray: array of TmiParam);
//-------------------------------------------------------------------
var i,j: integer;
  S: string;
  sL, sB: string;
begin
  Writeln (FMifFile, Format('Region %d', [1]));
  Writeln (FMifFile, Format('   %d', [Length(aBLPoints)]));

  for j:=0 to Length(aBLPoints)-1 do begin
    sL:= ReplaceStr(AsString(TruncFloat(aBLPoints[j].l,6)), ',', '.');
    sB:= ReplaceStr(AsString(TruncFloat(aBLPoints[j].b,6)), ',', '.');


    S:= Format('%1.6f %1.6f', [aBLPoints[j].l, aBLPoints[j].b]);



    Writeln (FMifFile, sL+' '+sB); // Format('%1.6f %1.6f', [aBLPoints[j].l, aBLPoints[j].b]));
  end;

  WritePen(aMIStyle);
  WriteBrush(aMIStyle);

  WriteFieldValues(aMiParamArray);
end;

//-------------------------------------------------------------------
procedure TMIF_MID_File.WritePoly(aBLPointList: TBLPointList; aMIStyle:
    TmiStyleRec; aMiParamArray: array of TmiParam);
//-------------------------------------------------------------------
var i,j: integer;
  S: string;
  sL, sB: string;
begin
  Writeln (FMifFile, Format('Region %d', [1]));
  Writeln (FMifFile, Format('   %d', [aBLPointList.Count]));

  for j:=0 to aBLPointList.Count - 1 do begin
    sL:= ReplaceStr(AsString(TruncFloat(aBLPointList[j].BLPoint.L,6)), ',', '.');
    sB:= ReplaceStr(AsString(TruncFloat(aBLPointList[j].BLPoint.b,6)), ',', '.');

    S:= Format('%1.6f %1.6f', [aBLPointList[j].BLPoint.l, aBLPointList[j].BLPoint.b]);


    Writeln (FMifFile, sL+' '+sB);
  end;

  WritePen(aMIStyle);
  WriteBrush(aMIStyle);

  WriteFieldValues(aMiParamArray);
end;


//-------------------------------------------------------------------
procedure TMIF_MID_File.WritePolyDef(var aBLPoints: TBLPointArray; aMIStyle:
    TmiStyleRec; aValue: string);
//-------------------------------------------------------------------
var i,j: integer;
  sL, sB: string;
begin
  Writeln (FMifFile, Format('Region %d', [1]));
 // for i:=0 to aRegion.PolygonCount-1 do
 //  with aRegion.Polygons[i] do begin
  Writeln (FMifFile, Format('   %d', [Length(aBLPoints)]));

  for j:=0 to Length(aBLPoints)-1 do begin
    sL:= ReplaceStr(AsString(TruncFloat(aBLPoints[j].l,6)), ',', '.');
    sB:= ReplaceStr(AsString(TruncFloat(aBLPoints[j].b,6)), ',', '.');

    Writeln (FMifFile, sL+ ' '+ sB); // Format('%1.6f %1.6f', [aBLPoints[j].l, aBLPoints[j].b]));
  end;
  // end;

  WritePen(aMIStyle);
  WriteBrush(aMIStyle);

  Writeln (FMidFile, aValue);

 //// WriteDataStr();
end;

//
//destructor TMIF_MID_File.Destroy;
//begin
////  Close;
//  inherited;
//end;


//--------------------------------------------------------
procedure ColorTo_R_G_B(Value: TColor; var R,G,B: byte);
//--------------------------------------------------------
var cl: packed record case b:byte of
          0: (int: TColor);   1: (RGB: array[0..4] of byte);
        end;
begin
  cl.int:=Value;
  R:=cl.rgb[0];  G:=cl.rgb[1];  B:=cl.rgb[2];
end;



function MakeMapinfoColor (aColor: integer): integer;
var r,g,b: byte;
begin
  ColorTo_R_G_B (aColor, r,g,b);
  Result:=(r*65536) + (g * 256) + b;
end;



//-------------------------------------------------------------------
function mapx_MapInfoInstalled(): boolean;
//-------------------------------------------------------------------
var
  vApp: Variant;
  S: string;
begin
  try
    vApp:= CreateOLEObject('MapInfo.Application');
    Result:= true;
  except
    Result:= false;
  end;
end;

//-------------------------------------------------------------------
function TMIF_MID_File.ExportToMapinfoTab: boolean;
//-------------------------------------------------------------------
var
  sTab: string;
begin

  sTab:= ChangeFileExt(FMifFileName, '.tab');

 // Close;

  Result:= mapx_Export_MIFMID_to_TAB(FMifFileName, sTab);

  if Result then begin
    SysUtils.DeleteFile(FMifFileName);
    SysUtils.DeleteFile(FMidFileName);
  end;

end;


//-------------------------------------------------------------------
function mapx_Export_MIFMID_to_TAB(aMifFileName, aTabFileName: string): boolean;
//-------------------------------------------------------------------
const
  ERR_CREATE = '������ �������� OLE ������� MapInfo.Application. ����������� �� ��������. ����� %s';
  ERR_EXEC = '������ ���������� ������� %s. ����� %s';
var
  vApp: Variant;

  s: string;
 // sWorFileName: string;
 // vApp: Variant;
begin
  Result:= false;

  mapx_DeleteFile (aTabFileName);

  try
//���� ���� ���������� ��������� - ������������ � ����
    vApp:= CreateOLEObject('MapInfo.Application');
//    vApp := GetActiveOleObject('MapInfo.Application');
  except
    on e: Exception do begin
//      custom_error_log.AddError (Format(ERR_CREATE, [e.Message]));
//      MsgDlg(Format(ERR_CREATE, [e.Message]));
      exit;
    end;
//���� ��� - ������� �����
//    vApp:=CreateOLEObject('MapInfo.Application');
  end;

  if VarIsNull(vApp) then
    Exit;

  s:= Format('Import "%s" Type "MIF"   Into "%s" Overwrite', [aMifFileName, aTabFileName]);

  try
    vApp.RunCommand(s);
  except
    on e: Exception do begin
//      custom_error_log.AddError (Format(ERR_EXEC, [s, e.Message]));
      MsgDlg(Format(ERR_EXEC, [s, e.Message]));
      exit;
    end;
  end;

  Result:= FileExists(aTabFileName);
end;

{
begin
  Application.Initialize;
  mapx_Export_MIFMID_to_TAB ('d:\a\relief.MIF','d:\a\relief1.tab');
}

{$define test1}

{$IFDEF test}


var
  o: TMIF_MID_File;

begin

  o:=TMIF_MID_File.Create;

//  o.LoadFromFile ('t:\!\exp\������ ���.mif');
//  o.MemData.SaveToTextFile ('t:\!\exp\aassssssa.txt');

{$ENDIF}


end.



(*

//-----------------------------------------------------
function TMIF_MID_File.LoadFromFile (aFileName: string): boolean; // MIF
//-----------------------------------------------------
begin
{  Result:=False;

  if not FileExists (aFileName) then
    Exit;

  LoadHeaderFromFile (aFileName);
//  LoadDataFile (ChangeFileExt(aFileName,'.mid'));
  Result:=True;
}
end;



//-----------------------------------------------------
procedure TMIF_MID_File.LoadHeaderFromFile (aFileName: string); // MIF
//-----------------------------------------------------
var list: TStringList;
    i,j: integer;
    strArr: TStrArray;
    colType: TGridColumnType;
    colName: string;
    iColCount: integer;
begin
//  MemData.Close;
//  db_ClearFields (MemData);
{
  if not FileExists (aFileName) then
    Exit;

  list:=TStringList.Create;
  try
    list.LoadFromFile (aFileName);

    // parse header ------------------------
    i:=-1;
    while i < list.Count-1 do
    begin
      Inc(i);
      strArr:=StringToStrArray (list[i],' ');

      if High(strArr)>=1 then
      begin
        if Eq(strArr[0],'Version')   then Header.Version:=strArr[1] else
        if Eq(strArr[0],'Charset')   then Header.Charset:=strArr[1] else
        if Eq(strArr[0],'Delimiter') then Header.Delimiter:=ReplaceStr(strArr[1],'"','') else
        if Eq(strArr[0],'CoordSys')  then Header.FCoordSys:=strArr[1] else

        if Eq(strArr[0],'Columns')   then
        begin
          //SetLength(Columns, AsInteger(strArr[1]));
          iColCount:=AsInteger(strArr[1]);

          for j:=0 to iColCount-1 do
          begin
             Inc(i);
             strArr:=StringToStrArray (list[i],' ');

             colName:=strArr[0];

             if Eq(strArr[1],'Float')    then colType:=ctFloat else
             if Eq(strArr[1],'Integer')  then colType:=ctInteger else
             if Eq(strArr[1],'Smallint') then colType:=ctSmallint
                                         else colType:=ctString;
             Columns.AddItem (colName,colType);

             if Eq(strArr[1],'Float')    then db_CreateField (MemData, colName, ftFloat) else
             if Eq(strArr[1],'Integer')  then db_CreateField (MemData, colName, ftInteger) else
             if Eq(strArr[1],'Smallint') then db_CreateField (MemData, colName, ftSmallint)
                                         else db_CreateField (MemData, colName, ftString);

          end;
        end;

        if Eq(strArr[0],'Data') then
          Break;

      end else
        Break;
    end;

    Delimiter:=Header.Delimiter;
  finally
    list.Free;
  end;

  MemData.CreateFile;}


end;



{//------------------------------------------------
procedure TMIF_MID_File.WriteHeader();
//------------------------------------------------
var i:integer; str:string;
begin}
{

  str:=MIF_HEADER;
  str:=ReplaceStr (str, ':Delimiter', Header.Delimiter);
  str:=ReplaceStr (str, ':CoordSys',  Header.FCoordSys);

  Writeln (FMifFile, str);
  Writeln (FMifFile, Format('Columns %d',[Columns.Count]));

  for i:=0 to Columns.Count-1 do
  begin
    case Columns[i].Type_ of
      ctString  : str:='Char(50)';
      ctFloat   : str:='Float';
      ctInteger : str:='Smallint';
      ctSmallint: str:='Smallint';
      ctDecimal : str:=Format('Decimal (%d,0)', [Columns[i].Precision]);
    end;

    Writeln (FMifFile, Format('  %s %s', [Columns[i].Name,str]));
  end;

  Writeln (FMifFile, 'Data');

}
//end;





{

procedure TMIF_MID_File.WriteLine (b1,l1, b2,l2: double);
begin
  Writeln (FMifFile, Format('Line %1.6f %1.6f %1.6f %1.6f', [l1,b1, l2,b2]));
  WritePen();
  WriteDataStr();
end;



procedure TMIF_MID_File.WriteRect (b1,l1, b2,l2: double);
begin
  Writeln (FMifFile, Format('Rect %1.2f %1.2f %1.2f %1.2f', [l1,b1, l2,b2]));
  WritePen();
  WriteDataStr();
end;


procedure TMIF_MID_File.WriteText (b1,l1: double; aCaption,aAlign: string; aBackColor: integer );
begin
  Writeln (FMifFile, Format('Text', []) );
  Writeln (FMifFile, Format('    "%s"', [aCaption]));
  Writeln (FMifFile, Format('    %1.6f %1.6f %1.6f %1.6f', [l1,b1,l1+0.01,b1+0.001]));
  Writeln (FMifFile, Format('    Justify %s',  [aAlign]));
  Writeln (FMifFile, Format('    Font ("Arial",0,10,0,%d)',[MakeMapinfoColor(aBackColor)]));

//  WritePen();
  WriteDataStr();

end;
}



{
procedure TMIF_MID_File.WritePen1(aWidth, aPattern, aColor: integer);
begin
  with FPen do
    Writeln (FMifFile, Format('    Pen (%d,%d,%d)', [awidth, apattern, MakeMapinfoColor(acolor)]));
end;

procedure TMIF_MID_File.WriteBrush1(aPattern, aForeColor, aBackcolor: integer);
begin
//  with FBrush do
 // if pattern>1
  ///  then Writeln (FMifFile, Format('    Brush (%d,%d)', [pattern, MakeMapinfoColor(forecolor), MakeMapinfoColor(backcolor)]))
    //else

    Writeln (FMifFile, Format('    Brush (%d,%d,%d)', [apattern, MakeMapinfoColor(aforecolor), MakeMapinfoColor(abackcolor)]))

end;
}
{
procedure TMIF_MID_File.WritePoly (var aBLPoints: TBLPointArrayF);
begin

  Writeln (FMifFile, Format('Region %d', [1]));
  //for i:=0 to aBLPoints. PolygonCount-1 do
//   with aRegion.Polygons[i] do begin
  Writeln (FMifFile, Format('   %d', [aBLPoints.Count]));

  for j:=0 to aBLPoints.Count-1 do
    Writeln (FMifFile, Format('%1.6f %1.6f',
                               [aBLPoints.Items[j].L, aBLPoints.Items[j].B]));
 //  end;

   WritePen();
   WriteBrush();
   WriteDataStr();

end;
}



{
procedure TMIF_MID_File.WritePolyLine(var aBLPoints: TBLPointArrayF);
var i,iCount: integer;
  str: string;
begin
  Writeln (FMifFile, 'PLINE');
  iCount:=aBLPoints.Count;
  Writeln (FMifFile, Format('  %d', [iCount]));
  for i:=0 to iCount-1 do begin
    str:=Format('%1.6f %1.6f', [aBLPoints.Items[i].B, aBLPoints.Items[i].L]);
    Writeln (FMifFile, Format('%1.6f %1.6f', [aBLPoints.Items[i].L, aBLPoints.Items[i].B]));
  end;
  WriteDataStr();
end;
}





(*
//------------------------------------------------
procedure TMIF_MID_File.WriteDataStr(); // (Value: string);
//------------------------------------------------
begin
//////  Writeln (FMidFile, FDataStr);
end;
*)
