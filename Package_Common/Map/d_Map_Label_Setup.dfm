inherited dlg_Map_Label_Setup: Tdlg_Map_Label_Setup
  Left = 1142
  Top = 362
  VertScrollBar.Range = 0
  BorderStyle = bsDialog
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1087#1086#1076#1087#1080#1089#1077#1081
  ClientHeight = 443
  ClientWidth = 634
  OldCreateOrder = True
  Position = poDefault
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 408
    Width = 634
    inherited Bevel1: TBevel
      Width = 634
    end
    inherited Panel3: TPanel
      Left = 448
      Width = 186
      inherited btn_Ok: TButton
        Left = 7
      end
      inherited btn_Cancel: TButton
        Left = 90
      end
    end
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 145
      Height = 23
      Action = act_Default
      TabOrder = 1
    end
  end
  inherited pn_Top_: TPanel
    Width = 634
    inherited Bevel2: TBevel
      Width = 634
    end
    inherited pn_Header: TPanel
      Width = 634
    end
  end
  object pn_Main: TPanel [2]
    Left = 0
    Top = 60
    Width = 421
    Height = 348
    Align = alLeft
    BevelOuter = bvNone
    BorderWidth = 5
    Caption = 'pn_Main'
    TabOrder = 2
    object cxVerticalGrid1: TcxVerticalGrid
      Left = 5
      Top = 5
      Width = 411
      Height = 338
      BorderStyle = cxcbsNone
      Align = alClient
      LookAndFeel.Kind = lfOffice11
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = ''
      OptionsView.CellTextMaxLineCount = 3
      OptionsView.AutoScaleBands = False
      OptionsView.PaintStyle = psDelphi
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.RowHeaderMinWidth = 30
      OptionsView.RowHeaderWidth = 251
      OptionsView.ValueWidth = 40
      TabOrder = 0
      OnEdited = cxVerticalGrid1Edited
      Version = 1
      object cxVerticalGrid1EditorRow1: TcxEditorRow
        Options.TabStop = False
        Properties.Caption = #1057#1074#1086#1081#1089#1090#1074#1072' '#1087#1086#1076#1087#1080#1089#1080
        Properties.DataBinding.ValueType = 'String'
        Properties.Options.Editing = False
        Properties.Value = Null
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object row_Overlap: TcxEditorRow
        Expanded = False
        Properties.Caption = #1055#1077#1088#1077#1082#1088#1099#1074#1072#1090#1100' '#1087#1086#1076#1087#1080#1089#1080' ('#1088#1080#1089#1086#1074#1072#1090#1100' '#1074#1089#1077')'
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.Alignment = taLeftJustify
        Properties.EditProperties.NullStyle = nssUnchecked
        Properties.EditProperties.ReadOnly = False
        Properties.EditProperties.ValueChecked = 'True'
        Properties.EditProperties.ValueGrayed = ''
        Properties.EditProperties.ValueUnchecked = 'False'
        Properties.DataBinding.ValueType = 'Variant'
        Properties.Value = Null
        ID = 1
        ParentID = 0
        Index = 0
        Version = 1
      end
      object row_DisplayWithinRange1: TcxEditorRow
        Properties.Caption = #1054#1090#1086#1073#1088#1072#1078#1072#1090#1100' '#1090#1086#1083#1100#1082#1086' '#1087#1088#1080' '#1084#1072#1089#1096#1090#1072#1073#1077
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.Alignment = taLeftJustify
        Properties.EditProperties.NullStyle = nssUnchecked
        Properties.EditProperties.ReadOnly = False
        Properties.EditProperties.ValueChecked = 'True'
        Properties.EditProperties.ValueGrayed = ''
        Properties.EditProperties.ValueUnchecked = 'False'
        Properties.DataBinding.ValueType = 'Variant'
        Properties.Value = Null
        Visible = False
        ID = 2
        ParentID = 0
        Index = 1
        Version = 1
      end
      object row_HigherThan1: TcxEditorRow
        Expanded = False
        Properties.Caption = #1041#1086#1083#1100#1096#1077' '#1095#1077#1084' ('#1082#1084')'
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = False
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
        ID = 3
        ParentID = 2
        Index = 0
        Version = 1
      end
      object row_LowerThan1: TcxEditorRow
        Expanded = False
        Properties.Caption = #1052#1077#1085#1100#1096#1077' '#1095#1077#1084' ('#1082#1084')'
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = False
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
        ID = 4
        ParentID = 2
        Index = 1
        Version = 1
      end
      object cxVerticalGrid1EditorRow6: TcxEditorRow
        Expanded = False
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = True
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
        ID = 5
        ParentID = -1
        Index = 1
        Version = 1
      end
      object row_BackgroundType: TcxEditorRow
        Properties.Caption = #1060#1086#1085' '#1087#1086#1076#1087#1080#1089#1080
        Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
        Properties.EditProperties.DropDownListStyle = lsFixedList
        Properties.EditProperties.Items.Strings = (
          #1053#1077#1090' '#1092#1086#1085#1072
          #1055#1088#1103#1084#1086#1091#1075#1086#1083#1100#1085#1080#1082#1086#1084
          #1054#1088#1077#1086#1083#1086#1084)
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
        ID = 6
        ParentID = -1
        Index = 2
        Version = 1
      end
      object row_Background_Color: TcxEditorRow
        Expanded = False
        Properties.Caption = #1062#1074#1077#1090' '#1092#1086#1085#1072
        Properties.EditPropertiesClassName = 'TcxColorComboBoxProperties'
        Properties.EditProperties.ColorDialogType = cxcdtCustom
        Properties.EditProperties.CustomColors = <>
        Properties.EditProperties.ShowDescriptions = False
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
        ID = 7
        ParentID = 6
        Index = 0
        Version = 1
      end
      object cxVerticalGrid1EditorRow9: TcxEditorRow
        Expanded = False
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = True
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
        ID = 8
        ParentID = -1
        Index = 3
        Version = 1
      end
      object row_FontName: TcxEditorRow
        Properties.Caption = #1064#1088#1080#1092#1090' '#1087#1086#1076#1087#1080#1089#1080
        Properties.EditPropertiesClassName = 'TcxFontNameComboBoxProperties'
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
        ID = 9
        ParentID = -1
        Index = 4
        Version = 1
      end
      object row_FontSize: TcxEditorRow
        Expanded = False
        Properties.Caption = #1056#1072#1079#1084#1077#1088
        Properties.EditPropertiesClassName = 'TcxFontNameComboBoxProperties'
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
        ID = 10
        ParentID = 9
        Index = 0
        Version = 1
      end
      object row_Color: TcxEditorRow
        Expanded = False
        Properties.Caption = #1062#1074#1077#1090
        Properties.EditPropertiesClassName = 'TcxColorComboBoxProperties'
        Properties.EditProperties.ColorDialogType = cxcdtCustom
        Properties.EditProperties.CustomColors = <>
        Properties.EditProperties.ShowDescriptions = False
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
        ID = 11
        ParentID = 9
        Index = 1
        Version = 1
      end
      object row_Effects1: TcxEditorRow
        Properties.Caption = #1069#1092#1092#1077#1082#1090#1099
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = True
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
        Visible = False
        ID = 12
        ParentID = 9
        Index = 2
        Version = 1
      end
      object row_DoubleInterval1: TcxEditorRow
        Expanded = False
        Properties.Caption = #1044#1074#1086#1081#1085#1086#1081' '#1080#1085#1090#1077#1088#1074#1072#1083
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.Alignment = taLeftJustify
        Properties.EditProperties.NullStyle = nssUnchecked
        Properties.EditProperties.ReadOnly = False
        Properties.EditProperties.ValueChecked = 'True'
        Properties.EditProperties.ValueGrayed = ''
        Properties.EditProperties.ValueUnchecked = 'False'
        Properties.DataBinding.ValueType = 'Variant'
        Properties.Value = Null
        ID = 13
        ParentID = 12
        Index = 0
        Version = 1
      end
      object row_AllCaps1: TcxEditorRow
        Expanded = False
        Properties.Caption = #1042#1089#1077' '#1079#1072#1075#1083#1072#1074#1085#1099#1077
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.Alignment = taLeftJustify
        Properties.EditProperties.NullStyle = nssUnchecked
        Properties.EditProperties.ReadOnly = False
        Properties.EditProperties.ValueChecked = 'True'
        Properties.EditProperties.ValueGrayed = ''
        Properties.EditProperties.ValueUnchecked = 'False'
        Properties.DataBinding.ValueType = 'Variant'
        Properties.Value = Null
        ID = 14
        ParentID = 12
        Index = 1
        Version = 1
      end
      object row_FontIsBold1: TcxEditorRow
        Expanded = False
        Properties.Caption = #1055#1086#1083#1091#1078#1080#1088#1085#1099#1081
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.Alignment = taLeftJustify
        Properties.EditProperties.NullStyle = nssUnchecked
        Properties.EditProperties.ReadOnly = False
        Properties.EditProperties.ValueChecked = 'True'
        Properties.EditProperties.ValueGrayed = ''
        Properties.EditProperties.ValueUnchecked = 'False'
        Properties.DataBinding.ValueType = 'Variant'
        Properties.Value = Null
        ID = 15
        ParentID = 12
        Index = 2
        Version = 1
      end
      object row_FontIsItalic1: TcxEditorRow
        Expanded = False
        Properties.Caption = #1053#1072#1082#1083#1086#1085#1085#1099#1081
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.Alignment = taLeftJustify
        Properties.EditProperties.NullStyle = nssUnchecked
        Properties.EditProperties.ReadOnly = False
        Properties.EditProperties.ValueChecked = 'True'
        Properties.EditProperties.ValueGrayed = ''
        Properties.EditProperties.ValueUnchecked = 'False'
        Properties.DataBinding.ValueType = 'Variant'
        Properties.Value = Null
        ID = 16
        ParentID = 12
        Index = 3
        Version = 1
      end
      object row_Shadows1: TcxEditorRow
        Expanded = False
        Properties.Caption = #1058#1077#1085#1080
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.Alignment = taLeftJustify
        Properties.EditProperties.NullStyle = nssUnchecked
        Properties.EditProperties.ReadOnly = False
        Properties.EditProperties.ValueChecked = 'True'
        Properties.EditProperties.ValueGrayed = ''
        Properties.EditProperties.ValueUnchecked = 'False'
        Properties.DataBinding.ValueType = 'Variant'
        Properties.Value = Null
        ID = 17
        ParentID = 12
        Index = 4
        Version = 1
      end
      object cxVerticalGrid1EditorRow19: TcxEditorRow
        Expanded = False
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = True
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
        ID = 18
        ParentID = -1
        Index = 5
        Version = 1
      end
      object cxVerticalGrid1EditorRow20: TcxEditorRow
        Properties.Caption = #1055#1086#1083#1086#1078#1077#1085#1080#1077' '#1087#1086#1076#1087#1080#1089#1080
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = True
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
        ID = 19
        ParentID = -1
        Index = 6
        Version = 1
      end
      object row_RotateWithLine: TcxEditorRow
        Expanded = False
        Properties.Caption = #1055#1086#1074#1086#1088#1072#1095#1080#1074#1072#1090#1100' '#1087#1086#1076#1087#1080#1089#1100' '#1074#1084#1077#1089#1090#1077' '#1089' '#1083#1080#1085#1080#1077#1081' ('#1076#1083#1103' '#1083#1080#1085#1077#1081#1085#1099#1093' '#1086#1073#1098#1077#1082#1090#1086#1074')'
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.Alignment = taLeftJustify
        Properties.EditProperties.NullStyle = nssUnchecked
        Properties.EditProperties.ReadOnly = False
        Properties.EditProperties.ValueChecked = 'True'
        Properties.EditProperties.ValueGrayed = ''
        Properties.EditProperties.ValueUnchecked = 'False'
        Properties.DataBinding.ValueType = 'Variant'
        Properties.Value = Null
        ID = 20
        ParentID = 19
        Index = 0
        Version = 1
      end
      object row_Position: TcxEditorRow
        Properties.Caption = #1056#1072#1089#1087#1086#1083#1086#1075#1072#1090#1100' '#1087#1086#1076#1087#1080#1089#1100
        Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.DropDownRows = 7
        Properties.EditProperties.Items.Strings = (
          #1055#1086' '#1094#1077#1085#1090#1088#1091
          #1057#1083#1077#1074#1072'-'#1089#1074#1077#1088#1093#1091
          #1057#1074#1077#1088#1093#1091
          #1057#1087#1088#1072#1074#1072'-'#1089#1074#1077#1088#1093#1091
          #1057#1083#1077#1074#1072
          #1057#1087#1088#1072#1074#1072
          #1057#1083#1077#1074#1072'-'#1057#1085#1080#1079#1091
          #1057#1085#1080#1079#1091
          #1057#1087#1088#1072#1074#1072'-'#1057#1085#1080#1079#1091)
        Properties.EditProperties.MaxLength = 0
        Properties.EditProperties.ReadOnly = False
        Properties.EditProperties.Revertable = True
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
        ID = 21
        ParentID = 19
        Index = 1
        Version = 1
      end
      object row_Offset: TcxEditorRow
        Expanded = False
        Properties.Caption = #1085#1072' '#1091#1076#1072#1083#1077#1085#1080#1080' '#1086#1090' '#1094#1077#1085#1090#1088#1072' '#1086#1073#1098#1077#1082#1090#1072
        Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
        Properties.EditProperties.Alignment.Horz = taLeftJustify
        Properties.EditProperties.Alignment.Vert = taVCenter
        Properties.EditProperties.MaxValue = 30.000000000000000000
        Properties.EditProperties.MinValue = 2.000000000000000000
        Properties.EditProperties.ReadOnly = False
        Properties.DataBinding.ValueType = 'Float'
        Properties.Value = Null
        ID = 22
        ParentID = 21
        Index = 0
        Version = 1
      end
    end
  end
  object Panel1: TPanel [3]
    Left = 192
    Top = 512
    Width = 297
    Height = 301
    TabOrder = 3
    Visible = False
    object GroupBox1: TGroupBox
      Left = 88
      Top = 20
      Width = 273
      Height = 93
      Caption = #1057#1074#1086#1081#1089#1090#1074#1072' '#1087#1086#1076#1087#1080#1089#1080
      TabOrder = 0
      object CheckBox1: TCheckBox
        Left = 8
        Top = 20
        Width = 249
        Height = 17
        Caption = #1055#1077#1088#1077#1082#1088#1099#1074#1072#1090#1100' '#1087#1086#1076#1087#1080#1089#1080' ('#1088#1080#1089#1086#1074#1072#1090#1100' '#1074#1089#1077')'
        TabOrder = 0
      end
      object CheckBox2: TCheckBox
        Left = 8
        Top = 40
        Width = 249
        Height = 17
        Caption = #1054#1090#1086#1073#1088#1072#1078#1072#1090#1100' '#1090#1086#1083#1100#1082#1086' '#1087#1088#1080' '#1084#1072#1089#1096#1090#1072#1073#1077
        TabOrder = 1
      end
    end
    object GroupBox2: TGroupBox
      Left = 8
      Top = 124
      Width = 301
      Height = 69
      Caption = ' '#1055#1086#1083#1086#1078#1077#1085#1080#1077' '#1087#1086#1076#1087#1080#1089#1080' '
      TabOrder = 1
      object CheckBox3: TCheckBox
        Left = 8
        Top = 16
        Width = 305
        Height = 17
        Caption = #1055#1086#1074#1086#1088#1072#1095#1080#1074#1072#1090#1100' '#1087#1086#1076#1087#1080#1089#1100' '#1074#1084#1077#1089#1090#1077' '#1089' '#1083#1080#1085#1080#1077#1081' ('#1076#1083#1103' '#1083#1080#1085#1077#1081#1085#1099#1093' '#1086#1073#1098#1077#1082#1090#1086#1074')'
        TabOrder = 0
      end
    end
    object GroupBox3: TGroupBox
      Left = 20
      Top = 216
      Width = 301
      Height = 157
      Caption = #1064#1088#1080#1092#1090' '#1087#1086#1076#1087#1080#1089#1080
      TabOrder = 2
      object CheckBox4: TCheckBox
        Left = 8
        Top = 80
        Width = 305
        Height = 17
        Caption = #1055#1086#1074#1086#1088#1072#1095#1080#1074#1072#1090#1100' '#1087#1086#1076#1087#1080#1089#1100' '#1074#1084#1077#1089#1090#1077' '#1089' '#1083#1080#1085#1080#1077#1081' ('#1076#1083#1103' '#1083#1080#1085#1077#1081#1085#1099#1093' '#1086#1073#1098#1077#1082#1090#1086#1074')'
        TabOrder = 0
      end
      object GroupBox4: TGroupBox
        Left = 28
        Top = 104
        Width = 249
        Height = 49
        Caption = #1069#1092#1092#1077#1082#1090#1099
        TabOrder = 1
        object cb_DoubleInterval: TCheckBox
          Left = 12
          Top = 20
          Width = 137
          Height = 17
          Caption = #1044#1074#1086#1081#1085#1086#1081' '#1080#1085#1090#1077#1088#1074#1072#1083
          TabOrder = 0
        end
      end
      object Edit1: TEdit
        Left = 72
        Top = 24
        Width = 33
        Height = 21
        TabOrder = 2
        Text = 'Edit1'
      end
      object ColorBox2: TColorBox
        Left = 72
        Top = 48
        Width = 113
        Height = 22
        ItemHeight = 16
        TabOrder = 3
      end
    end
    object ColorBox1: TColorBox
      Left = 96
      Top = 104
      Width = 113
      Height = 22
      ItemHeight = 16
      TabOrder = 3
    end
  end
  inherited ActionList1: TActionList
    Left = 104
    inherited act_Cancel: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
    end
  end
  inherited FormStorage1: TFormStorage
    Left = 76
  end
  object ActionList_new: TActionList
    Left = 264
    Top = 4
    object act_Default: TAction
      Caption = #1047#1085#1072#1095#1077#1085#1080#1103' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
      OnExecute = act_DefaultExecute
    end
  end
end
