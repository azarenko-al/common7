unit u_MapX_lib;

interface
//{$I Mapx.inc}

uses SysUtils,Variants,DB,  Dialogs, MapXLib_TLB,

    u_files,

    u_func,
    u_DB,
    u_dlg,
    u_Geo,

    u_MapX
    ;

type

  //-----------------------------------------------------
  TmiMap = class
  //-----------------------------------------------------
  private
    FMap: TMap;

//    FCount: integer;

    FBLBounds: TBLRect;

 {   FVDataSet: CMapXDataset;
    1
}



    FFeatureCountInBuffer: integer;
    FileName: string;

    function GetActive: boolean;

  public
    FieldArr: TmiFieldArray;

    VLayer: CMapXLayer;
    VDataSet: CMapXDataset;


    constructor Create;
    destructor Destroy; override;


    function CreateFile1(aFileName: string; aFields: array of TmiFieldRec):
        CMapXLayer;

    function  OpenFile (aFileName: string): boolean;

    procedure CloseFile ();

    function GetBounds: TBLRect;

    function AddFeature (aFeature: CMapXFeature): CMapXFeature;
    procedure BeginAccess;
// TODO: CreateMemFromMapFields

    procedure CreateMemFromMapFields(aDataSet: TDataSet);

    function WriteRegionF (var aPoints: TBLPointArrayF;  aParams: array of TmiParam): CMapXFeature; overload;

    function WriteRegion(aPoints: TBLPointArray; aParams: array of TmiParam):
        CMapXFeature; overload;

    function WriteBLRect (aBLRect: TBLRect; aParams: array of TmiParam): CMapXFeature;

    function  WriteLine   (aBLVector: TBLVector; aParams: array of TmiParam): CMapXFeature; overload;
    function  WriteLine   (aBLPoint1,aBLPoint2: TBLPoint; aParams: array of TmiParam): CMapXFeature; overload;
    function  WriteLineF  (var aBLPoints: TBLPointArrayF; aParams: array of TmiParam): CMapXFeature; overload;

    function  WriteSymbol (aPoint: TBLPoint; aParams: array of TmiParam): CMapXFeature;
    //aAngle: integer;
    function  WriteText   (aPoint: TBLPoint; aCaption: string; aPosition: integer; aAngle: double): CMapXFeature;

    procedure WriteFeatureFieldValues (vFeature: CMapXFeature; aParams: array of TmiParam); overload;

    procedure WriteFeaturePoints (aFeature: CMapXFeature; aBLVector: TBLVector);
    procedure WriteFeaturePoint  (aFeature: CMapXFeature; aBLPoint: TBLPoint);
    procedure WriteFeatureFontRotation (aFeature: CMapXFeature; aAngle: integer);
    function  WriteBezier (var aBLPoints: TBLPointArrayF; aParams: array of TmiParam): CMapXFeature;

    function PrepareStyle(aFeatureType: integer; aStyle: TmiStyleRec): boolean;

    function ExtractObjectFields(aVFeature: CMapXFeature; var aFields: TmiFieldArray): boolean;
    function ExtractObjectRec (aVFeature: CMapXFeature; var aObjectRec: TmiObjectRec): boolean;

    function  GetObjectCount(): integer;

    function  FieldIndexByName (aFieldName: string): integer;

    procedure DelByCondition1(aCondition: string);

    function  FindFieldValue (aKeyField,aValueField:string; aKeyValue: Variant; var aValue:Variant): boolean;
    function  GetFieldValue (vFeature: CMapXFeature; aFieldName: string): Variant;
    procedure SetZoom (Value: double);

    procedure DisableDrawings();
    procedure EnableDrawings();
    procedure EndAccess;
    procedure NumericCoordSys_Set_DATUM_KRASOVKSY42;

    //aStyle: TmiStyleRec;

    procedure WriteBezierF(var aBLPoints: TBLPointArrayF; aParams: array of
        TmiParam);

//    function WriteCollection(aParams: array of TmiParam): CMapXFeature;

    function WriteLineArr(aBLPoints: TBLPointArray; aParams: array of TmiParam):
        CMapXFeature;

    function WritePart(aFeature: CMapXFeature; aBLPoints: TBLPointArray): Integer;

    function WriteVector(aBLVector: TBLVector; aParams: array of TmiParam):
        CMapXFeature;

    property Active: boolean read GetActive;

    property Bounds: TBLRect read GetBounds;

{    property VDataSet: CMapXDataset read GetVDataSet;
    property VLayer: CMapXLayer read GetVLayer;
}
  end;


//  function mapx_CreateMemFromMapFile111111111(aMapFileName: string; aDataSet:
  //    TDataSet): boolean;


//type
// TMapLibX = class(TInterfacedObject, IMapLibX)
//  public


  function mapx_GetMapFileBounds(aFileName: string): TBLRect;

//;
//    aParams: array of TmiParam): CMapXFeature;

procedure mapx_WriteFeatureFieldValues(aVLayer: CMapXLayer; aCMapXDataset:
    CMapXDataset; aVFeature: CMapXFeature; aParams: array of TmiParam);

procedure mapx_DeleteFeatures(aVLayer: CMapXLayer; aCondition: string);

//procedure mapx_SetMapDefaultProjection(aMap: TMap);


// -------------------------

function mapx_WritePoint_(aMap: TMap; aVLayer: CMapXLayer; aBLPoint: TBLPoint):
    CMapXFeature;

function mapx_WriteVector_(aMap: TMap; aVLayer: CMapXLayer; aBLVector:
    TBLVector): CMapXFeature;

function mapx_WritePoly_(aMap: TMap; aVLayer: CMapXLayer; aBLPointArray:
    TBLPointArray; aColor: Integer = 0): CMapXFeature;

function mapx_WriteRegion_(aMap: TMap; aVLayer: CMapXLayer; aBLPointArray:
    TBLPointArray): CMapXFeature;

// -------------------------

function mapx_RowValues_GetItemValue(aRowValues: CMapXRowValues; aName:
    string): Variant;

function mapx_Dataset_FieldExists(aCMapxDataset: CMapxDataset; aName: string):
    Boolean;


//====================================================================
implementation
//====================================================================

{

// Constants for enum FieldTypeConstants
type
  FieldTypeConstants = TOleEnum;
const
  miTypeString = $00000000;
  miTypeNumeric = $00000001;
  miTypeDate = $00000002;
  miTypeInteger = $00000003;
  miTypeSmallInt = $00000004;
  miTypeFloat = $00000005;
  miTypeLogical = $00000006;

 }

//------------------------------------------------------
constructor TmiMap.Create;
//------------------------------------------------------
var
  s: string;
begin
  inherited Create;

  try
    FMap:=TMap.Create(nil);

   //  Map1.TitleText:='';
    FMap.Title.X:=0;
    FMap.Title.Y:=0;

    FMap.Title.Editable:= False;
    FMap.Title.Border:= False;
    FMap.Title.Caption:='-';

    FMap.Title.Visible:=False;

{  //   Map1.Title.Visible:=False;
   FMap.Title.Caption:=' ';
   FMap.Title.Visible:=False;
  // Map1.TitleText:='dhdhfgh';
}


 //   FMap.MapUnit:=miUnitKilometer;
  except

      s:='MapX 50 initialization error!';


    ErrorDlg (s);
  //  raise Exception.Create ('MapX initialization error!');
  end;
end;


destructor TmiMap.Destroy;
begin
  CloseFile();
  if Assigned(FMap) then
    FreeAndNil(FMap);
//    FMap.Free;

  inherited;
end;

procedure TmiMap.BeginAccess;
begin
  VLayer.BeginAccess(miAccessReadWrite);
end;

procedure TmiMap.EndAccess;
begin
  VLayer.EndAccess(miAccessEnd);
end;


procedure TmiMap.SetZoom (Value: double);
begin
  FMap.Zoom:=Value;
end;

//------------------------------------------------------
procedure TmiMap.DisableDrawings();
//------------------------------------------------------
begin
  FMap.AutoRedraw:= false;
end;

//------------------------------------------------------
procedure TmiMap.EnableDrawings();
//------------------------------------------------------
begin
  FMap.AutoRedraw:= true;
end;


//------------------------------------------------------
function TmiMap.FieldIndexByName (aFieldName: string): integer;
//------------------------------------------------------
var i: integer;
begin
  for i:=0 to High(FieldArr) do
    if Eq(FieldArr[i].Name, aFieldName) then begin Result:=i; Exit; end;
  Result:=-1;
end;


//------------------------------------------------------
function TmiMap.ExtractObjectFields(aVFeature: CMapXFeature; var aFields:
    TmiFieldArray): boolean;
//------------------------------------------------------
var
  vRowValues: CMapXRowValues;
  vRowValue: CMapXRowValue;
  t,i,j,iCount: integer;
  vPoints: CMapXPoints;
  vPoint: CMapXPoint;
 // s: string;
  blPoint: TBLPoint;

 // selectedBounds: CMapXRectangle;

begin
  Result := False;
 // FillChar (aFields, SizeOf(aObjectRec), 0);
  SetLength(aFields, Length(FieldArr));

  vRowValues:=VDataSet.RowValues[aVFeature];
  if not Assigned(vRowValues) then
    Exit;


  for i:=0 to High(FieldArr) do
  begin
    aFields[i].name :=FieldArr[i].Name;


      vRowValue:=vRowValues.Item[FieldArr[i].Name];


    if assigned(vRowValue) then
      aFields[i].Value:=vRowValue.Value
    else
      aFields[i].Value:=null;
  end;

//  aObjectRec.FieldArr:=FieldArr;

  Result := True;

end;

//------------------------------------------------------
function TmiMap.ExtractObjectRec (aVFeature: CMapXFeature;
                                   var aObjectRec: TmiObjectRec): boolean;
//------------------------------------------------------
var vRowValues: CMapXRowValues;
  t,i,j,iCount: integer;
  vPoints: CMapXPoints;
  vPoint: CMapXPoint;
 // s: string;
  blPoint: TBLPoint;

 // selectedBounds: CMapXRectangle;

begin
  FillChar (aObjectRec, SizeOf(aObjectRec), 0);

  ExtractObjectFields(aVFeature, aObjectRec.Fields );

{


  //--------------------------------------------------------------
  vRowValues:=VDataSet.RowValues[aVFeature];

  for i:=0 to High(FieldArr) do
    FieldArr[i].Value:=vRowValues.Item[FieldArr[i].Name].Value;

  aObjectRec.FieldArr:=FieldArr;
  //--------------------------------------------------------------
}



{  aObjectRec.LineStyle.Color:=aVFeature.Style.LineColor;
  aObjectRec.Bounds         :=mapx_XRectangleToBLRect (aVFeature.Bounds);
}
//  Extra

  aObjectRec.FeatureType    :=aVFeature.Type_;

  case aObjectRec.FeatureType of

    //---------------------------------------------
    miFeatureTypeRegion, miFeatureTypeLine:
    //---------------------------------------------
    begin
        iCount:=aVFeature.Parts.Count;
        SetLength (aObjectRec.Parts, iCount);
        for i:=0 to aVFeature.Parts.Count-1 do
        begin

            vPoints:=aVFeature.Parts.Item[i+1];


          iCount:= vPoints.Count;

//          aObjectRec.Parts[i].Points.Count:=iCount;

          SetLength (aObjectRec.Parts[i].Points, iCount);

          for j:=0 to vPoints.Count-1 do
          begin

              vPoint := vPoints.Item[j+1];

            blPoint.B:=vPoint.Y;
            blPoint.L:=vPoint.X;

            aObjectRec.Parts[i].Points[j]:=blPoint;
          end;
        end;
    end;

    //---------------------------------------------
    miFeatureTypeSymbol:
    //---------------------------------------------
    begin
        vPoint:=aVFeature.Point;
        aObjectRec.Point.B:=vPoint.Y;
        aObjectRec.Point.L:=vPoint.X;
    end;

    //---------------------------------------------
    miFeatureTypeText:
    //---------------------------------------------
    begin
        vPoint:=aVFeature.Point;
        aObjectRec.Point.B:=vPoint.Y;
        aObjectRec.Point.L:=vPoint.X;
    end;
  end;
end;


//------------------------------------------------------
function TmiMap.OpenFile (aFileName: string): boolean;
//------------------------------------------------------
var vBounds: CMapXRectangle;
    i,j,k,m: integer;

 //   vFields: CMapXFields;
begin
  Result:=false;


  aFileName:=ChangeFileExt(Trim(aFileName), '.tab');
  FileName:=aFileName;


  Assert ( FileExists(aFileName) );

//  begin
  //  Result:=False;
  //  Exit;
 // end;


  FBLBounds:=MakeBLRect(0,0,0,0);


  Assert(FMap.Layers<>nil);

  FMap.Layers.RemoveAll;
  FMap.Datasets.RemoveAll;
//  ClearFields ();

  try
    VLayer:=FMap.Layers.Add (aFileName, EmptyParam);
    if not Assigned(VLayer) then
      Exit;

      ////////////////////


    NumericCoordSys_Set_DATUM_KRASOVKSY42();

    {
    FMap.NumericCoordSys.Set_ (miLongLat, DATUM_KRASOVKSY42,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam);
    }


    VDataSet:=FMap.Datasets.Add (miDataSetLayer, VLayer, EmptyParam,EmptyParam,EmptyParam,EmptyParam,EmptyParam,EmptyParam ) ;


    i := VDataSet.RowCount;


     j:=vDataSet.Fields.Count;

//     vDataSet.Fields.Item()


//    FVDataSet:=nil;

    mapx_GetMapLayerFields (VLayer, FieldArr);

    i := 0;

{
    // get FieldArr
    if VLayer.Datasets.Count > 0 then
    begin
      FVDataSet:=VLayer.Datasets.Item[1];
      vFields:=FVDataSet.FieldArr;

      SetLength (FieldArr, vFields.Count);
      for i:=0 to vFields.Count-1 do
      begin
        FieldArr[i].Name:=vFields.Item[i+1].Name;
        FieldArr[i].Type_:=vFields.Item[i+1].Type_;

//        FieldArr[i].Name:=vFields.Item(i+1).Name;
 //       FieldArr[i].Type_:=vFields.Item(i+1).Type_;
//        AddField (vFields.Item(i+1).Name, vFields.Item(i+1).Type_);
      end;
    end;
}

    FBLBounds:=mapx_XRectangleToBLRect (VLayer.Bounds);

    FMap.MapUnit:=miUnitKilometer;
 //   FMap.Zoom:=DEF_ZOOM;

    Result:=True;
  except
    FMap.Layers.RemoveAll;
    FMap.Datasets.RemoveAll;

  end;
end;


// ---------------------------------------------------------------
procedure TmiMap.NumericCoordSys_Set_DATUM_KRASOVKSY42;
// ---------------------------------------------------------------
var
  vMapXDatum: CMapXDatum;

begin
{    FMap.NumericCoordSys.Set_ (miLongLat, DATUM_KRASOVKSY42,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam);

}

   vMapXDatum:=CoDatum.create;
   vMapXDatum.Set_(3, 23.57, -140.95, -79.8, 0, -0.35, -0.79, -0.22, 0);


  FMap.NumericCoordSys.Set_(miLongLat,  vMapXDatum, // 9999, // DATUM_KRASOVKSY42,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam);


end;



// ---------------------------------------------------------------
procedure TmiMap.CloseFile();
// ---------------------------------------------------------------
begin
  if Assigned(FMap) then
  begin
    FMap.Datasets.RemoveAll;
    FMap.Layers.RemoveAll;
  end else
    raise Exception.Create('TmiMap.CloseFile');
end;

//--------------------------------------------------------------------
procedure TmiMap.DelByCondition1(aCondition: string);
//--------------------------------------------------------------------
begin
  mapx_DeleteFeatures(VLayer, aCondition);

end;


//------------------------------------------------------
function TmiMap.AddFeature (aFeature: CMapXFeature): CMapXFeature;
//------------------------------------------------------
begin
  Result:=VLayer.AddFeature (aFeature, EmptyParam);
//  Inc(FCount);
end;

//------------------------------------------------------
function TmiMap.WriteLine (aBLPoint1,aBLPoint2: TBLPoint; aParams: array of TmiParam): CMapXFeature;
//------------------------------------------------------
begin
  Result:=WriteLine (MakeBLVector (aBLPoint1,aBLPoint2), aParams);
end;

//------------------------------------------------------
function TmiMap.WriteLineF(var aBLPoints: TBLPointArrayF; aParams: array of
    TmiParam): CMapXFeature;
//------------------------------------------------------
var i: integer;
    f: CMapXFeature;
    ps: CMapXPoints;
begin
  ps := CoPoints.Create;

//  ps.RemoveAll;

  for i:=0 to aBLPoints.Count-1 do
    ps.AddXY (aBLPoints.Items[i].L, aBLPoints.Items[i].B, EmptyParam);

  f:=FMap.FeatureFactory.CreateLine (ps, FMap.DefaultStyle);

//  f:=FMap.FeatureFactory.CreateLine (ps, EmptyParam);

  try
    Assert(Assigned(VLayer), 'Value not assigned');

 //zzz   VLayer.BeginAccess(miAccessReadWrite);
    Result:=AddFeature (f);
    WriteFeatureFieldValues (Result, aParams);
  finally
//zz    VLayer.EndAccess(miAccessEnd);
  end;
end;


//------------------------------------------------------
function TmiMap.WriteLineArr(aBLPoints: TBLPointArray; aParams: array of
    TmiParam): CMapXFeature;
//------------------------------------------------------
var i: integer;
    f: CMapXFeature;
    ps: CMapXPoints;
begin
  ps := CoPoints.Create;

//  ps.RemoveAll;

  for i:=0 to High(aBLPoints) do
  begin
  //  Assert (aBLPoints[i].L<>0);
  //  Assert (aBLPoints[i].B<>0);

 //   if aBLPoints[i].L<>0 then
    ps.AddXY (aBLPoints[i].L, aBLPoints[i].B, EmptyParam);
  end;

  f:=FMap.FeatureFactory.CreateLine (ps, FMap.DefaultStyle);

//  f:=FMap.FeatureFactory.CreateLine (ps, EmptyParam);

  try
    Assert(Assigned(VLayer), 'Value not assigned');

//    BeginAccess();

 //zzz   VLayer.BeginAccess(miAccessReadWrite);
    Result:=AddFeature (f);
    WriteFeatureFieldValues (Result, aParams);
  finally
//zz    VLayer.EndAccess(miAccessEnd);
  end;
end;

//------------------------------------------------------
function TmiMap.WritePart(aFeature: CMapXFeature; aBLPoints: TBLPointArray):
    Integer;
//------------------------------------------------------
var i: integer;
    f: CMapXFeature;
    vMapXPoints: CMapXPoints;
begin
  vMapXPoints := CoPoints.Create;

  for i:=0 to High(aBLPoints) do
  begin
    Assert (aBLPoints[i].L<>0);
    Assert (aBLPoints[i].B<>0);


    vMapXPoints.AddXY (aBLPoints[i].L, aBLPoints[i].B, EmptyParam);
  end;

  aFeature.Parts.Add(vMapXPoints);
end;



function TmiMap.WriteVector(aBLVector: TBLVector; aParams: array of TmiParam):
    CMapXFeature;
begin
  Result := WriteLine (aBLVector, aParams);
end;


//------------------------------------------------------
procedure TmiMap.WriteBezierF(var aBLPoints: TBLPointArrayF; aParams: array of
    TmiParam);
//------------------------------------------------------
const
  ARROW_LEN = 300;
var
  arrBLPointsOut: TBLPointArrayF;
  iCenter: integer;
  dAzimuth: double;
  blVector: TBLVector;
begin
  geo_MakeBezierF (aBLPoints, arrBLPointsOut);
               

//  WriteLineF(arrBLPointsOut, aStyle, aParams);
  //PrepareStyle(miFeatureTypeLine, aStyle );

//  WriteLineF(arrBLPointsOut, aStyle, aParams);
  WriteLineF(arrBLPointsOut, aParams);

  iCenter:=arrBLPointsOut.Count div 2;

  //draw arrow on the centre

  dAzimuth:=geo_Azimuth(arrBLPointsOut.Items[iCenter], arrBLPointsOut.Items[iCenter-1]);
  blVector.Point1:= arrBLPointsOut.Items[iCenter];

  blVector.Point2:= geo_RotateByAzimuth (blVector.Point1, ARROW_LEN, geo_Azimuth_Plus_Azimuth(dAzimuth,30));

//  WriteVector(blVector, aStyle, [] );
  WriteVector(blVector, [] );

//  WriteVector(blVector, aStyle, [mapx_Par('objname','')] );

  blVector.Point2:=geo_RotateByAzimuth (blVector.Point1, ARROW_LEN, geo_Azimuth_Plus_Azimuth(dAzimuth,-30));
//  WriteVector(blVector, aStyle, []);
  WriteVector(blVector, []);

//  WriteVector(blVector, aStyle, [mapx_Par('objname','')]);
end;


//------------------------------------------------------
function TmiMap.WriteLine (aBLVector: TBLVector; aParams: array of TmiParam): CMapXFeature;
//------------------------------------------------------
var i: integer;
    vFeature: CMapXFeature;
    ps: CMapXPoints;
begin
  Assert(Assigned(FMap), 'FMap not assigned');
  Assert(Assigned(VLayer), 'FMap not assigned');

  Assert (aBLVector.Point1.L<>0);
  Assert (aBLVector.Point1.B<>0);

  Assert (aBLVector.Point2.L<>0);
  Assert (aBLVector.Point2.B<>0);


  ps := CoPoints.Create;

  with aBLVector do
  begin
    ps.AddXY (Point1.L, Point1.B, EmptyParam);
    ps.AddXY (Point2.L, Point2.B, EmptyParam);
  end;

  vFeature:=FMap.FeatureFactory.CreateLine (ps, FMap.DefaultStyle);

//  vFeature.Parts[1].;

//  vFeature:=FMap.FeatureFactory. CreateLine (ps, FMap.DefaultStyle);


  try
    Assert(Assigned(VLayer), 'Value not assigned');

    VLayer.BeginAccess(miAccessReadWrite);
    Result:=AddFeature (vFeature);

    WriteFeatureFieldValues (Result, aParams);
  finally
    VLayer.EndAccess(miAccessEnd);
  end;
end;


//------------------------------------------------------
function TmiMap.WriteSymbol (aPoint: TBLPoint; aParams: array of TmiParam): CMapXFeature;
//------------------------------------------------------
//aAngle: integer;
var f: CMapXFeature;
    p: CMapXPoint;
begin
  Assert(Assigned(VLayer), 'Value not assigned');


  p := CoPoint.Create;
  p.Set_ (aPoint.L, aPoint.B);

  //FMap.DefaultStyle.SymbolFontRotation:=aAngle;

  f:=FMap.FeatureFactory.CreateSymbol (p, FMap.DefaultStyle);

  try

    VLayer.BeginAccess(miAccessReadWrite);
    Result:=AddFeature (f);

    WriteFeatureFieldValues (Result, aParams);
  finally
    VLayer.EndAccess(miAccessEnd);
  end;
end;



//------------------------------------------------------
function TmiMap.WriteText (aPoint: TBLPoint; aCaption: string;
        aPosition: integer; aAngle: double): CMapXFeature;
//------------------------------------------------------
var f: CMapXFeature;
    p: CMapXPoint;

//    iPosition: integer;
begin
  p := CoPoint.Create;
  p.Set_ (aPoint.L, aPoint.B);

  try
    FMap.DefaultStyle.TextFontRotation:=Round(aAngle);

    f:=FMap.FeatureFactory.CreateText (p, aCaption, aPosition, FMap.DefaultStyle);
    Result:=AddFeature (f);

//    Result.Style.TextFontRotation:=Round(aAngle);
//    Result.Update (EmptyParam, EmptyParam);
  except
    Result:=nil;
  end;
end;


//------------------------------------------------------
function TmiMap.WriteRegionF(var aPoints: TBLPointArrayF; aParams: array of
    TmiParam): CMapXFeature;
//------------------------------------------------------
var i:  integer;
    f:  CMapXFeature;
    ps: CMapXPoints;
begin
  if aPoints.Count<3 then
  begin
//  if Length(aPoints)<3 then begin
    Result:=nil;
    Exit;
  end;

  f:=FMap.FeatureFactory.CreateRegion(EmptyParam, EmptyParam);
  f.Style:=FMap.DefaultStyle;

  ps := CoPoints.Create;
  for i:=0 to aPoints.Count-1 do //High(aPoints) do
    ps.AddXY (aPoints.Items[i].l, aPoints.Items[i].b, EmptyParam);

  f.Parts.Add (ps);

  try
    Assert(Assigned(VLayer), 'Value not assigned');

    VLayer.BeginAccess(miAccessReadWrite);
    Result:=AddFeature (f);
    WriteFeatureFieldValues (Result, aParams);
  finally
    VLayer.EndAccess(miAccessEnd);
  end;
end;


//------------------------------------------------------
function TmiMap.WriteRegion(aPoints: TBLPointArray; aParams: array of
    TmiParam): CMapXFeature;
//------------------------------------------------------
var i:  integer;
    vFeature:  CMapXFeature;
    vPoints: CMapXPoints;
begin
  if Length(aPoints)<3 then
  begin
//  if Length(aPoints)<3 then begin
    Result:=nil;
    Exit;
  end;

  vFeature:=FMap.FeatureFactory.CreateRegion(EmptyParam, EmptyParam);

//  vFeature:=FMap.FeatureFactory.CreateLine (EmptyParam, EmptyParam);
  vFeature.Style:=FMap.DefaultStyle;

  vPoints := CoPoints.Create;
  for i:=0 to High(aPoints) do
    vPoints.AddXY (aPoints[i].l, aPoints[i].b, EmptyParam);

  vFeature.Parts.Add (vPoints);

  try
    Assert(Assigned(VLayer), 'Value not assigned');

    VLayer.BeginAccess(miAccessReadWrite);

    Result:=VLayer.AddFeature (vFeature, EmptyParam);

//    Result:=AddFeature (vFeature);
    WriteFeatureFieldValues (Result, aParams);
  finally
    VLayer.EndAccess(miAccessEnd);
  end;
end;



//------------------------------------------------------
function TmiMap.WriteBLRect (aBLRect: TBLRect; aParams: array of TmiParam): CMapXFeature;
//------------------------------------------------------
var
  arrPoints: TBLPointArrayF;
begin
  Assert(aBLRect.TopLeft.B<>aBLRect.BottomRight.B);


  geo_BLRectToBLPointsF(aBLRect, arrPoints);
  Result:=WriteRegionF (arrPoints, aParams);
end;


//------------------------------------------------------
function TmiMap.FindFieldValue (aKeyField,aValueField:string; aKeyValue: Variant; var aValue:Variant): boolean;
//------------------------------------------------------
var rv: CMapXRowValues;
    i: integer;
    vFeatures: CMapXFeatures;
    vFeature: CMapXFeature;
begin
  vFeatures:=VLayer.AllFeatures;
  Result:=false;

  for i:=1 to vFeatures.Count do
  begin

    vFeature:=vFeatures.Item[i];
    rv:=VLayer.Datasets.Item[1].RowValues[vFeature];

    if rv.Item[aKeyField].Value=aKeyValue then
    begin
      aValue:=rv.Item[aValueField].Value;
      Result:=true;  Exit;
    end;


  end;
end;

//------------------------------------------------------
function TmiMap.GetFieldValue (vFeature: CMapXFeature; aFieldName: string): Variant;
//------------------------------------------------------
var i: integer;
    rv: CMapXRowValues;
begin

  rv:=VLayer.Datasets.Item[1].RowValues[vFeature];
  try
    Result:=rv.Item[aFieldName].Value;
//    if VarIsNull(Result) then Result:='';
  except
    Result:=null;
  end;


end;



function TmiMap.GetObjectCount(): integer;
begin
  Result:=VLayer.AllFeatures.Count;
end;

//------------------------------------------------------
procedure TmiMap.WriteFeatureFieldValues (vFeature: CMapXFeature; aParams: array of TmiParam);
//------------------------------------------------------
var rvs: CMapXRowValues;
//    v: variant;
    i: integer;
    rv: CMapXRowValue;
begin

  rvs:=VLayer.Datasets.Item[1].RowValues[vFeature];

  Assert(rvs<>nil);

  for i:=0 to High(aParams) do
    if not VarIsNull (aParams[i].Value) then
    begin

      rv:=rvs.Item[aParams[i].FieldName];

    //  Assert(v<>null);

      rv.Value:=aParams[i].Value;
    end;

//    vFeature.
  vFeature.Update (EmptyParam, rvs);
end;

//------------------------------------------------------
procedure TmiMap.WriteFeaturePoints (aFeature: CMapXFeature; aBLVector: TBLVector);
//------------------------------------------------------
var ps: CMapXPoints;
begin
  ps := CoPoints.Create;
  ps.AddXY (aBLVector.Point1.L, aBLVector.Point1.B, EmptyParam);
  ps.AddXY (aBLVector.Point2.L, aBLVector.Point2.B, EmptyParam);

  aFeature.Parts.RemoveAll;
  aFeature.Parts.Add (ps);
  aFeature.Update (EmptyParam, EmptyParam);
end;

//------------------------------------------------------
procedure TmiMap.WriteFeaturePoint (aFeature: CMapXFeature; aBLPoint: TBLPoint);
//------------------------------------------------------
begin
  aFeature.Point.Set_ (aBLPoint.L, aBLPoint.B);
  aFeature.Update (EmptyParam, EmptyParam);

end;

//------------------------------------------------------
procedure TmiMap.WriteFeatureFontRotation (aFeature: CMapXFeature; aAngle: integer);
//------------------------------------------------------
begin
  aFeature.Style.TextFontRotation:=aAngle;
  aFeature.Update (EmptyParam, EmptyParam);
end;


//------------------------------------------------------
function TmiMap.PrepareStyle(aFeatureType: integer; aStyle: TmiStyleRec): boolean;
//------------------------------------------------------
begin
  mapx_PrepareStyle (FMap, aFeatureType,  aStyle);
end;


//------------------------------------------------------
function TmiMap.WriteBezier(var aBLPoints: TBLPointArrayF; aParams: array of TmiParam): CMapXFeature;
//------------------------------------------------------
var
  arrBLPointsOutF: TBLPointArrayF;
  iCenter: integer;
  dAzimuth: double;
  blVector: TBLVector;
begin
  geo_MakeBezierF (aBLPoints, arrBLPointsOutF);

  Result:= WriteLineF(arrBLPointsOutF, aParams);

  iCenter:=arrBLPointsOutF.Count div 2;

  //draw arrow on the centre
  dAzimuth:=geo_Azimuth(arrBLPointsOutF.Items[iCenter], arrBLPointsOutF.Items[iCenter-1]);
  blVector.Point1:= arrBLPointsOutF.Items[iCenter];

  blVector.Point2:= geo_RotateByAzimuth (blVector.Point1,300, geo_Azimuth_Plus_Azimuth(dAzimuth,30));
  WriteLine(blVector, [] );

  blVector.Point2:=geo_RotateByAzimuth (blVector.Point1,300, geo_Azimuth_Plus_Azimuth(dAzimuth,-30));
  WriteLine(blVector, []);
end;



function TmiMap.GetBounds: TBLRect;
begin
  Result := FBLBounds;
end;


//-------------------------------------------------
function mapx_GetMapFileBounds(aFileName: string): TBLRect;
//-------------------------------------------------
var oMifFile: TmiMap;
begin
  Result:=MakeBLRect(0,0,0,0);

  if (not FileExists(aFileName)) then
    Exit;

  oMifFile:=TmiMap.Create;

  if oMifFile.OpenFile (aFileName) then
    Result:=oMifFile.Bounds;

  oMifFile.Free;
end;

// ---------------------------------------------------------------
function TmiMap.CreateFile1(aFileName: string; aFields: array of TmiFieldRec):
    CMapXLayer;
// ---------------------------------------------------------------
var
  vFlds: Variant;
  vLayerInfoObject: CMapXLayerInfo;
  vXField: CMapXField;
  i: Integer;
begin
  Assert(Length(aFields)>0);

  aFileName:=ChangeFileExt(aFileName, '.Tab');

  mapx_DeleteFile (aFileName);
  ForceDirByFileName (aFileName);


  vFlds := CoFields.Create;
//  vFlds.


  for i:=0 to High(aFields) do
    case aFields[i].Type_ of
      miTypeString : begin
                       if aFields[i].Size=0 then
                         aFields[i].Size:=200;

                      // vXField :=
                       vFlds.AddStringField (aFields[i].Name, aFields[i].Size); //, aFields[i].IsIndexed
//                       vXField.
                     end;

      miTypeFloat    : vFlds.AddFloatField   (aFields[i].Name);
      miTypeInt      : vFlds.AddIntegerField (aFields[i].Name);
      miTypeSmallInt : vFlds.AddIntegerField (aFields[i].Name);
    end;

//  vFlds.


  vLayerInfoObject := CoLayerInfo.Create;
  vLayerInfoObject.Type_ := miLayerInfoTypeNewTable;
  vLayerInfoObject.AddParameter('FileSpec', aFileName);
  vLayerInfoObject.AddParameter('Name',     aFileName);
  vLayerInfoObject.AddParameter('Fields',   vFlds);
 // vLayerInfoObject.AddParameter('Features', aFeatures);

//  FMap.Layers.

  try
    Result:= FMap.Layers.Add(vLayerInfoObject, 1);
  except
    ShowMessage ('Error creating file: '+ aFileName);
    Result:=nil;
    Exit;
  end;

  OpenFile (aFileName);


end;

procedure TmiMap.CreateMemFromMapFields(aDataSet: TDataSet);
begin
  mapx_CreateMemFromMapFields(aDataSet, FieldArr);

//  Result := ;
end;


function TmiMap.GetActive: boolean;
begin
  Result := FMap.Layers.Count > 0;
end;


//------------------------------------------------------
function mapx_WritePoint_(aMap: TMap; aVLayer: CMapXLayer; aBLPoint: TBLPoint):
    CMapXFeature;
//------------------------------------------------------
var
  vFeature: CMapXFeature;
  p: CMapXPoint;
begin
  Assert(Assigned(aMap), 'FMap not assigned');
  Assert(Assigned(aVLayer), 'FMap not assigned');

  p := CoPoint.Create;
  p.Set_ (aBLPoint.L, aBLPoint.B);


  vFeature:=aMap.FeatureFactory.CreateSymbol(p, aMap.DefaultStyle);

  try
    aVLayer.BeginAccess(miAccessReadWrite);
    Result:=aVLayer.AddFeature (vFeature, EmptyParam);
  finally
    aVLayer.EndAccess(miAccessEnd);
  end;
end;         


//------------------------------------------------------
function mapx_WritePoly_(aMap: TMap; aVLayer: CMapXLayer; aBLPointArray:
    TBLPointArray; aColor: Integer = 0): CMapXFeature;

//------------------------------------------------------
var
  I: Integer;
  vFeature: CMapXFeature;
  ps: CMapXPoints;
begin
  Assert(Assigned(aMap), 'FMap not assigned');
  Assert(Assigned(aVLayer), 'FMap not assigned');

  ps := CoPoints.Create;

  for I := 0 to High(aBLPointArray) do
    ps.AddXY (aBLPointArray[i].L, aBLPointArray[i].B, EmptyParam);


  aMap.DefaultStyle.LineColor:= aColor;


  vFeature:=aMap.FeatureFactory.CreateLine(ps, aMap.DefaultStyle);

  try
    aVLayer.BeginAccess(miAccessReadWrite);

    Result:=aVLayer.AddFeature (vFeature, EmptyParam);
  finally
    aVLayer.EndAccess(miAccessEnd);
  end;
end;



//------------------------------------------------------
function mapx_WriteVector_(aMap: TMap; aVLayer: CMapXLayer; aBLVector:
    TBLVector): CMapXFeature;
    //; aParams: array of TmiParam
//------------------------------------------------------
var
 // vFeature: CMapXFeature;
 // ps: CMapXPoints;

  aBLPointArray: TBLPointArray;

begin
  SetLength(aBLPointArray, 2);

  aBLPointArray[0]:=aBLVector.Point1;
  aBLPointArray[1]:=aBLVector.Point2;

  Result := mapx_WritePoly_(aMap, aVLayer, aBLPointArray);


end;


//------------------------------------------------------
function mapx_WriteRegion_(aMap: TMap; aVLayer: CMapXLayer; aBLPointArray:
    TBLPointArray): CMapXFeature;
//------------------------------------------------------
var i:  integer;
    vFeature:  CMapXFeature;
    ps: CMapXPoints;
begin
  if Length(aBLPointArray)<3 then
  begin
//  if Length(aPoints)<3 then begin
    Result:=nil;
    Exit;
  end;

  vFeature:=aMap.FeatureFactory.CreateRegion(EmptyParam, EmptyParam);
  vFeature.Style:=aMap.DefaultStyle;

  ps := CoPoints.Create;
  for i:=0 to High(aBLPointArray) do //High(aPoints) do
    ps.AddXY (aBLPointArray[i].l, aBLPointArray[i].b, EmptyParam);

  vFeature.Parts.Add (ps);

  try
   // Assert(Assigned(VLayer), 'Value not assigned');

    aVLayer.BeginAccess(miAccessReadWrite);
    Result:=aVLayer.AddFeature (vFeature, EmptyParam);
  //  WriteFeatureFieldValues (Result, aParams);
  finally
    aVLayer.EndAccess(miAccessEnd);
  end;
end;



//--------------------------------------------------------------------
procedure mapx_DeleteFeatures(aVLayer: CMapXLayer; aCondition: string);
//--------------------------------------------------------------------
var
  i: integer;
  vFeatures: CMapXFeatures;
  vFeature: CMapXFeature;
begin
  vFeatures:=aVLayer.Search (aCondition, EmptyParam); //Format('%s = %d', [aFieldName, aID]));

  for i:=1 to vFeatures.Count do
  begin
    vFeature:=vFeatures.Item[i];

    aVLayer.DeleteFeature (vFeature);
  end;
end;



// ---------------------------------------------------------------
function mapx_Dataset_FieldExists(aCMapxDataset: CMapxDataset; aName: string): Boolean;
// ---------------------------------------------------------------
var
  k: Integer;
  s: string;
begin
   for k := 1 to aCMapxDataset.Fields.Count do
   begin
     s:=aCMapxDataset.Fields.Item[k].Name;


     if Eq(s,aName) then
     begin
       Result := True;
       Exit;
     end;
   end;

  Result := False;
end;





function mapx_RowValues_GetItemValue(aRowValues: CMapXRowValues; aName:
    string): Variant;
begin
  Assert(aRowValues<>nil);

  try

      Result := aRowValues.Item[aName].Value;

  except
    on E: Exception do ;
  end;

end;
 

//------------------------------------------------------
procedure mapx_WriteFeatureFieldValues(aVLayer: CMapXLayer; aCMapXDataset:
    CMapXDataset; aVFeature: CMapXFeature; aParams: array of TmiParam);
//------------------------------------------------------
var
  I: Integer;
  vRowValues: CMapXRowValues;

  rv: CMapXRowValue;
  s: string;
  v: Variant;
  vDS: CMapXDataset;
begin
  Assert(Assigned(aVFeature), 'aVFeature not assigned');
  Assert(Assigned(aVLayer), 'aVLayer not assigned');
  Assert(Assigned(aCMapXDataset), 'aCMapXDataset not assigned');

  i := aVLayer.Datasets.Count;

//  vDS:=mapx_GetDatasetByInd (aVLayer.Datasets, 1);

  vRowValues:=aCMapXDataset.RowValues[aVFeature];


  Assert(vRowValues<>nil);


  i :=vRowValues.Count;

  for I := 1 to vRowValues.Count  do
  begin

    v := vRowValues.Item[i].Value;

  end;



  for i := 1 to aCMapxDataset.Fields.Count do
  begin
     s:=aCMapxDataset.Fields.Item[i].Name;
   end;



  for i:=0 to High(aParams) do
    if not VarIsNull (aParams[i].Value) then
    begin
      Assert (mapx_Dataset_FieldExists (aCMapXDataset, aParams[i].FieldName));


      rv:=vRowValues.Item[aParams[i].FieldName];

    //  Assert(v<>null);

      rv.Value:=aParams[i].Value;
    end;

//    vFeature.
  aVFeature.Update (EmptyParam, vRowValues);
end;





begin

end.



