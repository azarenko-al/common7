unit d_MAP_MIF_Layers;

interface
{$I Mapx.inc}


//{$I ver.inc}

uses
  SysUtils, Classes, Controls, Forms,  cxGridCustomTableView, ActnList,
  cxGridDBTableView, cxGridCustomView, cxGridLevel,
  cxGrid, DB, dxmdaset, StdCtrls, ExtCtrls, ComCtrls,

  Variants,

  MapXLib_TLB,

//  MapXLib_TLB_4,

  d_Wizard,

  //u_const,
  //u_const_str,
  //u_const_db,

  u_MapX,

  u_db,

   ImgList, ToolWin, cxGridTableView, cxClasses, cxControls,
  cxPropertiesStore, rxPlacemnt;

type
  Tdlg_MAP_MIF_Layers = class(Tdlg_Wizard)
    pn_Main: TPanel;
    DataSource1: TDataSource;
    mem_Data: TdxMemData;
    act_Up: TAction;
    act_Down: TAction;
    act_Refresh: TAction;
    ImageList1: TImageList;
    act_Top: TAction;
    act_Bottom: TAction;
    cb_ImmediateRefresh: TCheckBox;
    cxGrid1: TcxGrid;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    col_Name1: TcxGridDBColumn;
    col_Visible1: TcxGridDBColumn;
    col_AutoLabel1: TcxGridDBColumn;
    col_index1: TcxGridDBColumn;
    col_FileName1: TcxGridDBColumn;
    ToolBar2: TToolBar;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
    procedure Action(Sender: TObject);
//    procedure dxDBGridExit(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
//    procedure dxDBGridHotTrackNode(Sender: TObject;
  //    AHotTrackInfo: TdxTreeListHotTrackInfo; var ACursor: TCursor);
  private
    FMap: TMap;

    procedure Load;
    procedure UpDateLayers;
  public
    class procedure ExecDlg (aMap: TMap);
  end;



//==================================================================
implementation {$R *.dfm}
//==================================================================

const
  FLD_NAME  = 'Name';
  FLD_FILENAME  = 'FileName';
  FLD_VISIBLE   = 'visible';
  FLD_AUTOLABEL = 'AutoLabel';
  FLD_INDEX     = 'index';

  FLD_MIF_LAYER   = 'MIF_LAYER';
  FLD_MIF_OVERLAP = 'MIF_OVERLAP';
  FLD_MIF_HALO    = 'MIF_HALO';


  FORM_Caption  = '��������� �����';

  STR_NAME     = '��������';
  STR_FILENAME = '����';



//--------------------------------------------------------------------
class procedure Tdlg_MAP_MIF_Layers.ExecDlg(aMap: TMap);
//--------------------------------------------------------------------
begin
  with Tdlg_Map_MIF_Layers.Create(Application) do
  try
    FMap:=aMap;
    Load();

    ShowModal;

  finally
    Free;
  end;

end;


//--------------------------------------------------------------------
procedure Tdlg_MAP_MIF_Layers.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;
  Caption:=FORM_CAPTION;

  cxGrid1.Align:=alClient;

//  dxDBGrid.Align:=alClient;

  db_CreateField(mem_Data,
                 [db_Field(FLD_ID,        ftInteger),
                  db_Field(FLD_NAME,      ftString),
                  db_Field(FLD_CAPTION,   ftString),
                  db_Field(FLD_FILENAME,  ftString),
                  db_Field(FLD_VISIBLE,   ftBoolean),
                  db_Field(FLD_AUTOLABEL, ftBoolean),
                  db_Field(FLD_INDEX,     ftInteger),
                  db_Field(FLD_DATA,     ftInteger),

                  db_Field(FLD_MIF_LAYER,     ftInteger)

{                  db_Field(FLD_MIF_OVERLAP,   ftBoolean),
                  db_Field(FLD_MIF_HALO,      ftBoolean)}
                 ]);


  col_Name1.Caption     :=STR_NAME;
  col_FileName1.Caption :=STR_FILENAME; //'����';
  col_Visible1.Caption  :='��������';
  col_AutoLabel1.Caption:='�������';

  pn_Main.Align:=alClient;

//  FRegPath:= REGISTRY_FORMS + ClassName +'\';

{  dx_CheckRegistry (dxDBGrid,  FRegPath + dxDBGrid.Name);
  dxDBGrid.LoadFromRegistry (FRegPath + dxDBGrid.Name);
}

  cxGrid1DBTableView1.RestoreFromRegistry (FRegPath + cxGrid1DBTableView1.Name);

end;



procedure Tdlg_MAP_MIF_Layers.FormDestroy(Sender: TObject);
begin
  cxGrid1DBTableView1.StoreToRegistry (FRegPath + cxGrid1DBTableView1.Name);

 /// dxDBGrid.SaveToRegistry (FRegPath + dxDBGrid.Name);

  inherited;
end;

//--------------------------------------------------------------------
procedure Tdlg_MAP_MIF_Layers.Load;
//--------------------------------------------------------------------
const
  STR_MAP_USER_LAYER_NAME = 'STR_MAP_USER_LAYER_NAME';

var i: integer;
  vLayer: CMapXLayer;
  sName: string;
  S: string;

begin
  db_Clear(mem_Data);

  for i:=1 to FMap.Layers.Count do
  //  with mem_Data do
  begin
    vLayer:= mapx_GetLayerByIndex_from_1(FMap, i);

(*      {$IFDEF Mapx5}
        vLayer:= FMap.Layers.Item[i];
      {$ELSE}
        vLayer:= FMap.Layers.Item(i);
      {$ENDIF}
*)
    //    vLayer:= FMap.Layers.Item(i);

 //   if Eq(vLayer.Name, STR_MAP_USER_LAYER_NAME) then
 //     Continue;

    if Pos('Temp layer', vLayer.Name) > 0 then
      sName:= ExtractFileName(vLayer.Filespec)
    else
      sName:= vLayer.Name;

    db_AddRecord_ (mem_Data,
                [
                 FLD_ID,        i,
                 FLD_INDEX,     i,
                 FLD_CAPTION,   sName,
                 FLD_NAME,      vLayer.Name,
                 FLD_FILENAME,  vLayer.Filespec,
                 FLD_VISIBLE,   vLayer.Visible,
                 FLD_AUTOLABEL, vLayer.AutoLabel,
                 FLD_DATA,      Integer(vLayer),
                 FLD_MIF_LAYER, Integer(vLayer)
{                 db_par(FLD_MIF_LAYER,   vLayer.),
                 db_par(FLD_MIF_OVERLAP, vLayer.),
                 db_par(FLD_MIF_HALO,    vLayer.)         }
                ]);
  end;

end;


//-----------------------------------------------------------
procedure Tdlg_MAP_MIF_Layers.UpDateLayers;
//-----------------------------------------------------------
var
  sName: string;
  iOldIndex: integer;
  iNewIndex: integer;
  sFileName: string;
  iIndex: integer;

  vLayer: CMapXLayer;

begin
  mem_Data.DisableControls;
  mem_Data.First;

  with mem_Data do
   while not Eof do
  begin
    sName :=FieldByName(FLD_NAME).AsString;

    iNewIndex:=FieldByName(FLD_INDEX).AsInteger;

    iOldIndex:=mapx_GetLayerIndexByName_from_1(FMap, sName);


    {$IFDEF Mapx5}
      vLayer:= FMap.Layers.Item [sName];
    {$ELSE}
      vLayer:= FMap.Layers.Item (sName);
    {$ENDIF}

    if not VarIsNull(vLayer) then
    begin
      if iNewIndex<>iOldIndex then
        FMap.Layers.Move (iOldIndex, iNewIndex);

      vLayer.Visible:= FieldByName(FLD_VISIBLE).AsBoolean;

      if vLayer.Type_ = miLayerTypeNormal then
        vLayer.AutoLabel:= FieldByName(FLD_AUTOLABEL).AsBoolean;

    end;


    Next;

  end;

  mem_Data.EnableControls;

end;



//-----------------------------------------------------------
procedure Tdlg_MAP_MIF_Layers.act_OkExecute(Sender: TObject);
//-----------------------------------------------------------
begin
  UpDateLayers();
end;


//-----------------------------------------------------------
procedure Tdlg_MAP_MIF_Layers.Action(Sender: TObject);
//-----------------------------------------------------------
var
  iID: integer;
  bDirectionIsUp: Boolean;
begin

  iID:=mem_Data.FieldByName(FLD_ID).AsInteger;

  //-----------------------------------------------
  if (Sender=act_Down) or (Sender=act_Up) then
  //-----------------------------------------------
  begin
    bDirectionIsUp:= (Sender=act_Up);

    db_MoveRecordUpDown (mem_Data, FLD_INDEX, bDirectionIsUp);
  end else

  //-----------------------------------------------
  if (Sender=act_Top) or (Sender=act_Bottom) then
  //-----------------------------------------------
  begin
    bDirectionIsUp:= (Sender=act_Top);

    db_MoveRecordTopBottom (mem_Data, FLD_INDEX, bDirectionIsUp);
  end;

  if cb_ImmediateRefresh.Checked then
    UpDateLayers();

  mem_Data.Locate(FLD_ID, iID, []);


end;

end.




{


//-----------------------------------------------------------
procedure Tdlg_MAP_MIF_Layers.dxDBGridExit(Sender: TObject);
//-----------------------------------------------------------
begin
  db_PostDataset (mem_Data);
end;


procedure Tdlg_MAP_MIF_Layers.dxDBGridHotTrackNode(Sender: TObject;
  AHotTrackInfo: TdxTreeListHotTrackInfo; var ACursor: TCursor);
begin
 // dx_ShowHint(dxDBGrid, AHotTrackInfo, [col_Name, col_FileName]);
  dx_ShowHint (dxDBGrid, AHotTrackInfo, [col_Name, col_FileName]);
end;




