unit d_MapX_Print;


interface

uses
  SysUtils, Classes, Controls, Forms,
  
  StdCtrls, cxVGrid,  cxPropertiesStore, rxToolEdit,

  MapXLib_TLB,
//  MapXLib_TLB_4,

  d_Wizard,

  d_MIF_export,

  u_func,
  u_mapx,
  u_files,
  u_cx_VGrid,
  u_Geo,

  Mask, cxControls, cxInplaceContainer, rxPlacemnt, ActnList,
  ExtCtrls;

type

  Tdlg_MapX_Print = class(Tdlg_Wizard)
    cxVerticalGrid1: TcxVerticalGrid;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    row_H: TcxEditorRow;
    row_W: TcxEditorRow;
    cxVerticalGrid1CategoryRow2: TcxCategoryRow;
    row_Scale: TcxEditorRow;
    ed_ImgFileName: TFilenameEdit;
    Label1: TLabel;
    cxVerticalGrid1EditorRow1: TcxEditorRow;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction;  var Handled: Boolean);
    procedure act_OkExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Print;
  private
    FRec: TExportMapRec;

    FMap: TMap ;
    FBLRect: TBLRect;

  public
    class procedure ExecDlg(aMap: TMap; aBLRect: TBLRect);
  end;

  



 // procedure Tdlg_MapX_Print_ExecDlg(aMap: TMap);

(*exports
   Tdlg_MapX_Print_ExecDlg;
*)

//==================================================================
implementation {$R *.DFM}
//==================================================================

(*
procedure Tdlg_MapX_Print_ExecDlg(aMap: TMap);
begin
  Tdlg_MapX_Print.ExecDlg(aMap);
end;
*)


//------------------------------------------------------------------
class procedure Tdlg_MapX_Print.ExecDlg(aMap: TMap; aBLRect: TBLRect);
//------------------------------------------------------------------
//var sType: string;
//  i: integer;
begin
  with Tdlg_MapX_Print.Create(Application) do
  try
    FMap:=aMap;

    ShowModal;

  {
    db_OpenTable (qry_Report, TBL_REPORT, [db_Par(FLD_CATEGORY, aMap)]);


    Result:=(ShowModal=mrOk);

    if Result then
    begin
      aReportSelectRec.ReportID         :=qry_Report[FLD_ID];
      aReportSelectRec.ReportFileName   :=qry_Report[FLD_FileName];

//      i:=cb_MapDesktop.KeyValue ;

 //     aReportSelectRec.MapDesktopID     :=qry_MapDesktop.FieldByName(FLD_ID).AsInteger;
      aReportSelectRec.MapDesktopID     :=cb_MapDesktop.KeyValue;


      aReportSelectRec.FileDir          :=ed_OutputDir.Text;
      aReportSelectRec.Scale            :=AsInteger (ReplaceStr(cb_Scale.Text,' ',''));
    end;}

  finally
    Free;
  end;
end;



//------------------------------------------------------------------
procedure Tdlg_MapX_Print.FormCreate(Sender: TObject);
//------------------------------------------------------------------
begin
  inherited;

  Caption:='������';

  SetActionName('������ ����� � ����... ');


  AddComponentProp(ed_ImgFileName, 'FileName');

  AddComponentProp(row_Scale, DEF_PropertiesValue);
  AddComponentProp(row_H,  DEF_PropertiesValue);
  AddComponentProp(row_W,  DEF_PropertiesValue);

  cxPropertiesStore.RestoreFrom;

  cx_InitVerticalGrid(cxVerticalGrid1);


  SetDefaultSize();

end;



//------------------------------------------------------------------
procedure Tdlg_MapX_Print.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//------------------------------------------------------------------
begin
//  act_Ok.Enabled:=not qry_Report.IsEmpty;

end;


procedure Tdlg_MapX_Print.act_OkExecute(Sender: TObject);
begin
  if Sender=act_Ok then
    Print

end;




//-------------------------------------------------------------------
procedure Tdlg_MapX_Print.Print;
//-------------------------------------------------------------------
var
  sTempFile: string;
begin

  FRec.Scale :=AsInteger (ReplaceStr(row_Scale.Properties.Value,' ',''));;
  FRec.Width :=row_W.Properties.Value;
  FRec.Height:=row_H.Properties.Value;

  FRec.BLRect:=FBLRect;


  FRec.Mode:=mtRect;


  sTempFile:= GetTempFileNameWithExt('gst');
 // sTempFile:='e:\1.gst';

  mapx_SaveWorkspace(FMap, sTempFile);


  FRec.GeosetFileName:=sTempFile;
  FRec.ImgFileName:=ed_ImgFileName.FileName;

  Tdlg_MIF_export.Execute(FRec);
//  MIF_ExportMap(FRec);

  DeleteFile(sTempFile);


end;



end.

