unit u_MapX;

interface
//{$I Mapx.Inc}

uses  SysUtils,Classes,Forms,Math,Graphics,Db,IniFiles,Registry,Variants,

  StrUtils,

  Dialogs,
  ActiveX, //OLE_COLOR

  MapXLib_TLB,


  u_log,

  u_assert,
//  u_mapX_lib,

  u_DB,
  u_files,
  u_func,
  u_Geo;

 // frxStorage;


type

  TmiFieldRec = packed record
    Name  : string;
    Size  : integer;
    Type_ : integer;
//    TypeStr : string;
    Value : Variant;
    IsIndexed : Boolean;
  end;

//  TmiFields = array of TmiFieldRec;
  TmiFieldArray = array of TmiFieldRec;


  TmiStyleRec = packed record
    LineColor            : integer;
    LineWidth            : integer;
    LineStyle            : integer;

    SymbolCharacter      : integer;
    SymbolFontColor      : integer;
    SymbolFontSize       : integer;
    SymbolFontName       : string;


    //Region Border
    RegionBorderWidth    : integer;
    RegionBorderStyle    : integer;
    RegionBorderColor    : integer;

    //Region fill
    RegionPattern        : integer;
    RegionForegroundColor: integer;  //���� ����� ��� �����
    RegionBackgroundColor: integer;
    RegionIsTransparent  : boolean;

  //
  //  IsFontHalo           : boolean;
    SymbolFontRotation   : integer;

    FontIsItalic         : Boolean;
    FontIsOpaque         : boolean;
    FontIsHalo           : boolean;
    FontBackColor        : integer;
    FontRotation         : integer;


    LabelProperties: packed record
      TextFontName      : string;
      TextFontSize      : integer;
      TextFontBold      : boolean;
      TextFontItalic    : boolean;

      TextFontColor     : integer;
      TextFontBackColor : integer;
      TextFontRotation  : integer;

      Overlap           : boolean;
      Parallel          : boolean;   //������������ ������� � ������

      TextFontShadow    : boolean;
      TextFontAllCaps   : boolean;
      TextFontDblSpace  : boolean;

      Position          : integer;
      Offset            : integer;

      LabelZoomMax      : integer;
      LabelZoomMin      : integer;
      LabelZoom         : boolean;

      FontIsOpaque      : boolean;
      FontIsHalo        : boolean;
    end;
  end;


  TmiMapCreateRec  = record
    Fields: TmiFieldArray; //array of TmiFieldRec;
    Datum:  integer;       //=1001;
  end;


  //-----------------------------------------------------
  TmiObjectRec = record
  //-----------------------------------------------------

     FeatureType  : integer;
     Point : TBLPoint;

     Parts :  array of record
                Points: TBLPointArray;
              end;

     Fields: TmiFieldArray;


  {   LineStyle    : record Color:integer; end;
     RegionStyle  : record Color:integer; end;
}   //  Bounds       : TBLRect;

  end;

  TmiParam = record
    FieldName : string;
    Value: Variant;
  end;

  TmiParamArray = array of TmiParam;

  TmiPolygon1 = class
    Points: TBLPointArray;//F
  end;



  function mapx_Par(aFieldName: string; aValue: Variant): TmiParam;
  function mapx_Field(aName: string; aType: integer; aSize: integer = 0;
      aIsIndexed: Boolean = false): TmiFieldRec;

  function Make_miFieldArray (aArr: array of TmiFieldRec): TmiFieldArray;
  function Make_miParamArray (aArr: array of TmiParam): TmiParamArray;

  function mapx_GetMapBounds(aMap: TMap): TBLRect;

//type

// IMapLibX = interface(IInterface)

//    procedure mapx_CreateFile   (aFileName: string; aFields: array of TmiFieldRec);

  function  mapx_MoveMap    (aSrcFileName, aDestFileName: string): Boolean;
  function  mapx_CopyMap    (aSrcFileName, aDestFileName: string): Boolean;
  procedure mapx_DeleteFile (aFileName: string);

//  function mapx_GetFileDate111111(aFileName: string): TDateTime;


  // -------------------------------------------------------------------
  // FieldName:
  // -------------------------------------------------------------------
  function mapx_GetFieldIndexByFieldName(aDataset: CMapXDataset; aFieldName:
      string): integer;

      //aDatasetIndex : Integer;

  function mapx_GetFieldValue(//aVLayer: CMapXLayer;
  aDataset: CMapXDataset;
      aVFeature: CMapXFeature; aFieldName: string): Variant;

  procedure mapx_SetFieldValue (aVLayer: CMapXLayer; aVFeature: CMapXFeature; aFieldName: string; aValue: Variant);

  function mapx_GetIntFieldValue(//aVLayer: CMapXLayer; //aDatasetIndex : Integer;
      aDataset: CMapXDataset;
      aVFeature: CMapXFeature; aFieldName: string): Integer;

  function mapx_GetStringFieldValue(//aVLayer: CMapXLayer; //aDatasetIndex : Integer;
      aDataset: CMapXDataset;
      aVFeature: CMapXFeature; aFieldName: string): string;

  function mapx_GetPlanBounds(aMap: TMap): TBLRect;

  function mapx_XRectangleToBLRect  (vBounds: CMapXRectangle): TBLRect;

  function mapx_FeatureTypeToStr    (aValue: integer): string;


//  function  mapx_GetDefaultColorOfPolygonMap (aFileName: string): integer;


  // -------------------------------------------------------------------
  // Layer
  // -------------------------------------------------------------------
  function mapx_GetLayerIndexByName_from_1(aMap: TMap; aName: string): integer;
  function mapx_GetLayerIndexByFileName_from_1(aMap: TMap; aFileName: string):
      integer;

  function mapx_GetLayerByIndex_from_0(aMap: TMap; aIndex: integer): CMapXLayer;



  function mapx_GetLayerByFileName (aMap: TMap; aFileName: string): CMapXLayer;
  function mapx_GetLayerByName(aMap: TMap; aName: string): CMapXLayer;


  procedure mapx_ReOpenMapByFileName(aMap: TMap; aFileName: string);


  // -------------------------------------------------------------------
  // TmiStyle;
  // -------------------------------------------------------------------
  procedure mapx_FontStyle_SaveToReg1(aMIStyle: TmiStyleRec; aRegPath: string);
  procedure mapx_LabelStyle_LoadFromReg (var aMIStyle: TmiStyleRec; aRegPath: string);

//  procedure mapx_LabelStyle_SaveToReg1111(aMIStyle: TmiStyleRec; aRegPath:
//      string);


// TODO: mapx_CreateTempLayerWithFeatures_1111
//function mapx_CreateTempLayerWithFeatures_1111(aMap: TMap; aLayerName: string;
//    aRec: TmiMapCreateRec; aFeatures: CMapxFeatures): CMapXLayer;

  procedure mapx_AddBezierToLayer(aMap : TMap; aVLayer: CMapXLayer; var
      aBlPoints: TBLPointArrayF; aColor: integer; aWidth: integer);

  function mapx_GetScaleStr(aMap: TMap): string;

  procedure mapx_AddLineToLayer  (aMap: TMap;
                                  aVLayer: CMapXLayer;
                                  aBlVector: TBLVector;
                                  aColor: integer;
                                  aStyle: integer = -1;
                                  aWidth: integer = 1;
                                  aLabelFieldName: string = '';
                                  aLabel: string = '');

  procedure mapx_SetBounds            (aMap: TMap; aBLRect: TBLRect);
  procedure mapx_SetBoundsWithOffset  (aMap: TMap; aBLRect: TBLRect);
  procedure mapx_SetBoundsForBLVector (aMap: TMap; aBLVector: TBLVector);
  procedure mapx_SetScale             (aMap: TMap; aScale: double);
  function  mapx_GetScale             (aMap: TMap): double;

  procedure mapx_SaveWorkspace        (aMap: TMap; aFileName: string);
  function  mapx_LoadWorkspace        (aMap: TMap; aFileName: string): boolean;


  procedure mapx_ClearSelection(aMap: TMap);

  procedure mapx_Dlg_PrintToFile (aMap: TMap);

  function mapx_GetDefaultColorOfLayer(aLayer: CMapXLayer; var aColor: integer): Boolean;

  function mapx_GetLayerPlanBounds(aVLayer: CMapXLayer): TBLRect;

  function mapx_GetFeatureBounds(aVFeature: CMapXFeature): TBLRect;

  procedure mapx_GetFeatureParts(aVFeature: CMapXFeature; var aMIObjectRec:
      TmiObjectRec);

  procedure mapx_GetMapLayerFields(aMapXLayer: CMapXLayer; var aFields:
      TmiFieldArray);

  //  function  mapx_CreateMemFromMapFields(aMapFileName: string; aDataSet: TDataSet): boolean; overload;
    procedure mapx_CreateMemFromMapFields(aDataSet: TDataSet; var aFields:  TmiFieldArray); //overload;

  function mapx_GetDatasetByName(aVLayer: CMapXLayer; aName: string):
      CMapXDataset;

  function mapx_PrepareStyle(aMap: TMap; aFeatureType: integer; var aStyle: TmiStyleRec): boolean;

//  function mapX_GetFeatureTypeStr(aFeatureType: Integer): string;

  procedure mapx_CopyDatasetFieldsToMiFields(aDataSet: TDataSet; var
      aMiFieldArray: TmiFieldArray);

//  procedure mapx_SaveXYRectFrameToMIF(aXYRect: TXYRect; aTabFileName: string;
//      aZone: Integer = 0);


function mapx_GetDatasetByIndex(aCMapXDatasets: CMapXDatasets; aIndex: integer):
    CMapXDataset;

//procedure mapx_MakeLayerTransparent111111111__________(aLayer: CMapXLayer);

// TODO: mapx_GetLayerIndex111111111111
//function mapx_GetLayerIndex111111111111(aMap: TMap; aLayer: CMapXLayer):
//  integer;

procedure mapx_AddLineToLayer_(aMap: TMap; aLayerName: string; aBlVector: TBLVector;
    aColor: integer);

procedure mapx_DeleteLayerFeatures(aMap: TMap; aLayerName: string);

procedure mapx_SetCenter(aMap: TMap; aBLPoint: TBLPoint);



procedure mapx_Set_Width_Height(aMap: TMap; aWidth,aHeight: Integer);




procedure mapx_SetZoom(aMap: TMap; aZoomInCm: Integer);
procedure mapx_SetZoomPlan(aMap: TMap);

function mapx_GetZoomByScale(aMap: TMap; aZoomInCm: Integer): Double;

procedure mapx_SetCustomZoomDlg(aMap: TMap);

function mapx_SetMapXLayertStyle(aCMapXLayer: CMapXLayer; aFeatureType:
    integer; aStyle: TmiStyleRec): boolean;

function mapx_WritePolygon(aMap: TMap; aVLayer: CMapXLayer; aBLPoints:
    TBLPointArray; aColor, aStyle, aWidth: Integer; aLabel: string):
    CMapXFeature;

function mapx_DeleteByCondition(aCMapXLayer: CMapXLayer; aCondition: string):
    Integer;


function mapx_IsFieldExists(aDS: CMapxDataset; aFileName: string): Boolean;

function mapx_GetLayerIndex_from_1(aMap: TMap; aLayer: CMapXLayer): integer;

function mapx_GetLayerByIndex_from_1(aMap: TMap; aIndex: integer): CMapXLayer;

function mapx_Show_Layers_Dlg(aMap: TMap): CMapXLayer;

procedure mapx_SetDefaultCoordSys(aMap: TMap);

procedure mapx_SetBoundsFromLayer(aMap: TMap; aLayer: CMapXLayer);

//function mapx_GetFileModifyDate______(aFilename: string): TDateTime;

function mapx_GetTransparence_0_100(aTabFileName: string): Integer;

procedure mapx_SetTransparence_0_100(aTabFileName: string; aValue: Integer);

function mapx_MapHasRaster(aMap: TMap): Boolean;

function mapx_GetLayerBounds(aCMapXLayer: CMapXLayer): TBLRect;

function mapx_AddMap(aMap: TMap; aFileName : string): CMapXLayer;

procedure mapx_RemoveMapFile(aMap: TMap; aFileName : string);


//procedure mapx_SetMapDefaultProjection_DisplayCoordSys_KRASOVKSY42(aMap: TMap);

function mapx_GetLayerIndexByMask_from_1(aMap: TMap; aMask: string): integer;

function mapx_GetCenter(aMap: TMap): TBLPoint;

function mapx_MapAdd_Ex(aMap: TMap; aFileName: string): CMapXLayer;

function mapx_CreateTempLayerWithFeatures(aMap: TMap; aLayerName: string; aRec:
    TmiMapCreateRec; aFeatures: CMapxFeatures; aIndex: Integer=1;
    aAutoCreateDataset: boolean = false): CMapXLayer;

function mapx_CreateFields(var aInFields: TmiFieldArray; aCreateIndexOnID:
    boolean = false): CMapXFields;

procedure map_Tile_Align_Back(aMap: TMap);

function mapx_CMapXCoordSys_to_str(aCMapXCoordSys: CMapXCoordSys): string;

function mapx_FileExists(aMap: TMap; aFileName: string): boolean; overload;
function mapx_FileExists(aMap: CMapX; aFileName: string): boolean; overload;

function mapx_GetLayerIndexByFileName_CMapX(aMap: CMapX; aFileName: string):
    integer;

procedure mapx_TilesClear(aMap: CMapX); overload;
procedure mapx_TilesClear(aMap: TMap); overload;

procedure mapx_SetBounds_CMapX(aMap: CMapX; aBLRect: TBLRect);

procedure mapx_SetMapDefaultProjection_NumericCoordSys_KRASOVKSY42_WMS(aMap: TMap);
procedure mapx_SetMapDefaultProjection_NumericCoordSys_KRASOVKSY42_simple(aMap: TMap);

procedure mapx_SetMapDefaultProjection_NumericCoordSys_KRASOVKSY42_simple_(aMap:    CMapX);
procedure mapx_SetMapDefaultProjection_NumericCoordSys_KRASOVKSY42_WMS_(aMap: CMapX);

procedure mapx_SetBoundsForBLVector_for_print(aMap: TMap; aBLVector: TBLVector);


(*function mapx_GetDatasetByIndex(aCMapXDatasets: CMapXDatasets; aIndex: integer):
    CMapXDataset;

*)


const
  FLD_ID = 'ID';


//  MI_SYMBOL_STAR = 36;

  MI_BRUSH_PATTERN_BLANK = 1;
  MI_BRUSH_PATTERN_SOLID = 2;
  MI_BRUSH_PATTERN_DOTS  = 16;
  MI_BRUSH_PATTERN_RARE_DOTS  = 53;

  // pen pattern styles
  miPenNone  = 0;
  miPenSolid = 1;

  MI_PEN_PATTERN_BLANK   = 1;
  MI_PEN_PATTERN_SOLId   = 2;
  MI_PEN_PATTERN_DASH    = 8;
  MI_PEN_PATTERN_ARROW   = 59;

  //region border style
  MI_PEN_NONE  = 0;
  MI_PEN_SOLID = 1;

  MI_FONT_MAPINFO_SYMBOLS= 'MapInfo Symbols';
  MI_SYMBOL_STAR = 36;

  MI_FONT_Transportation = 'MapInfo Transportation';
  MI_FONT_Cartographic   = 'MapInfo Cartographic';


const
  miUnitKilometer  = 1;
  miUnitCentimeter = $00000006;


  miFormatWMF = $00000000;
  miFormatBMP = $00000001;
  miFormatGIF = $00000002;
  miFormatJPEG = $00000003;
  miFormatTIF = $00000004;
  miFormatPNG = $00000005;
  miFormatPSD = $00000006;


  // text position
  miPositionCC = 0;
  miPositionTL = 1;
  miPositionTC = 2;
  miPositionTR = 3;
  miPositionCL = 4;
  miPositionCR = 5;
  miPositionBL = 6;
  miPositionBC = 7;
  miPositionBR = 8;

  // feature types
  miFeatureTypeRegion  = 0;
  miFeatureTypeLine    = 1;
  miFeatureTypeSymbol  = 2;
  miFeatureTypeMixed   = 3;
  miFeatureTypeUnknown = 4;
  miFeatureTypeText    = 5;
  miFeatureTypeNull    = 6;

  // field types
  miTypeString   = 0;
  miTypeNumeric  = 1;
  miTypeDate     = 2;
  miTypeInt      = 3;
  miTypeSmallInt = 4;
  miTypeFloat    = 5;
  miTypeLogical  = 6;

  //miPattern
  miPatternNoFill  = 0;
  miPatternHollow = 1;
  miPatternSolid = 2;
  miPatternHorizontal = 3;
  miPatternVertical = 4;
  miPatternFDiag = 5;
  miPatternFilBDiag = 6;
  miPatternCross = 7;
  miPatternDiagCross = 8;


const
  miLayerInfoTypeNewTable = 7;
  miLayerInfoTypeTemp = 6;


const
  DATUM_KRASOVKSY42 = 1001;
  DATUM_WGS84       = 104; //28;



//====================================================================
implementation
//====================================================================
//
//uses
//  u_mitab;//, frxStorage;



const
  FLD_COMMENTS ='COMMENTS';


// ---------------------------------------------------------------
function mapx_GetDatasetByName(aVLayer: CMapXLayer; aName: string):
    CMapXDataset;
// ---------------------------------------------------------------
var
  i: Integer;
  s: string;
  vDS: CMapXDataset;
begin
  result :=nil;

  i :=aVLayer.Datasets.Count;

  for I := 1 to aVLayer.Datasets.Count  do
  begin
 
    vDS :=aVLayer.Datasets.Item[i];


    s:=vDS.Name;

    if Eq(s, aName) then
    begin
      result := vDS;
      exit;
    end;

  end;
end;


function mapx_Par(aFieldName: string; aValue: Variant): TmiParam;
begin
  Result.FieldName:=aFieldName;
  Result.Value:=aValue;
end;


function mapx_Field(aName: string; aType: integer; aSize: integer = 0;
    aIsIndexed: Boolean = false): TmiFieldRec;
begin
  with Result do
  begin
    Name :=aName;
    Type_:=aType;
    Size :=aSize;
    IsIndexed  := aIsIndexed;
  end;
end;


function Make_miFieldArray (aArr: array of TmiFieldRec): TmiFieldArray;
var  i: integer;
begin
  SetLength(Result, Length(aArr));
  for i:=0 to High(aArr) do
    Result[i]:=aArr[i];
end;


function Make_miParamArray (aArr: array of TmiParam): TmiParamArray;
var  i: integer;
begin
  SetLength(Result, Length(aArr));
  for i:=0 to High(aArr) do
    Result[i]:=aArr[i];
end;



//--------------------------------------------------------------------
procedure mapx_AddLineToLayer  (aMap: TMap;
                                aVLayer: CMapXLayer;
                                aBlVector: TBLVector;
                                aColor: integer;
                                aStyle: integer = -1;
                                aWidth: integer = 1;
                                aLabelFieldName: string = '';
                                aLabel: string = '');
//--------------------------------------------------------------------
var
  i: Integer;
  ps: CMapXPoints;
  f, f1: CMapXFeature;
  rv: CMapXRowValues;
  ds: CMapxDataSets;
  ds1: CMapxDataSet;
begin
  Assert (aMap<>nil);
  Assert (aVLayer<>nil);
  Assert (aBlVector.Point1.B>0);

  ps := CoPoints.Create;

  with aBlVector do
  begin
    ps.AddXY (Point1.L, Point1.B, EmptyParam);
    ps.AddXY (Point2.L, Point2.B, EmptyParam);
  end;

  if aStyle > -1 then
    aMap.DefaultStyle.LineStyle:= aStyle
  else
    aMap.DefaultStyle.LineStyle:= 1;


  aMap.DefaultStyle.LineColor:= aColor;
  aMap.DefaultStyle.LineWidth:= aWidth;

  f:= aMap.FeatureFactory.CreateLine (ps, aMap.DefaultStyle);

  f1:= aVLayer.AddFeature (f, EmptyParam);

  if (aLabel <> '')and (aLabelFieldName<>'') then
  begin
      ds:=  avLayer.Datasets;

     
        ds1:= avLayer.Datasets.Item[1];
      //  ds1:= mapx_CMapXDatasetByIndex(avLayer.Datasets, 1);

        rv:=avLayer.Datasets.Item[1].RowValues[f1];
        rv.Item[aLabelFieldName].Value:= aLabel;


//      rv.Item[FLD_NAME].Value:= aLabel;

      f1.Update (EmptyParam, rv);

      avLayer.AutoLabel:= True;
  end;
end;



//----------------------------------------------------------------------
function ShowSaveDialog1(AOwner: TComponent; aTitle,aDefaultExt,aFilter,
    aFileName:string): string;
//----------------------------------------------------------------------
var SaveDialog1: TSaveDialog;
begin
   SaveDialog1:=TSaveDialog.Create (AOwner);
   SaveDialog1.Title     :=aTitle;
   SaveDialog1.DefaultExt:=aDefaultExt;
   SaveDialog1.Filter    :=aFilter;
   SaveDialog1.FileName  :=aFileName;

   if SaveDialog1.Execute then Result:=SaveDialog1.FileName
                          else Result:='';
   SaveDialog1.Free;
end;

//---------------------------------------------------------------------------
procedure mapx_Dlg_PrintToFile (aMap: TMap);
//---------------------------------------------------------------------------
var
  sFile: string;
  dWidth,dHeight: double;
begin
{  Map1.PrintMap(iFileHandle, Trunc(iScreenX1), Trunc(iScreenY1),
                Trunc(iScreenX2 - iScreenX1), Trunc(iScreenY2 - iScreenY1));
 }

  sFile:=ShowSaveDialog1 (nil, '���������� �����', GraphicExtension(TGraphic),
                                    GraphicFilter(TGraphic), 'map.jpg');
  if sFile<>'' then
  begin
    dWidth := aMap.Width  / (Screen.PixelsPerInch / 2.54);
    dHeight:= aMap.Height / (Screen.PixelsPerInch / 2.54);

    aMap.ExportSelection := true;
    aMap.PaperUnit:= miUnitCentimeter;

    sFile := ChangeFileExt(sFile, '.jpg');

    aMap.ExportMap(sFile, miFormatJPEG, dWidth, dHeight);
  end;

end;

//--------------------------------------------------------------------
function mapx_GetMapBounds(aMap: TMap): TBLRect;
//--------------------------------------------------------------------
begin
  Result:=mapx_XRectangleToBLRect(aMap.Bounds);
end;


//--------------------------------------------------------------------
function mapx_GetLayerBounds(aCMapXLayer: CMapXLayer): TBLRect;
//--------------------------------------------------------------------
begin
  Result:=mapx_XRectangleToBLRect(aCMapXLayer.Bounds);
end;




// -------------------------------------------------------------------
function mapx_XRectangleToBLRect (vBounds: CMapXRectangle): TBLRect;
// -------------------------------------------------------------------
begin
  Result.TopLeft.B:=Max (vBounds.YMax, vBounds.YMin);
  Result.TopLeft.L:=Min (vBounds.XMin, vBounds.XMax);
  Result.BottomRight.B:=Min (vBounds.YMax, vBounds.YMin);
  Result.BottomRight.L:=Max (vBounds.XMin, vBounds.XMax);


  if (Trunc(Result.TopLeft.B)=0) and
     (Trunc(Result.TopLeft.L)=0) and
     (Trunc(Result.BottomRight.B)=0) and
     (Trunc(Result.BottomRight.L)=0)
  then
    Result:=NULL_BLRECT();
end;

//------------------------------------------------------
procedure mapx_DeleteFile (aFileName: string);
//------------------------------------------------------
begin
  SysUtils.DeleteFile (ChangeFileExt (aFileName, '.dat'));
  SysUtils.DeleteFile (ChangeFileExt (aFileName, '.id'));
  SysUtils.DeleteFile (ChangeFileExt (aFileName, '.map'));
  SysUtils.DeleteFile (ChangeFileExt (aFileName, '.tab'));
end;

//------------------------------------------------------
function mapx_MoveMap(aSrcFileName, aDestFileName: string): Boolean;
//------------------------------------------------------
begin
  MoveFile(ChangeFileExt(aSrcFileName, '.dat'),  ChangeFileExt(aDestFileName, '.dat'));
  MoveFile(ChangeFileExt(aSrcFileName, '.id'),   ChangeFileExt(aDestFileName, '.id'));
  MoveFile(ChangeFileExt(aSrcFileName, '.ind'),  ChangeFileExt(aDestFileName, '.ind'));
  MoveFile(ChangeFileExt(aSrcFileName, '.map'),  ChangeFileExt(aDestFileName, '.map'));
  MoveFile(ChangeFileExt(aSrcFileName, '.tab'),  ChangeFileExt(aDestFileName, '.tab'));


end;

//------------------------------------------------------
function mapx_CopyMap(aSrcFileName, aDestFileName: string): Boolean;
//------------------------------------------------------
begin
  Assert(FileExists(ChangeFileExt(aSrcFileName, '.tab')));


  FileCopy(ChangeFileExt(aSrcFileName, '.dat'),  ChangeFileExt(aDestFileName, '.dat'));
  FileCopy(ChangeFileExt(aSrcFileName, '.id'),   ChangeFileExt(aDestFileName, '.id'));
  FileCopy(ChangeFileExt(aSrcFileName, '.ind'),  ChangeFileExt(aDestFileName, '.ind'));
  FileCopy(ChangeFileExt(aSrcFileName, '.map'),  ChangeFileExt(aDestFileName, '.map'));
  FileCopy(ChangeFileExt(aSrcFileName, '.tab'),  ChangeFileExt(aDestFileName, '.tab'));
end;

//------------------------------------------------------
function mapx_GetIntFieldValue(//aVLayer: CMapXLayer; //aDatasetIndex : Integer;
  aDataset: CMapXDataset;
    aVFeature: CMapXFeature; aFieldName: string): Integer;
//------------------------------------------------------
var v: Variant;
begin
  v:=mapx_GetFieldValue (//aVLayer, //aDatasetIndex,
  aDataset,
  aVFeature, aFieldName);

  if VarExists(v) then Result:=v
                  else Result:=0;
end;

//------------------------------------------------------
function mapx_GetStringFieldValue(//aVLayer: CMapXLayer;
   aDataset: CMapXDataset;
    aVFeature: CMapXFeature; aFieldName: string): string;
//------------------------------------------------------
var v: Variant;                                        //aDatasetIndex : Integer;
begin
  v:=mapx_GetFieldValue (//aVLayer,//aDatasetIndex,
   aDataset,
   aVFeature, aFieldName);

  if VarExists(v) then Result:=v
                  else Result:='';
end;

//------------------------------------------------------
function mapx_GetFieldValue(//aVLayer: CMapXLayer;
aDataset: CMapXDataset;
    aVFeature: CMapXFeature; aFieldName: string): Variant;
//------------------------------------------------------
//aDatasetIndex : Integer;
var i: integer;
  iCount: Integer;
    rv: CMapXRowValues;
    v: variant;
  //  vDataset: CMapXDataset;
begin
  Result:=null;

//  iCount:=aVLayer.Datasets.Count;

//  assert(aDatasetIndex<=aVLayer.Datasets.Count, aVLayer.Name + ' ' + IntToStr(aVLayer.Datasets.Count));

//  if aDatasetIndex <= aVLayer.Datasets.Count  then

//  Assert( aVLayer.Datasets.Count = 1);

//  vDataset := aVLayer.Datasets.Item[aDatasetIndex];

//  vDataset := aVLayer.Datasets.Item[1];


  if mapx_GetFieldIndexByFieldName (aDataset, aFieldName) > 0 then
  try

      rv:=aDataset.RowValues[aVFeature];
      v :=aDataset.Fields.Item[aFieldName];
      Result:=rv.Item[aFieldName].Value;


//    if VarIsNull(Result) then Result:='';
  except
  end;

end;






//------------------------------------------------------
procedure mapx_SetFieldValue(aVLayer: CMapXLayer; aVFeature: CMapXFeature;
    aFieldName: string; aValue: Variant);
//------------------------------------------------------
var rvs: CMapXRowValues;
    v: variant;
    i: integer;
    rv: CMapXRowValue;
begin

    rvs:=aVLayer.Datasets.Item[1].RowValues[aVFeature];


  Assert(rvs<>nil);

//  for i:=0 to High(aParams) do
 //   if not VarIsNull (aParams[i].Value) then
  //  begin


    rv:=rvs.Item[aFieldName];



  Assert(v<>null);
  rv.Value:=aValue;
   // end;

  aVFeature.Update (EmptyParam, rvs);
end;

//------------------------------------------------------
function mapx_GetFieldIndexByFieldName(aDataset: CMapXDataset; aFieldName:
    string): integer;
//------------------------------------------------------
var i: integer;
  iCount: Integer;
  sName: string;
  vField: CMapXField;
begin
  Result:=-1;

 // if aVLayer.Datasets.Count >0 then

  iCount:=aDataset.Fields.Count;

  for i:=1 to aDataset.Fields.Count do
  begin

    vField:=aDataset.Fields.Item[i];


    sName:=vField.Name;

    if Eq (sName, aFieldName) then
    begin
      Result:=i;
      Exit;
    end;
  end;


end;


// -------------------------------------------------------------------
procedure mapx_SetCustomZoomDlg(aMap: TMap);
// -------------------------------------------------------------------
var
  sValue: string;
  iScale: integer;
begin
  sValue := '100 000';
  if not InputQuery ('�������', '������� ������� ����� - 1: ...', sValue) then
    Exit;

  sValue:=ReplaceStr(sValue, ' ', '');

  //Ffrm_MapView.SetScale(200000 div 100)

  iScale:=AsInteger(sValue)  div 100 ;

  if iScale>0 then
    mapx_SetScale(aMap, iScale);
//    SetZoom(iScale);
end;

// aZoomInCm: Integer


function mapx_GetZoomByScale(aMap: TMap; aZoomInCm: Integer): Double;
begin
  Result:= aMap.MapPaperWidth * aZoomInCm / 100000;
end;


procedure mapx_SetZoom(aMap: TMap; aZoomInCm: Integer);
begin
  aMap.Zoom:= mapx_GetZoomByScale(aMap, aZoomInCm);
end;


function mapx_GetCenter(aMap: TMap): TBLPoint;
begin
  result.B := aMap.CenterX;
  result.L := aMap.CenterY;
end;




//--------------------------------------------------------------------
procedure mapx_SetCenter(aMap: TMap; aBLPoint: TBLPoint);
//--------------------------------------------------------------------
var
  eZoom: Double;
begin
  Assert(Assigned(aMap), 'Value not assigned');

  eZoom:=aMap.Zoom;

  if eZoom>2000 then
    aMap.Zoom:=2000;

  if eZoom=0 then
    aMap.Zoom:=1;

  aMap.ZoomTo (aMap.Zoom, aBLPoint.L, aBLPoint.B);
end;


//------------------------------------------------------
function mapx_GetLayerIndexByName_from_1(aMap: TMap; aName: string): integer;
//------------------------------------------------------
var i: integer;
  s: string;
  sName: string;
  vLayer: CMapXLayer;
begin
  Assert(Assigned(aMap), 'Value not assigned');


  Result:=-1;

  for i:=1 to aMap.Layers.Count do
  begin

      vLayer:=aMap.Layers.Item[i];

    s:=vLayer.Name;

    if Eq(vLayer.Name, aName) then
    begin
      Result:=i;
      Exit;
    end;
  end;
end;

//--------------------------------------------------------------------
function mapx_GetPlanBounds(aMap: TMap): TBLRect;
//--------------------------------------------------------------------
var i,iCount: integer;
    blRects: TBLRectArray;
  j: Integer;
  vLayer: CMapXLayer;
  blRect: TBLRect;
  sFileName: string;
  sName: string;
begin
  Assert(Assigned(aMap), 'Value not assigned');


  iCount:=aMap.Layers.Count;
  SetLEngth (blRects, iCount);

  for i:=1 to aMap.Layers.Count do
  begin
    vLayer:=mapx_GetLayerByIndex_from_1(aMap, i);

    sName:=vLayer.Name;
    sFileName:=vLayer.Filespec;

 //   blRects[i-1]:= mapx_GetLayerPlanBounds(vLayer)


  //  blRect := mapx_GetLayerPlanBounds(vLayer);

  //  j:=vLayer.AllFeatures.Count;

//    j:=vLayer.AllFeatures.Count;


(*       //check if map is system objects
       if (not vLayer.Visible) or
          (vLayer.Type_<>miLayerTypeNormal)
       then Continue;

*)
    if vLayer.Visible and
//      (vLayer.Type_ = miLayerTypeNormal)

      (vLayer.Type_ in [miLayerInfoTypeTab, miLayerInfoTypeRaster])
      then
//                                      miLayerInfoTypeTab,miLayerInfoTypeRaster


//    (vLayer.AllFeatures.Count>0) then
 //   if vLayer.Visible then
      blRects[i-1]:= mapx_GetLayerPlanBounds(vLayer)
    else
      blRects[i-1]:=NULL_BLRECT();


  end;

  Result:=geo_GetRoundBLRect(blRects);
end;

//--------------------------------------------------------------------
function mapx_MapHasRaster(aMap: TMap): Boolean;
//--------------------------------------------------------------------
var
  i,iCount: integer;

  vLayer: CMapXLayer;

  sFileName: string;

begin
  Result:=False;

  iCount:=aMap.Layers.Count;


  for i:=1 to aMap.Layers.Count do
  begin
    vLayer:=aMap.Layers.Item[i];

    sFileName:=vLayer.Filespec;

    if (vLayer.Type_ in [miLayerInfoTypeRaster]) then
    begin
      Result:=True;
      Break;
    end;
  end;
end;




//--------------------------------------------------------------------
function mapx_GetLayerPlanBounds(aVLayer: CMapXLayer): TBLRect;
//--------------------------------------------------------------------
var
  sFile: string;
begin
  Assert(Assigned(aVLayer), 'Value not assigned');

  if aVLayer.type_ in [miLayerInfoTypeTab, miLayerInfoTypeRaster] then
  begin
    Result:=mapx_XRectangleToBLRect (aVLayer.Bounds);

    if Result.BottomRight.b < 5 then
      if Pos('~',aVLayer.FileSpec)>0 then
    begin
      sFile:= ChangeFileExt(aVLayer.FileSpec, 'ini');

      with TIniFile.Create(sFile) do
      begin
        Result.TopLeft.B    := ReadFloat('main','MAX_LAT',Result.TopLeft.B);
        Result.TopLeft.L    := ReadFloat('main','MIN_LON',Result.TopLeft.L);
        Result.BottomRight.B:= ReadFloat('main','MIN_LAT',Result.BottomRight.B);
        Result.BottomRight.L:= ReadFloat('main','MAX_LON',Result.BottomRight.L);

      end;

    end;    

  end
  else
    Result:=NULL_BLRECT();

end;


// ---------------------------------------------------------------
function mapx_FeatureTypeToStr(aValue: integer ): string;
// ---------------------------------------------------------------
begin
  case aValue of
    miFeatureTypeRegion  : result:='Region';
    miFeatureTypeLine    : result:='Line';
    miFeatureTypeSymbol  : result:='Symbol';
    miFeatureTypeMixed   : result:='';
    miFeatureTypeUnknown : result:='';
    miFeatureTypeText    : result:='Text';
    miFeatureTypeNull    : result:='';
  end;
end;




//--------------------------------------------------------------------
procedure mapx_DeleteLayerFeatures(aMap: TMap; aLayerName: string);
//--------------------------------------------------------------------
var
  vLayer: CMapXLayer;
  vFeature: CMapXFeature;
  i: Integer;
begin
  Assert(Assigned(aMap), 'Value not assigned');


  vLayer:=mapx_GetLayerByName(aMap, aLayerName);

//  vLayer:= GetLayerByName(aLayerName);
  if not Assigned(vLayer) then
    exit;

  vLayer.BeginAccess(miAccessReadWrite);

  for i:= vLayer.AllFeatures.Count downto 1 do
  begin

      vFeature :=vLayer.AllFeatures.Item[i];

    vLayer.DeleteFeature(vFeature);
  end;

  vLayer.EndAccess(miAccessEnd);
end;

//--------------------------------------------------------------------
procedure mapx_AddLineToLayer_(aMap: TMap; aLayerName: string; aBlVector: TBLVector;
    aColor: integer);
//--------------------------------------------------------------------
var
  vLayer: CMapXLayer;
  i: Integer;
  ps: CMapXPoints;
  vFeature: CMapXFeature;
begin
  Assert(Assigned(aMap), 'Value not assigned');


  vLayer:=mapx_GetLayerByName(aMap, aLayerName);

//  GetLayerByName(aName);
  if not Assigned(vLayer) then
    exit;

  ps := CoPoints.Create;

  with aBlVector do
  begin
    ps.AddXY (Point1.L, Point1.B, EmptyParam);
    ps.AddXY (Point2.L, Point2.B, EmptyParam);
  end;

  aMap.DefaultStyle.LineColor:= aColor;

  vFeature:= aMap.FeatureFactory.CreateLine (ps, aMap.DefaultStyle);
  vLayer.AddFeature (vFeature, EmptyParam);
end;


//--------------------------------------------------------------------
procedure mapx_SetBounds(aMap: TMap; aBLRect: TBLRect);
//--------------------------------------------------------------------
var
  vMapXRectangle: CMapXRectangle;
  e: Double;
begin
  Assert(Assigned(aMap), 'Value not assigned');


  if (aBLRect.TopLeft.B=0)  and (aBLRect.TopLeft.L=0) then
    Exit;

  vMapXRectangle := CoRectangle.Create;

  with aBLRect do
    vMapXRectangle.Set_(TopLeft.L, TopLeft.B,  BottomRight.L, BottomRight.B);

  aMap.Bounds := vMapXRectangle;

  e:=aMap.Zoom;

  if aMap.Zoom=0 then
    aMap.Zoom :=1;
                    
  aMap.Refresh;
end;


//--------------------------------------------------------------------
procedure mapx_SetBoundsFromLayer(aMap: TMap; aLayer: CMapXLayer);
//--------------------------------------------------------------------
var
  vMapXRectangle: CMapXRectangle;
//  e: Double;
begin
  Assert(Assigned(aMap), 'Value not assigned');
  Assert(Assigned(aLayer), 'Value not assigned');

  vMapXRectangle := aLayer.Bounds;

//  if (aBLRect.TopLeft.B=0)  and (aBLRect.TopLeft.L=0) then
//    Exit;

//  vMapXRectangle := CoRectangle.Create;

(*  with aBLRect do
    vMapXRectangle.Set_(TopLeft.L, TopLeft.B,
              BottomRight.L, BottomRight.B);
*)

  aMap.Bounds := vMapXRectangle;

(*  e:=aMap.Zoom;

  if aMap.Zoom=0 then
    aMap.Zoom :=1;
*)
 // aMap.Refresh;

end;



procedure mapx_SetBoundsWithOffset(aMap: TMap; aBLRect: TBLRect);
begin
  aBLRect:=geo_ZoomRect (aBLRect, 0.1);
  mapx_SetBounds (aMap, aBLRect);
end;


procedure mapx_SetBoundsForBLVector(aMap: TMap; aBLVector: TBLVector);
var blRect: TBLRect;
begin
  blRect:=geo_BoundBLVector(aBLVector);
  blRect:=geo_ZoomRect (blRect, 0.1);
  mapx_SetBounds (aMap, blRect);
end;


procedure mapx_SetBoundsForBLVector_for_print(aMap: TMap; aBLVector: TBLVector);
var blRect: TBLRect;
  eDeltaL: Double;
  eLen_km: Double;
 // eDeltaL: Double;
begin
  blRect:=geo_BoundBLVector(aBLVector);

  eLen_km:=geo_Distance_km(aBLVector);

  eDeltaL:=Abs(aBLVector.Point1.L - aBLVector.Point2.L);
  blRect:=geo_ZoomRect (blRect, eDeltaL / 10);

//  blRect:=geo_ZoomRect (blRect, 0.1);
  mapx_SetBounds (aMap, blRect);
end;


//--------------------------------------------------------------------
function mapx_GetLayerByFileName(aMap: TMap; aFileName: string): CMapXLayer;
//--------------------------------------------------------------------
var
  vLayer: CMapXLayer;
  sFileSpec: string;
  i: Integer;
begin
  Assert(Assigned(aMap), 'Value not assigned');

  Result:= nil;

  for i:=1 to aMap.Layers.Count do
  begin
    vLayer:= aMap.Layers.Item[i];


    sFileSpec:= vLayer.FileSpec;

    if Eq(aFileName, sFileSpec) then
    begin
      Result:= vLayer;
      Exit;
    end;
  end;
end;

//--------------------------------------------------------------------
function mapx_GetLayerByName(aMap: TMap; aName: string): CMapXLayer;
//--------------------------------------------------------------------
var
  vLayer: CMapXLayer;
  sName: string;
  i: Integer;
begin
  Assert(Assigned(aMap), 'Value not assigned');


  for i:=1 to aMap.Layers.Count do
  begin
    vLayer:= aMap.Layers.Item[i];


    sName:= vLayer.Name;

    if Eq(aName, sName) then
    begin
      Result:= vLayer;
      Exit;
    end;
  end;

  Result:= nil;
end;


// ---------------------------------------------------------------
procedure mapx_FontStyle_LoadFromReg1(var aMIStyle: TmiStyleRec; aRegPath:
    string);
// ---------------------------------------------------------------
var
  oIni: TRegistryIniFile;
begin
  oIni:=TRegistryIniFile.Create(aRegPath);


  with aMIStyle.LabelProperties, oIni do
  begin
    TextFontName     :=ReadString ('', 'TextFontName',  'Arial');
    TextFontSize     :=ReadInteger('', 'TextFontSize', 10);

    TextFontColor    :=ReadInteger('', 'TextFontColor', 0);
    TextFontBackColor:=ReadInteger('', 'TextFontBackColor', 65535);

    Overlap          :=ReadBool   ('', 'Overlap',          false);
    TextFontShadow   :=ReadBool   ('', 'TextFontShadow',   false);
    TextFontAllCaps  :=ReadBool   ('', 'TextFontAllCaps',  false);
    TextFontDblSpace :=ReadBool   ('', 'TextFontDblSpace', false);

    Parallel         :=ReadBool   ('', 'Parallel', true);
    Position         :=ReadInteger('', 'Position', 0);
    Offset           :=ReadInteger('', 'Offset',   3);

    FontIsOpaque        :=ReadBool('', 'FontIsOpaque', false);
    FontIsHalo          :=ReadBool('', 'FontIsHalo',   false);


    if TextFontName='' then TextFontName:='Arial';
    if TextFontSize=0  then TextFontSize:=10;


  //  IsShowLabel         :=ReadBool('', 'IsShowLabel',   True);
   // No_Back             :=ReadBool('', 'No_Back',      false);
  end;

  oIni.Free;
end;

// ---------------------------------------------------------------
procedure mapx_FontStyle_SaveToReg1(aMIStyle: TmiStyleRec; aRegPath: string);
// ---------------------------------------------------------------
var
  oIni: TRegistryIniFile;
begin
  oIni:=TRegistryIniFile.Create(aRegPath);

  with aMIStyle.LabelProperties, oIni do
  begin
//    WriteBool   ('', 'IsShowLabel',   IsShowLabel );

    WriteInteger('', 'TextFontSize',      TextFontSize);
    WriteString ('', 'TextFontName',      TextFontName);

    WriteInteger('', 'TextFontColor',     TextFontColor);
    WriteInteger('', 'TextFontBackColor', TextFontBackColor);

    WriteBool   ('', 'Overlap',           Overlap);
    WriteBool   ('', 'TextFontShadow',    TextFontShadow);
    WriteBool   ('', 'TextFontAllCaps',   TextFontAllCaps);
    WriteBool   ('', 'TextFontDblSpace',  TextFontDblSpace);

    WriteBool   ('', 'Parallel',          Parallel);
    WriteInteger('', 'Position',          Position);
    WriteInteger('', 'Offset',            Offset);


    WriteBool   ('', 'FontIsOpaque',  FontIsOpaque );
    WriteBool   ('', 'FontIsHalo',    FontIsHalo   );



//    WriteBool('', 'No_Back',       No_Back      );
  end;

  oIni.Free;
end;

// ---------------------------------------------------------------
procedure mapx_SetScale (aMap: TMap; aScale: double);
// ---------------------------------------------------------------
// 1cm =  100m

// scale:
//  100 000,
//  200 000
var
  iPaperUnit, iMapUnit: Integer;
begin
  Assert(Assigned(aMap), 'Value not assigned');


//  iPaperUnit := aMap.PaperUnit;
//  iMapUnit   := aMap.MapUnit  ;

//  aMap.PaperUnit:= miUnitCentimeter;
  aMap.MapUnit  := miUnitMeter;

//  aMap.PaperUnit:= miUnitCentimeter;
//  aMap.MapUnit  := miUnitCentimeter;

  aMap.Zoom     := aMap.MapPaperWidth * aScale; // /100000

//  aMap.PaperUnit := iPaperUnit;
//  aMap.MapUnit   := iMapUnit  ;

end;

// ---------------------------------------------------------------
function mapx_GetScale (aMap: TMap): double;
// ---------------------------------------------------------------
// scale: 100 000, 200 000
//var
 // dMapLength,
//  dPaperLength: Double;

//  iPaperUnit, iMapUnit: Integer;
begin
  Assert(Assigned(aMap), 'Value not assigned');


//  aMap.PaperUnit:= miUnitCentimeter;
  aMap.MapUnit  := miUnitMeter;

  Result:= aMap.Zoom / aMap.MapPaperWidth;

{  iPaperUnit := aMap.PaperUnit;
  iMapUnit   := aMap.MapUnit  ;

  aMap.PaperUnit:= miUnitCentimeter;
  aMap.MapUnit  := miUnitCentimeter;

  dMapLength:= aMap.Zoom;
  dPaperLength:= aMap.MapPaperWidth; // + 1.5;

  Result:= dMapLength/dPaperLength;

  aMap.PaperUnit := iPaperUnit;
  aMap.MapUnit   := iMapUnit  ;
}

end;


// ---------------------------------------------------------------
procedure mapx_LabelStyle_LoadFromReg(var aMIStyle: TmiStyleRec; aRegPath: string);
// ---------------------------------------------------------------
var
  oIni: TRegistryIniFile;
begin
  oIni:=TRegistryIniFile.Create(aRegPath);


  with aMIStyle.LabelProperties, oIni do
  begin
    TextFontName     :=ReadString  ('', 'TextFontName',   'Arial');
    TextFontSize     :=ReadInteger ('', 'TextFontSize',   10);
    TextFontBold     :=ReadBool    ('', 'TextFontBold',   false);
    TextFontItalic   :=ReadBool    ('', 'TextFontItalic', false);

    TextFontColor    :=ReadInteger ('', 'TextFontColor',     0);
    TextFontBackColor:=ReadInteger ('', 'TextFontBackColor', 65535);
    TextFontRotation :=ReadInteger ('', 'TextFontRotation',  0);


    TextFontShadow   :=ReadBool    ('', 'TextFontShadow',   false);
    TextFontAllCaps  :=ReadBool    ('', 'TextFontAllCaps',  false);
    TextFontDblSpace :=ReadBool    ('', 'TextFontDblSpace', false);

    Overlap          :=ReadBool    ('', 'Overlap',  false);
    Parallel         :=ReadBool    ('', 'Parallel', true);

    Position         :=ReadInteger ('', 'Position', 5);  //������ //������
    Offset           :=ReadInteger ('', 'Offset',   3);  //iDefLinkPos

    LabelZoomMax     :=ReadInteger ('', 'LabelZoomMax', 100);
    LabelZoomMin     :=ReadInteger ('', 'LabelZoomMin', 0);
    LabelZoom        :=ReadBool    ('', 'LabelZoom',    false);


    FontIsOpaque     :=ReadBool    ('', 'FontIsOpaque', true);
    FontIsHalo       :=ReadBool    ('', 'FontIsHalo',   false);


    if Position>100 then
      Position:=5;


  end;

  oIni.Free;
end;



//------------------------------------------------------
function mapx_WritePolygon(aMap: TMap; aVLayer: CMapXLayer; aBLPoints:
    TBLPointArray; aColor, aStyle, aWidth: Integer; aLabel: string):
    CMapXFeature;
//------------------------------------------------------
var i: Integer;
    f: CMapXFeature;
    ps: CMapXPoints;
  v: Variant;
begin
  Assert(Assigned(aMap), 'Value not assigned');


  ps := CoPoints.Create;

//  ps.RemoveAll;

  Assert( Length( aBLPoints )>1, 'Value <=0');

  for i:=0 to High( aBLPoints)do
    ps.AddXY (aBLPoints[i].L, aBLPoints[i].B, EmptyParam);

  ps.AddXY (aBLPoints[0].L, aBLPoints[0].B, EmptyParam);

  aMap.DefaultStyle.LineColor:= aColor;
  aMap.DefaultStyle.LineWidth:= aWidth;


  f:=aMap.FeatureFactory.CreateLine (ps, aMap.DefaultStyle);
 // f.KeyValue := aLabel;
//  f.
 // name:=aLabel;


//  f.Caption  :=aLabel;

//  f:=FMap.FeatureFactory.CreateLine (ps, EmptyParam);

  try
//    Assert(Assigned(VLayer), 'Value not assigned');

 //zzz   VLayer.BeginAccess(miAccessReadWrite);
    Result:=aVLayer.AddFeature (f, EmptyParam);
    Result.KeyValue := aLabel;

//    v:=Result.KeyValue;
//    v:=Result.KeyValue;

    //  Name :=aLabel;

//    WriteFeatureFieldValues (Result, aParams);
  finally
//zz    VLayer.EndAccess(miAccessEnd);
  end;
end;



//--------------------------------------------------------------------
procedure mapx_AddBezierToLayer(aMap : TMap; aVLayer: CMapXLayer; var
    aBlPoints: TBLPointArrayF; aColor: integer; aWidth: integer);
//--------------------------------------------------------------------
var
  i, iCenter: Integer;
  ps: CMapXPoints;
  f: CMapXFeature;
  arrBLPointsBezier: TBLPointArrayF;
  dAzimuth: double;
  blVector: TBLVector;

const
  DEF_ARROW_DEGREE = 25;//15;
begin
  Assert( aMap<>nil);
  Assert( aVLayer<>nil);



  {vLayer:= GetTempLayer(aName);
  if not Assigned(vLayer) then
    exit;
}

  ps := CoPoints.Create;

  geo_MakeBezierF(aBLPoints, arrBLPointsBezier);

  for i := 0 to arrBLPointsBezier.Count - 1 do
    ps.AddXY (arrBLPointsBezier.Items[i].L, arrBLPointsBezier.Items[i].B, EmptyParam);

  iCenter:=arrBLPointsBezier.Count div 2;

  //draw arrow
  dAzimuth:=geo_Azimuth(arrBLPointsBezier.Items[iCenter], arrBLPointsBezier.Items[iCenter+1]);
//  dAzimuth:=geo_Azimuth(arrBLPointsBezier.Items[iCenter], arrBLPointsBezier.Items[iCenter-1]);

  blVector.Point1:= arrBLPointsBezier.Items[iCenter];

  blVector.Point2:= geo_RotateByAzimuth (blVector.Point1,300, geo_Azimuth_Plus_Azimuth(dAzimuth,DEF_ARROW_DEGREE));
  mapx_AddLineToLayer (aMap,aVLayer, blVector, aColor, -1, aWidth);

  blVector.Point2:=geo_RotateByAzimuth (blVector.Point1,300, geo_Azimuth_Plus_Azimuth(dAzimuth,-DEF_ARROW_DEGREE));
  mapx_AddLineToLayer (aMap,aVLayer, blVector, aColor, -1, aWidth);


  aMap.DefaultStyle.LineColor:= aColor;

  f:= aMap.FeatureFactory.CreateLine (ps, aMap.DefaultStyle);
  aVLayer.AddFeature (f, EmptyParam);
end;

// -------------------------------------------------------------------
function mapx_GetLayerIndex_from_1(aMap: TMap; aLayer: CMapXLayer): integer;
// -------------------------------------------------------------------
var
  i: integer;
begin
  Assert(Assigned(aMap), 'Value not assigned');

  for i:=1 to aMap.Layers.Count do

      if (aMap.Layers.Item[i] = aLayer) then

     begin
       Result:=i;
       Exit;
     end;

  Result:=-1;
end;



//--------------------------------------------------------------------
function mapx_GetLayerIndexByFileName_from_1(aMap: TMap; aFileName: string):
    integer;
//--------------------------------------------------------------------
var vLayer: CMapXLayer;
  i: integer;
  s: string;

begin
  Assert(Assigned(aMap), 'Value not assigned');

   aFileName:= LowerCase ( aFileName );

//  vLayer:=Map1.Layers.Item(aFileName);
//  iCount:= aMap.Layers.Count;

  for i:=1 to aMap.Layers.Count do
  begin

      vLayer:=aMap.Layers.Item[i];

      s:=LowerCase (vLayer.Filespec );

      if Eq (s, aFileName) then
      begin
        Result:=i;
        Exit;
      end;
   // s:=Map1.Layers.Item(i).Filespec;

  end;

   Result:=-1;
//  Result:=not VarIsNull(vLayer);
end;




//--------------------------------------------------------------------
function mapx_GetLayerIndexByMask_from_1(aMap: TMap; aMask: string): integer;
//--------------------------------------------------------------------
var vLayer: CMapXLayer;
  i: integer;
  s: string;
  sFileName: string;

begin
  Assert(Assigned(aMap), 'Value not assigned');

  aMask:= LowerCase ( aMask );

//  vLayer:=Map1.Layers.Item(aFileName);
//  iCount:= aMap.Layers.Count;

  for i:=1 to aMap.Layers.Count do
  begin
    vLayer:=aMap.Layers.Item[i];

    sFileName:= LowerCase ( vLayer.Filespec );

    if Pos (  aMask, sFileName)>0 then
    begin
      Result:=i;
      Exit;
    end;
   // s:=Map1.Layers.Item(i).Filespec;

  end;

  Result:=-1;

//  Result:=not VarIsNull(vLayer);
end;


//--------------------------------------------------------------------
function mapx_LoadWorkspace(aMap: TMap; aFileName: string): boolean;
//--------------------------------------------------------------------
var
  dCenterX, dCenterY: double;
  I: Integer;
var
  oIni: TIniFile;
  sIniFileName: string;

begin
  Assert(Assigned(aMap), 'Value not assigned');


  aFileName:=ChangeFileExt (aFileName, '.GST');
  Result:=False;


  AssertFileExists(aFileName);

  if FileExists(aFileName) then
  try
    aMap.GeoSet:=aFileName;

 //   aMap.Title.Caption:=' ';
//    aMap.Title.Visible:=False;

//    aMap.
    Result:=True;
  except
    DeleteFile (aFileName);
  end;


  {
  sIniFileName:=ChangeFileExt(aFileName, '.ini');


  if FileExists(sIniFileName) then
  begin
    oIni:=TIniFile.Create (sIniFileName);
      dCenterX:=AsFloat(oIni.ReadString('main','CenterX','0'));
      dCenterY:=AsFloat(oIni.ReadString('main','CenterY','0'));
    oIni.Free;

    if (aMap.Zoom>0) and (dCenterX<>0) then
      aMap.ZoomTo (aMap.Zoom, dCenterX, dCenterY);
  end;
     }



{  dCenterX:= AsFloat(ReadString(ClassName, aFileName+'CenterX', '0'));
  dCenterY:= AsFloat(ReadString(ClassName, aFileName+'CenterY', '0'));
}
{    dCenterX:= AsFloat(ReadString(ClassName, aFileName+'CenterX', '0'));
  dCenterY:= AsFloat(ReadString(ClassName, aFileName+'CenterY', '0'));
}

//  SetCoordSys();

  mapx_SetMapDefaultProjection_NumericCoordSys_KRASOVKSY42_simple(aMap);


{   i:=DATUM_KRASOVKSY42;

   aMap.NumericCoordSys.Set_ (miLongLat, 1001, //DATUM_KRASOVKSY42,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam);
}

(*  for I := 1 to Count do
    Map1.Datasets.Add (miDataSetLayer, Map1.Layers.Item(i), EmptyParam,EmptyParam,EmptyParam,EmptyParam,EmptyParam,EmptyParam) ;*)
end;

//--------------------------------------------------------------------
procedure mapx_SaveWorkspace(aMap: TMap; aFileName: string);
//--------------------------------------------------------------------
var
  I: Integer;
  oInifile: TIniFile;
  dCenterX,dCenterY: double;
  iType: Integer;
begin
  Assert(Assigned(aMap), 'Value not assigned');

  aFileName:=ChangeFileExt (aFileName, '.GST');
  ForceDirByFileName(aFileName);

  if not Assigned(aMap) then
    raise Exception.Create('');





  for I := aMap.Layers.Count downto 1 do
  begin
      iType := aMap.Layers.Item[i].Type_;

    if iType = miLayerInfoTypeTemp then
      aMap.Layers.Remove(i);

  end;
{

  vLayerInfoObject := CoLayerInfo.Create;
  vLayerInfoObject.Type_ := miLayerInfoTypeTemp;

}

  if aMap.Layers.COunt>0 then
    aMap.SaveMapAsGeoset ('', aFileName);
end;



//------------------------------------------------------
procedure mapx_SetMapDefaultProjection_NumericCoordSys_KRASOVKSY42_simple(aMap: TMap);
//------------------------------------------------------
//var
//  vCoordSys: CMapXCoordSys;
//  vMapXDatum: CMapXDatum;

begin
{
  vMapXDatum:=CoDatum.create;
  vMapXDatum.Set_(3, 23.57, -140.95, -79.8, 0, -0.35, -0.79, -0.22, 0);


  vCoordSys := CoCoordSys.Create;

//  vCoordSys.Datum.Set_(3, 23.57, -140.95, -79.8, 0, -0.35, -0.79, -0.22, 0);


  vCoordSys.Set_(miLongLat,  vMapXDatum, // 9999, // DATUM_KRASOVKSY42,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam);
 }



  aMap.NumericCoordSys.Set_ (miLongLat, DATUM_KRASOVKSY42,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam);
  

end;
(*
//------------------------------------------------------
procedure mapx_SetMapDefaultProjection_DisplayCoordSys_KRASOVKSY42(aMap: TMap);
//------------------------------------------------------
var
  vCoordSys: CMapXCoordSys;
  vMapXDatum: CMapXDatum;

begin
  vMapXDatum:=CoDatum.create;
  vMapXDatum.Set_(3, 23.57, -140.95, -79.8, 0, -0.35, -0.79, -0.22, 0);


  vCoordSys := CoCoordSys.Create;

//  vCoordSys.Datum.Set_(3, 23.57, -140.95, -79.8, 0, -0.35, -0.79, -0.22, 0);


  aMap.DisplayCoordSys.Set_(miLongLat,  DATUM_KRASOVKSY42,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam);


{
  aMap.DisplayCoordSys.Set_ (miLongLat,  vMapXDatum, //miLongLat, DATUM_KRASOVKSY42,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam);
 }
end;
 *)



function mapx_GetScaleStr(aMap: TMap): string;
var
  eScale: Double;
begin
  eScale:=mapx_GetScale(aMap);

  {
  if dZoom >=1 then
    result:= Format('1 �� = %1.1f ��', [dZoom])
  else
    result:= Format('1 �� = %1.1f �', [dZoom*1000]);
  }

  if eScale>1 then
    result := Format('1 �� = %1.1f ��',[ eScale/1000])
  else
    result := Format('1 �� = %1.3f ��',[ eScale/1000]);


//  mapx_GetScaleStr

end;


procedure mapx_ClearSelection(aMap: TMap);
var
  i: Integer;
begin
  for i:=1 to aMap.Layers.Count do

    if aMap.Layers.Item[i].Type_ = miLayerTypeNormal then
      aMap.Layers.Item[i].Selection.ClearSelection;


//  if Map1.Layers.Count>1 then
 //   FSelectedLayerList.Clear;
end;

{

function TMapFileX_Create: IMapFileX;
begin
  result:=TmiMap.Create;
end;
}
// -------------------------------------------------------------------
function mapx_GetDefaultColorOfLayer(aLayer: CMapXLayer; var aColor: integer): Boolean;
// -------------------------------------------------------------------
var
  v: CMapXFeatures;
begin
  Result:= False;

  if aLayer.PredominantFeatureType <> miFeatureTypeRegion then
  begin

    exit;
  end;

  aLayer.BeginAccess(miAccessRead);


//      oMiMap.vLayer.Selection.


  v:=aLayer.AllFeatures;

  if v.Count>0 then begin

      aColor:= aLayer.AllFeatures.Item[1].Style.RegionColor;

    Result := True;
  end;

  aLayer.EndAccess(miAccessEnd);


end;

function mapx_GetFeatureBounds(aVFeature: CMapXFeature): TBLRect;
var
  vSelectedBounds: CMapXRectangle;
begin
  vSelectedBounds:=aVFeature.Bounds;
  result:=mapx_XRectangleToBLRect (vSelectedBounds);
end;


//------------------------------------------------------
procedure mapx_GetFeatureParts(aVFeature: CMapXFeature; var aMIObjectRec: TmiObjectRec);
//------------------------------------------------------
var //vRowValues: CMapXRowValues;
  vPoints: CMapXPoints;
  vSelectedBounds: CMapXRectangle;
  vPoint: CMapXPoint;
//  s: string;
  blPoint: TBLPoint;

  t,i,j,iCount: integer;

begin
 // FillChar (aMIObjectRec, SizeOf(aMIObjectRec), 0);
  Assert(Assigned(aVFeature), 'aVFeature not assigned');


  aMIObjectRec.FeatureType:=aVFeature.Type_;


{
  aMIObjectRec.LineStyle.Color:=aVFeature.Style.LineColor;
  vSelectedBounds:=aVFeature.Bounds;
  aMIObjectRec.Bounds:=mapx_XRectangleToBLRect (vSelectedBounds);

  //------------------------------------------------------------------
  vRowValues:=FmiFile.VDataSet.RowValues[aVFeature];

  for i:=0 to High(FmiFile.Fields) do
    FmiFile.Fields[i].Value:=vRowValues.Item[FmiFile.Fields[i].Name].Value;

  aMIObjectRec.Fields:=FmiFile.Fields;
  //------------------------------------------------------------------
}
  case aMIObjectRec.FeatureType of

    //---------------------------------------------
    miFeatureTypeRegion, miFeatureTypeLine:
    //---------------------------------------------
    begin
        iCount:=aVFeature.Parts.Count;
        SetLength (aMIObjectRec.Parts, iCount);

        for i:=0 to aVFeature.Parts.Count-1 do
        begin

           vPoints:=aVFeature.Parts.Item[i+1];

          iCount:= vPoints.Count;

          SetLength (aMIObjectRec.Parts[i].Points, iCount);

//          aMIObjectRec.Parts[i].Points.Count:=iCount;

       //   SetLength (aMIObjectRec.Parts[i].Points, iCount);
          for j:=0 to vPoints.Count-1 do
          begin

              vPoint := vPoints.Item[j+1];


            blPoint.B:=vPoint.Y;
            blPoint.L:=vPoint.X;

            aMIObjectRec.Parts[i].Points[j]:=blPoint;
//            aMIObjectRec.Parts[i].PointsF.Items[j]:=blPoint;
          end;
        end;
    end;

    //---------------------------------------------
    miFeatureTypeSymbol:
    //---------------------------------------------
    begin
        vPoint:=aVFeature.Point;
        aMIObjectRec.Point.B:=vPoint.Y;
        aMIObjectRec.Point.L:=vPoint.X;
    end;

    //---------------------------------------------
    miFeatureTypeText:
    //---------------------------------------------
    begin
        vPoint:=aVFeature.Point;
        aMIObjectRec.Point.B:=vPoint.Y;
        aMIObjectRec.Point.L:=vPoint.X;
    end;

  end;
end;

//------------------------------------------------------
procedure mapx_GetMapLayerFields(aMapXLayer: CMapXLayer; var aFields:
    TmiFieldArray);
//------------------------------------------------------
var
  i,j,k,m: integer;
  vFields: CMapXFields;
  vDataSet: CMapXDataset;
begin

  // get fields
  if aMapXLayer.Datasets.Count > 0 then
  begin

      vDataSet:=aMapXLayer.Datasets.Item[1];

    vFields:=vDataSet.Fields;

    SetLength (aFields, vFields.Count);
    for i:=0 to vFields.Count-1 do
    begin

        aFields[i].Name :=vFields.Item[i+1].Name;
        aFields[i].Type_:=vFields.Item[i+1].Type_;


//        Fields[i].Name:=vFields.Item(i+1).Name;
//       Fields[i].Type_:=vFields.Item(i+1).Type_;
//        AddField (vFields.Item(i+1).Name, vFields.Item(i+1).Type_);
    end;

  end else
    SetLength (aFields, 0);
end;


//------------------------------------------------------------------------------
function mapx_MakeColor(aColor: integer): LongWord;
//------------------------------------------------------------------------------
begin
  //if aColor<0 then
  Result:= Graphics.ColorToRGB(aColor);
//  Result:= aColor and $00FFFFFF;

end;

//------------------------------------------------------------------------------
function mapx_MakeColor1(aColor: integer): integer;
//------------------------------------------------------------------------------
begin
  //if aColor<0 then
  Result:= Graphics.ColorToRGB(aColor);
 // Result:= aColor and $00FFFFFF;
end;



//------------------------------------------------------
function mapx_SetMapXLayertStyle(aCMapXLayer: CMapXLayer; aFeatureType:
    integer; aStyle: TmiStyleRec): boolean;
//------------------------------------------------------
var
  v1: Variant;
//  vStyle1: Variant;
  k: Integer;
  vStyle: CMapXStyle;
  zoom: double;

  lColor: OLE_COLOR;
const
  RUSSIAN_CHARSET = 204;

begin
 //  aCMapXLayer.OverrideStyle :=True;

   vStyle:=aCMapXLayer.Style;

//  try

  case aFeatureType of
    //-----------------------------------------------------
    miFeatureTypeLine:
    //-----------------------------------------------------
    begin
      vStyle.LineColor:=mapx_MakeColor (aStyle.LineColor);

      if aStyle.LineWidth < 8
        then vStyle.LineWidth:=aStyle.LineWidth
        else vStyle.LineWidth:=1;

      vStyle.LineStyle:=aStyle.LineStyle;
    end;

    //-----------------------------------------------------
    miFeatureTypeRegion:
    //-----------------------------------------------------
    begin
      vStyle.RegionBorderColor:=mapx_MakeColor (aStyle.RegionBorderColor);
      vStyle.RegionBorderWidth:=aStyle.RegionBorderWidth;
      vStyle.RegionBorderStyle:=aStyle.RegionBorderStyle;

      vStyle.RegionBackColor  :=mapx_MakeColor (aStyle.RegionBackgroundColor);
      vStyle.RegionColor      :=mapx_MakeColor (aStyle.RegionForegroundColor);
      vStyle.RegionPattern    :=aStyle.RegionPattern;
      vStyle.RegionTransparent:=aStyle.RegionIsTransparent;

    end;

    //-----------------------------------------------------
    miFeatureTypeSymbol:
  //  miFeatureTypeText:
    //-----------------------------------------------------
    begin
    //  vStyle:=aCMapXLayer.Style;

      if aStyle.SymbolFontName='' then
        aStyle.SymbolFontName:='Arial CYR';


      if Screen.Fonts.IndexOf (aStyle.SymbolFontName)<0 then begin
       // ErrorDlg ( 'Font not found: '+ Style.FontName );
        Exit;
      end;


      case aFeatureType of
        miFeatureTypeSymbol:begin
                              if aStyle.SymbolFontSize=0  then
                                aStyle.SymbolFontSize:=10;

                              vStyle.SymbolCharacter:=aStyle.SymbolCharacter; //36
                            //  vStyle.SymbolCharacter:=aStyle.Character+1;
                              //////////////////////

                              vStyle.SymbolFontColor:=mapx_MakeColor (aStyle.SymbolFontColor);

                            //  vStyle.SymbolFontColor:=mapx_MakeColor (clRed);

                             // vStyle.SymbolFontColor:=aStyle.FontColor;
                              /////////////////////
                              vStyle.SymbolFontHalo :=aStyle.FontIsHalo;
                             // vStyle.SymbolFontRotation:=aStyle.SymbolFontRotation;
                            //  vStyle.SymbolFontRotation:=aStyle.FontRotation;

                              v1:=vStyle.SymbolFont;

                              try
                                v1.Name:=aStyle.SymbolFontName;
                                v1.Size:=aStyle.SymbolFontSize;
                              except end;


                            end;
(*
        miFeatureTypeText:  begin
                              if aStyle.FontSize=0  then aStyle.FontSize:=10;

                              aMap.MapUnit:=miUnitKilometer;
                            //  aMap.Zoom:=DEF_ZOOM;

                              vStyle.TextFontColor:=mapx_MakeColor (aStyle.FontColor);
                              vStyle.TextFontHalo :=aStyle.FontIsHalo;

                            //  vStyle.TextFontRotation:=30; //aStyle.FontRotation;
                            //  vStyle.SymbolFontRotation:=30; //aStyle.FontRotation;

                            //  vStyle1:=aMap.DefaultStyle;
                              vStyle.TextFont.Name:=aStyle.FontName;
                              vStyle.TextFont.Size:=aStyle.FontSize;
                              vStyle.TextFont.Italic:=aStyle.FontIsItalic;
                            end;
        *)

      end;
    end;

    else
      raise Exception.Create('function TmiMap.PrepareStyle(aFeatureType: integer; aStyle: TmiStyle): boolean;');

  end;

 // except end;
end;



//------------------------------------------------------
function mapx_PrepareStyle(aMap: TMap; aFeatureType: integer; var aStyle: TmiStyleRec): boolean;
//------------------------------------------------------
var vStyle1: Variant;
    k: Integer;
    vStyle: CMapXStyle;
    zoom: double;

const
  RUSSIAN_CHARSET = 204;

begin

//  try

  case aFeatureType of
    //-----------------------------------------------------
    miFeatureTypeLine:
    //-----------------------------------------------------
    begin
      aMap.DefaultStyle.LineColor:=mapx_MakeColor (aStyle.LineColor);

      if aStyle.LineWidth < 8
        then aMap.DefaultStyle.LineWidth:=aStyle.LineWidth
        else aMap.DefaultStyle.LineWidth:=3;

      aMap.DefaultStyle.LineStyle:=aStyle.LineStyle;
    end;

    //-----------------------------------------------------
    miFeatureTypeRegion:
    //-----------------------------------------------------
    begin
      aMap.DefaultStyle.RegionBorderColor:=mapx_MakeColor (aStyle.RegionBorderColor);
      aMap.DefaultStyle.RegionBorderWidth:=aStyle.RegionBorderWidth;
      aMap.DefaultStyle.RegionBorderStyle:=aStyle.RegionBorderStyle;

      aMap.DefaultStyle.RegionBackColor  :=mapx_MakeColor (aStyle.RegionBackgroundColor);
      aMap.DefaultStyle.RegionColor      :=mapx_MakeColor (aStyle.RegionForegroundColor);
      aMap.DefaultStyle.RegionPattern    :=aStyle.RegionPattern;
      aMap.DefaultStyle.RegionTransparent:=aStyle.RegionIsTransparent;

    end;

    //-----------------------------------------------------
    miFeatureTypeSymbol,
    miFeatureTypeText:
    //-----------------------------------------------------
    begin
      vStyle:=aMap.DefaultStyle;
      if aStyle.SymbolFontName='' then
        aStyle.SymbolFontName:='Arial CYR';


      if Screen.Fonts.IndexOf (aStyle.SymbolFontName)<0 then begin
       // ErrorDlg ( 'Font not found: '+ Style.FontName );
        Exit;
      end;


      case aFeatureType of
        miFeatureTypeSymbol:begin
                              if aStyle.SymbolFontSize=0  then
                                aStyle.SymbolFontSize:=10;

                              vStyle.SymbolCharacter:=aStyle.SymbolCharacter;
                              //////////////////////

                              vStyle.SymbolFontColor:=mapx_MakeColor (aStyle.SymbolFontColor);
                             // vStyle.SymbolFontColor:=aStyle.FontColor;
                              /////////////////////
                              vStyle.SymbolFontHalo :=aStyle.FontIsHalo;
                              vStyle.SymbolFontRotation:=aStyle.SymbolFontRotation;

                            //  vStyle.SymbolFontRotation:=aStyle.FontRotation;

                              vStyle1:=aMap.DefaultStyle;
                              try
                                vStyle1.SymbolFont.Name:=aStyle.SymbolFontName;
                                vStyle1.SymbolFont.Size:=aStyle.SymbolFontSize;
                              except end;
                            end;

        miFeatureTypeText:  begin
                              if aStyle.SymbolFontSize=0  then aStyle.SymbolFontSize:=10;

                              aMap.MapUnit:=miUnitKilometer;
                            //  aMap.Zoom:=DEF_ZOOM;

                              vStyle.TextFontColor:=mapx_MakeColor (aStyle.SymbolFontColor);
                              vStyle.TextFontHalo :=aStyle.FontIsHalo;

                            //  vStyle.TextFontRotation:=30; //aStyle.FontRotation;
                            //  vStyle.SymbolFontRotation:=30; //aStyle.FontRotation;

                              vStyle1:=aMap.DefaultStyle;
                              vStyle1.TextFont.Name:=aStyle.SymbolFontName;
                              vStyle1.TextFont.Size:=aStyle.SymbolFontSize;
                              vStyle1.TextFont.Italic:=aStyle.FontIsItalic;
                            end;
      end;
    end;

    else
      raise Exception.Create('function TmiMap.PrepareStyle(aFeatureType: integer; aStyle: TmiStyle): boolean;');

  end;

 // except end;
end;


// ---------------------------------------------------------------
procedure mapx_CopyDatasetFieldsToMiFields(aDataSet: TDataSet; var aMiFieldArray: TmiFieldArray);
// ---------------------------------------------------------------

var
  i,k: Integer;
begin
  if not aDataSet.Active then
    exit;

  SetLength(aMiFieldArray, 0);
  k:= 0;

  for i := 0 to aDataSet.FieldCount-1 do
  begin
    with aDataSet.Fields.Fields[i] do
     // if not FoundInExceptArr(FieldName) then
      begin
        SetLength(aMiFieldArray, Length(aMiFieldArray)+1);

        case DataType of
          ftString,
          ftWideString: aMiFieldArray[k]:= mapX_Field (FieldName, miTypeString, DataSize);

          ftInteger,
          ftSmallInt,
          ftWord,
          ftLargeInt:   aMiFieldArray[k]:= mapX_Field (FieldName, miTypeInteger);
          ftFloat:      aMiFieldArray[k]:= mapX_Field (FieldName, miTypeFloat);

        else
            Raise Exception.Create('');
        end;

        Inc(k);
      end;
  end;
end;


//--------------------------------------------------------------------
procedure mapx_CreateMemFromMapFields(aDataSet: TDataSet; var aFields: TmiFieldArray);
//--------------------------------------------------------------------

{ TODO : !!!!!!!!! }

var
  i: Integer;
 // oMiMap: TMiMap;
begin
 // Result:= false;

  aDataSet.Close;
  db_ClearFields(aDataSet);

//  oMiMap:= TMiMap.Create;

{  try
    oMiMap.OpenFile(aMapFileName);
  except
    exit;
  end;
}

  for i := 0 to High(aFields) do
  begin
    case aFields[i].Type_ of
      miTypeString,
      miTypeDate     : db_CreateField(aDataSet, [db_Field(aFields[i].Name, ftString, 100)  ]);

      miTypeFloat    : db_CreateField(aDataSet, [db_Field(aFields[i].Name, ftFloat)   ]);

      miTypeInt,
      miTypeNumeric,
      miTypeSmallInt,
      miTypeLogical  : db_CreateField(aDataSet, [db_Field(aFields[i].Name, ftInteger)  ]);
    end;
  end;

  aDataSet.Open;

 // oMiMap.Free;

 // Result:= true;
end;


{
function mapX_GetFeatureTypeStr(aFeatureType: Integer): string;
begin
    case aFeatureType of    //
      miFeatureTypeRegion  : Result := 'region-0';
      miFeatureTypeLine    : Result := 'line-1';
      miFeatureTypeSymbol  : Result := 'symbol-2';
      miFeatureTypeMixed   : Result := '';
      miFeatureTypeUnknown : Result := '';
      miFeatureTypeText    : Result := 'text-5';
      miFeatureTypeNull    : Result := '';
    else
      Result := '';
   end;
 end;
}


//-------------------------------------------------------------------
function mapx_GetDatasetByIndex(aCMapXDatasets: CMapXDatasets; aIndex: integer):
    CMapXDataset;
//-------------------------------------------------------------------
begin
  //i:=aCMapXDatasets.Count;
  Assert((aIndex>=0) and (aIndex<=aCMapXDatasets.Count));

  if aIndex<=aCMapXDatasets.Count then


    Result := aCMapXDatasets.Item[aIndex]

  else
    Result := nil;
end;
    {

//--------------------------------------------------------------------
procedure mapx_MakeLayerTransparent111111111__________(aLayer: CMapXLayer);
//--------------------------------------------------------------------
var iColor: integer;
begin
  if mapx_GetDefaultColorOfLayer (aLayer, iColor) then
  begin
    aLayer.OverrideStyle:= true;

    aLayer.Style.RegionPattern:=     15;
    aLayer.Style.RegionColor:=       iColor;
    aLayer.Style.RegionTransparent:= true;
  end;
end;
   }


//-------------------------------------------------------------------
function mapx_GetLayerByIndex_from_0(aMap: TMap; aIndex: integer): CMapXLayer;
//-------------------------------------------------------------------
// index[0..]

var i: integer;
begin
  i:=aMap.Layers.Count;

  Assert((aIndex>=0), Format('aIndex>=0: %d', [aIndex]));
  Assert((aIndex<aMap.Layers.Count), Format('aIndex < aMap.Layers.Count: %d - %d', [aIndex, aMap.Layers.Count]));


  Result := aMap.Layers.Item[aIndex+1]

end;

//-------------------------------------------------------------------
function mapx_GetLayerByIndex_from_1(aMap: TMap; aIndex: integer): CMapXLayer;
//-------------------------------------------------------------------
// index[0..]

var i: integer;
begin
  i:=aMap.Layers.Count;

  Assert((aIndex>=1), Format('aIndex>=1: %d', [aIndex]));
  Assert((aIndex<=aMap.Layers.Count), Format('aIndex < aMap.Layers.Count: %d - %d', [aIndex, aMap.Layers.Count]));


  Result := aMap.Layers.Item[aIndex]

end;



//-------------------------------------------------------------------
function mapx_Show_Layers_Dlg(aMap: TMap): CMapXLayer;
//-------------------------------------------------------------------
var
  I: Integer;
  oSList: TStringList;
  sFileName: string;
  sName: string;
  vLayer: CMapXLayer;

  blRect: TBLRect;

// index[0..]
begin
  oSList:=TStringList.Create;

 // blRect := mapx_XRectangleToBLRect (aMap.DisplayCoordSys.Bounds);


//  aMap.DisplayCoordSys.type_;
 // aMap.DisplayCoordSys. Bounds;

  oSList.Add(Format('DisplayCoordSys.type_: %d', [aMap.DisplayCoordSys.type_]));
  oSList.Add(Format('NumericCoordSys.type_: %d', [aMap.NumericCoordSys.type_]));

  oSList.Add(Format('DisplayCoordSys.Datum.Ellipsoid: %d', [aMap.DisplayCoordSys.Datum.Ellipsoid]));
  oSList.Add(Format('NumericCoordSys.Datum.Ellipsoid: %d', [aMap.NumericCoordSys.Datum.Ellipsoid]));


//  FMap.Layers.Remove (aIndex);


(*              iEllipsoid1:=FMap.DisplayCoordSys.Datum.Ellipsoid;
              iEllipsoid2:=FMap.NumericCoordSys.Datum.Ellipsoid;

              vLayer:= FMap.Layers.Add (oMapItem.FileName, FMap.Layers.Count+1);

              iEllipsoid1:=FMap.DisplayCoordSys.Datum.Ellipsoid;
              iEllipsoid2:=FMap.NumericCoordSys.Datum.Ellipsoid;
*)


//  for I := 1 to FMap.Layers.Count do
  for I := 1 to  aMap.Layers.Count  do
  begin
    vLayer:=mapx_GetLayerByIndex_from_1(aMap, i);

    sFileName:=vLayer.Filespec;
    sName    :=vLayer.Name;

    ;

    oSList.Add(Format('%d - %s', [i, sFileName]));

   // end;
  end;


  ShowMessage(oSList.Text);

  FreeAndNil(oSList);


end;


//--------------------------------------------------------------------
function mapx_DeleteByCondition(aCMapXLayer: CMapXLayer; aCondition: string):
    Integer;
//--------------------------------------------------------------------
var
  vFeatures: CMapXFeatures;
  i: integer;
  iAll: Integer;
  s: string;
  v: variant;
  vFeature: CMapXFeature;
begin
  vFeatures:=aCMapXLayer.Search (aCondition, EmptyParam); //Format('%s = %d', [aFieldName, aID]));

  iAll:=aCMapXLayer.AllFeatures.Count;


//  aCMapXLayer.AllFeatures.

  i:=vFeatures.Count;

  s := Format('count: %d, aCondition: %s', [i, aCondition]);
  ShowMessage(s);


 // Assert(vFeatures.Count=1, 'vFeatures.Count=1');

  for i:=1 to vFeatures.Count do
//  for i:=vFeatures.Count downto 1 do
  begin
//    mapx_Get


      vFeature:=vFeatures.Item[i];


//    vFeature.FeatureKey

 v:=vFeature.FeatureKey;

    try
      aCMapXLayer.DeleteFeature (vFeature.FeatureKey);
    except
      on E: Exception do
        ShowMessage(s);
    end;

  end;

  Result := vFeatures.Count ;
end;


// ---------------------------------------------------------------
function mapx_IsFieldExists(aDS: CMapxDataset; aFileName: string): Boolean;
// ---------------------------------------------------------------
var
  k: Integer;
  s: string;
begin
  Result := False;

  for k := 1 to aDS.Fields.Count do
  begin

    s:=aDS.Fields.Item[k].Name;


    if Eq(s,aFileName) then
    begin
      Result := True;
      Exit;
    end;
  end;
end;

// ---------------------------------------------------------------
procedure mapx_SetDefaultCoordSys(aMap: TMap);
// ---------------------------------------------------------------

var
  vCoordSys: CMapXCoordSys;
  vMapXDatum: CMapXDatum;

begin
  vMapXDatum:=CoDatum.create;
  vMapXDatum.Set_(3, 23.57, -140.95, -79.8, 0, -0.35, -0.79, -0.22, 0);


  vCoordSys := CoCoordSys.Create;


{  iDisplayCoordSys_type:=aMap.DisplayCoordSys.type_;
  iNumericCoordSys_type:=aMap.NumericCoordSys.type_;

  iEllipsoid1:=aMap.DisplayCoordSys.Datum.Ellipsoid;
  iEllipsoid2:=aMap.NumericCoordSys.Datum.Ellipsoid;

}

  aMap.DisplayCoordSys.Set_ (miLongLat,  vMapXDatum, //miLongLat, DATUM_KRASOVKSY42,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam);

    {

  aMap.DisplayCoordSys.Set_ (miLongLat, DATUM_KRASOVKSY42,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam);
    }

end;


//------------------------------------------------------
procedure mapx_SetMapDefaultProjection_NumericCoordSys_KRASOVKSY42_WMS(aMap: TMap);
//------------------------------------------------------
var
  vCoordSys: CMapXCoordSys;
  vMapXDatum: CMapXDatum;

begin
  vMapXDatum:=CoDatum.create;
  vMapXDatum.Set_(3, 23.57, -140.95, -79.8, 0, -0.35, -0.79, -0.22, 0);


  vCoordSys := CoCoordSys.Create;

//  vCoordSys.Datum.Set_(3, 23.57, -140.95, -79.8, 0, -0.35, -0.79, -0.22, 0);

{
  vCoordSys.Set_(miLongLat,  vMapXDatum, // 9999, // DATUM_KRASOVKSY42,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam);
}


  aMap.NumericCoordSys.Set_ (miLongLat,  vMapXDatum, //miLongLat, DATUM_KRASOVKSY42,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam);

  {
  aMap.NumericCoordSys.Set_ (miLongLat, DATUM_KRASOVKSY42,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam);

  }
end;



//---------------------------------------------------------------------
function mapx_FileIsRasterLayer(aTabFileName: string): boolean;
//---------------------------------------------------------------------
const
  STR_RASTER = 'Type "RASTER"';
var
  i: Integer;
  oStrList: TStringList;
begin
  Result:= false;

  if not FileExists(aTabFileName) then
    exit;

  oStrList:= TStringList.Create;
  oStrList.LoadFromFile(aTabFileName);
  
  for i := 0 to oStrList.Count - 1 do
  begin
    if Pos_(STR_RASTER, oStrList[i]) then
    begin
      Result:= true;
      break;
    end;
  end;

  FreeAndNil(oStrList);

end;


//---------------------------------------------------------------------
function mapx_GetTransparence_0_100(aTabFileName: string): Integer;
//---------------------------------------------------------------------
//   0 - not transparent
// 100 - transparent
//---------------------------------------------------------------------
var
  i: Integer;
  oStrList: TStringList;
  s: string;
begin
//  Result:= -1;
  Result:= 0;

  oStrList:= TStringList.Create;
//
//  if MapFileHasParts(aTabFileName) then
//  begin
//    MapFileGetParts(aTabFileName, oStrList);
//    if oStrList.Count = 0 then exit;
//    aTabFileName:= oStrList[0];
//  end;

  if not mapx_FileIsRasterLayer(aTabFileName) then
    exit;

  oStrList.LoadFromFile(aTabFileName);

  for i := 0 to oStrList.Count - 1 do
  begin
    s:=oStrList[i];

    if Pos_('RasterStyle 8 ', oStrList[i]) then
    begin
      Result:= AsInteger( ReplaceStr(oStrList[i], 'RasterStyle 8 ', '') );

//      Result:= 255 - Result;

//      Result:= Result div 100;

      break;
    end;
  end;

//  iVal:=Round((255/10
//   0 - transparent
// 255 - not transparent


  Result := Round(((255-Result) / 255) * 100);



  FreeAndNil(oStrList);


end;


//---------------------------------------------------------------------
procedure mapx_SetTransparence_0_100(aTabFileName: string; aValue: Integer);
//---------------------------------------------------------------------
//   0 - not transparent
// 100 - transparent
//////////////////////////////////
//   0 - transparent
// 255 - not transparent
//---------------------------------------------------------------------
var
  i: Integer;
  iIndex: Integer;
  iTrans: Integer;
  iVal: Integer;
  oStrList: TStringList;
  s: string;
begin
//  Result:= -1;
//  Result:= 0;

  oStrList:= TStringList.Create;
//
//  if MapFileHasParts(aTabFileName) then
//  begin
//    MapFileGetParts(aTabFileName, oStrList);
//    if oStrList.Count = 0 then exit;
//    aTabFileName:= oStrList[0];
//  end;

  if not mapx_FileIsRasterLayer(aTabFileName) then
    exit;

  oStrList.LoadFromFile(aTabFileName);

  iIndex :=-1;

  for i := 0 to oStrList.Count - 1 do
  begin
    s:=oStrList[i];

    if Pos_('RasterStyle 8 ', oStrList[i]) then
    begin
      iIndex := i;
      break;
    end;

(*    if Pos_('RasterStyle 8 ', oStrList[i]) then
    begin
      Result:= AsInteger( ReplaceStr(oStrList[i], 'RasterStyle 8 ', '') );
      Result:= 255 - Result;
      break;
    end;
    *)
  end;

  if iIndex<0 then
    iIndex :=oStrList.Add('');

  //iTrans:=Trunc(255 * aValue / 100 );


  iVal:=255 - Round((255/100) * aValue );


  oStrList[i]:= 'RasterStyle 8 ' + IntToStr(iVal);


  oStrList.SaveToFile(aTabFileName);
  FreeAndNil(oStrList);
end;
                                     

// ---------------------------------------------------------------
procedure mapx_ReOpenMapByFileName(aMap: TMap; aFileName: string);
// ---------------------------------------------------------------
var
  iIndex: Integer;
  vLayer: CMapXLayer;

begin
  vLayer := mapx_GetLayerByFileName(aMap, aFileName);
  iIndex := mapx_GetLayerIndexByFileName_from_1(aMap, aFileName);

  aMap.Layers.Remove(vLayer);

  vLayer := aMap.Layers.Add (aFileName, iIndex);

 // mapx_

//  mapx_De


  // TODO -cMM: mapx_ReOpenMapByFileName default body inserted
end;

// ---------------------------------------------------------------
procedure mapx_SetZoomPlan(aMap: TMap);
// ---------------------------------------------------------------
var
  BLRect: TBLRect;

begin
  if aMap.Layers.Count=0 then
    exit;

  BLRect:=mapx_GetPlanBounds(aMap);
  mapx_SetBounds (aMap, BLRect);


  if not  mapx_MapHasRaster(aMap) then
    mapx_SetDefaultCoordSys(aMap);

end;




// ---------------------------------------------------------------
function mapx_AddMap(aMap: TMap; aFileName : string): CMapXLayer;
// ---------------------------------------------------------------
var
  vLayer: CMapXLayer;

begin
  if not FileExists(aFileName) then
  begin
    ShowMessage ('���� �� ����������: '+ aFileName);
    Exit;
  end;

  vLayer:=mapx_GetLayerByFileName(aMap, aFileName);


  if vLayer=nil then
    aMap.Layers.Add (aFileName, EmptyParam);


  if aMap.Layers.Count=1 then
    mapx_SetZoomPlan (aMap);


   result := vLayer;

//   function mapx_MapHasRaster(aMap: TMap): Boolean;


end;

//--------------------------------------------------------------------
function mapx_MapAdd_Ex(aMap: TMap; aFileName: string): CMapXLayer;
//--------------------------------------------------------------------
var
  bTile: Boolean;
  vLayer: CMapXLayer;
  sRegPath: string;
    i: Integer;

    iTileInd: Integer;

  s: string;
  sFileName: string;
  sName: string;
begin
  Assert(FileExists(aFileName), Format('FileExists(aFileName): %s', [aFileName]));


  Result := nil;

//  mapx_L

  vLayer:=mapx_GetLayerByFileName(aMap, aFileName);

  Assert(vLayer=nil, '  vLayer:=mapx_GetLayerByFileName(aMap, aFileName);');



  sName:= ExtractFileName(aFileName);

  bTile:=pos('tile',LowerCase(sName))=1;
                          
  iTileInd :=0;
//  iRasterInd :=0;  
                          
  
  for i:=1 to aMap.Layers.Count do
  begin     
    vLayer:=aMap.Layers.Item[i];

    sFileName:=vLayer.FileSpec;    
                   
    case vLayer.Type_ of
        miLayerTypeRaster: ;                   
        miLayerTypeNormal: ;
    end;                       
    
    s:= LowerCase (ExtractFileName(sFileName));

    if Copy(s,1,4) = 'tile' then
      if iTileInd=0 then
        iTileInd:=i;

  end;

  // add TILE to end
  if bTile then
    try
      vLayer:= aMap.Layers.Add (aFileName, aMap.Layers.Count+1)

    except
      begin
//         s:='!!! mapx_MapAdd_Ex','vLayer:= aMap.Layers.Add (aFileName, aMap.Layers.Count+1)  - ' + aFileName;
         g_log.Msg('!!! mapx_MapAdd_Ex - vLayer:= aMap.Layers.Add (aFileName, aMap.Layers.Count+1)  - ' + aFileName);
         g_log.Msg(s);

         exit;

      end;
//      ShowMessage('vLayer:= aMap.Layers.Add (aFileName, aMap.Layers.Count+1)  - '+ aFileName);

    end


  else begin
//    if bTile then
 //     vLayer:= aMap.Layers.Add (aFileName, 1)
  //  else

    try
      vLayer:= aMap.Layers.Add (aFileName, 1);
    except
      begin
        g_log.Msg('!!! mapx_MapAdd_Ex - vLayer:= aMap.Layers.Add (aFileName, aMap.Layers.Count+1)  - '+ aFileName);

        exit;

      end;
    //  ShowMessage('vLayer:= aMap.Layers.Add (aFileName, 1);  - '+ aFileName);

    end;

    if aMap.Layers.Count>1 then
      if not bTile then
        case vLayer.Type_ of
           miLayerTypeRaster: if iTileInd>0 then
                               aMap.Layers.Move(1, iTileInd) ;

//           miLayerTypeNormal:  aMap.Layers.Move(aMap.Layers.Count, 1) ;

        end;  

    
//    vLayer.
//    aMap.Layers.M
  end;

  Result := vLayer;
end;
          


// ---------------------------------------------------------------
procedure mapx_RemoveMapFile(aMap: TMap; aFileName : string);
// ---------------------------------------------------------------
var
  vLayer: CMapXLayer;

begin
  Assert (aFileName<>'', 'aFileName = ""');

  vLayer:=mapx_GetLayerByFileName(aMap, aFileName);

  if vLayer<>nil then
     aMap.Layers.Remove (vLayer);

  if not  mapx_MapHasRaster(aMap) then
    mapx_SetDefaultCoordSys(aMap);


end;

// ---------------------------------------------------------------
procedure mapx_Set_Width_Height(aMap: TMap; aWidth,aHeight: Integer);
// ---------------------------------------------------------------
begin

//  aMap.PaperUnit:= miUnitCentimeter;
//  aMap.MapUnit  := miUnitCentimeter;


{
  aMap.PaperUnit:= miUnitCentimeter;
  aMap.MapUnit  := miUnitMeter;
}


  // Width,Height - in cm
  aMap.Width :=Round (aWidth  * (Screen.PixelsPerInch )/2.54)  ;
  aMap.Height:=Round (aHeight * (Screen.PixelsPerInch )/2.54)  ;

 // TODO -cMM: mapx_Set_Width_Height default body inserted
end;
          

//------------------------------------------------------------------------------
function mapx_CreateFields(var aInFields: TmiFieldArray; aCreateIndexOnID:
    boolean = false): CMapXFields;
//------------------------------------------------------------------------------

var
  i: Integer;
begin
  Result:=nil;                             
  if Length(aInFields)=0 then exit;

  Result:= CoFields.Create;

  for i:=0 to High(aInFields) do
  begin
    if Eq(aInFields[i].Name, FLD_ID) then
      Result.AddIntegerField (aInFields[i].Name, aCreateIndexOnID)
    else
      case aInFields[i].Type_ of
        miTypeString :
        begin
          if (aInFields[i].Size=0) or (aInFields[i].Size>200) then
             aInFields[i].Size:= 200;

          Result.AddStringField (aInFields[i].Name, aInFields[i].Size, false);
        end;

        miTypeFloat    : Result.AddFloatField   (aInFields[i].Name, false);
        miTypeInteger  : Result.AddIntegerField (aInFields[i].Name, false);
        miTypeSmallInt : Result.AddIntegerField (aInFields[i].Name, false);
        miTypeNumeric  : Result.AddIntegerField (aInFields[i].Name, false);
      end;
  end;
end;


//------------------------------------------------------------------------------
function mapx_CreateTempLayerWithFeatures(aMap: TMap; aLayerName: string; aRec:
    TmiMapCreateRec; aFeatures: CMapxFeatures; aIndex: Integer=1;
    aAutoCreateDataset: boolean = false): CMapXLayer;
//------------------------------------------------------------------------------
var
  vLayerInfoObject: CMapXLayerInfo;
  vFlds: CMapxFields;
begin
  vFlds:= mapx_CreateFields(aRec.Fields);

  vLayerInfoObject:= CoLayerInfo.Create;

  vLayerInfoObject.Type_:= miLayerInfoTypeTemp;

  vLayerInfoObject.AddParameter('Name',     aLayerName);

    if Assigned(vFlds) then
       vLayerInfoObject.AddParameter('Fields',   vFlds);

    if Assigned(aFeatures) then
       vLayerInfoObject.AddParameter('Features', aFeatures);

    if Assigned(vFlds) then
       vLayerInfoObject.AddParameter('TableStorageType', 'MemTable');

    if aAutoCreateDataset then
      vLayerInfoObject.AddParameter('AutoCreateDataset', 1);

  Result:= aMap.Layers.Add(vLayerInfoObject, aIndex);

//..  vLayerInfoObject:= nil;
//..  vFlds:= nil;
end;
       

//--------------------------------------------------------------------
procedure map_Tile_Align_Back(aMap: TMap);
//--------------------------------------------------------------------
var
//  bTile: Boolean;
  vLayer: CMapXLayer;
 // sRegPath: string;
  i: Integer;
  s: string;
  sFileName: string;
  sName: string;

begin

  for i:=2 to aMap.Layers.Count do
  begin
    vLayer:=aMap.Layers.Item[i];

    sFileName:=vLayer.FileSpec;

    s:= LowerCase (ExtractFileName(sFileName));

    if  (vLayer.Type_ = miLayerTypeRaster) and
        (LeftStr(s,4) = 'wms_tile')
    then begin
      aMap.Layers.Move(i, 1) ;
      break;
    end;
  end;

end;


// ---------------------------------------------------------------
function mapx_MapUnit_to_str(aValue: Integer): string;
// ---------------------------------------------------------------
begin                       
  case aValue of
    miUnitKilometer:  Result:='miUnitKilometer';
    miUnitMeter:      Result:='miUnitMeter';    
    miUnitDegree:     Result:='miUnitDegree';
  else
    Result:= IntToStr(aValue) 
  end;

end;

// ---------------------------------------------------------------
procedure mapx_TilesClear(aMap: CMapX);
// ---------------------------------------------------------------
var
  I: Integer;
  s: string;
begin
  for i:=aMap.Layers.Count downto 1 do
  begin
    s:=aMap.Layers.Item[i].FileSpec;

    s:= LowerCase (ExtractFileName(s));

    if LeftStr(s,Length('wms_tile')) = 'wms_tile' then
      aMap.Layers.Remove(i);

  end;
end;

// ---------------------------------------------------------------
procedure mapx_TilesClear(aMap: TMap);
// ---------------------------------------------------------------
var
  I: Integer;
  s: string;
begin
  for i:=aMap.Layers.Count downto 1 do
  begin
    s:=aMap.Layers.Item[i].FileSpec;

    s:= LowerCase (ExtractFileName(s));

    if LeftStr(s,Length('wms_tile')) = 'wms_tile' then
      aMap.Layers.Remove(i);

  end;
end;


//--------------------------------------------------------------------
procedure mapx_SetBounds_CMapX(aMap: CMapX; aBLRect: TBLRect);
//--------------------------------------------------------------------
var
  vMapXRectangle: CMapXRectangle;
  e: Double;
begin
  Assert(Assigned(aMap), 'Value not assigned');


  if (aBLRect.TopLeft.B=0)  and (aBLRect.TopLeft.L=0) then
    Exit;

  vMapXRectangle := CoRectangle.Create;

  with aBLRect do
    vMapXRectangle.Set_(TopLeft.L, TopLeft.B,
              BottomRight.L, BottomRight.B);
//
  aMap.Bounds := vMapXRectangle;
//
//  with aBLRect do
//    aMap.Bounds.Set_(TopLeft.L, TopLeft.B,
//                BottomRight.L, BottomRight.B);


//  e:=aMap.Zoom;
//
//  if aMap.Zoom=0 then
//    aMap.Zoom :=1;
                    
//  aMap.Refresh;
end;


// ---------------------------------------------------------------
function mapx_GetLayerIndexByFileName_CMapX(aMap: CMapX; aFileName: string):
    integer;
// ---------------------------------------------------------------
var vLayer: CMapXLayer;
  i, iCount: integer;
  s: string;
 // S: string;
begin
  Assert(Assigned(aMap), 'Value not assigned');

  Result:=-1;

//  vLayer:=Map1.Layers.Item(aFileName);
  iCount:= aMap.Layers.Count;

  for i:=1 to aMap.Layers.Count do
  begin     
    vLayer:=aMap.Layers.Item[i];    

    s:=vLayer.Filespec;


    if Eq (s, aFileName) then
    begin
      Result:=i;
      Exit;
    end;
   // s:=Map1.Layers.Item(i).Filespec;

  end;
//  Result:=not VarIsNull(vLayer);
end;



// ---------------------------------------------------------------
function mapx_FileExists(aMap: CMapX; aFileName: string): boolean;
// ---------------------------------------------------------------
var    
  k: Integer;
 // S: string;
begin
  k:=mapx_GetLayerIndexByFileName_CMapX (aMap, aFileName);

  Result:=k>0;

end;

// ---------------------------------------------------------------
function mapx_FileExists(aMap: TMap; aFileName: string): boolean;
// ---------------------------------------------------------------
var
  k: Integer;
 // S: string;
begin
  k:=mapx_GetLayerIndexByFileName_from_1 (aMap, aFileName);

  Result:=k>0;

end;



// ---------------------------------------------------------------
function mapx_CMapXCoordSys_to_str(aCMapXCoordSys: CMapXCoordSys): string;
// ---------------------------------------------------------------
var
  k: Integer;
  sType: string;
  sUnits: string;
begin
//  aCMapXCoordSys.Datum.


//  miMercator = $0000000A;
//miLongLat

{
const
  DATUM_KRASOVKSY42 = 1001;
  DATUM_WGS84       = 104; //28;

}
  case aCMapXCoordSys.type_ of
    miMercator: sType:='miMercator';
    miLongLat:  sType:='miLongLat';
  else
    sType:=IntToStr(aCMapXCoordSys.type_)
  end;

  k:=aCMapXCoordSys.Units;
  
//  case aCMapXCoordSys.Units of
//    miUnitMeter:  sUnits:='miUnitMeter';
//    miUnitDegree: sUnits:='miUnitDegree';
//  else
//    sUnits:= IntToStr(aCMapXCoordSys.Units) 
//  end;

  sUnits:=mapx_MapUnit_to_str (aCMapXCoordSys.Units);
  
  
  Result:=
    Format('type_: %s',[sType]) + CRLF +
    Format('Units: %s',[sUnits]) + CRLF +    
    Format('Datum.Ellipsoid: %d',[aCMapXCoordSys.Datum.Ellipsoid]) + CRLF ;
  //  Format('aCMapXCoordSys.Datum.Ellipsoid: %d',[aCMapXCoordSys.Datum.Ellipsoid]) + CRLF +    


{
 FMap.NumericCoordSys.Set_ (miMercator, DATUM_WGS84, 
  //miUnitDegree,  
   miUnitMeter, 
   0,
//  FMap.NumericCoordSys.Set_ (miLongLat, DATUM_KRASOVKSY42,
      EmptyParam, EmptyParam, //EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam);

               
//  CoordSys Earth Projection 10, 104, "m", 0
//
//    procedure Set_(Type_: CoordSysTypeConstants; Datum: OleVariant; Units: OleVariant; 
//                   OriginLongitude: OleVariant; OriginLatitude: OleVariant; 
//                   StandardParallelOne: OleVariant; StandardParallelTwo: OleVariant; 
//                   Azimuth: OleVariant; ScaleFactor: OleVariant; FalseEasting: OleVariant; 
//                   FalseNorthing: OleVariant; Range: OleVariant; Bounds: OleVariant; 
//                   AffineTransform: OleVariant); dispid 15;



 property type_: CoordSysTypeConstants readonly dispid 1;
    property Datum: CMapXDatum readonly dispid 2;
    property Units: MapUnitConstants readonly dispid 3;
    property Bounds: CMapXRectangle readonly dispid 4;
    property OriginLongitude: Double readonly dispid 5;
    property OriginLatitude: Double readonly dispid 6;
    property StandardParallelOne: Double readonly dispid 7;
    property StandardParallelTwo: Double readonly dispid 8;
    property Azimuth: Double readonly dispid 9;
    property ScaleFactor: Double readonly dispid 10;
    property FalseEasting: Double readonly dispid 11;
    property FalseNorthing: Double readonly dispid 12;
    property Range: Double readonly dispid 13;

}

  
end;


//------------------------------------------------------
procedure mapx_SetMapDefaultProjection_NumericCoordSys_KRASOVKSY42_simple_(aMap: CMapX);
//------------------------------------------------------
//var
//  vCoordSys: CMapXCoordSys;
//  vMapXDatum: CMapXDatum;

begin
//  vMapXDatum:=CoDatum.create;
//  vMapXDatum.Set_(3, 23.57, -140.95, -79.8, 0, -0.35, -0.79, -0.22, 0);


//  vCoordSys := CoCoordSys.Create;

//  vCoordSys.Datum.Set_(3, 23.57, -140.95, -79.8, 0, -0.35, -0.79, -0.22, 0);

{
  vCoordSys.Set_(miLongLat,  vMapXDatum, // 9999, // DATUM_KRASOVKSY42,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam);
}

  {
  aMap.NumericCoordSys.Set_ (miLongLat,  vMapXDatum, //miLongLat, DATUM_KRASOVKSY42,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam);

   }
  aMap.NumericCoordSys.Set_ (miLongLat, DATUM_KRASOVKSY42,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam);

//
// procedure Set_(Type_: CoordSysTypeConstants; Datum: OleVariant; Units: OleVariant; 
//                   OriginLongitude: OleVariant; OriginLatitude: OleVariant; 
//                   StandardParallelOne: OleVariant; StandardParallelTwo: OleVariant; 
//                   Azimuth: OleVariant; ScaleFactor: OleVariant; FalseEasting: OleVariant; 
//                   FalseNorthing: OleVariant; Range: OleVariant; Bounds: OleVariant; 
//                   AffineTransform: OleVariant); dispid 15;    
//
//                   
end;

//------------------------------------------------------
procedure mapx_SetMapDefaultProjection_NumericCoordSys_KRASOVKSY42_WMS_(aMap: CMapX);
//------------------------------------------------------
var
  vCoordSys: CMapXCoordSys;
  vMapXDatum: CMapXDatum;

begin
  vMapXDatum:=CoDatum.create;
  vMapXDatum.Set_(3, 23.57, -140.95, -79.8, 0, -0.35, -0.79, -0.22, 0);


  vCoordSys := CoCoordSys.Create;

//  vCoordSys.Datum.Set_(3, 23.57, -140.95, -79.8, 0, -0.35, -0.79, -0.22, 0);

{
  vCoordSys.Set_(miLongLat,  vMapXDatum, // 9999, // DATUM_KRASOVKSY42,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam);
}


  aMap.NumericCoordSys.Set_ (miLongLat,  vMapXDatum, //miLongLat, DATUM_KRASOVKSY42,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam);

  {
  aMap.NumericCoordSys.Set_ (miLongLat, DATUM_KRASOVKSY42,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam);

  }
end;





begin

end.



{

//--------------------------------------------------------------------
function map_MapAdd_Ex_CMapX(aMap: CMapX; aFileName: string): CMapXLayer;
//--------------------------------------------------------------------
var
  bTile: Boolean;
  vLayer: CMapXLayer;
  sRegPath: string;
    i: Integer;

    iTileInd: Integer;
    
  s: string;
  sFileName: string;
  sName: string;
begin
  Result := nil;

  sName:= ExtractFileName(aFileName);

  bTile:=pos('tile',LowerCase(sName))=1;
                          
  iTileInd :=0;
//  iRasterInd :=0;  
                          
  
//  for i:=aMap.Layers.Count downto 1  do
  for i:=1 to aMap.Layers.Count do
  begin
    vLayer:=aMap.Layers.Item[i];

    sFileName:=vLayer.FileSpec;    

    case vLayer.Type_ of
        miLayerTypeRaster: ;                   
        miLayerTypeNormal: ;
    end;                       

    s:= LowerCase (ExtractFileName(sFileName));
    
    if LeftStr(s,4) = 'tile' then
      if iTileInd=0 then
        iTileInd:=i;

  end;


  Assert(FileExists(aFileName));

  // add TILE to end
  if bTile then
    vLayer:= aMap.Layers.Add (aFileName, aMap.Layers.Count+1)
    
  else begin      
  //  if bTile then
   //   vLayer:= aMap.Layers.Add (aFileName, 1)
   // else

    vLayer:= aMap.Layers.Add (aFileName, 1);
      

    if aMap.Layers.Count>1 then
      if not bTile then
        case vLayer.Type_ of
           miLayerTypeRaster: if iTileInd>0 then
                               aMap.Layers.Move(1, iTileInd) ;
       
//           miLayerTypeNormal:  aMap.Layers.Move(aMap.Layers.Count, 1) ;

        end;    


    
//    vLayer.
//    aMap.Layers.M
  end;
   

  Result := vLayer;
end;
}
