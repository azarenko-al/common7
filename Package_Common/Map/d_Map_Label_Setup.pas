unit d_Map_Label_Setup;

interface

uses
  SysUtils, Classes, Controls, Forms, 
  ExtCtrls, StdCtrls, ActnList, cxVGrid, cxDropDownEdit,

  u_Mapx_Style,

  MapXLib_TLB,

  d_wizard,

  u_func,
  u_cx_VGrid,
  u_mapx,

  cxControls, cxInplaceContainer, cxPropertiesStore, rxPlacemnt;


type
  Tdlg_Map_Label_Setup = class(Tdlg_Wizard)
    pn_Main: TPanel;
    cxVerticalGrid1: TcxVerticalGrid;
    cxVerticalGrid1EditorRow1: TcxEditorRow;
    row_Overlap: TcxEditorRow;
    row_DisplayWithinRange1: TcxEditorRow;
    row_HigherThan1: TcxEditorRow;
    row_LowerThan1: TcxEditorRow;
    cxVerticalGrid1EditorRow6: TcxEditorRow;
    row_BackgroundType: TcxEditorRow;
    row_Background_Color: TcxEditorRow;
    cxVerticalGrid1EditorRow9: TcxEditorRow;
    row_FontName: TcxEditorRow;
    row_FontSize: TcxEditorRow;
    row_Color: TcxEditorRow;
    row_Effects1: TcxEditorRow;
    row_DoubleInterval1: TcxEditorRow;
    row_AllCaps1: TcxEditorRow;
    row_FontIsBold1: TcxEditorRow;
    row_FontIsItalic1: TcxEditorRow;
    row_Shadows1: TcxEditorRow;
    cxVerticalGrid1EditorRow19: TcxEditorRow;
    cxVerticalGrid1EditorRow20: TcxEditorRow;
    row_RotateWithLine: TcxEditorRow;
    row_Position: TcxEditorRow;
    row_Offset: TcxEditorRow;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    GroupBox3: TGroupBox;
    CheckBox4: TCheckBox;
    GroupBox4: TGroupBox;
    cb_DoubleInterval: TCheckBox;
    ColorBox1: TColorBox;
    Edit1: TEdit;
    ColorBox2: TColorBox;
    Button1: TButton;
    ActionList_new: TActionList;
    act_Default: TAction;
    procedure act_DefaultExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
//    procedure Button1Click(Sender: TObject);
    procedure cxVerticalGrid1Edited(Sender: TObject; ARowProperties: TcxCustomEditorRowProperties);
//    procedure dx_BackgroundMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);

  private
//    FTabFileName : string;
    FLabelPropertiesRef: TmiLabelProperties;

 //   FMap: TMap;

//    FLayer: CMapXLayer;
    FRegPath: string;

    procedure LoadFromClass(aLabelProperties: TmiLabelProperties);
    procedure SaveToClass(aLabelProperties: TmiLabelProperties);

//    procedure SaveToRecord;

   // class function ExecDlg (aLayerName,aRegPath: string): Boolean;


  public
  //; aMap: TMap

    class function ExecDlg_Obj(aLayerName: string; aLabelProperties: TmiLabelProperties):         Boolean;

    class function ExecDlg_Obj_new(var aLabelStyleText: string): Boolean;

  end;

  //;   aMap: TMap

  
  function MapX_Label_Setup_Dlg___(var aLabelStyleText: String): Boolean; stdcall;


(*

exports
  MapX_Label_Setup_Dlg___;
*)


(*  function Tdlg_Map_Label_Setup_ExecDlg_Obj(aLayerName: string; aLabelProperties:
      TmiLabelProperties): Boolean;
*)

{
exports
  Tdlg_Map_Label_Setup_ExecDlg;
}

//========================================================
//implementation
//========================================================
implementation {$R *.DFM}


function MapX_Label_Setup_Dlg___(var aLabelStyleText: String): Boolean;
//var
//  iOldHandle : Integer;
begin
(*  iOldHandle :=Application.Handle;
  Application.Handle := aAppHandle;
*)

  Result := Tdlg_Map_Label_Setup.ExecDlg_Obj_new(aLabelStyleText); //aTabFileName,        //,  aMap

 // Application.Handle := iOldHandle;

end;


procedure Tdlg_Map_Label_Setup.act_DefaultExecute(Sender: TObject);
begin
  FLabelPropertiesRef.SetDefaultValues1;
  LoadFromClass(FLabelPropertiesRef);
end;

//--------------------------------------------------------
procedure Tdlg_Map_Label_Setup.FormCreate(Sender: TObject);
//--------------------------------------------------------
var
  oComboBoxProperties: TcxComboBoxProperties;
begin
  inherited;
 // ed_fonts.Visible:= false;

//  TcxComboBoxProperties(row_FontName.Properties.EditProperties).Items.Text:=ed_fonts.Items.Text;

 /////////////// row_FontName.Items.Text := ed_fonts.Items.Text;

 //zzzzzzzzz FTempMap:=TMap.Create(nil);

{
   Map1.Title.X:=0;
   Map1.Title.Y:=0;
   Map1.Title.Editable:= False;
   Map1.Title.Border:= False;
   Map1.Title.Caption:=' ';
   Map1.Title.Visible:=False;}


  cx_InitVerticalGrid(cxVerticalGrid1);

  //FTempMap:= TMap.Create(nil);

(*  FTempRect := CoRectangle.Create;
  FTempRect.Set_(0, 0, Image1.Height, Image1.Width);
*)
  pn_Main.Align:= alClient;


(*    ��� ����
���������������
�������
*)


(*  iValue:=TcxComboBoxProperties(
    row_BackgroundType.Properties.EditProperties).Items.IndexOf(
      row_BackgroundType.Properties.Value);
*)

  oComboBoxProperties:=TcxComboBoxProperties(row_BackgroundType.Properties.EditProperties);

  with oComboBoxProperties.Items do
  begin
    Clear;
    Add('��� ����');
    Add('���������������');
    Add('�������');
  end;

 // row_BackgroundType.Properties.Value:=oComboBoxProperties.Items.Strings[iIndex];;

end;


procedure Tdlg_Map_Label_Setup.FormDestroy(Sender: TObject);
begin
////////////////  FTempMap.Free;

  inherited;
end;



//--------------------------------------------------------
class function Tdlg_Map_Label_Setup.ExecDlg_Obj(aLayerName: string; aLabelProperties:
    TmiLabelProperties): Boolean;
//--------------------------------------------------------
var
  iIndex: Integer;
  rStyle: TmiStyleRec;
begin
  with Tdlg_Map_Label_Setup.Create(Application) do
  begin
    if aLayerName<>'' then
      lb_Action.Caption:=Format('��������� �������� ���� "%s"', [aLayerName])
    else
      lb_Action.Caption:='��������� ��������';

    FLabelPropertiesRef := aLabelProperties;


    LoadFromClass (aLabelProperties);


    Result:=ShowModal=mrOK;
    if Result then
      SaveToClass(aLabelProperties);

    Free;
  end;
end;


//--------------------------------------------------------
class function Tdlg_Map_Label_Setup.ExecDlg_Obj_new(var aLabelStyleText:
    string): Boolean;
//--------------------------------------------------------
var
  iIndex: Integer;
  rStyle: TmiStyleRec;

  oLabelProperties: TmiLabelProperties;
begin

  with Tdlg_Map_Label_Setup.Create(Application) do
  begin
    lb_Action.Caption:='��������� ��������';

//    FTabFileName := aTabFileName;

    oLabelProperties := TmiLabelProperties.Create;

    oLabelProperties.AsText := aLabelStyleText;

//    FMap := aMap;

    FLabelPropertiesRef := oLabelProperties;
    LoadFromClass(oLabelProperties);

   // RefreshSample();

    Result:=ShowModal=mrOK;

    if Result then
    begin
      SaveToClass(oLabelProperties);
      aLabelStyleText:=oLabelProperties.SaveToText;
    end;

    FreeAndNil(oLabelProperties);

    Free;
  end;
end;



// ---------------------------------------------------------------
procedure Tdlg_Map_Label_Setup.LoadFromClass(aLabelProperties: TmiLabelProperties);
// ---------------------------------------------------------------
var
  iIndex: integer;
  oComboBoxProperties: TcxComboBoxProperties;

//  oFontNameComboBoxProperties: TcxFontNameComboBoxProperties;
  i: integer;

begin
  if aLabelProperties.TextFontName='' then
    aLabelProperties.SetDefaultValues1;


  with  aLabelProperties do
  begin
    row_FontName.Properties.Value:=           TextFontName;
    row_FontSize.Properties.Value:=           TextFontSize;
//    row_FontIsBold1.Properties.Value:=         IsTextFontBold;
 //   row_FontIsItalic1.Properties.Value:=       IsTextFontItalic;

    row_Color.Properties.Value:=              TextFontColor;
    row_Background_Color.Properties.Value:=  (TextFontBackColor);

   // row_Shadows1.Properties.Value:=           (TextFontShadow);
  //  row_AllCaps1.Properties.Value:=           (TextFontAllCaps);
   // row_DoubleInterval.Properties.Value:=    (TextFontDblSpace);

    row_Overlap.Properties.Value:=           (Overlap);
    row_RotateWithLine.Properties.Value:=    (Parallel);
                     //    TcxComboBoxProperties(row_Position.Properties.EditProperties).Items.Text
    row_Position.Properties.Value:=TcxComboBoxProperties(row_Position.Properties.EditProperties).Items.Strings[Position];
    //row_Position.Items.Strings[Position];
    row_Offset.Properties.Value:=            (Offset);

(*
    row_DisplayWithinRange.Properties.Value:=(IsLabelZoom);
    row_HigherThan.Properties.Value:=        (LabelZoomMin);
    row_LowerThan.Properties.Value:=         (LabelZoomMax);
*)

    iIndex:= 0;
    if IsFontOpaque then iIndex:= 1 else
    if IsFontHalo   then iIndex:= 2;


(*    ��� ����
���������������
�������
*)


(*  iValue:=TcxComboBoxProperties(
    row_BackgroundType.Properties.EditProperties).Items.IndexOf(
      row_BackgroundType.Properties.Value);
*)

    oComboBoxProperties:=TcxComboBoxProperties(row_BackgroundType.Properties.EditProperties);
    i := oComboBoxProperties.Items.Count;
    row_BackgroundType.Properties.Value:=oComboBoxProperties.Items.Strings[iIndex];;

(*
///////////////////////
    oComboBoxProperties:=TcxComboBoxProperties(row_BackgroundType.Properties.EditProperties);
    i := oComboBoxProperties.Items.Count;


    row_BackgroundType.Properties.Value:=
      TcxComboBoxProperties(row_BackgroundType.Properties.EditProperties).Items.Strings[iIndex];
*)

   //row_BackgroundType.Items.Strings[iIndex];
  end;
end;



//--------------------------------------------------------
procedure Tdlg_Map_Label_Setup.SaveToClass(aLabelProperties: TmiLabelProperties);
//--------------------------------------------------------
var
  iValue: integer;
  //miStyle: TmiStyleRec;
  oComboBoxProperties: TcxComboBoxProperties;
//  oFontNameComboBoxProperties: TcxFontNameComboBoxProperties;

begin
  aLabelProperties.TextFontName      := row_FontName.Properties.Value;
  aLabelProperties.TextFontSize      := AsInteger(row_FontSize.Properties.Value);
 // aLabelProperties.IsTextFontBold      := (row_FontIsBold.Properties.Value);
 // aLabelProperties.IsTextFontItalic    := (row_FontIsItalic.Properties.Value);

  aLabelProperties.TextFontColor     := AsInteger(row_Color.Properties.Value);
  aLabelProperties.TextFontBackColor := AsInteger(row_Background_Color.Properties.Value);

//      row_Background_Color.Properties.Value:=  (TextFontBackColor);


 // aLabelProperties.TextFontShadow    := (row_Shadows.Properties.Value         );
//  aLabelProperties.TextFontAllCaps   := (row_AllCaps.Properties.Value         );
 // aLabelProperties.TextFontDblSpace  := (row_DoubleInterval.Properties.Value  );

  aLabelProperties.Overlap           := (row_Overlap.Properties.Value         );
  aLabelProperties.Parallel          := (row_RotateWithLine.Properties.Value  );

  iValue:=TcxComboBoxProperties(row_Position.Properties.EditProperties).Items.IndexOf(row_Position.Properties.Value);

  aLabelProperties.Position          := iValue;//row_Position.Items.IndexOf(row_Position1.Text);
  aLabelProperties.Offset            := AsInteger(row_Offset.Properties.Value          );

  (*
  aLabelProperties.IsLabelZoom       := AsBoolean(row_DisplayWithinRange.Properties.Value );
  aLabelProperties.LabelZoomMin      := AsInteger(row_HigherThan.Properties.Value         );
  aLabelProperties.LabelZoomMax      := AsInteger(row_LowerThan.Properties.Value          );
*)

  aLabelProperties.IsFontOpaque:= false;
  aLabelProperties.IsFontHalo  := false;

//  oFontNameComboBoxProperties:=TcxFontNameComboBoxProperties (row_BackgroundType.Properties.EditProperties);

  oComboBoxProperties:=TcxComboBoxProperties(row_BackgroundType.Properties.EditProperties);
  iValue:=oComboBoxProperties.Items.IndexOf(row_BackgroundType.Properties.Value);

//  oComboBoxProperties.

(*  iValue:=TcxComboBoxProperties(
    row_BackgroundType.Properties.EditProperties).Items.IndexOf(
      row_BackgroundType.Properties.Value);
*)


  case iValue of
    1: aLabelProperties.IsFontOpaque:= true;
    2: aLabelProperties.IsFontHalo  := true;
  end;


 //  mapx_LabelStyle_SaveToReg(miStyle, FRegPath);

end;

//--------------------------------------------------------
procedure Tdlg_Map_Label_Setup.act_OkExecute(Sender: TObject);
//--------------------------------------------------------
begin
 (* if Sender = act_Ok then
  begin
    SaveToRecord();
  end;*)

end;

procedure Tdlg_Map_Label_Setup.cxVerticalGrid1Edited(Sender: TObject; ARowProperties:
    TcxCustomEditorRowProperties);
begin
//  RefreshSample();
end;


end.


