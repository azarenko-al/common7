unit d_MIF_export;

interface

 {$DEFINE CLOSE}

// {$DEFINE wms}


uses
  Classes, Controls, Forms, Windows, Messages, SysUtils, OleCtrls,

  u_debug,
  x_tile_manager,

  u_Map_engine,

  MapXLib_TLB,

  u_MapX,
  u_GEO;




type

  TExportMapRec = record
    GeosetFileName: string;
    ImgFileName: string;

    Scale: integer;

//    Mode: (mtNone,mtVEctor);//,mtPoint); //,mtRect);

//    BLPoint1: TblPoint;

    BLVector: TblVector;
 //   BLRect: TBLRect;

    Width_cm,
    Height_cm: double;

 //   IsDebug : Boolean;

//    MapFileList : TStringList;
  end;


type

  Tdlg_MIF_export1 = class(TForm)
    Map1: TMap;
    procedure FormDestroy(Sender: TObject);
//    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
//    procedure FormShow(Sender: TObject);
//    procedure ToolButton1Click(Sender: TObject);

  protected

  private
    FMapEngine: TMapEngine;


    FMap: TMap;
    FParams: TExportMapRec;

    procedure ExportMap;
    procedure Insert_wms_map_;
//    procedure Insert_wms_map_ActiveX;

  public
    Params: record

       WMS : record
         Checked   : boolean;

         LayerName : string;
         Dir       : string;

         Z: integer;
         IsAutoScale : boolean;

         URL : string;
         EPSG: integer;
         //Name: string;

       end;    
    end;

    class procedure Execute(aRec: TExportMapRec);
    procedure ExportMap_new(aFileName: string);
  end;

//procedure MIF_ExportMap (aRec: TExportMapRec);


implementation
{$R *.dfm}


class procedure Tdlg_MIF_export1.Execute(aRec: TExportMapRec);
begin
  with Tdlg_MIF_export1.Create(Application) do
  begin
    FParams:=aRec;

    ExportMap;

    Free;
  end;
end;

// -------------------------------------------------------------------
procedure Tdlg_MIF_export1.ExportMap;
// -------------------------------------------------------------------
var
  s: string;

  BLPoint: TBLPoint;
begin
  FMap.Geoset:=FParams.GeosetFileName;

//  if FParams.Scale=0 then
//    FParams.Scale:=100000;    

  FMap.PaperUnit:= miUnitCentimeter;

  FMap.SetSize(Round (FParams.Width_cm  * (Screen.PixelsPerInch )/2.54),
               Round (FParams.Height_cm * (Screen.PixelsPerInch )/2.54)  );
      

    BLPoint:=geo_GetVectorCenter (FParams.BLVector);


    FMapEngine.SetCenter_LatLon(BLPoint.B, BLPoint.L);

    if Params.Wms.Checked and Params.Wms.IsAutoScale then
      FMapEngine.SetBoundsForBLVector(FParams.BLVector);

    FMapEngine.SetScale(FParams.Scale/100);



  {$IFDEF CLOSE}
   PostMessage(Handle, WM_CLOSE, 0,0);
  {$ENDIF}

  ShowModal;

  FMap.ExportMap (FParams.ImgFileName, miFormatBmp);
end;


// ---------------------------------------------------------------
procedure Tdlg_MIF_export1.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  Caption:='�������� �����...';

  {$IFDEF CLOSE}
//  Top:=3000;
  {$ENDIF}

  FMap:=Map1;


  Map1.Title.X:=0;
  Map1.Title.Y:=0;
//  FMap.Title.Editable:= False;
  Map1.Title.Border:= False;
  Map1.Title.Caption:='-';
  Map1.Title.Visible:=False;

  FMap.AutoRedraw := False;

  FMapEngine:=TMapEngine.Create(Map1);

end;


procedure Tdlg_MIF_export1.FormDestroy(Sender: TObject);
begin
  FreeAndNil (FMapEngine);
end;



// -------------------------------------------------------------------
procedure Tdlg_MIF_export1.ExportMap_new(aFileName: string);
// -------------------------------------------------------------------
var
  s: string;
begin
//  FMap.AutoRedraw := False;


  {$IFDEF CLOSE}
   PostMessage(Handle, WM_CLOSE, 0,0);
  {$ENDIF}


//  FMap.;
//  FMap.ExportMap ('d:\aaaaa.bmp', miFormatBmp);



  ShowModal;

//  Application.ProcessMessages;

//{$IFDEF wms}
  if Params.WMS.Checked then
    Insert_wms_map_ ();
//    Insert_wms_map_ActiveX ();

//{$ENDIF}


  FMap.AutoRedraw := True;


  FMap.ExportMap (aFileName, miFormatBmp);


end;



// ---------------------------------------------------------------
procedure Tdlg_MIF_export1.Insert_wms_map_;
// ---------------------------------------------------------------

var
  vTileManager: TTileManagerX;

//  oWMS: TWmsComClient;

begin
  if not Params.WMS.Checked then
    exit;

  debug ('Tdlg_MIF_export1.Insert_wms_map_...');

  vTileManager:=TTileManagerX.Create;
//  WMS_Init_ITileManagerX();

//  oWMS:=TWmsComClient.Create;

//  vTileManager:=oWMS.Intf;

//  vTileManager:=CoTileManagerX.Create;

  debug ('vTileManager.Init(Map1)');

  vTileManager.Init(Map1);


//  DbugIntf.Se

  //FMapEngine.


 // oTileManager:=TTileManager.Create;

  assert(Params.WMS.Dir<>'', 'Params.WMS.Dir null');


  vTileManager.Set_Dir(  Params.WMS.Dir);
//  FTileManagerX.Set_Dir(IncludeTrailingBackslash(Trim(DirectoryEdit_WMS.Text)));

//  WMS.LayerName:='GoogleMap';

//  assert(Params.WMS.LayerName<>'', 'Params.WMS.LayerName null');


//  if Params.WMS.LayerName='' then
  //  Params.WMS.LayerName:='YandexSat';
  Assert (Params.WMS.URL<>'');
  Assert (Params.WMS.EPSG>0);

  vTileManager.Set_WMS_Layer (Params.WMS.LayerName, Params.WMS.URL, Params.WMS.EPSG, 0);

  {
function TTileManagerX.Set_WMS_layer(aName: WideString; aURL: string; aEPSG:
    word; aZ_max: byte): HResult;
}

  if (not Params.WMS.IsAutoScale) and (Params.WMS.Z>0) then
    vTileManager.Set_Z(Params.WMS.Z);


// ---------------------------------------------------------------  if Params.WMS.Z>0 then
//    vTileManager.Set_Z (Params.WMS.Z);

  debug ('vTileManager.Load_CMap()');
  vTileManager.Load_CMap();


//  vTileManager:=nil;

//  _WMS_Update;

  FreeAndNil (vTileManager);

//  FreeAndNil (oWMS);


 // DbugIntf.
end;



end.



{
// ---------------------------------------------------------------
procedure Tdlg_MIF_export1.Insert_wms_map_ActiveX;
// ---------------------------------------------------------------

var
  vTileManager: ITileManagerX;

//  oWMS: TWmsComClient;

begin
  if not Params.WMS.Checked then
    exit;


  vTileManager:=WMS_Init_ITileManagerX();

//  oWMS:=TWmsComClient.Create;

//  vTileManager:=oWMS.Intf;

//  vTileManager:=CoTileManagerX.Create;
  vTileManager.Init(Map1.DefaultInterface);


//  DbugIntf.Se

  //FMapEngine.


 // oTileManager:=TTileManager.Create;

  assert(Params.WMS.Dir<>'', 'Params.WMS.Dir null');


  vTileManager.Set_Dir(  Params.WMS.Dir);
//  FTileManagerX.Set_Dir(IncludeTrailingBackslash(Trim(DirectoryEdit_WMS.Text)));

//  WMS.LayerName:='GoogleMap';

  assert(Params.WMS.LayerName<>'', 'Params.WMS.LayerName null');


//  if Params.WMS.LayerName='' then
  //  Params.WMS.LayerName:='YandexSat';

  vTileManager.Set_WMS_Layer (Params.WMS.LayerName);

  if Params.WMS.Z>0 then
    vTileManager.Set_Z(Params.WMS.Z);


// ---------------------------------------------------------------  if Params.WMS.Z>0 then
//    vTileManager.Set_Z (Params.WMS.Z);

  vTileManager.Load_CMap();

//  vTileManager:=nil;

//  _WMS_Update;

//  FreeAndNil (oTileManager);

//  FreeAndNil (oWMS);


 // DbugIntf.
end;
}

