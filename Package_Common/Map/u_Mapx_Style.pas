unit u_Mapx_Style;

interface
uses
  SysUtils, Classes, IniFiles,Variants, Graphics,
  MapXLib_TLB;

type
  TmiLabelProperties = class
  private
  //  FText : string;

 //   function GetAsText: string;
    procedure SetAsText(const Value: string);

  public
    Text : string;
  public
    TextFontName      : string;
    TextFontSize      : integer;


    TextFontColor     : integer;
    TextFontBackColor : integer;
    TextFontRotation  : integer;

    Overlap           : boolean;
    Parallel          : boolean;   //������������ ������� � ������


    Position          : integer;
    Offset            : integer;

    LabelZoomMax      : integer;
    LabelZoomMin      : integer;
    IsLabelZoom       : Boolean;

    IsFontOpaque      : boolean;
    IsFontHalo        : boolean;


    constructor Create;

    procedure SaveToIniFile(aIni: TIniFile; aSection: string);
    procedure LoadFromIniFile(aIni: TIniFile; aSection: string);

    procedure LoadFromMapinfoLayer(aVLayer: CMapXLayer);
    procedure SaveToMapinfoLayer(aVLayer: CMapXLayer);

    function SaveToText: string;
    procedure LoadFromText(aText: string);

    procedure SetDefaultValues1;

    property AsText: string write SetAsText;
  end;

  // ---------------------------------------------------------------
  TmiLabel = class
  public
    Checked : boolean;
    ObjName: string;

    AUTOLABEL : boolean;

    LabelProperties: TmiLabelProperties;

    constructor Create;
    destructor Destroy; override;
  end;

  // ---------------------------------------------------------------
  TmiLabelList = class(TStringList)
  // ---------------------------------------------------------------
  private

    function GetItems(Index: Integer): TmiLabel;
  public
    IniFileName: string;


    function GetItemByName(aName: string): TmiLabel;

    constructor Create;
    function AddItem(aObjName: string): TmiLabel;

    property Items[Index: Integer]: TmiLabel read GetItems; default;

//    property ItemByName[Index: string]: TmiLabel read GetItemByName;


    procedure SaveToIniFile(aIniFile: string);
    procedure LoadFromIniFile(aIniFile: string);

    procedure LoadFromDir(aDir: string);

  end;



  TmiStyleObj = class
  public
    LineColor            : integer;
    LineWidth            : integer;
    LineStyle            : integer;

    Character            : integer;
    FontColor            : integer;
    FontSize             : integer;
    FontName             : string;

    //Region Border
    RegionBorderWidth    : integer;
    RegionBorderStyle    : integer;
    RegionBorderColor    : integer;

    //Region fill
    RegionPattern        : integer;
    RegionForegroundColor: integer;  //���� ����� ��� �����
    RegionBackgroundColor: integer;
    RegionIsTransparent  : boolean;

  //
  //  IsFontHalo           : boolean;
    SymbolFontRotation   : integer;

    FontIsItalic         : Boolean;
    FontIsOpaque         : boolean;
    FontIsHalo           : boolean;
    FontBackColor        : integer;
    FontRotation         : integer;

    LabelProperties: TmiLabelProperties;
  public
    constructor Create;
    destructor Destroy; override;
  end;


implementation


constructor TmiStyleObj.Create;
begin
  inherited;

  LabelProperties := TmiLabelProperties.Create();
end;

destructor TmiStyleObj.Destroy;
begin
  FreeAndNil(LabelProperties);

  inherited;
end;


constructor TmiLabelProperties.Create;
begin
  inherited;
  SetDefaultValues1();
end;

//-------------------------------------------------------------------
procedure TmiLabelProperties.LoadFromMapinfoLayer(aVLayer: CMapXLayer);
//-------------------------------------------------------------------
var

  v: CMapXLabelProperties;

begin
//vLayer: CMapXLayer;;

  Assert(Assigned(aVLayer), 'Value not assigned');

(*  aVLayer.LabelProperties



*)
//   if LabelProperties.TextFontSize>0 then
  v :=aVLayer.LabelProperties;
//    with aVLayer.LabelProperties do
 //   begin

  TextFontName      := OleVariant(v.Style.TextFont).Name;
  TextFontSize      := OleVariant(v.Style.TextFont).Size;


  TextFontColor     := v.Style.TextFontColor;
  TextFontBackColor := v.Style.TextFontBackColor;


  Overlap           := v.Overlap;
  Parallel          := v.Parallel;

  Position          := v.Position;

  Offset            := v.Offset;

  LabelZoomMax      := Round(v.LabelZoomMax);
  LabelZoomMin      := Round(v.LabelZoomMin);
  IsLabelZoom       := v.LabelZoom;

  IsFontOpaque      := v.Style.TextFontOpaque;
  IsFontHalo        := v.Style.TextFontHalo;

  //  end;

end;

//-------------------------------------------------------------------
procedure TmiLabelProperties.SaveToMapinfoLayer(aVLayer: CMapXLayer);
//-------------------------------------------------------------------
var
  v: CMapXLabelProperties;
begin
//vLayer: CMapXLayer;;

  Assert(Assigned(aVLayer), 'Value not assigned');

  if TextFontName='' then
    Exit;
               

(*  aVLayer.LabelProperties


*)
//   if LabelProperties.TextFontSize>0 then
  v :=aVLayer.LabelProperties;
//    with aVLayer.LabelProperties do
 //   begin

  OleVariant(v.Style.TextFont).Name  := TextFontName;
  OleVariant(v.Style.TextFont).Size  := TextFontSize;


  v.Style.TextFontColor     := ColorToRGB(TextFontColor);
  v.Style.TextFontBackColor := ColorToRGB(TextFontBackColor);


  v.Overlap                 := Overlap;
  v.Parallel                := Parallel;

  v.Position                := Position;

  v.Offset                  := Offset;

  v.LabelZoomMax            := LabelZoomMax;
  v.LabelZoomMin            := LabelZoomMin;
  v.LabelZoom               := IsLabelZoom;

  v.Style.TextFontOpaque    := IsFontOpaque;
  v.Style.TextFontHalo      := IsFontHalo;

  //  end;

end;

// ---------------------------------------------------------------
procedure TmiLabelProperties.LoadFromIniFile(aIni: TIniFile; aSection: string);
// ---------------------------------------------------------------
begin
  Assert(Assigned(aIni), 'Value not assigned');

  if not aIni.SectionExists(aSection) then
    Exit;


  with aIni do
  begin
    TextFontName     :=ReadString (aSection, 'TextFontName', TextFontName);
    TextFontSize     :=ReadInteger(aSection, 'TextFontSize', TextFontSize);

    TextFontColor    :=ReadInteger(aSection, 'TextFontColor', TextFontColor);
    TextFontBackColor:=ReadInteger(aSection, 'TextFontBackColor', TextFontBackColor);

    Overlap          :=ReadBool   (aSection, 'Overlap',         Overlap           );

//    TextFontShadow   :=ReadBool   (aSection, 'TextFontShadow',  TextFontShadow    );
//    TextFontAllCaps  :=ReadBool   (aSection, 'TextFontAllCaps', TextFontAllCaps   );
//    TextFontDblSpace :=ReadBool   (aSection, 'TextFontDblSpace',TextFontDblSpace  );

    Parallel         :=ReadBool   (aSection, 'Parallel', Parallel );
    Position         :=ReadInteger(aSection, 'Position', Position );
    Offset           :=ReadInteger(aSection, 'Offset',   Offset   );

    IsFontOpaque      :=ReadBool(aSection, 'IsFontOpaque', IsFontOpaque);
    IsFontHalo        :=ReadBool(aSection, 'IsFontHalo',   IsFontHalo  );


    if TextFontName='' then TextFontName:='Arial';
    if TextFontSize=0  then TextFontSize:=10;

  //  IsShowLabel         :=ReadBool('', 'IsShowLabel',   True);
   // No_Back             :=ReadBool('', 'No_Back',      false);
  end;

end;

// ---------------------------------------------------------------
procedure TmiLabelProperties.LoadFromText(aText: string);
// ---------------------------------------------------------------
var
  oSList: TStringList;
  s: string;
begin
  Text :=aText;

  if aText='' then
  begin
    SetDefaultValues1;
    Exit;
  end;


  oSList:=TStringList.Create;
  oSList.Text := aText;

  TextFontName  := oSList.Values['TextFontName'];
  TextFontSize  := StrToIntDef (oSList.Values['TextFontSize'],    TextFontSize);


  TextFontColor    :=StrToIntDef (oSList.Values['TextFontColor'],     TextFontColor);
  TextFontBackColor:=StrToIntDef (oSList.Values['TextFontBackColor'], TextFontBackColor);
  TextFontRotation :=StrToIntDef (oSList.Values['TextFontRotation'],  TextFontRotation);

  Overlap          :=StrToBoolDef (oSList.Values['Overlap'], Overlap);
  Parallel         :=StrToBoolDef (oSList.Values['Parallel'],Parallel);   //������������ ������� � ������


  Position         :=StrToIntDef (oSList.Values['Position'], Position);
  Offset           :=StrToIntDef (oSList.Values['Offset'],   Offset);

  LabelZoomMax     :=StrToIntDef  (oSList.Values['LabelZoomMax'], LabelZoomMax);
  LabelZoomMin     :=StrToIntDef  (oSList.Values['LabelZoomMin'], LabelZoomMin);
  IsLabelZoom      :=StrToBoolDef (oSList.Values['LabelZoom'],    IsLabelZoom);

  IsFontOpaque     :=StrToBoolDef (oSList.Values['FontIsOpaque'],IsFontOpaque);
  IsFontHalo       :=StrToBoolDef (oSList.Values['FontIsHalo'],  IsFontHalo);
                         
  s := oSList.Text;

  FreeAndNil(oSList);

end;


// ---------------------------------------------------------------
procedure TmiLabelProperties.SaveToIniFile(aIni: TIniFile; aSection: string);
// ---------------------------------------------------------------
begin
  Assert(Assigned(aIni), 'Value not assigned');

  with aIni do
//  with LabelProperties, aIni do
  begin
//    WriteBool   ('', 'IsShowLabel',   IsShowLabel );
    WriteString (aSection, 'TextFontName',      TextFontName);
    WriteInteger(aSection, 'TextFontSize',      TextFontSize);
    WriteInteger(aSection, 'TextFontColor',     TextFontColor);
    WriteInteger(aSection, 'TextFontBackColor', TextFontBackColor);

    WriteBool   (aSection, 'Overlap',           Overlap);
  //  WriteBool   (aSection, 'TextFontShadow',    TextFontShadow);
  //  WriteBool   (aSection, 'TextFontAllCaps',   TextFontAllCaps);
 //   WriteBool   (aSection, 'TextFontDblSpace',  TextFontDblSpace);

    WriteBool   (aSection, 'Parallel',          Parallel);
    WriteInteger(aSection, 'Position',          Position);
    WriteInteger(aSection, 'Offset',            Offset);

    WriteBool   (aSection, 'FontIsOpaque',  IsFontOpaque );
    WriteBool   (aSection, 'FontIsHalo',    IsFontHalo   );


//    WriteBool('', 'No_Back',       No_Back      );
  end;

end;


// ---------------------------------------------------------------
function TmiLabelProperties.SaveToText: string;
// ---------------------------------------------------------------
var
  oSList: TStringList;
  s: string;
begin
  oSList:=TStringList.Create;

  oSList.Values['TextFontName']    := TextFontName;
  oSList.Values['TextFontSize']    := VarToStr (TextFontSize);   

  oSList.Values['TextFontColor'    ]:= VarToStr (TextFontColor);
  oSList.Values['TextFontBackColor']:= VarToStr (TextFontBackColor);
  oSList.Values['TextFontRotation' ]:= VarToStr (TextFontRotation );

  oSList.Values['Overlap'          ]:= VarToStr (Overlap);
  oSList.Values['Parallel'         ]:= VarToStr (Parallel);   //������������ ������� � ������

  oSList.Values['Position'         ]:= VarToStr (Position);
  oSList.Values['Offset'           ]:= VarToStr (Offset);

  oSList.Values['LabelZoomMax'     ]:= VarToStr (LabelZoomMax);
  oSList.Values['LabelZoomMin'     ]:= VarToStr (LabelZoomMin);
  oSList.Values['LabelZoom'        ]:= VarToStr (IsLabelZoom);

  oSList.Values['FontIsOpaque'     ]:= VarToStr (IsFontOpaque);
  oSList.Values['FontIsHalo'       ]:= VarToStr (IsFontHalo);
             

  s := oSList.Text;

  Result := oSList.Text;

  FreeAndNil(oSList);


end;


// ---------------------------------------------------------------
procedure TmiLabelProperties.SetDefaultValues1;
// ---------------------------------------------------------------
begin
  TextFontName     :='Arial';
  TextFontSize     :=10;

  TextFontColor    :=0;
  TextFontBackColor:=65535;
  TextFontRotation :=0;

  Overlap          :=false;
  Parallel         :=true;

  Position         :=5;  //������ //������
  Offset           :=3;  //iDefLinkPos

  LabelZoomMax     :=100;
  LabelZoomMin     :=0;
  IsLabelZoom        :=false;

  IsFontOpaque     :=true;
  IsFontHalo       :=false;

  if Position>100 then
    Position:=5;

end;



procedure TmiLabelProperties.SetAsText(const Value: string);
begin
  LoadFromText(Value);
end;

constructor TmiLabelList.Create;
begin
  inherited create; // (TmiLabel);


  AddItem('link');
  AddItem('property');
  AddItem('linkline');

end;

function TmiLabelList.AddItem(aObjName: string): TmiLabel;
begin
  Result := TmiLabel.Create;
  Result.ObjName:=aObjName;

  AddObject(aObjName, Result); ;

end;

// ---------------------------------------------------------------
function TmiLabelList.GetItemByName(aName: string): TmiLabel;
// ---------------------------------------------------------------
var
  k: Integer;
begin
  k:=IndexOf(aName);
  if k>=0 then
    result := TmiLabel ( Objects[k])
  else
    result := nil;

 // Result := ;
end;

function TmiLabelList.GetItems(Index: Integer): TmiLabel;
begin
  Result := TmiLabel(Objects[Index]);
end;

procedure TmiLabelList.LoadFromDir(aDir: string);
begin
  LoadFromIniFile (aDir + 'labels.ini');
end;

// ---------------------------------------------------------------
procedure TmiLabelList.LoadFromIniFile(aIniFile: string);
// ---------------------------------------------------------------
var
  I: Integer;
  oIni: TIniFile;
  oItem: TmiLabel;
begin
  IniFileName:=aIniFile;

  oIni:=TIniFile.create(aIniFile);

  for I := 0 to Count - 1 do
  begin
    oItem:=Items[i];//.ObjName;

    oItem.Checked:=oIni.ReadBool(oItem.ObjName, 'Checked', True);
    oItem.AUTOLABEL:=oIni.ReadBool(oItem.ObjName, 'AUTOLABEL', True);

    oItem.LabelProperties.LoadFromIniFile(oIni, oItem.ObjName);

  end;


  FreeAndNil (oIni);
end;

// ---------------------------------------------------------------
procedure TmiLabelList.SaveToIniFile(aIniFile: string);
// ---------------------------------------------------------------
var
  I: Integer;
  oIni: TIniFile;
  oItem: TmiLabel;
  s: string;
begin
  DeleteFile(aIniFile);

  oIni:=TIniFile.create(aIniFile);

  for I := 0 to Count - 1 do
  begin
    oItem:=Items[i];//.ObjName;

    oIni.WriteBool(oItem.ObjName, 'Checked', oItem.Checked);
    oIni.WriteBool(oItem.ObjName, 'AUTOLABEL', oItem.AUTOLABEL);

    oItem.LabelProperties.SaveToIniFile(oIni, oItem.ObjName);

  end;

  FreeAndNil (oIni);

end;

constructor TmiLabel.Create;
begin
  inherited Create();
  LabelProperties:=TmiLabelProperties.Create();
end;

destructor TmiLabel.Destroy;
begin
  FreeAndNil(LabelProperties);
  inherited Destroy;
end;



procedure Test;
var
  oList: TmiLabelList;
begin
  oList:=TmiLabelList.Create;

  oList.SaveToIniFile('d:\aaa.ini');

end;


begin
 // test;

(*
var
  obj: TmiLabelProperties;
begin
  obj:=TmiLabelProperties.create;
  obj.SaveToText;
*)
end.


