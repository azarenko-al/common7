unit u_Mapinfo_WOR_classes;

interface

uses
  Classes, SySutils, Graphics,

   MapXLib_TLB,

//   u_arr,

  u_str,

  u_func,
  u_files,

  u_func_arrays,

  u_mapX;


type
//  TworMap = class;
  TworTable = class;
//  TworTableList = class;
  TWorkspace = class;

  // -------------------------------------------------------------------
  TworTable = class(TCollectionItem)
  // -------------------------------------------------------------------
  public
    Name : string;
    FileName : string;
    LayerName: string;
//    Alias : string;

    Type_ : Integer;


    ZoomMin, ZoomMax: double;
    IsZoom: boolean;

    LabelProperties_Style:
      record
        TextFontName : string;
        TextFontSize : Integer;
        TextFontBackColor : Integer;

        Position : Integer;

        IsAutoLabel : boolean;
        KeyField : string;

      end;

(*
    sName     :=OleVariant(vLayer.LabelProperties.Style.TextFont).Name;
    iSize     :=OleVariant(vLayer.LabelProperties.Style.TextFont).Size;
    iColorBack:=vLayer.LabelProperties.Style.TextFontBackColor;
    sPos      :=PositionToStr(vLayer.LabelProperties.Position);
*)
  end;



  TWorkspace = class (TCollection)
  private
    function GetItems(Index: Integer): TworTable;

  public
    CenterX : double;
    CenterY : double;
    Zoom    : double;

    OrderedFileList: TStringList;

  public
    constructor Create;

    function GetItemByName(aName: string): TworTable;


    procedure LoadFromFile(aFileName: string);
//    procedure LoadFromMap(aMap: TMap);
    procedure SaveToFile(aFileName: string);

    function AddItem: TworTable;

    property Items[Index: Integer]: TworTable read GetItems; default;

  end;



implementation

uses StrUtils;



  //--------------------------------------------------------
  procedure ColorTo_R_G_B(Value: TColor; var R,G,B: byte);
  //--------------------------------------------------------
  var cl: packed record case b:byte of
            0: (int: TColor);   1: (RGB: array[0..4] of byte);
          end;
  begin
    cl.int:=Value;
    R:=cl.rgb[0];  G:=cl.rgb[1];  B:=cl.rgb[2];
  end;


  function MakeMapinfoColor (aColor: integer): integer;
  var r,g,b: byte;
  begin
    ColorTo_R_G_B (aColor, r,g,b);
    Result:=(r*65536) + (g * 256) + b;
  end;





//LoadFromFile(aFileName: string);


constructor TWorkspace.Create;
begin
  inherited Create(TworTable);


  OrderedFileList:=TStringList.Create;

end;



function TWorkspace.AddItem: TworTable;
begin
  Result := TworTable (inherited Add);
end;

// ---------------------------------------------------------------
function TWorkspace.GetItemByName(aName: string): TworTable;
// ---------------------------------------------------------------
var
  i: Integer;
begin
  Result := nil;

  for i:=0 to Count-1 do
    if Items[i].LayerName = aName then
    begin
      Result := Items[i];
      Exit; 
    end;

end;


function TWorkspace.GetItems(Index: Integer): TworTable;
begin
  Result := TworTable(Inherited Items[index]);
end;




procedure TWorkspace.SaveToFile(aFileName: string);

     //-------------------------------------------------------------------
     function GetMapinfoShortFileName(aFileName: string): string;
     //-------------------------------------------------------------------
     var
       s: string;
     begin
      aFileName:=ExtractFileNameNoExt(aFileName) ;
      aFileName:=ReplaceStr(aFileName,' ','_');
      aFileName:=ReplaceStr(aFileName,'.','_');
      aFileName:=ReplaceStr(aFileName,'-','_');
      aFileName:=ReplaceStr(aFileName,',','_');

      s:=Copy(aFileName, 1,1);
      if Copy(aFileName, 1,1)[1] in ['0'..'9'] then
        aFileName:='_'+ aFileName;

      Result := aFileName;
    end;


    function MiPositionToStr(aValue: Integer): string;
    begin
      case aValue of
        miPositionCC : Result := 'Center';
        miPositionTL : Result := 'Above RIGHT';
        miPositionTC : Result := 'Above Left';
        miPositionTR : Result := 'Above Left';
        miPositionCL : Result := 'LEFT';
        miPositionCR : Result := 'RIGHT';
        miPositionBL : Result := 'Below Left';
        miPositionBC : Result := 'Below';
        miPositionBR : Result := 'Below RIGHT';
      end;

    end;


    function StrToMiPosition(aValue: string): Integer;
    begin
     (* case aValue of
        miPositionCC : Result := 'Center';
        miPositionTL : Result := 'Above RIGHT';
        miPositionTC : Result := 'Above Left';
        miPositionTR : Result := 'Above Left';
        miPositionCL : Result := 'LEFT';
        miPositionCR : Result := 'RIGHT';
        miPositionBL : Result := 'Below Left';
        miPositionBC : Result := 'Below';
        miPositionBR : Result := 'Below RIGHT';
      end;
*)
    end;



const
  CRLF = #13+#10;

var
  v: Variant;
 // vField : CMapXField;

  sKeyField: string;
  st: string;
  bAuto: Boolean;
  sPos: string;
  iColorBack: integer;
  iSize: integer;
  sFont: string;
  sName: string;
  sFilename: string;
  i: integer;
  S,sNames: string;
 // v: Variant;

 // vLayer: CMapxLayer;
   oTable: TworTable;


  oSList: TStringList;
  s1: string;
begin
  oSList:=TStringList.Create;


(*  oSList.Add('!Workspace');
  oSList.Add('!Version 600');
  oSList.Add('!Charset WindowsCyrillic');

*)
  s1:= '!Workspace' + CRLF +
      '!Version 600' + CRLF +
      '!Charset WindowsCyrillic';// + CRLF;


  oSList.Add(s1);

  sNames := '';

  for i := 0 to Count-1 do
  begin
    oTable :=Items[i];

    //u_mapX_tools

//    sFilename:=aMap.Layers.Item[i].FileSpec;
    sFilename:=oTable.FileName;

  //  st:=aMap.Layers.Item[i].KeyField;


    sName:=GetMapinfoShortFileName(sFilename) ;
  //  map1.Layers.Item[i].Name;

    s1:=Format('Open Table "%s" As %s Interactive', [sFilename,sName]);
    oSList.Add(s1);

    s:=s+ Format('Open Table "%s" As %s Interactive', [sFilename,sName]) + CRLF;

    sNames:=sNames+ sName + IIF(i<Count-1,',','');

  end;

  //Map From pmp_site,property,aqua,pmp_sector_ant
  s:=s+ Format('Map From %s', [sNames]) + CRLF ;

  s1:=Format('Map From %s', [sNames]);
  oSList.Add(s1);


  s:=s+ 'Set Map ' + CRLF;
  oSList.Add('Set Map ');

  oSList.Add('  CoordSys Earth Projection 1, 1001 ');

  s1:=Format('  Center (%1.8f,%1.8f)  ',  [CenterX, CenterY]);
  oSList.Add(s1);

  s1:=Format('  Zoom %1.6f Units "km" ', [Zoom]);
  oSList.Add(s1);

     // s+
  s1:= '  CoordSys Earth Projection 1, 1001 '+ CRLF +
       Format('  Center (%1.8f,%1.8f)  ',  [CenterX, CenterY]) + CRLF +
       Format('  Zoom %1.6f Units "km" ', [Zoom]); // + CRLF ;

  oSList.Add(s1);

  s:=s+ 'Set Map '+ CRLF;

  oSList.Add('Set Map ');

  for i := 0 to Count-1   do
  begin
    oTable :=Items[i];


//    vLayer:=aMap.Layers.Item[i];

  //  vLayer.LabelProperties.Visible

    s:=s+ Format('  Layer %d      ', [i+1])+ CRLF;

    s1:=Format('  Layer %d      ', [i+1]);
    oSList.Add(s1);


    case oTable.Type_ of

        miLayerTypeRaster:
        begin
          s1:= '  Display Graphic  ' + CRLF +
               '  Selectable Off   ' + CRLF ;
          s:=s+ s1;

          oSList.Add(s1);
        end;


        miLayerTypeNormal:
        begin
           // v := vLayer.LabelProperties.Visible;
           // sName:=AsString(v);
            sName     :=oTable.LabelProperties_Style.TextFontName;// OleVariant(vLayer.LabelProperties.Style.TextFont).Name;
            iSize     :=oTable.LabelProperties_Style.TextFontSize;// OleVariant(vLayer.LabelProperties.Style.TextFont).Size;
            iColorBack:=oTable.LabelProperties_Style.TextFontBackColor;//  vLayer.LabelProperties.Style.TextFontBackColor;
            sPos      :=MiPositionToStr(oTable.LabelProperties_Style.Position);

        //    v:=vLayer.LabelProperties.DataField;

            if Pos('cyr',sName)=0 then
              sName:=sName + ' cyr';


            sFont:= Format('("%s",0,%d,0,%d)',
                      [sName,
                       iSize,
                       MakeMapinfoColor(iColorBack)
                      ]);

(*
           if (vLayer.Datasets.Count = 0) then
             aMap.Datasets.Add (miDataSetLayer, vLayer, EmptyParam, EmptyParam,
                                EmptyParam, EmptyParam, EmptyParam, EmptyParam) ;

*)(*
            vField:=vLayer.LabelProperties.DataField as CMapXField;
            if Assigned(vField) then
              sKeyField:=vField.Name;*)

            bAuto:=oTable.LabelProperties_Style.IsAutoLabel;//  vLayer.AutoLabel;
            sKeyField:=oTable.LabelProperties_Style.KeyField;// vLayer.KeyField;

            s1:= Format('    Label Line Arrow Position %s Font %s Pen (1,2,0)', [sPos,sFont])+ CRLF +
       IIF(sKeyField<>'','    With '+ sKeyField+ CRLF,'') +
//                  Format('    With %s       ', [sName])+ CRLF +
                  Format('    Parallel On Auto %s ', [IIF(bAuto,'On','Off') ])+
                             'Overlap Off Duplicates On Offset 2'+ CRLF +
                         '    Visibility On '; // + CRLF;



            s:=s+ s1;
            oSList.Add(s1);


{            Format('    Label Line Arrow Position %s Font %s Pen (1,2,0)', [sPos,sFont])+ CRLF +
       IIF(sKeyField<>'','    With '+ sKeyField+ CRLF,'') +
//                  Format('    With %s       ', [sName])+ CRLF +
                  Format('    Parallel On Auto %s ', [IIF(bAuto,'On','Off') ])+
                             'Overlap Off Duplicates On Offset 2'+ CRLF +
                         '    Visibility On ' + CRLF;
}

        end;
    //  end;
    end;
  end;

//  StrToTxtFile (s, aWorFileName);

  ForceDirByFileName(aFileName);

  oSList.SaveToFile (aFileName);
  FreeAndNil(oSList);

end;

//---------------------------------------------------------
procedure TWorkspace.LoadFromFile(aFileName: string);
//---------------------------------------------------------
var
  oStrList: TStringList;
  iLayerInd, i, j: Integer;
 // oIDLIst: TVarIDList;
 strArr, oOrderArr, oStrArrTmp: TStrArray;
//  oArr: TdmMapFileRecArr;
  bOrderTagFound: boolean;
  iPos: Integer;
  iPos1: Integer;
  iPos2: Integer;
  sStr, sDir, sFileName: string;

  oItem :TworTable;
//  oOrderedList: TStringList;
  s: string;
  sName: string;
begin
//   Self.


//  aFileName

 // SetLength(aArr, 0);
//  SetLength(oArr, 0);

  if not FileExists(aFileName) then
    exit;

  sDir:= ExtractFilePath(aFileName);

  oStrList:= TStringList.Create;

 // oOrderedList:= TStringList.Create;


//  oIDLIst:= TVarIDList.CReate;

  oStrList.LoadFromFile(aFileName);

//  bOrderTagFound:= false;
 // iLayerInd:= -1;

  i:=0;

  while i <  oStrList.Count - 1 do
  begin

  {
Map From Sport,Bridge,Streets2,Streets1,Roads3,Roads2,
  Roads1,Railways,������,Streets2_buff,Streets1_buff,�������,
  Roads2_buff,Roads1_buff,Block,Railway_st,Airports,Industrialareas,
  Place3,Place2,Place1,Relief,Gidrograf,Swamp,Vegetation,russia_obl
  Position (0.0520833,0.0520833) Units "in"
  Width 9.95833 Units "in" Height 4.875 Units "in"

 }


    // File Name List
    if Pos_('Open Table', oStrList[i]) then
    begin
      oItem:=AddItem;

   //   SetLength(oArr, Length(oArr)+1);
      oItem.FileName:=sDir + Trim(GetStrBetween(oStrList[i], 'Open Table "', '" As')) + '.tab';

      oItem.LayerName:=Trim(GetStrBetween(oStrList[i], '" As ', ' Interactive'));

      Inc(i);
    end else

    // ---------------------------------------------------------------
    if LeftStr(oStrList[i], Length('Map From ')) = 'Map From ' then
    // ---------------------------------------------------------------
    begin
      s:=Copy(oStrList[i], Length('Map From ')+1, 100);

      strArr:= StringToStrArray(s, ',');
      StrArrayToStrList (strArr, OrderedFileList);


      Inc(i);

      while (i < oStrList.Count) and (LeftStr(oStrList[i],2)='  ')  do
      begin
        if (Pos('Position ', oStrList[i])=0) and
           (Pos('Width ',    oStrList[i])=0) and
           (Pos(',', oStrList[i])>0)
        then begin
          strArr:= StringToStrArray(oStrList[i], ',');
          StrArrayToStrList (strArr, OrderedFileList);
        end;


        Inc(i);

      end;


    end else

    // ---------------------------------------------------------------
    if LeftStr(oStrList[i], Length('  Layer ')) = '  Layer ' then
    // ---------------------------------------------------------------
    begin
     //

      iPos:=Pos('Layer ', oStrList[i]);

      s:= Copy (oStrList[i], iPos+Length('Layer '), 10);
      iLayerInd:=AsInteger(s);

      sName:=OrderedFileList[iLayerInd-1];


      oItem:=GetItemByName(sName); // Items[iLayerInd-1];

//      if iLayerInd=17 then
  //      iLayerInd:=iLayerInd;

//    StringOfChar(' ',4)
//    Zoom (0, 201) Units "km" Off

//    ZoomMin, ZoomMax: double;
//    IsZoomed: boolean;


      Inc(i);

      while (i < oStrList.Count) and (LeftStr(oStrList[i],4)='    ')  do
      begin

        if Pos_('Zoom', oStrList[i]) then
        begin

          iPos1:=Pos('(', oStrList[i]);
          iPos2:=Pos(')', oStrList[i]);

          s:=copy(oStrList[i], iPos1+1, iPos2-iPos1-1);

          strArr:= StringToStrArray(s, ',');

          oItem.ZoomMin:=AsFloat(strArr[0]);
          oItem.ZoomMax:=AsFloat(strArr[1]);

          oItem.IsZoom:=  RightStr(trim(oStrList[i]), 3) <> 'Off';

        end;

        Inc(i);

      end;

      
    end else
      Inc(i);

  end;
  

{
  Layer 14
    Display Graphic
    Global   Symbol (35,0,12)
    Zoom (0, 500) Units "km"
    Label Line None Position Center Font ("Arial Cyr",0,9,0) Pen (1,2,0)
      With Type
      Parallel On Auto Off Overlap Off Duplicates On Offset 2
      Visibility On
}





end;


{

var
  oWorkspace: TWorkspace;

begin
  oWorkspace:=TWorkspace.Create;
  oWorkspace.LoadFromFile('E:\lenobl.WOR');
}


end.





{



constructor TmtsProperty.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  Cells := TmtsCellList.Create();
end;

destructor TmtsProperty.Destroy;
begin
  FreeAndNil(Cells);
  inherited Destroy;
end;

procedure TmtsProperty.LoadFromDataset(aDataset: TDataset);
var
  iMaxH: INTeger;
  s: string;
begin
  with aDataset do
  begin

    BLPoint_Pulkovo.B:=FieldByName('������__����').AsFloat;
    BLPoint_Pulkovo.L:=FieldByName('�������_����').AsFloat;

    BLPoint_WGS:=geo_BL_to_BL(BLPoint_Pulkovo, EK_KRASOVSKY42, EK_WGS84);


    GROUND_HEIGHT:=   FieldByName('h_�����').AsInteger;
    ADDRESS:=         FieldByName('�����').AsString;

    H1_m:=FieldByName('h1_m').AsInteger;
    H2_m:=FieldByName('h2_m').AsInteger;
    H3_m:=FieldByName('h3_m').AsInteger;

    Name_:=Trim(FieldByName('���_�').AsString);

    s:=GetLastChar(Name);
    if (s='a') or (s='b') or (s='c') then
      Name_:=Copy(Name_, 1, Length(Name_)-1 );


   iMaxH:=Max( Max(h1_m, h2_m), h3_m);

 //  sName:=sName + Format(' (H: %d)', [iMaxH]);


   Standard:=FieldByName('��������').AsString;


   RegionName := FieldByName(DEF_REGION_).AsString;

   Comment:= Format('max H: %d', [iMaxH]);

  end;

 ////////// Cells.Clear;
(*
  if h1_m > 0 then LoadFromDataset(aDataset, Cells.AddItem, 1);
  if h2_m > 0 then LoadFromDataset(aDataset, Cells.AddItem, 2);
  if h3_m > 0 then LoadFromDataset(aDataset, Cells.AddItem, 3);
*)
end;






//-------------------------------------------------------------------
procedure TWorkspace.LoadFromMap(aMap: TMap);
//-------------------------------------------------------------------

 (*    //-------------------------------------------------------------------
     function GetMapinfoShortFileName(aFileName: string): string;
     //-------------------------------------------------------------------
     var
       s: string;
     begin
      aFileName:=ExtractFileNameNoExt(aFileName) ;
      aFileName:=ReplaceStr(aFileName,' ','_');
      aFileName:=ReplaceStr(aFileName,'.','_');
      aFileName:=ReplaceStr(aFileName,'-','_');
      aFileName:=ReplaceStr(aFileName,',','_');

      s:=Copy(aFileName, 1,1);
      if Copy(aFileName, 1,1)[1] in ['0'..'9'] then
        aFileName:='_'+ aFileName;

      Result := aFileName;
}    end;
*)

  (*  function MiPositionToStr(aValue: Integer): string;
    begin
      case aValue of
        miPositionCC : Result := 'Center';
        miPositionTL : Result := 'Above RIGHT';
        miPositionTC : Result := 'Above Left';
        miPositionTR : Result := 'Above Left';
        miPositionCL : Result := 'LEFT';
        miPositionCR : Result := 'RIGHT';
        miPositionBL : Result := 'Below Left';
        miPositionBC : Result := 'Below';
        miPositionBR : Result := 'Below RIGHT';
      end;

    end;*)

//
//    function StrToMiPosition(aValue: string): Integer;
//    begin
//     (* case aValue of
//        miPositionCC : Result := 'Center';
//        miPositionTL : Result := 'Above RIGHT';
//        miPositionTC : Result := 'Above Left';
//        miPositionTR : Result := 'Above Left';
//        miPositionCL : Result := 'LEFT';
//        miPositionCR : Result := 'RIGHT';
//        miPositionBL : Result := 'Below Left';
//        miPositionBC : Result := 'Below';
//        miPositionBR : Result := 'Below RIGHT';
//      end;
//*)
//    end;
//

//
//const
//  CRLF = #13+#10;

var
  v: Variant;
  vField : CMapXField;

(*  sKeyField: string;
  st: string;
  bAuto: Boolean;
  sPos: string;
  iColorBack: integer;
  iSize: integer;
  sFont: string;
  sName: string;
  sFilename: string;*)
  i: integer;
 // S,sNames: string;
 // v: Variant;

  vLayer: CMapxLayer;

  oTable: TworTable;
       

//  oSList: TStringList;
  s1: string;
begin
//  oSList:=TStringList.Create;


(*  oSList.Add('!Workspace');
  oSList.Add('!Version 600');
  oSList.Add('!Charset WindowsCyrillic');

*)
(*
  s1:= '!Workspace' + CRLF +
      '!Version 600' + CRLF +
      '!Charset WindowsCyrillic';// + CRLF;
*)

 // oSList.Add(s1);

  CenterX:=aMap.CenterX;
  CenterY:=aMap.CenterY;
  Zoom:=aMap.Zoom;


//  sNames := '';

  for i := 1 to aMap.Layers.Count do
  begin
    oTable:=AddItem;


//    vLayer:=

    //u_mapX_tools
//    vLayer:=mapx_GetLayerByIndex_from_0(aMap,i);
    vLayer:=mapx_GetLayerByIndex_from_1(aMap,i);

//    sFilename:=aMap.Layers.Item[i].FileSpec;

    oTable.FileName:=vLayer.FileSpec;
//    oTable.Name:=GetMapinfoShortFileName(sFilename) ;


    oTable.LabelProperties_Style.TextFontName := OleVariant(vLayer.LabelProperties.Style.TextFont).Name;
    oTable.LabelProperties_Style.TextFontSize := OleVariant(vLayer.LabelProperties.Style.TextFont).Size;
    oTable.LabelProperties_Style.TextFontBackColor := vLayer.LabelProperties.Style.TextFontBackColor;
    oTable.LabelProperties_Style.Position := vLayer.LabelProperties.Position;

(*

     if (vLayer.Datasets.Count = 0) then
       aMap.Datasets.Add (miDataSetLayer, vLayer, EmptyParam, EmptyParam,
                          EmptyParam, EmptyParam, EmptyParam, EmptyParam) ;


    vField:=vLayer.LabelProperties.DataField as CMapXField;
    if Assigned(vField) then
      sKeyField:=vField.Name;*)


    oTable.LabelProperties_Style.IsAutoLabel:=vLayer.AutoLabel;
    oTable.LabelProperties_Style.KeyField:=vLayer.KeyField;


(*
        miLayerTypeNormal:
        begin
           // v := vLayer.LabelProperties.Visible;
           // sName:=AsString(v);
            sName     :=OleVariant(vLayer.LabelProperties.Style.TextFont).Name;
            iSize     :=OleVariant(vLayer.LabelProperties.Style.TextFont).Size;
            iColorBack:=vLayer.LabelProperties.Style.TextFontBackColor;
            sPos      :=MiPositionToStr(vLayer.LabelProperties.Position);

*)


  //  st:=aMap.Layers.Item[i].KeyField;


(*    sName:=GetMapinfoShortFileName(sFilename) ;
  //  map1.Layers.Item[i].Name;

    s1:=Format('Open Table "%s" As %s Interactive', [sFilename,sName]);
    oSList.Add(s1);

    s:=s+ Format('Open Table "%s" As %s Interactive', [sFilename,sName]) + CRLF;

    sNames:=sNames+ sName + IIF(i<aMap.Layers.Count-1,',','');
*)
  end;
(*
  //Map From pmp_site,property,aqua,pmp_sector_ant
  s:=s+ Format('Map From %s', [sNames]) + CRLF ;

  s1:=Format('Map From %s', [sNames]);
  oSList.Add(s1);


  s:=s+ 'Set Map ' + CRLF;
  oSList.Add('Set Map ');

  oSList.Add('  CoordSys Earth Projection 1, 1001 ');
*)

  oTable.Type_ := vLayer.Type_;


(*
  s1:=Format('  Center (%1.8f,%1.8f)  ',  [aMap.CenterX, aMap.CenterY]);
  oSList.Add(s1);

  s1:=Format('  Zoom %1.6f Units "km" ', [aMap.Zoom]);
  oSList.Add(s1);*)

(*     // s+
  s1:= '  CoordSys Earth Projection 1, 1001 '+ CRLF +
       Format('  Center (%1.8f,%1.8f)  ',  [aMap.CenterX, aMap.CenterY]) + CRLF +
       Format('  Zoom %1.6f Units "km" ', [aMap.Zoom]); // + CRLF ;

  oSList.Add(s1);

  s:=s+ 'Set Map '+ CRLF;

  oSList.Add('Set Map ');
*)

//
//  for i := 0 to aMap.Layers.Count-1   do
//  begin
//
////    vLayer:=aMap.Layers.Item[i];
//    vLayer:=mapx_GetLayerByIndex_from_0(aMap,i);
//
//  //  vLayer.LabelProperties.Visible
//
//    s:=s+ Format('  Layer %d      ', [i+1])+ CRLF;
//
//    s1:=Format('  Layer %d      ', [i+1]);
//    oSList.Add(s1);
//
//
//    case vLayer.Type_ of
//
//        miLayerTypeRaster:
//        begin
//          s1:= '  Display Graphic  ' + CRLF +
//               '  Selectable Off   ' + CRLF ;
//          s:=s+ s1;
//
//          oSList.Add(s1);
//        end;
//
//
//        miLayerTypeNormal:
//        begin
//           // v := vLayer.LabelProperties.Visible;
//           // sName:=AsString(v);
//            sName     :=OleVariant(vLayer.LabelProperties.Style.TextFont).Name;
//            iSize     :=OleVariant(vLayer.LabelProperties.Style.TextFont).Size;
//            iColorBack:=vLayer.LabelProperties.Style.TextFontBackColor;
//            sPos      :=MiPositionToStr(vLayer.LabelProperties.Position);
//
//        //    v:=vLayer.LabelProperties.DataField;
//
//            if Pos('cyr',sName)=0 then
//              sName:=sName + ' cyr';
//
//
//            sFont:= Format('("%s",0,%d,0,%d)',
//                      [sName,
//                       iSize,
//                       MakeMapinfoColor(iColorBack)
//                      ]);
//
//
//           if (vLayer.Datasets.Count = 0) then
//             aMap.Datasets.Add (miDataSetLayer, vLayer, EmptyParam, EmptyParam,
//                                EmptyParam, EmptyParam, EmptyParam, EmptyParam) ;
//
//
//            vField:=vLayer.LabelProperties.DataField as CMapXField;
//            if Assigned(vField) then
//              sKeyField:=vField.Name;
//
//            bAuto:=vLayer.AutoLabel;
//            sKeyField:=vLayer.KeyField;
//
//            s1:= Format('    Label Line Arrow Position %s Font %s Pen (1,2,0)', [sPos,sFont])+ CRLF +
//       IIF(sKeyField<>'','    With '+ sKeyField+ CRLF,'') +
////                  Format('    With %s       ', [sName])+ CRLF +
//                  Format('    Parallel On Auto %s ', [IIF(bAuto,'On','Off') ])+
//                             'Overlap Off Duplicates On Offset 2'+ CRLF +
//                         '    Visibility On '; // + CRLF;
//
//
//
//            s:=s+ s1;
//            oSList.Add(s1);
//
//
//{            Format('    Label Line Arrow Position %s Font %s Pen (1,2,0)', [sPos,sFont])+ CRLF +
//       IIF(sKeyField<>'','    With '+ sKeyField+ CRLF,'') +
////                  Format('    With %s       ', [sName])+ CRLF +
//                  Format('    Parallel On Auto %s ', [IIF(bAuto,'On','Off') ])+
//                             'Overlap Off Duplicates On Offset 2'+ CRLF +
//                         '    Visibility On ' + CRLF;
//}
//
//        end;
//    //  end;
//    end;
//  end;

//  StrToTxtFile (s, aWorFileName);
(*
  ForceDirByFileName(aWorFileName);

  oSList.SaveToFile (aWorFileName);
  FreeAndNil(oSList);*)

end;


       {


constructor TworTableList.Create;
begin
  inherited Create(TworTable);
end;


function TworTableList.AddItem: TworTable;
begin
  Result := TworTable (inherited Add);
end;


function TworTableList.GetItems(Index: Integer): TworTable;
begin
  Result := TworTable(Inherited Items[index]);
end;





    {
    // File Name List
    if Pos_('Open Table', oStrList[i]) then
    begin
      oItem:=AddItem;

   //   SetLength(oArr, Length(oArr)+1);
      oItem.FileName :=Trim(GetStrBetween(oStrList[i], 'Open Table "', '" As')) + '.tab';
      oItem.LayerName:=Trim(GetStrBetween(oStrList[i], '" As ', ' Interactive'));
    end;

    // Order List
    if bOrderTagFound then begin
      if Pos_('Position', oStrList[i]) then
        bOrderTagFound:= false
      else begin
        oStrArrTmp:= StringToStrArray(oStrList[i], ',');
        for j := 0 to High(oStrArrTmp) do
          StrArrayAddValue(oOrderArr, oStrArrTmp[j]);
      end;
    end;

    if Pos_('Map From', oStrList[i]) then
    begin
      bOrderTagFound:= true;
      sStr:= ReplaceStr(oStrList[i], 'Map From', '');
      oOrderArr:= StringToStrArray(sStr, ',');
    end;

    // Start Read info from layer. Get LayerIndex

    if Pos_('Layer ', oStrList[i]) then
    begin
      iPos:=Pos('Layer ', oStrList[i]);

      s:= Copy (oStrList[i], iPos+ Length('Layer ', 10);
      iLayerInd:=AsInteger(s);

//      geo_GetNumbersFromString(oStrList[i], oIDLIst);
//      iLayerInd:= -1;
//      if oIDLIst.Count>0 then
//        iLayerInd:= AsInteger(oIDLIst[0].ID) - 1;
    end;

    // By last readed LayerIndex - layer is checked or not
    if Pos_('Display Off', oStrList[i]) then begin
      if iLayerInd>=0 then begin
        j:= DoGetIndInOutArray(oArr, oOrderArr[iLayerInd]);
        if j<0 then continue;
        oArr[j].UnChecked:= true;
      end;
    end;

    // By last readed LayerIndex - layer zoom info
    if Pos_('Zoom', oStrList[i]) then begin
      if iLayerInd>=0 then begin
        j:= DoGetIndInOutArray(oArr, oOrderArr[iLayerInd]);
        if j<0 then continue;
        geo_GetNumbersFromString(oStrList[i], oIDLIst);
        if oIDLIst.Count>1 then begin
          oArr[j].AllowZoom:= true;
          oArr[j].ZoomMin:= AsInteger(oIDLIst[0].ID);
          oArr[j].ZoomMax:= AsInteger(oIDLIst[1].ID);
        end;
      end;
    end;
  end;


  for i := 0 to High(oOrderArr) do
  begin
    j:= DoGetIndInOutArray(oArr, oOrderArr[i]);
    if j<0 then continue;

    sFileName:= oArr[j].FileName;
    if not Pos_('\', oArr[j].FileName) then
      sFileName:= IncludeTrailingBackslash(sDir) + oArr[j].FileName;

    SetLength(aArr, Length(aArr)+1);
    aArr[High(aArr)].FileName := sFileName;
    aArr[High(aArr)].UnChecked:= oArr[j].UnChecked;
    aArr[High(aArr)].AllowZoom:= oArr[j].AllowZoom;
    aArr[High(aArr)].ZoomMin  := oArr[j].ZoomMin;
    aArr[High(aArr)].ZoomMax  := oArr[j].ZoomMax;
  end;

  oIDLIst.Free;
  oStrList.Free;

  }

