inherited dlg_MapX_Print: Tdlg_MapX_Print
  Left = 1095
  Top = 298
  VertScrollBar.Range = 0
  BorderStyle = bsDialog
  Caption = #1055#1077#1095#1072#1090#1100' '#1082#1072#1088#1090#1099
  ClientHeight = 455
  ClientWidth = 482
  OldCreateOrder = True
  Position = poDefault
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 5
    Top = 68
    Width = 60
    Height = 13
    Caption = #1048#1084#1103' '#1092#1072#1081#1083#1072':'
  end
  inherited pn_Buttons: TPanel
    Top = 420
    Width = 482
    inherited Bevel1: TBevel
      Width = 482
    end
    inherited Panel3: TPanel
      Left = 303
      inherited btn_Ok: TButton
        Left = 322
      end
      inherited btn_Cancel: TButton
        Left = 406
      end
    end
  end
  inherited pn_Top_: TPanel
    Width = 482
    inherited Bevel2: TBevel
      Width = 482
    end
    inherited pn_Header: TPanel
      Width = 482
    end
  end
  object cxVerticalGrid1: TcxVerticalGrid [3]
    Left = 5
    Top = 120
    Width = 470
    Height = 241
    Anchors = [akLeft, akTop, akRight, akBottom]
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    OptionsView.PaintStyle = psDelphi
    OptionsView.RowHeaderWidth = 118
    TabOrder = 2
    Version = 1
    object cxVerticalGrid1CategoryRow1: TcxCategoryRow
      Properties.Caption = #1056#1072#1079#1084#1077#1088#1099' [cm]'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object row_H: TcxEditorRow
      Properties.Caption = #1042#1099#1089#1086#1090#1072
      Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = '20'
      ID = 1
      ParentID = 0
      Index = 0
      Version = 1
    end
    object row_W: TcxEditorRow
      Properties.Caption = #1064#1080#1088#1080#1085#1072
      Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = '20'
      ID = 2
      ParentID = 0
      Index = 1
      Version = 1
    end
    object cxVerticalGrid1CategoryRow2: TcxCategoryRow
      ID = 3
      ParentID = -1
      Index = 1
      Version = 1
    end
    object row_Scale: TcxEditorRow
      Properties.Caption = #1052#1072#1089#1096#1090#1072#1073
      Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
      Properties.EditProperties.Items.Strings = (
        '10 000'
        '100 000'
        '200 000'
        '400 000')
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = '100 000'
      ID = 4
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxVerticalGrid1EditorRow1: TcxEditorRow
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 5
      ParentID = -1
      Index = 3
      Version = 1
    end
  end
  object ed_ImgFileName: TFilenameEdit [4]
    Left = 5
    Top = 84
    Width = 471
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    NumGlyphs = 1
    TabOrder = 3
    Text = 'e:\print.jpg'
  end
  inherited ActionList1: TActionList
    Left = 408
  end
  inherited FormStorage1: TFormStorage
    Left = 380
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 351
    Top = 3
  end
end
