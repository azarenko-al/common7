object dlg_Message1: Tdlg_Message1
  Left = 792
  Top = 297
  Width = 604
  Height = 481
  ActiveControl = btn_Ok
  Caption = 'dlg_Message1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 93
    Width = 596
    Height = 329
    Align = alBottom
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 0
    object Memo1: TMemo
      Left = 5
      Top = 56
      Width = 586
      Height = 268
      Align = alBottom
      ReadOnly = True
      ScrollBars = ssBoth
      TabOrder = 0
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 422
    Width = 596
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 412
      Top = 0
      Width = 184
      Height = 31
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object btn_Ok: TButton
        Left = 13
        Top = 2
        Width = 75
        Height = 25
        Caption = 'Ok'
        Default = True
        ModalResult = 1
        TabOrder = 0
      end
      object Button2: TButton
        Left = 93
        Top = 2
        Width = 75
        Height = 25
        Cancel = True
        Caption = #1054#1090#1084#1077#1085#1072
        ModalResult = 2
        TabOrder = 1
      end
    end
  end
  object ActionList1: TActionList
    Left = 72
    Top = 24
    object act_Continue: TAction
      Caption = 'Continue'
    end
    object act_Ok: TAction
      Caption = 'act_Ok'
    end
  end
  object FormPlacement1: TFormPlacement
    IniFileName = 'Software\Onega\'
    Left = 152
    Top = 24
  end
end
