object dlg_GEO_Coordinates: Tdlg_GEO_Coordinates
  Left = 575
  Top = 552
  ActiveControl = CancelBtn
  BorderStyle = bsToolWindow
  Caption = 'dlg_GEO_Coordinates'
  ClientHeight = 118
  ClientWidth = 499
  Color = clBtnFace
  ParentFont = True
  FormStyle = fsStayOnTop
  OldCreateOrder = True
  Position = poDefault
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    499
    118)
  PixelsPerInch = 96
  TextHeight = 13
  object OKBtn: TButton
    Left = 339
    Top = 90
    Width = 75
    Height = 23
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object CancelBtn: TButton
    Left = 423
    Top = 90
    Width = 75
    Height = 23
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 1
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 499
    Height = 81
    Align = alTop
    TabOrder = 2
  end
end
