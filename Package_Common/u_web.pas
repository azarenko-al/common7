unit u_web;

interface
uses
  u_debug,
  u_Log,
//  u_log_,

  Windows, SysUtils, Dialogs, ExtActns, WinInet;


//function DownloadFile(aURL, aFileName: string): Boolean;
function DownloadFile(aURL, aDestFile: string; aZ: longword): Boolean;
function DownloadFile_HTTPS(aURL, aFileName: string): Boolean;


implementation



// ---------------------------------------------------------------
function DownloadFile(aURL, aDestFile: string; aZ: longword): Boolean;
// ---------------------------------------------------------------
var
//Vcl.ExtActns
  pdfStreamed: TDownloadUrl;
  s: string;
begin
  pdfStreamed:= TDownLoadURL.Create(nil);

  pdfStreamed.URL      := aURL;
  pdfStreamed.FileName := aDestFile;
  try
    pdfStreamed.ExecuteTarget(nil) ;
  except
    begin
  //  u_Log

      s:=Format('������ ��� �������� ����� ��� Z = %d. �������� ������� �� ����� ������.  url=%s', [aZ, aURL]);
      g_Log.Error (s);

//       on E: Exception do
      raise Exception.Create (s);
    end

  end;

    {

//  with pdfStreamed do//  begin  try    pdfStreamed.ExecuteTarget(nil) ;  except    on E: Exception do      ShowMessage(E.Message);  end; // end;   }     FreeAndNil(pdfStreamed);  Result:=FileExists(aDestFile);end;


// ---------------------------------------------------------------
function DownloadFile_HTTPS(aURL, aFileName: string): Boolean;
// ---------------------------------------------------------------
const
  DEF_BufferSize = 1024;
var
  hSession, hURL: HInternet;
  Buffer: array[1..DEF_BufferSize] of Byte;
  BufferLen: DWORD;
  F: File;
begin
   Result := False;
   hSession := InternetOpen('', INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0) ;

   // Establish the secure connection
   InternetConnect (
     hSession,
     PChar(aURL),
     INTERNET_DEFAULT_HTTPS_PORT,
     nil, //PChar(aUser),
     nil, //PChar(aPass),
     INTERNET_SERVICE_HTTP,
     0,
     0
   );

  try
    hURL := InternetOpenURL(hSession, PChar(aURL), nil, 0, 0, 0) ;
    try
      AssignFile(f, aFileName);
      Rewrite(f,1);
      try
        repeat
          InternetReadFile(hURL, @Buffer, SizeOf(Buffer), BufferLen) ;
          BlockWrite(f, Buffer, BufferLen)
        until BufferLen = 0;
      finally
        CloseFile(f) ;
        Result := True;
      end;
    finally
      InternetCloseHandle(hURL)
    end
  finally
    InternetCloseHandle(hSession)
  end;

//  if not FileExists(aFileName) then
//    Debug();


  Assert( FileExists(aFileName), aFileName);
end;




end.

