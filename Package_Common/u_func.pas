
unit u_func;

interface
uses
  Messages,  FileCtrl,
  Windows,Math,StrUtils,SysUtils,Classes, Variants, Forms,Controls,ActnList,extctrls,Dialogs,
  ShellAPI, RxVerInf, DateUtils

//uses
 // u_files
  ;
//  DateUtils;


{  Caption:= '������-������� ������ '+  GetAppVersionStr() + '  ������: '
    + FormatDateTime ('yyyy-mm-dd hh:mm', GetFileDate (Application.Exename));
}

var
  g_Is_Debug_mode : Boolean; //g_Is_Debug_mode := FileExists(sDir + 'debug.ini');


const
  CRLF = #13+#10;

type
  TOnProgressEvent = procedure (aProgress, aMax: Integer) of object;


  TIntNotifyEvent = procedure ({Sender: TObject;} aValue: integer) of object;

  TStrNotifyEvent = procedure (Sender: TObject; aStr: string) of object;
  TLogEvent = procedure (aMsg: string) of object;

  //---------------------------------------------------
  // COMMON types
  //---------------------------------------------------
  TIntArray     = array of integer;
  TStrArray     = array of string;
  TDoubleArray  = array of double;
  TVariantArray = array of Variant;
 // TStr2DArray    = array of TStrArray;

  // ������� N x N �� ���������� (��-���) (������-�������)
  TBoolArray       = array of boolean;
  TBoolean2DMatrix = array of TBoolArray;
  TInteger2DMatrix = array of TIntArray;
  TDouble2DMatrix  = array of TDoubleArray;


  procedure CloseApplicationForms;


  //---------------------------------------------------
  // COMMON functions
  //---------------------------------------------------
  function IIF (Condition: boolean; TrueValue,FalseValue: variant): variant; //overload;
  function IIF_NULL (aValue: variant): variant;

 ////// function IIF (Conditions: array of boolean; Values: array of variant; ElseValue:Variant): variant; overload;

  function Eq(aValue1, aValue2: string): boolean;

{
  function GetLastErrorMessage (aErrorCode: integer): string;
}
  //---------------------------------------------------
  // datatypes transform
  //---------------------------------------------------
  function AsFloat   (aValue: Variant; aDefault:double=0): double;
//  function AsFloat_  (Value:  variant; aDefault:double=0): double;
  function AsInteger (aValue: Variant; aDefault:integer=0): integer;
  function AsBoolean (aValue: Variant): boolean;
  function AsString  (aValue: Variant): string;

  //---------------------------------------------------
  // String functions
  //---------------------------------------------------
  function StrIsDouble  (asValue: string): boolean;
  function StrIsInt     (asValue: string): boolean;

//  function  VarToString     (Value: Variant): string;
  function  StringToVar     (Value:string; DestValue:Variant): Variant;
  function  ReplaceBlanks   (Value : string): string;
  procedure SeparateString  (Value,Separator:string; var Value1,Value2:string);
  function  RemoveStrHeader (SourceStr, RemovedStr: string): string;
  function StringIsInArr(Value:string; Args: array of string): boolean;
//  function  Replace         (aSource:string; aParams,aValues: array of string): string;

  function ReplaceStr (Value, OldPattern, NewPattern: string): string; //overload;
  function ReplaceInt (aValue, aOldPattern: string; aNewPattern: integer): string; //overload;

  function RemoveSubStr   (aValue, SubStr: string): string;

  function Eq_Any(aValue1: string; Value2: array of string): boolean;


  procedure ClearArray           (var IntArr: TIntArray);

// TODO: StringToStringist
//procedure StringToStringist(aValue: string; aStrings: TStringList; aDelimiter:
//    string = ';');


  function CreateStringListFromString (aValue: string; aDelimiter: string=';'): TStringList;



  function IsStrInStrArray(aStr: string; aStrArray: array of string): boolean;

  //---------------------------------------------------
  // Misc
  //---------------------------------------------------

  function Inc_ (var aValue: integer): integer;


  // procedure EnableChildControls (Value: boolean; aParentControl,aExceptControl: TWinControl);
  function  TruncFloat      (aValue: double; aCount: integer=2): double;
  procedure TruncFloatVar   (var aValue: double; aCount: integer=2);

  function RoundFloat(aValue: double; aCount: integer=2): double;


  function CompareStrings   (aList1,aList2: TStrings): integer;

  procedure CursorHourGlass;
  procedure CursorDefault;
  procedure CursorSQL;
  procedure CursorRestore;

  function VarExists (aValue: Variant): boolean;

//  function FindForm (aClassName: string): TForm;

  procedure SetComponentActionsExecuteProc (aComponent: TComponent; aProc: TNotifyEvent); //overload;
  procedure SetActionsExecuteProc (aActionArr: array of TAction; aProc: TNotifyEvent); //overload;


  function CompareDates      (aValue1,aValue2: TDateTime): integer;
  function FormatDateVar     (aValue: Variant): string;
  function FormatDateTimeVar (aValue: Variant): string;


// TODO: StrListCopy
//procedure StrListCopy (aSrcList, aDestList: TStringList);

  // DIALOGS ---------------------------------------------------------
  procedure CopyControls(aSrcForm: TForm; aDest: TWinControl);
  procedure CopyDocking1(aSrcForm: TForm; aDest: TWinControl);


//  function IsFormExists (aClassName: string): TForm;


  function str_DeleteLastChar(var aKeyFields: string): boolean;

  procedure SaveTime;
  function GetTimeDiff: string;

  function GetFileVersion: string;

  procedure CreateChildForm(InstanceClass: TComponentClass; var aReference; aDestControl: TWinControl);
  procedure CopyDockingEx (aSrcForm: TControl; aDest: TWinControl);



//procedure CreateChildForm1(InstanceClass: TComponentClass; var aReference; aOwner, aDestControl: TWinControl);


  function PickVarArrayItem(Value: Variant; SrcValues,DestValues: array of
      Variant): Variant;

  function ExtractStrByIndex(aStr: string; aIndex: integer): string;

  //------------------------------------------------------
  function IsFormExists(aApplication: TApplication; aClassName: string): TForm;
  function FindForm(aClassName: string): TForm;
  function GetAppVersionStr (): string;

  procedure ShellExec(aFileName: string; aFileParams: string = '');

  procedure SetApplicationName(aName : string);


procedure ExchangeInt(var aValue1,aValue2: integer);

procedure ExchangeStr(var aValue1,aValue2: string);

function GetComputerNetName: string;

function IIF_NOT_0(aValue: Double): variant;

function GetApplicationDir: string;

function ConfirmDlg(aMsg: string): boolean;

procedure Show_Application_DataModules;

procedure Application_DataModules_Free;

function AsString_(aValue: Variant): string;

function Eq_Any_variant(aValue: variant; Value2: array of variant): boolean;

function String_Reverse1(aValue: String): String;

function GetApplicationIniFileName(aExeName: string = ''): string;

function DoubleQuotedStr(aValue: String): String;

function DateTimeRemoveSec(aValue1: TDateTime): TDateTime;

procedure CreateChildForm_(var aReference; InstanceClass: TComponentClass;
    aDestControl: TWinControl);

function SArr(Values: array of string): TStrArray; overload;

function SArr(aValue1,aValue2: string): TStrArray; overload;

//  function MakeStrArray         (Values: array of string): TStrArray;
  function StrArray             (Values: array of string): TStrArray;

// TODO: StringToStringList___
//function StringToStringList___(aValue: string; aDelimiter: string=';'):
//  TStringList;

procedure StrToFile(aStr: string; aFileName: string);

procedure ShellExec_Notepad1(aFileName: string);

//function RoundFloat(Value: double; aCount: integer=3): double;

function StringListToStrArray(aStringList: TStringList): TStrArray;

function StringToStringList(aValue: string; aDelimiter: char=';'): TStringList;

procedure SetActionsEnabled1(aActionArr: array of TAction; aEnabled: Boolean);

procedure SetFormActionsEnabled(aComponent: TComponent; aEnabled: Boolean);

//function ChecksumOfDouble1111111111(const ADouble: Double): Integer;

procedure ShowApplicationComponents;

function MakeDoubleArray(aArr: array of double): TDoubleArray;
function DoubleArray_GetMax(aArr: TDoubleArray): Double;


function TextFileToStr(aFileName: string; var aStr: string): Boolean;

function Pos_(const aSubstr, aStr: string): boolean;

function StringToStrArray_new(aValue: string; aDelimiter: char=';'): TStrArray;

function CompareMemoryStreams(msOne,msTwo:TMemoryStream): boolean;

procedure Dialog_SetDefaultSize(aForm: TForm);

procedure Dialog_SetDefaultWidth(aForm: TForm);

function GetRandomStr: string;

function PosCount(aSubStr, aStr: string): Integer;

procedure AppendStrToFile(aFileName: string; aMsg: string);

procedure StrToTextFile(aFileName, aStr: string);

procedure AssertFileExists(aFileName: string);

function SearchFileInPath(aFileName: string): string;

procedure DeleteDirectory(const aDirName: string);


//


//function Inc_(var aValue: integer): integer;


(*
procedure SetActionsArray(aActions: array of TAction; var aDestActionsArr:
    array of TAction);
*)

//function CompareSqlText(aValue1, aValue2: String; var APos: Integer): Boolean;

const
  DEF_EXCEL_Filter = 'Excel (*.xls,*.xlsx)|*.xls;*.xlsx';



var
  ApplicationName : string;
  CommonApplicationDataDir : string;



//========================================================
implementation
//========================================================

uses
  u_files, ComCtrls;


//--------------------------------------------------------
function IIF (Condition: boolean; TrueValue,FalseValue: variant): variant;
//--------------------------------------------------------
begin
  if Condition then Result:=TrueValue else Result:=FalseValue;
end;



// ---------------------------------------------------------------
function PosCount(aSubStr, aStr: string): Integer;
// ---------------------------------------------------------------
var
  i: Integer;
  iPos: Integer;
  s: string;
begin
  iPos:=0;
  Result := 0;

  repeat
    iPos:=PosEx(aSubStr, aStr, iPos+1);

    if iPos>0 then
      Inc(Result);
  until
    iPos<=0;

end;




function GetRandomStr: string;
var
  i: Integer;
  s: string;
begin
  Random (50);

  s:='';
  for i:=0 to 10 + Random (50) do
  begin
    s:=s + Chr(Ord('a') + Random (20));
  end;

  Result:=s;

end;



//--------------------------------------------------------
function Eq(aValue1, aValue2: string): boolean;
//--------------------------------------------------------
begin
  Result := AnsiCompareText(Trim(aValue1),Trim(aValue2))=0;
end;


//--------------------------------------------------------
function AsFloat (aValue: Variant; aDefault:double=0): double;
//--------------------------------------------------------
var err: integer;
  i: Integer;
   S: string;
begin
  i:=VarType(aValue);


  case VarType(aValue) of
    0: Result:=0;

    varString,
    varOleStr:  begin
                  s:=Trim (aValue);
                  s:=ReplaceStr (s, ',','.');
                  Val(s, Result, err);

                  if err<>0 then
                    Result:=aDefault;
                end;

    varInteger,
    varSingle,
    varCurrency,
    varByte,
    varDouble : Result:= aValue;

    varNull: Result:=0;
  else
    raise Exception.Create('VarType(aValue) is not found: '+ IntToStr(VarType(aValue)));
//    ShowMessage('function AsFloat (aValue: Variant; aDefault:double=0): double;');
 //   Result:=0;
  end;
end;

// ---------------------------------------------------------------
function AsInteger(aValue: Variant; aDefault:integer=0): integer;
// ---------------------------------------------------------------
//var i: integer;
begin
//  i:=VarType(Value);

  case VarType(aValue) of
    varString,
    varOleStr:
              begin
                aValue:=ReplaceStr(aValue, #9, '');
                Result:=StrToIntDef (aValue,aDefault);
              end;

    varDouble: Result:=Trunc(aValue);

    varInteger,
    varByte: Result:=aValue;

    varNull: Result:=0;
  else
//    raise Exception.Create('function AsInteger (Value: Variant; aDefault:integer=0): integer; code:'+ Format('%d', [VarType(aValue)]) );
    Result:=0;
  end;
end;

//--------------------------------------------------------
function AsBoolean(aValue: Variant): boolean;
//--------------------------------------------------------
var
  i: integer;
begin
  if VarIsNull(aValue) then
    result := False
  else
  begin
    i:=VarType(aValue);

    case VarType(aValue) of
      varEmpty    : result := False;
      varBoolean  : Result:=aValue;
      varInteger  : Result:=aValue<>0;

      varOleStr,
      varString   : begin
                      aValue:=Trim(aValue);
                      Result:=(aValue='1') or (aValue='-1') or (Eq(aValue,'True'));
                    end;
    else
      ShowMessage(IntToStr(i));
      raise Exception.Create('function AsBoolean(aValue: Variant): boolean; - '+IntToStr(VarType(aValue)));
    end;
  end;
end;

//--------------------------------------------------------
function StringIsInArr(Value:string; Args: array of string): boolean;
//--------------------------------------------------------
var i:integer;
begin
  Result:=false;

  for I:=0 to High(Args) do
    if AnsiCompareText(Value,Args[I])=0 then
    begin
      Result:=true;
      break;
    end;
end;

{
//--------------------------------------------------------
function VarToString(Value : Variant): string;
//--------------------------------------------------------
begin
  case VarType(Value) of
    varString  : Result:=Trim(Value);
    varInteger : Result:=IntToStr(Value);
    varDouble  : Result:=FloatToStr(Value);
    varBoolean : Result:=IIF(Value=true,1,0);
    varDate    : Result:=DateTimeToStr (Value);
    varNull    : Result:='';
  end;
end;
}

function ReplaceBlanks (Value : string): string;
begin
  Result:=StringReplace (Value, ' ', '_', [rfReplaceAll]);
end;


function RemoveStrHeader (SourceStr, RemovedStr: string): string;
// ������� ��������� ���������
begin
  if Pos(RemovedStr,SourceStr) = 1 then Delete(SourceStr,1,Length(RemovedStr));
  Result:=SourceStr;
end;

//--------------------------------------------------------
function StringToVar (Value:string; DestValue:Variant): Variant;
//--------------------------------------------------------
begin
  case VarType(DestValue) of
    varString  : Result:=Trim(Value);
    varInteger : Result:=StrToIntDef(Value,0);
    varDouble  : Result:=AsFloat(Value);
    varBoolean : Result:=Eq(Value,'-1') or Eq(Value,'1') or Eq(Value,'��') or Eq(Value,'yes');
    varDate    : try Result:=StrToDateTime (Value); except Result:=Now(); end
  end;
end;

//--------------------------------------------------------
procedure SeparateString (Value,Separator:string; var Value1,Value2:string);
//--------------------------------------------------------
var i:integer;
begin
  Value:=StringReplace (Value, #9,'', [rfReplaceAll]);
  i:=Pos(Separator,Value);
  if i>0 then begin Value1:=Copy(Value,1,i-1); Value2:=Copy(Value,i+1,Length(Value)); end
         else begin Value1:=Value; Value2:=''; end;
end;



function ReplaceInt(aValue, aOldPattern: string; aNewPattern: integer): string;
begin
  Result:=ReplaceStr(aValue, aOldPattern, IntToStr(aNewPattern));
end;


function Inc_(var aValue: Integer): Integer;
begin
  Inc(aValue);
  Result := aValue;
end;



function ReplaceStr (Value, OldPattern, NewPattern: string): string;
begin
  Result:=StringReplace (Value, OldPattern, NewPattern, [rfReplaceAll, rfIgnoreCase]);
end;

function RemoveSubStr(aValue, SubStr: string): string;
begin
  Result:=ReplaceStr (aValue, SubStr, '');
end;

function FirstUpperCase(aValue: string): string;
begin
  Result:=AnsiUpperCase(Copy(aValue,1,1)) + AnsiLowerCase(Copy(aValue,2,1000));
end;


//--------------------------------------------------------
function RemoveCharFromString (Ch:char; SrcStr: string): string;
//--------------------------------------------------------
begin
  Result:=StringReplace (SrcStr, Ch, '', [rfReplaceAll] );
end;

//--------------------------------------------------------
function PickVarArrayItem(Value: Variant; SrcValues,DestValues: array of
    Variant): Variant;
//--------------------------------------------------------
var i: integer;
begin
  Result:='';
  for i:=0 to High(SrcValues) do
    if SrcValues[i]=Value then begin Result:=DestValues[i]; Exit; end;
end;


//---------------------------------------------
function IsSingleApplication (AClassName: string): boolean;
// only 1 copy of the program should be loaded
//---------------------------------------------
var Handle: THandle;
begin
  Result:=false;

  Handle:=FindWindow(PChar(AClassName),nil);
  if Handle <> 0
    then SetForegroundWindow(Handle)
    else Result:=true;

  if FindWindow(PChar('TAppBuilder'),nil)<>0
    then Result:=true;
end;


procedure ClearArray (var IntArr:TIntArray);
var i:integer;
begin
  for i:=0 to High(IntArr) do IntArr[i]:=0; // clear array
end;

// ---------------------------------------------------------------
function TruncFloat (aValue: double; aCount: integer=2): double;
// ---------------------------------------------------------------
var
  d: Double;
begin
  d:=Power(10,aCount);
  Result:=Trunc(aValue * d) / d;
end;

{
// ---------------------------------------------------------------
function RoundFloat (aValue: double; aCount: integer=2): double;
// ---------------------------------------------------------------
var
  d: Double;
begin
  d:=Power(10,aCount);
  Result:=Round(aValue * d) / d;
end;
   }


//------------------------------------------------------------------------------
// �������� ���������� ������� SQL
//------------------------------------------------------------------------------
function RoundFloat(aValue: double; aCount: integer=2): double;
//------------------------------------------------------------------------------
var dExt: double;
begin
  dExt:= Power(10, aCount);

  Result:= round(aValue*dExt) / dExt;

  //Result:= AsFloat (format('%.'+IntToStr(aCount)+'f',[Value]));
end;



procedure TruncFloatVar (var aValue: double; aCount: integer=2);
begin
  aValue:=Trunc(aValue*Power(10,aCount)) / Power(10,aCount);
end;


procedure MoveRect (var aRect: TRect; aLeft,aTop: integer);
var
  dH,dW: integer;
begin
  dH:=Abs(aRect.Top - aRect.Bottom);
  dW:=Abs(aRect.Left - aRect.Right);

  aRect.Top :=aTop;
  aRect.Left:=aLeft;

  aRect.Bottom:=aRect.Top  + dH;
  aRect.Right :=aRect.Left + dW;
end;


//-----------------------------------------------------------
function CompareStrings (aList1,aList2: TStrings): integer;
//-----------------------------------------------------------
var i: integer;
begin
  Result:=-1;

  if aList1.Count <> aList2.Count then Exit;

  for i:=0 to aList1.Count-1 do
   if not Eq (aList1[i], aList2[i]) then Exit;

  Result:=0;
end;


procedure CursorHourGlass;
begin
  Screen.Cursor:=crHourGlass;
//  Application.ProcessMessages;
end;

procedure CursorDefault;
begin
  Screen.Cursor:=crDefault;
//  Application.ProcessMessages;
  
end;


function VarExists (aValue: Variant): boolean;
begin
  Result:=(not VarIsNull(aValue));
//          (aValue<>0);
end;


// without boolean conversion
function AsString_(aValue: Variant): string;
begin
  if VarIsNull(aValue) then
    Result:='' else

  case VarType(aValue) of
    varInteger: Result:=IntToStr(aValue);
    varDouble : begin
      Result:= FloatToStr(aValue);
      Result:= ReplaceStr(Result, ',', '.');
    end;
    varOleStr,varString:  Result:=aValue;
    varBoolean: Result:= IIF(aValue, '1', '0');
  end;
end;


function AsString(aValue: Variant): string;
var i: integer;
begin
  if VarIsNull(aValue) then
    Result:=''

  else

    case VarType(aValue) of
      varEmpty,
      varNull:          Result:='';

      varByte,
      varInteger,
      varSmallint:      Result:=IntToStr(aValue);

      varDouble :       Result:=FloatToStr(aValue);   //san

      varOleStr,
      varString:        Result:=aValue;

      varBoolean:       Result:= IIF(aValue, 'True', 'False');
    else
      Result:='';
//      ShowMessage('function AsString(aValue: Variant): string;;  VarType(aValue)=?' + IntToStr(VarType(aValue)));
    end;
end;



function ExtractStrByIndex(aStr: string; aIndex: integer): string;
var
  oList: TStringList;
begin
  oList:=TStringList.Create;
  oList.Text:=aStr;
  if aIndex<oList.Count then Result:=oList[aIndex]
                        else Result:='';
  oList.Free;
end;


//--------------------------------------------------------
function CreateStringListFromString (aValue: string; aDelimiter: string=';'): TStringList;
//--------------------------------------------------------
begin
  Assert(length(aDelimiter)>0);

  Result:=TStringList.Create;
  Result.Delimiter:=aDelimiter[1];
  Result.DelimitedText:=aValue;
end;



//---------------------------------------------------------
procedure SetComponentActionsExecuteProc (aComponent: TComponent; aProc: TNotifyEvent);
//---------------------------------------------------------
var i: integer;
begin
  with aComponent do
  for i :=0  to ComponentCount-1 do
    if (Components[i].ClassName = 'TAction') then
      (TAction(Components[i])).OnExecute:=aProc;
end;


//---------------------------------------------------------
procedure SetActionsExecuteProc (aActionArr: array of TAction; aProc: TNotifyEvent);
//---------------------------------------------------------
var i: integer;
begin
  for i :=0  to High(aActionArr) do
    aActionArr[i].OnExecute:=aProc;
end;


//---------------------------------------------------------
procedure SetActionsEnabled1(aActionArr: array of TAction; aEnabled: Boolean);
//---------------------------------------------------------
var i: integer;
begin
  for i :=0  to High(aActionArr) do
    aActionArr[i].Enabled:=aEnabled;
end;


//---------------------------------------------------------
procedure SetFormActionsEnabled(aComponent: TComponent; aEnabled: Boolean);
//---------------------------------------------------------
var i: integer;
begin
  for i :=0  to aComponent.ComponentCount-1 do
   if aComponent.Components[i] is TAction then
    (aComponent.Components[i] as TAction).Enabled:=aEnabled;
end;



//---------------------------------------------------------
function GetAppVersionStr(): string;
//---------------------------------------------------------
var obj: TVersionInfo;
begin
  obj:=AppVerInfo();
  Result:=RemoveSubStr (obj.FileVersion, '.0');
  Result:=Format(' [ver %s]', [Result]);
  obj.Free;
end;

//---------------------------------------------------------
function FormatDateVar (aValue: Variant): string;
//---------------------------------------------------------
begin
  if VarExists(aValue) then
    Result:=FormatDateTime ('dd:mm:yyyy', aValue)
  else
    Result:='';
end;

//---------------------------------------------------------
function FormatDateTimeVar (aValue: Variant): string;
//---------------------------------------------------------
begin
  if VarExists(aValue) then
    Result:=FormatDateTime ('dd:mm:yyyy hh:mm', aValue)
  else
    Result:='';
end;

//--------------------------------------------------------
function CompareDates (aValue1,aValue2: TDateTime): integer;
//--------------------------------------------------------
//  1: Val1 > Val2
// -1: Val1 < Val2
//--------------------------------------------------------
var
  y1,m1,d1,y2,m2,d2: Word;
  h1, min1, s1, ms1: Word;
  h2, min2, s2, ms2: Word;

begin
//function CompareDateTime(const A, B: TDateTime): TValueRelationship;

//-1	LessThanValue	The first value is less than the second value.
//0	EqualsValue	The two values are equal.
//1	GreaterThanValue	The first value is greater than the second Value.


  DecodeDate (aValue1, y1,m1,d1);
  DecodeDate (aValue2, y2,m2,d2);

  DecodeTime (aValue1, h1, min1, s1, ms1);
  DecodeTime (aValue2, h2, min2, s2, ms2);

  if aValue1=aValue2 then
    Result:=0

  else
    if (y1>=y2) or (m1>=m2) or (d1>=d2) or (h1>=h2) or (min1>=min2) or (s1>=s2) then
      Result:=1
    else
      Result:=-1
end;


//--------------------------------------------------------
function DateTimeRemoveSec(aValue1: TDateTime): TDateTime;
//--------------------------------------------------------
var
  y1,m1,d1, h1, min1, s1, ms1: Word;
begin
  DecodeDateTime (aValue1, y1,m1,d1, h1, min1, s1, ms1);
  Result := EncodeDateTime(y1,m1,d1, h1, min1, 0, 0);
end;

(*
function Inc_(var aValue: integer): integer;
begin
  Inc(aValue);
  Result:=aValue;
end;    //
*)

// ---------------------------------------------------------------
procedure Show_Application_DataModules;
// ---------------------------------------------------------------
var
  i : Integer;
begin
  i := Application.ComponentCount;

  // ShowMessage('DBLogin.Destroy;'+IntToStr(Application.ComponentCount));

  for I := Application.ComponentCount - 1  downto 0 do
    if Application.Components[i] is TDataModule then
    begin
      ShowMessage(Application.Components[i].ClassName +'  '+ Application.Components[i].Name);

//      FreeAndNil(Application.Components[i]);
    //     ShowMessage(Application.Components[i].ClassName +'  '+ Application.Components[i].Name);

    end;
end;


// ---------------------------------------------------------------
procedure Application_DataModules_Free;
// ---------------------------------------------------------------
var
  i : Integer;
begin
  i := Application.ComponentCount;

  // ShowMessage('DBLogin.Destroy;'+IntToStr(Application.ComponentCount));

  for I := Application.ComponentCount - 1  downto 0 do
    if Application.Components[i] is TDataModule then
    begin
    //  ShowMessage(Application.Components[i].ClassName +'  '+ Application.Components[i].Name);

      (Application.Components[i] as TDataModule).Free;

    //     ShowMessage(Application.Components[i].ClassName +'  '+ Application.Components[i].Name);

    end;
end;

//-------------------------------------------------------------------
function FindForm(aClassName: string): TForm;
//-------------------------------------------------------------------
var i: integer;
begin
  result:=nil;

  for i:=0 to Application.ComponentCount-1 do
    if (Application.Components[i] is TForm) then
      if Eq (Application.Components[i].ClassName, aClassName) then
    begin
      Result:=TForm (Application.Components[i]);
      Exit;
    end;

  if Assigned(Application.MainForm) then
    for i:=0 to Application.MainForm.ComponentCount-1 do
      if (Application.MainForm.Components[i] is TForm) then
        if Eq (Application.MainForm.Components[i].ClassName, aClassName) then
      begin
        Result:=TForm (Application.MainForm.Components[i]);
        Exit;
      end;
end;


function str_DeleteLastChar(var aKeyFields: string): boolean;
begin
  Result:= true;

  if aKeyFields = '' then
  begin
    Result:= false;
    exit;
  end;

  SetLength(aKeyFields, Length(aKeyFields)-1);
end;    //


// -------------------------------------------------------------------
procedure CopyDocking1(aSrcForm: TForm; aDest: TWinControl);
// -------------------------------------------------------------------
var
  s: string;
begin
  Assert(aDest<>nil);

  s:=aDest.ClassName;

  if aDest.ClassName='TPanel' then
    TPanel(aDest).BevelOuter:=bvNone;


  aSrcForm.Align:=alClient;
  aSrcForm.ManualDock(aDest);
  aSrcForm.Show;
end;

// -------------------------------------------------------------------
procedure CopyControls(aSrcForm: TForm; aDest: TWinControl);
// -------------------------------------------------------------------
var i: integer;
begin
  Assert(aDest<>nil);

  CopyDocking1 (aSrcForm, aDest);
  Exit;

(*
  if aDest.ClassName='TPanel' then
    TPanel(aDest).BevelOuter:=bvNone;

  aSrcForm.ParentFont:=False;
  aSrcForm.Align:=alClient;

  for i:=aSrcForm.ControlCount-1 downto 0 do
    aSrcForm.Controls[i].Parent:=aDest;
*)

end;


function StrIsDouble (asValue: string): boolean;
var err: integer;
  dval: double;
begin
  asValue:= Trim (asValue);
  asValue:= ReplaceStr (asValue, ',', '.');
  Val(asValue, dval, err);
  Result:= (err=0);
end;


function StrIsInt (asValue: string): boolean;
var err: integer;
  iVal: Integer;
begin
  asValue:= Trim (asValue);
  asValue:= ReplaceStr (asValue, ',', '.');
  Val(asValue, iVal, err);
  Result:= (err=0);
end;



//-------------------------------------------------------
function  IIF_NULL (aValue: variant): variant;
//-------------------------------------------------------
begin
  if VarIsNull(aValue) or
     ((VarType(aValue)=varInteger)  and (aValue=0)) or
     ((VarType(aValue)=varSingle)   and (aValue=0)) or
     ((VarType(aValue)=varDouble)   and (aValue=0)) or
     ((VarType(aValue)=varSmallInt) and (aValue=0)) or
     ((VarType(aValue)=varString)   and (aValue=''))
  then
    Result:= NULL
  else
    Result:= aValue;
end;


//-------------------------------------------------------
function IIF_NOT_0(aValue: Double): variant;
//-------------------------------------------------------
begin
  if aValue=0 then
    Result:= NULL
  else
    Result:= aValue;
end;


procedure CursorSQL;
begin
  Screen.Cursor:=crSQLWait;
end;


var
  LTime_dt: TDateTime;


procedure SaveTime;
begin
  LTime_dt  := Now;
end;


function GetTimeDiff: string;
var
  dt: TDateTime;
  iSec: Int64;
begin
  dt:= Now - LTime_dt;


  iSec:=SecondsBetween( Now, LTime_dt );

  result := Format('%d:%d',[iSec div 60, iSec mod 60]);


//  result := FormatDateTime ('hh:mm:ss:zzz', dt);

//  ShowMessage (FormatDateTime ('hh:mm:ss:zzz', dt));
//  ShowMessage (result);

end;

function GetFileVersion: string;
var obj: TVersionInfo;
begin
  obj:=TVersionInfo.Create(Application.ExeName);

//  obj:=AppVerInfo();
  Result:='������  '+RemoveSubStr (obj.FileVersion, '.0');
  obj.Free;
end;



// -------------------------------------------------------------------
procedure CopyDockingEx (aSrcForm: TControl; aDest: TWinControl);
// -------------------------------------------------------------------
var s: string;
begin
  Assert(Assigned(aSrcForm));
  Assert(Assigned(aDest));

  s:=aDest.ClassName;

  if aDest is TPanel then
  begin
    TPanel(aDest).BevelOuter:=bvNone;
    TPanel(aDest).BevelWidth:=10;
  end;


  aSrcForm.Align:=alClient;
  aSrcForm.ManualDock(aDest);
  aSrcForm.Show;
end;

procedure CreateChildForm_(var aReference; InstanceClass: TComponentClass;
    aDestControl: TWinControl);
begin
  CreateChildForm(InstanceClass, aReference, aDestControl);
end;


//-------------------------------------------------------------------
procedure CreateChildForm(InstanceClass: TComponentClass; var aReference; aDestControl: TWinControl);
//-------------------------------------------------------------------
var
  s: string;
  oInstance: TComponent;

begin
  Assert(aDestControl<>nil);

  oInstance := TComponent(InstanceClass.NewInstance);
  TComponent(aReference) := oInstance;
  try
      oInstance.Create(aDestControl);               
//    aDestControl.InsertControl(TControl(oInstance));           

//ShowMessage(IntToStr(aDestControl.ControlCount));

  except
    TComponent(aReference) := nil;
    raise;
  end;

//  s := TComponent(aReference).ClassName;
  //Assert(s = 'TControl');
//  Assert(TComponent(aReference) is TControl);


//  if aDestControl<>nil then

  if aDestControl is TPanel then
  begin
    (aDestControl as TPanel).BevelOuter:=bvNone;
    (aDestControl as TPanel).BevelWidth:=10;
  end;

  TForm(aReference).Parent := aDestControl;
  TForm(aReference).BorderStyle:=bsNone;
  TForm(aReference).Align:=alClient;
  TForm(aReference).Show;
  

(*
  TForm(aReference).Align:=alClient;
  TForm(aReference).ManualDock(aDestControl);
  TForm(aReference).Show;

*)
 // CopyDockingEx(TForm(aReference),aDestControl);

//ShowMessage(IntToStr(aDestControl.ControlCount));

end;



procedure CursorRestore;
begin
  Screen.Cursor:=crDefault;
end;


//--------------------------------------------------------
function IsStrInStrArray(aStr: string; aStrArray: array of string): boolean;
//--------------------------------------------------------
var i: integer;
begin
  Result:= false;
  for i:=0 to High(aStrArray) do
  begin
    Result:= Eq(aStr,aStrArray[i]);
    if Result then  Exit;
  end;
end;



//-------------------------------------------------------------------
function IsFormExists(aApplication: TApplication; aClassName: string): TForm;
//-------------------------------------------------------------------
var i: integer;
begin
  Result:=nil;

  for i:=0 to aApplication.ComponentCount-1 do
    if Eq (aApplication.Components[i].ClassName, aClassName) then
    begin
      Result:=TForm (aApplication.Components[i]);
      Exit;
    end;

  if Assigned(aApplication.MainForm) then
    for i:=0 to aApplication.MainForm.ComponentCount-1 do
      if Eq (Aapplication.MainForm.Components[i].ClassName, aClassName) then
      begin
        Result:=TForm (aApplication.MainForm.Components[i]);
        Exit;
      end;
end;


// ---------------------------------------------------------------
function CompareMemoryStreams(msOne,msTwo:TMemoryStream): boolean;
// ---------------------------------------------------------------
var
  i:Integer;
  p1,p2:LongInt;
  buffer1,buffer2:Char;
begin
  result := false;

  if msOne.size <> msTwo.size then
    Exit;

  try
    p1 := msOne.position; //temp storage for position
    msOne.position := 0; //start at the beginning
    p2 := msOne.position; //temp storage for position
    msTwo.position := 0; //start at the beginning

    for i := 0 to msOne.size-1 do
    begin

      if msOne.read(buffer1,sizeOf(buffer1)) <> msTwo.read(buffer2,sizeOf(buffer2)) then
        exit;

      if buffer1 <> buffer2 then
        exit;

    end;
      result := true;
  finally
  end;
end;


//---------------------------------------------------------------------------
procedure ShellExec(aFileName: string; aFileParams: string = '');
//---------------------------------------------------------------------------
var
  r: Integer;
begin
  Assert(aFileName<>'');
 // Assert(FileExists( aFileName));

//  if aFileName<>'' then

//function ShellExecute(hWnd: HWnd; Operation, FileName, Parameters, Directory: PChar; ShowCmd: Integer): HINST; stdcall;

  r := ShellExecute(0,nil, pChar(string(aFileName)), pChar(string(aFileParams)), nil, SW_NORMAL);
  r:=0;

//  ShellExecute(0,nil, pChar(aFileName), pChar(aFileParams), nil, SW_NORMAL);
end;


procedure ShellExec_Notepad1(aFileName: string);
begin
 // ShellExecute(Handle,
 // zz'open', 'c:\MyDocuments\Letter.doc', nil, nil, SW_SHOWNORMAL);

  ShellExecute(0, 'open',
     'notepad.exe', PChar(aFileName), nil, SW_SHOWNORMAL);

 // if aFileName<>'' then
 //   ShellExecute(0,nil, pChar(string(aFileName)), pChar(string(aFileParams)), nil, SW_NORMAL);

//  ShellExecute(0,nil, pChar(aFileName), pChar(aFileParams), nil, SW_NORMAL);
end;


procedure SetApplicationName(aName : string);
begin
  ApplicationName:=aName;

  CommonApplicationDataDir:=GetCommonApplicationDataDir_Local_AppData(ApplicationName);
end;






//--------------------------------------------------------
function GetApplicationDir: string;
//--------------------------------------------------------
begin
  Result :=IncludeTrailingBackslash(ExtractFileDir(Application.ExeName));
end;


//--------------------------------------------------------
function GetApplicationIniFileName(aExeName: string = ''): string;
//--------------------------------------------------------
begin
  if aExeName='' then
    Result := ChangeFileExt(Application.ExeName, '.ini')
  else
    Result := GetApplicationDir + aExeName;


end;


procedure ExchangeInt(var aValue1,aValue2: integer);
var
  i: Integer;
begin
  i:=aValue1;
  aValue1:=aValue2;
  aValue2:=i;
end;


procedure ExchangeStr(var aValue1,aValue2: string);
var
  s: string;
begin
  s:=aValue1;
  aValue1:=aValue2;
  aValue2:=s;
end;


function GetComputerNetName: string;
var
  buffer: array[0..255] of char;
  size: dword;
begin
  size := 256;
  if Windows.GetComputerName(buffer, size) then
    Result := buffer
  else
    Result := ''
end;


function ConfirmDlg(aMsg: string): boolean;
begin
  Result:=(MessageDlg (aMsg, mtConfirmation, [mbYes,mbNo],0)=mrYes);
end;


function Eq_Any(aValue1: string; Value2: array of string): boolean;
var
  i: Integer;
begin
  Result:= false;

  for i := 0 to High(Value2) do begin
    if Eq(aValue1, Value2[i]) then begin
      Result:= true; exit;
    end;
  end;
end;


function Eq_Any_variant(aValue: variant; Value2: array of variant): boolean;
var
  i: Integer;
begin
  Result:= false;

  for i := 0 to High(Value2) do begin
    if Eq(AsString(aValue), AsString(Value2[i]) ) then begin
      Result:= true; exit;
    end;
  end;
end;


function String_Reverse1(aValue: String): String;
Var
 i : Integer;
Begin
  Result := '';
  For i := Length(aValue) DownTo 1 Do
    Result := Result + Copy(aValue,i,1) ;
end;


function DoubleQuotedStr(aValue: String): String;
Begin
  Result:='"'+aValue+'"';
end;


function StrArray (Values: array of string): TStrArray;
var i: integer;
begin
  SetLength (Result, High(Values)+1);
  for i:=0 to High(Values) do
    Result[i]:=Values[i];
end;


function StringListToStrArray(aStringList: TStringList): TStrArray;
var i: Integer;
begin
  SetLength (Result, aStringList.Count);
  for i:=0 to aStringList.Count-1 do
    Result[i]:=aStringList[i];
end;




function SArr(Values: array of string): TStrArray;
begin
  Result := StrArray(Values);
end;



function SArr(aValue1,aValue2: string): TStrArray;
begin
  Result := StrArray([aValue1,aValue2]);
end;



   {
//--------------------------------------------------------
function StringToStringList_new(aValue: string; aDelimiter: char=';'): TStringList;
////--------------------------------------------------------
var
  i: Integer;
begin
  Result:=TStringList.Create;
  Result.Delimiter:=aDelimiter;
  Result.DelimitedText:=Trim(aValue);

  for I := 0 to Result.Count - 1 do
    Result[i] := Trim(Result[i]);
end;

}


//--------------------------------------------------------
function StringToStrArray_new(aValue: string; aDelimiter: char=';'): TStrArray;
////--------------------------------------------------------
var
  i: Integer;
  oSL: TStringList;
begin
  oSL:=StringToStringList(aValue, aDelimiter);

  SetLength(Result, oSL.Count);

  for I := 0 to oSL.Count - 1 do
    Result[i] := Trim(oSL[i]);
end;



//--------------------------------------------------------
function StringToStringList(aValue: string; aDelimiter: char=';'): TStringList;
////--------------------------------------------------------
var
  i: Integer;
  iOffset: Integer;
  j: Integer;

  s: string;
  s1: string;
begin
  Assert(length(aDelimiter)>0);

  aValue := Trim(aValue);

  Result:=TStringList.Create;

 // iOffset:=1;

  iOffset:=1;

  repeat
    while (iOffset<Length(aValue)) and (aValue[iOffset] = aDelimiter) do
      Inc(iOffset);

    if (iOffset<=Length(aValue)) then
    begin
      j:=PosEx(aDelimiter, aValue, iOffset+1);
      if j=0 then
        j:=Length(aValue)+1;

      s1:=Trim(Copy(aValue,iOffset,j-iOffset));
      Result.Add(s1);

      iOffset :=j+1;
    end;
 // repeat



  until
     iOffset>Length(aValue);



 // StrToTxtFile(Result.Text, 'c:\123.txt');


//  ShowMessage(Result.Text);

//  Result.Delimiter:=aDelimiter[1];
 // Result.DelimitedText:=aValue;
end;


// ---------------------------------------------------------------
procedure ShowApplicationComponents;
// ---------------------------------------------------------------
var
  I: Integer;
  s: string;
begin
  s:='';

  for I := 0 to Application.ComponentCount - 1 do
  begin
    s:=s+ Format('%s-%s',
           [Application.Components[i].Name,
            Application.Components[i].ClassName ]) + sLineBreak;

  end;

  ShowMessage(s);
end;

//------------------------------------------------------------------
function Pos_(const aSubstr, aStr: string): boolean;
//------------------------------------------------------------------
begin
  Result:= Pos(AnsiUpperCase(aSubstr), AnsiUpperCase(aStr))>0;
end;


// ---------------------------------------------------------------
function MakeDoubleArray(aArr: array of double): TDoubleArray;
// ---------------------------------------------------------------
var I: Integer;
begin
  SetLength(Result, High(aArr)+1);

  for I := 0 to High(aArr) do
    Result[i] := aArr[i];
end;



//---------------------------------------------------------
procedure AppendStrToFile(aFileName: string; aMsg: string);
//---------------------------------------------------------
var
  oTextFile: TextFile;
begin
  Assert( aFileName<>'');


  AssignFile (oTextFile, aFileName);

  if FileExists(aFileName) then
    System.Append(oTextFile)
  else
    System.Rewrite(oTextFile);

//  Writeln(oTextFile, DateTimeToStr(now)+' : '+aMsg);
  Writeln(oTextFile, aMsg);
//  Flush(oTextFile);

  CloseFile(oTextFile);

end;


// ---------------------------------------------------------------
function DoubleArray_GetMax(aArr: TDoubleArray): Double;
// ---------------------------------------------------------------
var I: Integer;
begin
  Result := 0;
//
//  if Length(aArr)=0 then
//    Exit;
//
//  Result := aArr[0];

  for I := 0 to High(aArr) do
    if Result < aArr[i] then
      Result := aArr[i];

end;



// ---------------------------------------------------------------
procedure StrToFile(aStr: string; aFileName: string);
// ---------------------------------------------------------------
var
  list: TstringList;
begin
  ForceDirByFileName(aFileName);

  list:=TstringList.Create;
  list.Text:=aStr;
  list.SaveToFile (aFileName);
  list.Free;
end;



// ---------------------------------------------------------------
procedure StrToTextFile(aFileName, aStr: string);
// ---------------------------------------------------------------
var
  list: TstringList;
begin
  ForceDirByFileName(aFileName);

  list:=TstringList.Create;
  list.Text:=aStr;
  list.SaveToFile (aFileName);
  list.Free;
end;




procedure Dialog_SetDefaultSize(aForm: TForm);
begin
  with aForm.Constraints do
  begin
    MaxHeight:=385;
    MinHeight:=MaxHeight;
  end;

  Dialog_SetDefaultWidth(aForm);
end;


procedure Dialog_SetDefaultWidth(aForm: TForm);
begin
  with aForm.Constraints do
  begin
    MaxWidth :=505;
    MinWidth :=MaxWidth;
  end;
end;



// ---------------------------------------------------------------
function TextFileToStr(aFileName: string; var aStr: string): Boolean;
// ---------------------------------------------------------------
var oList: TstringList;
begin
  Result := FileExists(aFileName);

  oList:=TstringList.Create;
  oList.LoadFromFile (aFileName);
  aStr:=oList.Text;
  oList.Free;
end;


procedure PageControl_HideTabs(aPageControl: TPageControl; aTabVisible: Boolean
    = False);
var
  I: Integer;
begin
  for I := 0 to aPageControl.PageCount-1 do
    aPageControl.Pages[i].TabVisible:=aTabVisible;

  aPageControl.ActivePage:=aPageControl.Pages[0];

end;


procedure AssertFileExists(aFileName: string);
begin
  Assert(FileExists(aFileName), 'File not exists: ' + aFileName);

end;


// ---------------------------------------------------------------
function SearchFileInPath(aFileName: string): string;
// ---------------------------------------------------------------
var
  SPPath:Array[0..255] of char;
  pPathPtr:PChar;
  
begin
  if SearchPath(nil, PChar(aFileName), nil, 255, SPPath, pPathPtr)>0 then 
    Result:=StrPas(SPPath)
  else 
    Result:=aFileName;
end;



//-------------------------------------------------------------------
procedure CloseApplicationForms;
//-------------------------------------------------------------------
var i: integer;
begin
  for i:=Application.ComponentCount-1 downto 0 do
    if Application.Components[i] is TForm then
//    begin
      PostMessage((Application.Components[i] as TForm).Handle, WM_CLOSE, 0,0);

//      Application.Components[i].Free;
  //  end;

//  if Assigned(aApplication.MainForm) then
//    for i:=0 to aApplication.MainForm.ComponentCount-1 do
//      if Eq (Aapplication.MainForm.Components[i].ClassName, aClassName) then
//      begin
//        Result:=TForm (aApplication.MainForm.Components[i]);
//        Exit;
//      end;

end;



procedure DeleteDirectory(const aDirName: string);
//uses
//  ShellAPI;

var
  FileOp: TSHFileOpStruct;
begin
  FillChar(FileOp, SizeOf(FileOp), 0);
  FileOp.wFunc := FO_DELETE;
  FileOp.pFrom := PChar(aDirName+#0);//double zero-terminated
  FileOp.fFlags := FOF_SILENT or FOF_NOERRORUI or FOF_NOCONFIRMATION;
  SHFileOperation(FileOp);
end;



var
  I,k: Integer;
  v: Variant;
  e: double;

  sDir : string;

initialization
//  k:=PosCount('1', '112');


  sDir:=IncludeTrailingBackslash(ExtractFileDir(Application.ExeName));

  g_Is_Debug_mode := FileExists(sDir + 'debug.ini');


//  Application.Initialize;

  DecimalSeparator:='.'; // don't change this line


//  StringToStringList_new('asdas; dfsdfa ; asdfasdf; ', ';');

  //--------------------------------------------------------
 //  StringToStringList('s', ' ');
  // StringToStringList('1=3', '=');
//   StringToStringList('sdfsd werrw wer we22r  ', ' ');


//  strArr:=StringToStrArray('Longitude;Latitude;CellId;E', ';');

 // strArr:=StringToStrArray('1-3',';');

  Randomize; // to make random color

/////////  tmp_StrList:=TStringList.Create;

//  SaveTime();
 // ShowMessage('');
//  GetTimeDiff;

//finalization
 // tmp_StrList.Free;
(*
  e:=0;
  v:=IIF_NULL(e);
  e:=33.444;*)

end.




