unit u_debug;

interface
uses
  DbugIntf,
  SysUtils;


  procedure Debug (aMsg: string);
  procedure Debug_error (aMsg: string);
	procedure Debug_int(aValue: integer; aMsg: string = '');
  procedure Debug_float(aValue: double; aMsg: string = '');



implementation

procedure Debug (aMsg: string);
begin
  SendDebug(aMsg);
end;


procedure Debug_error (aMsg: string);
begin
  SendDebugError(aMsg);
end;


procedure Debug_int(aValue: integer; aMsg: string = '');
begin
  SendDebug(aMsg + IntToStr(aValue));
end;


procedure Debug_float(aValue: double; aMsg: string = '');
begin
  SendDebug(aMsg + FloatToStr(aValue));
end;



end.
