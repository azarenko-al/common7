unit d_db_ViewDataset;

interface

uses
  Classes, SysUtils, Controls, Forms, DBGrids, DB, ComCtrls, Grids;

type
  Tdlg_db_ViewDataset = class(TForm)
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    StatusBar1: TStatusBar;
    procedure FormCreate(Sender: TObject);
  private

  public
    class procedure ExecDlg(aDataSet: TDataSet; aCaption: string = '');

  end;


  procedure db_View(aDataSet: TDataSet; aCaption: string = '');

implementation

{$R *.dfm}

{var
  dlg_db_ViewDataset: Tdlg_db_ViewDataset;

}

const
  DEF_COLWIDTH = 100;

procedure db_View(aDataSet: TDataSet; aCaption: string = '');
begin
  Tdlg_db_ViewDataset.ExecDlg (aDataSet, aCaption);
end;



procedure Tdlg_db_ViewDataset.FormCreate(Sender: TObject);
begin
  DBGrid1.Align:=alClient;
end;

// ---------------------------------------------------------------
class procedure Tdlg_db_ViewDataset.ExecDlg(aDataSet: TDataSet; aCaption:
    string = '');
// ---------------------------------------------------------------
var
  i: Integer;
begin
  with Tdlg_db_ViewDataset.Create(Application) do
  begin
    DataSource1.DataSet:=aDataSet;

    if aCaption<>'' then
      Caption:=aCaption;
      

    for i:=0 to DBGrid1.Columns.Count-1 do
      if DBGrid1.Columns[i].Width>DEF_COLWIDTH then
        DBGrid1.Columns[i].Width:=DEF_COLWIDTH;

(*
   // i:=0;
    aDataSet.First;

    while not aDataSet.EOF do
    begin
  //    Inc(i);
      aDataSet.Next;
    end;
*)

    StatusBar1.SimpleText := Format('record count: %d', [aDataSet.recordCount]);
//    StatusBar1.SimpleText := Format('record count: %d', [i]);


    ShowModal;
    Free;
  end;
end;


end.
