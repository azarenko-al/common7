unit UnicodeFile;

interface


uses Windows, SysUtils, StrUtils, Classes,

  u_func
  ;


type
  TUnicodeFile = class(TStringList)
  private
    FMemStream: TMemoryStream;
    UnicodeBuf: PChar;

    function  IsUnicodeFile(const FileName: String): Boolean;
    procedure LoadFromUnicodeFile(const FileName: String);
  public
    procedure LoadFromFile(const FileName: String); override;
    procedure SaveToUnicodeFile(const FileName: String);

    constructor Create;
    destructor  Destroy; override;
  end;


implementation


//------------------------------------------------------------------------------
constructor TUnicodeFile.Create;
//------------------------------------------------------------------------------
begin
  inherited Create;
  FMemStream:= TMemoryStream.Create;
end;

//------------------------------------------------------------------------------
destructor TUnicodeFile.Destroy;
//------------------------------------------------------------------------------
begin
  FMemStream.Free;
  inherited Destroy;
end;

//------------------------------------------------------------------------------
function TUnicodeFile.IsUnicodeFile(const FileName: String): Boolean;
//------------------------------------------------------------------------------
var
  UnicodeMagic: Word;
begin
  Result := False;
  FMemStream.LoadFromFile(FileName);
  if FMemStream.Size < 2 then
    Result := False;
  FMemStream.Position := 0;
  FMemStream.Read(UnicodeMagic, 2);
  if UnicodeMagic <> $FEFF then
    Result := False
  else
    Result := True;
  FMemStream.Clear;
end;

//------------------------------------------------------------------------------
procedure TUnicodeFile.LoadFromFile(const FileName: String);
//------------------------------------------------------------------------------
begin
  if IsUnicodeFile(FileName) then
    LoadFromUnicodeFile(FileName)
  else
    inherited LoadFromFile(FileName);
end;

//------------------------------------------------------------------------------
procedure TUnicodeFile.LoadFromUnicodeFile(const FileName: String);
//------------------------------------------------------------------------------
var
  UnicodeMagic: Word;
  i: Integer;
begin
  Clear;
  FMemStream.LoadFromFile(FileName);
  if FMemStream.Size < 2 then
    Exit;
  FMemStream.Position := 0;
  FMemStream.Read(UnicodeMagic, 2);
  if UnicodeMagic <> $FEFF then
    Exit;
  FMemStream.Position := 2;
  UnicodeBuf := StrAlloc(FMemStream.Size);
  try
    ZeroMemory(UnicodeBuf, FMemStream.Size);
    FMemStream.Read(UnicodeBuf^, FMemStream.Size-2);
    Text:= WideCharToString(PWideChar(UnicodeBuf));
    for i := 0 to Count - 1 do
      Strings[i]:= Trim(Strings[i]);
  finally
    StrDispose(UnicodeBuf);
    FMemStream.Clear;
  end;
end;

//------------------------------------------------------------------------------
procedure TUnicodeFile.SaveToUnicodeFile(const FileName: String);
//------------------------------------------------------------------------------
var
  UnicodeDest: PWideChar;
  UnicodeMagic: Word;
begin
  FMemStream.Clear;
  FMemStream.Position := 0;
  UnicodeMagic := $FEFF;
  FMemStream.Write(UnicodeMagic, 2);
  FMemStream.Position := 2;
  GetMem(UnicodeDest, (Length(Text)+1)*2);
  try
    ZeroMemory(UnicodeDest, (Length(Text)+1)*2);
    StringToWideChar(Text, UnicodeDest, Length(Text)+1);
    FMemStream.Write(UnicodeDest^, Length(Text)*2);
    FMemStream.SaveToFile(FileName);
  finally
    FreeMem(UnicodeDest);
    FMemStream.Clear;
  end;
end;


end.
