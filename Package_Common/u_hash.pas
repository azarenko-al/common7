unit u_hash;

interface
uses
  Classes, SysUtils, IdHashMessageDigest, idHash;

{
function MD5_from_file_(const aFileName: string): string;
function MD5_from_str_(const aValue: string): string;
 }

//  SysUtils, DCPcrypt2, DCPhaval, DCPsha512;


 // function GetHashFromStr(aString: string): string;


//==============================================================================
implementation
//==============================================================================



//returns MD5 has for a file
// ---------------------------------------------------------------
function MD5_from_file_(const aFileName: string): string;
// ---------------------------------------------------------------
var
  idmd5 : TIdHashMessageDigest5;
  fs : TFileStream;
  hash : T4x4LongWordRecord;
begin
  idmd5 := TIdHashMessageDigest5.Create;
  fs := TFileStream.Create(aFileName, fmOpenRead OR fmShareDenyWrite) ;
  try
    result := idmd5.AsHex(idmd5.HashValue(fs)) ;
  finally
    fs.Free;
    idmd5.Free;
  end;
end;


//returns MD5 has for a file
// ---------------------------------------------------------------
function MD5_from_str_(const aValue: string): string;
// ---------------------------------------------------------------
var
  idmd5 : TIdHashMessageDigest5;
  fs : TStringStream;
  hash : T4x4LongWordRecord;
begin
  idmd5 := TIdHashMessageDigest5.Create;
  fs := TStringStream.Create(aValue) ;
  try
    result := idmd5.AsHex(idmd5.HashValue(fs)) ;
  finally
    fs.Free;
    idmd5.Free;
  end;
end;



end.
