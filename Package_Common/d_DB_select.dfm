inherited dlg_DB_select: Tdlg_DB_select
  Left = 846
  Top = 220
  VertScrollBar.Range = 0
  ActiveControl = DBGrid1
  BorderStyle = bsDialog
  Caption = #1042#1099#1073#1086#1088
  ClientHeight = 500
  ClientWidth = 403
  FormStyle = fsStayOnTop
  OldCreateOrder = True
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 465
    Width = 403
    DesignSize = (
      403
      35)
    inherited Bevel1: TBevel
      Width = 403
    end
    inherited btn_Ok: TButton
      Left = 240
    end
    inherited btn_Cancel: TButton
      Left = 324
    end
  end
  inherited pn_Top_: TPanel
    Width = 403
    inherited Bevel2: TBevel
      Width = 403
    end
    inherited pn_Header: TPanel
      Width = 403
      Visible = False
    end
  end
  object pn_Main: TPanel [2]
    Left = 0
    Top = 60
    Width = 403
    Height = 207
    Align = alTop
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 2
    object DBGrid1: TDBGrid
      Left = 5
      Top = 5
      Width = 393
      Height = 197
      Align = alClient
      DataSource = DataSource1
      Options = [dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = DBGrid1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'name'
          Width = 292
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'id'
          Visible = False
        end>
    end
  end
  inherited ActionList1: TActionList
    Left = 76
    Top = 8
  end
  inherited FormStorage1: TFormStorage
    Options = []
    Left = 104
    Top = 8
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 236
    Top = 60
  end
  object ADOQuery1: TADOQuery
    Parameters = <>
    Left = 240
    Top = 16
  end
end
