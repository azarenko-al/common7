unit dm_Progress;

interface

uses
  Classes, 

  d_Progress
  ;

type
//  TOnDMProgressEvent = procedure (aProgress,aMax: integer; var aTerminated: boolean; var aPause : boolean; var aSkiped : boolean) of object;
  TOnDMProgressEvent = procedure (aProgress,aMax: integer; var aTerminated: boolean) of object;
  TOnDMLogEvent      = procedure (aMsg: string) of object;

  //----------------------------------------------------------
  TdmProgress = class(TDataModule)
  //----------------------------------------------------------
  private
    Fdlg_Progress_adv: Tdlg_Progress;

    procedure DoOnStartProgressDlg(Sender: TObject);
  protected
    FOnProgress,
    FOnProgress2: TOnDMProgressEvent;

    FOnLog,
    FOnLog2,
    FOnProgressMsg,
    FOnProgressMsg2:  TOnDMLogEvent;

    FOnCaption: TOnDMLogEvent;

    FOnAfterDone:  TNotifyEvent;

    procedure DoLog (aMsg: string);
    procedure DoLog2 (aMsg: string);
    procedure DoCaption (aMsg: string);

    procedure DoProgress (aPosition,aTotal: integer);
    procedure DoProgress2 (aPosition,aTotal: integer);

    procedure DoProgressMsg (aMsg: string);
    procedure DoProgressMsg2 (aMsg: string);

    procedure DoProgressMsgAndLog(aMsg: string);

    procedure ExecuteProc; virtual; 
  public
    Terminated: boolean;
    Pause     : boolean;
    Skipped   : boolean;

    ProgressStep: integer;
    Progress2Step: integer;

    procedure Terminate();
    procedure ExecuteDlg (aCaption: string='');

// TODO: DlgSetShowDetails
//  procedure DlgSetShowDetails (aValue: boolean);

    property OnCaption:   TOnDMLogEvent read FOnCaption write FOnCaption;
    property OnProgress:  TOnDMProgressEvent read FOnProgress write FOnProgress;
    property OnProgress2: TOnDMProgressEvent read FOnProgress2 write FOnProgress2;
    property OnLog:       TOnDMLogEvent read FOnLog write FOnLog;
    property OnLog2:      TOnDMLogEvent read FOnLog2 write FOnLog2;

    property OnProgressMsg:  TOnDMLogEvent read FOnProgressMsg write FOnProgressMsg;
    property OnProgressMsg2: TOnDMLogEvent read FOnProgressMsg2 write FOnProgressMsg2;

    property OnAfterDone:  TNotifyEvent read FOnAfterDone write FOnAfterDone;

  end;

{
var
  dmProgress: TdmProgress;
}


//==================================================================
implementation

uses SysUtils;  {$R *.DFM}
//==================================================================


//-----------------------------------------------------------------
procedure TdmProgress.DoProgress (aPosition,aTotal: integer);
//-----------------------------------------------------------------
begin
  if (ProgressStep=0) or
     (ProgressStep>0) and (aPosition mod ProgressStep=0)
  then
//    if Assigned(FOnProgress) then FOnProgress(aPosition,aTotal,Terminated,Pause,Skipped);
    if Assigned(FOnProgress) then FOnProgress(aPosition,aTotal,Terminated);
end;

//-----------------------------------------------------------------
procedure TdmProgress.DoProgress2(aPosition, aTotal: integer);
//-----------------------------------------------------------------
begin
 // if aPosition<0 then
 //   raise Exception.Create('');

  if (Progress2Step=0) or
     (Progress2Step>0) and (aPosition mod Progress2Step=0)
  then
//    if Assigned(FOnProgress2) then FOnProgress2(aPosition,aTotal,Terminated, Pause, Skipped);
    if Assigned(FOnProgress2) then FOnProgress2(aPosition,aTotal,Terminated);
end;

//-----------------------------------------------------------------
procedure TdmProgress.DoLog(aMsg: string);
//-----------------------------------------------------------------
begin
  if Assigned(FOnLog) then  FOnLog(aMsg);
end;

//-----------------------------------------------------------------
procedure TdmProgress.DoLog2(aMsg: string);
//-----------------------------------------------------------------
begin
  if Assigned(FOnLog2) then  FOnLog2(aMsg);
end;

//-----------------------------------------------------------------
procedure TdmProgress.DoCaption(aMsg: string);
//-----------------------------------------------------------------
begin
  if Assigned(FOnCaption) then  FOnCaption(aMsg);
end;


//-----------------------------------------------------------------
procedure TdmProgress.Terminate();
//-----------------------------------------------------------------
begin
  Terminated:=True;
end;


//-----------------------------------------------------------------
procedure TdmProgress.DoProgressMsg(aMsg: string);
//-----------------------------------------------------------------
begin
  if Assigned(FOnProgressMsg) then
    FOnProgressMsg(aMsg);

//  DoLog(aMsg);
end;

//-----------------------------------------------------------------
procedure TdmProgress.DoProgressMsgAndLog(aMsg: string);
//-----------------------------------------------------------------
begin
  DoProgressMsg(aMsg);
  DoLog(aMsg);
end;

//-----------------------------------------------------------------
procedure TdmProgress.DoProgressMsg2(aMsg: string);
//-----------------------------------------------------------------
begin
  if Assigned(FOnProgressMsg2) then
    FOnProgressMsg2(aMsg);
//  DoLog(aMsg);
end;

//-----------------------------------------------------------------
procedure TdmProgress.DoOnStartProgressDlg(Sender: TObject);
//-----------------------------------------------------------------
begin
  ExecuteProc();
 end;

// TODO: DlgSetShowDetails
////-----------------------------------------------------------------
//procedure TdmProgress.DlgSetShowDetails (aValue: boolean);
////-----------------------------------------------------------------
//begin
//if Assigned(Fdlg_Progress_adv) then
//  Fdlg_Progress_adv.ShowDetails (aValue);
//end;

//-----------------------------------------------------------------
procedure TdmProgress.ExecuteDlg (aCaption: string='');
//-----------------------------------------------------------------
begin
  Terminated:=False;

  Fdlg_Progress_adv:=Tdlg_Progress.CreateForm(aCaption);
  Fdlg_Progress_adv.OnStart:=DoOnStartProgressDlg;

  OnProgress :=Fdlg_Progress_adv.DoOnProgress1;
  OnProgress2:=Fdlg_Progress_adv.DoOnProgress2;
  OnLog      :=Fdlg_Progress_adv.Log;
//  OnLog2     :=Fdlg_Progress_adv.DoOnLog;

  OnProgressMsg:= Fdlg_Progress_adv.DoOnProgressMsg;
  OnProgressMsg2:= Fdlg_Progress_adv.DoOnProgressMsg2;

  Fdlg_Progress_adv.ShowModal;

  FreeAndNil(Fdlg_Progress_adv);

  OnProgress :=nil;
  OnProgress2:=nil;
  OnLog      :=nil;
//  OnLog2     :=Fdlg_Progress_adv.DoOnLog;

  OnProgressMsg:= nil;
  OnProgressMsg2:= nil;


(*
  Fdlg_Progress_adv.Free;

  Fdlg_Progress_adv:=nil;
*)

////////////  Application.ProcessMessages;

  if Assigned(FOnAfterDone) then
    FOnAfterDone(Self);

end;


//-----------------------------------------------------------------
procedure TdmProgress.ExecuteProc;
//-----------------------------------------------------------------
begin
  Terminated:=False; 
end;



end.