unit x_main;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, ActiveX, Classes, Forms,

  ComObj, mapx_tools_TLB, StdVcl;

type
  TMapX_tools_ = class(TTypedComObject, IMapX)
  protected
    function Method1: HResult; stdcall;
    function Method2: HResult; stdcall;
    function Get_Version: WideString; stdcall;
    
  end;

implementation

uses ComServ;

function TMapX_tools_.Method1: HResult;
begin

end;

function TMapX_tools_.Method2: HResult;
begin

end;

function TMapX_tools_.Get_Version: WideString;
begin

end;

initialization
  TTypedComObjectFactory.Create(ComServer, TMapX_tools_, Class_MapX_tools_,
    ciMultiInstance, tmApartment);
end.
