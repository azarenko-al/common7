unit mapx_tools_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 31.07.2015 15:41:08 from Type Library described below.

// ************************************************************************  //
// Type Lib: W:\common7\Package_Common\map_dll\mapx_tools.tlb (1)
// LIBID: {00363252-0186-4A06-8825-D2DC54303F54}
// LCID: 0
// Helpfile: 
// HelpString: mapx_tools Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
// Errors:
//   Hint: TypeInfo 'mapx_tools' changed to 'mapx_tools_'
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  mapx_toolsMajorVersion = 1;
  mapx_toolsMinorVersion = 0;

  LIBID_mapx_tools: TGUID = '{00363252-0186-4A06-8825-D2DC54303F54}';

  IID_IMapX: TGUID = '{C69A6B80-ECE2-4123-86AA-5893729BEB2E}';
  CLASS_mapx_tools_: TGUID = '{704969F3-D7CD-41B0-973D-65B31E7A249F}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IMapX = interface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  mapx_tools_ = IMapX;


// *********************************************************************//
// Interface: IMapX
// Flags:     (256) OleAutomation
// GUID:      {C69A6B80-ECE2-4123-86AA-5893729BEB2E}
// *********************************************************************//
  IMapX = interface(IUnknown)
    ['{C69A6B80-ECE2-4123-86AA-5893729BEB2E}']
    function Method2: HResult; stdcall;
    function Get_Version: WideString; stdcall;
  end;

// *********************************************************************//
// The Class Comapx_tools_ provides a Create and CreateRemote method to          
// create instances of the default interface IMapX exposed by              
// the CoClass mapx_tools_. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  Comapx_tools_ = class
    class function Create: IMapX;
    class function CreateRemote(const MachineName: string): IMapX;
  end;

implementation

uses ComObj;

class function Comapx_tools_.Create: IMapX;
begin
  Result := CreateComObject(CLASS_mapx_tools_) as IMapX;
end;

class function Comapx_tools_.CreateRemote(const MachineName: string): IMapX;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_mapx_tools_) as IMapX;
end;

end.
