library mapx_tools;

uses
  ComServ,
  mapx_tools_TLB in 'mapx_tools_TLB.pas',
  x_main in 'x_main.pas' {MapX_tools: CoClass},
  d_Map_Label_Setup in '..\Map\d_Map_Label_Setup.pas' {dlg_Map_Label_Setup};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
