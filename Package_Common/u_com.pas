unit u_com;

interface
uses
  Windows, shellapi,  Forms, Sysutils, Dialogs,  ActiveX;

  function GetComObject(aDllFileName: string; const CLSID, IID: TGUID; var aObj):
      THandle;

  function RegisterOCX(aFileName: string): Boolean;

  function GetModuleName: string;



implementation



// ---------------------------------------------------------------
function GetModuleName: string;
// ---------------------------------------------------------------
var
  szFileName: array[0..MAX_PATH] of AnsiChar;
begin
  FillChar(szFileName, SizeOf(szFileName), #0);
  //unit SysInit;
  GetModuleFileName(hInstance, szFileName, MAX_PATH);
  Result := szFileName;
end;



// ---------------------------------------------------------------
function RegisterOCX(aFileName: string): Boolean;
// ---------------------------------------------------------------
type
  TDllRegisterServer = function: HResult; stdcall;
var
  h: THandle;
  iRet: Integer;
  RegFunc: TDllRegisterServer;
  sErr: string;
begin
  Assert(FileExists(aFileName), aFileName);


  h := LoadLibrary(PChar(aFileName));

  if h = 0 then
  begin
    iRet := GetLastError();

    sErr := SysErrorMessage(iRet);
    ShowMessage(sErr);
  end; //...


  Assert(h>0);

  RegFunc := GetProcAddress(h, 'DllRegisterServer');
  if @RegFunc <> nil then
    Result := RegFunc = S_OK
  else
    Result := False;

//  Assert(Result);

  FreeLibrary(h);
end;

{
function RegisterOCX(FileName: string): Boolean;
type
  TDllRegisterServer = function: HResult; stdcall;
var
  h: THandle;
  RegFunc: TDllRegisterServer;
begin
  h := LoadLibrary(PAnsiChar(FileName));
  RegFunc := GetProcAddress(h, 'DllRegisterServer');
  if @RegFunc <> nil then
    Result := RegFunc = S_OK
  else
    Result := False;

  FreeLibrary(h);
end;

}

// ---------------------------------------------------------------
function ExecAndWait(const ExecuteFile, ParamString : string): boolean;
// ---------------------------------------------------------------
//shellapi
var
  SEInfo: TShellExecuteInfo;
  ExitCode: DWORD;
begin
  FillChar(SEInfo, SizeOf(SEInfo), 0);
  SEInfo.cbSize := SizeOf(TShellExecuteInfo);

  with SEInfo do begin
    fMask := SEE_MASK_NOCLOSEPROCESS;
    Wnd := Application.Handle;
    lpFile := PChar(ExecuteFile);
    lpParameters := PChar(ParamString);
    nShow := SW_HIDE;
  end;

  if ShellExecuteEx(@SEInfo) then
  begin
    repeat
      Application.ProcessMessages;
      GetExitCodeProcess(SEInfo.hProcess, ExitCode);
      
    until
       (ExitCode  = STILL_ACTIVE) or Application.Terminated;

    Result:=True;
  end
  else Result:=False;
end;



//-------------------------------------------------------------------
function GetComObject(aDllFileName: string; const CLSID, IID: TGUID; var aObj):
    THandle;
//-------------------------------------------------------------------
type
  TGetObject = function(const CLSID, IID: TGUID; var aObj): HResult; stdcall;
var
  GetObject: TGetObject;
  vFactory: IClassFactory;

begin
  Assert(FileExists(aDllFileName));


  Result:=LoadLibrary(PChar(aDllFileName));
  Integer(aObj):=0;

  if Result >= 32 then
  begin
    @GetObject:=GetProcAddress(Result,'DllGetClassObject');

    if Assigned(GetObject) then
      if GetObject(CLSID,IClassFactory,vFactory) = S_OK then
        vFactory.CreateInstance(nil,IID,aObj);

    vFactory:=nil
  end
end;


end.



{

uses shellapi;
...
function ExecAndWait(const ExecuteFile, ParamString : string): boolean;
var
SEInfo: TShellExecuteInfo;
ExitCode: DWORD;
begin
FillChar(SEInfo, SizeOf(SEInfo), 0);
SEInfo.cbSize := SizeOf(TShellExecuteInfo);
with SEInfo do begin
fMask := SEE_MASK_NOCLOSEPROCESS;
Wnd := Application.Handle;
lpFile := PChar(ExecuteFile);
lpParameters := PChar(ParamString);
nShow := SW_HIDE;
end;
if ShellExecuteEx(@SEInfo) then
begin
repeat
Application.ProcessMessages;
GetExitCodeProcess(SEInfo.hProcess, ExitCode);
until (ExitCode STILL_ACTIVE) or Application.Terminated;
Result:=True;
end
else Result:=False;
end;




procedure RegisterOCX;
type
TRegFunc = function : HResult; stdcall;
var
ARegFunc : TRegFunc;
aHandle : THandle;
ocxPath : string;
begin
try
ocxPath := ExtractFilePath(Application.ExeName) + 'Flash.ocx';
aHandle := LoadLibrary(PChar(ocxPath));
if aHandle 0 then
begin
ARegFunc := GetProcAddress(aHandle,'DllRegisterServer');
if Assigned(ARegFunc) then
begin
ExecAndWait('regsvr32','/s ' + ocxPath);
end;
FreeLibrary(aHandle);
end;
except
ShowMessage(Format('Unable to register %s', [ocxPath]));
end;
end;}

