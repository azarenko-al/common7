unit u_dlg;

interface

uses SySutils,Controls, Menus, Classes, ComCtrls, Forms, Windows, Messages,
     StdCtrls, Dialogs;

const
  MSG_ASK_DEL_FILES = '������� ����� ?';
  ERROR_FILE_NOT_EXISTS = '���� �� ������: %s';


//  ed_FileName.Filter := 'XML (*.xml)|*.xml';
   

  //---------------------------------------------------
  // DIALOGS
  //---------------------------------------------------
  function  ConfirmDlg (aMsg: string): boolean;
  procedure MsgDlg     (aMsg: string);
  procedure ErrorDlg   (aMsg: string);
  procedure ErrDlg     (aMsg: string);
  procedure ExceptDlg  (aMsg: string);
  procedure WarningDlg (aMsg: string);

   procedure ErrDlg_FileNotExists (aFileName: string);


  function ShowSaveDialog (AOwner: TComponent; aTitle,aDefaultExt,aFilter,aFileName:string): string;


  function ShowSaveFileDialog(aTitle, aDefaultExt, aFilter, aDefaultFileName:
      string): string;
  function ShowOpenFileDialog(aTitle, aDefaultExt, aFilter, aDefaultFileName:
      string): string;
      
//  function ShowOpenDirDialog (aTitle:string): string;

  //---------------------------------------------------
  // WinControl functions
  //---------------------------------------------------
  // ����������� ���� ������ �� ������
//  procedure ClearControls (aSrcForm: TWinControl);
//  procedure InitComboBox (aComboBox: TComboBox; aItemIndex: integer);

//  procedure ComboBox_AssignStrArray (aComboBox: TComboBox; aArr: TStrArray);

//  procedure DisableControls_ (aParentControl: TWinControl);
//  procedure EnableControls_ (aParentControl: TWinControl);


  //---------------------------------------------------
  // Menu functions
  //---------------------------------------------------
  function dlg_AddMenuItem (aOwner: TObject;
                            aAction: TBasicAction;
                            aCaption: string=''): TMenuItem; overload;

  function menu_AddGroup (aOwner: TObject; aCaption: string=''): TMenuItem;
//  function AddMenuItem (aOwner: TObject;
  //                      aAction: TBasicAction=nil): TMenuItem; overload;

  function AddMenuDelimiter (aOwner: TObject): TMenuItem;

  procedure CopyMenu1111111111(aMenu: TMainMenu; aToolbar: TToolBar);


(*  function CreateMenuRadioItem111111111(aOwner: TPopupMenu; aCaption:string;
      aOnClickProc: TNotifyEvent): TMenuItem;
*)

  procedure dlg_ScrollMemo (aHandle: THandle);
  procedure ScrollListView (aListView: TListView);


  function dlg_GetComboBoxFocusedObjectData (Sender: TComboBox): integer;


  procedure DisableMenu (aMenu: TMenu);
  procedure EnableMenu  (aMenu: TMenu);

  function MyMessageDialog(const Msg: string; DlgType: TMsgDlgType;
     Buttons: TMsgDlgButtons; Captions: array of string): Integer;

procedure dlg_SetDefaultWidth(aForm: TForm);

procedure dlg_SetDefaultSize(aForm: TForm);

function MyMessageDlg(aMsg: string; aDlgTypt: TmsgDlgType; abutton:
    TMsgDlgButtons; aCaption: ARRAY OF string; adlgcaption: string): Integer;


//  function dlg_GetListBoxSelectedString (aListBox: TListBox): string;
 // procedure EnableChildControls (Value: boolean; aParentControl,aExceptControl: TWinControl);

{  procedure EnableChildControls (aParentControl: TWinControl);
  procedure DisableChildControls (aParentControl,aExceptControl: TWinControl);
}


//==================================================================
implementation
//==================================================================

  function AddPopupMenuItem (aPopupMenu: TPopupMenu; aParentItem: TMenuItem;
                        aAction: TBasicAction): TMenuItem; forward;


// ---------------------------------------------------------------
function MyMessageDialog(const Msg: string; DlgType: TMsgDlgType;
   Buttons: TMsgDlgButtons; Captions: array of string): Integer;
// ---------------------------------------------------------------
 var
   aMsgDlg: TForm;
   i: Integer;
   dlgButton: TButton;
   CaptionIndex: Integer;
 begin
   { Create the Dialog }
   { Dialog erzeugen }
   aMsgDlg := CreateMessageDialog(Msg, DlgType, Buttons);
   captionIndex := 0;
   { Loop through Objects in Dialog }
   { Uber alle Objekte auf dem Dialog iterieren}

   for i := 0 to aMsgDlg.ComponentCount - 1 do
   begin
    { If the object is of type TButton, then }
    { Wenn es ein Button ist, dann...}
     if (aMsgDlg.Components[i] is TButton) then
     begin
       dlgButton := TButton(aMsgDlg.Components[i]);
       if CaptionIndex > High(Captions) then Break;
       { Give a new caption from our Captions array}
       { Schreibe Beschriftung entsprechend Captions array}
       dlgButton.Caption := Captions[CaptionIndex];
       Inc(CaptionIndex);
     end;
   end;
   Result := aMsgDlg.ShowModal;
 end;



 procedure test_MyMessageDialog(Sender: TObject);
 begin
   if MyMessageDialog('How much...?', mtConfirmation, mbOKCancel,
     ['1', '2']) = mrOk then
     ShowMessage('"1" clicked')
   else
     ShowMessage('"2" clicked');
 end;

 

function img_GetListBoxSelectedString (aListBox: TListBox): string;
var ind: integer;
begin
  Result:='';
  with aListBox do //begin
    if Items.Count>0 then Result:=Items[ItemIndex];
//    if Items.Count=1 then Result:=Items[0];
//  end;
end;


procedure dlg_ScrollMemo (aHandle: THandle);
begin
  SendMessage (aHandle, EM_LINESCROLL, 0, 1);
end;


procedure ScrollListView (aListView: TListView);
begin
  PostMessage (aListView.Handle, WM_VSCROLL, 3, 0);
end;


procedure dlg_SetDefaultSize(aForm: TForm);
begin
  with aForm.Constraints do
  begin
    MaxHeight:=385;
    MinHeight:=MaxHeight;
  end;

  dlg_SetDefaultWidth(aForm);
end;


procedure dlg_SetDefaultWidth(aForm: TForm);
begin
  with aForm.Constraints do
  begin
    MaxWidth :=505;
    MinWidth :=MaxWidth;
  end;
end;




function ConfirmDlg (aMsg: string): boolean;
begin
  Result:=(MessageDlg (aMsg, mtConfirmation, [mbYes,mbNo],0)=mrYes);
end;

procedure ErrorDlg (aMsg: string);
begin
  MessageDlg (aMsg, mtError, [mbOk], 0);
end;

procedure ErrDlg (aMsg: string);
begin
  MessageDlg (aMsg, mtError, [mbOk], 0);
end;

procedure WarningDlg  (aMsg: string);
begin
  MessageDlg (aMsg, mtWarning, [mbOk], 0);
end;


procedure ErrDlg_FileNotExists (aFileName: string);
begin
  ErrDlg (Format(ERROR_FILE_NOT_EXISTS,[aFileName]));
end;


procedure ExceptDlg (aMsg: string);
begin
  raise Exception.Create (aMsg);
end;

procedure MsgDlg (aMsg: string);
begin
  MessageDlg (aMsg, mtInformation, [mbOk], 0);
end;

function menu_AddGroup (aOwner: TObject; aCaption: string=''): TMenuItem;
begin
  Result:=dlg_AddMenuItem (aOwner, nil, aCaption);
end;


function AddMenuItem (aOwner: TObject; aAction: TBasicAction=nil): TMenuItem;
begin
  Result:=dlg_AddMenuItem (aOwner, aAction);
end;

function AddMenuDelimiter (aOwner: TObject): TMenuItem;
begin
  Result:=AddMenuItem (aOwner, nil);
end;

//-------------------------------------------------------------------
function dlg_AddMenuItem (aOwner: TObject;
                          aAction: TBasicAction; aCaption: string=''): TMenuItem;
//-------------------------------------------------------------------
begin
  if (aOwner is TPopupMenu) then
    Result:=AddPopupMenuItem ((aOwner as TPopupMenu), nil, aAction)
  else

  if (aOwner is TMenuItem) then
    Result:=AddPopupMenuItem (nil, (aOwner as TMenuItem), aAction);

  if aCaption<>'' then  Result.Caption:=aCaption;
end;

//-------------------------------------------------------------------
function AddPopupMenuItem (aPopupMenu: TPopupMenu;
                       aParentItem: TMenuItem;
                       aAction: TBasicAction): TMenuItem;
//-------------------------------------------------------------------
begin
  Result:=TMenuItem.Create(Application); //aPopupMenu);
  if aAction=nil then Result.Caption:='-' // Action:=
                 else Result.Action:=aAction;
//  if aAction<>nil then
//    aAction.Tag:=Integer(aTag);

  if Assigned(aParentItem) then
    aParentItem.Add (Result)
  else
    aPopupMenu.Items.Add (Result);
end;


// ---------------------------------------------------------------
function MyMessageDlg(aMsg: string; aDlgTypt: TmsgDlgType; abutton:
    TMsgDlgButtons; aCaption: ARRAY OF string; adlgcaption: string): Integer;
// ---------------------------------------------------------------
//
//    k :=  MyMessageDlg(oStrList.Text, mtWarning, [mbYes, mbNo],
//        ['����������','���������'], '��������������' );
//    Result := k = 6;


var
  aMsgdlg: TForm;
  i: Integer;
  Dlgbutton: Tbutton;
  Captionindex: Integer;
begin
  aMsgdlg := createMessageDialog(aMsg, aDlgTypt, abutton);
  aMsgdlg.Caption := adlgcaption;
  aMsgdlg.BiDiMode := bdRightToLeft;

  Captionindex := 0;
  
  for i := 0 to aMsgdlg.componentcount - 1 Do
  begin
    if (aMsgdlg.components[i] is Tbutton) then
    Begin
      Dlgbutton := Tbutton(aMsgdlg.components[i]);
      if Captionindex <= High(aCaption) then
        Dlgbutton.Caption := aCaption[Captionindex];
      inc(Captionindex);
    end;
  end;

  Result := aMsgdlg.Showmodal;


//  ModalResults: array[TMsgDlgBtn] of Integer = (
//    mrYes, mrNo, mrOk, mrCancel, mrAbort, mrRetry, mrIgnore, mrAll, mrNoToAll,
//    mrYesToAll, 0, mrClose);

//  MyMessageDlg('Hello World!'; mtInformation; [mbYes, mbNo];
//      ['Yessss','Noooo']; 'New MessageDlg Box'):

end;


{procedure InitComboBox (aComboBox: TComboBox; aItemIndex: integer);
begin
  with aComboBox do begin Style:=csDropDownList; ItemIndex:=aItemIndex; end;
end;

}

(*
procedure ComboBox_AssignStrArray (aComboBox: TComboBox; aArr: TStrArray);
var i:integer;
begin
  aComboBox.Items.Clear;
  for i:=0 to High(aArr) do  aComboBox.Items.Add(aArr[i]);
end;

*)

// ---------------------------------------------------------------
function ShowOpenFileDialog(aTitle, aDefaultExt, aFilter, aDefaultFileName:
    string): string;
var OpenDialog1: TOpenDialog;
begin
   OpenDialog1:=TOpenDialog.Create (Application);
   OpenDialog1.Title     :=aTitle;
   OpenDialog1.DefaultExt:=aDefaultExt;
   OpenDialog1.Filter    :=aFilter;
   OpenDialog1.FileName  :=aDefaultFileName;

   if OpenDialog1.Execute then Result:=OpenDialog1.FileName
                          else Result:=aDefaultFileName;
   OpenDialog1.Free;
end;

// ---------------------------------------------------------------
function ShowSaveFileDialog(aTitle, aDefaultExt, aFilter, aDefaultFileName:
    string): string;
var SaveDialog1: TSaveDialog;
begin
   SaveDialog1:=TSaveDialog.Create (Application);
   SaveDialog1.Title     :=aTitle;
   SaveDialog1.DefaultExt:=aDefaultExt;
   SaveDialog1.Filter    :=aFilter;
   SaveDialog1.FileName  :=aDefaultFileName;

   if SaveDialog1.Execute then Result:=SaveDialog1.FileName
                          else Result:=aDefaultFileName;
   SaveDialog1.Free;
end;


// -------------------------------------------------------------------
procedure CopyMenu1111111111(aMenu: TMainMenu; aToolbar: TToolBar);
// -------------------------------------------------------------------
var  i: integer;
     b: TToolButton;

  //  ����������� ��������� ����������� ��������� �� ������ ���� � ������
  procedure CreateDropdownMenu(aToItems: TMenuItem; aFromItem: TMenuItem);
  var m: TMenuItem;
      j: integer;
  begin
    for j := 0 to aFromItem.Count - 1 do
    begin
      m := TMenuItem.Create(aToItems.Owner);
      m.Caption := aFromItem.Items[j].Caption;
      m.Action := aFromItem.Items[j].Action;
      aToItems.Add(m);
      if aFromItem.Items[j].Count > 0 then
         CreateDropdownMenu(m, aFromItem.Items[j]);
    end;

  end;

begin
  for i := aMenu.Items.Count - 1 downto 0 do
  begin
    // ������ �������� ������
    b := TToolButton.Create(aToolbar);

    b.Parent := aToolbar;
    b.Caption := aMenu.Items[i].Caption;
 //  b.Style:=tbsDropDown;
//    b.AutoPopup:=True;

    // ���� ��� �������, �� �������� Action
    if aMenu.Items[i].Count = 0 then
    begin
      b.Action := aMenu.Items[i].Action;
    end
    // ����� �������� ������� � �������� ��� ����������
    else begin
      b.DropdownMenu := TPopupMenu.Create(nil);
    //  b.DropdownMenu.AutoPopup:=True;

      CreateDropdownMenu(b.DropdownMenu.Items, aMenu.Items[i]);
    end;
  end;
end;


function dlg_GetComboBoxFocusedObjectData (Sender: TComboBox): integer;
begin
  with (Sender as TComboBox) do
    if ItemIndex<0 then Result:=0
                   else Result:=Integer(Items.Objects[ItemIndex]);
end;

//-------------------------------------------------------------------
procedure DisableMenu(aMenu: TMenu);
//-------------------------------------------------------------------
var
  i: Integer;
begin
  for I:= 0 to aMenu.Items.Count-1 do
    aMenu.Items[i].Enabled:= false;
end;

//-------------------------------------------------------------------
procedure EnableMenu  (aMenu: TMenu);
//-------------------------------------------------------------------
var
  i: Integer;
begin
  for I:= 0 to aMenu.Items.Count-1 do
    aMenu.Items[i].Enabled:= true;
end;


//----------------------------------------------------------------------
function ShowSaveDialog (AOwner: TComponent; aTitle,aDefaultExt,aFilter,aFileName:string): string;
//----------------------------------------------------------------------
var SaveDialog1: TSaveDialog;
begin
   SaveDialog1:=TSaveDialog.Create (AOwner);
   SaveDialog1.Title     :=aTitle;
   SaveDialog1.DefaultExt:=aDefaultExt;
   SaveDialog1.Filter    :=aFilter;
   SaveDialog1.FileName  :=aFileName;

   if SaveDialog1.Execute then Result:=SaveDialog1.FileName
                          else Result:='';
   SaveDialog1.Free;
end;



end.


(*
//-----------------------------------------------------------
function CreateMenuRadioItem111111111(aOwner: TPopupMenu; aCaption:string;
    aOnClickProc: TNotifyEvent): TMenuItem;
//-----------------------------------------------------------
var obj: TMenuItem;
begin
  obj:=TMenuItem.Create (aOwner);
  obj.Caption:=aCaption;
  obj.GroupIndex:=1;
  obj.RadioItem:=true;
  obj.OnClick:=aOnClickProc;
  Result:=obj;

  aOwner.Items.Add (obj);
  obj.Checked:=true;

end;

*)
