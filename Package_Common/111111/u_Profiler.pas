unit u_Profiler;

interface

uses
  Windows,SysUtils,Classes, 
  ShellAPI;

type
  TProfiler = class(TStringList)
  private
   FTime_dt: TDateTime;

   FTimeList: TStringList;

  public
    constructor Create;
    destructor Destroy; override;

    procedure Show;

    procedure Start(aLabel: string);
    procedure Stop(aLabel: string);
  end;

var
  g_Profiler: TProfiler;

implementation

constructor TProfiler.Create;
begin
  inherited Create;
  FTimeList := TStringList.Create();
end;

destructor TProfiler.Destroy;
begin
  FreeAndNil(FTimeList);
  inherited Destroy;
end;

// ---------------------------------------------------------------
procedure TProfiler.Show;
// ---------------------------------------------------------------
var
  sFileName: string;
begin
  sFileName :='c:\temp.txt';

  SaveToFile(sFileName);

  ShellExecute(0, 'open', 'notepad.exe', PChar(sFileName), nil, SW_SHOWNORMAL);

  // TODO -cMM: TProfiler.Show default body inserted
end;

// ---------------------------------------------------------------
procedure TProfiler.Start(aLabel: string);
// ---------------------------------------------------------------
var
  s: string;
begin
  Assert(aLabel<>'');

  FTime_dt  := Now;

  s:=TimeToStr(Now);

  FTimeList.Values[aLabel]:=s;

//  s := FormatDateTime ('hh:mm:ss:zzz', Now);

  Add(aLabel);

//  Add(aLabel + '-' + s);
//  Add(aLabel + '-' + FormatDateTime ('hh:mm:ss:zzz', Now));

end;

// ---------------------------------------------------------------
procedure TProfiler.Stop(aLabel: string);
// ---------------------------------------------------------------
var
  dt: TDateTime;
  s: string;
begin
  Assert(aLabel<>'');

  s:=FTimeList.Values[aLabel];


  dt:= Now - FTime_dt;
  s := FormatDateTime ('hh:mm:ss:zzz', dt);

//  Add(aLabel + '-' + FormatDateTime ('hh:mm:ss:zzz', Now));
  Add(aLabel + '::::::' + s);
end;


// ---------------------------------------------------------------
initialization
  g_Profiler:=TProfiler.Create;
finalization
(*
  if g_Profiler.Count>0 then
    g_Profiler.Show;
*)

  FreeAndNil(g_Profiler);

end.



