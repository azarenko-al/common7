{
  LkJSON v1.07

  06 november 2009

* Copyright (c) 2006,2007,2008,2009 Leonid Koninin
* leon_kon@users.sourceforge.net
* All rights reserved.
*
}

unit u_JSON;

{$IFDEF fpc}
  {$MODE objfpc}
  {$H+}
  {.$DEFINE HAVE_FORMATSETTING}
{$ELSE}
  {$IF RTLVersion > 14.00}
    {$DEFINE HAVE_FORMATSETTING}
    {$IF RTLVersion > 19.00}
      {$DEFINE USE_D2009}
    {$IFEND}
  {$IFEND}
{$ENDIF}

interface

{.$DEFINE USE_D2009}
{.$DEFINE KOL}
{.$define DOTNET}
{$DEFINE THREADSAFE}
{$DEFINE NEW_STYLE_GENERATE}
{.$DEFINE USE_HASH}
{.$DEFINE TCB_EXT}

uses windows,
  SysUtils, StrUtils,
  classes,
  variants;

type
  TJSONtypes = (jsBase, jsNumber, jsString, jsBoolean, jsNull,
    jsList, jsObject
    );


  TjsonAttrib = record
    Name: string;
    Value: Variant;
  end;



  TJSONobject = class;


  TJSONbase = class
  protected
    function GetValue: variant; virtual;
    procedure SetValue(const AValue: variant); virtual;
    function GetChild(idx: Integer): TJSONbase; virtual;
    procedure SetChild(idx: Integer; const AValue: TJSONbase);
      virtual;
    function GetCount: Integer; virtual;
    function GetField(AName: Variant):TJSONbase; virtual;
  public
    property Field[AName: Variant]: TJSONbase read GetField;
    property Count: Integer read GetCount;
    property Child[idx: Integer]: TJSONbase read GetChild write SetChild;
    property Value: variant read GetValue write SetValue;
    class function SelfType: TJSONtypes; virtual;
    class function SelfTypeName: string; virtual;
  end;

  TJSONnumber = class(TJSONbase)
  protected
    FValue: extended;
    function GetValue: Variant; override;
    procedure SetValue(const AValue: Variant); override;
  public
    procedure AfterConstruction; override;
    class function Generate(AValue: extended = 0): TJSONnumber;
    class function SelfType: TJSONtypes; override;
    class function SelfTypeName: string; override;
  end;

  TJSONstring = class(TJSONbase)
  protected
    FValue: WideString;
    function GetValue: Variant; override;
    procedure SetValue(const AValue: Variant); override;
  public
    procedure AfterConstruction; override;
    class function Generate(const wsValue: WideString = ''):
      TJSONstring;
    class function SelfType: TJSONtypes; override;
    class function SelfTypeName: string; override;
  end;

  TJSONboolean = class(TJSONbase)
  protected
    FValue: Boolean;
    function GetValue: Variant; override;
    procedure SetValue(const AValue: Variant); override;
  public
    procedure AfterConstruction; override;
    class function Generate(AValue: Boolean = true): TJSONboolean;
    class function SelfType: TJSONtypes; override;
    class function SelfTypeName: string; override;
  end;

  TlkJSONnull = class(TJSONbase)
  protected
    function GetValue: Variant; override;
    function Generate: TlkJSONnull;
  public
    class function SelfType: TJSONtypes; override;
    class function SelfTypeName: string; override;
  end;

  TlkJSONFuncEnum = procedure(ElName: string; Elem: TJSONbase;
    data: pointer; var Continue: Boolean) of object;

  TJSONcustomlist = class(TJSONbase)
  protected
//    FValue: array of TJSONbase;
    fList: TList;
    function GetCount: Integer; override;
    function GetChild(idx: Integer): TJSONbase; override;
    procedure SetChild(idx: Integer; const AValue: TJSONbase);
      override;
    function ForEachElement(idx: Integer; var nm: string):
      TJSONbase; virtual;

    function GetField(AName: Variant):TJSONbase; override;

    function _Add(obj: TJSONbase): Integer; virtual;
    function _IndexOf(obj: TJSONbase): Integer; virtual;
  public
    procedure ForEach(fnCallBack: TlkJSONFuncEnum; pUserData:
      pointer);
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;

    function getInt(idx: Integer): Integer; virtual;
    function getString(idx: Integer): string; virtual;
    function getWideString(idx: Integer): WideString; virtual;
    function getDouble(idx: Integer): Double; virtual;
    function getBoolean(idx: Integer): Boolean; virtual;
  end;

  TJSONlist = class(TJSONcustomlist)
  protected
  public
    function Add(obj: TJSONbase): Integer; overload;
    function AddObject: TJSONobject;

    function AddObjectEx(aArr: array of TjsonAttrib): TJSONobject;

//function AddList(const aName: WideString): TJSONlist;


    function Add(aboolean: Boolean): Integer; overload;
    function Add(nmb: double): Integer; overload;
    function Add(s: string): Integer; overload;
    function Add(const ws: WideString): Integer; overload;
    function Add(inmb: Integer): Integer; overload;

    function IndexOf(obj: TJSONbase): Integer;
    
    class function Generate: TJSONlist;
    class function SelfType: TJSONtypes; override;
    class function SelfTypeName: string; override;
  end;

  TlkJSONobjectmethod = class(TJSONbase)
  protected
    FValue: TJSONbase;
    FName: WideString;
    procedure SetName(const AValue: WideString);
  public
    property ObjValue: TJSONbase read FValue;
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
    property Name: WideString read FName write SetName;
    class function Generate(const aname: WideString; aobj: TJSONbase):
      TlkJSONobjectmethod;
  end;


  TJSONobject = class(TJSONcustomlist)
  protected
    function GetFieldByIndex(idx: Integer): TJSONbase;
    function GetNameOf(idx: Integer): WideString;

    procedure SetFieldByIndex(idx: Integer; const AValue: TJSONbase);


    function ForEachElement(idx: Integer; var nm: string): TJSONbase;
      override;
    function GetField(AName: Variant):TJSONbase; override;
  public

    function Add(const aname: WideString; aobj: TJSONbase): Integer; overload;

    function OldGetField(nm: WideString): TJSONbase;
    procedure OldSetField(nm: WideString; const AValue: TJSONbase);

    function Add(const aname: WideString; aboolean: Boolean): Integer; overload;
    function Add(const aname: WideString; nmb: double): Integer; overload;
    function Add(const aname: WideString; s: string): Integer; overload;
    function Add(const aname: WideString; const ws: WideString): Integer;
      overload;

    function Add(const aname: WideString; inmb: Integer): Integer; overload;


    function AddList(const aName: WideString): TJSONlist;
    function AddObject(const aName: WideString): TJSONobject;

// TODO: Delete
// //   l:=TJSONlist.Create;
////  oMainJS.Add('link',l);
////  oMainJS.Add('items',l);
//
//
//  procedure Delete(idx: Integer);

    function IndexOfName(const aname: WideString): Integer;
//    function IndexOfObject(aobj: TJSONbase): Integer;

//    property Field[nm: WideString]: TJSONbase read OldGetField
//      write OldSetField; default;

    constructor Create;
    destructor Destroy; override;

    class function Generate(AUseHash: Boolean = true): TJSONobject;
    class function SelfType: TJSONtypes; override;
    class function SelfTypeName: string; override;

    property FieldByIndex[idx: Integer]: TJSONbase read GetFieldByIndex
    write SetFieldByIndex;
    property NameOf[idx: Integer]: WideString read GetNameOf;

    function getDouble(idx: Integer): Double; overload; override;
    function getInt(idx: Integer): Integer; overload; override;
    function getString(idx: Integer): string; overload; override;
    function getWideString(idx: Integer): WideString; overload; override;
    function getBoolean(idx: Integer): Boolean; overload; override;

    function {$ifdef TCB_EXT}getDoubleFromName{$else}getDouble{$endif}
      (nm: string): Double; overload;
    function {$ifdef TCB_EXT}getIntFromName{$else}getInt{$endif}
      (nm: string): Integer; overload;
    function {$ifdef TCB_EXT}getStringFromName{$else}getString{$endif}
      (nm: string): string; overload;
    function {$ifdef TCB_EXT}getWideStringFromName{$else}getWideString{$endif}
      (nm: string): WideString; overload;
    function {$ifdef TCB_EXT}getBooleanFromName{$else}getBoolean{$endif}
      (nm: string): Boolean; overload;
  end;

  TJSON = class
  public
    class function ParseText(const txt: string): TJSONbase;
    class function GenerateText(obj: TJSONbase): string;
  end;

////////{$IFNDEF KOL}
(*
  TlkJSONstreamed___ = class(TJSON)
    class function LoadFromStream(src: TStream): TJSONbase;
    class procedure SaveToStream(obj: TJSONbase; dst: TStream);
    class function LoadFromFile(srcname: string): TJSONbase;
    class procedure SaveToFile(obj: TJSONbase; dstname: string);
  end;
  *)

////////{$ENDIF}

function GenerateReadableText(vObj: TJSONbase; var vLevel:
  Integer): string;

// Alex
function json_Att(aName: string; aValue: Variant): TjsonAttrib;

implementation

uses math;

//type
//  ElkIntException = class(Exception)
//  public
//    idx: Integer;
//    constructor Create(idx: Integer; msg: string);
//  end;


function JSON_EscapeValue(const AValue: string): string; forward;

// author of next two functions is Kusnassriyanto Saiful Bahri

function Indent(vTab: Integer): string;
begin
  result := DupeString('  ', vTab);
end;


function GenerateReadableText(vObj: TJSONbase; var vLevel:
  Integer): string;
var
  i: Integer;
  vStr: string;
  xs: TJSONstring;
begin
  vLevel := vLevel + 1;
  if vObj is TJSONobject then
    begin
      vStr := '';
      for i := 0 to TJSONobject(vObj).Count - 1 do
        begin
          if vStr <> '' then
            begin
              vStr := vStr + ','#13#10;
            end;
          vStr := vStr + Indent(vLevel) +
            GenerateReadableText(TJSONobject(vObj).Child[i], vLevel);
        end;
      if vStr <> '' then
        begin
          vStr := '{'#13#10 + vStr + #13#10 + Indent(vLevel - 1) + '}';
        end
      else
        begin
          vStr := '{}';
        end;
      result := vStr;
    end
  else if vObj is TJSONlist then
    begin
      vStr := '';
      for i := 0 to TJSONlist(vObj).Count - 1 do
        begin
          if vStr <> '' then
            begin
              vStr := vStr + ','#13#10;
            end;
          vStr := vStr + Indent(vLevel) +
              GenerateReadableText(TJSONlist(vObj).Child[i], vLevel);
        end;
      if vStr <> '' then
        begin
          vStr := '['#13#10 + vStr + #13#10 + Indent(vLevel - 1) + ']';
        end
      else
        begin
          vStr := '[]';
        end;
      result := vStr;
    end
  else
//
//    if vObj is TlkJSONobjectmethod then
//    begin
//      vStr := '';
//      xs := TJSONstring.Create;
//      try
//        xs.Value := TlkJSONobjectMethod(vObj).Name;
//        vStr := GenerateReadableText(xs, vLevel);
//        vLevel := vLevel - 1;
//        vStr := vStr + ':' + GenerateReadableText(TJSONbase(
//          TlkJSONobjectmethod(vObj).ObjValue), vLevel);
//      //vStr := vStr + ':' + GenerateReadableText(TJSONbase(vObj), vLevel);
//        vLevel := vLevel + 1;
//        result := vStr;
//      finally
//        xs.Free;
//      end;
//    end


 // else
    begin
//      if vObj is TlkJSONobjectmethod then
//        begin
//          if TlkJSONobjectMethod(vObj).Name <> '' then
//            begin
//            end;
//        end;

      result := TJSON.GenerateText(vObj);
    end;
  vLevel := vLevel - 1;
end;

// author of this routine is IVO GELOV

function code2utf(iNumber: Integer): UTF8String;
begin
  if iNumber < 128 then Result := chr(iNumber)
  else if iNumber < 2048 then
    Result := chr((iNumber shr 6) + 192) + chr((iNumber and 63) + 128)
  else if iNumber < 65536 then
    Result := chr((iNumber shr 12) + 224) + chr(((iNumber shr 6) and
      63) + 128) + chr((iNumber and 63) + 128)
  else if iNumber < 2097152 then
    Result := chr((iNumber shr 18) + 240) + chr(((iNumber shr 12) and
      63) + 128) + chr(((iNumber shr 6) and 63) + 128) +
      chr((iNumber and 63) + 128);
end;

{ TJSONbase }

function TJSONbase.GetChild(idx: Integer): TJSONbase;
begin
  result := nil;
end;

function TJSONbase.GetCount: Integer;
begin
  result := 0;
end;

function TJSONbase.GetField(AName: Variant):TJSONbase;
begin
  result := self;
end;

function TJSONbase.GetValue: variant;
begin
  result := variants.Null;
end;

class function TJSONbase.SelfType: TJSONtypes;
begin
  result := jsBase;
end;

class function TJSONbase.SelfTypeName: string;
begin
  result := 'jsBase';
end;

procedure TJSONbase.SetChild(idx: Integer; const AValue:
  TJSONbase);
begin

end;

procedure TJSONbase.SetValue(const AValue: variant);
begin

end;

{ TJSONnumber }

procedure TJSONnumber.AfterConstruction;
begin
  inherited;
  FValue := 0;
end;

class function TJSONnumber.Generate(AValue: extended):
  TJSONnumber;
begin
  result := TJSONnumber.Create;
  result.FValue := AValue;
end;

function TJSONnumber.GetValue: Variant;
begin
  result := FValue;
end;

class function TJSONnumber.SelfType: TJSONtypes;
begin
  result := jsNumber;
end;

class function TJSONnumber.SelfTypeName: string;
begin
  result := 'jsNumber';
end;

procedure TJSONnumber.SetValue(const AValue: Variant);
begin
  FValue := VarAsType(AValue, varDouble);
end;

{ TJSONstring }

procedure TJSONstring.AfterConstruction;
begin
  inherited;
  FValue := '';
end;

class function TJSONstring.Generate(const wsValue: WideString):
  TJSONstring;
begin
  result := TJSONstring.Create;
  result.FValue := wsValue;
end;

function TJSONstring.GetValue: Variant;
begin
  result := FValue;
end;

class function TJSONstring.SelfType: TJSONtypes;
begin
  result := jsString;
end;

class function TJSONstring.SelfTypeName: string;
begin
  result := 'jsString';
end;

procedure TJSONstring.SetValue(const AValue: Variant);
begin
  FValue := VarToWideStr(AValue);
end;

{ TJSONboolean }

procedure TJSONboolean.AfterConstruction;
begin
  FValue := false;
end;

class function TJSONboolean.Generate(AValue: Boolean):
  TJSONboolean;
begin
  result := TJSONboolean.Create;
  result.Value := AValue;
end;

function TJSONboolean.GetValue: Variant;
begin
  result := FValue;
end;

class function TJSONboolean.SelfType: TJSONtypes;
begin
  Result := jsBoolean;
end;

class function TJSONboolean.SelfTypeName: string;
begin
  Result := 'jsBoolean';
end;

procedure TJSONboolean.SetValue(const AValue: Variant);
begin
  FValue := boolean(AValue);
end;

{ TlkJSONnull }

function TlkJSONnull.Generate: TlkJSONnull;
begin
  result := TlkJSONnull.Create;
end;

function TlkJSONnull.GetValue: Variant;
begin
  result := variants.Null;
end;

class function TlkJSONnull.SelfType: TJSONtypes;
begin
  result := jsNull;
end;

class function TlkJSONnull.SelfTypeName: string;
begin
  result := 'jsNull';
end;

{ TJSONcustomlist }

function TJSONcustomlist._Add(obj: TJSONbase): Integer;
begin
  if not Assigned(obj) then
    begin
      result := -1;
      exit;
    end;
  result := fList.Add(obj);
end;

procedure TJSONcustomlist.AfterConstruction;
begin
  inherited;
  fList := TList.Create;
end;

procedure TJSONcustomlist.BeforeDestruction;
var
  i: Integer;
begin
//  for i := (Count - 1) downto 0 do _Delete(i);
  fList.Free;
  inherited;
end;

function TJSONcustomlist.GetChild(idx: Integer): TJSONbase;
begin
  if (idx < 0) or (idx >= Count) then
    begin
      result := nil;
    end
  else
    begin
      result := fList.Items[idx];
    end;
end;

function TJSONcustomlist.GetCount: Integer;
begin
  result := fList.Count;
end;

function TJSONcustomlist._IndexOf(obj: TJSONbase): Integer;
begin
  result := fList.IndexOf(obj);
end;

procedure TJSONcustomlist.SetChild(idx: Integer; const AValue:
  TJSONbase);
begin
  if not ((idx < 0) or (idx >= Count)) then
    begin
      if fList.Items[idx] <> nil then
        TJSONbase(fList.Items[idx]).Free;
      fList.Items[idx] := AValue;
    end;
end;

procedure TJSONcustomlist.ForEach(fnCallBack: TlkJSONFuncEnum;
  pUserData:
  pointer);
var
  iCount: Integer;
  IsContinue: Boolean;
  anJSON: TJSONbase;
  wsObject: string;
begin
  if not assigned(fnCallBack) then exit;
  IsContinue := true;
  for iCount := 0 to GetCount - 1 do
    begin
      anJSON := ForEachElement(iCount, wsObject);
      if assigned(anJSON) then
        fnCallBack(wsObject, anJSON, pUserData, IsContinue);
      if not IsContinue then break;
    end;
end;

///---- renamed to here

function TJSONcustomlist.GetField(AName: Variant):TJSONbase;
var
  index: Integer;
begin
  if VarIsNumeric(AName) then
    begin
      index := integer(AName);
      result := GetChild(index);
    end
  else
    begin
      result := inherited GetField(AName);
    end;
end;

function TJSONcustomlist.ForEachElement(idx: Integer; var nm:
  string): TJSONbase;
begin
  nm := inttostr(idx);
  result := GetChild(idx);
end;

function TJSONcustomlist.getDouble(idx: Integer): Double;
var
  jn: TJSONnumber;
begin
  jn := Child[idx] as TJSONnumber;
  if not assigned(jn) then result := 0
  else result := jn.Value;
end;

function TJSONcustomlist.getInt(idx: Integer): Integer;
var
  jn: TJSONnumber;
begin
  jn := Child[idx] as TJSONnumber;
  if not assigned(jn) then result := 0
  else result := round(int(jn.Value));
end;

function TJSONcustomlist.getString(idx: Integer): string;
var
  js: TJSONstring;
begin
  js := Child[idx] as TJSONstring;
  if not assigned(js) then result := ''
  else result := VarToStr(js.Value);
end;

function TJSONcustomlist.getWideString(idx: Integer): WideString;
var
  js: TJSONstring;
begin
  js := Child[idx] as TJSONstring;
  if not assigned(js) then result := ''
  else result := VarToWideStr(js.Value);
end;

function TJSONcustomlist.getBoolean(idx: Integer): Boolean;
var
  jb: TJSONboolean;
begin
  jb := Child[idx] as TJSONboolean;
  if not assigned(jb) then result := false
  else result := jb.Value;
end;

{ TlkJSONobjectmethod }
//
//procedure TlkJSONobjectmethod.AfterConstruction;
//begin
//  inherited;
//  FValue := nil;
//  FName := '';
//end;
//
//procedure TlkJSONobjectmethod.BeforeDestruction;
//begin
//  FName := '';
//  if FValue <> nil then
//    begin
//      FValue.Free;
//      FValue := nil;
//    end;
//  inherited;
//end;
//
//class function TlkJSONobjectmethod.Generate(const aname: WideString;
//  aobj: TJSONbase): TlkJSONobjectmethod;
//begin
//  result := TlkJSONobjectmethod.Create;
//  result.FName := aname;
//  result.FValue := aobj;
//end;
//
//procedure TlkJSONobjectmethod.SetName(const AValue: WideString);
//begin
//  FName := AValue;
//end;

{ TJSONlist }

function TJSONlist.Add(obj: TJSONbase): Integer;
begin
  result := _Add(obj);
end;

function TJSONlist.Add(nmb: double): Integer;
begin
  Result := self.Add(TJSONnumber.Generate(nmb));
end;

function TJSONlist.Add(aboolean: Boolean): Integer;
begin
  Result := self.Add(TJSONboolean.Generate(aboolean));
end;

function TJSONlist.Add(inmb: Integer): Integer;
begin
  Result := self.Add(TJSONnumber.Generate(inmb));
end;

function TJSONlist.Add(const ws: WideString): Integer;
begin
  Result := self.Add(TJSONstring.Generate(ws));
end;

function TJSONlist.Add(s: string): Integer;
begin
  Result := self.Add(TJSONstring.Generate(s));
end;

function TJSONlist.AddObject: TJSONobject;
begin
  Result := TJSONobject.create;
  Add(Result);
end;


function TJSONlist.AddObjectEx(aArr: array of TjsonAttrib): TJSONobject;
var
  I: Integer;
begin
(*  Result := TJSONobject.create;

  for I := 0 to High(aArr) do
    Result.Add(aArr[i].Name, aArr[i].Value);

  Add(Result);
*)
end;

class function TJSONlist.Generate: TJSONlist;
begin
  result := TJSONlist.Create;
end;

function TJSONlist.IndexOf(obj: TJSONbase): Integer;
begin
  result := _IndexOf(obj);
end;

class function TJSONlist.SelfType: TJSONtypes;
begin
  result := jsList;
end;

class function TJSONlist.SelfTypeName: string;
begin
  result := 'jsList';
end;

{ TJSONobject }

function TJSONobject.Add(const aname: WideString; aobj: TJSONbase): Integer;
var
  mth: TlkJSONobjectmethod;
begin
  if not assigned(aobj) then
    begin
      result := -1;
      exit;
    end;


  mth := TlkJSONobjectmethod.Create;
  mth.FName := aname;
  mth.FValue := aobj;
  result := self._Add(mth);

 // if FUseHash then
{$IFDEF USE_HASH}
//    ht.AddPair(aname, result);
{$ELSE}
 //   ht.Insert(aname, result);
{$ENDIF USE_HASH}
end;


class function TJSONobject.Generate(AUseHash: Boolean = true):
  TJSONobject;
begin
  result := TJSONobject.Create();
end;

function TJSONobject.OldGetField(nm: WideString): TJSONbase;
var
//  mth: TlkJSONobjectmethod;
  i: Integer;
begin
  i := IndexOfName(nm);
  if i = -1 then
    begin
      result := nil;
    end
  else
    begin
//      mth := TlkJSONobjectmethod(FValue[i]);
      mth := TlkJSONobjectmethod(fList.Items[i]);
      result := mth.FValue;
    end;
end;


function TJSONobject.IndexOfName(const aname: WideString): Integer;
var
  mth: TlkJSONobjectmethod;
  i: Integer;
begin
 // if not FUseHash then
    begin
      result := -1;
      for i := 0 to Count - 1 do
        begin
//          mth := TlkJSONobjectmethod(FValue[i]);
          mth := TlkJSONobjectmethod(fList.Items[i]);
          if mth.Name = aname then
            begin
              result := i;
              break;
            end;
        end;
    end
 // else
   // begin
   //   result := ht.IndexOf(aname);
   // end;
end;


//
//function TJSONobject.IndexOfObject(aobj: TJSONbase): Integer;
//var
//  mth: TlkJSONobjectmethod;
//  i: Integer;
//begin
//  result := -1;
//  for i := 0 to Count - 1 do
//    begin
////      mth := TlkJSONobjectmethod(FValue[i]);
//      mth := TlkJSONobjectmethod(fList.Items[i]);
//      if mth.FValue = aobj then
//        begin
//          result := i;
//          break;
//        end;
//    end;
//end;
//
//procedure TJSONobject.OldSetField(nm: WideString; const AValue:
//  TJSONbase);
//var
//  mth: TlkJSONobjectmethod;
//  i: Integer;
//begin
//  i := IndexOfName(nm);
//  if i <> -1 then
//    begin
////      mth := TlkJSONobjectmethod(FValue[i]);
//      mth := TlkJSONobjectmethod(fList.Items[i]);
//      mth.FValue := AValue;
//    end;
//end;

function TJSONobject.Add(const aname: WideString; nmb: double):
  Integer;
begin
  Result := self.Add(aname, TJSONnumber.Generate(nmb));
end;

function TJSONobject.Add(const aname: WideString; aboolean: Boolean):
  Integer;
begin
  Result := self.Add(aname, TJSONboolean.Generate(aboolean));
end;

function TJSONobject.Add(const aname: WideString; s: string):
  Integer;
begin
  Result := self.Add(aname, TJSONstring.Generate(s));
end;

function TJSONobject.Add(const aname: WideString; inmb: Integer):
  Integer;
begin
  Result := self.Add(aname, TJSONnumber.Generate(inmb));
end;

function TJSONobject.Add(const aname, ws: WideString): Integer;
begin
  Result := self.Add(aname, TJSONstring.Generate(ws));
end;

class function TJSONobject.SelfType: TJSONtypes;
begin
  Result := jsObject;
end;

class function TJSONobject.SelfTypeName: string;
begin
  Result := 'jsObject';
end;

function TJSONobject.GetFieldByIndex(idx: Integer): TJSONbase;
var
  nm: WideString;
begin
  nm := GetNameOf(idx);
  if nm <> '' then
    begin
      result := Field[nm];
    end
  else
    begin
      result := nil;
    end;
end;

function TJSONobject.GetNameOf(idx: Integer): WideString;
var
  mth: TlkJSONobjectmethod;
begin
  if (idx < 0) or (idx >= Count) then
    begin
      result := '';
    end
  else
    begin
      mth := Child[idx] as TlkJSONobjectmethod;
      result := mth.Name;
    end;
end;

{
procedure TJSONobject.SetFieldByIndex(idx: Integer;
  const AValue: TJSONbase);
var
  nm: WideString;
begin
  nm := GetNameOf(idx);
  if nm <> '' then
    begin
      Field[nm] := AValue;
    end;
end;

}

function TJSONobject.ForEachElement(idx: Integer;
  var nm: string): TJSONbase;
begin
  nm := GetNameOf(idx);
  result := GetFieldByIndex(idx);
end;

function TJSONobject.GetField(AName: Variant):TJSONbase;
begin
  if VarIsStr(AName) then
    result := OldGetField(VarToWideStr(AName))
  else
    result := inherited GetField(AName);
end;

//{$IFDEF USE_HASH}
//{$ELSE}
//function TJSONobject.GetHashTable: TlkBalTree;
//{$ENDIF USE_HASH}
//begin
// // result := ht;
//end;



constructor TJSONobject.Create;
begin
  inherited Create;
 // FUseHash := bUseHash;
{$IFDEF USE_HASH}
{$ELSE}
 // ht := TlkBalTree.Create;
{$ENDIF}
end;

destructor TJSONobject.Destroy;
begin
 // if assigned(ht) then FreeAndNil(ht);
  inherited;
end;

function TJSONobject.AddList(const aName: WideString): TJSONlist;
begin
  Result := TJSONlist.Create;
  Add(aName, Result);
end;

function TJSONobject.AddObject(const aName: WideString): TJSONobject;
begin
  Result := TJSONobject.create;
  Add(aName, Result);
end;

function TJSONobject.getDouble(idx: Integer): Double;
var
  jn: TJSONnumber;
begin
  jn := FieldByIndex[idx] as TJSONnumber;
  if not assigned(jn) then result := 0
  else result := jn.Value;
end;

function TJSONobject.getInt(idx: Integer): Integer;
var
  jn: TJSONnumber;
begin
  jn := FieldByIndex[idx] as TJSONnumber;
  if not assigned(jn) then result := 0
  else result := round(int(jn.Value));
end;

function TJSONobject.getString(idx: Integer): string;
var
  js: TJSONstring;
begin
  js := FieldByIndex[idx] as TJSONstring;
  if not assigned(js) then result := ''
  else result := vartostr(js.Value);
end;

function TJSONobject.getWideString(idx: Integer): WideString;
var
  js: TJSONstring;
begin
  js := FieldByIndex[idx] as TJSONstring;
  if not assigned(js) then result := ''
  else result := VarToWideStr(js.Value);
end;

{$ifdef TCB_EXT}
function TJSONobject.getDoubleFromName(nm: string): Double;
{$else}
function TJSONobject.getDouble(nm: string): Double;
{$endif}
begin
  result := getDouble(IndexOfName(nm));
end;

{$ifdef TCB_EXT}
function TJSONobject.getIntFromName(nm: string): Integer;
{$else}
function TJSONobject.getInt(nm: string): Integer;
{$endif}
begin
  result := getInt(IndexOfName(nm));
end;

{$ifdef TCB_EXT}
function TJSONobject.getStringFromName(nm: string): string;
{$else}
function TJSONobject.getString(nm: string): string;
{$endif}
begin
  result := getString(IndexOfName(nm));
end;

{$ifdef TCB_EXT}
function TJSONobject.getWideStringFromName(nm: string): WideString;
{$else}
function TJSONobject.getWideString(nm: string): WideString;
{$endif}
begin
  result := getWideString(IndexOfName(nm));
end;

function TJSONobject.getBoolean(idx: Integer): Boolean;
var
  jb: TJSONboolean;
begin
  jb := FieldByIndex[idx] as TJSONboolean;
  if not assigned(jb) then result := false
  else result := jb.Value;
end;

{$ifdef TCB_EXT}
function TJSONobject.getBooleanFromName(nm: string): Boolean;
{$else}
function TJSONobject.getBoolean(nm: string): Boolean;
{$endif}
begin
  result := getBoolean(IndexOfName(nm));
end;

{ TJSON }

class function TJSON.GenerateText(obj: TJSONbase): string;
var
{$IFDEF HAVE_FORMATSETTING}
  fs: TFormatSettings;
{$ENDIF}

  pt1, pt0, pt2: PChar;
  ptsz: cardinal;

{$IFNDEF NEW_STYLE_GENERATE}

  function gn_base(obj: TJSONbase): string;
  var
    ws: string;
    i, j: Integer;
    xs: TJSONstring;
  begin
    result := '';
    if not assigned(obj) then exit;
    if obj is TJSONnumber then
      begin
{$IFDEF HAVE_FORMATSETTING}
        result := FloatToStr(TJSONnumber(obj).FValue, fs);
{$ELSE}

        result := FloatToStr(TJSONnumber(obj).FValue);
        i := pos(DecimalSeparator, result);
        if (DecimalSeparator <> '.') and (i > 0) then
          result[i] := '.';

{$ENDIF}
      end
    else if obj is TJSONstring then
      begin
        ws := UTF8Encode(TJSONstring(obj).FValue);

        i := 1;
        result := '"';
        while i <= length(ws) do
          begin
            case ws[i] of
              '/', '\', '"': result := result + '\' + ws[i];
              #8: result := result + '\b';
              #9: result := result + '\t';
              #10: result := result + '\n';
              #13: result := result + '\r';
              #12: result := result + '\f';
            else
              if ord(ws[i]) < 32 then
                result := result + '\u' + inttohex(ord(ws[i]), 4)
              else
                result := result + ws[i];
            end;
            inc(i);
          end;
        result := result + '"';
      end
    else if obj is TJSONboolean then
      begin
        if TJSONboolean(obj).FValue then
          result := 'true'
        else
          result := 'false';
      end
    else if obj is TlkJSONnull then
      begin
        result := 'null';
      end
    else if obj is TJSONlist then
      begin
        result := '[';
        j := TJSONobject(obj).Count - 1;
        for i := 0 to j do
          begin
            if i > 0 then result := result + ',';
            result := result + gn_base(TJSONlist(obj).Child[i]);
          end;
        result := result + ']';
      end
    else if obj is TlkJSONobjectmethod then
      begin
        try
          xs := TJSONstring.Create;
          xs.FValue := TlkJSONobjectmethod(obj).FName;
          result := gn_base(TJSONbase(xs)) + ':';
          result := result +
            gn_base(TJSONbase(TlkJSONobjectmethod(obj).FValue));
        finally
          if assigned(xs) then FreeAndNil(xs);
        end;
      end
    else if obj is TJSONobject then
      begin
        result := '{';
        j := TJSONobject(obj).Count - 1;
        for i := 0 to j do
          begin
            if i > 0 then result := result + ',';
            result := result + gn_base(TJSONobject(obj).Child[i]);
          end;
        result := result + '}';
      end;
  end;
{$ELSE}

  procedure get_more_memory;
  var
    delta: cardinal;
  begin
    delta := 50000;
    if pt0 = nil then
      begin
        pt0 := AllocMem(delta);
        ptsz := 0;
        pt1 := pt0;
      end
    else
      begin
        ReallocMem(pt0, ptsz + delta);
        pt1 := pointer(cardinal(pt0) + ptsz);
      end;
    ptsz := ptsz + delta;
    pt2 := pointer(cardinal(pt1) + delta);
  end;

  procedure mem_ch(ch: char);
  begin
    if pt1 >= pt2 then
      get_more_memory;
      
    pt1^ := ch;
    inc(pt1);
  end;

  procedure mem_write(rs: string);
  var
    i: Integer;
  begin
    for i := 1 to length(rs) do
      begin
        if pt1 >= pt2 then
          get_more_memory;

        pt1^ := rs[i];
        inc(pt1);
      end;
  end;

  procedure gn_base(obj: TJSONbase);
  var
    ws: string;
    i, j: Integer;
    xs: TJSONstring;
  begin
    if not assigned(obj) then exit;
    if obj is TJSONnumber then
      begin
{$IFDEF HAVE_FORMATSETTING}
        mem_write(FloatToStr(TJSONnumber(obj).FValue, fs));
{$ELSE}

        ws := FloatToStr(TJSONnumber(obj).FValue);
        i := pos(DecimalSeparator, ws);
        if (DecimalSeparator <> '.') and (i > 0) then ws[i] := '.';
        mem_write(ws);

{$ENDIF}
      end
    else

      if obj is TJSONstring then
      begin
        ws := UTF8Encode(TJSONstring(obj).FValue);
      //  ws := (TJSONstring(obj).FValue);

//        ws:=JSON_EscapeValue(UTF8Encode(TJSONstring(obj).FValue));
        ws:=JSON_EscapeValue(TJSONstring(obj).FValue);


        i := 1;

        mem_ch('"');
        while i <= length(ws) do
          begin
            case ws[i] of
              '/', '\', '"':
                begin
                  mem_ch('\');
                  mem_ch(ws[i]);
                end;
              #8:  mem_write('\b');
              #9:  mem_write('\t');
              #10: mem_write('\n');
              #13: mem_write('\r');
              #12: mem_write('\f');
            else
              if ord(ws[i]) < 32 then
                mem_write('\u' + inttohex(ord(ws[i]), 4))
              else
                mem_ch(ws[i]);
            end;
            inc(i);
          end;

        mem_ch('"');
      end
    else

      if obj is TJSONboolean then
      begin
        if TJSONboolean(obj).FValue then
          mem_write('true')
        else
          mem_write('false');
      end

    else

      if obj is TlkJSONnull then
      begin
        mem_write('null');
      end

    else

      if obj is TJSONlist then
      begin
        mem_ch('[');
        j := TJSONobject(obj).Count - 1;
        for i := 0 to j do
          begin
            if i > 0 then mem_ch(',');
            gn_base(TJSONlist(obj).Child[i]);
          end;
        mem_ch(']');
      end

    else if obj is TlkJSONobjectmethod then
      begin
        try
          xs := TJSONstring.Create;
          xs.FValue := TlkJSONobjectmethod(obj).FName;
          gn_base(TJSONbase(xs));
          mem_ch(':');
          gn_base(TJSONbase(TlkJSONobjectmethod(obj).FValue));
        finally
          if assigned(xs) then FreeAndNil(xs);
        end;
      end
    else
    
      if obj is TJSONobject then
      begin
        mem_ch('{');
        j := TJSONobject(obj).Count - 1;
        for i := 0 to j do
          begin
            if i > 0 then mem_ch(',');
            gn_base(TJSONobject(obj).Child[i]);
          end;
        mem_ch('}');
      end;
  end;
{$ENDIF NEW_STYLE_GENERATE}

begin
{$IFDEF HAVE_FORMATSETTING}
  GetLocaleFormatSettings(GetThreadLocale, fs);
  fs.DecimalSeparator := '.';
{$ENDIF}
{$IFDEF NEW_STYLE_GENERATE}
  pt0 := nil;
  get_more_memory;
  gn_base(obj);
  mem_ch(#0);
  result := string(pt0);
  freemem(pt0);

/////
  Result:=StringReplace (Result, '\\', '\', [rfReplaceAll, rfIgnoreCase]);

{$ELSE}
  result := gn_base(obj);
{$ENDIF}
end;

class function TJSON.ParseText(const txt: string): TJSONbase;
{$IFDEF HAVE_FORMATSETTING}
var
  fs: TFormatSettings;
{$ENDIF}

  function js_base(idx: Integer; var ridx: Integer; var o:
    TJSONbase): Boolean; forward;

  function xe(idx: Integer): Boolean;
  {$IFDEF FPC}inline;
  {$ENDIF}
  begin
    result := idx <= length(txt);
  end;

  procedure skip_spc(var idx: Integer);
  {$IFDEF FPC}inline;
  {$ENDIF}
  begin
    while (xe(idx)) and (ord(txt[idx]) < 33) do
      inc(idx);
  end;

  procedure add_child(var o, c: TJSONbase);
  var
    i: Integer;
  begin
    if o = nil then
      begin
        o := c;
      end
    else
      begin
        if o is TlkJSONobjectmethod then
          begin
            TlkJSONobjectmethod(o).FValue := c;
          end
        else if o is TJSONlist then
          begin
            TJSONlist(o)._Add(c);
          end
        else if o is TJSONobject then
          begin
            i := TJSONobject(o)._Add(c);

         //   if TJSONobject(o).UseHash then
{$IFDEF USE_HASH}
//              TJSONobject(o).ht.AddPair(TlkJSONobjectmethod(c).Name, i);
{$ELSE}
         //     TJSONobject(o).ht.Insert(TlkJSONobjectmethod(c).Name, i);
{$ENDIF USE_HASH}
          end;
      end;
  end;

  function js_boolean(idx: Integer; var ridx: Integer; var o:
    TJSONbase): Boolean;
  var
    js: TJSONboolean;
  begin
    skip_spc(idx);
    if copy(txt, idx, 4) = 'true' then
      begin
        result := true;
        ridx := idx + 4;
        js := TJSONboolean.Create;
        js.FValue := true;
        add_child(o, TJSONbase(js));
      end
    else if copy(txt, idx, 5) = 'false' then
      begin
        result := true;
        ridx := idx + 5;
        js := TJSONboolean.Create;
        js.FValue := false;
        add_child(o, TJSONbase(js));
      end
    else
      begin
        result := false;
      end;
  end;

  function js_null(idx: Integer; var ridx: Integer; var o:
    TJSONbase): Boolean;
  var
    js: TlkJSONnull;
  begin
    skip_spc(idx);
    if copy(txt, idx, 4) = 'null' then
      begin
        result := true;
        ridx := idx + 4;
        js := TlkJSONnull.Create;
        add_child(o, TJSONbase(js));
      end
    else
      begin
        result := false;
      end;
  end;

  function js_integer(idx: Integer; var ridx: Integer): Boolean;
  begin
    result := false;
    while (xe(idx)) and (txt[idx] in ['0'..'9']) do
      begin
        result := true;
        inc(idx);
      end;
    if result then ridx := idx;
  end;

  function js_number(idx: Integer; var ridx: Integer; var o:
    TJSONbase): Boolean;
  var
    js: TJSONnumber;
    ws: string;
  {$IFNDEF HAVE_FORMATSETTING}
    i: Integer;
  {$ENDIF}
  begin
    skip_spc(idx);
    result := xe(idx);
    if not result then exit;
    if txt[idx] in ['+', '-'] then
      begin
        inc(idx);
        result := xe(idx);
      end;
    if not result then exit;
    result := js_integer(idx, idx);
    if not result then exit;
    if (xe(idx)) and (txt[idx] = '.') then
      begin
        inc(idx);
        result := js_integer(idx, idx);
        if not result then exit;
      end;
    if (xe(idx)) and (txt[idx] in ['e', 'E']) then
      begin
        inc(idx);
        if (xe(idx)) and (txt[idx] in ['+', '-']) then inc(idx);
        result := js_integer(idx, idx);
        if not result then exit;
      end;
    if not result then exit;
    js := TJSONnumber.Create;
    ws := copy(txt, ridx, idx - ridx);
{$IFDEF HAVE_FORMATSETTING}
    js.FValue := StrToFloat(ws, fs);
{$ELSE}
    i := pos('.', ws);
    if (DecimalSeparator <> '.') and (i > 0) then
      ws[pos('.', ws)] := DecimalSeparator;
    js.FValue := StrToFloat(ws);
{$ENDIF}
    add_child(o, TJSONbase(js));
    ridx := idx;
  end;

{

}
  function js_string(idx: Integer; var ridx: Integer; var o:
    TJSONbase): Boolean;

    function strSpecialChars(const s: string): string;
    var
      i, j : integer;
    begin
      i := Pos('\', s);
      if (i = 0) then
        Result := s
      else
      begin
        Result := Copy(s, 1, i-1);
        j := i;
        repeat
          if (s[j] = '\') then
          begin
            inc(j);
            case s[j] of
              '\': Result := Result + '\';
              '"': Result := Result + '"';
              '''': Result := Result + '''';
              '/': Result := Result + '/';
              'b': Result := Result + #8;
              'f': Result := Result + #12;
              'n': Result := Result + #10;
              'r': Result := Result + #13;
              't': Result := Result + #9;
              'u':
                begin
                  Result := Result + code2utf(strtoint('$' + copy(s, j + 1, 4)));
                  inc(j, 4);
                end;
            end;
          end
          else
            Result := Result + s[j];
          inc(j);
        until j > length(s);
      end;
    end;

  var
    js: TJSONstring;
    fin: Boolean;
    ws: String;
    i,j,widx: Integer;
  begin
    skip_spc(idx);

    result := xe(idx) and (txt[idx] = '"');
    if not result then exit;

    inc(idx);
    widx := idx;

    fin:=false;
    REPEAT
      i := 0;
      j := 0;
      while (widx<=length(txt)) and (j=0) do
        begin
          if (i=0) and (txt[widx]='\') then i:=widx;
          if (j=0) and (txt[widx]='"') then j:=widx;
          inc(widx);
        end;
// incorrect string!!!
      if j=0 then
        begin
          result := false;
          exit;
        end;
// if we have no slashed chars in string
      if (i=0) or (j<i) then
        begin
          ws := copy(txt,idx,j-idx);
          idx := j;
          fin := true;
        end
// if i>0 and j>=i - skip slashed char
      else
        begin
          widx:=i+2;
        end;
    UNTIL fin;

    ws := strSpecialChars(ws);
    inc(idx);

    js := TJSONstring.Create;
{$ifdef USE_D2009}
    js.FValue := UTF8ToString(ws);
{$else}
    js.FValue := UTF8Decode(ws);
{$endif}
    add_child(o, TJSONbase(js));
    ridx := idx;
  end;

  function js_list(idx: Integer; var ridx: Integer; var o:
    TJSONbase): Boolean;
  var
    js: TJSONlist;
  begin
    result := false;
    try
      js := TJSONlist.Create;
      skip_spc(idx);
      result := xe(idx);
      if not result then exit;
      result := txt[idx] = '[';
      if not result then exit;
      inc(idx);
      while js_base(idx, idx, TJSONbase(js)) do
        begin
          skip_spc(idx);
          if (xe(idx)) and (txt[idx] = ',') then inc(idx);
        end;
      skip_spc(idx);
      result := (xe(idx)) and (txt[idx] = ']');
      if not result then exit;
      inc(idx);
    finally
      if not result then
        begin
          js.Free;
        end
      else
        begin
          add_child(o, TJSONbase(js));
          ridx := idx;
        end;
    end;
  end;

  function js_method(idx: Integer; var ridx: Integer; var o:
    TJSONbase): Boolean;
  var
    mth: TlkJSONobjectmethod;
    ws: TJSONstring;
  begin
    result := false;
    try
      ws := nil;
      mth := TlkJSONobjectmethod.Create;
      skip_spc(idx);
      result := xe(idx);
      if not result then exit;
      result := js_string(idx, idx, TJSONbase(ws));
      if not result then exit;
      skip_spc(idx);
      result := xe(idx) and (txt[idx] = ':');
      if not result then exit;
      inc(idx);
      mth.FName := ws.FValue;
      result := js_base(idx, idx, TJSONbase(mth));
    finally
      if ws <> nil then ws.Free;
      if result then
        begin
          add_child(o, TJSONbase(mth));
          ridx := idx;
        end
      else
        begin
          mth.Free;
        end;
    end;
  end;

  function js_object(idx: Integer; var ridx: Integer; var o:
    TJSONbase): Boolean;
  var
    js: TJSONobject;
  begin
    result := false;
    try
      js := TJSONobject.Create;
      skip_spc(idx);
      result := xe(idx);
      if not result then exit;
      result := txt[idx] = '{';
      if not result then exit;
      inc(idx);
      while js_method(idx, idx, TJSONbase(js)) do
        begin
          skip_spc(idx);
          if (xe(idx)) and (txt[idx] = ',') then inc(idx);
        end;
      skip_spc(idx);  
      result := (xe(idx)) and (txt[idx] = '}');
      if not result then exit;
      inc(idx);
    finally
      if not result then
        begin
          js.Free;
        end
      else
        begin
          add_child(o, TJSONbase(js));
          ridx := idx;
        end;
    end;
  end;

  function js_base(idx: Integer; var ridx: Integer; var o:
    TJSONbase): Boolean;
  begin
    skip_spc(idx);
    result := js_boolean(idx, idx, o);
    if not result then result := js_null(idx, idx, o);
    if not result then result := js_number(idx, idx, o);
    if not result then result := js_string(idx, idx, o);
    if not result then result := js_list(idx, idx, o);
    if not result then result := js_object(idx, idx, o);
    if result then ridx := idx;
  end;

var
  idx: Integer;
begin
{$IFDEF HAVE_FORMATSETTING}
  GetLocaleFormatSettings(GetThreadLocale, fs);
  fs.DecimalSeparator := '.';
{$ENDIF}

  result := nil;
  if txt = '' then exit;
  try
    idx := 1;
    // skip a BOM utf8 marker
    if copy(txt,idx,3)=#239#187#191 then
      begin
        inc(idx,3);
    // if there are only a BOM - exit;
        if idx>length(txt) then exit;
      end;
    if not js_base(idx, idx, result) then FreeAndNil(result);
  except
    if assigned(result) then FreeAndNil(result);
  end;
end;




// ---------------------------------------------------------------
function JSON_EscapeValue(const AValue: string): string;
// ---------------------------------------------------------------

  procedure AddChars(const AChars: string; var Dest: string; var AIndex: Integer);
  begin
    System.Insert(AChars, Dest, AIndex);
    System.Delete(Dest, AIndex + 2, 1);
    Inc(AIndex, 2);
  end;

  procedure AddUnicodeChars(const AChars: string; var Dest: string; var AIndex: Integer);
  begin
    System.Insert(AChars, Dest, AIndex);
    System.Delete(Dest, AIndex + 6, 1);
    Inc(AIndex, 6);
  end;

var
  i, ix: Integer;
  AChar: Char;
begin
  Result := AValue;

  ix := 1;

  for i := 1 to System.Length(AValue) do
  begin
    AChar :=  AValue[i];

    case AChar of
      '/', '\', '"':
      begin
        System.Insert('\', Result, ix);
        Inc(ix, 2);
      end;

      #8:  AddChars('\b', Result, ix); //backspace \b
      #9:  AddChars('\t', Result, ix);
      #10: AddChars('\n', Result, ix);
      #12: AddChars('\f', Result, ix);
      #13: AddChars('\r', Result, ix);

      #0 .. #7, #11, #14 .. #31:
           AddUnicodeChars('\u' + IntToHex(Word(AChar), 4), Result, ix);

      else
      begin
        if Word(AChar) > 127 then
//          AddUnicodeChars('\u' + IntToHex(Word(AChar) + 1057-209 , 4), Result, ix)
          AddUnicodeChars('\u' + IntToHex(Word(AChar) + 1057-209 , 4), Result, ix)
        else
          Inc(ix);
      end;
    end;
  end;
end;


function json_Att(aName: string; aValue: Variant): TjsonAttrib;
begin
  Result.Name:=LowerCase(aName);
  Result.Value:=aValue;
end;




end.



{
var
  js: TJSONobject;
  ws: TJSONstring;
  s: String;
  i: Integer;

  js_1: TJSONobject;

begin
  js_1 := TJSONobject.Create;
  js_1.Add('aaaaaaaaaaaaa','hhhhhhhhhh');
  js_1.Add('a33333333aaaaaaaaaaaa','222hhhhhhhhhh');



  js := TJSONobject.Create;
//  js.add('namestring', TlkJSONstring.Generate('namevalue'));
  js.Add('namestring','namevalue');
  js. Add('namestring1','namevalue2');
  js.Add('namestring1',js_1);



// get the text of object
  s := TJSON.GenerateText(js);
  writeln(s);
  writeln;
  writeln('more readable variant:');
// (ver 1.03+) generate readable text
  i := 0;
  s := GenerateReadableText(js,i);
  writeln(s);

  js.Free;
// restore object (parse text)
  js := TJSON.ParseText(s) as TJSONobject;
// and get string back
// old syntax
  ws := js.Field['namestring'] as TJSONstring;
  s := ws.Value;
  writeln(s);
// syntax of 0.99+ buenos
  s := js.getString('namestring');
  writeln(s);

  readln;
  js.Free;
end.



program sample1;

{$APPTYPE CONSOLE}
  {
uses
  SysUtils,
  u_JSON in 'u_JSON.pas';

var
  js: TJSONobject;
  ws: TJSONstring;
  s: String;
  i: Integer;

  js_1: TJSONobject;
  js_2: TJSONobject;

  l: TJSONlist;

begin
  
  js_1 := TJSONobject.Create;
  js_1.Add('aaaaaaaaaaaaa','hhhhhhhhhh');
  js_1.Add('a33333333aaaaaaaaaaaa','222hhhhhhhhhh');


  l:=TJSONlist.Create;

  js_2 := TJSONobject.Create;
  js_2.Add('aaaaaaaaaaaaa','hhhhhhhhhh');
  js_2.Add('a33333333aaaaaaaaaaaa','222hhhhhhhhhh');

  l.Add(js_2);
 // l.Add(js_2);
 // l.Add(js_2);   

  js_1.Add('aaa',l);
  

  js := TJSONobject.Create;
//  js.add('namestring', TlkJSONstring.Generate('namevalue'));
  js.Add('namestring',  'namevalue');
  js.Add('namestring1', 'namevalue2');
  js.Add('namestring1', js_1);




