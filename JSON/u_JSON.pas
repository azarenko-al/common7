
unit u_JSON;

{$ifdef fpc}
  {$mode objfpc}{$H+}
  {.$DEFINE HAVE_FORMATSETTING}
{$else}
  {$if RTLVersion > 14.00}
    {$DEFINE HAVE_FORMATSETTING}
  {$ifend}
{$endif}

interface

{.$DEFINE KOL}
{.$define DOTNET}
{$define THREADSAFE}
{$define NEW_STYLE_GENERATE}

uses windows,
  SysUtils, 

{$IFNDEF KOL}
  classes,
{$ELSE}
//  kol,
{$ENDIF}
  variants;

const
  DEF_LF = #13+#10;
  

type
  TlkJSONtypes = (jsBase, jsNumber, jsString, jsBoolean, jsNull,
    jsList, jsObject);

{$IFDEF DOTNET}

{$ENDIF DOTNET}

  TJSONobject = class;



  TJSONbase = class
  protected
    function GetValue: variant; virtual;
    procedure SetValue(const AValue: variant); virtual;
    function GetChild(idx: Integer): TJSONbase; virtual;
    procedure SetChild(idx: Integer; const AValue: TJSONbase);
      virtual;
    function GetCount: Integer; virtual;
  public
    property Count: Integer read GetCount; 
    property Child[idx: Integer]: TJSONbase read GetChild write SetChild;
    property Value: variant read GetValue write SetValue;
    class function SelfType: TlkJSONtypes; virtual;
    class function SelfTypeName: string; virtual; 
  end; 

  TlkJSONnumber = class(TJSONbase) 
  protected
    FValue: extended; 
    function GetValue: Variant; override; 
    procedure SetValue(const AValue: Variant); override; 
  public 
    procedure AfterConstruction; override; 
    class function Generate(AValue: extended = 0): TlkJSONnumber; 
    class function SelfType: TlkJSONtypes; override; 
    class function SelfTypeName: string; override; 
  end; 
 
  TlkJSONstring = class(TJSONbase) 
  protected
    FValue: WideString; 
    function GetValue: Variant; override; 
    procedure SetValue(const AValue: Variant); override; 
  public
    procedure AfterConstruction; override;
    class function Generate(const wsValue: WideString = ''): TlkJSONstring; 
    class function SelfType: TlkJSONtypes; override;
    class function SelfTypeName: string; override; 
  end; 
 
  TJSONboolean = class(TJSONbase)
  protected
    FValue: Boolean; 
    function GetValue: Variant; override; 
    procedure SetValue(const AValue: Variant); override; 
  public 
    procedure AfterConstruction; override; 
    class function Generate(AValue: Boolean = true): TJSONboolean; 
    class function SelfType: TlkJSONtypes; override;
    class function SelfTypeName: string; override; 
  end; 
 
  TJSONnull = class(TJSONbase)
  protected
    function GetValue: Variant; override; 
    function Generate: TJSONnull; 
  public
    class function SelfType: TlkJSONtypes; override; 
    class function SelfTypeName: string; override; 
  end;

  TlkJSONFuncEnum = procedure(ElName: string; Elem: TJSONbase;
    data: pointer; var Continue: Boolean) of object;
 
  TlkJSONcustomlist = class(TJSONbase) 
  protected
//    FValue: array of TJSONbase; 
    fList:TList;
    function GetCount: Integer; override; 
    function GetChild(idx: Integer): TJSONbase; override; 
    procedure SetChild(idx: Integer; const AValue: TJSONbase); 
      override;
    function ForEachElement(idx: Integer; var nm: string): 
      TJSONbase; virtual; 

    function _Add(obj: TJSONbase): Integer; virtual; 
//    procedure _Delete(idx: Integer); virtual;
    function _IndexOf(obj: TJSONbase): Integer; virtual; 
  public
    procedure ForEach(cb: TlkJSONFuncEnum; data: pointer); 
    procedure AfterConstruction; override; 
    procedure BeforeDestruction; override; 
 
    function getInt(idx: Integer): Integer; virtual; 
    function getString(idx: Integer): String; virtual; 
    function getWideString(idx: Integer):WideString; virtual;
    function getDouble(idx: Integer): Double; virtual;
    function getBoolean(idx: Integer): Boolean; virtual; 
  end; 
 
  TJSONlist = class(TlkJSONcustomlist)
  private
  public
    function Add(obj: TJSONbase): Integer; overload;

    function Add(bool: Boolean): Integer; overload; 
    function Add(nmb: double): Integer; overload; 
    function Add(s: string): Integer; overload; 
    function Add(const ws: WideString): Integer; overload;
    function Add(inmb: Integer): Integer; overload;

    function AddList: TJSONlist;

    function AddObject: TJSONobject;
    function AddObjectEx(aValues: array of Variant): TJSONobject;

//    procedure Delete(idx: Integer); 
    function IndexOf(obj: TJSONbase): Integer;
    class function Generate: TJSONlist;

    class function SelfType: TlkJSONtypes; override;
    class function SelfTypeName: string; override; 

  end; 
 
  TlkJSONobjectmethod = class(TJSONbase)
  protected
    FValue: TJSONbase; 
    FName: WideString;
    procedure SetName(const AValue: WideString); 
  public 
    procedure AfterConstruction; override; 
    procedure BeforeDestruction; override;
    property Name: WideString read FName write SetName;
    class function Generate(const aname: WideString; aobj: TJSONbase):
      TlkJSONobjectmethod;
  end;


  //TlkHashFunction = function(const ws: WideString): cardinal of object;

  TJSONobject = class(TlkJSONcustomlist)
  private
  protected
  //  ht: TlkHashTable; 
  //  FUseHash: Boolean;
    function GetFieldByIndex(idx: Integer): TJSONbase;
    function GetNameOf(idx: Integer): WideString;
    procedure SetFieldByIndex(idx: Integer; const AValue:
      TJSONbase); 
    function ForEachElement(idx: Integer; var nm: string):
      TJSONbase;
      override;
  public 
  //  property UseHash: Boolean read FUseHash;
    function GetField(nm: string): TJSONbase;
    procedure SetField(nm: string; const AValue: TJSONbase); 

    procedure AddEx(aValues: array of Variant);



    function Add(const aname: WideString; aobj: TJSONbase): Integer;
      overload;

    function Add(const aname: WideString; bool: Boolean): Integer;
      overload;
    function Add(const aname: WideString; nmb: double): Integer; overload;
    function Add(const aname: WideString; s: string): Integer; overload;
    function Add(const aname: WideString; const ws: WideString): Integer;
      overload;
    function Add(const aname: WideString; inmb: Integer): Integer;
      overload;

   // procedure Delete(idx: Integer);

    function IndexOfName(const aname: WideString): Integer;
    function IndexOfObject(aobj: TJSONbase): Integer;
    property Field[nm: string]: TJSONbase read GetField write SetField; default;

    constructor Create;
    destructor Destroy; override;
 
    class function Generate: TJSONobject;
    class function SelfType: TlkJSONtypes; override;
    class function SelfTypeName: string; override;
 
    property FieldByIndex[idx: Integer]: TJSONbase read GetFieldByIndex write SetFieldByIndex; 
    property NameOf[idx: Integer]: WideString read GetNameOf;

    function AddList(const aName: WideString): TJSONlist;
    function AddObject(const aName: WideString): TJSONobject;

    function getDouble(idx: Integer): Double; overload; override;
    function getInt(idx: Integer): Integer; overload; override; 
    function getString(idx: Integer): String; overload; override;
    function getWideString(idx: Integer): WideString; overload; override; 
    function getBoolean(idx: Integer): Boolean; overload; override; 

    function getDouble(nm: String): Double; overload; 
    function getInt(nm: String): Integer; overload;
    function getString(nm: String): String; overload;
    function getWideString(nm: String): WideString; overload;
    function getBoolean(nm: String): Boolean; overload;
  end;

  TJSON = class
  private
   // FIsInsertLF : Boolean;
  public
    class function ParseText(const txt: string): TJSONbase;
    class function GenerateText(obj: TJSONbase; aIsInsertLF: Boolean = false):
        string;
  end;

//function JSON_encode(aValues: array of Variant): string;

function JSON_ConvertString(AValue: string): string;

function JSON_encode(aValues: array of Variant): string;

function JSON_encode_new(aValues: array of Variant): string;


{$IFNDEF KOL}
{$ENDIF} 

implementation
 
 
 
type 
  ElkIntException = class(Exception) 
  public
    idx: Integer; 
    constructor Create(idx: Integer; msg: string); 
  end; 
 
// author of this routine is IVO GELOV 
function code2utf(iNumber: Integer): UTF8String; 
begin 
  if iNumber < 128 then Result := chr(iNumber) 
  else if iNumber < 2048 then 
    Result := chr((iNumber shr 6) + 192) + chr((iNumber and 63) + 128) 
  else if iNumber < 65536 then 
    Result := chr((iNumber shr 12) + 224) + chr(((iNumber shr 6) and 63) + 128) 
    + chr((iNumber and 63) + 128) 
  else if iNumber < 2097152 then
    Result := chr((iNumber shr 18) + 240) + chr(((iNumber shr 12) and 63) + 128) 
    + chr(((iNumber shr 6) and 63) + 128) + chr((iNumber and 63) + 128); 
end;
 
{ TJSONbase }

function TJSONbase.GetChild(idx: Integer): TJSONbase; 
begin
  result := nil; 
end; 
 
function TJSONbase.GetCount: Integer;
begin
  result := 0; 
end; 
 
function TJSONbase.GetValue: variant; 
begin
  result := variants.Null; 
end; 
 
class function TJSONbase.SelfType: TlkJSONtypes; 
begin
  result := jsBase; 
end; 
 
class function TJSONbase.SelfTypeName: string; 
begin
  result := 'jsBase'; 
end; 

procedure TJSONbase.SetChild(idx: Integer; const AValue:
  TJSONbase); 
begin
 
end; 
 
procedure TJSONbase.SetValue(const AValue: variant);
begin
 
end; 
 
{ TlkJSONnumber } 
 
procedure TlkJSONnumber.AfterConstruction;
begin 
  inherited; 
  FValue := 0; 
end; 
 
class function TlkJSONnumber.Generate(AValue: extended): TlkJSONnumber;
begin 
  result := TlkJSONnumber.Create;
  result.FValue := AValue; 
end; 
 
function TlkJSONnumber.GetValue: Variant; 
begin
  result := FValue; 
end; 
 
class function TlkJSONnumber.SelfType: TlkJSONtypes; 
begin 
  result := jsNumber;
end; 
 
class function TlkJSONnumber.SelfTypeName: string; 
begin 
  result := 'jsNumber'; 
end; 
 
procedure TlkJSONnumber.SetValue(const AValue: Variant); 
begin 
  FValue := VarAsType(AValue, varDouble); 
end; 

{ TlkJSONstring } 
 
procedure TlkJSONstring.AfterConstruction; 
begin 
  inherited; 
  FValue := ''; 
end; 
 
class function TlkJSONstring.Generate(const wsValue: WideString): TlkJSONstring; 
begin 
  result := TlkJSONstring.Create; 
  result.FValue := wsValue; 
end; 

function TlkJSONstring.GetValue: Variant; 
begin 
  result := FValue; 
end; 
 
class function TlkJSONstring.SelfType: TlkJSONtypes; 
begin 
  result := jsString; 
end; 
 
class function TlkJSONstring.SelfTypeName: string;
begin 
  result := 'jsString'; 
end; 
 
procedure TlkJSONstring.SetValue(const AValue: Variant); 
begin 
  FValue := VarToWideStr(AValue); 
end; 
 
{ TJSONboolean } 

procedure TJSONboolean.AfterConstruction; 
begin
  FValue := false; 
end;
 
class function TJSONboolean.Generate(AValue: Boolean): TJSONboolean; 
begin
  result := TJSONboolean.Create; 
  result.Value := AValue;
end; 
 
function TJSONboolean.GetValue: Variant; 
begin
  result := FValue; 
end;
 
class function TJSONboolean.SelfType: TlkJSONtypes; 
begin
  Result := jsBoolean; 
end; 
 
class function TJSONboolean.SelfTypeName: string; 
begin
  Result := 'jsBoolean'; 
end; 
 
procedure TJSONboolean.SetValue(const AValue: Variant); 
begin
  FValue := boolean(AValue); 
end;
 
{ TJSONnull } 

function TJSONnull.Generate: TJSONnull; 
begin
  result := TJSONnull.Create; 
end;
 
function TJSONnull.GetValue: Variant; 
begin
  result := variants.Null;
end; 
 
class function TJSONnull.SelfType: TlkJSONtypes; 
begin
  result := jsNull; 
end; 
 
class function TJSONnull.SelfTypeName: string; 
begin
  result := 'jsNull'; 
end; 
 
{ TlkJSONcustomlist } 
 
function TlkJSONcustomlist._Add(obj: TJSONbase): Integer;
begin
  if not Assigned(obj) then 
    begin 
      result := -1; 
      exit; 
    end; 
  result := fList.Add(obj); 
end; 
 
procedure TlkJSONcustomlist.AfterConstruction; 
begin
  inherited; 
  fList := TList.Create; 
end; 
 
procedure TlkJSONcustomlist.BeforeDestruction; 
var 
  i: Integer; 
begin 
//  for i := (Count - 1) downto 0 do _Delete(i);
  fList.Free; 
  inherited; 
end; 

//
//procedure TlkJSONcustomlist._Delete(idx: Integer);
//begin
//  if not ((idx < 0) or (idx >= Count)) then
//    begin
//      if fList.Items[idx]<>nil then TJSONbase(fList.Items[idx]).Free;
//      fList.Delete(idx);
//    end;
//end;

function TlkJSONcustomlist.GetChild(idx: Integer): TJSONbase; 
begin
  if (idx < 0) or (idx >= Count) then
    begin 
      result := nil; 
    end 
  else 
    begin 
      result := fList.Items[idx]; 
    end; 
end; 
 
function TlkJSONcustomlist.GetCount: Integer; 
begin 
  result := fList.Count; 
end; 
 
function TlkJSONcustomlist._IndexOf(obj: TJSONbase): Integer; 
var
  i: Integer; 
begin 
  result := fList.IndexOf(obj); 
end; 
 
procedure TlkJSONcustomlist.SetChild(idx: Integer; const AValue: 
  TJSONbase); 
begin
  if not ((idx < 0) or (idx >= Count)) then 
    begin
      if fList.Items[idx]<>nil then TJSONbase(fList.Items[idx]).Free; 
      fList.Items[idx] := AValue;
    end; 
end; 
 
///---- renamed to here 
 
procedure TlkJSONcustomlist.ForEach(cb: TlkJSONFuncEnum; data: 
  pointer); 
var 
  i: Integer; 
  doCont: Boolean; 
  obj: TJSONbase; 
  ws: string;
begin 
  if not assigned(cb) then exit;
  doCont := true; 
  for i := 0 to GetCount - 1 do 
    begin 
      obj := ForEachElement(i, ws); 
      if assigned(obj) then cb(ws, obj, data, doCont); 
      if not doCont then break; 
    end; 
end; 
 
function TlkJSONcustomlist.ForEachElement(idx: Integer; var nm:
  string): TJSONbase; 
begin
  nm := inttostr(idx); 
  result := GetChild(idx); 
end; 
 
function TlkJSONcustomlist.getDouble(idx: Integer): Double; 
var 
  jn:TlkJSONnumber; 
begin 
  jn := Child[idx] as TlkJSONnumber; 
  if not assigned(jn) then result := 0 
  else result := jn.Value; 
end; 
 
function TlkJSONcustomlist.getInt(idx: Integer): Integer;
var 
  jn:TlkJSONnumber; 
begin 
  jn := Child[idx] as TlkJSONnumber; 
  if not assigned(jn) then result := 0 
  else result := round(int(jn.Value)); 
end; 
 
function TlkJSONcustomlist.getString(idx: Integer): String; 
var
  js:TlkJSONstring; 
begin 
  js := Child[idx] as TlkJSONstring; 
  if not assigned(js) then result := '' 
  else result := VarToStr(js.Value); 
end; 
 
function TlkJSONcustomlist.getWideString(idx: Integer): WideString; 
var 
  js:TlkJSONstring; 
begin 
  js := Child[idx] as TlkJSONstring; 
  if not assigned(js) then result := '' 
  else result := VarToWideStr(js.Value); 
end; 

function TlkJSONcustomlist.getBoolean(idx: Integer): Boolean; 
var 
  jb:TJSONboolean; 
begin
  jb := Child[idx] as TJSONboolean; 
  if not assigned(jb) then result := false
  else result := jb.Value; 
end; 
 
{ TlkJSONobjectmethod }
 
procedure TlkJSONobjectmethod.AfterConstruction; 
begin 
  inherited; 
  FValue := nil; 
  FName := ''; 
end; 
 
procedure TlkJSONobjectmethod.BeforeDestruction; 
begin 
  FName := ''; 
  if FValue <> nil then 
    begin 
      FValue.Free; 
      FValue := nil; 
    end;
  inherited; 
end; 
 
class function TlkJSONobjectmethod.Generate(const aname: WideString; 
  aobj: TJSONbase): TlkJSONobjectmethod; 
begin
  result := TlkJSONobjectmethod.Create; 
  result.FName := aname; 
  result.FValue := aobj; 
end;
 
procedure TlkJSONobjectmethod.SetName(const AValue: WideString); 
begin 
  FName := AValue; 
end; 
 
{ TJSONlist } 

function TJSONlist.Add(obj: TJSONbase): Integer; 
begin
  result := _Add(obj); 
end; 
 
function TJSONlist.Add(nmb: double): Integer; 
begin
  Result := self.Add(TlkJSONnumber.Generate(nmb));
end; 
 
function TJSONlist.Add(bool: Boolean): Integer; 
begin
  Result := self.Add(TJSONboolean.Generate(bool)); 
end;
 
function TJSONlist.Add(inmb: Integer): Integer; 
begin
  Result := self.Add(TlkJSONnumber.Generate(inmb));
end; 
 
function TJSONlist.Add(const ws: WideString): Integer; 
begin
  Result := self.Add(TlkJSONstring.Generate(ws)); 
end; 
 
function TJSONlist.Add(s: string): Integer; 
begin
  Result := self.Add(TlkJSONstring.Generate(s)); 
end; 
// 
//procedure TJSONlist.Delete(idx: Integer); 
//begin
//  _Delete(idx); 
//end;

class function TJSONlist.Generate: TJSONlist; 
begin
  result := TJSONlist.Create; 
end;
 
function TJSONlist.IndexOf(obj: TJSONbase): Integer; 
begin
  result := _IndexOf(obj); 
end;
 
class function TJSONlist.SelfType: TlkJSONtypes; 
begin
  result := jsList; 
end; 
 
class function TJSONlist.SelfTypeName: string; 
begin
  result := 'jsList'; 
end; 
 
{ TJSONobject } 

function TJSONobject.Add(const aname: WideString; aobj: TJSONbase): 
  Integer;
var
  mth: TlkJSONobjectmethod; 
begin 
  if not assigned(aobj) then 
    begin 
      result := -1; 
      exit; 
    end; 
  mth := TlkJSONobjectmethod.Create; 
  mth.FName := aname; 
  mth.FValue := aobj;
  result := self._Add(mth); 
 // if FUseHash then ht.AddPair(aname, result); 
end; 

//
//procedure TJSONobject.Delete(idx: Integer);
//var
//  mth: TlkJSONobjectmethod;
//begin
//  if (idx >= 0) and (idx < Count) then
//    begin
////      mth := FValue[idx] as TlkJSONobjectmethod;
//      mth := TlkJSONobjectmethod(fList.Items[idx]);
//    //  if FUseHash then ht.Delete(mth.FName);
//    end;
//  _Delete(idx);
//end;

class function TJSONobject.Generate: TJSONobject;
begin
  result := TJSONobject.Create();
end;
 
function TJSONobject.GetField(nm: string): TJSONbase; 
var
  mth: TlkJSONobjectmethod;
  i: Integer; 
begin 
  i := IndexOfName(nm); 
  if i = -1 then 
    begin 
      result := nil; 
    end 
  else 
    begin 
//      mth := TlkJSONobjectmethod(FValue[i]); 
      mth := TlkJSONobjectmethod(fList.Items[i]); 
      result := mth.FValue; 
    end; 
end; 
 
function TJSONobject.IndexOfName(const aname: WideString): Integer; 
var
  mth: TlkJSONobjectmethod; 
  i: Integer; 
begin 

      result := -1; 
      for i := 0 to Count - 1 do 
        begin 
//          mth := TlkJSONobjectmethod(FValue[i]); 
          mth := TlkJSONobjectmethod(fList.Items[i]);
          if mth.Name = aname then 
            begin 
              result := i; 
              break; 
            end; 
        end; 
     

end; 
 
function TJSONobject.IndexOfObject(aobj: TJSONbase): Integer; 
var
  mth: TlkJSONobjectmethod;
  i: Integer; 
begin 
  result := -1; 
  for i := 0 to Count - 1 do 
    begin 
//      mth := TlkJSONobjectmethod(FValue[i]); 
      mth := TlkJSONobjectmethod(fList.Items[i]); 
      if mth.FValue = aobj then 
        begin 
          result := i; 
          break; 
        end; 
    end;
end; 
 
procedure TJSONobject.SetField(nm: string; const AValue: 
  TJSONbase);
var
  mth: TlkJSONobjectmethod; 
  i: Integer; 
begin 
  i := IndexOfName(nm); 
  if i <> -1 then 
    begin 
//      mth := TlkJSONobjectmethod(FValue[i]); 
      mth := TlkJSONobjectmethod(fList.Items[i]);
      mth.FValue := AValue; 
    end; 
end; 
 
function TJSONobject.Add(const aname: WideString; nmb: double): Integer; 
begin
  Result := self.Add(aname, TlkJSONnumber.Generate(nmb)); 
end; 
 
function TJSONobject.Add(const aname: WideString; bool: Boolean): 
  Integer;
begin 
  Result := self.Add(aname, TJSONboolean.Generate(bool));
end;
 
function TJSONobject.Add(const aname: WideString; s: string): Integer; 
begin
  Result := self.Add(aname, TlkJSONstring.Generate(s)); 
end; 
 
function TJSONobject.Add(const aname: WideString; inmb: Integer): 
  Integer;
begin 
  Result := self.Add(aname, TlkJSONnumber.Generate(inmb)); 
end; 

function TJSONobject.Add(const aname, ws: WideString): Integer; 
begin
  Result := self.Add(aname, TlkJSONstring.Generate(ws)); 
end; 
 
class function TJSONobject.SelfType: TlkJSONtypes; 
begin
  Result := jsObject; 
end; 
 
class function TJSONobject.SelfTypeName: string; 
begin
  Result := 'jsObject';
end; 
 
function TJSONobject.GetFieldByIndex(idx: Integer): TJSONbase; 
var
  nm: WideString; 
begin 
  nm := GetNameOf(idx); 
  if nm <> '' then 
    begin 
      result := Field[nm]; 
    end 
  else 
    begin
      result := nil; 
    end; 
end; 
 
function TJSONobject.GetNameOf(idx: Integer): WideString; 
var
  mth: TlkJSONobjectmethod; 
begin 
  if (idx < 0) or (idx >= Count) then
    begin 
      result := ''; 
    end 
  else
    begin 
      mth := Child[idx] as TlkJSONobjectmethod; 
      result := mth.Name;
    end; 
end; 
 
procedure TJSONobject.SetFieldByIndex(idx: Integer; 
  const AValue: TJSONbase);
var
  nm: WideString; 
begin 
  nm := GetNameOf(idx); 
  if nm <> '' then
    begin 
      Field[nm] := AValue; 
    end; 
end; 
 
function TJSONobject.ForEachElement(idx: Integer; 
  var nm: string): TJSONbase;
begin
  nm := GetNameOf(idx);
  result := GetFieldByIndex(idx); 
end; 

constructor TJSONobject.Create;
begin
  inherited Create;
 // FUseHash := bUseHash;
 // ht := TlkHashTable.Create;
end; 
 
destructor TJSONobject.Destroy; 
begin
 // if assigned(ht) then FreeAndNil(ht); 
  inherited; 
end; 
 
function TJSONobject.getDouble(idx: Integer): Double;
var
  jn:TlkJSONnumber; 
begin 
  jn := FieldByIndex[idx] as TlkJSONnumber; 
  if not assigned(jn) then result := 0 
  else result := jn.Value; 
end; 
 
function TJSONobject.getInt(idx: Integer): Integer;
var
  jn:TlkJSONnumber; 
begin 
  jn := FieldByIndex[idx] as TlkJSONnumber;
  if not assigned(jn) then result := 0 
  else result := round(int(jn.Value)); 
end; 
 
function TJSONobject.getString(idx: Integer): String; 
var
  js:TlkJSONstring; 
begin 
  js := FieldByIndex[idx] as TlkJSONstring; 
  if not assigned(js) then result := '' 
  else result := vartostr(js.Value); 
end; 

function TJSONobject.getWideString(idx: Integer): WideString; 
var
  js:TlkJSONstring; 
begin 
  js := FieldByIndex[idx] as TlkJSONstring; 
  if not assigned(js) then result := '' 
  else result := VarToWideStr(js.Value); 
end; 
 
function TJSONobject.getDouble(nm: String): Double; 
begin
  result := getDouble(IndexOfName(nm)); 
end;
 
function TJSONobject.getInt(nm: String): Integer; 
begin
  result := getInt(IndexOfName(nm)); 
end; 
 
function TJSONobject.getString(nm: String): String; 
begin
  result := getString(IndexOfName(nm)); 
end; 
 
function TJSONobject.getWideString(nm: String): WideString; 
begin
  result := getWideString(IndexOfName(nm)); 
end; 
 
function TJSONobject.getBoolean(idx: Integer): Boolean; 
var
  jb:TJSONboolean; 
begin
  jb := FieldByIndex[idx] as TJSONboolean; 
  if not assigned(jb) then result := false
  else result := jb.Value; 
end; 
 
function TJSONobject.getBoolean(nm: String): Boolean;
begin
  result := getBoolean(IndexOfName(nm)); 
end; 
 
{ TJSON } 

class function TJSON.GenerateText(obj: TJSONbase; aIsInsertLF: Boolean =
    false): string;
var
{$IFDEF HAVE_FORMATSETTING} 
  fs: TFormatSettings; 
{$ENDIF} 
  pt1,pt0,pt2:PAnsiChar; 
  ptsz:cardinal;

{$ifndef NEW_STYLE_GENERATE}
  function gn_base(obj: TJSONbase): string;
  var
    ws: string;
    i, j: Integer;
    xs: TlkJSONstring;
  begin
    result := '';
    if not assigned(obj) then exit;
    if obj is TlkJSONnumber then
      begin
{$ifdef HAVE_FORMATSETTING}
        result := FloatToStr(TlkJSONnumber(obj).FValue, fs);
{$else}
        result := FloatToStr(TlkJSONnumber(obj).FValue);
        i := pos(DecimalSeparator, result);
        if (DecimalSeparator <> '.') and (i > 0) then
          result[i] := '.';
{$endif}
      end

    else if obj is TlkJSONstring then
      begin
//        ws := UTF8Encode(TlkJSONstring(obj).FValue);
        ws := TlkJSONstring(obj).FValue;
        i := 1;
        result := '"';


        result := Result + JSON_ConvertString(ws);

         {
        while i <= length(ws) do
          begin
            case ws[i] of
              '/', '\', '"': result := result + '\' + ws[i];
              #8: result := result + '\b'; 
              #9: result := result + '\t'; 
              #10: result := result + '\n'; 
              #13: result := result + '\r'; 
              #12: result := result + '\f'; 
            else 
              if ord(ws[i]) < 32 then
                result := result + '\u' + inttohex(ord(ws[i]), 4) 
              else 
                result := result + ws[i]; 
            end;
            inc(i); 
          end;
           }

        result := result + '"'; 
      end

    else if obj is TJSONboolean then 
      begin
        if TJSONboolean(obj).FValue then 
          result := 'true'
        else 
          result := 'false'; 
      end

    else if obj is TJSONnull then 
      begin
        result := 'null'; 
      end 
    else if obj is TJSONlist then 
      begin
        result := '['; 
        j := TJSONobject(obj).Count - 1;
        for i := 0 to j do
          begin 
            if i > 0 then result := result + ','; 
            result := result + gn_base(TJSONlist(obj).Child[i]); 
          end;
        result := result + ']'; 
      end

    else if obj is TlkJSONobjectmethod then 
      begin
        try 
          xs := TlkJSONstring.Create; 
          xs.FValue := TlkJSONobjectmethod(obj).FName; 
          result := gn_base(TJSONbase(xs)) + ':'; 
          result := result +
            gn_base(TJSONbase(TlkJSONobjectmethod(obj).FValue)); 
        finally
          if assigned(xs) then FreeAndNil(xs); 
        end; 
      end 
    else if obj is TJSONobject then 
      begin
        result := '{'; 
        j := TJSONobject(obj).Count - 1; 
        for i := 0 to j do
          begin 
            if i > 0 then result := result + ',';
            result := result + gn_base(TJSONobject(obj).Child[i]); 
          end;
        result := result + '}'; 
      end; 
  end; 
{$else} 
  procedure get_more_memory; 
  var delta: Integer; 
  begin
    delta := 20000; 
    if pt0 = nil then 
      begin 
        pt0 := AllocMem(delta); 
        ptsz := 0; 
        pt1 := pt0; 
      end 
    else 
      begin 
        ReallocMem(pt0,ptsz+delta); 
        pt1 := pointer(cardinal(pt0)+ptsz); 
      end; 
    ptsz := ptsz + delta; 
    pt2 := pointer(cardinal(pt1)+delta); 
  end; 
 
  procedure mem_ch(ch:char);
  begin 
    if pt1 >= pt2 then get_more_memory; 
    pt1^ := ch; 
    inc(pt1); 
  end; 
 
  procedure mem_write(rs: String); 
  var i: Integer; 
  begin
    for i := 1 to length(rs) do 
      begin 
        if pt1 >= pt2 then get_more_memory; 
        pt1^ := rs[i]; 
        inc(pt1); 
      end; 
  end; 
 
  procedure gn_base(obj: TJSONbase); 
  var
    ws: string;
    ch: Char;
    i, j: Integer;
    xs: TlkJSONstring;
  begin 
    if not assigned(obj) then
      exit;

    if obj is TlkJSONnumber then 
      begin 
{$ifdef HAVE_FORMATSETTING} 
        mem_write(FloatToStr(TlkJSONnumber(obj).FValue, fs)); 
{$else} 
        ws := FloatToStr(TlkJSONnumber(obj).FValue); 
        i := pos(DecimalSeparator, ws);
         
        if (DecimalSeparator <> '.') and (i > 0) then
           ws[i] := '.';

        mem_write(ws); 
{$endif} 
      end 
    else if obj is TlkJSONstring then 
      begin

//        ws := UTF8Encode(TlkJSONstring(obj).FValue);
        ws := TlkJSONstring(obj).FValue;


        i := 1;
        mem_ch('"');
        while i <= length(ws) do
          begin
            ch:=ws[i];

            case ch of
              '/', '\', '"':
                begin
                  mem_ch('\');
                  mem_ch(ws[i]);
                end;
              #8: mem_write('\b');
              #9: mem_write('\t');
              #10: mem_write('\n');
              #13: mem_write('\r');
              #12: mem_write('\f');
            else
              if (ord(ch) < 32) or (ord(ch) > 126) then
                mem_write('\u' + inttohex(ord(ch)- 224 +1072, 4))

//          if (Integer(AChar) < 32) or (Integer(AChar) > 126) then
//        begin
//          k:=Integer(AChar);
//
//          //� u0430 =  1072
//
//          s:=ESCAPE + 'u' + IntToHex(Integer(AChar) - 224 +1072, 4);



              else
                mem_ch(ch);
            end;
            inc(i);
          end;
          
        mem_ch('"');
      end

    else
      if obj is TJSONboolean then
      begin
        if TJSONboolean(obj).FValue then
          mem_write('true')
        else
          mem_write('false');
      end
    else
      if obj is TJSONnull then
      begin
        mem_write('null');
      end
    else
      if obj is TJSONlist then
      begin
        mem_ch('[');

        if aIsInsertLF then
          mem_write(DEF_LF);

        j := TJSONobject(obj).Count - 1;
        for i := 0 to j do
          begin
            if i > 0 then
            begin
               mem_ch(',');

               if aIsInsertLF then
                 mem_write(DEF_LF);
            end;

            gn_base(TJSONlist(obj).Child[i]);
          end;

        mem_ch(']');

        if aIsInsertLF then
          mem_write(DEF_LF);
      end
    else
      if obj is TlkJSONobjectmethod then
      begin
        try
          xs := TlkJSONstring.Create;
          xs.FValue := TlkJSONobjectmethod(obj).FName;
          gn_base(TJSONbase(xs));
          mem_ch(':');
          gn_base(TJSONbase(TlkJSONobjectmethod(obj).FValue));
        finally
          if assigned(xs) then FreeAndNil(xs);
        end;
      end

    else
      if obj is TJSONobject then
      begin
        mem_ch('{');

        if aIsInsertLF then
          mem_write(DEF_LF);

        j := TJSONobject(obj).Count - 1;
        for i := 0 to j do
          begin
            if i>0 then begin
              mem_ch(',');

              if aIsInsertLF then
                mem_write(DEF_LF);
            end;

            gn_base(TJSONobject(obj).Child[i]);
          end;

        mem_ch('}');

        if aIsInsertLF then
          mem_write(DEF_LF);
      end;
  end;
{$endif NEW_STYLE_GENERATE}

begin
{$ifdef HAVE_FORMATSETTING}
  GetLocaleFormatSettings(GetThreadLocale, fs); 
  fs.DecimalSeparator := '.'; 
{$endif} 
{$ifdef NEW_STYLE_GENERATE} 
  pt0 := nil; 
  get_more_memory; 
  gn_base(obj); 
  mem_ch(#0); 
  result := string(pt0); 
  freemem(pt0); 
{$else} 
  result := gn_base(obj); 
{$endif} 
end; 
 
class function TJSON.ParseText(const txt: string): TJSONbase;
{$ifdef HAVE_FORMATSETTING}
var 
  fs: TFormatSettings; 
{$endif} 

  function js_base(idx: Integer; var ridx: Integer; var o: 
    TJSONbase): Boolean; forward; 

  function xe(idx: Integer): Boolean;{$IFDEF FPC}inline;{$ENDIF}
  begin 
    result := idx <= length(txt); 
  end; 
 
  procedure skip_spc(var idx: Integer);{$IFDEF FPC}inline;{$ENDIF} 
  begin 
    while (xe(idx)) and (ord(txt[idx]) < 33) do 
      inc(idx); 
  end; 
 
  procedure add_child(var o, c: TJSONbase); 
  var
    i: Integer; 
  begin 
    if o = nil then 
      begin 
        o := c; 
      end 
    else 
      begin 
        if o is TlkJSONobjectmethod then 
          begin 
            TlkJSONobjectmethod(o).FValue := c; 
          end 
        else if o is TJSONlist then 
          begin
            TJSONlist(o)._Add(c); 
          end
        else if o is TJSONobject then 
          begin
            i := TJSONobject(o)._Add(c); 
           // if TJSONobject(o).UseHash then
            //  TJSONobject(o).ht.AddPair(TlkJSONobjectmethod(c).Name, i); 
          end;
      end; 
  end; 
 
  function js_boolean(idx: Integer; var ridx: Integer; var o: 
    TJSONbase): Boolean; 
  var
    js: TJSONboolean; 
  begin
    skip_spc(idx); 
    if copy(txt, idx, 4) = 'true' then 
      begin 
        result := true; 
        ridx := idx + 4; 
        js := TJSONboolean.Create; 
        js.FValue := true;
        add_child(o, TJSONbase(js)); 
      end
    else if copy(txt, idx, 5) = 'false' then 
      begin 
        result := true; 
        ridx := idx + 5; 
        js := TJSONboolean.Create; 
        js.FValue := false;
        add_child(o, TJSONbase(js)); 
      end
    else 
      begin 
        result := false; 
      end; 
  end; 
 
  function js_null(idx: Integer; var ridx: Integer; var o: 
    TJSONbase): Boolean; 
  var
    js: TJSONnull; 
  begin
    skip_spc(idx); 
    if copy(txt, idx, 4) = 'null' then 
      begin 
        result := true; 
        ridx := idx + 4; 
        js := TJSONnull.Create; 
        add_child(o, TJSONbase(js));
      end
    else 
      begin 
        result := false; 
      end; 
  end; 
 
  function js_integer(idx: Integer; var ridx: Integer): Boolean; 
  begin 
    result := false; 
    while (xe(idx)) and (txt[idx] in ['0'..'9']) do 
      begin 
        result := true; 
        inc(idx); 
      end; 
    if result then ridx := idx; 
  end; 
 
  function js_number(idx: Integer; var ridx: Integer; var o: 
    TJSONbase): Boolean; 
  var
    js: TlkJSONnumber; 
    ws: string; 
{$IFNDEF HAVE_FORMATSETTING} 
    i: Integer; 
{$ENDIF} 
  begin 
    skip_spc(idx); 
    result := xe(idx); 
    if not result then exit; 
    if txt[idx] in ['+', '-'] then 
      begin 
        inc(idx); 
        result := xe(idx); 
      end; 
    if not result then exit; 
    result := js_integer(idx, idx); 
    if not result then exit; 
    if (xe(idx)) and (txt[idx] = '.') then 
      begin 
        inc(idx); 
        result := js_integer(idx, idx); 
        if not result then exit; 
      end; 
    if (xe(idx)) and (txt[idx] in ['e', 'E']) then 
      begin 
        inc(idx); 
        if (xe(idx)) and (txt[idx] in ['+', '-']) then inc(idx); 
        result := js_integer(idx, idx); 
        if not result then exit; 
      end; 
    if not result then exit; 
    js := TlkJSONnumber.Create; 
    ws := copy(txt, ridx, idx - ridx); 
{$IFDEF HAVE_FORMATSETTING} 
    js.FValue := StrToFloat(ws, fs); 
{$ELSE} 
    i := pos('.', ws); 
    if (DecimalSeparator <> '.') and (i > 0) then 
      ws[pos('.', ws)] := DecimalSeparator; 
    js.FValue := StrToFloat(ws); 
{$ENDIF} 
    add_child(o, TJSONbase(js)); 
    ridx := idx;
  end; 
 
  function js_string(idx: Integer; var ridx: Integer; var o: 
    TJSONbase): Boolean; 
  var
    js: TlkJSONstring; 
    fin: Boolean; 
    ws: WideString; 
  begin 
    skip_spc(idx);
    ws := ''; 
    result := xe(idx); 
    if not result then exit; 
    result := txt[idx] = '"'; 
    if not result then exit; 
    inc(idx); 
    result := false; 
    repeat 
      fin := not xe(idx);
      if not fin then 
        begin 
          if txt[idx] = '\' then 
            begin 
              inc(idx); 
              if not xe(idx) then exit; 
              case txt[idx] of 
                '\': ws := ws + '\'; 
                '"': ws := ws + ''''; 
                '/': ws := ws + '/'; 
                'b': ws := ws + #8; 
                'f': ws := ws + #12; 
                'n': ws := ws + #10; 
                'r': ws := ws + #13; 
                't': ws := ws + #9; 
                'u': 
                  begin 
//                    ws := ws + widechar(strtoint('$' + 
//                      copy(txt, idx + 1, 4))); 
                    ws := ws + code2utf(strtoint('$' + copy(txt, idx + 1, 4)));
                    idx := idx + 4; 
                  end;
              end; 
            end 
          else if txt[idx] <> '"' then 
            begin 
              ws := ws + txt[idx]; 
            end 
          else 
            begin 
              fin := true; 
              result := true; 
            end; 
          inc(idx); 
        end; 
    until fin; 
    if not result then exit; 
    js := TlkJSONstring.Create; 
    js.FValue := UTF8Decode(ws); 
    add_child(o, TJSONbase(js)); 
    ridx := idx;
  end; 
 
  function js_list(idx: Integer; var ridx: Integer; var o: 
    TJSONbase): Boolean; 
  var
    js: TJSONlist; 
  begin
    result := false; 
    try 
      js := TJSONlist.Create; 
      skip_spc(idx);
      result := xe(idx); 
      if not result then exit; 
      result := txt[idx] = '['; 
      if not result then exit; 
      inc(idx); 
      while js_base(idx, idx, TJSONbase(js)) do 
        begin
          skip_spc(idx); 
          if (xe(idx)) and (txt[idx] = ',') then inc(idx); 
        end; 
      result := (xe(idx)) and (txt[idx] = ']'); 
      if not result then exit; 
      inc(idx); 
    finally 
      if not result then 
        begin 
          js.Free; 
        end 
      else 
        begin 
          add_child(o, TJSONbase(js)); 
          ridx := idx;
        end; 
    end; 
  end; 
 
  function js_method(idx: Integer; var ridx: Integer; var o: 
    TJSONbase): Boolean; 
  var
    mth: TlkJSONobjectmethod; 
    ws: TlkJSONstring; 
  begin 
    result := false; 
    try 
      ws := nil; 
      mth := TlkJSONobjectmethod.Create; 
      skip_spc(idx); 
      result := xe(idx); 
      if not result then exit; 
      result := js_string(idx, idx, TJSONbase(ws)); 
      if not result then exit;
      skip_spc(idx); 
      result := xe(idx) and (txt[idx] = ':'); 
      if not result then exit; 
      inc(idx); 
      mth.FName := ws.FValue; 
      result := js_base(idx, idx, TJSONbase(mth)); 
    finally
      if ws <> nil then ws.Free; 
      if result then 
        begin 
          add_child(o, TJSONbase(mth)); 
          ridx := idx;
        end 
      else 
        begin 
          mth.Free; 
        end; 
    end; 
  end; 
 
  function js_object(idx: Integer; var ridx: Integer; var o: 
    TJSONbase): Boolean; 
  var
    js: TJSONobject; 
  begin
    result := false; 
    try 
      js := TJSONobject.Create; 
      skip_spc(idx);
      result := xe(idx); 
      if not result then exit; 
      result := txt[idx] = '{'; 
      if not result then exit;
      inc(idx); 
      while js_method(idx, idx, TJSONbase(js)) do 
        begin
          skip_spc(idx); 
          if (xe(idx)) and (txt[idx] = ',') then inc(idx); 
        end; 
      result := (xe(idx)) and (txt[idx] = '}'); 
      if not result then exit; 
      inc(idx); 
    finally 
      if not result then 
        begin 
          js.Free;
        end 
      else 
        begin 
          add_child(o, TJSONbase(js)); 
          ridx := idx;
        end; 
    end; 
  end; 
 
  function js_base(idx: Integer; var ridx: Integer; var o: 
    TJSONbase): Boolean; 
  begin
    skip_spc(idx);
    result := js_boolean(idx, idx, o); 
    if not result then result := js_null(idx, idx, o); 
    if not result then result := js_number(idx, idx, o); 
    if not result then result := js_string(idx, idx, o); 
    if not result then result := js_list(idx, idx, o); 
    if not result then result := js_object(idx, idx, o); 
    if result then ridx := idx; 
  end; 
 
var 
  idx: Integer; 
begin 
{$ifdef HAVE_FORMATSETTING}
  GetLocaleFormatSettings(GetThreadLocale, fs); 
  fs.DecimalSeparator := '.'; 
{$endif} 
 
  result := nil; 
  if txt = '' then exit; 
  try 
    idx := 1; 
    if not js_base(idx, idx, result) then FreeAndNil(result); 
  except 
    if assigned(result) then FreeAndNil(result); 
  end; 
end;
 
{ ElkIntException } 
 
constructor ElkIntException.Create(idx: Integer; msg: string); 
begin 
  self.idx := idx; 
  inherited Create(msg); 
end; 


{ TlkJSONstreamed } 
{$IFNDEF KOL} 
 
{$ENDIF} 
 
{ TlkJSONdotnetclass } 
 
{$IFDEF DOTNET}

{$ENDIF DOTNET}




function TJSONobject.AddList(const aName: WideString): TJSONlist;
begin
  Result := TJSONlist.Create;
  Add(aName, Result);
end;


function TJSONobject.AddObject(const aName: WideString): TJSONobject;
begin
  Result := TJSONobject.create;
  Add(aName, Result);
end;


function TJSONlist.AddObject: TJSONobject;
begin
  Result := TJSONobject.create;
  Add(Result);
end;


function TJSONlist.AddList: TJSONlist;
begin
  Result := TJSONlist.create;
  Add(Result);
end;



procedure TJSONobject.AddEx(aValues: array of Variant);
var
  I: Integer;
  s: string;
  sName: string;
  v: Variant;
begin
//  Result := TJSONobject.create;
 // Add(Result);

  assert(Length(aValues) mod 2 = 0);

  for I := 0 to (Length(aValues) div 2) - 1 do
  begin
    v:=aValues[i*2+1];

    sName:=aValues[i*2];

   // VarTypeAsText()

    case VarType(v) of
      varNull   : Add(sName, TJSONnull.Create );
      varBoolean: Add(sName, Boolean(v));

      varByte,
      varSmallint,
      varInteger: Add(sName, Integer (v) );

      varString : Add(sName, String (v) );
      varDouble : Add(sName, Double (v) );
    else
     begin
     //  ShowMessage(VarTypeAsText(VarType(v));

      s:=VarTypeAsText(VarType(v));

      raise Exception.Create( VarTypeAsText(VarType(v)));

     end;
//      ShowMessage(VarTypeAsText(VarType(v));

    end;

   // Result.Add(aValues[i*2], aValues[i*2+1]);

  end;

end;

// ---------------------------------------------------------------
function TJSONlist.AddObjectEx(aValues: array of Variant): TJSONobject;
// ---------------------------------------------------------------
var
  I: Integer;
  v: Variant;
begin
  Result := TJSONobject.create;
  Add(Result);

  Result.AddEx (aValues);

//
//  assert(Length(aValues) mod 2 = 0);
//
//  for I := 0 to (Length(aValues) div 2) - 1 do
//  begin
//    v:=aValues[i*2+1];
//
//   // VarTypeAsText()
//
//    case VarType(v) of
//      varSmallint,
//      varInteger: Result.Add(aValues[i*2], Integer (v) );
//
//      varString : Result.Add(aValues[i*2], String (v) );
//      varDouble : Result.Add(aValues[i*2], Double (v) );
//    else
//      raise Exception.Create( VarTypeAsText(VarType(v)));
//    end;
//
//   // Result.Add(aValues[i*2], aValues[i*2+1]);
//
//  end;

end;

//
//type
//  TVarArr = array of Variant;
//
//
//function JSON_encode11(aValues: array of TVarArr): string;
//begin
//
//
//end;
//


function JSON_encode(aValues: array of Variant): string;
var
  js: TJSONobject;
begin
  js := TJSONobject.Create;
  js.AddEx (aValues);

  result := TJSON.GenerateText(js);

  FreeAndNil(js);
end;


function JSON_encode_new(aValues: array of Variant): string;
var
  I: Integer;
  iCount: Integer;
  js: TJSONobject;

//  oSL: TStringList;
  sParam: string;
  sValue: string;
begin
 // oSL:=TStringList.Create;

  Result:='';

  iCount:=(Length(aValues) div 2);

  for I := 0 to iCount - 1 do
  begin
    sParam:=aValues[i*2];
    sValue:=VarToStr( aValues[i*2+1]);

  //  oSl.Add( Format('"%s": "%s"', [sParam, sValue]));


    Result:=Result + Format('"%s": "%s"', [sParam, sValue]);

    if i<> (iCount - 1) then
      Result:=Result + ',' ;

  end;

 // oSL.QuoteChar:=' ';
 // oSL.Delimiter:=',';


 // Result:=oSL.CommaText;

  Result:=Format('{%s}', [Result]);

 // FreeAndNil(oSL);

end;



// ---------------------------------------------------------------
function JSON_ConvertString(AValue: string): string;
// ---------------------------------------------------------------
const
  ESCAPE = '\';
  // QUOTATION_MARK = '"';
  REVERSE_SOLIDUS = '\';
  SOLIDUS = '/';
  BACKSPACE = #8;
  FORM_FEED = #12;
  NEW_LINE = #10;
  CARRIAGE_RETURN = #13;
  HORIZONTAL_TAB = #9;
var
  I: Integer;
  AChar: Char;
  k: Integer;
  s: string;
begin
  Result := '';

//  AValue := UTF8Encode(AValue);



  for I := 1 to Length(AValue)  do
  begin
    AChar:=AValue[i];
//  end;

//  for AChar in AValue do
 // begin

    case AChar of
      // !! Double quote (") is handled by TJSONString
      // QUOTATION_MARK: Result := Result + ESCAPE + QUOTATION_MARK;
      REVERSE_SOLIDUS: Result := Result + ESCAPE + REVERSE_SOLIDUS;
      SOLIDUS: Result := Result + ESCAPE + SOLIDUS;
      BACKSPACE: Result := Result + ESCAPE + 'b';
      FORM_FEED: Result := Result + ESCAPE + 'f';
      NEW_LINE: Result := Result + ESCAPE + 'n';
      CARRIAGE_RETURN: Result := Result + ESCAPE + 'r';
      HORIZONTAL_TAB: Result := Result + ESCAPE + 't';
      else
      begin
        if (Integer(AChar) < 32) or (Integer(AChar) > 126) then
        begin
          k:=Integer(AChar);

          //� u0430 =  1072

          s:=ESCAPE + 'u' + IntToHex(Integer(AChar) - 224 +1072, 4);

          Result := Result + s;
        end
        else
          Result := Result + AChar;
      end;
    end;
  end;
end;




procedure test;
var
  s: string;
begin
  s:=JSON_ConvertString('���');
  s:='';

end;    //


begin
 // test;

  //JSON_encode11 ([[11] [22]]);



end.





program sample1;

{$APPTYPE CONSOLE}
  {
uses
  SysUtils,
  u_JSON in 'u_JSON.pas';

var
  js: TJSONobject;
  ws: TJSONstring;
  s: String;
  i: Integer;

  js_1: TJSONobject;
  js_2: TJSONobject;

  l: TJSONlist;

begin
  
  js_1 := TJSONobject.Create;
  js_1.Add('aaaaaaaaaaaaa','hhhhhhhhhh');
  js_1.Add('a33333333aaaaaaaaaaaa','222hhhhhhhhhh');


  l:=TJSONlist.Create;

  js_2 := TJSONobject.Create;
  js_2.Add('aaaaaaaaaaaaa','hhhhhhhhhh');
  js_2.Add('a33333333aaaaaaaaaaaa','222hhhhhhhhhh');

  l.Add(js_2);
 // l.Add(js_2);
 // l.Add(js_2);   

  js_1.Add('aaa',l);
  

  js := TJSONobject.Create;
//  js.add('namestring', TlkJSONstring.Generate('namevalue'));
  js.Add('namestring',  'namevalue');
  js.Add('namestring1', 'namevalue2');
  js.Add('namestring1', js_1);





  // ---------------------------------------------------------------
function TdmTask_Relief.Get_All_Bounds_JSON: string;
// ---------------------------------------------------------------
var
  I: Integer;
  js: TJSONobject;
  l: TJSONlist;
 // oMainJS: TJSONobject;
  oPoint: TJSONobject;
  oPoints: TJSONlist;

  bl_arr: TBLPointArray;
  j: Integer;
  oRootJS: TJSONobject;
  sFileName: string;

begin
  oRootJS := TJSONobject.Create;
 // oMainJS :=oRootJS.AddObject('site');

  l:=oRootJS.AddList('items');

  for I := 0 to GetReliefEngine.Count-1  do
  begin
    sFileName:=GetReliefEngine.Items[i].FileName;
    bl_arr   :=GetReliefEngine.Items[i].GetPoints_WGS();

    js:=l.AddObject;
//    js:=TJSONobject_AddObject(l);
  //  js.Add('filename', sFileName);


    oPoints:=js.AddList('points');

    for j := 0 to High(bl_arr) do
    begin
     // oPoint := oPoints.AddObjectEx ([
      oPoints.AddObjectEx ([
        'lat', bl_arr[j].B,
        'lng', bl_arr[j].L
      ]);

     // oPoint.Add('lat', bl_arr[j].B);
    //  oPoint.Add('lng', bl_arr[j].L);
    end;
  end;

  result := TJSON.GenerateText(oRootJS);

  FreeAndNil(oRootJS);

end;






function EscapeString(const AValue: string): string;
const
  ESCAPE = '\';
  // QUOTATION_MARK = '"';
  REVERSE_SOLIDUS = '\';
  SOLIDUS = '/';
  BACKSPACE = #8;
  FORM_FEED = #12;
  NEW_LINE = #10;
  CARRIAGE_RETURN = #13;
  HORIZONTAL_TAB = #9;
var
  AChar: Char;
begin
  Result := '';
  for AChar in AValue do
  begin
    case AChar of
      // !! Double quote (") is handled by TJSONString
      // QUOTATION_MARK: Result := Result + ESCAPE + QUOTATION_MARK;
      REVERSE_SOLIDUS: Result := Result + ESCAPE + REVERSE_SOLIDUS;
      SOLIDUS: Result := Result + ESCAPE + SOLIDUS;
      BACKSPACE: Result := Result + ESCAPE + 'b';
      FORM_FEED: Result := Result + ESCAPE + 'f';
      NEW_LINE: Result := Result + ESCAPE + 'n';
      CARRIAGE_RETURN: Result := Result + ESCAPE + 'r';
      HORIZONTAL_TAB: Result := Result + ESCAPE + 't';
      else
      begin
        if (Integer(AChar) < 32) or (Integer(AChar) > 126) then
          Result := Result + ESCAPE + 'u' + IntToHex(Integer(AChar), 4)
        else
          Result := Result + AChar;
      end;
    end;
  end;
end;




function JSON_ConvertString11(AValue: string): string;

  procedure AddChars(const AChars: string; var Dest: string; var AIndex: Integer);
  begin
    System.Insert(AChars, Dest, AIndex);
    System.Delete(Dest, AIndex + 2, 1);
    Inc(AIndex, 2);
  end;

  procedure AddUnicodeChars(const AChars: string; var Dest: string; var AIndex: Integer); 
  begin
    System.Insert(AChars, Dest, AIndex);
    System.Delete(Dest, AIndex + 6, 1);
    Inc(AIndex, 6);
  end;

var
  i, ix: Integer;
  AChar: Char;
begin
  Result := AValue;
  ix := 1;
  for i := 1 to System.Length(AValue) do
  begin
    AChar :=  AValue[i];
    case AChar of
      '/', '\', '"':
      begin
        System.Insert('\', Result, ix);
        Inc(ix, 2);
      end;
      #8:  //backspace \b
      begin
        AddChars('\b', Result, ix);
      end;
      #9:
      begin
        AddChars('\t', Result, ix);
      end;
      #10:
      begin
        AddChars('\n', Result, ix);
      end;
      #12:
      begin
        AddChars('\f', Result, ix);
      end;
      #13:
      begin
        AddChars('\r', Result, ix);
      end;
      #0 .. #7, #11, #14 .. #31:
      begin
        AddUnicodeChars('\u' + IntToHex(Word(AChar), 4), Result, ix);
      end
      else
      begin
        if Word(AChar) > 127 then
        begin
          AddUnicodeChars('\u' + IntToHex(Word(AChar), 4), Result, ix);
        end
        else
        begin
          Inc(ix);
        end;
      end;
    end;
  end;
end;


