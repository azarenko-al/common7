unit u_crc32;

interface
uses
  SysUtils,

//  md5,

   IdHashCRC;

  function GetHashCRC32_AsString(aValue: string): string;

  function GetHashCRC32_AsLongWord(aValue: string): LongWord;
//  function GetHashCRC32(aValue: string): LongWord;


implementation


function GetHashCRC32_AsString(aValue: string): string;
//  function GetHashCRC32(aValue: string): Longword;
//string;
var
  obj: TIdHashCRC32;

 // w: LongWord;
begin

   {$RANGECHECKS ON}
 // Result := MD5String2(aValue);
   {$RANGECHECKS OFF}

  obj:=TIdHashCRC32.Create();

//  obj.
  Result:=  IntToStr( obj.HashValue(aValue));
//  Result := Integer(obj.HashValue(aValue));

  FreeAndNil(obj);

end;



function GetHashCRC32_AsLongWord(aValue: string): LongWord;
//  function GetHashCRC32(aValue: string): Longword;
//string;
var
  obj: TIdHashCRC32;

 // w: LongWord;
begin

   {$RANGECHECKS ON}
 // Result := MD5String2(aValue);
   {$RANGECHECKS OFF}

  obj:=TIdHashCRC32.Create();

//  obj.
//  Result:=  IntToStr( obj.HashValue(aValue));
  Result := Integer(obj.HashValue(aValue));

  FreeAndNil(obj);

end;


end.

