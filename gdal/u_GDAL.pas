unit u_GDAL;

interface

uses
  SysUtils,

  u_func;



type

  TGDAL_HDR = record
 // public
    ColCount, RowCount: integer;
    Lon_min,Lat_max: double;
    CellSize: double;

    datatype: integer;
    Zone: integer;
  

//    procedure SaveToFile(aFileName: string);

  end;


  procedure TGDAL_HDR_SaveToFile(aRec: TGDAL_HDR; aFileName: string);



implementation

// ---------------------------------------------------------------
procedure TGDAL_HDR_SaveToFile(aRec: TGDAL_HDR; aFileName: string);
// ---------------------------------------------------------------
//  sParam:= Format(' -a_srs "+proj=utm  +zone=%d +datum=WGS84" -of GTiff  -co "COMPRESS=LZW" -a_ullr %1.12f  %1.12f %1.12f %1.12f  %s %s ',

const
  CRLF = #13+#10;
  LF = CRLF;  
 
  
const
  DEF =
      'ENVI'+ LF+ 
      'samples = %d'+ LF+ 
      'lines   = %d'+ LF+ 
      'bands   = 1 '+ LF+ 
      'header offset = 0  '+ LF+ 
      'file type = ENVI Standard '+ LF+ 
      'data type = %d '+ LF+ 
      'interleave = bsq '+ LF+ 
      'byte order = 0 '+ LF+ 
      'map info = {Transverse Mercator,  1, 1, %d, %d, %d, %d, %d}'; 
      
//projection info = {3, 6378245, 63, 1, 1, %d, %d, %d, %d, %d, North,WGS-84}' ;
      
//      'map info = {UTM, 1, 1, %d, %d, %d, %d, %d, North,WGS-84}' ;

 //gdalwarp -t_srs EPSG:28406  all_wgs_.tif all_pulkovo.tif 

//map info = {Transverse Mercator, 1, 1, 8414011.63, 6256311.48, 20, 20}
//projection info = {3, 6378245, 6356863.0188, 0, 45, 8500000, 0, 1, Transverse Mercator}
//coordinate system string = {PROJCS["Transverse_Mercator",GEOGCS["GCS_Pulkovo 1942",DATUM["D_Pulkovo_1942",SPHEROID["Krasovsky_1940",6378245,298.3]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",45],PARAMETER["scale_factor",1],PARAMETER["false_easting",8500000],PARAMETER["false_northing",0],UNIT["Meter",1]]}
//band names = {
//Band 1}

//!table
//!version 300
//!charset WindowsCyrillic
//
//Definition Table   
//  File "color_nnovg_pl_2D.bmp" 
//  Type "RASTER"    
//  (8414011.6300000013,6256311.4800000000) (0,0) Label "Pt 1",
//  (8454431.6300000013,6256311.4800000000) (2021,0) Label "Pt 2",
//  (8454431.6300000013,6227051.4800000000) (2021,1463) Label "Pt 3",
//  (8414011.6300000013,6227051.4800000000) (0,1463) Label "Pt 4"
//            
//CoordSys Earth Projection 8, 1001, "m", 45, 0, 1,  8500000, 0
//Units "m" 
//RasterStyle 4 0 
//RasterStyle 7 16777215

      

var
  s: string;
begin

  s:= Format(DEF,[
      ColCount, RowCount,

      datatype,

    Trunc(Lon_min),
    Trunc(Lat_max),    

    Trunc(CellSize),
    Trunc(CellSize),

    Zone
   ]);


//  TFile.WriteAllText(ChangeFileExt(aFileName,'.hdr'), s  );

  StrToTextFile(ChangeFileExt(aFileName,'.hdr'), s  );

end;



end.
