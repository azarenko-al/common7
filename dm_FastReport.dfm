object dmFastReport: TdmFastReport
  OldCreateOrder = False
  Left = 65528
  Top = 65528
  Height = 1176
  Width = 1936
  object frxReport: TfrxReport
    Version = '6.2.11'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.ShowCaptions = True
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41121.613084722200000000
    ReportOptions.LastChange = 41940.802632372700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '    '
      '    '
      ''
      ''
      '//------------------------------------------------------------'
      'function DegreeToStr (aValue: double): String;'
      '//------------------------------------------------------------'
      'const'
      '//  STR_BLPOINT_FORMAT1 = '#39'%d'#176'%.2d'#39#39'%s%1.2f'#39#39#39#39#39';'
      '  STR_BLPOINT_FORMAT1 = '#39'%d'#176' %.2d'#39#39' %1.2f'#39#39#39#39#39';'
      'var    '
      ' iDeg,iMin:integer; '
      ' eSec: Double;   '
      '           '
      'begin'
      '  geo_DecodeDegree_(aValue, iDeg,iMin,eSec);'
      '                           '
      ' // angle.Sec:=TruncFloat(angle.Sec,2);'
      ' '
      '    Result:=Format(STR_BLPOINT_FORMAT1, [iDeg, iMin, eSec]);'
      ''
      '  if aValue<0 then'
      '    Result:='#39'-'#39'+Result;'
      'end;'
      '  '
      ''
      ' //------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_(aValue: Double; var aDeg,aMin:intege' +
        'r; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eSec: Double;'
      ''
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      ''
      ''
      '   //    aValue:=99;                            '
      '          '
      ''
      '          '
      '  aDeg:=Trunc(aValue);'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      ''
      '//  e:= (aValue - Trunc(aValue));'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      ''
      ''
      '  aMin:=Trunc(e);'
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  while aSec>(60-0.1) do'
      '  begin'
      '    aMin:=aMin+1;'
      '    aSec:=aSec-60;'
      '  end;    '
      ''
      '  while aMin>=60 do'
      '  begin'
      '      aMin:=aMin-60;'
      '      aDeg:=aDeg+1;'
      '  end;  '
      ''
      '  if aSec<0 then'
      '     aSec:=0;'
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      ''
      ''
      ''
      'end;'
      '  '
      ''
      '                                 '
      ''
      '//------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_1111(aValue: Double; var aDeg,aMin:in' +
        'teger; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'const'
      ' EXP_ = 0.000000001;'
      '     //97,033333333'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eMinutes: Double;'
      '  eSec: Double;'
      '  eSeconds: Double;'
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      ' // e:=Abs(aValue)*60;'
      ''
      '//  , Frac(X) = X - Int(X).'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      '//  Result.Sec:=Frac(Frac(Abs(aValue))*60)*60;'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      '  aMin:=Trunc(e);'
      ''
      ' eMinutes := Trunc (e);'
      ' eSeconds := (e - Trunc (e)) * 60 ;'
      ''
      ''
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  if (aSec - 60) > EXP_ then'
      '  begin'
      '    Inc(aMin);'
      '    aSec := 0;'
      '  end;'
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      ''
      '      '
      ''
      ''
      'end;'
      '        '
      ''
      '       '
      'begin'
      ''
      'end.')
    Left = 32
    Top = 32
    Datasets = <>
    Variables = <>
    Style = <
      item
        Name = 'Title'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        Fill.BackColor = clGray
      end
      item
        Name = 'Header'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
      end
      item
        Name = 'Group header'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        Fill.BackColor = 16053492
      end
      item
        Name = 'Data'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
      end
      item
        Name = 'Group footer'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
      end
      item
        Name = 'Header line'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftBottom]
        Frame.Width = 2.000000000000000000
      end>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 600.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 256
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      PrintIfEmpty = False
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Frame.Typ = []
        Height = 45.354360000000000000
        Top = 16.000000000000000000
        Width = 2192.127400000000000000
        StartNewPage = True
        object Memo8: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Width = 2192.127400000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            
              #1056#1119#1057#1026#1056#1109#1056#181#1056#1108#1057#8218' '#1057#8225#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#1029#1056#1109'-'#1057#8218#1056#181#1057#1026#1057#1026#1056#1105#1057#8218#1056#1109#1057#1026#1056#1105#1056#176#1056#187#1057#1034#1056#1029#1056#1109#1056#1110#1056#1109' '#1056 +
              #1111#1056#187#1056#176#1056#1029#1056#176'  '#1056#1029#1056#176' '#1057#8218#1056#181#1057#1026#1057#1026#1056#1105#1057#8218#1056#1109#1057#1026#1056#1105#1056#1105)
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 120.944960000000000000
        Top = 216.000000000000000000
        Width = 2192.127400000000000000
        DataSet = dmLink_SDB_report.frxDBDataset_link
        DataSetName = 'Link'
        RowCount = 0
        object property1name: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 162.519790000000000000
          Height = 60.472440940000000000
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."LinkEnd1_Property_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2name: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 60.472480000000000000
          Width = 162.519790000000000000
          Height = 60.472440940000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."LinkEnd2_Property_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line: TfrxMemoView
          AllowVectorExport = True
          Width = 30.236240000000000000
          Height = 120.944923390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Line]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property1address: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 238.110390000000000000
          Width = 226.771653540000000000
          Height = 60.472440940000000000
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."LinkEnd1_Property_address"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2address: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 238.110390000000000000
          Top = 60.472480000000000000
          Width = 226.771653540000000000
          Height = 60.472440940000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."LinkEnd2_Property_address"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1LinkEndTypename: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 691.653697080000000000
          Width = 132.283464570000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_LinkEndType_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 691.653697080000000000
          Top = 60.472440940000000000
          Width = 132.344510160000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_LinkEndType_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1antenna_height: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1065.826969370000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_antenna_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1065.888014960000000000
          Top = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_antenna_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2rx_freq_MHz: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 823.998207240000000000
          Top = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 823.937161650000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2antenna_polarization: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1487.683336930000000000
          Width = 53.291338580000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_antenna_polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1487.683336930000000000
          Top = 60.472480000000000000
          Width = 53.291338580000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_antenna_polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2antenna_diameter: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1434.391998350000000000
          Width = 53.291338580000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_antenna_diameter"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1434.391998350000000000
          Top = 60.472480000000000000
          Width = 53.291338580000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_antenna_diameter"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 899.527712830000000000
          Width = 49.133858270000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_antenna_horz_width"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 899.588758420000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_antenna_horz_width"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 948.661571100000000000
          Width = 49.133858270000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_antenna_vert_width"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 948.722616690000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_antenna_vert_width"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 997.795429370000000000
          Width = 68.031540000000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_antenna_gain"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 997.856474960000000000
          Top = 60.472480000000000000
          Width = 68.031540000000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_antenna_gain"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object linklength_km: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1194.391951970000000000
          Width = 52.913385830000000000
          Height = 120.944960000000000000
          DataField = 'length_km'
          DataSet = dmLink_SDB_report.frxDBDataset_link
          DataSetName = 'Link'
          DisplayFormat.FormatStr = '%2.1f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."length_km"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1power_dBm: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1247.305337800000000000
          Width = 52.913385830000000000
          Height = 120.944960000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend1_power_dBm"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2radiation_class: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1300.218723630000000000
          Top = 60.472480000000000000
          Width = 71.811070000000000000
          Height = 60.472480000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_radiation_class"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1300.218723630000000000
          Width = 71.811070000000000000
          Height = 60.472480000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_radiation_class"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2bitrate_Mbps: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1372.029793630000000000
          Top = 60.472480000000000000
          Width = 62.362204720000000000
          Height = 60.472480000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_bitrate_Mbps"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1372.029793630000000000
          Width = 62.362204720000000000
          Height = 60.472480000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_bitrate_Mbps"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2lat_str: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 464.882043540000000000
          Top = 60.472480000000000000
          Width = 113.385826770000000000
          Height = 60.472480000000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<link."LinkEnd2_property_lat_gck_2011">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 464.882043540000000000
          Width = 113.385826770000000000
          Height = 60.472480000000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<link."LinkEnd1_property_lat_gck_2011">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 578.267870310000000000
          Top = 60.472480000000000000
          Width = 113.385826770000000000
          Height = 60.472480000000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<link."LinkEnd2_property_lon_gck_2011">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 578.267870310000000000
          Width = 113.385826770000000000
          Height = 60.472480000000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<link."LinkEnd1_property_lon_gck_2011">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 30.236240000000000000
          Width = 45.354360000000000000
          Height = 60.472440940000000000
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."linkend1_property_code"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 30.236240000000000000
          Top = 60.472480000000000000
          Width = 45.354360000000000000
          Height = 60.472440940000000000
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."linkend2_property_code"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1141.417520550000000000
          Width = 52.913385830000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_antenna_azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1141.478566140000000000
          Top = 60.472480000000000000
          Width = 52.913385830000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_antenna_azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 75.590551180000000000
        Top = 120.000000000000000000
        Width = 2192.127400000000000000
        object Memo3: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 238.110390000000000000
          Width = 226.771653540000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1106#1056#1169#1057#1026#1056#181#1057#1027)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 948.661571100000000000
          Width = 49.133858270000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8221#1056#1116#1056#1106
            #1056#1030#1056#181#1057#1026#1057#8218'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 899.527712830000000000
          Width = 49.133858270000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8221#1056#1116#1056#1106
            #1056#1110#1056#1109#1057#1026'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 30.236240000000000000
          Height = 75.590551181102400000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1074#8222#8211
            #1056#1111'/'#1056#1111)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 464.882043540000000000
          Width = 113.385826770000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1025#1056#1105#1057#1026#1056#1109#1057#8218#1056#176','
            #1057#1027'.'#1057#8364'.  ('#1056#8220'CK-2011)')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 691.653697080000000000
          Width = 132.283464570000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '
            #1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 823.937161650000000000
          Width = 75.590551180000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          Memo.UTF8 = (
            #1056#152#1056#1029#1056#1169#1056#181#1056#1108#1057#1027
            '('#1056#1029#1056#1109#1056#1112#1056#1105#1056#1029#1056#176#1056#187#1057#8249' '#1057#8225#1056#176#1057#1027#1057#8218#1056#1109#1057#8218'), '#1056#1114#1056#8220#1057#8224)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 578.267870310000000000
          Width = 113.385826770000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8221#1056#1109#1056#187#1056#1110#1056#1109#1057#8218#1056#176','
            #1056#1030'.'#1056#1169'.   ('#1056#8220'CK-2011)')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 30.236240000000000000
          Width = 45.354360000000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1074#8222#8211
            #1056#177'/'#1057#1027)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 997.795429370000000000
          Width = 68.031496060000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1113#1057#1107','
            #1056#1169#1056#8216)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1065.826925430000000000
          Width = 75.590551180000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '
            #1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176' '
            #1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249','
            ' '#1056#1112)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1486.126183440000000000
          Width = 53.291338580000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1432.834844860000000000
          Width = 53.291338580000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112'.'
            #1056#176#1056#1029#1057#8218'., '#1056#1112)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1194.330862440000000000
          Width = 52.913385830000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#160#1056#176#1057#1027#1057#1027#1057#8218'., '#1056#1108#1056#1112)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1247.244248270000000000
          Width = 52.913385830000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240'-'
            #1056#1029#1056#1109#1057#1027#1057#8218#1057#1034', '
            #1056#1169#1056#8216#1056#1112)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1300.157634100000000000
          Width = 70.315006040000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1113#1056#187#1056#176#1057#1027#1057#1027
            #1056#1105#1056#183#1056#187#1057#1107#1057#8225#1056#181#1056#1029#1056#1105#1057#1039)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1370.472640140000000000
          Width = 62.362204720000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#1109#1057#8218#1056#1109#1056#1108#1056#176', M'#1056#8216#1056#1105#1057#8218'/'#1057#1027)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 162.519790000000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1116#1056#176#1056#183#1056#1030#1056#176#1056#1029#1056#1105#1056#181' ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1141.417476610000000000
          Width = 52.913385830000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218', '#1056#1110#1057#1026#1056#176#1056#1169'.')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxRTFExport1: TfrxRTFExport
    FileName = 'D:\222222222222333233333444222222.rtf'
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    PictureType = gpPNG
    OpenAfterExport = True
    Wysiwyg = True
    Creator = 'FastReport'
    SuppressPageHeadersFooters = False
    HeaderFooterMode = hfText
    AutoSize = False
    Left = 144
    Top = 96
  end
  object frxXLSExport1: TfrxXLSExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    ExportEMF = True
    AsText = False
    Background = True
    FastExport = True
    PageBreaks = True
    EmptyLines = True
    SuppressPageHeadersFooters = False
    Left = 144
    Top = 32
  end
  object frxPDFExport1: TfrxPDFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    OpenAfterExport = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Transparency = False
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = False
    Left = 240
    Top = 32
  end
  object frxDesigner1: TfrxDesigner
    DefaultScriptLanguage = 'PascalScript'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = -13
    DefaultFont.Name = 'Arial'
    DefaultFont.Style = []
    DefaultLeftMargin = 10.000000000000000000
    DefaultRightMargin = 10.000000000000000000
    DefaultTopMargin = 10.000000000000000000
    DefaultBottomMargin = 10.000000000000000000
    DefaultPaperSize = 9
    DefaultOrientation = poPortrait
    TemplatesExt = 'fr3'
    Restrictions = []
    RTLLanguage = False
    MemoParentFont = False
    Left = 240
    Top = 96
  end
end
