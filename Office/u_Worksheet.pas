unit u_Worksheet;

interface
uses SysUtils,ComObj, DB, Dialogs, Variants;

type
  TVariant2DArray = array of array of Variant;
  TString2DArray  = array of array of string;


  TColumn = record
             Name      : string;
             FieldType : TFieldType;
           end;
  TColumnArray = array of TColumn;

  TWorksheet = class
  private
    FCells   : TVariant2DArray;
    FColumns : TColumnArray;
    function GetCellValue   (aRow:integer; aColumnName: string): Variant;
    function GetCellValueInt   (aRow:integer; aColumn: integer): Variant;
    function GetColumnIndex (aColumnName: string): integer;
  public
    RowCount: integer;
    ColCount : integer;
    HeaderRows: integer;
    HeaderRowIndex: integer;

    constructor Create;

    function LoadFromFile (aFileName: string): boolean;
    function GetColumnName (aIndex: integer): string;

    procedure SetupColumns (aColumns: TColumnArray);
    
    property Cells [Row: integer; ColumnName: string]: Variant read GetCellValue;
    property CellsInt [Row: integer; Column: integer]: Variant read GetCellValueInt;
  end;


  function ExcelWorkSheetToArray (aFileName: string): TVariant2DArray;
  function Eq (Value1,Value2: string): boolean;


//==================================================
implementation
//==================================================

constructor TWorksheet.Create;
begin
  inherited;
  HeaderRows:=2;
  HeaderRowIndex:=0;
end;


procedure TWorksheet.SetupColumns (aColumns: TColumnArray);
begin
  FColumns:=aColumns;
end;


function TWorksheet.GetCellValue (aRow:integer; aColumnName: string): Variant;
var i,ind: integer;
begin
  Result:='';

  if (aRow>=0) and (aRow<RowCount) then
  begin
    ind:=GetColumnIndex (aColumnName);
    if ind>=0 then Result:=FCells[aRow+HeaderRows,ind];
  end;
end;


function TWorksheet.GetCellValueInt (aRow:integer; aColumn: integer): Variant;
var i,ind: integer;
begin
  Result:='';

  if (aRow>=0) and (aRow<RowCount) then
  begin
    Result:=FCells[aRow+HeaderRows,aColumn];
  end;
end;

//-----------------------------------------------------------
function TWorksheet.GetColumnName (aIndex: integer): string;
//-----------------------------------------------------------
var i:integer;
begin
  if aIndex> High(FColumns)
  then
    Result := ''
  else
    result := FColumns[aIndex].Name;
end;


//-----------------------------------------------------------------
function TWorksheet.GetColumnIndex (aColumnName: string): integer;
//-----------------------------------------------------------------
var i:integer;
begin
   for i:=0 to High(FColumns) do
     if Eq(FColumns[i].Name, aColumnName) then begin
       Result:=i; Exit; //FColumns[i].ColIndex; Exit;
     end;
   Result:=-1;
end;


//-----------------------------------------------------------
function TWorksheet.LoadFromFile (aFileName: string): boolean;
//-----------------------------------------------------------
var i,j: integer;  str: string;
begin
  Result:=FileExists(aFileName);

  FCells:=ExcelWorkSheetToArray (aFileName);
  // ���������� ������� �������
  // ����� ����� ��������� �� 2 ������ � Excel

  // clear all data
 // for i:=0 to High(FColumns) do FColumns[i].ColIndex:=-1;

  SetLength (FColumns, Length(FCells[1]));
  for i:=0 to High(FColumns) do FColumns[i].Name:=FCells[HeaderRowIndex][i];

  ColCount := High(FColumns)+1;

  if High(FCells)>=2 then
  begin
   {
    for j:=0 to High(FColumns) do
    for i:=0 to High(FCells[1]) do
      if Eq(FCells[1][i], FColumns[j].Name) then begin
        FColumns[j].ColIndex:=i;
        str:=FCells[1][i];
        Break;
      end;
    }
    RowCount:=Length(FCells)-1; // only data rows
  end else
    RowCount:=0;

end;


//--------------------------------------------------------
// function loads Excel worksheet to variant array
//--------------------------------------------------------
function ExcelWorkSheetToArray (aFileName: string): TVariant2DArray;
//--------------------------------------------------------
var Excel : Variant;
    v,WorkSheet : Variant;
    i,j,iColCount,iRowCount : integer;
    arrGrid: TVariant2DArray;
    s: string;
begin
  try
    Excel := CreateOleObject('Excel.Application');
    Excel.Visible := False;
    Excel.Workbooks.Open (aFileName);

    WorkSheet := Excel.Workbooks[1].WorkSheets[1];

    i:=Excel.Workbooks[1].WorkSheets.Count;
    s:=Excel.Workbooks[1].WorkSheets[1].Name;

    showMessage (IntToStr(i));
    showMessage (s);

    iColCount:=WorkSheet.Cells.CurrentRegion.Columns.Count;
    iRowCount:=WorkSheet.Cells.CurrentRegion.Rows.Count;

//    s:=WorkSheet.Cells[7,3];

    SetLength (arrGrid, iRowCount);
    for i:=0 to High(arrGrid) do
      SetLength (arrGrid[i], iColCount);

    for i := 0 to WorkSheet.Cells.CurrentRegion.Rows.Count-1 do
    for j := 0 to WorkSheet.Cells.CurrentRegion.Columns.Count-1 do
       arrGrid [i,j] := VarToStr(WorkSheet.Cells[I+1,J+1]);

    Excel.Workbooks.Close;
    Excel.Quit;
    Result:=arrGrid;
  except
    SetLength(Result,0);
  end;
end;

//--------------------------------------------------------
function Eq (Value1,Value2 : string): boolean;
//--------------------------------------------------------
begin
  Result := AnsiCompareText(Value1,Value2)=0;
end;




end.
