unit u_Excel_new;

interface
uses Windows,SysUtils,ComObj,ActiveX,dxmdaset,Variants,


  u_Func_arrays,
  u_func;

type
//  TStr2DArray = array of TStrArray;


  TExcel = class
  private
    FActiveSheet: OleVariant; // ������ �� �������� ����
    FWorkBook: OleVariant;    // ������ �� �������� �����
    FCells: OleVariant;       // ������ ����������� �����
    FSheetReaded: Boolean;    // �� Excel �������� ���� ����
    FColsCount: Integer;
    FRowsCount: Integer;
    FExcel : OleVariant;

    function GetColumnName(ACol: Cardinal): string;
    function Get_CellValue(ACol, ARow: Cardinal): Variant; overload;
    procedure Set_CellValue(ACol, ARow: Cardinal; const Value: Variant);
  protected
    procedure Update;
  public
    constructor Create(ACreateWorkBook: Boolean=False; const AFileName: String=
      '');
    constructor CreateSimple();
    destructor Destroy; override;

    function GetColumnIndexByName(const AColumnName: String): integer;
    function GetValueByColName(aRow: integer; aColName: string): variant;

    function GetCellValue_(ACol: Cardinal; CONST sColName: string): Variant;


    procedure Show();

    function  OpenBook(const AFileName: String): Boolean;
    function  CloseBook: Boolean;
    function  SaveBook: Boolean;

    function  CreateNewSheet(aSheetName: string): boolean;
    function  SetSheet(aSheetName: string; aCreateSheet: boolean=false): boolean; overload;
    procedure DeleteSheet(aSheetName: string);

    function  GetSheetNameArr(): TStrArray;
    function  ActiveSheetName(): string;

    function  SheetExists(const ASheetName: String): Boolean;
    function  SetSheet(const ASheetNumber: Integer): Boolean; overload;
    function  ReadSheet: Boolean;
    function  WriteSheet(AStartCol, AStartRow: Integer; ATable: TdxMemData; aFromColumnInd: Integer = 1): Boolean;
    function  SetNameSheet(const ASheetName: String): Boolean;

    property Cells[ACol: Cardinal; ARow: Cardinal]: Variant read Get_CellValue write Set_CellValue;
    property ColsCount: Integer read FColsCount;
    property RowsCount: Integer read FRowsCount;

    property ColumnName[ACol: Cardinal]: string read GetColumnName;
    property WorkBook: OleVariant read FWorkBook;
  end;



//==============================================================================
implementation
//==============================================================================
const
  EXCEL_APPLICATION = 'Excel.Application';


//-------------------------------------------------------------------
constructor TExcel.CreateSimple();
//-------------------------------------------------------------------
begin
  try
    FExcel := CreateOleObject(EXCEL_APPLICATION);
  except
    on E: EOLESysError do
      if E.ErrorCode=CO_E_CLASSSTRING then //Excel �� ����������
        Raise;
  end;

  FExcel.Visible:= false;
  //�� ���������� ��������������� ���������
  FExcel.DisplayAlerts:=False;
end;


//------------------------------------------------------------------------------
constructor TExcel.Create(ACreateWorkBook: Boolean=false; const AFileName: String='');
//------------------------------------------------------------------------------
var
  ClassID : TCLSID;
  Unknown : IUnknown;
  excel   : IDispatch;
begin
  ClassID:= ProgIDToClassID (EXCEL_APPLICATION);

  if Succeeded(GetActiveObject(ClassID, nil, Unknown)) and
     Succeeded(Unknown.QueryInterface(IDispatch, excel))
  then
    FExcel:= excel
  else
  begin
    try
      FExcel:= CreateOleObject(EXCEL_APPLICATION);
    except
      on E: EOLESysError do
        if E.ErrorCode=CO_E_CLASSSTRING then //Excel �� ����������
          Raise;
    end;

    FExcel.Visible:= false;
  end;

  try
    //��������� Excel �� ������ �����
    //FExcel.WindowState:=-4137;
    //�� ���������� ��������������� ���������
    FExcel.DisplayAlerts:=False;

    if ACreateWorkBook then
    begin
      FWorkBook:=FExcel.WorkBooks.Add;

      while FWorkBook.WorkSheets.Count>1 do
        FWorkBook.ActiveSheet.Delete;

      Update;
      FActiveSheet.Name:='����1';
      FWorkBook.SaveAs(AFileName)
    end else
      if AFileName <> '' then
        FWorkBook:=FExcel.WorkBooks.Open(AFileName);

    Update;
  except
    Raise;
  end;
end;


//------------------------------------------------------------------------------
destructor TExcel.Destroy;
//------------------------------------------------------------------------------
begin
  try
    //SaveBook;
    CloseBook;
    FExcel.DisplayAlerts:=True;
    if FExcel.WorkBooks.Count=0 then
       FExcel.Quit;
  except end;

  VarClear(FExcel);
  inherited;
end;



//------------------------------------------------------------------------------
function TExcel.GetColumnIndexByName(const AColumnName: String): integer;
//------------------------------------------------------------------------------
var
  i: integer;
begin
  Result:=0;

  for i:=1 to ColsCount do
    if AnsiCompareText(AColumnName, Cells[i,1])=0 then
    begin
      Result:=i;
      Exit;
    end;
end;


//------------------------------------------------------------------------------
function TExcel.GetColumnName(ACol: Cardinal): string;
//------------------------------------------------------------------------------
begin
  Result:= Cells[aCol,1];
end;


//------------------------------------------------------------------------------
function TExcel.GetValueByColName(aRow: integer; aColName: string): variant;
//------------------------------------------------------------------------------
var
  iCol: integer;
  S: String;
begin
  iCol:= GetColumnIndexByName(aColName);
  
  if iCol>0 then  Result:= Cells[iCol,aRow]
            else  Result:= NULL;
end;


//------------------------------------------------------------------------------
function TExcel.OpenBook(const AFileName: String): Boolean;
//------------------------------------------------------------------------------
begin
  try
    FWorkBook:=FExcel.WorkBooks.Open(AFileName);
    Update;
    Result:=True;
  except
    Result:=False;
  end;
end;


//------------------------------------------------------------------------------
function TExcel.CloseBook: Boolean;
//------------------------------------------------------------------------------
begin
  try
    if not VarIsEmpty(FWorkBook) then
      FWorkBook.Close;
    Result:=True;
  except
    Result:=False;
  end;
end;


//------------------------------------------------------------------------------
function TExcel.SaveBook: Boolean;
//------------------------------------------------------------------------------
begin
  try
    FWorkBook.Save;
    Result:=True;
  except
    Result:=False;
  end;
end;


//------------------------------------------------------------------------------
procedure TExcel.Show();
//------------------------------------------------------------------------------
begin
  FExcel.visible:= true;
end;

//------------------------------------------------------------------------------
function TExcel.ReadSheet: Boolean;
//------------------------------------------------------------------------------
begin
  try
    FCells:=FActiveSheet.Range['A1', FActiveSheet.Cells.Item[RowsCount, ColsCount]].Value;
    Result:=True;
  except
    Result:=False;
  end;
end;


//------------------------------------------------------------------------------
function TExcel.CreateNewSheet(aSheetName: string): boolean;           //andrey
//------------------------------------------------------------------------------
begin
  try
    FActiveSheet:= FWorkBook.Sheets.Add (After:=FActiveSheet);
    FActiveSheet.Name:=aSheetName;
    FActiveSheet.Activate;

    FRowsCount:=0;
    FColsCount:=0;

    Result:=True;
  except
    Result:=False;
  end;
end;


//------------------------------------------------------------------------------
function TExcel.SetSheet(aSheetName: string; aCreateSheet: boolean=false): boolean; //andrey
//------------------------------------------------------------------------------
var i: integer;
begin
  if VarIsEmpty(FWorkBook) then
    exit;

  for i:=1 to FWorkBook.WorkSheets.Count do
    if Eq(FWorkBook.Sheets.Item[i].Name, aSheetName) then
    begin
      FWorkBook.Sheets.Item[i].Activate;
      Update;
      Result:=true;
      exit;
    end;

  if aCreateSheet then  Result:=CreateNewSheet(aSheetName)
                  else  Result:=false;
end;

//------------------------------------------------------------------------------
function  TExcel.GetSheetNameArr(): TStrArray;
//------------------------------------------------------------------------------
var i: Integer;
begin
  SetLength(Result, 0);

  if VarIsEmpty(FWorkBook) then
    exit;

  for i:=1 to FWorkBook.WorkSheets.Count do
    StrArrayAddValue(Result, FWorkBook.Sheets.Item[i].Name);
end;

//------------------------------------------------------------------------------
function  TExcel.ActiveSheetName(): string;
//------------------------------------------------------------------------------
begin
  Result:= FWorkBook.ActiveSheet.Name;
end;

//------------------------------------------------------------------------------
procedure TExcel.DeleteSheet(aSheetName: string);
//------------------------------------------------------------------------------
var i: integer;
begin
  for i:=1 to FWorkBook.WorkSheets.Count do
    if Eq(FWorkBook.Sheets.Item[i].Name, aSheetName) then
    begin
      FWorkBook.Sheets.Item[i].Delete;   //FWorkBook.ActiveSheet.Delete;
      Update;
      exit;
    end;          
end;


//------------------------------------------------------------------------------
function TExcel.GetCellValue_(ACol: Cardinal; CONST sColName: string): Variant;
//------------------------------------------------------------------------------
var iIndex: Integer;
begin
  iIndex:= GetColumnIndexByName(sColName);
  if iIndex>0 then
     Result:=FCells[ACol, iIndex];
end;


//------------------------------------------------------------------------------
function TExcel.Get_CellValue(ACol, ARow: Cardinal): Variant;
//------------------------------------------------------------------------------
begin
  if (ARow<=FRowsCount) and (ACol<=FColsCount) then
    Result:=FCells[ARow, ACol];
end;


//------------------------------------------------------------------------------
procedure TExcel.Set_CellValue(ACol, ARow: Cardinal; const Value: Variant);
//------------------------------------------------------------------------------
begin
//  if (ARow<=FRowsCount) and (ACol<=FColsCount) then
    FActiveSheet.Cells[ARow, ACol].Value:=Value;
end;


//------------------------------------------------------------------------------
procedure TExcel.Update;
//------------------------------------------------------------------------------
begin
  FActiveSheet:=FExcel.ActiveSheet;
  FColsCount:=FActiveSheet.UsedRange.Columns.Count;
  FRowsCount:=FActiveSheet.UsedRange.Rows.Count;
end;


//------------------------------------------------------------------------------
function TExcel.WriteSheet(AStartCol, AStartRow: Integer; ATable: TdxMemData;
      aFromColumnInd: Integer = 1): Boolean;
//------------------------------------------------------------------------------
var
  cells, cellLeftTop, cellRightBottom: OleVariant;
  rowCount, colCount, curRec: Integer;
begin
  Result:=False;

  try
    if (ATable.RecordCount=0) or (ATable.FieldCount=1) then  Exit;

    ATable.DisableControls;
    curRec:= ATable.RecNo;

    //� ����� ���� ���� id � �������� 0, ��� ��� �e �����
    cells:= VarArrayCreate([1, ATable.RecordCount, 1, ATable.FieldCount-(aFromColumnInd-1)-1], varVariant);

    ATable.First;

    for rowCount:=1 to ATable.RecordCount do
    begin
      for colCount:=1 to ATable.FieldCount-(aFromColumnInd-1)-1 do
        cells[rowCount, colCount]:=ATable.Fields.Fields[aFromColumnInd-1+colCount].Value;

      ATable.Next;
    end;

    //������� ����� ������� ������
    cellLeftTop:=FActiveSheet.Cells[AStartRow, AStartCol];

    //������� ������ ������ ������
    cellRightBottom:=FActiveSheet.Cells[AStartRow+ATable.RecordCount-1,
                     AStartCol+ATable.FieldCount-(aFromColumnInd-1)-2];

    //������� ������ cells � �������, ����������� ����� ��������
    FActiveSheet.Range[cellLeftTop, cellRightBottom].Value:= cells;

    ATable.RecNo:=curRec;
    ATable.EnableControls;
    VarClear(cells);

    FColsCount:=FActiveSheet.UsedRange.Columns.Count;
    FRowsCount:=FActiveSheet.UsedRange.Rows.Count;

    Result:=True;
  except
    Result:=False;
  end;
end;


//------------------------------------------------------------------------------
function TExcel.SetNameSheet(const ASheetName: String): Boolean;
//------------------------------------------------------------------------------
begin
  try
    FActiveSheet.Name:=ASheetName;
    Result:=True;
  except
    Result:=False;
  end;
end;

//------------------------------------------------------------------------------
function TExcel.SetSheet(const ASheetNumber: Integer): Boolean;
//------------------------------------------------------------------------------
begin
  Result:=False;

  try
    FExcel.WorkSheets[ASheetNumber].Activate;
    Update;
    Result:=True;
  except end;
end;


//------------------------------------------------------------------------------
function TExcel.SheetExists(const ASheetName: String): Boolean;
//------------------------------------------------------------------------------
var i: Integer;
begin
  Result:=False;

  try
    for i:=1 to FExcel.WorkSheets.Count do
      if Eq(FExcel.WorkSheets[i].Name, ASheetName) then
        Result:=True;
  except end;
end;


end.


