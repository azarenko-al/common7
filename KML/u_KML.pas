unit u_KML;

interface

uses Classes, SysUtils,

  u_geo_convert_new,
             
//  u_geo_poly,

   //  u_Geo_new,
     u_Geo;


type
  TKML = class(TStringList)
  private
    FPolyList :  TStringList;

   // FStrList: TStringList;
    FStyleUrl:string;
    FPlacemarkNumber: integer;
    procedure AddStyleMap(aStyleID: string);
//    procedure AddStyle_old(aColor, aStyleID: string);

    procedure AddPlacemark_Polygon(var aCoords_Pulkovo: TBLPointArrayF; aID_style:
        Integer);


  public
    constructor Create;
    destructor Destroy; override;

    procedure AddFooter;
    procedure AddHeader;

    procedure AddPlacemark_Polygon_(aCoords_Pulkovo: TBLPointArray; aID_style:
        Integer);


    procedure AddStyle1(aColor, aTransparence: Integer);

    procedure Clear;

    procedure SaveToFile(aFileName: string); 

  end;


implementation
const
  CRLF = #13+#10;



constructor TKML.Create;
begin
  inherited Create;
  FPolyList := TStringList.Create();
end;


destructor TKML.Destroy;
begin
  FreeAndNil(FPolyList);

  inherited Destroy;
end;


procedure TKML.AddFooter;
const
  DEF_STR_END =
  '</Document>' + CRLF +
  '</kml>';
begin
  // FStrList.
   Add(DEF_STR_END);
end;



// ---------------------------------------------------------------
procedure TKML.AddStyle1(aColor, aTransparence: Integer);
// ---------------------------------------------------------------
const
   DEF_STR_STYLE =
   '  <Style id="aid_' + '%s' + '">' + CRLF +
   '    <LineStyle>' + CRLF +
	 '		     <color>' + '%s' + '</color>' + CRLF +
	 '    </LineStyle>' + CRLF +

   '    <PolyStyle> ' + CRLF +
   '		     <color>' + '%s' + '</color>' + CRLF +
   '    </PolyStyle>' + CRLF +
   '  </Style>';


   DEF_STR_STYLEMAP =
   '  <StyleMap id="id_' + '%s' + '">' + CRLF +
   '    <Pair>' + CRLF +
   '   			<key>normal</key>' + CRLF +
   '        <styleUrl>#aid_' + '%s' + '</styleUrl>' + CRLF +
   '    </Pair>' + CRLF +
   '  </StyleMap>' + CRLF  + CRLF ;


  //ff00aaff;
  //ff0000ff;


var
  sPolyColor: string;
  sStyleID: string;
begin
  sStyleID := IntToStr(aColor);
  sPolyColor:= IntToHex(aTransparence, 2) + IntToHex(aColor, 6);


  Add(Format(DEF_STR_STYLE, [sStyleID, sPolyColor, sPolyColor]));

  Add(Format(DEF_STR_STYLEMAP, [sStyleID, sStyleID]));

end;



// ---------------------------------------------------------------
procedure TKML.AddStyleMap(aStyleID: string);
// ---------------------------------------------------------------
const
   DEF_STR_STYLEMAP =
   '  <StyleMap id="id_' + '%s' + '">' + CRLF +
   '    <Pair>' + CRLF +
   '        <styleUrl>#id_' + '%s' + '</styleUrl>' + CRLF +
   '    </Pair>' + CRLF +
   '  </StyleMap>';
begin
 // FStrList.
  Add(Format(DEF_STR_STYLEMAP, [aStyleID,aStyleID]));
end;


procedure TKML.AddHeader;
const
  DEF_STR = '<?xml version="1.0" encoding="UTF-8"?>' + CRLF +
            '<kml xmlns="http://earth.google.com/kml/2.2">' + CRLF +
            '<Document>' + CRLF +
            '  <name>poly.kml</name>';
begin
 // FStrList.
  Add(DEF_STR);
  
  FPlacemarkNumber := 0;
end;

// ---------------------------------------------------------------
procedure TKML.AddPlacemark_Polygon(var aCoords_Pulkovo: TBLPointArrayF;
    aID_style: Integer);
// ---------------------------------------------------------------
var
  arrCoords_Pulkovo: TBLPointArray;
begin
  arrCoords_Pulkovo:=geo_BLPointArrayF_to_BLPointArray(aCoords_Pulkovo);

  AddPlacemark_Polygon_(arrCoords_Pulkovo, aID_style);

end;

// ---------------------------------------------------------------
procedure TKML.AddPlacemark_Polygon_(aCoords_Pulkovo: TBLPointArray; aID_style:
    Integer);
// ---------------------------------------------------------------

const
  DEF_STR_PLACEMARK =
  ' <Placemark>'                    + CRLF +
  '     <name>%d</name>'            + CRLF +
  '     <styleUrl>#id_' + '%s' + '</styleUrl>' + CRLF +
  '     <Polygon> '                 + CRLF +
  '         <outerBoundaryIs>'      + CRLF +
  '             <LinearRing>'       + CRLF  +
  '                 <coordinates>'  + CRLF  +
  '                    %s '         + CRLF  +
  '                 </coordinates>' + CRLF +
  '             </LinearRing>'      + CRLF +
  '         </outerBoundaryIs>'     + CRLF +
  '     </Polygon>'                 + CRLF +
  ' </Placemark>';
 //'         <altitudeMode>relativeToGround</altitudeMode>' + CRLF  +
var
  i:integer;
  s: string;

  blPosWgs: TBlPoint;

begin
  inc(FPlacemarkNumber);
  s:='';

  //if FPlacemarkNumber>2 then
  //  Exit;

//  u_geo_convert_new,


  for i := 0 to High(aCoords_Pulkovo) do
  begin
    blPosWgs:= geo_Pulkovo42_to_WGS84(aCoords_Pulkovo[i]);

//    s := s + Format('%1.15f,%1.15f,0', [blPosWgs.L, blPosWgs.B]);
    s := s + Format('%1.8f,%1.8f,0', [blPosWgs.L, blPosWgs.B]);

    if i <> High(aCoords_Pulkovo) then
      s := s  + ' '
(*
    if (i = 0) then
      s := s + ' ' + Format('%1.8f, %1.8f', [blPosWgs.L, blPosWgs.B]);

    if i = aCoords_Pulkovo.Count-1 then
      s := s  + Format(',0 %1.8f, %1.8f,0', [blPosWgs.L, blPosWgs.B])
    else
      if (i <> 0) then
        s := s + Format(',0 %1.8f, %1.8f', [blPosWgs.L, blPosWgs.B]);
*)

  end;

  FPolyList.AddObject(s, Pointer( aID_style));

  s:=Format(DEF_STR_PLACEMARK, [FPlacemarkNumber, IntToStr(aID_style), s]);
 // FStrList.
  Add(s);

end;


procedure TKML.SaveToFile(aFileName: string);
begin
 // DeleteFile(aFileName);
 // FstrList.
  aFileName := ChangeFileExt(aFileName, '.kml');

  Inherited SaveToFile(aFileName);
end;


procedure TKML.Clear;
begin
  FPolyList.Clear;

  inherited Clear;

  FPlacemarkNumber:= 0;
end;


end.


{


// ---------------------------------------------------------------
procedure TKML.AddStyle_old(aColor, aStyleID: string);
// ---------------------------------------------------------------
const
   DEF_STR_STYLE =
   '  <Style id="sn_ylw-pushpin_copy_' + '%s' + '">' + CRLF +
//   '    <LineStyle>' + CRLF +
//	 '		     <color>' + '%s' + '</color>' + CRLF +
//	 '		     <color>' + '%x' + '</color>' + CRLF +
//	 '    </LineStyle>' + CRLF +
   '    <PolyStyle> ' + CRLF +
   '		     <color>' + '%s' + '</color>' + CRLF +
   '    </PolyStyle>' + CRLF +
   '  </Style>';

  //ff00aaff;
  //ff0000ff;

//var
//  s: string;

begin
//  s:=

 // FStrList.
  Add(Format(DEF_STR_STYLE, [aStyleID, aColor]));

end;


}
