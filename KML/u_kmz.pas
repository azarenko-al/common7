unit u_KMZ;

interface

uses Classes, SysUtils, Graphics,

     dm_Progress,

     unicodefile,
     u_classes,
     KAZip,
     u_func_files,
     u_func_img,
     u_Geo
     ;


type
  TKMZCreateRec = record
    FolderName,
    Name_,
    BMPFileName: string;
    BL42_BottomLeft,
    BL42_BottomRight,
    BL42_TopRight,
    BL42_TopLeft: TBLPoint;
  end;

  TKMZCreateRecArr = record
    KmzFileName: string;
    Files: array of TKMZCreateRec;
  end;


  TKMZ = class(TObject)
  private
    FStrList: TUnicodeFile;
    FKAZip: TKAZip;

    procedure AddHeader;
    procedure SaveToFile(aFileName: string);

    procedure AddPic_Box(aName, aBMPFileName, aKMZFileName: string; aTransparence: Integer;
                     const aTopLeftPoint_SK42, aBottomRightPoint_SK42: TBLPoint
                     ); overload;
    procedure AddPic(aName, aBMPFileName, aKMZFileName: string; aTransparence: Integer;
                     const aBL42_BottomLeft, aBL42_BottomRight,
                           aBL42_TopRight, aBL42_TopLeft: TBLPoint); overload;
    procedure Clear;

  public
    constructor Create;
    destructor Destroy; override;

    //--------------
    procedure AddPicNew(aRec: TKMZCreateRec; aKMZFileName: string;
                          aTransparence: Integer);

    procedure CreateFiles(aArr: TKMZCreateRecArr; aTransparence: Integer;
                          aOnProgress: TOnDMProgressEvent);
    //--------------

    // aName - ��� ����� � ������ Google Earth
    // aKMZFilename - �������� ����
    // aBMPFileName - �������� �������� � ������� BMP,
    //                ����� ������������� � PNG ��� ����������� ������������
    // aTransparence - ������� ������������ � ��������� 0..255 (0 - ��������� ���������)
    procedure CreateFile_forBox(aName, aKMZFileName, aBMPFileName : string;
                                aTransparence: Integer;
                                const aTopLeftPoint_SK42, aBottomRightPoint_SK42: TBLPoint);

    // TBLPoint -
    //   ��������� ���������� ������ ����������������, ������������� ���� ���������.
    //   ���������� ������� ��������� ������ ������� �������,
    //   ������� � ������� ������ ���� ����������� �����������.
    procedure CreateFile(aName, aKMZFileName, aBMPFileName : string;
                         aTransparence: Integer;
                         const aBL42_BottomLeft, aBL42_BottomRight,
                               aBL42_TopRight, aBL42_TopLeft: TBLPoint); overload;

    // 4-� ���������� ����������� �������������
    procedure CreateFile(aName, aKMZFileName, aBMPFileName : string;
                         aTransparence: Integer;
                         const aBL42_TopLeft, aBL42_BottomRight: TBLPoint); overload;

  end;

implementation

const
  CRLF = #13+#10;
  PNG_FILE = 'attachment.png';

//----------------------------------------------------------------------------
constructor TKMZ.Create;
//----------------------------------------------------------------------------
begin

  inherited Create;

  FKAZip:= TKAZip.Create(nil);
  FStrList := TUnicodeFile.Create();
end;

//----------------------------------------------------------------------------
destructor TKMZ.Destroy;
//----------------------------------------------------------------------------
begin
  FStrList.Free;
  FKAZip.Free;

  inherited Destroy;
end;

//----------------------------------------------------------------------------
procedure TKMZ.CreateFiles(aArr: TKMZCreateRecArr; aTransparence: Integer;
                           aOnProgress: TOnDMProgressEvent);
//----------------------------------------------------------------------------
var
  i: Integer;
  s, sKMLFileName, sFolder, sPNGFileName: string;
  b: boolean;
begin
  FStrList.Clear;

  AddHeader;
  FStrList.Add('<Document>');

  sFolder:= '-1zdvdC';
  for i := 0 to High(aArr.Files) do
    with aArr.Files[i] do
  begin
    if Assigned(aOnProgress) then
      aOnProgress(i, High(aArr.Files), b);

    if sFolder <> FolderName then begin
      if i<>0 then
        FStrList.Add(' </Folder>');

      FStrList.Add(' <Folder>');
      FStrList.Add(Format('  <name>%s</name>', [FolderName]));
      sFolder:= FolderName;
    end;

    FStrList.Add('   <Folder>');
    FStrList.Add(Format('     <name>%s</name>', [Name_]));

    AddPicNew( aArr.Files[i], aArr.KmzFileName, aTransparence );
//    DeleteFile( aArr.Files[i].BMPFileName );

    FStrList.Add('   </Folder>');
  end;

  FStrList.Add(' </Folder>');
  FStrList.Add('</Document>');
  FStrList.Add('</kml>'); //�������� ��������� ����

  sKMLFileName:= ExtractFilePath(aArr.KmzFileName)+'doc.kml';

  SaveToFile(sKMLFileName);

  s:= ExtractFileDir(ExcludeTrailingBackslash(aArr.KmzFileName));

  FKAZip.CreateZip(aArr.KmzFileName);
  FKAZip.Open(aArr.KmzFileName);
  // ������ ���������� - ��� � ���� ������ ������
  FKAZip.AddFile(sKMLFileName, ExtractFileName(sKMLFileName));         // 'doc.kml');

  for i := 0 to High(aArr.Files) do
    with aArr.Files[i] do
  begin
    if Assigned(aOnProgress) then
      aOnProgress(i, High(aArr.Files), b);

    sPNGFileName:= s+'\files\'+ ExtractFileNameNoExt(BMPFileName)+'.png';
    FKAZip.AddFile(sPNGFileName, 'files\'+ExtractFileName(sPNGFileName));//'files\kup.png');
  end;

  FKAZip.Close;

  DeleteFile(sKMLFileName);
//  DeleteFile(sPNGFileName);
end;

//----------------------------------------------------------------------------
procedure TKMZ.AddPicNew(aRec: TKMZCreateRec; aKMZFileName: string; aTransparence: Integer);
//----------------------------------------------------------------------------
//------------------<gx:LatLonQuad>---------------------------
//��������� ���������� ������ ����������������, ������������� ���� ���������.
//������ ���� ����� ������ ����� ���������, ������ �� ������� ������� ��
//�������� � ��������� ������� ��� ������� � ������.
//���������� ������ ����� �����������.
//�� ��������� ������� � �������� ���������.
//���������� ������� ��������� ������ ������� �������,
//������� � ������� ������ ���� ����������� �����������.
const
  DEF_STR_GroundOverlay =
'         <GroundOverlay>' + CRLF +
'             <name>%s</name>' + CRLF +
'             <color>%xffffff</color> ' + CRLF +
'             <Icon> ' + CRLF +
'                 <href>%s</href> ' + CRLF +
'                 <viewBoundScale>%d</viewBoundScale>' + CRLF  +
'             </Icon> ' + CRLF  +
'             <gx:LatLonQuad>' + CRLF  +
'               <coordinates>' + CRLF  +
'                 %3.8f,%3.8f %3.8f,%3.8f %3.8f,%3.8f %3.8f,%3.8f' + CRLF  +
'               </coordinates>' + CRLF  +
'             </gx:LatLonQuad>' + CRLF  +
'         </GroundOverlay>';

var i:integer;
  s , sPNGFileName, sPNGFilePath: string;
  blPoint1, blPoint2, blPoint3, blPoint4: TBlPoint;
begin
//  s:= ExtractFileDir(ExcludeTrailingBackslash(aRec.BMPFileName));
//  sPNGFileName:= ExtractFileNameNoExt(aRec.BMPFileName)+'.png';
//  sPNGFilePath:=  IncludeTrailingBackslash(s)+'files\'+sPNGFileName;
//  ForceDirectories(IncludeTrailingBackslash(s)+'files\');
  s:= ExtractFileDir(ExcludeTrailingBackslash(aKMZFileName));
  sPNGFileName:= ExtractFileNameNoExt(aRec.BMPFileName)+'.png';
  sPNGFilePath:=  s+'\files\'+sPNGFileName;
  ForceDirectories(s+'\files\');

  if FileExists(aRec.BMPFileName) then
    img_BMP_to_PNG(PChar(aRec.BMPFileName), PChar(sPNGFilePath), true, clWhite);

  blPoint1 := geo_BL_to_BL(aRec.BL42_BottomLeft, EK_KRASOVSKY42, EK_WGS_84);
  blPoint2 := geo_BL_to_BL(aRec.BL42_BottomRight,EK_KRASOVSKY42, EK_WGS_84);
  blPoint3 := geo_BL_to_BL(aRec.BL42_TopRight,   EK_KRASOVSKY42, EK_WGS_84);
  blPoint4 := geo_BL_to_BL(aRec.BL42_TopLeft,    EK_KRASOVSKY42, EK_WGS_84);

  FStrList.Add(Format(DEF_STR_GroundOverlay,
                      [aRec.Name_, aTransparence, 'files/'+sPNGFileName, 1,
                       blPoint1.L, blPoint1.B,
                       blPoint2.L, blPoint2.B, 
                       blPoint3.L, blPoint3.B, 
                       blPoint4.L, blPoint4.B
                       ]));

end;

//----------------------------------------------------------------------------
procedure TKMZ.CreateFile_forBox(aName, aKMZFilename, aBMPFileName : string; aTransparence: Integer;
                             const aTopLeftPoint_SK42, aBottomRightPoint_SK42: TBLPoint);
//----------------------------------------------------------------------------
var sKMLFileName : string;
    s , sPNGFileName: string;
begin
  FStrList.Clear;

  try
    AddHeader;
    AddPic_Box(aName, aBMPFileName, aKMZFilename, aTransparence, aTopLeftPoint_SK42, aBottomRightPoint_SK42);
    //�������� ��������� ����
    FStrList.Add('</kml>');

    sKMLFileName:= ExtractFilePath(aKMZFilename)+'doc.kml';

    SaveToFile(sKMLFileName);

    // ��� ���������� ������� � �������� ������� ������ - ������������ ���������
    s:= ExtractFileDir(ExcludeTrailingBackslash(aKMZFileName));
    sPNGFileName:= s+'\files\'+PNG_FILE; //ExtractFileNameNoExt(aBMPFileName)+'.png';

    FKAZip.CreateZip(aKMZFilename);
    FKAZip.Open(aKMZFilename);
    // ������ ���������� - ��� � ���� ������ ������
    FKAZip.AddFile(sKMLFileName, ExtractFileName(sKMLFileName));         // 'doc.kml');
    FKAZip.AddFile(sPNGFileName, 'files\'+ExtractFileName(sPNGFileName));//'files\kup.png');
    FKAZip.Close;

    DeleteFile(sKMLFileName);
    DeleteFile(sPNGFileName);

  except
  end;
end;    //

//----------------------------------------------------------------------------
procedure TKMZ.CreateFile(aName, aKMZFileName, aBMPFileName : string; aTransparence: Integer;
     const aBL42_BottomLeft, aBL42_BottomRight, aBL42_TopRight, aBL42_TopLeft: TBLPoint);
//----------------------------------------------------------------------------
var sKMLFileName : string;
    s , sPNGFileName: string;
begin
  FStrList.Clear;

  try
    AddHeader;
    AddPic(aName, aBMPFileName, aKMZFilename,aTransparence,
           aBL42_BottomLeft, aBL42_BottomRight, aBL42_TopRight, aBL42_TopLeft);
    //�������� ��������� ����
    FStrList.Add('</kml>');

    sKMLFileName:= ExtractFilePath(aKMZFilename)+'doc.kml';

    SaveToFile(sKMLFileName);

    // ��� ���������� ������� � �������� ������� ������ - ������������ ���������
    s:= ExtractFileDir(ExcludeTrailingBackslash(aKMZFileName));
    sPNGFileName:= s+'\files\'+PNG_FILE; //ExtractFileNameNoExt(aBMPFileName)+'.png';

    FKAZip.CreateZip(aKMZFilename);
    FKAZip.Open(aKMZFilename);
    // ������ ���������� - ��� � ���� ������ ������
    FKAZip.AddFile(sKMLFileName, ExtractFileName(sKMLFileName));         // 'doc.kml');
    FKAZip.AddFile(sPNGFileName, 'files\'+ExtractFileName(sPNGFileName));//'files\kup.png');
    FKAZip.Close;

    DeleteFile(sKMLFileName);
    DeleteFile(sPNGFileName);
  except
  end;

end;    //

//----------------------------------------------------------------------------
procedure TKMZ.CreateFile(aName, aKMZFileName, aBMPFileName : string; aTransparence: Integer;
                               const aBL42_TopLeft, aBL42_BottomRight : TBLPoint);
//----------------------------------------------------------------------------
var bl_BottomLeft, bl_BottomRight, bl_TopRight, bl_TopLeft: TBLPoint;
begin
  bl_BottomLeft.B := aBL42_BottomRight.B; bl_BottomLeft.L:= aBL42_TopLeft.L;
  bl_BottomRight  := aBL42_BottomRight;
  bl_TopRight.B   := aBL42_TopLeft.B;     bl_TopRight.L:= aBL42_BottomRight.L;
  bl_TopLeft      := aBL42_TopLeft;

  CreateFile(aName, aKMZFileName, aBMPFileName, aTransparence, bl_BottomLeft, bl_BottomRight, bl_TopRight, bl_TopLeft);
end;    //

//----------------------------------------------------------------------------
procedure TKMZ.AddHeader;
//----------------------------------------------------------------------------
const
  DEF_STR = '<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom"> ';
begin
   FStrList.Add('<?xml version="1.0" encoding="UTF-8"?>');
   FStrList.Add(DEF_STR);
end;

//----------------------------------------------------------------------------
procedure TKMZ.AddPic_Box(aName, aBMPFileName, aKMZFileName: string; aTransparence: Integer;
                       const aTopLeftPoint_SK42, aBottomRightPoint_SK42: TBLPoint);
//----------------------------------------------------------------------------
const
  DEF_STR_GroundOverlay =
' <GroundOverlay>' + CRLF +
'     <name>%s</name>' + CRLF +
'     <color>%xffffff</color> ' + CRLF +
'     <Icon> ' + CRLF +
'         <href>%s</href> ' + CRLF +
'         <viewBoundScale>%d</viewBoundScale>' + CRLF  +
'     </Icon> ' + CRLF  +
'     <LatLonBox>'  + CRLF  +
'         <north>%3.8f</north>' + CRLF +
'         <south>%3.8f</south>' + CRLF +
'         <east>%3.8f</east>' + CRLF +
'         <west>%3.8f</west>' + CRLF +
'     </LatLonBox>' + CRLF +
' </GroundOverlay>';

var i:integer;
  s , sPNGFileName, sPNGFilePath: string;
  blTopLeftPoint, blBottomRightPoint: TBlPoint;
begin
  s:= ExtractFileDir(ExcludeTrailingBackslash(aKMZFileName));
  sPNGFileName:= PNG_FILE; //ExtractFileNameNoExt(aBMPFileName)+'.png';
  sPNGFilePath:=  s+'\files\'+sPNGFileName;
  ForceDirectories(s+'\files\');

  img_BMP_to_PNG(aBMPFileName, sPNGFilePath, True, clWhite);

  blTopLeftPoint := geo_BL_to_BL(aTopLeftPoint_SK42, EK_KRASOVSKY42, EK_WGS_84);
  blBottomRightPoint:= geo_BL_to_BL(aBottomRightPoint_SK42, EK_KRASOVSKY42, EK_WGS_84);

  FStrList.Add(Format(DEF_STR_GroundOverlay,
                      [aName, aTransparence,
                       'files/'+sPNGFileName, 1,
                       blTopLeftPoint.B,
                       blBottomRightPoint.B,
                       blTopLeftPoint.L,
                       blBottomRightPoint.L]));

end;

//----------------------------------------------------------------------------
procedure TKMZ.AddPic(aName, aBMPFileName, aKMZFileName: string; aTransparence: Integer;
      const aBL42_BottomLeft, aBL42_BottomRight, aBL42_TopRight, aBL42_TopLeft: TBLPoint);
//----------------------------------------------------------------------------
//------------------<gx:LatLonQuad>---------------------------
//��������� ���������� ������ ����������������, ������������� ���� ���������.
//������ ���� ����� ������ ����� ���������, ������ �� ������� ������� ��
//�������� � ��������� ������� ��� ������� � ������.
//���������� ������ ����� �����������.
//�� ��������� ������� � �������� ���������.
//���������� ������� ��������� ������ ������� �������,
//������� � ������� ������ ���� ����������� �����������.
const
  DEF_STR_GroundOverlay =
' <GroundOverlay>' + CRLF +
'     <name>%s</name>' + CRLF +
'     <color>%xffffff</color> ' + CRLF +
'     <Icon> ' + CRLF +
'         <href>%s</href> ' + CRLF +
'         <viewBoundScale>%d</viewBoundScale>' + CRLF  +
'     </Icon> ' + CRLF  +
'     <gx:LatLonQuad>' + CRLF  +
'       <coordinates>' + CRLF  +
'         %3.8f,%3.8f %3.8f,%3.8f %3.8f,%3.8f %3.8f,%3.8f' + CRLF  +
'       </coordinates>' + CRLF  +
'     </gx:LatLonQuad>' + CRLF  +
' </GroundOverlay>';

var i:integer;
  s , sPNGFileName, sPNGFilePath: string;
  blPoint1, blPoint2, blPoint3, blPoint4: TBlPoint;
begin
  s:= ExtractFileDir(ExcludeTrailingBackslash(aKMZFileName));
  sPNGFileName:= PNG_FILE; //ExtractFileNameNoExt(aBMPFileName)+'.png';
  sPNGFilePath:=  s+'\files\'+sPNGFileName;
  ForceDirectories(s+'\files\');

  img_BMP_to_PNG(aBMPFileName, sPNGFilePath, True, clWhite);

  blPoint1 := geo_BL_to_BL(aBL42_BottomLeft, EK_KRASOVSKY42, EK_WGS_84);
  blPoint2 := geo_BL_to_BL(aBL42_BottomRight,EK_KRASOVSKY42, EK_WGS_84);
  blPoint3 := geo_BL_to_BL(aBL42_TopRight,   EK_KRASOVSKY42, EK_WGS_84);
  blPoint4 := geo_BL_to_BL(aBL42_TopLeft,    EK_KRASOVSKY42, EK_WGS_84);

  FStrList.Add(Format(DEF_STR_GroundOverlay,
                      [aName,
                       aTransparence, //'6effffff',
                       'files/'+sPNGFileName, 1,
                       blPoint1.L, blPoint1.B,
                       blPoint2.L, blPoint2.B,
                       blPoint3.L, blPoint3.B,
                       blPoint4.L, blPoint4.B
                       ]));

end;


//----------------------------------------------------------------------------
procedure TKMZ.SaveToFile(aFileName: string);
//----------------------------------------------------------------------------
begin
  DeleteFile(aFileName);
  FstrList.SaveToUnicodeFile(aFileName);
end;

//----------------------------------------------------------------------------
procedure TKMZ.Clear;
//----------------------------------------------------------------------------
begin
  FstrList.Clear;
//  Fnum:= 0;
end;

end.
