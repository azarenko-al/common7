unit u_geo_convert_2017;

interface

uses Math, SysUtils, IniFiles, Classes, Dialogs,

//u_geo_types,

//  u_geo_convert,

  u_dll_geo_convert,

  u_geo_convert_new,

  u_neva_dm_bl,
  u_GEO;



//  function geo_Coord_Convert (aBLPoint: TBLPoint): TBLPoint;

  function geo_BL_to_BL_2017_(aBLPoint: TBLPoint; aDatumSrc, aDatumDest: word):
      TBLPoint;




implementation

// ---------------------------------------------------------------
function geo_BL_to_BL_2017_(aBLPoint: TBLPoint; aDatumSrc, aDatumDest: word): TBLPoint;
// ---------------------------------------------------------------
var
  bl_42: TBLPoint;
 // bl: TBLPoint;
//  k: Integer;

begin
//  ShowMessage( IntToStr(aDatumSrc) + ' ' +IntToStr(aDatumDest)  );

  if aDatumSrc = aDatumDest then
  begin
    result:=aBLPoint;
    Exit;
  end;


  case aDatumSrc of
    EK_CK_42:     bl_42:=aBLPoint;
    EK_CK_95:     bl_42:=geo_Pulkovo_CK_95_to_CK_42(aBLPoint);
    EK_WGS_84:    bl_42:=geo_WGS84_to_Pulkovo42(aBLPoint);
    EK_GCK_2011:  GSK_2011_to_CK42(aBLPoint.B, aBLPoint.L,   bl_42.B, bl_42.L);

  else
     raise EClassNotFound.Create('');
  end;


  case aDatumDest of
    EK_CK_42:     result:=bl_42;
    EK_CK_95:     result:=geo_Pulkovo42_to_CK_95(bl_42);
    EK_WGS_84:    //begin
                    result:=geo_Pulkovo42_to_WGS84(bl_42);
                  //  bl:= geo_WGS84_to_Pulkovo42(result);
                   // k:=0;
                 // end;

    EK_GCK_2011:  CK42_to_GSK_2011(bl_42.B, bl_42.L,  result.B, result.L);

  else
     raise EClassNotFound.Create('');
  end;



end;


end.

