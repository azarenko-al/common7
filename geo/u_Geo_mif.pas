unit u_Geo_mif;

interface
uses SysUtils, Classes,

u_geo_convert_new,

  u_Geo ;

type
//  TCoordSysType = (csWGS84, csPulkovo42);
  TCoordSysType = (csPulkovo42);
  
  TProjectionType = (ptUTM, ptGaussKruger);


  TTabRasterHeaderRec = record
    Projection : TProjectionType;
    CoordSys   : TCoordSysType;

    ZoneNum    : integer; //GK
  end;


  TMifHeaderInfoRec = record
    FileName:       string;   // MIF
    BitmapFileName: string;

   // Is_Transparent: boolean;

    Transparence_0_255 : Integer;


//    ImgRowCount: integer;
//    ImgColCount: integer;

    RowCount   : integer;
    ColCount   : integer;


    BL: record
      BLBounds: TBLRect;
      StepB,StepL: double;
    end;

    XYRect: TXYRect;

    XY: record
      TopLeft    : TXYPoint;
      StepX,StepY: double;
{
      RowCount   : integer;
      ColCount   : integer;
}
      ZoneNum    : integer; //GK
    end;

    //new
    Projection : TProjectionType;
    CoordSys   : TCoordSysType;

  end;


  TMapinfoRasterTab1 = class(TObject)
  private
//    FPointCount : Integer;

    xyPoints : array[0..3] of TXYPoint;
    blPoints : array[0..3] of TBLPoint;

    bmpPoints: array[0..3] of  record x,y: integer end;

 {   ImageWidth : Integer;
    ImageHeight : Integer;

    RowCount   : integer;
    ColCount   : integer;

}

(*
    FPointArr : array[0..3] of record
                                 xy: TXYPoint;
                                 bl: TBLPoint;

                                 bmp_x,bmp_y : Integer;
                               end;*)

    function GetCoordSysStr_CK42: string;
    function GetCoordSysStr_WGS: string;

    procedure SetBLRect(aBLRect: TBLRect);


   // FbmpPoints: array[0..3] of  record x,y: integer end;

  public
    BitmapFileName: string;

    Units:  (utDegree,utMeter);

    Transparence_0_255 : Integer; // ������������

//    XYRect: TXYRect;
//    BLBounds: TBLRect;

    ImgRowCount: integer;
    ImgColCount: integer;

//    Transparency : Integer; // ������������

    CoordSys  : TCoordSysType;
    ZoneNum   : integer; //GK

    procedure SetXYRect(aXYRect: TXYRect);
    procedure SaveToFile(aFileName: string);
  end;


  procedure geo_SaveFile_PRJ(aFileName: string; aUTM_Zone: Integer);




  // ������������ ��������� ��� ����� Mapinfo
  procedure geo_SaveXYHeaderForMIF (aRec: TMifHeaderInfoRec);
  procedure geo_SaveBLHeaderForMIF (aRec: TMifHeaderInfoRec);

procedure geo_SaveXYHeaderForMIF_new (aRec: TMifHeaderInfoRec);


  function geo_GetCoordSysStr_Pulkovo42(aRec: TTabRasterHeaderRec): string;


//===================================================================
implementation
//===================================================================
const
  CRLF = #13+#10;


//--------------------------------------------------------
function IIF (Condition: boolean; TrueValue,FalseValue: variant): variant;
//--------------------------------------------------------
begin
  if Condition then Result:=TrueValue else Result:=FalseValue;
end;

function ReplaceStr (Value, OldPattern, NewPattern: string): string;
begin
  Result:=StringReplace (Value, OldPattern, NewPattern, [rfReplaceAll, rfIgnoreCase]);
end;


procedure ForceDirByFileName (aFileName: string);
begin
  ForceDirectories (ExtractFileDir (aFileName));
end;



procedure SaveStringToFile(aStr: string; aFileName: string);
var
  list: TstringList;
begin
  ForceDirByFileName(aFileName);

  list:=TstringList.Create;
  list.Text:=aStr;
  list.SaveToFile (aFileName);
  list.Free;
end;



//------------------------------------------------------
procedure geo_SaveXYHeaderForMIF (aRec: TMifHeaderInfoRec);
//------------------------------------------------------
// ������������ ��������� ��� ����� Mapinfo
// - Bounds - ���������� ������� ������� ��������
//------------------------------------------------------

const
  ZONE_INFO_ARR : array[4..29] of string =
    (
      '21, 0, 1,  4500000, 0',  //5
      '27, 0, 1,  5500000, 0',  //5
      '33, 0, 1,  6500000, 0',  //6
      '39, 0, 1,  7500000, 0',  //7
      '45, 0, 1,  8500000, 0',  //8
      '51, 0, 1,  9500000, 0',  //9
      '57, 0, 1, 10500000, 0',  //10
      '63, 0, 1, 11500000, 0',  //11
      '69, 0, 1, 12500000, 0',  //12

      '75, 0, 1, 13500000, 0',  //13
      '81, 0, 1, 14500000, 0',  //14
      '87, 0, 1, 15500000, 0',  //15
      '93, 0, 1, 16500000, 0',  //16
      '99, 0, 1, 17500000, 0',  //17
     '105, 0, 1, 18500000, 0',  //18
     '111, 0, 1, 19500000, 0',   //19

     '117, 0, 1, 20500000, 0',   //19
     '123, 0, 1, 21500000, 0',   //19
     '129, 0, 1, 22500000, 0',   //19
     '135, 0, 1, 23500000, 0',   //19
     '141, 0, 1, 24500000, 0',   //19

     '147, 0, 1, 25500000, 0',   //19
     '153, 0, 1, 26500000, 0',   //19
     '159, 0, 1, 27500000, 0',   //19
     '165, 0, 1, 28500000, 0',   //19
     '171, 0, 1, 29500000, 0'   //19


    );

const MIF_RASTER =
    '!table'            + CRLF +                                                
    '!version 300'      + CRLF +
    '!charset WindowsCyrillic' + CRLF + CRLF +

    'Definition Table   '+ CRLF +
    '  File ":FileName" '+ CRLF +
    '  Type "RASTER"    '+ CRLF +
    ':Points            '+ CRLF +

    'CoordSys Earth Projection 8, 1001, "m", :Zone ' + CRLF + //39, 0, 1, 7500000, 0 ' + CRLF +
    'Units "m" '+ CRLF +

    //transparent setup
    'RasterStyle 4 1 '+ CRLF +
//    'RasterStyle 7 1 '+ CRLF + //16777215'; //bmp: 16776959'; //// JPG
 //   'RasterStyle 7 16777215 '+ CRLF + //16777215'; //bmp: 16776959'; //// JPG
    'RasterStyle 7 0 '+ CRLF + //16777215'; //bmp: 16776959'; //// JPG
    'RasterStyle 8 :transparence';




//    'RasterStyle 7 16777215 ';

var
  sZone,sContent,sXYPoints: string;

  xyPoints : array[0..3] of TXYPoint;
  bmpPoints: array[0..3] of  record x,y: integer end;
  i,iZone: integer;
  //y: double;
begin
  // we get center points of cells

  assert( aRec.RowCount>0);
  assert( aRec.ColCount>0);


  iZone:= aRec.XY.ZoneNum;
  if iZone=0 then
  begin
   // y:=aRec.XY.TopLeft.Y + (aRec.XY.ColCount-1)*aRec.XY.StepY;
//    iZone:=geo_Get6ZoneY ((aRec.XY.TopLeft.Y + y) / 2);
    iZone:=geo_Get6ZoneY (aRec.XY.TopLeft.Y);
  end;


  // bottom right corner
  bmpPoints[0].x:=aRec.ColCount-1;
  bmpPoints[0].y:=aRec.RowCount-1;
  xyPoints[0].X:=aRec.XY.TopLeft.X - (aRec.RowCount)*aRec.XY.StepX;
  xyPoints[0].Y:=aRec.XY.TopLeft.Y + (aRec.ColCount)*aRec.XY.StepY;


  // bottom left corner
  bmpPoints[1].x:=0;
  bmpPoints[1].y:=aRec.RowCount-1;
  xyPoints[1].X:=aRec.XY.TopLeft.X - (aRec.RowCount)*aRec.XY.StepX;
//  xyPoints[1].X:=aRec.XY.TopLeft.X - (aRec.RowCount-1)*aRec.XY.StepX;
  xyPoints[1].Y:=aRec.XY.TopLeft.Y;

  // top left point
  bmpPoints[2].x:=0;
  bmpPoints[2].y:=0;
  xyPoints[2].X:=aRec.XY.TopLeft.X;
  xyPoints[2].Y:=aRec.XY.TopLeft.Y;

  // top right corner
  bmpPoints[3].x:=aRec.ColCount-1;
  bmpPoints[3].y:=0;
  xyPoints[3].X:=aRec.XY.TopLeft.X;
  xyPoints[3].Y:=aRec.XY.TopLeft.Y + (aRec.ColCount)*aRec.XY.StepY;
//  xyPoints[3].Y:=aRec.XY.TopLeft.Y + (aRec.ColCount-1)*aRec.XY.StepY;



  sXYPoints:='';
  for i:=0 to 3 do
    sXYPoints:=sXYPoints + Format('  (%1.10f,%1.10f) (%d,%d) Label "Pt %d"',
      [xyPoints[i].Y, xyPoints[i].X, bmpPoints[i].x, bmpPoints[i].y, i+1])
      + IIF(i<3,',','') + CRLF;


  sContent:=MIF_RASTER;
  sContent:=ReplaceStr (sContent, ':FileName', ExtractFileName(aRec.BitmapFileName));
  sContent:=ReplaceStr (sContent, ':Points',   sXYPoints);

  sContent:=ReplaceStr (sContent, ':transparence',  IntToStr(  255-aRec.Transparence_0_255));





(*
   if aRec.Is_Transparent then
    sContent:=sContent + CRLF + 'RasterStyle 8 204 ';
*)


  if (iZone >= Low(ZONE_INFO_ARR)) and
     (iZone <= High(ZONE_INFO_ARR))
  then
//  if iZone in [6..8] then
    sZone:=ZONE_INFO_ARR[iZone]
  else
    sZone:='';

  sContent:=ReplaceStr (sContent, ':Zone',   sZone);


//  sMifFileName:=ChangeFileExt (aRec.FileName, '.tab');
  SaveStringToFile (sContent,
                    ChangeFileExt (aRec.FileName, '.tab'));


end;



//------------------------------------------------------
procedure geo_SaveXYHeaderForMIF_new (aRec: TMifHeaderInfoRec);
//------------------------------------------------------
// ������������ ��������� ��� ����� Mapinfo
// - Bounds - ���������� ������� ������� ��������
//------------------------------------------------------

//const
{
  ZONE_INFO_ARR : array[4..29] of string =
    (
      '21, 0, 1,  4500000, 0',  //5
      '27, 0, 1,  5500000, 0',  //5
      '33, 0, 1,  6500000, 0',  //6
      '39, 0, 1,  7500000, 0',  //7
      '45, 0, 1,  8500000, 0',  //8
      '51, 0, 1,  9500000, 0',  //9
      '57, 0, 1, 10500000, 0',  //10
      '63, 0, 1, 11500000, 0',  //11
      '69, 0, 1, 12500000, 0',  //12

      '75, 0, 1, 13500000, 0',  //13
      '81, 0, 1, 14500000, 0',  //14
      '87, 0, 1, 15500000, 0',  //15
      '93, 0, 1, 16500000, 0',  //16
      '99, 0, 1, 17500000, 0',  //17
     '105, 0, 1, 18500000, 0',  //18
     '111, 0, 1, 19500000, 0',   //19

     '117, 0, 1, 20500000, 0',   //19
     '123, 0, 1, 21500000, 0',   //19
     '129, 0, 1, 22500000, 0',   //19
     '135, 0, 1, 23500000, 0',   //19
     '141, 0, 1, 24500000, 0',   //19

     '147, 0, 1, 25500000, 0',   //19
     '153, 0, 1, 26500000, 0',   //19
     '159, 0, 1, 27500000, 0',   //19
     '165, 0, 1, 28500000, 0',   //19
     '171, 0, 1, 29500000, 0'   //19


    );
}

const MIF_RASTER =
    '!table'            + CRLF +
    '!version 300'      + CRLF +
    '!charset WindowsCyrillic' + CRLF + CRLF +

    'Definition Table   '+ CRLF +
    '  File ":FileName" '+ CRLF +
    '  Type "RASTER"    '+ CRLF +
    ':Points            '+ CRLF +

//CoordSys Earth Projection 1, 9999, 3, 23.57, -140.95, -79.8, 0, -0.35, -0.79, -0.22, 0

    'CoordSys Earth Projection 1, 9999, 3, 23.57, -140.95, -79.8, 0, -0.35, -0.79, -0.22, 0 ' + CRLF + //39, 0, 1, 7500000, 0 ' + CRLF +
//    'CoordSys Earth Projection 8, 1001, "m", :Zone ' + CRLF + //39, 0, 1, 7500000, 0 ' + CRLF +

//    'Units "m" '+ CRLF +
    'Units "degree" ' + CRLF +

    //transparent setup
    'RasterStyle 4 1 '+ CRLF +
//    'RasterStyle 7 1 '+ CRLF + //16777215'; //bmp: 16776959'; //// JPG
 //   'RasterStyle 7 16777215 '+ CRLF + //16777215'; //bmp: 16776959'; //// JPG
    'RasterStyle 7 0 '+ CRLF + //16777215'; //bmp: 16776959'; //// JPG
    'RasterStyle 8 :transparence';




//    'RasterStyle 7 16777215 ';

var
  sZone,sContent,sXYPoints: string;

  xyPoints : array[0..3] of TXYPoint;
  bmpPoints: array[0..3] of  record x,y: integer end;
  i,iZone: integer;

 // xypoints_arr: TXYPointArray;
  blPoints: TBLPointArray;

  //y: double;
begin
  // we get center points of cells

  assert( aRec.RowCount>0);
  assert( aRec.ColCount>0);


  iZone:= aRec.XY.ZoneNum;

  if iZone=0 then
  begin
   // y:=aRec.XY.TopLeft.Y + (aRec.XY.ColCount-1)*aRec.XY.StepY;
//    iZone:=geo_Get6ZoneY ((aRec.XY.TopLeft.Y + y) / 2);
    iZone:=geo_Get6ZoneY (aRec.XY.TopLeft.Y);
  end;


//  procedure

//  xypoints_arr:=geo_XYRectToXYPoints_(aRec.XYRect); //: TXYPointArray;




//
//  geo_XYRectToXYPointsF(aRec.XY, points_arr);




  // bottom right corner
  bmpPoints[0].x:=aRec.ColCount-1;
  bmpPoints[0].y:=aRec.RowCount-1;
  xyPoints[0].X:=aRec.XY.TopLeft.X - (aRec.RowCount)*aRec.XY.StepX;
  xyPoints[0].Y:=aRec.XY.TopLeft.Y + (aRec.ColCount)*aRec.XY.StepY;


  // bottom left corner
  bmpPoints[1].x:=0;
  bmpPoints[1].y:=aRec.RowCount-1;
  xyPoints[1].X:=aRec.XY.TopLeft.X - (aRec.RowCount)*aRec.XY.StepX;
//  xyPoints[1].X:=aRec.XY.TopLeft.X - (aRec.RowCount-1)*aRec.XY.StepX;
  xyPoints[1].Y:=aRec.XY.TopLeft.Y;

  // top left point
  bmpPoints[2].x:=0;
  bmpPoints[2].y:=0;
  xyPoints[2].X:=aRec.XY.TopLeft.X;
  xyPoints[2].Y:=aRec.XY.TopLeft.Y;

  // top right corner
  bmpPoints[3].x:=aRec.ColCount-1;
  bmpPoints[3].y:=0;
  xyPoints[3].X:=aRec.XY.TopLeft.X;
  xyPoints[3].Y:=aRec.XY.TopLeft.Y + (aRec.ColCount)*aRec.XY.StepY;
//  xyPoints[3].Y:=aRec.XY.TopLeft.Y + (aRec.ColCount-1)*aRec.XY.StepY;

  SetLength (blPoints,4);
  for i:=0 to 3 do
//    blPoints[i]:= geo_XY_to_BL(xyPoints[i], iZone, EK_KRASOVSKY42, EK_KRASOVSKY42);
    blPoints[i]:= geo_Pulkovo42_XY_to_BL(xyPoints[i], iZone); //, EK_KRASOVSKY42, EK_KRASOVSKY42);


//  function geo_Pulkovo42_to_WGS84(aBLPoint: TBLPoint): TBLPoint;
//  function geo_Pulkovo42_to_XY(aBLPoint: TBLPoint; aGK_Zone: word = 0):    TXYPoint;



  sXYPoints:='';
  for i:=0 to 3 do
    sXYPoints:=sXYPoints + Format('  (%1.10f,%1.10f) (%d,%d) Label "Pt %d"',
      [blPoints[i].L, blPoints[i].B, bmpPoints[i].x, bmpPoints[i].y, i+1])
//      [xyPoints[i].Y, xyPoints[i].X, bmpPoints[i].x, bmpPoints[i].y, i+1])
      + IIF(i<3,',','') + CRLF;


  sContent:=MIF_RASTER;
  sContent:=ReplaceStr (sContent, ':FileName', ExtractFileName(aRec.BitmapFileName));
  sContent:=ReplaceStr (sContent, ':Points',   sXYPoints);

  sContent:=ReplaceStr (sContent, ':transparence',  IntToStr(  255-aRec.Transparence_0_255));





(*
   if aRec.Is_Transparent then
    sContent:=sContent + CRLF + 'RasterStyle 8 204 ';
*)

  {
  if (iZone >= Low(ZONE_INFO_ARR)) and
     (iZone <= High(ZONE_INFO_ARR))
  then
//  if iZone in [6..8] then
    sZone:=ZONE_INFO_ARR[iZone]
  else
    sZone:='';

  sContent:=ReplaceStr (sContent, ':Zone',   sZone);
  }

//  sMifFileName:=ChangeFileExt (aRec.FileName, '.tab');
  SaveStringToFile (sContent,
                    ChangeFileExt (aRec.FileName, '.tab'));


end;


//------------------------------------------------------
procedure geo_SaveBLHeaderForMIF (aRec: TMifHeaderInfoRec);
//------------------------------------------------------
// ������������ ��������� ��� ����� Mapinfo
// - Bounds - ���������� ������� ������� ��������
//------------------------------------------------------
(*
!table
!version 300
!charset WindowsCyrillic

Definition Table
  File "nnovg_pl_50_3d.png"
  Type "RASTER"
  (44.266895029365,56.162716134481) (1010,731) Label "Pt 1",
  (43.616173366999,56.157143524305) (0,731) Label "Pt 2",
  (43.60664243142,56.42002339569) (0,0) Label "Pt 3",
  (44.26184443167,56.425651349409) (1010,0) Label "Pt 4"
  CoordSys Earth Projection 8, 1001, "m", 45, 0, 1, 8500000, 0
  Units "degree"
  RasterStyle 4 1
  RasterStyle 7 0


!table
!version 300
!charset WindowsCyrillic

Definition Table
  File "nnovg_pl_50_3d.png"
  Type "RASTER"
  (44.266895029365,56.162716134481) (1010,731) Label "Pt 1",
  (43.616173366999,56.157143524305) (0,731) Label "Pt 2",
  (43.60664243142,56.42002339569) (0,0) Label "Pt 3",
  (44.26184443167,56.425651349409) (1010,0) Label "Pt 4"
  CoordSys Earth Projection 1, 1001
  Units "degree"
  RasterStyle 4 1
  RasterStyle 7 0


*)


const
  MIF_RASTER =
    '!table'       + CRLF +
    '!version 300' + CRLF +
    '!charset WindowsCyrillic' + CRLF + CRLF +
    'Definition Table'    + CRLF +
    '  File ":FileName"'  + CRLF +
    '  Type "RASTER"'     + CRLF +
    ':Points'             + CRLF +
//    '  CoordSys Earth Projection 1, 1001 '+ CRLF +
    '  CoordSys Earth Projection 1, 1001 '+ CRLF +

//    'CoordSys Earth Projection 8, 1001, "m", :Zone ' + CRLF + //39, 0, 1, 7500000, 0 ' + CRLF +

    '  Units "degree" ' + CRLF +
    '  RasterStyle 4 1' + CRLF +
    '  RasterStyle 7 1'; //16777215';
//    '  RasterStyle 7 0'; //16777215';


var sContent,sPoints: string;

  blPoints : array[0..3] of TBLPoint;
  bmpPoints: array[0..3] of  record x,y: integer end;
  i: integer;
begin
  // we get center points of cells

  // bottom right corner
  bmpPoints[0].x:=aRec.ColCount-1;
  bmpPoints[0].y:=aRec.RowCount-1;
  blPoints[0]:=aRec.BL.BLBounds.BottomRight;

  // bottom left corner
  bmpPoints[1].x:=0;
  bmpPoints[1].y:=aRec.RowCount-1;
  blPoints[1]:=MakeBLPoint (aRec.BL.BLBounds.BottomRight.B, aRec.BL.BLBounds.TopLeft.L);

  // top left point
  bmpPoints[2].x:=0;
  bmpPoints[2].y:=0;
  blPoints[2]:=aRec.BL.BLBounds.TopLeft;

  // top right corner
  bmpPoints[3].x:=aRec.ColCount-1;
  bmpPoints[3].y:=0;
  blPoints[3]:=MakeBLPoint(aRec.BL.BLBounds.TopLeft.B, aRec.BL.BLBounds.BottomRight.L);


  sPoints:='';
  for i:=0 to 3 do
    sPoints:=sPoints + Format('  (%1.10f,%1.10f) (%d,%d) Label "Pt %d"',
      [blPoints[i].L, blPoints[i].B, bmpPoints[i].x, bmpPoints[i].y, i+1])
      + IIF(i<3,',','') + CRLF;

  sContent:=MIF_RASTER;
  sContent:=ReplaceStr (sContent, ':FileName', ExtractFileName(aRec.BitmapFileName));
  sContent:=ReplaceStr (sContent, ':Points',   sPoints);

//  Result:=sContent;

  SaveStringToFile (sContent,
                    ChangeFileExt (aRec.FileName, '.tab'));

end;

// ---------------------------------------------------------------
function geo_GetCoordSysStr_Pulkovo42(aRec: TTabRasterHeaderRec): string;
// ---------------------------------------------------------------
//CoordSys Earth Projection 8, 104, "m", 45, 0, 0.9996, 500000, 0
var
  d,k: Integer;
begin
 // Result := 'CoordSys Earth Projection ';

  case aRec.CoordSys of
//    csWGS84:     Result := 'CoordSys Earth Projection 8, 101, "m", ';
    csPulkovo42: Result := 'CoordSys Earth Projection 1, 1001, "m", ';
  end;

  d:=aRec.ZoneNum*6 + 3;
  k:= 1000000 * aRec.ZoneNum  + 500000;

  Result := Result + Format('%d, 0, 1, %d, 0', [d,k]);

end;


// ---------------------------------------------------------------
function geo_GetCoordSysStr_WGS(aRec: TTabRasterHeaderRec): string;
// ---------------------------------------------------------------
//CoordSys Earth Projection 8, 104, "m", 45, 0, 0.9996, 500000, 0
var
  d,k: Integer;
begin
 // Result := 'CoordSys Earth Projection ';

  case aRec.CoordSys of
//    csWGS84:     Result := 'CoordSys Earth Projection 8, 101, "m", ';
    csPulkovo42: Result := 'CoordSys Earth Projection 1, 104, "m", ';
  end;

  d:=aRec.ZoneNum*6 + 3;
  k:= 1000000 * aRec.ZoneNum  + 500000;

  Result := Result + Format('%d, 0, 1, %d, 0', [d,k]);

end;




// ---------------------------------------------------------------
procedure TMapinfoRasterTab1.SaveToFile(aFileName: string);
// ---------------------------------------------------------------
const
  MIF_RASTER =
    '!table'            + CRLF +
    '!version 300'      + CRLF +
    '!charset WindowsCyrillic' + CRLF + CRLF +

    'Definition Table   '+ CRLF +
    '  File ":FileName" '+ CRLF +
    '  Type "RASTER"    '+ CRLF +
    ':Points            '+ CRLF +
    'CoordSys :CoordSys'         + CRLF +
  //  'CoordSys Earth Projection 8, 1001, "m", :Zone ' + CRLF + //39, 0, 1, 7500000, 0 ' + CRLF +
//    ':Units'+ CRLF +
    'Units ":Units" '+ CRLF +

    //transparent setup
    'RasterStyle 4 1 '+ CRLF +
//    'RasterStyle 7 0 '+ CRLF + //16777215'; //bmp: 16776959'; //// JPG
    'RasterStyle 7 1 '+ CRLF + //16777215'; //bmp: 16776959'; //// JPG
    'RasterStyle 8 :transparence';

//    'RasterStyle 7 16777215 ';



var
  sContent,sPoints: string;

//  xyPoints : array[0..3] of TXYPoint;
 // bmpPoints: array[0..3] of  record x,y: integer end;
  i: integer;

var
  oSList: TStringList;
  sUnits: string;

  //y: double;
begin
  // we get center points of cells

//  assert( RowCount>0);
 // assert( aRec.ColCount>0);


  sPoints:='';
                //utDegree,utMeter
  case Units of
    utMeter :
      begin
        sUnits := 'm';

        for i:=0 to 3 do
        sPoints:=sPoints + Format('  (%1.10f,%1.10f) (%d,%d) Label "Pt %d"',
          [xyPoints[i].Y, xyPoints[i].X, bmpPoints[i].x, bmpPoints[i].y, i+1])
          + IIF(i<3,',','') + CRLF;
      end;

    utDegree:
      begin
        sUnits := 'degree';

        for i:=0 to 3 do
          sPoints:=sPoints + Format('  (%1.10f,%1.10f) (%d,%d) Label "Pt %d"',
            [blPoints[i].L, blPoints[i].B, bmpPoints[i].x, bmpPoints[i].y, i+1])
            + IIF(i<3,',','') + CRLF;
      end;
  end;

  sContent:=MIF_RASTER;
  sContent:=ReplaceStr (sContent, ':FileName', ExtractFileName(BitmapFileName));
  sContent:=ReplaceStr (sContent, ':Points',   sPoints);
  sContent:=ReplaceStr (sContent, ':CoordSys', GetCoordSysStr_CK42);
  sContent:=ReplaceStr (sContent, ':Units',    sUnits);


  sContent:=ReplaceStr (sContent, ':transparence', IntToStr(  255-Transparence_0_255));


 // if Is_Transparent then
//    sContent:=sContent + CRLF + 'RasterStyle 8 204 ';


  oSList:=TStringList.Create;
  oSList.Text:=sContent;

  ForceDirByFileName(aFileName);

//  oSList.SaveToFile (ChangeFileExt (aFileName, '.tab'));
  oSList.SaveToFile (aFileName);

  FreeAndNil(oSList);


(*  if (iZone >= Low(ZONE_INFO_ARR)) and
     (iZone <= High(ZONE_INFO_ARR))
  then
//  if iZone in [6..8] then
    sZone:=ZONE_INFO_ARR[iZone]
  else
    sZone:='';*)

 // sContent:=Replace/Str (sContent, ':Zone',   sZone);


//  sMifFileName:=ChangeFileExt (aRec.FileName, '.tab');
 // SaveStringToFile (sContent,
           //         ChangeFileExt (aRec.FileName, '.tab'));
end;


// ---------------------------------------------------------------
function TMapinfoRasterTab1.GetCoordSysStr_CK42: string;
// ---------------------------------------------------------------
var
  d,k: Integer;
begin
 // Result := 'CoordSys Earth Projection ';

  case CoordSys of
//    csWGS84:     Result := 'Earth Projection 8, 101, "m", ';
//    csPulkovo42: Result := 'Earth Projection 1, 1001, "m", ';
    csPulkovo42: Result := 'Earth Projection 8, 1001, "m", ';
  end;

  d:=(ZoneNum-1)*6 + 3;
  k:= 1000000 * ZoneNum  + 500000;

  Result := Result + Format('%d, 0, 1, %d, 0', [d,k]);

end;


// ---------------------------------------------------------------
function TMapinfoRasterTab1.GetCoordSysStr_WGS: string;
// ---------------------------------------------------------------
var
  d,k: Integer;
begin
 // Result := 'CoordSys Earth Projection ';

  case CoordSys of
//    csWGS84:     Result := 'Earth Projection 8, 101, "m", ';
//    csPulkovo42: Result := 'Earth Projection 1, 1001, "m", ';
    csPulkovo42: Result := 'Earth Projection 8, 104, "m", ';
  end;

  d:=(ZoneNum-1)*6 + 3;
  k:= 1000000 * ZoneNum  + 500000;

  Result := Result + Format('%d, 0, 1, %d, 0', [d,k]);

end;



// ---------------------------------------------------------------
procedure TMapinfoRasterTab1.SetXYRect(aXYRect: TXYRect);
// ---------------------------------------------------------------
begin
 // bottom right corner
  bmpPoints[0].x:=ImgColCount-1;
  bmpPoints[0].y:=ImgRowCount-1;
  xyPoints[0].X:=aXYRect.bottomRight.X;  // .TopLeft.X - (aRec.RowCount)*aRec.XY.StepX;
  xyPoints[0].Y:=aXYRect.bottomRight.Y; //aRec.XY.TopLeft.Y + (aRec.ColCount)*aRec.XY.StepY;


  // bottom left corner
  bmpPoints[1].x:=0;
  bmpPoints[1].y:=ImgRowCount-1;
  xyPoints[1].X:=aXYRect.bottomRight.X; //aRec.XY.TopLeft.X - (aRec.RowCount)*aRec.XY.StepX;
//  xyPoints[1].X:=aRec.XY.TopLeft.X - (aRec.RowCount-1)*aRec.XY.StepX;
  xyPoints[1].Y:=aXYRect.TopLeft.Y; //aRec.XY.TopLeft.Y;

  // top left point
  bmpPoints[2].x:=0;
  bmpPoints[2].y:=0;
  xyPoints[2].X:=aXYRect.TopLeft.X;
  xyPoints[2].Y:=aXYRect.TopLeft.Y;

  // top right corner
  bmpPoints[3].x:=ImgColCount-1;
  bmpPoints[3].y:=0;
  xyPoints[3].X:=aXYRect.TopLeft.X;
  xyPoints[3].Y:=aXYRect.bottomRight.Y; //aRec.XY.TopLeft.Y + (aRec.ColCount)*aRec.XY.StepY;
//  xyPoints[3].Y:=aRec.XY.TopLeft.Y + (aRec.ColCount-1)*aRec.XY.StepY;



end;

// ---------------------------------------------------------------
procedure TMapinfoRasterTab1.SetBLRect(aBLRect: TBLRect);
// ---------------------------------------------------------------
begin
 // bottom right corner
  bmpPoints[0].x:=ImgColCount-1;
  bmpPoints[0].y:=ImgRowCount-1;
  blPoints[0]:=aBLRect.BottomRight;

  // bottom left corner
  bmpPoints[1].x:=0;
  bmpPoints[1].y:=ImgRowCount-1;
  blPoints[1]:=MakeBLPoint (aBLRect.BottomRight.B, aBLRect.TopLeft.L);

  // top left point
  bmpPoints[2].x:=0;
  bmpPoints[2].y:=0;
  blPoints[2]:=aBLRect.TopLeft;

  // top right corner
  bmpPoints[3].x:=ImgColCount-1;
  bmpPoints[3].y:=0;
  blPoints[3]:=MakeBLPoint(aBLRect.TopLeft.B, aBLRect.BottomRight.L);
end;

// ---------------------------------------------------------------
procedure geo_SaveFile_PRJ(aFileName: string; aUTM_Zone: Integer);
// ---------------------------------------------------------------
const
  DEF = 'PROJCS["UTM_Zone_%d_Northern_Hemisphere",'+
        'GEOGCS["GCS_WGS_1984",DATUM["D_WGS84",SPHEROID["WGS84",6378137,298.257223560493]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],'+
        'PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",%d],PARAMETER["scale_factor",0.9996],PARAMETER["false_easting",500000],PARAMETER["false_northing",0],UNIT["Meter",1]]';
//38 - 45
var
  k: Integer;
  s: string;
begin
  k:= 6 * (aUTM_Zone-30) +3;

  s:= Format(DEF, [aUTM_Zone, k]);


{ROJCS["UTM_Zone_38_Northern_Hemisphere",
  GEOGCS["GCS_WGS_1984",
    DATUM["D_WGS84", SPHEROID["WGS84",6378137,298.257223560493]],
    PRIMEM["Greenwich",0],
    UNIT["Degree",0.017453292519943295]],
  PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",45],PARAMETER["scale_factor",0.9996],PARAMETER["false_easting",500000],PARAMETER["false_northing",0],UNIT["Meter",1]]
}
end;



end.


{

PROJCS["UTM_Zone_38_Northern_Hemisphere",
  GEOGCS["GCS_WGS_1984",
    DATUM["D_WGS84", SPHEROID["WGS84",6378137,298.257223560493]],
    PRIMEM["Greenwich",0],
    UNIT["Degree",0.017453292519943295]],
  PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",45],PARAMETER["scale_factor",0.9996],PARAMETER["false_easting",500000],PARAMETER["false_northing",0],UNIT["Meter",1]]



//------------------------------------------------------
procedure geo_SaveXYHeaderForMIF (aRec: TMifHeaderInfoRec);
//------------------------------------------------------
// ������������ ��������� ��� ����� Mapinfo
// - Bounds - ���������� ������� ������� ��������
//------------------------------------------------------

const
  ZONE_INFO_ARR : array[4..29] of string =
    (
      '21, 0, 1,  4500000, 0',  //5
      '27, 0, 1,  5500000, 0',  //5
      '33, 0, 1,  6500000, 0',  //6
      '39, 0, 1,  7500000, 0',  //7
      '45, 0, 1,  8500000, 0',  //8
      '51, 0, 1,  9500000, 0',  //9
      '57, 0, 1, 10500000, 0',  //10
      '63, 0, 1, 11500000, 0',  //11
      '69, 0, 1, 12500000, 0',  //12

      '75, 0, 1, 13500000, 0',  //13
      '81, 0, 1, 14500000, 0',  //14
      '87, 0, 1, 15500000, 0',  //15
      '93, 0, 1, 16500000, 0',  //16
      '99, 0, 1, 17500000, 0',  //17
     '105, 0, 1, 18500000, 0',  //18
     '111, 0, 1, 19500000, 0',   //19

     '117, 0, 1, 20500000, 0',   //19
     '123, 0, 1, 21500000, 0',   //19
     '129, 0, 1, 22500000, 0',   //19
     '135, 0, 1, 23500000, 0',   //19
     '141, 0, 1, 24500000, 0',   //19

     '147, 0, 1, 25500000, 0',   //19
     '153, 0, 1, 26500000, 0',   //19
     '159, 0, 1, 27500000, 0',   //19
     '165, 0, 1, 28500000, 0',   //19
     '171, 0, 1, 29500000, 0'   //19


    );

const MIF_RASTER =
    '!table'            + CRLF +                                                
    '!version 300'      + CRLF +
    '!charset WindowsCyrillic' + CRLF + CRLF +

    'Definition Table   '+ CRLF +
    '  File ":FileName" '+ CRLF +
    '  Type "RASTER"    '+ CRLF +
    ':Points            '+ CRLF +

//CoordSys Earth Projection 1, 9999, 3, 23.57, -140.95, -79.8, 0, -0.35, -0.79, -0.22, 0

    'CoordSys Earth Projection 8, 1001, "m", :Zone ' + CRLF + //39, 0, 1, 7500000, 0 ' + CRLF +
    'Units "m" '+ CRLF +

    //transparent setup
    'RasterStyle 4 1 '+ CRLF +
//    'RasterStyle 7 1 '+ CRLF + //16777215'; //bmp: 16776959'; //// JPG
 //   'RasterStyle 7 16777215 '+ CRLF + //16777215'; //bmp: 16776959'; //// JPG
    'RasterStyle 7 0 '+ CRLF + //16777215'; //bmp: 16776959'; //// JPG
    'RasterStyle 8 :transparence';




//    'RasterStyle 7 16777215 ';

var
  sZone,sContent,sXYPoints: string;

  xyPoints : array[0..3] of TXYPoint;
  bmpPoints: array[0..3] of  record x,y: integer end;
  i,iZone: integer;

 // xypoints_arr: TXYPointArray;
  blpoints_arr: TBLPointArray;

  //y: double;
begin
  // we get center points of cells

  assert( aRec.RowCount>0);
  assert( aRec.ColCount>0);


  iZone:= aRec.XY.ZoneNum;

  if iZone=0 then
  begin
   // y:=aRec.XY.TopLeft.Y + (aRec.XY.ColCount-1)*aRec.XY.StepY;
//    iZone:=geo_Get6ZoneY ((aRec.XY.TopLeft.Y + y) / 2);
    iZone:=geo_Get6ZoneY (aRec.XY.TopLeft.Y);
  end;


//  procedure

//  xypoints_arr:=geo_XYRectToXYPoints_(aRec.XYRect); //: TXYPointArray;




//
//  geo_XYRectToXYPointsF(aRec.XY, points_arr);




  // bottom right corner
  bmpPoints[0].x:=aRec.ColCount-1;
  bmpPoints[0].y:=aRec.RowCount-1;
  xyPoints[0].X:=aRec.XY.TopLeft.X - (aRec.RowCount)*aRec.XY.StepX;
  xyPoints[0].Y:=aRec.XY.TopLeft.Y + (aRec.ColCount)*aRec.XY.StepY;


  // bottom left corner
  bmpPoints[1].x:=0;
  bmpPoints[1].y:=aRec.RowCount-1;
  xyPoints[1].X:=aRec.XY.TopLeft.X - (aRec.RowCount)*aRec.XY.StepX;
//  xyPoints[1].X:=aRec.XY.TopLeft.X - (aRec.RowCount-1)*aRec.XY.StepX;
  xyPoints[1].Y:=aRec.XY.TopLeft.Y;

  // top left point
  bmpPoints[2].x:=0;
  bmpPoints[2].y:=0;
  xyPoints[2].X:=aRec.XY.TopLeft.X;
  xyPoints[2].Y:=aRec.XY.TopLeft.Y;

  // top right corner
  bmpPoints[3].x:=aRec.ColCount-1;
  bmpPoints[3].y:=0;
  xyPoints[3].X:=aRec.XY.TopLeft.X;
  xyPoints[3].Y:=aRec.XY.TopLeft.Y + (aRec.ColCount)*aRec.XY.StepY;
//  xyPoints[3].Y:=aRec.XY.TopLeft.Y + (aRec.ColCount-1)*aRec.XY.StepY;

  SetLength (blpoints_arr,4);
  for i:=0 to 3 do
    blpoints_arr[i]:= geo_XY_to_BL(xyPoints[i], iZone, EK_KRASOVSKY42, EK_KRASOVSKY42);




  sXYPoints:='';
  for i:=0 to 3 do
    sXYPoints:=sXYPoints + Format('  (%1.10f,%1.10f) (%d,%d) Label "Pt %d"',
      [xyPoints[i].Y, xyPoints[i].X, bmpPoints[i].x, bmpPoints[i].y, i+1])
      + IIF(i<3,',','') + CRLF;


  sContent:=MIF_RASTER;
  sContent:=ReplaceStr (sContent, ':FileName', ExtractFileName(aRec.BitmapFileName));
  sContent:=ReplaceStr (sContent, ':Points',   sXYPoints);

  sContent:=ReplaceStr (sContent, ':transparence',  IntToStr(  255-aRec.Transparence_0_255));





(*
   if aRec.Is_Transparent then
    sContent:=sContent + CRLF + 'RasterStyle 8 204 ';
*)


  if (iZone >= Low(ZONE_INFO_ARR)) and
     (iZone <= High(ZONE_INFO_ARR))
  then
//  if iZone in [6..8] then
    sZone:=ZONE_INFO_ARR[iZone]
  else
    sZone:='';

  sContent:=ReplaceStr (sContent, ':Zone',   sZone);


//  sMifFileName:=ChangeFileExt (aRec.FileName, '.tab');
  SaveStringToFile (sContent,
                    ChangeFileExt (aRec.FileName, '.tab'));


end;

