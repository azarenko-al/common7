unit fr_geo_Coordinates;

interface

uses
  SysUtils, rxCurrEdit,  StdCtrls, ExtCtrls,
  Variants,Classes, Controls, Forms,

  u_geo_convert_2017,

  u_func,
  u_Geo, Mask, rxToolEdit;

type
  Tframe_geo_Coordinates = class(TForm)
    rg_CoordSystem: TRadioGroup;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    CurrencyEdit_lon: TCurrencyEdit;
    CurrencyEdit_lat: TCurrencyEdit;
    Panel1: TPanel;
    CurrencyEdit11: TCurrencyEdit;
    CurrencyEdit12: TCurrencyEdit;
    CurrencyEdit_sec1: TCurrencyEdit;
    cb_North: TComboBox;
    cb_East_West: TComboBox;
    CurrencyEdit_lon_Sec: TCurrencyEdit;
    CurrencyEdit_lon_Min: TCurrencyEdit;
    CurrencyEdit_lon_Deg: TCurrencyEdit;
    lb_Lat: TLabel;
    lb_Lon: TLabel;

    procedure cb_East_WestChange(Sender: TObject);
    procedure CurrencyEdit11Change(Sender: TObject);
    procedure CurrencyEdit_latChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ed_BL_Change(Sender: TObject);
    procedure ed_LAtEditing(Sender: TObject; var CanEdit: Boolean);

    procedure rg_CoordSystemClick(Sender: TObject);
  private
    FBLPoint_Pulkovo: TBLPoint;
    FDisplayBLPoint: TBLPoint;

    FDisplayCoordSys: integer;

    FIsEdited: Boolean;
    FOnChange: TnotifyEvent;

    FPosChanged: Boolean;
    FUpdated: Boolean;

    procedure BLPointToEdit(aIndex: integer);
    procedure EditToBLPoint(aIndex: Integer);

    procedure UpdateDisplayBLPoint;
  private
    procedure deg_Changed;
    procedure Deg_min_sec_changed;

    procedure SetBLPoint_pulkovo(const Value: TBLPoint);
    function GetBLPoint_pulkovo: TBLPoint;

    function GetDisplayCoordSys: Integer;
    procedure SetDisplayCoordSys(aValue: Integer);

  public
    property OnChange: TnotifyEvent read FOnChange write FOnChange;

    //DisplayCoordSys: Integer

    procedure Set_LatLon_pulkovo(aLat, aLon: double);
    procedure Set_BLPoint_Pulkovo_and_CoordSys(aLat, aLon: double; aCoordSys:
        integer);

    property BLPoint_Pulkovo: TBLPoint read GetBLPoint_pulkovo write SetBLPoint_pulkovo;

//    property DisplayCoordSys: Integer read GetDisplayCoordSys write SetDisplayCoordSys;
    property DisplayCoordSys: Integer write SetDisplayCoordSys;

//EK_KRASOVSKY42, EK_WGS84);


    function GetBestHeight: Integer;

  end;

const
  DEF_frame_geo_Coordinates_HEIGHT = 110;


{
var
  frame_geo_Coordinates: Tframe_geo_Coordinates;  
}

implementation
{$R *.dfm}


//--------------------------------------------------------------
procedure Tframe_geo_Coordinates.FormCreate(Sender: TObject);
//--------------------------------------------------------------  
begin
  CurrencyEdit_lat.DecimalPlaces:=10;
  CurrencyEdit_lon.DecimalPlaces:=10;

  CurrencyEdit_lat.DisplayFormat:=',0.0000000000';
  CurrencyEdit_lon.DisplayFormat:=',0.0000000000';


  CurrencyEdit_sec1.DisplayFormat   :=',0.000';
  CurrencyEdit_lon_Sec.DisplayFormat:=',0.000';

  CurrencyEdit_sec1.DecimalPlaces:=3;
  CurrencyEdit_lon_Sec.DecimalPlaces:=3;

  FUpdated:=True;

  with rg_CoordSystem.Items do
  begin
    Clear;
    AddObject(DEF_GEO_STR_CK42,     Pointer(EK_CK_42));
    AddObject(DEF_GEO_STR_CK95,     Pointer(EK_CK_95));
    AddObject(DEF_GEO_STR_GCK_2011, Pointer(EK_GCK_2011));
    AddObject(DEF_GEO_STR_WGS84,    Pointer(EK_WGS84));

  end;

  rg_CoordSystem.OnClick := nil;
  rg_CoordSystem.ItemIndex :=3;
  rg_CoordSystem.OnClick := rg_CoordSystemClick;

  if Owner is TControl then
//    TControl(Owner).Height:=115;
    TControl(Owner).Height:=DEF_frame_geo_Coordinates_HEIGHT;  //85

  FUpdated:=False;

end;

//----------------------------------------------
procedure Tframe_geo_Coordinates.BLPointToEdit(aIndex: integer);
//----------------------------------------------
var
  rAngle1,rAngle2: TAngle;

begin
  FIsEdited := True;

  case aIndex of
    1: begin
        rAngle1:=geo_DecodeDegree (FDisplayBLPoint.B);
        rAngle2:=geo_DecodeDegree (FDisplayBLPoint.L);


        // ---------------------------------------------------------------
        // LAT
        // ---------------------------------------------------------------
        CurrencyEdit11.Value:=Abs(rAngle1.deg);
        CurrencyEdit12.Value:=rAngle1.min;
        CurrencyEdit_sec1.Value:=rAngle1.sec;

        // ---------------------------------------------------------------
        // LON
        // ---------------------------------------------------------------

        CurrencyEdit_lon_Deg.Value:=IIF(rAngle2.deg<180, rAngle2.deg, 360-rAngle2.deg);
        CurrencyEdit_lon_Min.Value:=rAngle2.min;
        CurrencyEdit_lon_sec.Value:=rAngle2.sec;

       end;

    2: begin
        CurrencyEdit_Lat.Value:=FDisplayBLPoint.B;

        CurrencyEdit_Lon.Value:=IIF(FDisplayBLPoint.L<180, FDisplayBLPoint.L, 360-FDisplayBLPoint.L);

       end;
  end;

  FIsEdited := False;
end;




//----------------------------------------------
procedure Tframe_geo_Coordinates.EditToBLPoint(aIndex: Integer);
//----------------------------------------------
var
  rAngle1: TAngle;
  rAngle2: TAngle;
  iCoordSys: integer;
begin
  case aIndex of
    1: begin
         rAngle1.deg :=Round(CurrencyEdit11.Value);
         rAngle1.min :=Round(CurrencyEdit12.Value);
         rAngle1.sec :=CurrencyEdit_sec1.Value;

//         if cb_East_West.Text='W' then

         rAngle2.deg :=Round(CurrencyEdit_lon_Deg.Value);
         rAngle2.min :=Round(CurrencyEdit_lon_Min.Value);
         rAngle2.sec :=      CurrencyEdit_lon_Sec.Value;

         FDisplayBLPoint.B := geo_EncodeDegreeRec (rAngle1);
         FDisplayBLPoint.L := geo_EncodeDegreeRec (rAngle2);
       end;

    2: begin
         FDisplayBLPoint.B := CurrencyEdit_Lat.Value;
         FDisplayBLPoint.L := CurrencyEdit_Lon.Value;

       end;
  end;

  //------------------------------------------------------
  iCoordSys :=GetDisplayCoordSys();


  if cb_North.Text='N' then
    FDisplayBLPoint.B:= Abs(FDisplayBLPoint.B)
  else
    FDisplayBLPoint.B:= -Abs(FDisplayBLPoint.B);


  if cb_East_West.Text='W' then
    FDisplayBLPoint.L:= 360 - FDisplayBLPoint.L;


  FBLPoint_Pulkovo := geo_BL_to_BL_2017_ (FDisplayBLPoint, iCoordSys, EK_CK_42);

end;

// ---------------------------------------------------------------
procedure Tframe_geo_Coordinates.ed_BL_Change(Sender: TObject);
// ---------------------------------------------------------------
begin
  if FIsEdited then
    Exit;

  FUpdated:=True;

  EditToBLPoint(2);
  BLPointToEdit(1);

  FUpdated:=False;

  if Assigned(FOnChange) then
    FOnChange(Self);
end;


// ---------------------------------------------------------------
procedure Tframe_geo_Coordinates.ed_LAtEditing(Sender: TObject; var CanEdit:  Boolean);
// ---------------------------------------------------------------
begin
 if FIsEdited then
    Exit;

  FUpdated:=True;

  EditToBLPoint(2);
  BLPointToEdit(1);

  FUpdated:=False;


  if Assigned(FOnChange) then
    FOnChange(Self);
end;

// ---------------------------------------------------------------
function Tframe_geo_Coordinates.GetBLPoint_pulkovo: TBLPoint;
// ---------------------------------------------------------------
begin
  //��������� ��������� �����

  if FPosChanged then
    EditToBLPoint(1);

 // FBLPoint_Pulkovo := geo_BL_to_BL_2017_ (FDisplayBLPoint, iCoordSys, EK_CK_42);

  Result:=FBLPoint_Pulkovo;

//  Result:=geo_BL_to_BL_2017_ (FDisplayBLPoint, iCoordSys, EK_CK_42);

end;

// ---------------------------------------------------------------
function Tframe_geo_Coordinates.GetDisplayCoordSys: Integer;
// ---------------------------------------------------------------
begin
  with rg_CoordSystem do
    Result := Integer(Items.Objects[ItemIndex]);
end;


procedure Tframe_geo_Coordinates.rg_CoordSystemClick(Sender: TObject);
begin
  if not FUpdated then
    UpdateDisplayBLPoint();

end;

// ---------------------------------------------------------------
procedure Tframe_geo_Coordinates.SetBLPoint_pulkovo(const Value: TBLPoint);
// ---------------------------------------------------------------
begin
  FPosChanged:=False;

  FUpdated:=True;

  FBLPoint_Pulkovo:=Value;
  UpdateDisplayBLPoint();

  FUpdated:=False;

end;


procedure Tframe_geo_Coordinates.SetDisplayCoordSys(aValue: Integer);
var
  ind: integer;
begin
  FDisplayCoordSys:=aValue;
 // rg_CoordSystem.ItemIndex:=IIF(aValue=EK_KRASOVSKY42, 0, 1);

  ind := rg_CoordSystem.Items.IndexOfObject(Pointer(aValue));
  if ind>=0 then
    rg_CoordSystem.ItemIndex :=ind;
end;

// ---------------------------------------------------------------
procedure Tframe_geo_Coordinates.UpdateDisplayBLPoint;
// ---------------------------------------------------------------
var
  iCoordSys_new: integer;
begin
  iCoordSys_new :=GetDisplayCoordSys();

  FDisplayBLPoint := geo_BL_to_BL_2017_ (FBLPoint_Pulkovo, EK_CK_42, iCoordSys_new);

  cb_North.ItemIndex := IIF(FDisplayBLPoint.B>=0,  0,1 );
//  cb_East.ItemIndex  := IIF(FDisplayBLPoint.L<180, 0,1 );

  cb_East_West.ItemIndex := IIF(FDisplayBLPoint.L<180, 0,1 );

  BLPointToEdit(1);
  BLPointToEdit(2);



end;

// ---------------------------------------------------------------
procedure Tframe_geo_Coordinates.Deg_min_sec_changed;
// ---------------------------------------------------------------
begin
 if FIsEdited then
    Exit;

  EditToBLPoint(1);
  BLPointToEdit(2);

  if Assigned(FOnChange) then
    FOnChange(Self);

end;

// ---------------------------------------------------------------
procedure Tframe_geo_Coordinates.deg_Changed;
// ---------------------------------------------------------------
begin
  if FIsEdited then
    Exit;

  FPosChanged:=True;

  EditToBLPoint(2);
  BLPointToEdit(1);

  if Assigned(FOnChange) then
    FOnChange(Self);
end;


procedure Tframe_geo_Coordinates.CurrencyEdit11Change(Sender: TObject);
begin
  if not FUpdated then
    Deg_min_sec_changed();
end;


procedure Tframe_geo_Coordinates.CurrencyEdit_latChange(Sender: TObject);
begin
  if not FUpdated then
    deg_Changed();
end;


procedure Tframe_geo_Coordinates.cb_East_WestChange(Sender: TObject);
begin
  FPosChanged:=True;

  if Assigned(FOnChange) then
    FOnChange(Self);
end;


function Tframe_geo_Coordinates.GetBestHeight: Integer;
begin
  Result := DEF_frame_geo_Coordinates_HEIGHT;
end;

procedure Tframe_geo_Coordinates.Set_BLPoint_Pulkovo_and_CoordSys(aBLPoint: TBLPoint; aCoordSys: integer);
begin

end;


procedure Tframe_geo_Coordinates.Set_LatLon_pulkovo(aLat, aLon: double);
begin
   SetBLPoint_pulkovo( MakeBLPoint(aLat, aLon) );

end;


end.

{
 Fframe_geo_Coordinates.BLPoint_Pulkovo:=aBLPoint;
 Fframe_geo_Coordinates.DisplayCoordSys:=IOptions.Get_CoordSys;
