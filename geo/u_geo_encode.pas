unit u_geo_encode;

interface
uses
  SysUtils,

  u_func
  ;
        

function geo_EncodeDegreeFromString(aValue: string): Double;

procedure geo_Encode2DegreeFromString(aValue: string; var aDegree1, aDegree2:
    Double);

    
//================================================================
implementation

type
  TAngle = packed record Deg,Min:integer; Sec: double; end;


function geo_EncodeDegree(aDeg: integer; aMin, aSec: Double): double;
begin
  Result:=aDeg + aMin/60 + aSec/3600;
end;


// ---------------------------------------------------------------
function geo_EncodeDegree_Deg_Min(aDeg: integer; aMin: double): double;
// ---------------------------------------------------------------
var
  e: Double;
  eSec: Double;
begin
  e:=Frac(aMin);
  if e>0 then
    eSec:=e * 60
  else
    eSec:=0;

//  Result:=aDeg + aMin/60;// + Sec/3600;
  Result:=aDeg + Trunc(aMin)/60 + eSec/3600;

end;



//--------------------------------------------------------------------
procedure geo_Encode2DegreeFromString(aValue: string; var aDegree1, aDegree2:
    Double);
//--------------------------------------------------------------------
// 54 44' 10.00''    52 42' 30.00''
//--------------------------------------------------------------------
  procedure DoAdd(var Value: string; aChar: Char);
  begin
    if (Value='') then
      if (aChar='0') or (not (aChar in ['0'..'9'])) then
         Exit;

    Value := Value + aChar;
  end;

  

var
  arrStr: array[0..6-1] of string;

  ang1: TAngle;
  ang2: TAngle;
  I: Integer;
  sTemp,sValue, sStr: string;

  sDeg1: string;
  sMin1: string;
  sSec1: string;

  sDeg2: string;
  sMin2: string;
  sSec2: string;

  iSection : Integer;
 // s: string;
  bIsDigit : Boolean;

  ch: Char;
  e: Double;
  eDeg: Double;
  eDeg1: Integer;
  eDeg2: Integer;
  eMin: Double;
  eMin1: Double;
  eMin2: Double;
  eSec1: Double;
  eSec2: Double;
  sNumber: string;

begin
  iSection:=0;
  sNumber:='';

  for I := 1 to Length(aValue) do
  begin
    ch := aValue[i];

    //  s:='51�40''27" �.�. 39�12''40" �.�.';

    if ch in ['0'..'9'] then
    begin
      sNumber:=sNumber+ ch;
    end else

    if ch in ['.',','] then
    begin
      if (sNumber<>'') then sNumber:=sNumber+ ch;
    end

    else begin
      if sNumber<>'' then
      begin
        arrStr[iSection] := sNumber;
        Inc(iSection);
        sNumber:='';

        if iSection>=6 then
          Break;
      end;
    end;

  end;



  eDeg1 := StrToIntDef(arrStr[0], 0);
  eMin1 := AsFloat(arrStr[1]);
  eSec2 := AsFloat(arrStr[2]);

  eDeg2 := StrToIntDef(arrStr[3], 0);
  eMin2 := AsFloat(arrStr[4]);
  eSec1 := AsFloat(arrStr[5]);


  e:=Frac(eMin1);
  e:=Frac(eMin2);

  // -------------------------
  if Frac(eMin1)>0 then
    aDegree1 := geo_EncodeDegree_Deg_Min(eDeg1, eMin1)
  else
    aDegree1 := geo_EncodeDegree(eDeg1, eMin1, eSec1);

  // -------------------------
  if Frac(eMin2)>0 then
    aDegree2 := geo_EncodeDegree_Deg_Min(eDeg2, eMin2)
  else
    aDegree2 := geo_EncodeDegree(eDeg2, eMin2, eSec2);
       
end;

//--------------------------------------------------------------------
function geo_EncodeDegreeFromString(aValue: string): Double;
//--------------------------------------------------------------------
var
  eDegree1 : double;
  eDegree2 : Double;
begin
  geo_Encode2DegreeFromString(aValue, eDegree1, eDegree2);

  Result := eDegree1;
end;


var
  aDegree1, aDegree2: Double;
  S: string;
begin

(*
  s:='051�40''27.33" �.�. 039�12''40.1" �.�.';

  geo_Encode2DegreeFromString(s, aDegree1, aDegree2);

  s:='51�40''27" �.�. 39�12''40" �.�.';

//51�40'27" �.�. 39�12'40" �.�.

*)
end.



(*
var
  aDegree1, aDegree2: Double;
begin

  geo_Encode2DegreeFromString('53? 25,09? �.�.'#$A'50? 06,55? �.�.',
    aDegree1, aDegree2);
*)




   {
    if ch in [#$A] then
      iSection:=3
    else

    if ch in ['0'..'9','.',','] then
    begin
      bIsDigit:=True;

      case iSection of
        0: DoAdd(sDeg1, ch);
        1: DoAdd(sMin1, ch);
        2: DoAdd(sSec1, ch);

        3: DoAdd(sDeg2, ch);
        4: DoAdd(sMin2, ch);
        5: DoAdd(sSec2, ch);
      end;

    end else
    begin
      if bIsDigit then
      begin
        bIsDigit := False;
        Inc(iSection);

       // if  then

      end;
     

    end;
     }


