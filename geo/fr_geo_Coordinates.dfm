object frame_geo_Coordinates: Tframe_geo_Coordinates
  Left = 681
  Top = 370
  Width = 564
  Height = 161
  BorderWidth = 1
  Caption = 'fr_geo_Coordinates'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 546
    Height = 77
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 233
      Height = 77
      Hint = #1064#1080#1088#1086#1090#1072
      Align = alLeft
      TabOrder = 0
      object lb_Lat: TLabel
        Left = 8
        Top = 19
        Width = 38
        Height = 13
        Caption = #1064#1080#1088#1086#1090#1072
      end
      object lb_Lon: TLabel
        Left = 8
        Top = 50
        Width = 43
        Height = 13
        Caption = #1044#1086#1083#1075#1086#1090#1072
      end
      object Panel1: TPanel
        Left = 64
        Top = 15
        Width = 167
        Height = 60
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object CurrencyEdit11: TCurrencyEdit
          Left = 0
          Top = 0
          Width = 30
          Height = 21
          Alignment = taLeftJustify
          DecimalPlaces = 0
          DisplayFormat = ',0'
          MaxValue = 90.000000000000000000
          MinValue = -90.000000000000000000
          TabOrder = 0
          OnChange = CurrencyEdit11Change
        end
        object CurrencyEdit12: TCurrencyEdit
          Left = 34
          Top = 0
          Width = 25
          Height = 21
          Hint = #1064#1080#1088#1086#1090#1072
          Alignment = taLeftJustify
          DecimalPlaces = 0
          DisplayFormat = ',0'
          MaxValue = 60.000000000000000000
          TabOrder = 1
          OnChange = CurrencyEdit11Change
        end
        object CurrencyEdit_sec1: TCurrencyEdit
          Left = 62
          Top = 0
          Width = 48
          Height = 21
          Hint = #1064#1080#1088#1086#1090#1072
          Alignment = taLeftJustify
          DisplayFormat = ',0.000'
          MaxValue = 60.000000000000000000
          TabOrder = 2
          OnChange = CurrencyEdit11Change
        end
        object cb_North: TComboBox
          Left = 113
          Top = 0
          Width = 43
          Height = 21
          Hint = 'N-north'#13#10'S-sourth'
          Style = csDropDownList
          ItemHeight = 13
          ItemIndex = 0
          TabOrder = 3
          Text = 'N'
          OnChange = cb_East_WestChange
          Items.Strings = (
            'N'
            'S')
        end
        object cb_East_West: TComboBox
          Left = 113
          Top = 28
          Width = 43
          Height = 21
          Hint = 'E - '#1074#1086#1089#1090#1086#1082#13#10'W - '#1079#1072#1087#1072#1076' '
          Style = csDropDownList
          ItemHeight = 13
          ItemIndex = 0
          ParentShowHint = False
          ShowHint = True
          TabOrder = 7
          Text = 'E'
          OnChange = cb_East_WestChange
          Items.Strings = (
            'E'
            'W')
        end
        object CurrencyEdit_lon_Sec: TCurrencyEdit
          Left = 62
          Top = 28
          Width = 48
          Height = 21
          Hint = #1044#1086#1083#1075#1086#1090#1072
          Alignment = taLeftJustify
          DisplayFormat = ',0.000'
          MaxValue = 60.000000000000000000
          TabOrder = 6
          OnChange = CurrencyEdit11Change
        end
        object CurrencyEdit_lon_Min: TCurrencyEdit
          Left = 34
          Top = 28
          Width = 25
          Height = 21
          Hint = #1044#1086#1083#1075#1086#1090#1072
          Alignment = taLeftJustify
          DisplayFormat = ',0'
          MaxValue = 60.000000000000000000
          TabOrder = 5
          OnChange = CurrencyEdit11Change
        end
        object CurrencyEdit_lon_Deg: TCurrencyEdit
          Left = 0
          Top = 28
          Width = 30
          Height = 21
          Hint = #1044#1086#1083#1075#1086#1090#1072
          Alignment = taLeftJustify
          DecimalPlaces = 0
          DisplayFormat = ',0'
          MaxValue = 180.000000000000000000
          MinValue = -180.000000000000000000
          TabOrder = 4
          OnChange = CurrencyEdit11Change
        end
      end
    end
    object GroupBox_center: TGroupBox
      Left = 233
      Top = 0
      Width = 160
      Height = 77
      Align = alLeft
      TabOrder = 1
      DesignSize = (
        160
        77)
      object CurrencyEdit_lon: TCurrencyEdit
        Left = 8
        Top = 43
        Width = 145
        Height = 21
        Alignment = taLeftJustify
        DecimalPlaces = 8
        DisplayFormat = ',0.000000000'
        Anchors = [akLeft, akTop, akRight]
        MaxValue = 180.000000000000000000
        MinValue = -180.000000000000000000
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnChange = CurrencyEdit_latChange
      end
      object CurrencyEdit_lat: TCurrencyEdit
        Left = 8
        Top = 14
        Width = 145
        Height = 21
        Alignment = taLeftJustify
        DecimalPlaces = 10
        DisplayFormat = ',0.0000000000'
        Anchors = [akLeft, akTop, akRight]
        MaxValue = 90.000000000000000000
        MinValue = -90.000000000000000000
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnChange = CurrencyEdit_latChange
      end
    end
    object rg_CoordSystem: TRadioGroup
      Left = 450
      Top = 0
      Width = 96
      Height = 77
      Align = alRight
      ItemIndex = 0
      Items.Strings = (
        'CK42'
        'CK95'
        'WGS 84'
        #1043#1057#1050' 2011')
      TabOrder = 2
      OnClick = rg_CoordSystemClick
    end
  end
end
