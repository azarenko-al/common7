unit u_custom_Raster;

interface

type
  TCustomRaster = class(TObject)
  public
    RasterFileName: string;
    RasterWidth  : Integer;
    RasterHeight : Integer;

    GK_Zone: integer;
    Meridian : integer;

    // �������� - ���� ��������������
    PointCount : Integer;
    Points: array[0..30] of record
                              img_x,img_y: Integer;
                              x,y: Double;
                            end;

    procedure SaveToFile(aFileName: string); virtual; abstract;
    procedure LoadFromiFile(aFileName: string); virtual; abstract;
                
  end;

implementation

end.
