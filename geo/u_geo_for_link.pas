unit u_geo_for_link;

interface
uses
  u_geo_convert_new,
//  u_geo_convert,
  u_geo;

// ��������� ���������� ����� ����������
function geo_Link_GetPoints(aBLVector: TBLVector; aCount: Integer; aOFFSET_M:
    Integer = 100): TBLPointArray;


implementation

(*const
  DEF_OFFSET_M = 100;
*)

// -------------------------------------------------------------------
function geo_Link_GetPoints(aBLVector: TBLVector; aCount: Integer; aOFFSET_M:
    Integer = 100): TBLPointArray;
// -------------------------------------------------------------------
// ����� ��� ���������� ��������
// -------------------------------------------------------------------


var
  eRadius_m: double;

  blPoint1, blPoint2: TBLPoint;
  eCos: Double;
  eDist_m: Double;
  eSin: Double;
  iZone: Integer;
  x2: double;
  X2_: double;
  x3: double;
  X3_: double;

  xy1: TXYPoint;
  xy2: TXYPoint;
  xy3: TXYPoint;
  xy2_: TXYPoint;
  xy3_: TXYPoint;
  xy4: TXYPoint;


  y2: double;
  Y2_: double;
  y3: Double;
  Y3_: double;
begin
//  iRadius_m:= DEF_OFFSET_M*(aCount);
  eRadius_m:= aOFFSET_M*(aCount);



  iZone := geo_Get6ZoneBL(aBLVector.Point1);

  xy1:= geo_Pulkovo42_BL_to_XY(aBLVector.Point1, iZone);
  xy4:= geo_Pulkovo42_BL_to_XY(aBLVector.Point2, iZone);




  eDist_m:=geo_DistanceXY_m(xy1,xy4);
  Assert(eDist_m>0, 'eDist_m <=0');

  if eDist_m < 1000 then
    eRadius_m:=eRadius_m / 3;



  eSin:=(xy1.X-xy4.X) / eDist_m;
  eCos:=(xy4.Y-xy1.Y) / eDist_m;

 // A:=Arc

  x2:=eRadius_m;
  y2:=eRadius_m;

  x3:=eDist_m - eRadius_m;
  y3:=eRadius_m;



  X2_ := x2 * eCos + y2 * eSin;
  Y2_ :=-x2 * eSin + y2 * eCos;

  X3_ := x3 * eCos + y3 * eSin;
  Y3_ :=- x3 * eSin + y3 * eCos;


  xy2_.X := xy1.X + y2_;
  xy2_.Y := xy1.Y + x2_;

  xy3_.X := xy1.X + y3_;
  xy3_.Y := xy1.Y + x3_;


  setLength(Result, 4);

  // aBLPoints.Count:=4;
  Result[0]:=aBLVector.Point1;
  Result[1]:=geo_Pulkovo42_XY_to_BL (xy2_, iZone);
  Result[2]:=geo_Pulkovo42_XY_to_BL (xy3_, iZone);


  Result[3]:=aBLVector.Point2;



end;



end.
