unit u_geo_classes;

interface
uses
  Classes, 

  u_GEO;

type

  TBLPointItem = class(TCollectionItem)
  public
    BLPoint: TBLPoint;

    ID: Integer;
    Name : string;
  end;


  TBLPointList = class(TCollection)
  private
    function GetItems(Index: Integer): TBLPointItem;
  public
    constructor Create;

    function Add(aBLPoint: TBLPoint; aID: Integer; aName : string): TBLPointItem;
    function GetRoundBLRect: TBLRect;

    property Items[Index: Integer]: TBLPointItem read GetItems; default;
  end;




implementation


constructor TBLPointList.Create;
begin
  inherited Create (TBLPointItem);
end;


function TBLPointList.Add(aBLPoint: TBLPoint; aID: Integer; aName : string): TBLPointItem;
begin
  Assert(aID>0, 'Value <=0');

  Result := TBLPointItem(inherited Add);
  Result.blPoint:=ablPoint;

  Result.ID:=aID;
  Result.Name:=aName;

end;


function TBLPointList.GetItems(Index: Integer): TBLPointItem;
begin
  Result := TBLPointItem(inherited Items[Index]);
end;


function TBLPointList.GetRoundBLRect: TBLRect;
var
  i : Integer;
  points: TBLPointArray;
begin
  SetLength(points, Count);

  for I := 0 to Count-1 do
    points[i] := Items[i].BLPoint;

  Result := geo_DefineRoundBLRectForArrayDinamic(points);
end;


end.


//  function geo_DefineRoundBLRectForArrayDinamic(aPoints: TBLPointArray): TBLRect;



