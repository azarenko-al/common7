unit u_geo_poly;

interface
uses
 Math,

  u_func,
  u_GEO;



//function geo_IsXYPointInXYPolygon1(aXYPoint: TXYPoint; var aXYPolygon: TXYPointArray): Boolean;
function geo_IsXYPointInXYPolygon(aP: TXYPoint; var aPolygon: TXYPointArray):  boolean;



implementation

(*

type XYPoint = record 
x,y: double; 
end; 

function IsInsidePolygon(const aPolygon: array of XYpoint; const aP: XYPoint): 
boolean;
// returns "true" if aP is inside aPolygon
var
i: integer;
p1, p2: ^XYPoint;
begin
Result:= false;
p2:= @aPolygon[High(aPolygon)];
for i:=Low(aPolygon) to High(aPolygon) do begin
p1:= @aPolygon[i];
if ( (((p1.y<=aP.y) and (aP.y<p2.y)) or ((p2.y<=aP.y) and (aP.y<p1.y)))
and
(aP.x<(p2.x-p1.x)*(aP.y-p1.y)/(p2.y-p1.y)+p1.x) )
then Result:= not Result;
p2:= p1; 
end; 
end;


function PtInPolygon(const Pt: TFixedPoint; const Points: TArrayOfFixedPoint): Boolean;
var
  I: Integer;
  iPt, jPt: PFixedPoint;
begin
  Result := False;
  iPt := @Points[0];
  jPt := @Points[High(Points)];
  for I := 0 to High(Points) do
  begin
    Result := Result xor (((Pt.Y >= iPt.Y) xor (Pt.Y >= jPt.Y)) and
      (Pt.X - iPt.X < MulDiv(jPt.X - iPt.X, Pt.Y - iPt.Y, jPt.Y - iPt.Y)));
    jPt := iPt;
    Inc(iPt);
  end;
end;


*)


//-------------------------------------------------
function geo_IsXYPointInXYPolygon(aP: TXYPoint; var aPolygon: TXYPointArray):
    boolean;
//-------------------------------------------------
var
  i: integer;
  p1, p2: TXYPoint;
begin
  Assert(Length(aPolygon)>0 , 'Value <=0');


  p2:= aPolygon[High(aPolygon)];

  for i:=Low(aPolygon) to High(aPolygon) do
  begin
    p1:= aPolygon[i];

    if ( (((p1.y<=aP.y) and (aP.y<p2.y)) or ((p2.y<=aP.y) and (aP.y<p1.y)))
      and
      (aP.x<(p2.x-p1.x)*(aP.y-p1.y)/(p2.y-p1.y)+p1.x) )
    then
      Result:= not Result;

    p2:= p1;
  end;


(*
  Result := False;
  iPt := aPolygon[0];
  jPt := aPolygon[High(aPolygon)];

  for I := 0 to High(aPolygon) do
  begin
    Result := Result xor (((Pt.Y >= iPt.Y) xor (Pt.Y >= jPt.Y)) and
      (Pt.X - iPt.X < MulDiv(jPt.X - iPt.X, Pt.Y - iPt.Y, jPt.Y - iPt.Y)));
    jPt := iPt;

    iPt := aPolygon[i];
 //   Inc(iPt);
  end;
*)
end;







//-------------------------------------------------
function geo_IsXYPointInXYPolygon1(aXYPoint: TXYPoint; var aXYPolygon:
    TXYPointArray): boolean;
//-------------------------------------------------
    //-------------------------------------------------
    function PointDelta (A,B: TXYPoint): TXYPoint;
    //-------------------------------------------------
    // - ��������� �����
    begin
      Result.x:=A.x-B.x;
      Result.y:=A.y-B.y;
    end;

    //-------------------------------------------------
    function PolarAngle (aPoint: TXYPoint): double;
    //-------------------------------------------------
    // - �������� ���������� �����
    var alpha: double;
    begin
      with aPoint do
      begin
        if (x=0) and (y=0) then
          Result:=-1
        else

        if (x=0) then
          Result:=IIF(y>0, 90, 270)

        else begin
          alpha:=RadToDeg(ArcTan (y/x));
          if x>0 then Result:=IIF(y>=0, alpha, 360+alpha)
                 else Result:=180+alpha;
        end;
      end;
    end;

    //-------------------------------------------------
    function SignedAngle (aPoint, aStartPoint,aEndPoint : TXYPoint ): double;
    //-------------------------------------------------
    var p1,p2: TXYPoint;  angle1,angle2,delta: double;
    begin
      p1:=PointDelta (aStartPoint, aPoint);
      p2:=PointDelta (aEndPoint, aPoint);
      angle1:=PolarAngle (p1);
      angle2:=PolarAngle (p2);

      if (angle1<0) or (angle2<0) then begin Result:=180; exit; end;

      delta:=angle1 - angle2 ;
      if (delta=180) or (delta=-180) then Result:=180 else
      if (delta<-180) then Result:=delta+360 else
      if (delta>180)  then Result:=delta-360
                      else Result:=delta;
    end;
    //-------------------------------------------------

// - ����������, ��������� �� ����� ������ ��������
var
  total,x: double;
  i,iCount: integer;
  p1,p2: TXYPoint;
begin
  iCount:= Length(aXYPolygon);

  total := 0;
  for i:=0 to iCount-1 do begin
    p1:=aXYPolygon[i];
    if i<iCount-1 then p2:=aXYPolygon[i+1]
                  else p2:=aXYPolygon[0];
    x:=SignedAngle (aXYPoint, p1,p2);
    if (x=180) then begin Result:=true; Exit; end;
    total := total + x;
  end;
  total := Abs(total);
  if (total > -0.1) and (total < 0.1) then Result := False
  else if (total > 359.9) and (total < 360.1) then Result := True;


(*
 function PtInPoly
    (const Points: Array of TPoint; X,Y: Integer):
    Boolean;
 var Count, K, J : Integer;
 begin
   Result := False;
   Count := Length(Points) ;
   J := Count-1;
   for K := 0 to Count-1 do begin
    if ((Points[K].Y <=Y) and (Y < Points[J].Y)) or
       ((Points[J].Y <=Y) and (Y < Points[K].Y)) then
    begin
     if (x < (Points[j].X - Points[K].X) *
        (y - Points[K].Y) /
        (Points[j].Y - Points[K].Y) + Points[K].X) then
         Result := not Result;
     end;
     J := K;
   end;
 end;
*)


end;


end.
