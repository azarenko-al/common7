unit u_ozi_explorer;

interface
uses
  classes,SysUtils, StrUtils,

  u_custom_Raster,

  u_func,
  u_func_arrays,

  u_Geo,
  u_geo_convert_new
  ;


type
  TOziExplorerFile = class //(TCustomRaster)
  public
    RasterFileName: string;
    RasterWidth  : Integer;
    RasterHeight : Integer;

    GK_Zone: integer;
    Meridian : Double;

    // ���� ��������������
    Points: array[0..30] of record
                              img_x,img_y: Integer;
                              x,y: Double;
                              B,L: Double;
                            end;

    PointCount : Integer;


    procedure SaveToFile(aFileName: string); //override;
    procedure LoadFromiFile(aFileName: string); //override;
  end;

implementation

(*
const
  CRLF = #13+#10;

function ReplaceStr (Value, OldPattern, NewPattern: string): string;
begin
  Result:=StringReplace (Value, OldPattern, NewPattern, [rfReplaceAll, rfIgnoreCase]);
end;

*)

// ---------------------------------------------------------------
procedure TOziExplorerFile.SaveToFile(aFileName: string);
// ---------------------------------------------------------------
const
  OZI_HEADER =
    'OziExplorer Map Data File Version 2.2'+ CRLF +
    '[:raster]'   + CRLF +
    '[:raster]'   + CRLF +
    '1 ,Map Code,'+ CRLF +
    'Pulkovo 1942 (2),WGS 84,   0.0000,   0.0000,WGS 84'+ CRLF +
    'Reserved 1'    + CRLF +
    'Reserved 2'    + CRLF +
    'Magnetic Variation,,,E'  + CRLF +
    'Map Projection,Transverse Mercator,PolyCal,No,AutoCalOnly,No,BSBUseWPX,No'+ CRLF +
    '[:Points]'+
    'Point05,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point06,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point07,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point08,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point09,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point10,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point11,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point12,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point13,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point14,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point15,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point16,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point17,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point18,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point19,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point20,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point21,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point22,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point23,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point24,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point25,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point26,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point27,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point28,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point29,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point30,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    //'Projection Setup,     0.000000000,    39.000000000,     1.000000000,       500000.00,            0.00,,,,,'+ CRLF +
    'Projection Setup,     0.000000000,[:meridian],     1.000000000,       500000.00,            0.00,,,,,'+ CRLF +
    'Map Feature = MF ; Map Comment = MC     These follow if they exist'+ CRLF +
    'Track File = TF      These follow if they exist'          + CRLF +
    'Moving Map Parameters = MM?    These follow if they exist'+ CRLF +
    'MM0,Yes' + CRLF +
    'MMPNUM,4'+ CRLF +
    '[:MMPXY]'+
    '[:MMPLL]'+
    'MM1B,60.972260'+ CRLF +
    'MOP,Map Open Position,0,0'+ CRLF +
    'IWH,Map Image Width/Height,[:Image]'; //3234,3641';


  OZI_Points =
    'Point0%d,xy,%5d,%5d,in, deg,%4d,%8.4f,N,%4d,%8.4f,E, grid,   ,           ,           ,N';

  OZI_MMPXY =
    'MMPXY,%d,%d,%d';

  OZI_MMPLL =
    'MMPLL,%d,%11.6f,%11.6f';

  OZI_Image=
    '%d,%d';




//Point01,xy,  141,   86,in, deg,  56,  0.0000,N,  36,  0.0000,E, grid,   ,           ,           ,N


//Point10,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N


var
 // sZone,
 // sCoordinates: string;
  s,sText : string;
 // i,iZone: integer;
 // d: integer;
  //d1: integer;
  oSList: TStringList;
  sDir: string;
  sGif: string;

  sMeridian : string;

  XY: TXYPoint;
  BL: TBLPoint;
  d: Double;
  eMin_x: Double;
  eMin_y: Double;
  i: Integer;
  iDegree_x: Integer;
  iDegree_y: Integer;
  k: Integer;
  sImage: string;

  sPoints : string;
  sMMPXY : string;
  sMMPLL : string;

begin
  //UpdateRaster(aFileName);


  d:=GK_Zone*6-3;
  sMeridian := Format('%16.9f', [d]);

  sPoints:='';
  sMMPXY := '';
  sMMPLL := '';

  for i:=0 to PointCount-1 do
  begin
    XY.X := Points[i].x;
    XY.Y := Points[i].y;


    bl:= geo_GK_XY_to_Pulkovo42_BL(XY, GK_Zone);

    geo_DecodeDegree_into_Degree_and_Minute(bl.B, iDegree_x,eMin_x);
    geo_DecodeDegree_into_Degree_and_Minute(bl.L, iDegree_y,eMin_y);


//  OZI_Points =
 //   'Point0%d,xy,%5d,%5d,in, deg,%4d,%8.4f,N,%4d,%8.4f,E, grid,   ,           ,           ,N';


    s:=Format(OZI_Points, [i+1,
                          Points[i].img_x, Points[i].img_y,
                          iDegree_x, eMin_x,
                          iDegree_y, eMin_y]);
    sPoints:=sPoints + s + CRLF;

    s:=Format(OZI_MMPXY, [i+1, Points[i].img_x, Points[i].img_y]);
    sMMPXY  := sMMPXY + s + CRLF;

    s:=Format(OZI_MMPLL, [i+1, bl.L, bl.B]);
    sMMPLL  := sMMPLL + s + CRLF;

  end;


  sImage:=Format(OZI_Image, [Points[2].img_x+1, Points[2].img_y+1]);

  sText:=OZI_HEADER;
  sText:=ReplaceStr (sText, '[:raster]', ExtractFileName(RasterFileName));
  sText:=ReplaceStr (sText, '[:Points]', sPoints);
  sText:=ReplaceStr (sText, '[:MMPXY]', sMMPXY);
  sText:=ReplaceStr (sText, '[:MMPLL]', sMMPLL);
  sText:=ReplaceStr (sText, '[:Image]', sImage);
  sText:=ReplaceStr (sText, '[:Meridian]', sMeridian);


  // -------------------------
  oSList:=TStringList.Create;

  oSList.Text := sText;   
  oSList.SaveToFile(ChangeFileExt (aFileName, '.map'));

  FreeAndNil(oSList);    

end;

// ---------------------------------------------------------------
procedure TOziExplorerFile.LoadFromiFile(aFileName: string);
// ---------------------------------------------------------------
var
  sZone,sCoordinates: string;
  s,sText : string;
  i,iZone: integer;
 // d: integer;
  //d1: integer;
  oSList: TStringList;
  sDir: string;
  sGif: string;

  sMeridian : string;


  XY: TXYPoint;
  BL: TBLPoint;
  d: Double;
  eMin_x: Double;
  eMin_y: Double;
  iDegree_x: Integer;
  iDegree_y: Integer;
  k: Integer;
  sImage: string;

  sPoints : string;
  sMMPXY : string;
  sMMPLL : string;

  arr: TStrArray;
  iArrLen: Integer;
  ind: Integer;
  s1: string;

 // BL: TBLPoint;

//  I: Integer;
 // oSList: TStringList;
begin
  PointCount :=0;


  oSList:=TStringList.Create;
  oSList.LoadFromFile(aFileName);

  if oSList.Count>1 then
    RasterFileName := oSList[1];

  for I := 0 to oSList.Count - 1 do
  begin
    s:=oSList[i];

    arr:=StringToStrArray(s, ',');

    iArrLen  := Length(arr);



//        'Point05,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
//Point01,xy,  197,   76,in, deg,  52,  0.0000,N,  84,  0.0000,E, grid,   ,           ,           ,N

    // -------------------------
    if Eq(LeftStr(s, Length('Point')) , 'Point') then
    begin
      s1 := Copy(arr[0],6,2);
      //AsInteger(arr[1])-1;
      ind := StrToIntDef(s1,0);

 //     if arr[4]='in' then
      if Trim(arr[2])<>'' then
      begin
        Points[PointCount].img_x := AsInteger(arr[2]);
        Points[PointCount].img_y := AsInteger(arr[3]);


        bl.B := AsFloat(arr[6]) + AsFloat(arr[7]) /60;
        bl.L := AsFloat(arr[9]) + AsFloat(arr[10])/60;

     //   XY := geo_Pulkovo42_to_XY(bl, GK_Zone);


        Points[PointCount].B := bl.B;
        Points[PointCount].L := bl.L;


(*      bl:= geo_GK_XY_to_Pulkovo42_BL(XY, GK_Zone);

    geo_DecodeDegree_into_Degree_and_Minute(bl.B, iDegree_x,eMin_x);
    geo_DecodeDegree_into_Degree_and_Minute(bl.L, iDegree_y,eMin_y);


*)
(*
      Points[ind].x := XY.X;
      Points[ind].y := XY.Y;

*)

//        Points[PointCount].y := xy.y;
 //       Points[PointCount].x := xy.x;


        Inc(PointCount);
      end;


     // Meridian := Trunc(AsFloat(arr[2]));

     // GK_Zone := (Meridian+3) div 6;

     // if iArrLen=2 then
      //  PointCount := AsInteger(arr[1]);
    end else


    // -------------------------
    if Eq(LeftStr(s, Length('Projection Setup')) , 'Projection Setup') then
    begin
      Meridian := Trunc(AsFloat(arr[2]));

      GK_Zone := Round((Meridian+3) / 6);
                                                           
     // if iArrLen=2 then
      //  PointCount := AsInteger(arr[1]);
    end else

    // -------------------------
    if Eq(LeftStr(s, Length('MMPNUM')) , 'MMPNUM11') then
    begin
      if iArrLen=2 then
        PointCount := AsInteger(arr[1]);
    end else

    // -------------------------
    if Eq(LeftStr(s, Length('MMPXY')) , 'MMPXY111') then
    begin
      ind := AsInteger(arr[1])-1;

      Points[ind].img_x := AsInteger(arr[2]);
      Points[ind].img_y := AsInteger(arr[3]);

(*
    s:=Format(OZI_MMPXY, [i+1, Points[i].img_x, Points[i].img_y]);
    sMMPXY  := sMMPXY + s + CRLF;

    s:=Format(OZI_MMPLL, [i+1, bl.L, bl.B]);
    sMMPLL  := sMMPLL + s + CRLF;
*)
    end;

    // -------------------------
    if Eq(LeftStr(s, Length('MMPLL')) , 'MMPLL111') then
    begin
      ind := AsInteger(arr[1])-1;

      bl.L := AsFloat(arr[2]);
      bl.B := AsFloat(arr[3]);

      XY := geo_Pulkovo42_to_XY(bl, GK_Zone);

(*      bl:= geo_GK_XY_to_Pulkovo42_BL(XY, GK_Zone);

    geo_DecodeDegree_into_Degree_and_Minute(bl.B, iDegree_x,eMin_x);
    geo_DecodeDegree_into_Degree_and_Minute(bl.L, iDegree_y,eMin_y);


*)

      Points[ind].x := XY.X;
      Points[ind].y := XY.Y;

    end;

    // -------------------------
    if Eq(LeftStr(s, Length('IWH')) , 'IWH') then
    begin
    //  ind := AsInteger(arr[1]);
     //IWH,Map Image Width/Height,4677,6817
      RasterWidth  := AsInteger(arr[2]);
      RasterHeight := AsInteger(arr[3]);
    end;
  end;

  FreeAndNil(oSList);


  for I := 0 to PointCount - 1 do
  begin

    bl.B := Points[i].b;
    bl.L := Points[i].l;

    XY := geo_Pulkovo42_to_XY(bl, GK_Zone);

//    xy:= geo_GK_XY_to_Pulkovo42_BL(bl, GK_Zone);

    Points[i].x := XY.x;
    Points[i].y := XY.y;

  end;


     //   XY := geo_Pulkovo42_to_XY(bl, GK_Zone);


     //   Points[PointCount].B := bl.B;
     //   Points[PointCount].L := bl.L;


(*      bl:= geo_GK_XY_to_Pulkovo42_BL(XY, GK_Zone);

    geo_DecodeDegree_into_Degree_and_Minute(bl.B, iDegree_x,eMin_x);
    geo_DecodeDegree_into_Degree_and_Minute(bl.L, iDegree_y,eMin_y);


*)
(*
      Points[ind].x := XY.X;
      Points[ind].y := XY.Y;

*)

//        Points[PointCount].y := xy.y;
 //       Points[PointCount].x := xy.x;


end;

end.
