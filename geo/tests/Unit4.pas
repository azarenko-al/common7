unit Unit4;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,

  u_func,
  u_geo,

  fr_geo_Coordinates, StdCtrls
  ;

type
  TForm4 = class(TForm)
    GroupBox1: TGroupBox;
    Button1: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    Fframe_geo_Coordinates: Tframe_geo_Coordinates;

    { Private declarations }


  public
    { Public declarations }
  end;

var
  Form4: TForm4;

implementation

{$R *.dfm}

procedure TForm4.Button1Click(Sender: TObject);
var
  bl: TBLPoint;
begin
  bl:=Fframe_geo_Coordinates.BLPoint_Pulkovo;

  Edit1.Text:= FloatToStr(bl.B);
  Edit2.Text:= FloatToStr(bl.L);

end;



procedure TForm4.FormCreate(Sender: TObject);
var
  bl: TBLPoint;
  s: string;
begin
  s:=geo_FormatDegree_Lon(184);
  s:=geo_FormatDegree_Lon(-10);


  s:=geo_FormatDegree_Lat(4);
  s:=geo_FormatDegree_Lat(-10);

  //////////////////



  CreateChildForm(Tframe_geo_Coordinates, Fframe_geo_Coordinates, GroupBox1);
//  Fframe_geo_Coordinates.OnChange:=DoOnPosChange;

  Fframe_geo_Coordinates.Set_LatLon_pulkovo(-56, 190);


  bl:=Fframe_geo_Coordinates.BLPoint_Pulkovo;

  Edit1.Text:= FloatToStr(bl.B);
  Edit2.Text:= FloatToStr(bl.L);


end;

end.
