unit Unit4;

interface

uses
  fr_geo_Coordinates,

  u_geo,
  u_func,

  SysUtils, Classes, Controls, Forms,
  StdCtrls;

type
  TForm4 = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
      Fframe_geo_Coordinates: Tframe_geo_Coordinates;
    procedure DoOnPosChange(Sender: TObject);


    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form4: TForm4;

implementation

{$R *.dfm}

procedure TForm4.FormDestroy(Sender: TObject);
begin
  FreeAndNil(Fframe_geo_Coordinates);

end;

procedure TForm4.FormCreate(Sender: TObject);
begin
  CreateChildForm(Tframe_geo_Coordinates, Fframe_geo_Coordinates, GroupBox1);

//  Fframe_geo_Coordinates.Set_BLPoint_and_CoordSys (MakeBLPoint(0,0), EK_CK_42 );
  Fframe_geo_Coordinates.Set_BLPoint_and_CoordSys (MakeBLPoint(10,10), EK_WGS84 );
  Fframe_geo_Coordinates.OnPosChange:=DoOnPosChange;


  //aBLPoint: TBLPoint         


//  Fframe_geo_Coordinates.Set_BLPoint_Pulkovo_and_CoordSys(MakeBLPoint(0,0), EK_WGS84 );
//  Fframe_geo_Coordinates.Set_BLPoint_Pulkovo_and_CoordSys(MakeBLPoint(10,10), EK_CK_42 );

// Fframe_geo_Coordinates.BLPoint_Pulkovo:=aBLPoint;
//    Fframe_geo_Coordinates.DisplayCoordSys:=IOptions.Get_CoordSys;

end;

procedure TForm4.DoOnPosChange(Sender: TObject);
begin
  with Fframe_geo_Coordinates.getBLPoint_Pulkovo do
    Label1.Caption:='Pulkovo   lat:'+  geo_DegreeToStr (b)+ '    lon:' +geo_DegreeToStr (l);
    

 // Frec.BLPoint_Pulkovo:=Fframe_geo_Coordinates.BLPoint_Pulkovo;
//  PosChanged();
end;



end.
