unit u_GeoTif;

interface

uses  Classes, Sysutils, Dialogs,

  u_geo,

  DSpGlobals,
  DSpGeoTIFF;

type
  TGeoTif = class
  private
    FArr: TDSpExtentArray;

    FInfo: TDSpRasterInfo;

    buff_: array of
           array of smallint;

  public
//    Blank: -9999;

    BLRect_WGS: TBLRect;
    // aB, aL - � WGS
    function FindByBL(aB, aL: double; var aValue: smallint): Boolean;

    procedure Open(aFileName: string);
  end;


//==============================================================================
implementation
//==============================================================================

//---------------------------------------------------------------
function TGeoTif.FindByBL(aB, aL: double; var aValue: smallint): boolean;
//--------------------------------------------------------------
var
  b: boolean;
  eStep: Double;
  iCol: Integer;
  iRow: Integer;
begin
  Result:=False;

  eStep:=FInfo.PixelSize;

  if (BLRect_WGS.BottomRight.B < aB) and (aB < BLRect_WGS.TopLeft.B) and
     (BLRect_WGS.TopLeft.L     < aL) and (aL < BLRect_WGS.BottomRight.L) then
  begin
    iRow := Integer(Round((BLRect_WGS.TopLeft.B - aB) / eStep));
    iCol := Integer(Round((aL - BLRect_WGS.TopLeft.L) / eStep));

    Result:=(iRow>=0) and (iCol>=0) and
            (iRow<=FInfo.Rows-1) and (iCol<=FInfo.Columns-1);

    if Result then
    begin
     // try
        aValue := buff_[iRow][iCol];
     // except
     // end;
    end;

//      Result:=true;
  end;
end;

// ---------------------------------------------------------------
procedure TGeoTif.Open(aFileName: string);
// ---------------------------------------------------------------
var
  i: Integer;

  oDSpGeoTIFF: TDSpGeoTIFF;

  iRow: Integer;
  oSList: TStringList;
  iCol: Integer;
  s: string;

  eMin : Double;
  eNorth: Double;
  eWest: Double;
  eEast: Double;
  eSouth: Double;
begin
  oDSpGeoTIFF:=TDSpGeoTIFF.Create;
  oDSpGeoTIFF.Open(aFileName, true);

  oDSpGeoTIFF.RasterInfo(0,FInfo);
  FArr:=oDSpGeoTIFF.Extent(0);

  SetLength(buff_, FInfo.Rows);
  for iRow := 0 to FInfo.Rows - 1 do
    SetLength(buff_[iRow], FInfo.Columns);

  for iRow := 0 to FInfo.Rows - 1 do
    oDSpGeoTIFF.ReadData(0,0,  iRow,0, 1,FInfo.Columns, buff_[iRow]);

  FreeAndNil(oDSpGeoTIFF);

  eNorth := FArr[dspNorth];
  eSouth := FArr[dspSouth];
  eWest  := FArr[dspWest];
  eEast  := FArr[dspEast];

  BLRect_WGS.TopLeft.B     := eNorth;
  BLRect_WGS.BottomRight.B := eSouth;

  BLRect_WGS.TopLeft.L     := eWest;
  BLRect_WGS.BottomRight.L := eEast;
end;



end.





