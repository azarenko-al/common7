unit I_rel_Matrix1;

interface
{$DEFINE test}

uses
  Graphics,

  u_Geo;

const
  DEF_CLU_OPEN_AREA = 0;  // 1- ��� (1)
  DEF_CLU_FOREST    = 1;  // 1- ��� (1)
  DEF_CLU_WATER     = 2;  // 3- ���
  DEF_CLU_COUNTRY   = 3;  // 3- ���
  DEF_CLU_ROAD      = 4;
  DEF_CLU_RailROAD  = 5;
//  DEF_CLU_BOLOTO_NEPROHODIMOE  = 6; // 6 - ������������ ������
  DEF_CLU_CITY      = 7;  // 7- �����
  DEF_CLU_BOLOTO    = 8; // ���������� ������
  DEF_CLU_ONE_BUILD = 73; // 73- ���.����
  DEF_CLU_ONE_HOUSE = 31; // 31- ���

//  FILTER_RLF_MATRIX      = '������� ����� (*.rlf;*.hgt)|*.rlf;*.hgt';
  FILTER_RLF_MATRIX      = '������� ����� (*.rlf;*.ter;*.hgt)|*.rlf;*.ter;*.hgt';
  FILTER_RLF_MATRIX_EXT  = '*.rlf;*.hgt;*.ter';


  MAX_REL_POINTS = 10000;
  EMPTY_HEIGHT  : smallint = High(smallint);

type

  //-------------------------------------------------
  TrelProfilePointRec = packed record  // ������ �� ������� ���������
  //-------------------------------------------------
    Rel_H         : smallint; // ������ ������� (��� ������� ����)

    Clutter_H     : single;   // ������ �������� - �� ���
                              //�������- ��� ���������������� ��������������
    Clutter_Code  : smallint; // ��� �������� ��������

    Distance_KM   : double;   // ���������� �� ��������� ����� (0)
    Earth_H       : double;   // ������ ����� (��� ����������� �������)
    Full_H       : double;

    x       : double;
    y       : double;

  end;

  // ---------------------------------------------------------------
  TBuildProfileParamRec = packed record
  // ---------------------------------------------------------------
    XYVector       : TXYVector;
    BLVector       : TBLVector;
    Step_m         : double;
    Zone           : integer;
    Refraction     : double;

    MaxDistance_KM : Double; //����������� �����

    BL_Length_KM   : Double; // ����� ��� ������� BL
  end;

  //-------------------------------------------------------------------
  TrelProfileRec = packed record
  //-------------------------------------------------------------------
    Count       : integer;
    XYVector    : TXYVector;
    BLVector    : TBLVector;

    StepX,StepY : double; //km
    Step_R_km   : double; //km

    Distance_KM : double; //km

    Items: array[0..MAX_REL_POINTS-1] of TrelProfilePointRec;
  end;

  // ������ ������� �������
  Trel_Record = packed record
    Rel_H        : smallint;  // ������ ������� � ����� ��
    Clutter_Code : byte;
    Clutter_H    : byte;
  end;

  //-------------------------------------------------------------------
  TrelMatrixInfoRec = packed record
  //-------------------------------------------------------------------
    RowCount,ColCount : integer;

    MatrixType: (mtLonLat,mtXY); //mtNone,
//    MatrixType: (mtNone,mtXY);

    // BL matrix
    BLBounds    : TBLRect;
    StepB,StepL : double;

    // XY matrix
    XYBounds: TXYRect;
    ZoneNum : integer;
    StepX,StepY: double;

    FileSize: int64;

    GroundMinH,GroundMaxH: double;

    NullValue : Integer;

   // ZoneNum : Byte;
    Projection: string; //array [0..2] of Char;  // 'GK','UTM' - ������������� �����

  end;




{$IFNDEF test}
//  function GetInterface_IReliefEngineX(var Obj): HResult; stdcall;
{$ENDIF}

const
  DEF_CLUTTER_ARR : array[0..8-1] of record
                                     Code   : Integer;
                                     Color  : Integer;
                                     Height : Integer;
                                     Name   : string;
                                   end
  =
  (
   (Code: DEF_CLU_OPEN_AREA; Color: clWhite),

   (Code: DEF_CLU_FOREST;    Color: clGreen; Height:20),
   (Code: DEF_CLU_WATER;     Color: clBlue),
   (Code: DEF_CLU_COUNTRY;   Color: clGray),
   (Code: DEF_CLU_ROAD;      Color: clYellow),
   (Code: DEF_CLU_CITY;      Color: clRed),
   (Code: DEF_CLU_ONE_BUILD; Color: clRed),
   (Code: DEF_CLU_ONE_HOUSE; Color: clRed)
  );

//
//  DEF_CLU_OPEN_AREA = 0;  // 1- ��� (1)
//  DEF_CLU_FOREST    = 1;  // 1- ��� (1)
//  DEF_CLU_WATER     = 2;  // 3- ���
//  DEF_CLU_COUNTRY   = 3;  // 3- ���
//  DEF_CLU_ROAD      = 4;
//  DEF_CLU_RailROAD  = 5;
//  DEF_CLU_CITY      = 7;  // 7- �����
//  DEF_CLU_BOLOTO    = 8; // 31- ���
//  DEF_CLU_ONE_BUILD = 73; // 73- ���.����
//  DEF_CLU_ONE_HOUSE = 31; // 31- ���
//
//

implementation


end.

(*



var
  IReliefEngine: IReliefEngineX;

//  function IReliefEngine: IReliefEngineX;

  function IReliefEngine_Load: Boolean;
  procedure IReliefEngine_UnLoad;



implementation

//const
//  DEF_DLL = 'dll_rel.dll';
////DllGetInterface_IReliefEngineX
//// function DllGetInterface(const IID: TGUID; var Obj): HResult; stdcall; external DEF_DLL;
// function GetInterface_IReliefEngineX(var Obj): HResult; stdcall; external DEF_DLL;
//            //         function GetInterface_IReliefEngineX(var Obj): HResult; stdcall;


{$IFDEF test}
uses
   x_rel;
{$ELSE}
const
  DEF_DLL = 'dll_rel.dll';
//DllGetInterface_IReliefEngineX
// function DllGetInterface(const IID: TGUID; var Obj): HResult; stdcall; external DEF_DLL;
 function GetInterface_IReliefEngineX(var Obj): HResult; stdcall; external DEF_DLL;
            //         function GetInterface_IReliefEngineX(var Obj): HResult; stdcall;


{$ENDIF}



var
  LHandle : Integer;



// ---------------------------------------------------------------
function IReliefEngine_Load: Boolean;
// ---------------------------------------------------------------
begin
{$IFDEF test}
//  Result := u_rel_engine.DllGetInterface (IReliefEngineX, IReliefEngine)=0;

  Result := x_rel.GetInterface_IReliefEngineX(IReliefEngine)=0;

  {$else}
 // Result := GetInterface_dll(DEF_DLL, LHandle, IReliefEngineX, IReliefEngine)=0;

//  DllGetInterface(IReliefEngineX, IReliefEngine);

  GetInterface_IReliefEngineX(IReliefEngine);

{$ENDIF}

  Assert(Assigned(IReliefEngine), 'IReliefEngine not assigned');

end;


procedure IReliefEngine_UnLoad;
begin
  IReliefEngine := nil;

{$IFNDEF test}
 // UnloadPackage_dll(LHandle);
{$ENDIF}
end;


initialization
finalization

end.






(*
  function BuildProfileXY  (var aProfileRec: TrelProfileRec;
                              aXYVector: TXYVector;
                              aStep: integer;
                              aZone: integer;
                              aRefraction: double;
                              aMaxDistance_KM: double
                              ): boolean;

    function BuildProfileBL  (var aProfileRec: TrelProfileRec;
                              aBLVector: TBLVector;
                              aStep: integer;
                              aRefraction: double;
                              aMaxDistance_KM: double
                              ): boolean;

*)



// ---------------------------------------------------------------
function Get_IReliefEngineX: IReliefEngineX;
// ---------------------------------------------------------------
var
  b: boolean;
begin
 // b := GetInterface_IReliefEngineX (Result)=0;


{$IFDEF test}
  b := x_rel.DllGetInterface (IReliefEngineX, Result)=0;
{$else}
//  b := GetInterface_dll(DEF_DLL, LHandle, IReliefEngineX, Result)=0;
//  DllGetInterface(IReliefEngineX, Result);

  b := GetInterface_IReliefEngineX (Result)=0;

{$ENDIF}

  Assert(Assigned(Result), 'IReliefEngineX not assigned');

end;




//
//type
//
//  IReliefEngineX_______ = interface(IInterface)
//  ['{4299880F-485E-40BC-877E-A2E1E879FAB5}']
//
//    function AddFile(aFileName: ShortString): Boolean; stdcall;
//    procedure RemoveFile(aFileName: ShortString); stdcall;
//
//    function FileNameExists(aFileName: ShortString): boolean; stdcall;
//
//    procedure Clear; stdcall;
//
//    function  FindPointXY  (aPoint: TXYPoint; aZoneNum: integer; var aRec: Trel_Record): boolean; stdcall;
//
//    function  FindPointBLInFile (aFileName: ShortString; aBLPoint: TBLPoint;
//                                  var aRec: Trel_Record;
//                                  var aZone: Integer
//                                  ): boolean; stdcall;
//
//
//    function  FindPointBL  (aBLPoint: TBLPoint; var aRec: Trel_Record): boolean; stdcall;
//    function  FindPointBL_with_FileName (aBLPoint: TBLPoint; var aRec: Trel_Record; var aFileName: ShortString): boolean; stdcall;
//
//    function GetBest6Zone (aDefaultZone: integer; aXYPoint: TXYPoint): integer;  stdcall;
//
//    function BuildProfileXY  (var aProfileRec: TrelProfileRec;
//   //                           var aRec: TBuildProfileParamRec
//                              var aParams: TBuildProfileParamRec;
//
//                              aXYVector: TXYVector;
//                              aStep_m: integer;
//                              aZone: integer;
//                              aRefraction: double;
//                              aMaxDistance_KM: double
//                              ): boolean; stdcall;
//
//    function BuildProfileBL  (var aProfileRec: TrelProfileRec;
//                              var aRec: TBuildProfileParamRec;
//
//                           //   aRec: TBuildProfileBuildRec;
//                              aBLVector: TBLVector;
//                              aStep_m: integer;
//                              aRefraction: double;
//                              aMaxDistance_KM: double
//
//                              ): boolean; stdcall;
//
//    function Count: Integer; stdcall;
//
//    procedure Dlg_ShowMatrixList; stdcall;
//
//    function GetMatrixInfoStr(aFileName: ShortString): ShortString; stdcall;
//
//    function GetMatrixInfo (aFileName: ShortString; var aRec: TrelMatrixInfoRec): boolean;stdcall;
//     //external DLL_PATH; stdcall;
//
//  end;
//

//function DllGetInterface(const IID: TGUID; var Obj): HResult; stdcall;


