unit u_rel_Neva_Matrix;

interface
uses
  SysUtils, Classes,

  I_rel_Matrix1,
  u_rel_Matrix_base,

  u_files,
  u_geo_mif,
  u_Geo_convert,
  u_GEO,
  u_VirtFile;


type
  TRel_Rec = packed record RelHeight: single; end;//rel - ������� � �������� �������
  TMrf_Rec = packed record LocHeight: byte;   end;//mrf - ������ ������� ���������
  TGSM_Rec = packed record LocCode  : word;   end;//gsm - ���� ������� ���������


  TmrfMatrixFile = class;
  TgsmMatrixFile = class;

  //----------------------------------------------------
  TnevaRelMatrix = class(TrelMatrixBase)
  //----------------------------------------------------
  private
    FFileStream: TFileStream;

    // ����� mrf � gsm � ��������� ���������� ?
    FCoverageFileExists: boolean;

    FmrfMatrixFile: TmrfMatrixFile;
    FgsmMatrixFile: TgsmMatrixFile;
    // mrf - ������ ������� ���������
    // gsm - ���� ������� ���������
  protected
     function GetItem (aRow,aCol: integer): Trel_Record; override;
     
  public
    constructor Create;
    destructor Destroy; override;

    function  Read (Index: integer; var Rec: TRel_Rec): boolean;
    function  Open (aFileName: string): boolean;

//    function  GetZoneNum (): integer;

    function  ReadCellXY (aX,aY: double; var aRec: Trel_Record): boolean;

    function  FindPointBL (aBLPoint: TBLPoint; var aRec: Trel_Record): boolean;

    procedure SaveHeaderToMIF (aFileName,aBitmapFileName: string);

    procedure SaveInfoToFile(aFileName: string);

  end;


  TrelXYFile = class
    function LoadBoundsFromFile (aFileName: string): TXYRect;
    procedure SaveBoundsToFile   (aFileName: string; aXYBounds: TXYRect);
  end;

  TmrfMatrixFile = class(TVirtualFile)
    constructor Create;
    function GetItem (Index:integer; var Rec: TMrf_Rec): boolean;
  end;

  TgsmMatrixFile = class(TVirtualFile)
    constructor Create;
    function GetItem (Index:integer; var Rec: Tgsm_Rec): boolean;
  end;




//==================================================
implementation

uses IniFiles;

//==================================================

type
  TGauss = packed record x,y: double; end;

const
  MAX_BUF_COUNT = 6000;
  RELIEF_BMP_FORMAT = 'bmp';

type
  //-------------------------------------------
  // ��������� ��� ����� ������� �������
  //-------------------------------------------
  TRelHeader = packed record
  //-------------------------------------------
    mark: array[1..4] of char; // 'RLF0' - ������������� �����
    data, // �������� ������ (�� ������ �����)
    res1,res2: longint;

    NX : integer; // NX - ���������� ����� � ����������� ��� X
    NZ : integer; // NY - ���������� ����� � ����������� ��� Y (= 1)
    NY : integer; // NZ - ���������� ����� � ����������� ��� Z
    FN : integer; // FN - �����(= 0)

    len, // ������ ������ � ������
    fmt, // ������ ������
    res3,res4: byte;

    Xmin  : DOUBLE; //  Xmin - X-���������� 0-� ����� �������
    Zmin  : DOUBLE; //  Ymin - Y-���������� 0-� ����� �������
    Ymin  : DOUBLE; //  Zmin - Z-���������� 0-� ����� �������
    Xstep : DOUBLE; //  Xstep - ��� �� ��� X
    Zstep : DOUBLE; //  Ystep - ��� �� ��� Y
    Ystep : DOUBLE; //  Zstep - ��� �� ��� Z

    xmax,zmax,ymax: double;
  end;



constructor TnevaRelMatrix.Create;
begin
  inherited;
  HeaderLen:=SizeOf(TRelHeader);
  RecordLen:=SizeOf(Trel_Rec);

  FmrfMatrixFile:=TmrfMatrixFile.Create;
  FgsmMatrixFile:=TgsmMatrixFile.Create;

  MatrixType:=mtXY_;

end;


destructor TnevaRelMatrix.Destroy;
begin
  FmrfMatrixFile.Free;
  FgsmMatrixFile.Free;

  inherited;
end;


//--------------------------------------------------
// TnevaRelMatrix
//--------------------------------------------------

{function TnevaRelMatrix.GetZoneNum (): integer;
begin
  Result:=geo_Get6ZoneY (XYBounds.TopLeft.Y);
end;
}

//--------------------------------------------------
function TrelXYFile.LoadBoundsFromFile (aFileName: string): TXYRect;
//--------------------------------------------------
var gauss : array[0..3] of tgauss;
    xy: TFileStream;
begin
  Result:=MakeXYRect (0,0,0,0);

  try // ������ ���������
    xy:=TFileStream.Create(aFileName, fmShareDenyNone);
    xy.Read (gauss, SizeOf(gauss));

    with Result do begin
      TopLeft.x:=gauss[0].x;      TopLeft.y:=gauss[0].y;
      BottomRight.x:=gauss[2].x;  BottomRight.y:=gauss[2].y;
    end;
  finally
    xy.Free;
  end;
end;


//--------------------------------------------------
procedure TrelXYFile.SaveBoundsToFile (aFileName: string; aXYBounds: TXYRect);
//--------------------------------------------------
type tgauss = packed record x,y: double; end;
var gauss: array[0..3] of tgauss;   xy: TFileStream;
begin
  with aXYBounds do
  begin
    gauss[0].x:=TopLeft.x;
    gauss[0].y:=TopLeft.y;

    gauss[1].x:=TopLeft.x;
    gauss[1].y:=BottomRight.y;

    gauss[2].x:=BottomRight.x;
    gauss[2].y:=BottomRight.y;

    gauss[3].x:=BottomRight.x;
    gauss[3].y:=TopLeft.y;
  end;

  try // ������ ���������
    xy:=TFileStream.Create(aFileName, fmCreate);
    xy.Write (gauss, SizeOf(gauss));
  finally
    xy.Free;
  end;
end;


//--------------------------------------------------
function TnevaRelMatrix.Open (aFileName: string): boolean;
//--------------------------------------------------
var fname_gsm,fname_mrf: string;
    FHeader: TRelHeader;

    oXYFile: TrelXYFile;

begin
  Result:=inherited OpenFile (aFileName);
  
  if Result=True then
  begin
    ReadHeader (FHeader); //��������� ��������� REL

    HeaderLen:=FHeader.data; //���������� ������ ���������

    RowCount  :=FHeader.Ny-1;
    ColCount  :=FHeader.Nx-1;
    GroundMinH:=Round(FHeader.Zmin);
    GroundMaxH:=Round(FHeader.Zmax);

    oXYFile:=TrelXYFile.Create;
    XYBounds:=oXYFile.LoadBoundsFromFile (ChangeFileExt(aFileName,'.xy'));
    oXYFile.Free;

    assert (XYBounds.TopLeft.X > 0);
    assert (XYBounds.TopLeft.Y > 0);
    assert (XYBounds.BottomRight.X > 0);
    assert (XYBounds.BottomRight.Y > 0);


    ZoneNum:=geo_Get6ZoneY (XYBounds.TopLeft.Y);

    Assert(ZoneNum>0);

    with XYBounds do
    begin
      StepX := (TopLeft.x     - BottomRight.x) / RowCount ;
      StepY := (BottomRight.y - TopLeft.y) / ColCount ;

      if (StepX<0) or (StepY<0) then
        raise Exception.Create('');
    end;

    Result:=true;
  end;

  fname_gsm:=ChangeFileExt (aFileName,'.gsm');
  fname_mrf:=ChangeFileExt (aFileName,'.mrf');

  if FileExists(fname_mrf) and FileExists(fname_gsm) then
  begin
    FmrfMatrixFile.Open (fname_mrf);
    FgsmMatrixFile.Open (fname_gsm);
    FCoverageFileExists:=True;
  end else
    FCoverageFileExists:=False;

end;

//------------------------------------------------------
procedure TnevaRelMatrix.SaveHeaderToMIF (aFileName,aBitmapFileName: string);
//------------------------------------------------------
var rec: TMifHeaderInfoRec;
begin
{
//  rec.FileName:=ChangeFileExt (aFileName, '.tab');
  rec.BitmapFileName:=aBitmapFileName;
  rec.TopLeft:=XYBounds.TopLeft;
  rec.StepX:=StepX;
  rec.StepY:=StepY;

  SaveStringToFile (geo_MakeXYHeaderForMIF (rec), aFileName);
 }

  {
  rec.RowCount:=FBitmapRowCount;
  rec.ColCount:=FBitmapColCount;
  }


  {
  geo_MakeXYHeaderForMIF (ChangeFileExt(aFileName, '.'+ RELIEF_BMP_FORMAT),
   XYBounds.TopLeft, RowCount,ColCount,
   StepX, StepY),
     ChangeFileExt (aFileName, '.tab'));
  }
end;


//-----------------------------------------------------------
function TnevaRelMatrix.GetItem (aRow,aCol: integer): Trel_Record;
//-----------------------------------------------------------
var rec_Rel: TRel_Rec;
    rec_Mrf: TMrf_Rec;
    rec_Gsm: TGSM_Rec;
    bool: boolean;
begin
  bool:=Read(aRow*(ColCount+1)+aCol, rec_Rel);

  if not bool then begin
    Result.Rel_H:=EMPTY_HEIGHT;
    Exit;
  end;

  try
  if FCoverageFileExists then
  begin
    rec_Mrf.LocHeight:=0;
    rec_Gsm.LocCode  :=0;

{ TODO : remo }
    FmrfMatrixFile.GetItem (aRow*ColCount+aCol, rec_Mrf);
    FgsmMatrixFile.GetItem (aRow*ColCount+aCol, rec_Gsm);
  end else begin
    rec_Mrf.LocHeight:=0;
    rec_Gsm.LocCode  :=0;
  end;
  
  except end;


  try
    Result.Rel_H:=Round (rec_Rel.RelHeight);
  except
//    on E: Exception do: ;
  end;
  Result.Clutter_H :=rec_Mrf.LocHeight;
  Result.Clutter_Code:=rec_Gsm.LocCode;

  // �������� ��������� ----------------
  if Result.Clutter_Code=0 then
    Result.Clutter_H:=0;
end;


constructor TgsmMatrixFile.Create;
begin
  inherited;
  RecordLen:=SizeOf(Tgsm_Rec);
end;

//-------------------------------------------------
function ReduceIntToByte (Value: integer): byte;
//-------------------------------------------------
begin
  if Value > High(byte) then Result:=High(byte) else
  if Value < 0          then Result:=0
                        else Result:=Value;
end;



function TgsmMatrixFile.GetItem (Index:integer; var Rec:Tgsm_Rec): boolean;

  function MacWordToPC(Value : word): word;
  asm xchg Al,Ah
  end;
begin
  Result:=ReadRecord (Index, @Rec);
  Rec.LocCode:=ReduceIntToByte(MacWordToPC(Rec.LocCode));
end;

// -------------------------------------------------------------------
// TmrfMatrixFile
// -------------------------------------------------------------------
constructor TmrfMatrixFile.Create;
begin
  inherited;
  HeaderLen:=SizeOf(TRelHeader);
  RecordLen:=SizeOf(Tmrf_Rec);
end;


function TmrfMatrixFile.GetItem (Index:integer; var Rec:Tmrf_Rec): boolean;
begin
  Result:=ReadRecord (Index, @Rec);
  Rec.LocHeight:=ReduceIntToByte(Rec.LocHeight);
end;


function TnevaRelMatrix.Read (Index:integer; var Rec:Trel_Rec): boolean;
begin
  try
    Result:=ReadRecord (Index, @Rec);
  except end;  
end;


//---------------------------------------------------------------
function TnevaRelMatrix.FindPointBL (aBLPoint: TBLPoint; var aRec: Trel_Record): boolean;
//---------------------------------------------------------------
var xyPos: TXYPoint;
begin
  xyPos:=geo_BL_to_XY (aBLPoint, ZoneNum );
  Result:=ReadCellXY (xyPos.X, xyPos.Y, aRec);
end;

{//---------------------------------------------------------------
function TnevaRelMatrix.FindCellXY (aX,aY: double; var aRow,aCol: integer): boolean;
//--------------------------------------------------------------
begin
  Result:= ((FXYBounds.BottomRight.x < aX) and (aX < FXYBounds.TopLeft.x) and
            (FXYBounds.TopLeft.y     < aY) and (aY < FXYBounds.BottomRight.y));

  aRow := Integer(Round((FXYBounds.TopLeft.x - aX) / StepX));
  aCol := Integer(Round((aY - FXYBounds.TopLeft.y) / StepY));

  if (aRow<0) or (aCol<0) then
    raise Exception.Create('');
end;
}

//--------------------------------------------------
function TnevaRelMatrix.ReadCellXY (aX,aY: double; var aRec: Trel_Record): boolean;
//--------------------------------------------------
var r,c: integer;
begin
  Result:=FindCellXY (aX,aY, r,c);
  if Result then
    aRec:=GetItem (r,c);
end;


procedure TnevaRelMatrix.SaveInfoToFile(aFileName: string);
var
  ini: TIniFile;
const
  DEF_MAIN = 'main';
begin
  ini:=TIniFile.Create(aFileName);

  ini.WriteInteger(DEF_MAIN, 'ColCount'    , ColCount);
  ini.WriteInteger(DEF_MAIN, 'RowCount'    , RowCount);

//  ini.WriteInteger(DEF_MAIN, XYBounds   , XYBounds);

//  ini.WriteInteger(DEF_MAIN, 'GroundMinH' , GroundMinH);
//  ini.WriteInteger(DEF_MAIN, 'GroundMaxH' , GroundMaxH);

  ini.Free;
end;



end.





{
function TnevaRelMatrix.OpenWrite(aFileName: string): boolean;
begin
  try
    FFileStream:=TFileStream.Create(aFileName,fmOpenWrite);
  except
    FFileStream.Free;
  end;

end;}


          {






}

