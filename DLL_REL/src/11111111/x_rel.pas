unit x_rel;

interface
uses
  ComObj,ActiveX;


  function DllGetInterface(const IID: TGUID; var Obj): HResult; stdcall;
  function GetInterface_IReliefEngineX(var Obj): HResult; stdcall;



exports
  GetInterface_IReliefEngineX,
  DllGetInterface;


implementation

uses
  I_rel_Matrix1,
  u_rel_engine;



// ---------------------------------------------------------------
function DllGetInterface(const IID: TGUID; var Obj): HResult;
// ---------------------------------------------------------------
begin
  Result:=S_FALSE; Integer(Obj):=0;

  if IsEqualGUID(IID, IReliefEngineX) then
  begin
    with TRelEngine.Create do
      if GetInterface(IID,OBJ) then
        Result:=S_OK
  end;
end;



// ---------------------------------------------------------------
function GetInterface_IReliefEngineX(var Obj): HResult;
// ---------------------------------------------------------------
begin
  Result:=S_FALSE; Integer(Obj):=0;

  with TRelEngine.Create do
    if GetInterface(IReliefEngineX,OBJ) then Result:=S_OK
end;


end.
