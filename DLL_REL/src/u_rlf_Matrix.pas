unit u_rlf_Matrix;

interface
uses Math,SysUtils,Classes, Dialogs,

   u_assert,


   u_VirtFile,

   I_rel_Matrix1,

   u_rel_Matrix_base,

   u_files,
   u_geo_MIF,

   u_Geo_convert_new,

//   u_Geo_convert,

   u_Geo
   ;


type
//  Trlf_Record = Trel_Record;

  //----------------------------------------
  TrlfMatrix = class(TrelMatrixBase)
  //----------------------------------------
  private
//    FFileStream: TFileStream;

//    FXYBounds: TXYRect; //���������� ������ : ����� ������� ����, ������ ������ ����

//    procedure WriteMemStream (aRow,aCol: integer; aMemStream: TMemoryStream);
//    procedure WriteItem (aRow,aCol: integer; aRec: Trel_Record);
    
  protected
// TODO: SaveHeaderToXYFile
//  procedure SaveHeaderToXYFile(aFileName: string); override;

  public

    //Info: TrelMatrixInfoRec; //inherited

  //  ZoneNum: integer;
//    function ZoneNum(): integer; override;

    constructor Create;

    function OpenFile (aFileName: string): boolean;  override;

    function OpenHeader(aFileName: string): Boolean; override;

    function  LoadHeaderFromFile (aFileName: string): boolean;

    function  FindPointBL  (aBLPoint: TBLPoint; var aRec: Trel_Record): boolean; override;
    function  FindPointXY  (aXYPoint: TXYPoint; aZoneNum: integer; var aRec: Trel_Record): boolean; override;

//    function  FindCellXY   (aXYPoint: TXYPoint; var aRow,aCol: integer): boolean; override;
    function  FindCellBL   (aBLPoint: TBLPoint; var aRow,aCol: integer): boolean; override;

//    procedure SaveHeaderToMIF(aFileName,aBitmapFileName: string); override;
//                               aImgRowCount,aImgColCount: integer); override;

    procedure UpdateMinMaxH(aFileName: string; aGroundMinH, aGroundMaxH: integer);

    class function CreateFileHeader (aFileName: string; aRec: TrelMatrixInfoRec): boolean;

  //  class function CreateFileHeader (aFileName: string; aRec: TrlfHeaderAddRec): boolean;

    class function GetInfo (aFileName: string; var aRec: TrelMatrixInfoRec): boolean;
    class function GetInfoStr (aFileName: string): string;


    class procedure SaveFileHeaderToFileStream(aFileStream: TFileStream; aRec:
        TrelMatrixInfoRec);
    procedure Update_Set_zone_0(aFileName: string);

  end;



//===============================================================
implementation


type

  //---- ��������� ������� ������� ----------------
  Trlf_HeaderRec = packed record
  //-----------------------------------------------
     Mark: array [0..5] of Char;  // 'RLF0  ' - ������������� �����

     ColCount,RowCount: integer;  // ������ ������� (���-�� �������� �� ��������� � ����������� )

     GroundMinH,GroundMaxH : double;          // ���-���� ������

     TopLeft,NotUsed1,
     BottomRight: packed record X,Y: double; end;

     //,NotUsed2
     ZoneNum : Byte;

     Projection: array [0..2] of Char;  // 'GK','UTM' - ������������� �����

     NotUsed2: array[0..8+8-1-1-3] of Byte; //7-3 bytes
  end;




//===============================================================
const
  CRLF = #13+#10;

{

const
  EMPTY_HEIGHT: smallint = High(smallint);
}


//---------------------------------------------------------------
constructor TrlfMatrix.Create;
//---------------------------------------------------------------
begin
  inherited;

  HeaderLen:=SizeOf(Trlf_HeaderRec);
  RecordLen:=SizeOf(Trel_Record);

  MatrixType:=mtXY_;

  NullValue:=EMPTY_HEIGHT;

end;




//---------------------------------------------------------------
function TrlfMatrix.OpenFile (aFileName: string): Boolean;
//---------------------------------------------------------------
begin

 // aFileName:=Trim(aFileName);
  {
  if Active then
  begin
    Result:=true;
    Exit;
  end;
  }
//   if not Enabled then begin Result:=false; Exit; end;

  Result:=LoadHeaderFromFile(aFileName);
  if not Result then
    Exit;


  Result := inherited OpenFile(aFileName);

//   FFileMode:=aMode;

//  Result:=FVirtualFile.Open (aFileName);

 // Active:=FVirtualFile.Active1;

  //if not Result then
//    Exit;
end;


// ---------------------------------------------------------------
function TrlfMatrix.OpenHeader(aFileName: string): boolean;
// ---------------------------------------------------------------
begin
  LoadHeaderFromFile(aFileName);
end;


//--------------------------------------------------------------
function TrlfMatrix.LoadHeaderFromFile (aFileName: string): boolean;
//--------------------------------------------------------------
var oFileStream: TFileStream;
    iRead: integer;
    rFileHeader  : Trlf_HeaderRec;
    iDataSize  : int64;
    bl: TBLPoint;
  kkk: Integer;

begin

  Result:=false;

  if not FileExists(aFileName) then
    Exit;

  AssertFileExists(aFileName);


 // Update_Set_zone_0(aFileName);



//  FileSize:=GetFil oFileStream.Size;


  try
    oFileStream:=TFileStream.Create (aFileName, fmShareDenyNone);
//    oFileStream:=TFileStream.Create (aFileName, fmOpenReadWrite);
    iRead:=oFileStream.Read (rFileHeader, SizeOf(rFileHeader));

    Assert(iRead=SizeOf(rFileHeader), '(iRead=SizeOf(rFileHeader))');

    Assert((StrComp(rFileHeader.Mark, 'rlf1') = 0), '(StrComp(rFileHeader.Mark, ''rlf1'') = 0)');

  //  (StrComp(rFileHeader.Mark, 'rlf1') = 0)

(*
    rFileHeader.ZoneNum :=0;

    oFileStream.Position := 0;
    oFileStream.Write (rFileHeader, SizeOf(rFileHeader));


    FreeAndNil(oFileStream);

*)

    FileSize:=oFileStream.Size;

    Assert(FileSize>0);


    if (iRead=SizeOf(rFileHeader)) then
      if (StrComp(rFileHeader.Mark, 'rlf1') = 0) then
    begin
      RowCount  := rFileHeader.RowCount;
      ColCount  := rFileHeader.ColCount;
//      Step      :=    (rFileHeader.TopLeft.x - rFileHeader.BottomRight.x) / rFileHeader.RowCount ;

      StepX     :=Abs(rFileHeader.TopLeft.x - rFileHeader.BottomRight.x) / rFileHeader.RowCount ;
      StepY     :=Abs(rFileHeader.TopLeft.Y - rFileHeader.BottomRight.Y) / rFileHeader.ColCount ;

//      StepY     :=(FFileHeader.BottomRight.y - FFileHeader.TopLeft.y) / FFileHeader.ColCount ;
      XYBounds.TopLeft.X := rFileHeader.TopLeft.x;
      XYBounds.TopLeft.Y := rFileHeader.TopLeft.y;

      XYBounds.BottomRight.x:=XYBounds.TopLeft.X - RowCount*StepX;
      XYBounds.BottomRight.y:=XYBounds.TopLeft.Y + ColCount*StepY;

//      GroundMinH:=Round(rFileHeader.GroundMinH);
//      GroundMaxH:=Round(rFileHeader.GroundMaxH);

      GroundMinH:= Round( rFileHeader.GroundMinH);
      GroundMaxH:= Round( rFileHeader.GroundMaxH);

      ZoneNum:=rFileHeader.ZoneNum;


      if ZoneNum>60 then
        ZoneNum:=0;

      //check filesize
    //  kkk:=SizeOf(Trel_Record);

      iDataSize:=1; // !!!!!! very important
      iDataSize:=iDataSize* ColCount * RowCount * SizeOf(Trel_Record);

//      iDataSize:=ColCount * RowCount * SizeOf(Trel_Record);


//ShowMessage('HeaderLen '+ IntToStr(HeaderLen) );
//ShowMessage('iDataSize '+ IntToStr(iDataSize) );
//ShowMessage('iDataSize + HeaderLen '+ IntToStr(iDataSize + HeaderLen) );
//ShowMessage('FileSize '+ IntToStr(FileSize) );

      Assert((iDataSize + HeaderLen) = FileSize, 'function TrlfMatrix.LoadHeaderFromFile (aFileName: string): boolean; - (iDataSize + HeaderLen) = FileSize');



      Assert((iDataSize + HeaderLen) = FileSize, 'function TrlfMatrix.LoadHeaderFromFile (aFileName: string): boolean; - (iDataSize + HeaderLen) = FileSize');

      Result:=((iDataSize + HeaderLen) = FileSize);
    end;

  finally
    FreeAndNil(oFileStream);
//    oFileStream.Free;
  end;


  if ZoneNum=0 then
    ZoneNum:=geo_Get6ZoneY (XYBounds.TopLeft.Y);

//  FXYBounds.TopLeft.Zone:=ZoneNum;
 // FXYBounds.BottomRight.Zone:=ZoneNum;

{
  ZoneNum:=ZoneNum-1;

  bl:=geo_XY_to_BL(FXYBounds.TopLeft);
  FXYBounds.TopLeft:=geo_BL_to_XY(bl, 0,  ZoneNum);

  bl:=geo_XY_to_BL(FXYBounds.BottomRight);
  FXYBounds.BottomRight:=geo_BL_to_XY(bl, 0,  ZoneNum);

}


  Info.FileSize:=FileSize;

  Info.RowCount:=RowCount;
  Info.ColCount:=ColCount;

  Info.XYBounds:=XYBounds;
  Info.StepX:=StepX;
  Info.StepY:=StepY;
  Info.ZoneNum:=ZoneNum;

  Info.GroundMinH:=GroundMinH;
  Info.GroundMaxH:=GroundMaxH;

  Info.MatrixType:=mtXY;

end;


//--------------------------------------------------------------
// STUFF
//--------------------------------------------------------------

//------------------------------------------------------
procedure TrlfMatrix.UpdateMinMaxH(aFileName: string; aGroundMinH, aGroundMaxH:
    integer);
//------------------------------------------------------
var header_rec: Trlf_HeaderRec;
    header_rec1: Trlf_HeaderRec;
    oFileStream: TFileStream;
begin
  try
  //  oFileStream:=TFileStream.Create (aFileName, fmShareDenyNone);
    oFileStream:=TFileStream.Create (aFileName, fmOpenReadWrite);
    oFileStream.ReadBuffer (header_rec, SizeOf(header_rec));

    header_rec.GroundMinH:=aGroundMinH;
    header_rec.GroundMaxH:=aGroundMaxH;

    oFileStream.Seek (0, soFromBeginning);
    oFileStream.WriteBuffer (header_rec, SizeOf(header_rec));

    oFileStream.Seek (0, soFromBeginning);
    oFileStream.ReadBuffer (header_rec1, SizeOf(header_rec1));



 //   oFileStream.Seek (0, soFromBeginning);
//    oFileStream.Read (header_rec, SizeOf(header_rec));

  finally
    oFileStream.Free;
  end;
end;


//------------------------------------------------------
procedure TrlfMatrix.Update_Set_zone_0(aFileName: string);
//------------------------------------------------------
var header_rec: Trlf_HeaderRec;
    oFileStream: TFileStream;
begin
  try
    oFileStream:=TFileStream.Create (aFileName, fmOpenReadWrite);
    oFileStream.ReadBuffer (header_rec, SizeOf(header_rec));

    header_rec.ZoneNum:=0;
   // header_rec.GroundMinH:=99;

    oFileStream.Seek (0, soFromBeginning);
    oFileStream.WriteBuffer (header_rec, SizeOf(header_rec));


    //--------------
    oFileStream.Seek (0, soFromBeginning);
    oFileStream.ReadBuffer (header_rec, SizeOf(header_rec));

    Assert(header_rec.ZoneNum=0);
    //--------------


  finally
    oFileStream.Free;
  end;
end;


  {
//------------------------------------------------------
procedure TrlfMatrix.WriteMemStream (aRow,aCol: integer; aMemStream: TMemoryStream);
//------------------------------------------------------
var iPos: integer;
begin
  iPos:=SizeOf(Trlf_HeaderRec) + SizeOf(Trel_Record)*(aRow*ColCount + aCol);
  FFileStream.Seek (iPos, soFromBeginning);
  aMemStream.SaveToStream (FFileStream);
end;

//------------------------------------------------------
procedure TrlfMatrix.WriteItem (aRow,aCol: integer; aRec: Trel_Record);
//------------------------------------------------------
var iPos: integer;
begin
  iPos:=SizeOf(Trlf_HeaderRec) + SizeOf(Trel_Record)*(aRow*ColCount + aCol);
  FFileStream.Seek (iPos, soFromBeginning);
  FFileStream.Write (aRec, SizeOf(aRec));
end;
}

//------------------------------------------------------
class function TrlfMatrix.CreateFileHeader(aFileName: string; aRec: TrelMatrixInfoRec): boolean;
//------------------------------------------------------
var
  header_rec: Trlf_HeaderRec;
//  r,j: integer;
// // cell_rec: Trel_Record;
  oFileStream: TFileStream;
begin

  assert (aRec.XYBounds.TopLeft.X > 0);
  assert (aRec.XYBounds.TopLeft.Y > 0);
  assert (aRec.XYBounds.BottomRight.X > 0);
  assert (aRec.XYBounds.BottomRight.Y > 0);

  assert (aRec.ColCount > 0);
  assert (aRec.RowCount > 0);


  Result:=False;

  FillChar (header_rec, SizeOf(header_rec), 0);
//  FillChar (cell_rec, SizeOf(cell_rec), 0);

  header_rec.Mark:='rlf1'; // 'RLF1' - ������������� �����
  header_rec.ColCount:=aRec.ColCount;
  header_rec.RowCount:=aRec.RowCount;


  header_rec.TopLeft.X    :=aRec.XYBounds.TopLeft.X;
  header_rec.TopLeft.Y    :=aRec.XYBounds.TopLeft.Y;
  header_rec.BottomRight.X:=aRec.XYBounds.BottomRight.X;
  header_rec.BottomRight.Y:=aRec.XYBounds.BottomRight.Y;

{  header_rec.TopLeft.X    :=aRec.XYFrame.TopLeft.X;
  header_rec.TopLeft.Y    :=aRec.XYFrame.TopLeft.Y;
  header_rec.BottomRight.X:=aRec.XYFrame.BottomRight.X;
  header_rec.BottomRight.Y:=aRec.XYFrame.BottomRight.Y;
}

  header_rec.GroundMinH:=aRec.GroundMinH;
  header_rec.GroundMaxH:=aRec.GroundMaxH;

  ForceDirByFileName (aFileName);


  try
    oFileStream:=TFileStream.Create (aFileName, fmCreate);
    oFileStream.Write (header_rec, SizeOf(header_rec));
    Result:=True;
  finally
    oFileStream.Free;
  end;

end;


//------------------------------------------------------
class procedure TrlfMatrix.SaveFileHeaderToFileStream(aFileStream: TFileStream; aRec:
    TrelMatrixInfoRec);
//------------------------------------------------------
var
  header_rec: Trlf_HeaderRec;

  xyBottomRight: TXYPoint;
begin
  Assert(Assigned(aFileStream), 'Value not assigned');

  assert (aRec.XYBounds.TopLeft.X <> 0);
  assert (aRec.XYBounds.TopLeft.Y <> 0);
  assert (aRec.XYBounds.BottomRight.X <> 0);
  assert (aRec.XYBounds.BottomRight.Y <> 0);

  assert (aRec.ColCount > 0);
  assert (aRec.RowCount > 0);


  assert (aRec.StepX> 0);
  assert (aRec.StepY> 0);


  xyBottomRight.X:= aRec.XYBounds.TopLeft.X - aRec.StepX * aRec.RowCount;
  xyBottomRight.Y:= aRec.XYBounds.TopLeft.Y + aRec.StepY * aRec.ColCount;


 // Result:=False;

  FillChar (header_rec, SizeOf(header_rec), 0);
//  FillChar (cell_rec, SizeOf(cell_rec), 0);

  header_rec.Mark:='rlf1'; // 'RLF1' - ������������� �����
  header_rec.ColCount:=aRec.ColCount;
  header_rec.RowCount:=aRec.RowCount;


  header_rec.TopLeft.X    :=aRec.XYBounds.TopLeft.X;
  header_rec.TopLeft.Y    :=aRec.XYBounds.TopLeft.Y;
  header_rec.BottomRight.X:=aRec.XYBounds.BottomRight.X;
  header_rec.BottomRight.Y:=aRec.XYBounds.BottomRight.Y;

{  header_rec.TopLeft.X    :=aRec.XYFrame.TopLeft.X;
  header_rec.TopLeft.Y    :=aRec.XYFrame.TopLeft.Y;
  header_rec.BottomRight.X:=aRec.XYFrame.BottomRight.X;
  header_rec.BottomRight.Y:=aRec.XYFrame.BottomRight.Y;
}

  header_rec.GroundMinH:=aRec.GroundMinH;
  header_rec.GroundMaxH:=aRec.GroundMaxH;

  header_rec.ZoneNum   := aRec.ZoneNum;

  if aRec.Projection='UTM' then
    header_rec.Projection:='UTM'
  else
    header_rec.Projection:='GK';

//    header_Rec.ZoneNum :=iGK_ZoneNum + 30;
  //header_Rec.Projection :='UTM';
                  
  try
    aFileStream.Write (header_rec, SizeOf(header_rec));
  finally
  end;

end;


//--------------------------------------------------------------
class function TrlfMatrix.GetInfo (aFileName: string; var aRec: TrelMatrixInfoRec): boolean;
//--------------------------------------------------------------
var obj: TrlfMatrix;
begin
  obj:=TrlfMatrix.Create;
  if obj.LoadHeaderFromFile(aFileName) then
  begin
    aRec:=obj.Info;

    Result:=True;
  end else
    Result:=False;

  FreeAndNil(obj);

end;


//-------------------------------------------------
class function TrlfMatrix.GetInfoStr (aFileName: string): string;
//-------------------------------------------------
const
  MSG_INFO = '��� [m]: %d'     + CRLF +
             '6�� ����: %d'    + CRLF +
             '������: %d x %d' + CRLF +
             '������ min,max [m]: %d, %d' + CRLF +
             '������� (xy): %0.0f; %0.0f  --  %0.0f; %0.0f' ;


var obj: TrlfMatrix;
begin
  obj:=TrlfMatrix.Create;
  obj.LoadHeaderFromFile (aFilenAME);

  with obj do
    Result:=Format(MSG_INFO,
       [Round(StepX), ZoneNum,
        RowCount, ColCount,
        Round(GroundMinH), Round(GroundMaxH),
        Info.XYBounds.TopLeft.x,     Info.XYBounds.TopLeft.y,
        Info.XYBounds.BottomRight.x, Info.XYBounds.BottomRight.y
       ]);

  FreeAndNil(obj);

end;



function TrlfMatrix.FindCellBL(aBLPoint: TBLPoint; var aRow,aCol: integer):
    boolean;
var xyPoint: TXYPoint;
begin
  xyPoint:=geo_Pulkovo42_BL_to_XY (aBLPoint, ZoneNum);
//  xyPoint:=geo_BL_to_XY (aBLPoint);
  Result:=FindCellXY (xyPoint, aRow,aCol);

//  function geo_Pulkovo42_to_WGS84(aBLPoint: TBLPoint): TBLPoint;
//  function geo_Pulkovo42_to_XY(aBLPoint: TBLPoint; aGK_Zone: word = 0):    TXYPoint;

end;


function TrlfMatrix.FindPointBL (aBLPoint: TBLPoint; var aRec: Trel_Record): boolean;
var
  iNewZone: Integer;
  xy: TXYPoint;
begin
//  iNewZone := geo_Get6ZoneBL(aBLPoint);
//  iNewZone := -1;

//  xy:=geo_BL_to_XY (aBLPoint, ZoneNum);
  xy:=geo_Pulkovo42_BL_to_XY (aBLPoint, ZoneNum);


//  function geo_Pulkovo42_to_WGS84(aBLPoint: TBLPoint): TBLPoint;
//  function geo_Pulkovo42_to_XY(aBLPoint: TBLPoint; aGK_Zone: word = 0):    TXYPoint;

(*
  //�������� � ����� ����
//  if aXYPoint.Zone<>ZoneNum then
  if aZoneNum <> ZoneNum then
  begin
    bl:=geo_XY_to_BL (aXYPoint, aZoneNum);
    aXYPoint:=geo_BL_to_XY (bl,  ZoneNum);
  end;

*)


//  zon

//  Result:=FindPointXY (xy, geo_Get6ZoneBL(aBLPoint), aRec);

 // Result:=FindPointXY (xy, iNewZone, aRec);

  Result:=FindPointXY (xy, ZoneNum, aRec);
end;


//---------------------------------------------------------------
function TrlfMatrix.FindPointXY (aXYPoint: TXYPoint; aZoneNum: integer; var aRec: Trel_Record): boolean;
//--------------------------------------------------------------
var
  iXofs,iYofs : integer;
  //S: string;
  iZone : integer;
  bl: TBLPoint;
begin
  Assert(ZoneNum>0, 'Value <=0');
  Assert(ZoneNum<60, 'Value > 60');

//  if ZoneNum>60 then


//elp: byte=EK_KRASOVSKY42; aZone: integer=0


{  if geo_Get6ZoneXY( aXYPoint) <> ZoneNum then
  begin
    bl:=geo_XY_to_BL (aXYPoint);
    aXYPoint:=geo_BL_to_XY (bl, ZoneNum);
  end;


}
  //�������� � ����� ����
//  if aXYPoint.Zone<>ZoneNum then
  if aZoneNum>0 then
    if aZoneNum <> ZoneNum then
    begin
//      bl:=geo_XY_to_BL (aXYPoint, aZoneNum);
//      aXYPoint:=geo_BL_to_XY (bl,  ZoneNum);

      bl:=geo_Pulkovo42_XY_to_BL (aXYPoint, aZoneNum);
     //   function geo_GK_XY_to_Pulkovo42_BL(aXYPoint: TXYPoint; aGK_Zone: Integer): TBLPoint;

      aXYPoint:=geo_Pulkovo42_BL_to_XY (bl,  ZoneNum);


//  function geo_Pulkovo42_to_WGS84(aBLPoint: TBLPoint): TBLPoint;
//  function geo_Pulkovo42_to_XY(aBLPoint: TBLPoint; aGK_Zone: word = 0):    TXYPoint;

    end;

{

  iZone:=ZoneNum;

  s:= FileName;}

 {  bl: TBLPoint;
   S: string;
}
  if  (Active) and
      (aXYPoint.X < XYBounds.TopLeft.x)     and
      (aXYPoint.X > XYBounds.BottomRight.x) and

      (aXYPoint.Y > XYBounds.TopLeft.y)     and
      (aXYPoint.Y < XYBounds.BottomRight.y)

  then begin
//     iXofs  := Integer(Round((XYBounds.TopLeft.x - aXYPoint.X) / Step));
//     iYofs  := Integer(Round((aXYPoint.Y - XYBounds.TopLeft.y) / Step));
     iXofs  := Trunc((XYBounds.TopLeft.x - aXYPoint.X) / StepX);
     iYofs  := Trunc((aXYPoint.Y - XYBounds.TopLeft.y) / StepY);

     aRec:=GetItem (iXofs, iYofs);

     Result:=(aRec.Rel_H <> EMPTY_HEIGHT);
  end else
    Result:=false;

end;

//
//
//var
//  obj: TrlfMatrix;
//
//begin
//
//  obj:=TrlfMatrix.Create;
//  obj.LoadHeaderFromFile ('d:\33\lenobl.rlf');
//
//  obj.Free;


end.



