unit u_rel_Profile_export;

interface
uses
  Variants, DB,  
  

  u_files,

  u_db,

  I_rel_Matrix1,
  u_rel_Profile;


type
  TrelProfile_Export = class(TObject)
  public
    class procedure SaveToDataset(aProfile: TrelProfile; aDataset: TDataset);
    class procedure LoadFromDataset(aProfile: TrelProfile; aDataset: TDataset);

// TODO: LoadFromXml
//  class function LoadFromXml(aProfile: TrelProfile; aValue: string): Boolean;

    class procedure LoadFromXmlFile(aProfile: TrelProfile; aFileName: string);
    class procedure SaveToXmlFile(aProfile: TrelProfile; aFileName: string);
  end;

implementation

(*const
  FLD_INDEX ='INDEX';
*)
const
  FLD_INDEX     = 'INDEX';
  FLD_DIST_M    = 'Dist_m';
  FLD_DIST_KM   = 'Dist_KM';
  FLD_REL_H     = 'Rel_H';
  FLD_CLU_H     = 'Clu_H';
  FLD_CLU_code  = 'Clu_code';
  FLD_CLU_color = 'clu_color';
  FLD_earth_h   = 'earth_h';




//---------------------------------------------------
class procedure TrelProfile_Export.LoadFromDataset(aProfile: TrelProfile;
    aDataset: TDataset);
//---------------------------------------------------
var
  i,iCluCode:integer;
  bm: TBookmark;
  rec:  TrelProfilePointRec;
begin
  with aProfile do
  begin


  Assert(Data.Count=aDataset.RecordCount);

  with aDataset do
  begin
    bm:=GetBookmark;
    DisableControls;

    First;


    while not EOF do
    begin
      i:=FieldByName(FLD_INDEX).AsInteger;

      FillChar(rec, SizeOf(rec), 0);

      rec.Clutter_H   := FieldByName(FLD_clu_h).AsInteger;
      rec.rel_h       := FieldByName(FLD_rel_h).AsInteger;
      rec.Clutter_code:= FieldByName(FLD_clu_code).AsInteger;

      Items[i]:=rec;

//      Item

{//      Data.Items[i].Distance_KM :=(FieldByName(FLD_DIST_M).AsFloat / 1000);
      Items[i].Clutter_H   := FieldByName(FLD_clu_h).AsInteger;
      Items[i].rel_h       := FieldByName(FLD_rel_h).AsInteger;
      Items[i].Clutter_code:= FieldByName(FLD_clu_code).AsInteger;
}
      Next;
    end;

    GotoBookmark(bm);
    EnableControls;
  end;
  end;    // with

end;

// TODO: LoadFromXml
////----------------------------------------------------------
//class function TrelProfile_Export.LoadFromXml(aProfile: TrelProfile; aValue:
//  string): Boolean;
////----------------------------------------------------------
//var i: integer;
//vGroup,vNode: IXMLNode;
//eStep,fDistance: double;
//
// oXMLDoc: TXMLDoc;
//begin
//with aProfile do
//begin
//
//Result:=False;
//
//oXMLDoc:=TXMLDoc.Create;
//
//oXMLDoc.LoadFromText (aValue);
////  oXMLDoc.XmlDocument.XML.Text := aValue;
//
////  oXMLDoc.LoadFromText(aValue);
//
//
////  StrToTxtFile(aValue, 'd:\profile.xml');
//
//
//vGroup:=xml_FindNode (oXMLDoc.DocumentElement, 'Items');
//if VarIsNull(vGroup) then
//  Exit;
//
//Refraction:= xml_GetFloatAttr(vGroup, 'Refraction');
//
//
//Data.Step_R_km           :=xml_GetFloatAttr (vGroup, 'step_km');
//
//Data.Distance_km      :=xml_GetFloatAttr (vGroup, 'Distance_km');
//Data.BLVector.Point1.B:=xml_GetFloatAttr (vGroup, 'B1');
//Data.BLVector.Point1.L:=xml_GetFloatAttr (vGroup, 'L1');
//Data.BLVector.Point2.B:=xml_GetFloatAttr (vGroup, 'B2');
//Data.BLVector.Point2.L:=xml_GetFloatAttr (vGroup, 'L2');
//
//
//Property1_id:=xml_GetIntAttr (vGroup, 'Property1_id');
//Property2_id:=xml_GetIntAttr (vGroup, 'Property2_id');
//
//Data.Count:=0;
//
////  for i:=0 to vGroup.ChildNodes.Count-1 do
//for i:=0 to vGroup.ChildNodes.Count-1 do
//begin
////    vNode:=vGroup.ChildNodes[i];
//  vNode:=vGroup.ChildNodes[i];
//
//  Data.Items[i].Distance_km:=xml_GetFloatAttr (vNode, 'dist');
//
//  if (Data.Items[i].Distance_km<0) or
//     (Data.Items[i].Distance_km>Data.Distance_km) then
//  begin
//  //  g_Log.AddError('TrelProfile_Export',
//   //                Format('(Data.Items[i].Distance_km>Data.Distance_km): %1.4f-%1.4f', [Data.Items[i].Distance_km, Data.Distance_km] ));
//
//    Data.Count := 0;
//    Break;
//  end;
//
//  Data.Items[i].Rel_H        :=xml_GetIntAttr   (vNode, 'Rel_H');
//  Data.Items[i].Clutter_H    :=xml_GetFloatAttr (vNode, 'Local_H');
//  Data.Items[i].Clutter_Code :=xml_GetIntAttr   (vNode, 'Local_Code');
//
//  Data.Items[i].Earth_H := GetItemEarthHeight(i);
//
//  UpdateItemFullHeight(i);
//
//  Inc(Data.Count);
//end;
//
//FreeAndNil(oXMLDoc);
//
////  oXMLDoc.Free;
//
//
//Result:=(Data.Count>0) and
//        (Data.BLVector.Point1.B<>0) and
//        (Data.BLVector.Point1.L<>0) and
//        (Data.BLVector.Point2.B<>0) and
//        (Data.BLVector.Point2.L<>0);
//
//end;
//end;

//----------------------------------------------------------
class procedure TrelProfile_Export.LoadFromXmlFile(aProfile: TrelProfile;
    aFileName: string);
//----------------------------------------------------------
var s: string;
begin
  s:=TxtFileToStr(aFileName);
//////////////  LoadFromXml(s);
end;

//--------------------------------------------------------------
class procedure TrelProfile_Export.SaveToDataset(aProfile: TrelProfile;
    aDataset: TDataset);
//--------------------------------------------------------------
var
  i: Integer;
begin
  with aProfile do
  begin


 with aDataset do
  begin
    DisableControls;

    Close;
    Open;

    for i:=0 to Count-1 do
    begin
      db_AddRecord (aDataset,
        [
          db_Par(FLD_INDEX,    i ),
          db_Par(FLD_RECNO,    i ), //+1
          db_Par(FLD_dist_M,   Items[i].Distance_KM * 1000  ),
          db_Par(FLD_earth_h,  Items[i].Earth_H),

          db_Par(FLD_rel_h,    Items[i].Rel_H),
          db_Par(FLD_clu_h,    Items[i].Clutter_H),
          db_Par(FLD_clu_code, Items[i].Clutter_Code)
        ]);
    end;

    First;
    EnableControls;

  //  OnCalcFields:=DoOnCalcFields ;
  end;

  end;
end;

//--------------------------------------------------------------------
class procedure TrelProfile_Export.SaveToXmlFile(aProfile: TrelProfile;
    aFileName: string);
//--------------------------------------------------------------------
var
  s: string;
begin

 // s:=GetXML();
 // StrToTxtFile(s, aFileName);

end;

end.
