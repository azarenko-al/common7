unit u_Clutters;

interface
uses
  Graphics,

  I_rel_Matrix1;



type
  //-------------------------------------------------
  TrelClutterModel = class
  //-------------------------------------------------
  private
// TODO: GetColorByCode_old
//  function GetColorByCode_old(aCode: integer): integer;
  public

    Items_new: array of record
                Code: Integer;
                Height: integer;
                Name: string;
                Color : integer;
              end;


    Items: array[Byte] of record
                Code: Integer;
                Height: integer;
                Name: string;
                Color : integer;

                IsUsed1: boolean;
                Index : integer;
              end;
              
    Count: integer;

    constructor Create;

//    function GetColorByCode(aCode: integer): integer;
    function GetNameByCode1(aCode: integer): string;
    function GetHeightByCode(aCode: integer): integer;

    procedure Assign (aSource: TrelClutterModel);
    procedure Prepare;

    function GetColorByCode(aCode: Integer; var aColor: Integer): Boolean; //overload;

  end;

implementation

constructor TrelClutterModel.Create;
var
  I: Integer;
begin
  inherited;

  Count:=Length(DEF_CLUTTER_ARR);

  SetLength(Items_new, Count);

  for I := 0 to Count-1 do
  begin
    Items_new[i].Code   := DEF_CLUTTER_ARR[i].Code;
    Items_new[i].Height := DEF_CLUTTER_ARR[i].Height;
    Items_new[i].Color  := DEF_CLUTTER_ARR[i].Color;
    Items_new[i].Name   := DEF_CLUTTER_ARR[i].name;
  end;

  Prepare;

end;

//--------------------------------------------------------
procedure TrelClutterModel.Assign (aSource: TrelClutterModel);
//--------------------------------------------------------
var i: integer;
begin
  Count:=aSource.Count;
  Items:=aSource.Items;
end;

// ---------------------------------------------------------------
function TrelClutterModel.GetColorByCode(aCode: Integer; var aColor: Integer):
    Boolean;
begin
  Assert(Count>0, 'function TrelClutterModel.GetColorByCode(aCode: Integer; var aColor: Integer):   Count <=0');

  Result := (aCode<High(Byte)) and (Items[aCode].IsUsed1);

  if Result then
    aColor := Items[aCode].Color
  else
    aColor := clNavy;
end;



function TrelClutterModel.GetNameByCode1(aCode: integer): string;
begin
  if (aCode<High(Byte)) and (Items[aCode].IsUsed1) then
    Result := Items[aCode].Name
  else
    Result := '';
end;


function TrelClutterModel.GetHeightByCode(aCode: integer): integer;
begin
  Assert(Count>0, 'TrelClutterModel.GetHeightByCode  -  Count <=0');


  if (aCode<High(Byte)) and (Items[aCode].IsUsed1) then
    Result := Items[aCode].Height
  else
    Result := 0;
end;


procedure TrelClutterModel.Prepare;
var
  I: Integer;
  iCode: Integer;
  iColor: Integer;
  iHeight: Integer;
  sName: string;
begin
  FillChar(Items, SizeOf(Items), 0);


  for I := 0 to High(Items_new) do
  begin
    iCode  :=Items_new[i].Code;
    iColor :=Items_new[i].Color;
    iHeight:=Items_new[i].Height;
    sName  :=Items_new[i].Name;

    if iCode<255 then
    begin
      Items[iCode].Code  :=iCode;
      Items[iCode].Color :=iColor;
      Items[iCode].Height:=iHeight;
      Items[iCode].Name  :=sName;

 //     Items[iCode].IsUsed:=True;

      Items[iCode].IsUsed1 := True;
    end;

  end;
(*

       aClutterModel.Items[iCode].Code  :=iCode;
       aClutterModel.Items[iCode].Color :=iColor;
       aClutterModel.Items[iCode].Height:=iHeight;
       aClutterModel.Items[iCode].IsUsed:=True;
       aClutterModel.Items[iCode].Name  :=sName;



     if iCode<255 then
     begin
       aClutterModel.Items[iCode].Code  :=iCode;
       aClutterModel.Items[iCode].Color :=iColor;
       aClutterModel.Items[iCode].Height:=iHeight;
       aClutterModel.Items[iCode].IsUsed:=True;
       aClutterModel.Items[iCode].Name  :=sName;
     end;
*)
end;

end.



// TODO: GetColorByCode_old
//function TrelClutterModel.GetColorByCode_old(aCode: integer): integer;
//begin
//if (aCode<High(Byte)) and (Items[aCode].IsUsed) then
//  Result := Items[aCode].Color
//else
//  Result := 0;
//end;

