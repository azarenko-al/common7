unit u_rel_Matrix_base;

interface
uses Classes, Math, Sysutils,

  u_func,

  I_rel_Matrix1,
  u_geo_MIF,

  u_geo_convert_new,
  // u_geo_convert,

   u_Geo,
   u_files,
   u_VirtFile;


type

  //---------------------------------------------------------------
  TrelMatrixBase = class //(TVirtualFile)
  //---------------------------------------------------------------
  private
    procedure SaveToDebugFiles(aDir: string);
  protected
    //-----------------------------
    // BL matrix
    //-----------------------------
    //FBLBounds : TBLRect; //���������� ������ ������� ����
    FVirtualFile: TVirtualFile;

    HeaderLen: integer; //����� ���������
    RecordLen: integer; //����� ������ ��� ����� �� �������


    //-----------------------------
    // XY matrix
    //-----------------------------
//    FXYBounds: TXYRect; //���������� ������ : ����� ������� ����, ������ ������ ����
//    FZoneNum : Byte;

    function GetItem (aRow,aCol: integer): Trel_Record; virtual;
  public
    function GetItem1(aRow,aCol: Integer; var aRec: Trel_Record): Boolean; virtual;

  public
    Active : Boolean; //opened or not

    FileSize: Int64;

    FileName : string;


//    XYBounds: TXYRect; //���������� ������ : ����� ������� ����, ������ ������ ����
    RowCount,ColCount : integer;

    MatrixType: (mtNone_,mtLonLat_,mtXY_);

    NullValue : Integer;

    //-----------------------------
    // BL matrix
    //-----------------------------
    BLBounds: TBLRect;
    StepB,StepL : double;
    Width,Height  : double;

    //-----------------------------
    // XY matrix
    //-----------------------------
    XYBounds: TXYRect;
    ZoneNum: integer;
    StepX,StepY: Double;
    StartX,StartY : double; //����� ������� ����

    // common ----------
//    FileSize: integer;

    Projection: (ptGK,ptUTM);

{
    RowCount,ColCount : integer;
    StepB,StepL   : double;
  //  Step          : double;
    StepX,StepY   : double;
}


    GroundMinH,GroundMaxH : integer;

//    ZoneNum: integer; //virtual; abstract;


    Info: TrelMatrixInfoRec;

    constructor Create;
    destructor Destroy; override;


//    function GetFileSize: Int64;

    function GetPoints_WGS: TBLPointArray;


    function OpenFile(aFileName: string): boolean; virtual;
    function OpenHeader(aFileName: string): boolean; virtual;abstract;

//    function ZoneNum(): integer; virtual; abstract;

//    procedure SaveHeaderToMIF (aFileName,aBitmapFileName: string); virtual; abstract;

//  aImgRowCount,aImgColCount: integer); virtual; abstract;

    procedure SaveHeaderToXYFile (aFileName: string); virtual; abstract;

    function  FindPointBL  (aBLPoint: TBLPoint; var aRec: Trel_Record): boolean; virtual; //abstract;
    function  FindPointXY  (aXYPoint: TXYPoint; aZoneNum: integer; var aRec: Trel_Record): boolean; virtual; abstract;

    function FindCellXY(aX,aY: double; var aRow,aCol: integer): boolean; overload;
    function FindCellXY (aXYPoint: TXYPoint; var aRow,aCol: integer): boolean; overload; virtual; //abstract;

    function FindCellBL(aBLPoint: TBLPoint; var aRow,aCol: integer): boolean; virtual;

    procedure CalcMinMaxHeights;

    property Items [Row,Col: integer]: Trel_Record read GetItem; default;

    class function GetInfo (aFileName: string; var aRec: TrelMatrixInfoRec): boolean; virtual; abstract;
    function GetInfoStr: string;

    function GetItemByRowCol(aRow,aCol: Integer; var aRec: Trel_Record): Boolean;
    function GetPoints_Pulkovo42: TBLPointArray;

    procedure SaveHeaderToMIF(aFileName,aBitmapFileName: string);
    function ToGeometry: string;
  end;


//===============================================================
implementation


constructor TrelMatrixBase.Create;
begin
  inherited;// Create;


  Active :=True;

//  FileName :=aFileName;

  FVirtualFile:=TVirtualFile.Create;
end;


destructor TrelMatrixBase.Destroy;
begin
  FreeAndNil(FVirtualFile);  
  inherited;
end;



//---------------------------------------------------------------
procedure TrelMatrixBase.CalcMinMaxHeights;
//---------------------------------------------------------------
var
  r,c: integer;
  rel: Trel_Record;
begin
  // define ground MinH, MaxH
//  aMinH:=0;  aMaxH:=0;


  GroundMinH:=0;
  GroundMaxH:=0;

  for r:=0 to RowCount-1 do
    for c:=0 to ColCount-1 do
    begin
      rel:=Items[r,c];

      if  (rel.Rel_H = EMPTY_HEIGHT) then
        Continue;

      if (rel.Rel_H > GroundMaxH) then
        GroundMaxH:=rel.Rel_H
      else
      
      if (GroundMinH=0) or (rel.Rel_H < GroundMinH) then
        GroundMinH:=rel.Rel_H;
    end;
    
end;

//---------------------------------------------------------------
function TrelMatrixBase.GetItem (aRow,aCol: integer): Trel_Record;
//---------------------------------------------------------------
var
  iOffset: Integer;
begin
  { TODO : check file size }
  FillChar(Result,SizeOf(Result),0);

  iOffset := (aRow * ColCount) + aCol;

//  if ReadRecord (iOffset, @Result) then
  if FVirtualFile.ReadRecord (iOffset, @Result) then
  begin
     {
     if Result.Rel_H<>EMPTY_HEIGHT then
       if Result.Loc_Code=0 then
         Result.Loc_H:=0;
      }
  end else
    Result.Rel_H:=EMPTY_HEIGHT;

end;


//---------------------------------------------------------------
function TrelMatrixBase.GetItemByRowCol(aRow,aCol: Integer; var aRec:
    Trel_Record): Boolean;
//---------------------------------------------------------------
begin
  FillChar(aRec,SizeOf(aRec),0);

  Result := FVirtualFile.ReadRecord ((aRow * ColCount) + aCol, @aRec);

  if not Result then
    aRec.Rel_H:=EMPTY_HEIGHT;

end;



//---------------------------------------------------------------
function TrelMatrixBase.FindCellBL(aBLPoint: TBLPoint; var aRow,aCol: integer): boolean;
//--------------------------------------------------------------
begin
  Result:=False;

  if not (Active) then
    Exit;


  if MatrixType=mtLonLat_ then
  begin
    if  ((BLBounds.TopLeft.B > aBLPoint.B) and (aBLPoint.B > BLBounds.BottomRight.B) and
         (BLBounds.TopLeft.L < aBLPoint.L) and (aBLPoint.L < BLBounds.BottomRight.L)) then
    begin
      aRow := Abs(Trunc((aBLPoint.B - BLBounds.TopLeft.B) / StepB));
      aCol := Abs(Trunc((aBLPoint.L - BLBounds.TopLeft.L) / StepL));

      Result:=(aRow>=0) and (aCol>=0) and (aRow<=RowCount-1) and (aCol<=ColCount-1);
    end;
  end;

end;

//---------------------------------------------------------------
function TrelMatrixBase.FindCellXY(aXYPoint: TXYPoint; var aRow,aCol: integer): boolean;
//--------------------------------------------------------------
begin
  Result:=False;

  aRow:=0;
  aCol:=0;

//  if not (Active) then
 //   Exit;
  assert(XYBounds.BottomRight.x>0);

  if MatrixType=mtXY_ then
  begin
  if
    ((XYBounds.BottomRight.x < aXYPoint.X) and (aXYPoint.X < XYBounds.TopLeft.x) and
     (XYBounds.TopLeft.y     < aXYPoint.Y) and (aXYPoint.Y < XYBounds.BottomRight.y)) then
    begin
      aRow := Integer(Round((XYBounds.TopLeft.x - aXYPoint.X) / StepX));
      aCol := Integer(Round((aXYPoint.Y - XYBounds.TopLeft.y) / StepY));

      Result:=(aRow>=0) and (aCol>=0) and (aRow<=RowCount-1) and (aCol<=ColCount-1);

//      Result:=true;
    end;
   end;

end;


function TrelMatrixBase.FindCellXY(aX,aY: double; var aRow,aCol: integer): boolean;
begin
  Result := FindCellXY( MakeXYPoint(aX,aY), aRow,aCol);
end;


//-------------------------------------------------
function TrelMatrixBase.GetInfoStr: string;
//-------------------------------------------------
const
  MSG_INFO = '��� B [m]: %0.7f'  + CRLF +
             '��� L [m]: %0.7f'  + CRLF +
             '������: %d x %d'   + CRLF +
             '������� (BL): %0.0f; %0.0f  --  %0.0f; %0.0f' ;
begin

 case MatrixType of
    mtXY_:;

    mtLonLat_: Result:=Format(MSG_INFO,
                   [StepB, StepL,
                    RowCount, ColCount,
                    BLBounds.TopLeft.B,     BLBounds.TopLeft.L,
                    BLBounds.BottomRight.B, BLBounds.BottomRight.L
                   ]);
  end;


end;




//---------------------------------------------------------------
function TrelMatrixBase.FindPointBL (aBLPoint: TBLPoint; var aRec: Trel_Record): boolean;
//--------------------------------------------------------------
var iRow,iCol: integer;
begin
  Result:=FindCellBL (aBLPoint, iRow,iCol);
  if Result then
  begin
    aRec:=Items[iRow,iCol];

    if aRec.rel_h = EMPTY_HEIGHT then
      Result:=False;
  end;
end;
 

function TrelMatrixBase.GetItem1(aRow,aCol: Integer; var aRec: Trel_Record):
    Boolean;
var
  iOffset: Integer;
begin
 // FillChar(Result,SizeOf(Result),0);

  iOffset := (aRow * ColCount) + aCol;
  Result := FVirtualFile.ReadRecord (iOffset, @aRec);

end;

// ---------------------------------------------------------------
function TrelMatrixBase.ToGeometry: string;
// ---------------------------------------------------------------
var
  I: Integer;
  xy_arr: TXYPointArray;
  bl_arr: TBLPointArray;

  bl_WGS_arr: TBLPointArray;

  bl: TBLPoint;
begin
  SetLength (bl_WGS_arr, 5);

  case MatrixType of
    mtXY_: begin
              xy_arr := geo_XYRectToXYPoints_(XYBounds);
//              Assert(XYBounds.TopLeft.B<>0);

              for I := 0 to High(bl_WGS_arr)-1 do
//              for I := 0 to High(bl_WGS_arr)-1 do
                bl_WGS_arr[i] := geo_Pulkovo42_XY_to_WGS84_BL(xy_arr[i], ZoneNum);

           end;

    mtLonLat_: begin
              assert(BLBounds.TopLeft.B<>0);

              bl_arr := geo_BLRectToBLPoints_(BLBounds);

             // Result := bl_arr;

              for I := 0 to High(bl_WGS_arr)-1 do
                bl_WGS_arr[i] := geo_Pulkovo42_to_WGS84(bl_arr[i]);

           end;

  end;
  // ---------------------------------------------------------------

  Result:='POLYGON((';

  bl_WGS_arr[4]:=bl_WGS_arr[0];

  for I := 0 to High(bl_WGS_arr) do
    Result:=Result+ ReplaceStr( Format('%1.6f %1.6f',[bl_WGS_arr[i].L, bl_WGS_arr[i].B]), ',','.') +
                    IIF(i<High(bl_WGS_arr), ', ','');

  Result:=Result + '))';


//  Result:=ReplaceStr(Result, ',','.');

end;

{
var
 // arr: TStringDynArray;
  I: Integer;
begin
  Result:='POLYGON((';

  for I := 0 to High(Points_WGS) do
    Result:=Result+ Format('%1.6f %1.6f',[Points_WGS[i].Lon, Points_WGS[i].Lat]) +
                    IfThen(i<High(Points_WGS), ', ','');

  Result:=Result + '))';


//
//DECLARE @g geography;
//SET @g = geography::STPolyFromText('POLYGON((-122.358 47.653, -122.348 47.649, -122.348 47.658, -122.358 47.658, -122.358 47.653))', 4326);
//SELECT @g.ToString();

//  Result := ;
end;
}

// ---------------------------------------------------------------
function TrelMatrixBase.GetPoints_WGS: TBLPointArray;
// ---------------------------------------------------------------
var
  I: Integer;
  xy_arr: TXYPointArray;
  bl_arr: TBLPointArray;
  bl: TBLPoint;
begin
  SetLength (Result, 4);

  case MatrixType of
    mtXY_: begin
              xy_arr := geo_XYRectToXYPoints_(XYBounds);

              for I := 0 to High(Result) do
                Result[i] := geo_Pulkovo42_XY_to_WGS84_BL(xy_arr[i], ZoneNum);

           end;

    mtLonLat_: begin
              assert(BLBounds.TopLeft.B<>0);

              bl_arr := geo_BLRectToBLPoints_(BLBounds);

             // Result := bl_arr;

              for I := 0 to High(Result) do
                Result[i] := geo_Pulkovo42_to_WGS84(bl_arr[i]);

           end;

  end;


end;


// ---------------------------------------------------------------
function TrelMatrixBase.GetPoints_Pulkovo42: TBLPointArray;
// ---------------------------------------------------------------
var
  I: Integer;
  xy_arr: TXYPointArray;
  bl_arr: TBLPointArray;
  bl: TBLPoint;
begin
  SetLength (Result, 4);

  case MatrixType of
    mtXY_: begin
              xy_arr := geo_XYRectToXYPoints_(XYBounds);

              for I := 0 to High(Result) do
                Result[i] := geo_Pulkovo42_XY_to_BL(xy_arr[i], ZoneNum);

           end;

    mtLonLat_: begin
              assert(BLBounds.TopLeft.B<>0);

              bl_arr := geo_BLRectToBLPoints_(BLBounds);

             // Result := bl_arr;

              for I := 0 to High(Result) do
                Result[i] := bl_arr[i];
              //  Result[i] := geo_Pulkovo42_to_84(bl_arr[i]);

           end;

  end;


end;


// ---------------------------------------------------------------
function TrelMatrixBase.OpenFile(aFileName: string): boolean;
// ---------------------------------------------------------------
begin
  FileName:=aFileName;

  Result := FVirtualFile.Open(aFileName);

  FVirtualFile.HeaderLen1:=HeaderLen;
  FVirtualFile.RecordLen1:=RecordLen;

  FileSize := FVirtualFile.FileSize;

 // Active:=True;

end;


//-------------------------------------------------------------------
procedure TrelMatrixBase.SaveToDebugFiles(aDir: string);
//-------------------------------------------------------------------
var
  r,c: integer;
 // list: TStringList;

  F_clu,F_rel,F_clu_h: TextFile;

  str1: string;
  str2: string;
  str3: string;

begin
  aDir:=IncludeTrailingBackslash(aDir);
  ForceDirectories(aDir);


  AssignFile(F_clu, aDir + '_clu.txt');
  ReWrite(F_clu);

  AssignFile(F_rel, aDir + '_rel.txt');
  ReWrite(F_rel);

  AssignFile(F_clu_h, aDir + '_clu_h.txt');
  ReWrite(F_clu_h);

  for r:=0 to RowCount-1 do
  begin
    str1:='';
    str2:='';
    str3:='';

    for c:=0 to ColCount-1 do
    begin
      str1:=str1+ Format('%3d', [Items[r,c].Clutter_Code]);
      str2:=str2+ Format('%6d', [Items[r,c].Clutter_H]);
      str3:=str3+ Format('%6d', [Items[r,c].Rel_H]);
    end;

//     str1:=Str1 + CellToStr (r,c);

    Writeln(F_clu,str1);
    Writeln(F_rel,str2);
    Writeln(F_clu_h,str3); 
  // list.Add(str);
  end;

  CloseFile(F_clu);
  CloseFile(F_rel);
  CloseFile(F_clu_h);
end;
         

//------------------------------------------------------
procedure TrelMatrixBase.SaveHeaderToMIF(aFileName,aBitmapFileName: string);
//------------------------------------------------------
var
  rec: TMifHeaderInfoRec;
begin
  FillChar(rec, SizeOf(rec), 0);

  rec.FileName      :=ChangeFileExt (aFileName, '.tab');
  rec.BitmapFileName:=aBitmapFileName;


  Assert(RowCount>0, 'Value <=0');
  Assert(ColCount>0, 'Value <=0');

  rec.RowCount   :=RowCount;
  rec.ColCount   :=ColCount;


  case MatrixType of
    mtLonLat_:begin
                rec.BL.BLBounds   :=BLBounds;
              //  rec.ImgRowCount   :=aImgRowCount;
              //  rec.ImgColCount   :=aImgColCount;
                rec.BL.StepB      :=StepB;
                rec.BL.StepL      :=StepL;

                geo_SaveBLHeaderForMIF (rec);
              end;

    mtXY_:    begin
                rec.XY.TopLeft:=XYBounds.TopLeft;
                rec.XY.StepX:=StepX;
                rec.XY.StepY:=StepY;

                geo_SaveXYHeaderForMIF (rec);

//                geo_GK_to_UTM_XYPointArray1 (XYBounds, ZoneNum, '');

                //function geo_GK_to_UTM_XYPointArray1(aXYBounds: TXYRect; aGK_zone: Integer):
//    TXYPointArray;

              end;



 // u_geo_MIF_lib.

  end;

end;



end.



