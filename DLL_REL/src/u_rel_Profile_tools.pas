unit u_rel_Profile_tools;

interface

uses
   u_profile_types,

  u_rel_Profile;

type
  TRelProfile_tools = class(TObject)
  public
    class procedure CalcSummary1(aRelProfile: TrelProfile);
    class function GetType1(aRelProfile: TrelProfile; aAntennaH1, aAntennaH2,
        aFreq_GHz: double): TrelProfileType;

    class function GetType_with_Los(aRelProfile: TrelProfile; aAntennaH1,
        aAntennaH2, aFreq_GHz: double; var aLosValue: double): TrelProfileType;
  end;

implementation

//--------------------------------------------------------------
class procedure TRelProfile_tools.CalcSummary1(aRelProfile: TrelProfile);
//--------------------------------------------------------------
var i: integer;
begin
  with aRelProfile do
  begin


  FillChar (Summary, SizeOf(Summary), 0);

//  with Summary do
  for i:=0 to Count-1 do
  begin
    if Data.Items[i].Clutter_H > Summary.MaxClutterH then
       Summary.MaxClutterH:=Round(Data.Items[i].Clutter_H);

    if Data.Items[i].Rel_H > Summary.MaxRelH then
      Summary.MaxRelH:=Data.Items[i].Rel_H;

    if (Summary.MinRelH=0) or (Data.Items[i].Rel_H < Summary.MinRelH) then
      Summary.MinRelH:=Data.Items[i].Rel_H;// + Round(GetItemEarthHeight(i));

    if Data.Items[i].Earth_H > Summary.MaxEarthH then
      Summary.MaxEarthH:=Round(Data.Items[i].Earth_H);
  end;

  end;    // with

end;

//-------------------------------------------------------------------
class function TRelProfile_tools.GetType1(aRelProfile: TrelProfile; aAntennaH1,
    aAntennaH2, aFreq_GHz: double): TrelProfileType;
//-------------------------------------------------------------------
const
  MIN_VALUE = -1000;

var
  i: integer;
   R,H1,H2: double;

  eFullDistanceKM: Double;

  dX,dY,dH,h: double;

  max_clu_code: integer;
  tang,min_dH,max_rel_h,max_Clutter_H,
  max_rel_dH,max_clu_dH: double;

begin
  Result:=prtUnknown;

  with aRelProfile do
  begin



  R:=GeoDistance_KM() ;
//  R:=Data.Distance ;
  if R=0 then
    Exit;

  eFullDistanceKM:= GeoDistance_KM();


  H1 := FirstItem().Rel_H + aAntennaH1; {������ ��������� � ����� ��+������ ������� ��}
  H2 := LastItem().Rel_H  + aAntennaH2; {������ ��������� � ����� ��+������ ������� ��}

  // ���������� �������� ���� ������� ���� ������
  tang := (H2 - H1) / R;

  max_clu_code:=MIN_VALUE;
  max_rel_dH:=MIN_VALUE;
  max_clu_dH:=MIN_VALUE;

 // aProfile.GetXML ('u:\aaa.xml');

//  Result:=ptNone;

//   for i:=aProfile.Count-1 to aProfile.Count-1 do
   for i:=1 to Count-1 do
   begin
     dX:=i * Data.Items[i].Distance_km;  // delta;
//     dX:=i * Data.Step_R1; // delta;

     h:= H1 + tang*dX; //������ ����� ��.�����.

     // ������ ���� �������
     dY:=Sqrt((300 / (aFreq_GHz*1000))* 0.3 * //Params.NFrenel
             (1000*dx)* (1 - dx / eFullDistanceKM));
 //            (1000*dx)* (1 - dx/Data.Distance));

     // ��������� ����������� �������� ���� ������ ���������
     if (Data.Items[i].Rel_H > h - dY) then
     begin
       dH:=Data.Items[i].Rel_H - h;
       if dH > max_rel_dH then  max_rel_dH:=dH;
     end else

     if (Data.Items[i].Full_H > h - dY) then
     begin
       // ������� ����� ������ ������ ��������� � ������������
       dH:=Data.Items[i].Full_H - h;

       // ���� ��� ����� ��������� �������
       if dH > max_clu_dH then
       begin
         max_clu_dH:=dH;
         max_clu_code:=Data.Items[i].Clutter_Code;
       end;
     end;
   end;


   // 1. ������ ���� ���� ������ ���������
   if (max_rel_dH > 0) then
     Result:=prtClosed_relief
   else

   // 2. �� ���� ���� ������ ���������
   if (max_clu_dH > 0) then
     case max_clu_code of
       DEF_FOREST   : Result:=prtClosed_forest;
       DEF_COUNTRY  : Result:=prtClosed_country;
       DEF_CITY     : Result:=prtClosed_city;
       else       Result:=prtClosed;
     end else

   // 3. ������ ���� ���� �������
   if (max_rel_dH <> MIN_VALUE) and (max_rel_dH < 0) then
     Result:=prtHalfOpened_relief
   else

   // 4. �� ���� ���� �������
   if (max_clu_dH <> MIN_VALUE) and (max_clu_dH < 0) then
     case max_clu_code of
       DEF_FOREST   : Result:=prtHalfOpened_forest;
       DEF_COUNTRY  : Result:=prtHalfOpened_country;
       DEF_CITY     : Result:=prtHalfOpened_city;
       else           Result:=prtHalfOpened;
     end;


   if (max_rel_dH = MIN_VALUE) and
      (max_clu_dH = MIN_VALUE)
   then
     Result:=prtOpen;

  end;    // with
     
end;

//-------------------------------------------------------------------
class function TRelProfile_tools.GetType_with_Los(aRelProfile: TrelProfile;
    aAntennaH1, aAntennaH2, aFreq_GHz: double; var aLosValue: double):
    TrelProfileType;
//-------------------------------------------------------------------
// aLosValue - ������� ����� ������ ������...
//
const
  MIN_VALUE = -1000;

var i, i_max_rel, i_max_clu: integer;
  H_mp,R,H1,H2: double;

  dX,dY,dH,h,
  eFullDistance_KM,
  mHp,mHp_Ho: double;

  max_clu_code: integer;
  tang,min_dH,max_rel_h,max_clu_h,
  max_rel_dH,max_clu_dH: double;

begin
  Result:=prtUnknown;

  with aRelProfile do
  begin



//  R:=GeoDistance() ;
  R:=Data.Distance_km ;
  if R=0 then
    Exit;

  eFullDistance_KM:=GeoDistance_KM() ;


//  Data.Items[0].

  H1 := FirstItem().Rel_H + aAntennaH1; {������ ��������� � ����� ��+������ ������� ��}
  H2 := LastItem().Rel_H  + aAntennaH2; {������ ��������� � ����� ��+������ ������� ��}

  // ���������� �������� ���� ������� ���� ������
  tang := (H2 - H1) / R;

  max_clu_code:=MIN_VALUE;
  max_rel_dH:=MIN_VALUE;
  max_clu_dH:=MIN_VALUE;

  aLosValue := MIN_VALUE;

  i_max_rel:=0;
  i_max_clu:=0;


   for i:=1 to Count-1 do
   begin
     dX:=Data.Items[i].Distance_km; // delta;

//     dX:=i * Data.Step_R; // delta;
     h:= H1 + tang*dX; //������ ����� ��.�����.


     // ��������� ����������� �������� ���� �������
     // �� 27.10.06
     //if (Items[i].Rel_H > h - dY) then

     //---------------------------------------------------------
     // ������ LOS
     //---------------------------------------------------------

     if aLosValue <(Data.Items[i].Full_H - Data.Items[i].Clutter_H - h) then
       aLosValue := (Data.Items[i].Full_H - Data.Items[i].Clutter_H - h);

     if aLosValue<(Data.Items[i].Full_H - h) then
       aLosValue := (Data.Items[i].Full_H - h);


     if aFreq_GHz=0 then
       Continue;


     // ������ ���� �������
     dY:=Sqrt((300 / (aFreq_GHz*1000))* 0.3 * //Params.NFrenel
    //         (1000*dx)* (1 - dx/(GeoDistance())));
             (1000*dx)* (1 - dx/eFullDistance_KM));




     if ((Data.Items[i].Full_H - Data.Items[i].Clutter_H) > h - dY) then
     begin
       // ������� ����� �������� � ���
       dH:=Data.Items[i].Full_H - Data.Items[i].Clutter_H - h;
       // ���� ��� ����� ��������� �������
       if dH > max_rel_dH then  begin
         i_max_rel:=i;
         max_rel_dH:=dH;
       end
     end else
     // ��������� ����������� �� � ���� �������
     if (Data.Items[i].Full_H > h - dY) then
     begin
       // ������� ����� �� � ���
       dH:=Data.Items[i].Full_H - h;
       // ���� ��� ����� ��������� �������
       if dH > max_clu_dH then begin
         i_max_clu:=i;
         max_clu_dH:=dH;
         max_clu_code:=Data.Items[i].Clutter_Code;
       end;
     end;
   end;

//   aLosValue := max_clu_dH;


   // 1. ������ ���� ���� ������ ���������
   if (max_rel_dH > 0) then
     Result:=prtClosed_relief
   else

   // 2. �� ���� ���� ������ ���������

//   aLosValue := max_clu_dH;


   if (max_clu_dH > 0) then           // ��������� max_clu_dH
     case max_clu_code of
       DEF_FOREST   : Result:=prtClosed_forest;
       DEF_COUNTRY  : Result:=prtClosed_country;
       DEF_CITY     : Result:=prtClosed_city;
       else       Result:=prtClosed;
     end else

   // 3. ������ ���� ���� �������
   if (max_rel_dH <> MIN_VALUE) and (max_rel_dH <= 0) then
     Result:=prtHalfOpened_relief
   else

   // 4. �� ���� ���� �������
   if (max_clu_dH <> MIN_VALUE) and (max_clu_dH < 0) then
     case max_clu_code of
       DEF_FOREST   : Result:=prtHalfOpened_forest;
       DEF_COUNTRY  : Result:=prtHalfOpened_country;
       DEF_CITY     : Result:=prtHalfOpened_city;
       else       Result:=prtHalfOpened;
     end;


   if (max_rel_dH = MIN_VALUE) and
      (max_clu_dH = MIN_VALUE)
   then Result:=prtOpen;

  end;    // with


//  Result:=Result;
end;

end.
