library dll_rel;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  SysUtils,
  Classes,
  u_bee_Matrix in '..\src\u_bee_Matrix.pas',
  u_profile_types in '..\src\u_profile_types.pas',
  u_rel_engine in '..\src\u_rel_engine.pas',
  u_rel_Matrix_base in '..\src\u_rel_Matrix_base.pas',
  u_rel_NASA in '..\src\u_rel_NASA.pas',
  u_rel_Neva_Matrix in '..\src\u_rel_neva_Matrix.pas',
  u_rel_Profile in '..\src\u_rel_Profile.pas',
  u_rlf2_Matrix in '..\src\u_rlf2_Matrix.pas',
  u_rlf_Matrix in '..\src\u_rlf_Matrix.pas',
  u_Trf_matrix in '..\src\u_Trf_matrix.pas',
  I_rel_Matrix1 in '..\src_shared\I_rel_Matrix1.pas';

{$R *.res}

begin
end.
