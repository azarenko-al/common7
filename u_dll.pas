unit u_dll;

interface
uses
  SysUtils,
  Windows;


//function GetInterface_BPL(const aBplFileName: string; var aHandle: Integer; const IID:
//    TGUID; var Obj): HResult;

function GetInterface_dll(const aDllFileName: string; var aHandle: Integer;
    const IID: TGUID; var Obj): HResult;

procedure UnloadPackage_dll(var aHandle: Integer);

function RegisterOCX(FileName: string): Boolean;


implementation


// ---------------------------------------------------------------
function GetInterface_dll(const aDllFileName: string; var aHandle: Integer;
    const IID: TGUID; var Obj): HResult;
// ---------------------------------------------------------------
type
  tfunc = function(const IID: TGUID; var Obj): HResult; stdcall;
var
  func: tfunc;

//  p: PAnsiChar;

 // sProc: AnsiString;
begin
 // sProc:=aProcName;

 // p:=PAnsiChar(AnsiString(aProcName));

  Result:=S_FALSE;
  Integer(Obj):=0;

  if aHandle = 0 then
    aHandle:=LoadLibrary(PAnsiChar(aDllFileName));

  if aHandle >= 32 then
  begin
//    @func:=GetProcAddress(aHandle, PAnsiChar(aProcName)); //'DllGetInterface');
    @func:=GetProcAddress(aHandle, 'DllGetInterface');
//    @func:=GetProcAddress(aHandle, PAnsiChar(AnsiString(aProcName)));
    if Assigned(func) then
      Result:=func(IID,Obj)
    else
//      raise Exception.Create('function '+aProcName+'(const aDllFileName: string; var aHandle: Integer; const IID:');
      raise Exception.Create('function DllGetInterface(const aDllFileName: string; var aHandle: Integer; const IID:');
  end;

  if Result = S_FALSE then
     Exception.Create('function GetInterface_dll(const aDllFileName: string; var aHandle: Integer; const IID:');

end;



procedure UnloadPackage_dll(var aHandle: Integer);
begin
  if aHandle > 0 then
  begin
    FreeLibrary(aHandle);
    aHandle :=0;
  end;
end;



// ---------------------------------------------------------------
function RegisterOCX(FileName: string): Boolean;
// ---------------------------------------------------------------
type
  TDllRegisterServer = function: HResult; stdcall; 
var
  OCXHand: THandle;
  RegFunc: TDllRegisterServer;
begin
  Assert(FileExists(FileName));


  OCXHand := LoadLibrary(PAnsiChar(FileName));
  RegFunc := GetProcAddress(OCXHand, 'DllRegisterServer');

  if @RegFunc <> nil then
    Result := RegFunc = S_OK
  else
    Result := False;

  Assert(Result);


  FreeLibrary(OCXHand);
end;




end.


(*

procedure TForm6.Button1Click(Sender: TObject);
var
  iPackageModule: HModule;
  AClass: TPersistentClass;
begin
  iPackageModule := LoadPackage('Link_Freq_report.bpl');

  if iPackageModule <> 0 then
  begin
    AClass := GetClass('TForm5');

    if AClass <> nil then
      with TComponentClass(AClass).Create(Application)
        as TCustomForm do
      begin
        ShowModal;
        Free;
      end;

    UnloadPackage(iPackageModule);
  end;
end;
*)



 {
     
// ---------------------------------------------------------------
function GetInterface_BPL(const aBplFileName: string; var aHandle: Integer; const IID:
    TGUID; var Obj): HResult;
// ---------------------------------------------------------------
type
  tfunc = function(const IID: TGUID; var Obj): HResult; stdcall;
var
  func: tfunc;
begin
  Result:=S_FALSE;
  Integer(Obj):=0;

  if aHandle = 0 then
    aHandle:=LoadPackage(aBplFileName);

  if aHandle > 0 then
  begin
    @func:=GetProcAddress(aHandle, 'DllGetInterface');
    if Assigned(func) then
      Result:=func(IID,Obj)
    else
      raise Exception.Create('function GetInterface_dll(const aDllFileName: string; var aHandle: Integer; const IID:');
  end;

  if Result = S_FALSE then
     Exception.Create('function GetInterface_dll(const aDllFileName: string; var aHandle: Integer; const IID:');

end;






function RegisterOCX(FileName: string): Boolean;
type 
  TDllRegisterServer = function: HResult; stdcall; 
var
  OCXHand: THandle;
  RegFunc: TDllRegisterServer;
begin
  OCXHand := LoadLibrary(PAnsiChar(FileName));
  RegFunc := GetProcAddress(OCXHand, 'DllRegisterServer');
  if @RegFunc <> nil then
    Result := RegFunc = S_OK
  else
    Result := False;
  FreeLibrary(OCXHand);
end;



begin
  RegisterOCX (IncludeTrailingBackslash( ExtractFileDir (Application.ExeName))
               + 'xdmx.ocx');


  Application.Initialize;
  Application.Title := 'RPLS - ������� ������������ ����������';
  Application.HelpFile := '';
  Application.CreateForm(TdmMain_init_, dmMain_init_);
  Application.Run;
end.






uses shellapi;
...
function ExecAndWait(const ExecuteFile, ParamString : string): boolean;
var
SEInfo: TShellExecuteInfo;
ExitCode: DWORD;
begin
FillChar(SEInfo, SizeOf(SEInfo), 0);
SEInfo.cbSize := SizeOf(TShellExecuteInfo);
with SEInfo do begin
fMask := SEE_MASK_NOCLOSEPROCESS;
Wnd := Application.Handle;
lpFile := PChar(ExecuteFile);
lpParameters := PChar(ParamString);
nShow := SW_HIDE;
end;
if ShellExecuteEx(@SEInfo) then
begin
repeat
Application.ProcessMessages;
GetExitCodeProcess(SEInfo.hProcess, ExitCode);
until (ExitCode STILL_ACTIVE) or Application.Terminated;
Result:=True;
end
else Result:=False;
end;
