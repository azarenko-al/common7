unit dm_FastReport;

interface

uses

  u_doc,

//  dm_Onega_DB_data,

//  dm_MDB_fp,

//  dm_Link_freq_plan,

//  u_radio,

  u_FastReport,

//  dm_report,

//  u_dll_geo_convert,

//  u_geo_convert_new,

//  u_geo,

//  u_Link_freq_report_classes,

  u_func,

//  u_const_db,
//
  u_files,

//  dm_Main,
//  u_db,

  SysUtils, Classes, Forms, frxDBSet, DB, ADODB, StdCtrls, Dialogs,
  frxDesgn, frxExportPDF, frxExportXLS, frxClass, frxExportRTF,

   dxmdaset, Controls, frxExportBaseDialog
  ;

type
  TdmFastReport = class(TDataModule)
//    Button1: TButton;
    frxReport: TfrxReport;
    frxRTFExport1: TfrxRTFExport;
    frxXLSExport1: TfrxXLSExport;
    frxPDFExport1: TfrxPDFExport;
    frxDesigner1: TfrxDesigner;
//    procedure DataModuleDestroy(Sender: TObject);
//    procedure DataModuleCreate(Sender: TObject);
//    procedure frxDesigner1GetTemplateList(List: TStrings);
//    procedure qry_Linkend2CalcFields(DataSet: TDataSet);
 //   procedure qry_LInksAfterScroll(DataSet: TDataSet);
//    procedure qry_property1CalcFields(DataSet: TDataSet);

//    procedure qry_LinkEnd1CalcFields(DataSet: TDataSet);
//    procedure qry_LInksCalcFields(DataSet: TDataSet);

  private
//    FAllowUpdated : Boolean;
//    FDataPrepared : Boolean;

//    FAntennaList1: TAntennaList;
//    FAntennaList2: TAntennaList;

//    FDataSet_antenna1: TDataSet;
//    FDataSet_antenna2: TDataSet;

//    FfrxReport: TfrxReport;


//    FAllowCalcFields: boolean;

    procedure DoExecExportByFilter(aExportFilter: TfrxCustomExportFilter; aExt:
        String);
//    procedure UpdateAntennaInfo;
   // procedure Open_Designer;
    procedure PrintReport;
    procedure ReportExportByFilter(aExportFilter: TfrxCustomExportFilter; aExt,
        aFileName: String);
   // procedure SetConnectionObject(aConnectionObject: _Connection);
    function ShowDesignReport: Boolean;
    { Private declarations }

//    procedure Prepeare_Data;


  public
    Params: record
        //FileType : string;
        FileType : (ftExcel_,ftWord_,ftPDF_);
        FileName : string;

//        IsPreview : Boolean;
        IsOpenReportAfterDone : Boolean;

//        ID_str : string;

//        Report_ID : Integer;
      end;


    function DesignReport: boolean;
//    procedure DesignReport;

//    procedure Execute(aID_str: string);
//    procedure Execute;

    class procedure Init;
//    procedure Open1;

//    procedure SetADOConnection(aADOConnection: TADOConnection);
//    procedure TestAdd;
  end;

var
  dmFastReport: TdmFastReport;

implementation  
{$R *.dfm}                                                                                          


class procedure TdmFastReport.Init;
begin
  if not Assigned(dmFastReport) then
    dmFastReport := TdmFastReport.Create(Application);

end; 


// ---------------------------------------------------------------
procedure TdmFastReport.ReportExportByFilter(aExportFilter:
    TfrxCustomExportFilter; aExt, aFileName: String);
// ---------------------------------------------------------------
var
  b: Boolean;
  sDocFile: string;
begin
  assert(aFileName<>'');

  aFileName:= ChangeFileExt(aFileName, aExt);

  b:= DeleteFile (aFileName);

  if FileExists(aFileName) then
     aFileName:= ChangeFileExt(aFileName, Format('_%d',[Random (100000)]) + aExt);



  aExportFilter.FileName := aFileName;
  aExportFilter.ShowDialog:= False;

//  FfrxReport.Export(aExportFilter);


  if Eq(aExt, '.rtf') then
  begin
    sDocFile:=ChangeFileExt(aFileName, '.doc');

    b:= DeleteFile (sDocFile);

    if FileExists(sDocFile) then
       sDocFile:= ChangeFileExt(sDocFile, Format('_%d',[Random (100000)]) + '.doc');


    RTF_to_doc (aFileName,  sDocFile);

    DeleteFile (aFileName);

   aFileName:= ChangeFileExt(aFileName, '.doc');

  end;




 if Params.IsOpenReportAfterDone then
    ShellExec (aFileName,'');

//      aExportFilter.ShowDialog:= True;
end;





// ---------------------------------------------------------------
procedure TdmFastReport.PrintReport;
// ---------------------------------------------------------------
var
  sTempFileName: string;
begin
  frxReport.PrepareReport;

  sTempFileName := Params.FileName;

//  dmOnega_DB_data.Activity_Log('link_freq_report', dmMain.ProjectID);

//Assert ( dmMain.ProjectID > 0);

{
  dmOnega_DB_data.ExecStoredProc_('sp_Activity_Log',
      [FLD_NAME,      'link_freq_report',
       FLD_Project_ID, dmMain.ProjectID,

       'param_name', 'link_count',
       'param_value', intToStr( qry_LInks.RecordCount)

      ]);

 }


  case Params.FileType of
    ftExcel_ : ReportExportByFilter(frxXLSExport1, '.xls', sTempFileName);
    ftWord_  : ReportExportByFilter(frxRTFExport1, '.doc', sTempFileName);
    ftPDF_   : ReportExportByFilter(frxPDFExport1, '.pdf', sTempFileName);
  end;

end;


// ---------------------------------------------------------------
procedure TdmFastReport.DoExecExportByFilter(aExportFilter:
    TfrxCustomExportFilter; aExt: String);
// ---------------------------------------------------------------
begin
  assert(params.FileName<>'');

  aExportFilter.FileName:= ChangeFileExt(params.FileName, aExt);
  aExportFilter.ShowDialog:= False;
  frxReport.Export(aExportFilter);
//      aExportFilter.ShowDialog:= True;
end;


// ---------------------------------------------------------------
function TdmFastReport.ShowDesignReport: Boolean;
// ---------------------------------------------------------------
var
  b: Boolean;
  oStream1: TMemoryStream;
  oStream2: TMemoryStream;
begin
  oStream1:=TMemoryStream.Create;
  oStream2:=TMemoryStream.Create;

  frxReport.SaveToStream(oStream1);


  frxReport.DesignReport(True);
//  frxReport.m
  b:=frxReport.Modified;

  frxReport.SaveToStream(oStream2);


  b:=not u_func.CompareMemoryStreams(oStream1,oStream2);

  Result := b;

  FreeAndNil(oStream1);
  FreeAndNil(oStream2);

end;

// ---------------------------------------------------------------
function TdmFastReport.DesignReport: boolean;
// ---------------------------------------------------------------
var
  s: string;
  sTemplateFileName: string;
begin
//  s:=Format('SELECT * FROM %s where id=0', [DEF_reports_VIEW_LINK_FREQ_ORDER]);

 // /'/  SELECT * FROM	view_Link  where id IN (@ID_STR)  order by name

//  db_OpenQuery(qry_LInks, s);

//  frxDBDataset_UpdateAll(Self);


//  sTemplateFileName := g_Link_Report_Param.FileDir + Format('report_%d.rep', [g_Link_Report_Param.Report_ID]);
  sTemplateFileName := Params.FileName + '_tmp_rep.fr3';

 // Assert(Params.Report_ID>0);

//  dmReport.SaveToFile(Params.Report_ID, sTemplateFileName);

  frxReport.LoadFromFile(sTemplateFileName);

  if ShowDesignReport then
  begin
    frxReport.SaveToFile(sTemplateFileName);

//    dmReport.LoadFromFile(Params.Report_ID, sTemplateFileName);

  end;

end;



end.

(*


{

// ---------------------------------------------------------------
procedure TdmFastReport.Execute;
// ---------------------------------------------------------------
var
  b: Boolean;
  sTempFileName: string;
  s: string;
begin
  Prepeare_Data();


    {
    db_View(qry_LInks_new);
    db_View(mem_Antenna1);
    db_View(mem_Antenna2);
   }

 ///// tfrm_test.ExecDlg;


  Assert(Params.Report_ID>0);


//  sTempFileName :=  'd:\1111111111111111.fr3';
  sTempFileName := GetTempFileNameWithExt('.fr3');

  b:=dmReport.SaveToFile(Params.Report_ID, sTempFileName);

  Assert(b, ' b:=dmReport.SaveToFile(Params.Report_ID, sTempFileName);');

 // sHOWModal;


//  dmMain.ADOConnection1.Execute(s);

  frxReport.LoadFromFile(sTempFileName);

  DeleteFile(sTempFileName);



//
//  Open_Designer;
//
//  Exit;
//

//  frxReport.Lo


  if Params.IsPreview then
    FfrxReport.ShowReport

  else
    PrintReport();


//  exec sp_Link_report '109902,109926,109903,109933,109904,'

end;

procedure TdmFastReport.frxDesigner1GetTemplateList(List: TStrings);
begin

end;


// ---------------------------------------------------------------
procedure TdmFastReport.Prepeare_Data;
// ---------------------------------------------------------------
var
  b: Boolean;
  sTempFileName: string;
  s: string;
begin
  if FDataPrepared then
    exit;


//  Open;

//  s:=Format('exec %s ''%s''', [sp_Link_report, aID_str]);
  Assert(Params.ID_str<>'');


//    db_OpenQuery(qry_LInks, 'select * from '+ DEF_View_Link_freq_order +'  where id=0');

  Assert (Params.ID_str <> '');


  s:=Format('SELECT * FROM %s where id IN (%s) order by name', [DEF_reports_VIEW_LINK_FREQ_ORDER, Params.ID_str]);

 // /'/  SELECT * FROM	view_Link  where id IN (@ID_STR)  order by name

  db_OpenQuery(qry_LInks, s);

  Assert (qry_LInks.RecordCount > 0, 'qry_LInks.RecordCount = 0');





  frxDBDataset_UpdateAll(Self);

  // FDataPrepared:=True;

//  exec sp_Link_report '109902,109926,109903,109933,109904,'

end;




// ---------------------------------------------------------------
procedure TdmFastReport.qry_LInksCalcFields(DataSet: TDataSet);
// ---------------------------------------------------------------

    procedure DoExec (aPrefix: string);
    var
      bl: TBLPoint;
      bl_WGS: TBLPoint;
      bl_CK95: TBLPoint;
      bl_GCK: TBLPoint;

    begin
      bl.B:=DataSet.FieldByName(aPrefix+ FLD_lat).AsFloat;
      bl.L:=DataSet.FieldByName(aPrefix + FLD_lon).AsFloat;

      bl_WGS :=geo_Pulkovo42_to_WGS84(bl);
      bl_CK95:=geo_Pulkovo42_to_CK_95(bl);

       CK42_to_GSK_2011(bl.B, bl.L, bl_GCK.B, bl_GCK.L);
    //      external DEF_LIB_geo_convert;


      DataSet[aPrefix + FLD_lat_wgs]:=bl_WGS.B;
      DataSet[aPrefix + FLD_lon_wgs]:=bl_WGS.L;

      DataSet[aPrefix + FLD_lat_CK95]:=bl_CK95.B;
      DataSet[aPrefix + FLD_lon_CK95]:=bl_CK95.L;


      DataSet[aPrefix + FLD_LAT_GCK_2011]:=bl_GCK.B;
      DataSet[aPrefix + FLD_LON_GCK_2011]:=bl_GCK.L;

    end;




begin
  if not FAllowCalcFields then
    exit;

  DoExec ('Linkend1_property_');
  DoExec ('Linkend2_property_');

  DoExec ('property1_');
  DoExec ('property2_');


end;




// ---------------------------------------------------------------
procedure TdmFastReport.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
//  if ADOConnection1.Connected then
//    ShowMessage('procedure TdmLink_freq_plan_report.FormCreate(Sender: TObject); - ADOConnection1.Connected ');

//  aQuery.Connection.Connected

  {
  db_SetComponentADOConnection(Self, dmMain.ADOConnection1 );

  Params.IsPreview := True;
  Params.FileType := ftExcel_;
  }
 // Open;

end;

