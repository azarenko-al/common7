unit u_dll_login;

interface

uses Registry,ADODB, DB,SysUtils,Dialogs
     ;

type

  TdbLoginRec = packed record
    Server:     ShortString;
    DataBase:   ShortString;

    Login:      ShortString;
    Password:   ShortString;

    IsUseWinAuth: boolean;  //win or SQL server

    NetworkLibrary: ShortString;
    NetworkAddress: ShortString;


  //  User1: ShortString;

    //extended

//    DynamicPort: Boolean;
 //   PortNumber: integer;


   // DefaultDB:   string;
  end;


implementation


end.

{


  //-----------------------------

  function db_Dlg_Login(aRegPath: string; var aLoginRec: TdbLoginRec; aADOConnection: TADOConnection): boolean;

  //-----------------------------

  procedure db_SaveConnectRecToReg(aRegPath: String; aDBLoginRec: TdbLoginRec);
  procedure db_LoadConnectRecFromReg(aRegPath: String; var aDBLoginRec: TdbLoginRec);

  function db_ADO_CheckConnectionToSQLServer(aLoginRec: TdbLoginRec): boolean;
//  function  db_ADO_CheckAdminRights (aLoginRec: TdbLoginRec): boolean;
  function  db_ADO_MakeConnectionString (aRec: TdbLoginRec): string;

//  function db_GetLoggedUserName(aADOConnection: TADOConnection): string;

  function db_OpenLoginRec(aADOConnection: TADOConnection; var aLoginRec: TdbLoginRec): boolean;

  function db_GetConnectionStatusStr(aLoginRec: TdbLoginRec): ShortString;



implementation

uses
  dm_Login,
  d_DB_Login,
  u_sql;


function db_Dlg_Login(aRegPath: string; var aLoginRec: TdbLoginRec; aADOConnection: TADOConnection): boolean;
begin
  Result:=Tdlg_DB_Login.ExecDlg (aRegPath, aLoginRec, aADOConnection);
end;


//--------------------------------------------------------
function Eq(aValue1, aValue2: string): boolean;
//--------------------------------------------------------
begin
  Result := AnsiSameText(aValue1,aValue2);
  //Result := AnsiCompareText(Trim(aValue1),Trim(aValue2))=0;
end;


//--------------------------------------------------------------------
procedure db_LoadConnectRecFromReg(aRegPath: String; var aDBLoginRec:
    TdbLoginRec);
//--------------------------------------------------------------------
 begin

  FillChar(aDBLoginRec, SizeOf(aDBLoginRec), 0);

  with TRegIniFile.Create(aRegPath), aDBLoginRec do
  begin
    Server        := ReadString ('', 'Server',     '');
    DataBase      := ReadString ('', 'DataBase', '');

    Login         := ReadString ('', 'Login',       '');

    if Login='' then
      Login:='sa';

  // ReadString ('', 'User',        '');

 //    User          := ReadString ('', 'USER',       '');
    Password      := ReadString ('', 'Password',   '');

    IsUseWinAuth  :=  ReadBool ('', 'UseWinAuth', False);

    NetworkLibrary:= ReadString ('', 'NetworkLibrary', 'DBNMPNTW');
    NetworkAddress:= ReadString ('', 'NetworkAddress', Server);

    Free;
  end;
end;

//--------------------------------------------------------------------
procedure db_SaveConnectRecToReg(aRegPath: String; aDBLoginRec: TdbLoginRec);
//--------------------------------------------------------------------
begin
  with TRegIniFile.Create (aRegPath), aDBLoginRec do
  begin
    WriteString ('', 'Server',     Server);
    WriteString ('', 'DataBase',   DataBase);
    WriteString ('', 'Login',       Login);
   //  WriteString ('', 'USER',       User);
    WriteString ('', 'Password',   Password);
    WriteBool   ('', 'UseWinAuth', IsUseWinAuth);

    WriteString ('', 'NetworkLibrary', NetworkLibrary);
    WriteString ('', 'NetworkAddress', NetworkAddress);
    Free;
  end;

end;


//--------------------------------------------------------------------
function db_ADO_CheckConnectionToSQLServer(aLoginRec: TdbLoginRec): boolean;
//--------------------------------------------------------------------
//var
//  oADOConnection: TADOConnection;
begin
  TdmLogin.Init;
  Result:= dmLogin.ADO_CheckConnectionToSQLServer(aLoginRec);
  FreeAndNil(dmLogin);


(*  oADOConnection:=TADOConnection.Create(nil);
  oADOConnection.LoginPrompt:=False;
// oADOConnection.DefaultDatabase:='master';

 // oADOConnection.Close;
  aLoginRec.DataBase:='master';

  oADOConnection.ConnectionString:=db_ADO_MakeConnectionString (aLoginRec);

  try
    oADOConnection.Open;
  except
    oADOConnection.Close;
  end;
*)

{
  try
    ADOConnection.Open;
    bIsConnected := True;
  except
    on E: Exception do
    begin
      ADOConnection.Close;
      ErrorMsg := E.Message;
      bIsConnected := False;
    end;
  end;}

(*

  Result:= oADOConnect
  ion.Connected;
  oADOConnection.Free;*)


end;

//----------------------------------------------------------------------------
function db_ADO_MakeConnectionString (aRec: TdbLoginRec): string;
//----------------------------------------------------------------------------
    //----------------------------------------------------------------------------
    function db_ADO_MakeConnectionString : string;
    //----------------------------------------------------------------------------
    const
      DEFAULT_NETWORK_LIB = 'DBNMPNTW';

      CONNECTION_STRING_WINDOWS_AUTH =
        'Provider=SQLOLEDB.1;'+
        'Integrated Security=SSPI;'+
        'Persist Security Info=False;'+
        'Data Source=%s;';
       // +
        //'Initial Catalog=%s;';

       CONNECTION_STRING_SQL_AUTH_LOCAL =
        'Provider=SQLOLEDB.1;'+
        'Persist Security Info=False;'+
        'Initial Catalog=%s;'+
        'Data Source=%s;'+
        'User ID=%s;'+
        'Password=%s;';

     CONNECTION_STRING_SQL_AUTH =
        'Provider=SQLOLEDB.1;'+
        'Network Library=%s;'+
        'Network Address=%s;'+
        'Persist Security Info=False;'+
//        'Persist Security Info=True;'+
        'Use Encryption for Data=False;'+
        'Initial Catalog=%s;'+
        'Data Source=%s;'+
        'User ID=%s;'+
        'Password=%s;';

    var
      sLocalCompName: string;

    begin
      with aRec do
      begin
        if NetworkLibrary='' then NetworkLibrary:=DEFAULT_NETWORK_LIB;
        if NetworkAddress='' then NetworkAddress:=Server;

        if IsUseWinAuth then
          Result:=Format(CONNECTION_STRING_WINDOWS_AUTH, [Server,DataBase])

        else begin
          sLocalCompName:= db_GetLocalComputerName();

          if Eq(Server, 'local')       or
             Eq(Server, '(local)')     or
             Eq(Server, '127.0.0.1')   or
             Eq(Server, sLocalCompName) or
             Eq(NetworkLibrary, DEFAULT_NETWORK_LIB)
          then
            Result:= Format(CONNECTION_STRING_SQL_AUTH_LOCAL,
                        [Database, Server, Login, Password])
          else
            Result:= Format(CONNECTION_STRING_SQL_AUTH,
                        [NetworkLibrary, NetworkAddress, Database,Server,Login, Password]); //Database,
        end;

      end;

    end;
    //----------------------------------------------------------------------------


begin
  with aRec do
    Result:=db_ADO_MakeConnectionString;// (Server, DataBase, User, Password, IsUseWinAuth);

//  ShowMessage(result);
end;


(*//----------------------------------------------------------------------------
function db_GetLoggedUserName(aADOConnection: TADOConnection): string;
//----------------------------------------------------------------------------
const
  DEF_SQL =
     'SELECT su.name as UserName, sl.name AS login              '+
     'FROM   sysusers su INNER JOIN                 '+
     '     master..syslogins sl ON su.sid = sl.sid  '+
     'WHERE su.name=user_name()                     ';

var
  oQuery: TADOQuery;
begin
  oQuery := TADOQuery.Create(nil);
  oQuery.Connection:=aADOConnection;

  oQuery.SQL.Text:=DEF_SQL;
  oQuery.Open;

  Result := oQuery.FieldByName('UserName').AsString;

  oQuery.Free;
end;
*)

//------------------------------- -------------------------------------
function db_OpenLoginRec(aADOConnection: TADOConnection; var aLoginRec:
    TdbLoginRec): boolean;
//--------------------------------------------------------------------
//var sConn: string;
begin
//  sConn:=db_ADO_MakeConnectionString (aLoginRec);
  if (aLoginRec.Server='') or (aLoginRec.DataBase='') then
  begin
    Result := False;
    Exit;
  end;

  aADOConnection.Close;
  aADOConnection.ConnectionString:=db_ADO_MakeConnectionString (aLoginRec);
  aADOConnection.DefaultDatabase:=aLoginRec.DataBase;

  try
    aADOConnection.Open;

////////    aLoginRec.User1:= db_GetLoggedUserName(aADOConnection);

  except
    ShowMessage ( '�� ������ ������������ � ���� ������.' );
  end;

  Result:= aADOConnection.Connected;

end;


function db_GetConnectionStatusStr(aLoginRec: TdbLoginRec): ShortString;
begin
  with aLoginRec do
    Result:=Format('������: %s  |  ��: %s  |  ������������: %s',//  |  ������: "%s"',
               [Server, Database, Login]); //, Password]);
end;


end.


{
//--------------------------------------------------------
function  db_ADO_CheckAdminRights (aLoginRec: TdbLoginRec): boolean;
//--------------------------------------------------------
var
  oUsersStoredProc: TAdoStoredProc;
  oADOConnection: TADOConnection;

begin
 // Result:= false;
  oADOConnection:=TADOConnection.Create(nil);
  oADOConnection.LoginPrompt:=False;
  oADOConnection.ConnectionString:=db_ADO_MakeConnectionString (aLoginRec);

  oUsersStoredProc:= TAdoStoredProc.Create(nil);
  oUsersStoredProc.Connection:= oADOConnection;
  oUsersStoredProc.ProcedureName:= 'sp_helplogins';

  try
    oADOConnection.Open;
    oUsersStoredProc.Open;
    Result:= true;
  except
   // oUsersStoredProc.Free;
    Result:= false;
   // exit;
  end;

  oUsersStoredProc.Free;
  oADOConnection.Free;
end;
}



{

  CONNECTION_STRING_WINDOWS_AUTH =
    'Provider=SQLOLEDB.1;'+
    'Integrated Security=SSPI;'+
    'Persist Security Info=False;'+
    'Data Source=%s;'
//    'Auto Translate=True;Packet Size=4096;Use Procedure for Prepare=1;'+
//    'Use Encryption for Data=False;Tag with column collation when possible=False;'
    ;

  CONNECTION_STRING_SQL_AUTH =
    'Provider=SQLOLEDB.1;'+
    'Network Library=%s;Network Address=%s;'+
    'Persist Security Info=False;'+
    'Initial Catalog=%s;'+
    'Data Source=%s;'
/////    'User ID=%s;Password=%s;'+

//    'Auto Translate=True;Packet Size=4096;Use Procedure for Prepare=1;'+
//    'Use Encryption for Data=False;Tag with column collation when possible=False;'
    ;

  CONNECTION_STRING_SQL_AUTH_LOCAL =
    'Provider=SQLOLEDB.1;'+
    'Persist Security Info=False;'+
    'Initial Catalog=%s;'+
    'Data Source=%s;'
//    'Password=%s;User ID=%s;'
//    'Auto Translate=True;Packet Size=4096;Use Procedure for Prepare=1;'+
//    'Use Encryption for Data=False;Tag with column collation when possible=False;'
    ;}

//'Provider=SQLOLEDB.1;Network Library=DBMSSOCN;Network Address=10.16.12.15,1054;
//'Persist Security Info=False;Initial Catalog=dbbs;Data Source=cleo\plan1'


//  CONNECTION_ADDITION_TCP_IP = 'Network Library=%s;Network Address=%s;'; ///
