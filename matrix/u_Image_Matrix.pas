unit u_image_matrix;

interface

uses ExtCtrls, Windows, Graphics, Math,
     u_Geo,
     u_Geo_convert_new,
     u_Matrix;

type
  TOnProgress = procedure(aPosition, aTotal : integer; var aTerminated : boolean) of object;

  //��� ��������������� ������� - ����������� ��� ���� ����� - ��� TRUE
  TImageMatrix = class(TByteMatrix)  //class(TIntMatrix)
  private
    FTotal : integer;
//    FCount : integer;
    FOnProgress: TOnProgress;

  public
    Terminated : boolean;

    function MakeImage (var aBLPolygon: TBLPointArray; aZone : integer) : integer; overload;
    function MakeImage (aCenter: TXYPoint; aRadius: double):integer; overload;

    property OnProgress: TOnProgress read FOnProgress write FOnProgress;
  end;


implementation


//---------------------------------------------------------------------------
function TImageMatrix.MakeImage (var aBLPolygon: TBLPointArray; aZone : integer):integer;
//---------------------------------------------------------------------------
var i, r,c, iCount, iTotal: integer;  x,y:integer;// double;
    xyPolygon: TXYPointArray;
//    xyPoint: TxyPoint;
    bTerminated : boolean;
//    dist1, dist2 : double;

    Image: TImage;
    pt: array of TPoint;
    k : integer;
    bInRegion: boolean;
begin
  Image:= TImage.Create(nil);

  bTerminated := False;
  Result := 0;
  iCount :=0;
  iTotal := RowCount*ColCount;
  FTotal := iTotal;

  if Length(aBLPolygon) = 0 then
    Exit;

  SetLength(xyPolygon, Length(aBLPolygon));

  for i:=0 to High(aBLPolygon) do
    xyPolygon[i]:=geo_BL_to_XY(aBLPolygon[i], aZone);

  //----- canvas ----------------
  k:= Trunc(Max(Step * ColCount, Step * RowCount)/1000);
  if k = 0 then
    k:= 1;

  Image.Width := Round(Step * ColCount / k + 1);
  Image.Height:= Round(Step * RowCount / k + 1);
  Image.Canvas.FloodFill(1,1,clWhite, fsBorder);

  SetLength(pt, Length(xyPolygon));
  for i := 0 to High(xyPolygon) do
  begin
    pt[i].X:= Round((xyPolygon[i].Y - TopLeft.Y)/k);
    pt[i].Y:= Round((TopLeft.X - xyPolygon[i].X)/k);
  end;

  Image.Canvas.Brush.Color:= clBlack;
  Image.Canvas.Pen.Color  := clBlack;
  Image.Canvas.Polygon(pt);
//  Image.Picture.SaveToFile('c:\temp\canvas.bmp');

  x:= -1;

  for r:=0 to RowCount-1 do
  begin
    for c:=0 to ColCount-1 do
    begin
      y:=Round((r+1/2)*Step/k);

      if Round((c+1/2)*Step/k) = x then
      begin
        if bInRegion then
          begin Items[r,c]:=1; Inc(Result); end
        else
                Items[r,c]:=0;

        Continue;
      end;

      x:=Round((c+1/2)*Step/k);

      bInRegion:= (Image.Canvas.Pixels[x,y] = clBlack);
      if bInRegion then
        begin Items[r,c]:=1; Inc(Result); end
      else
              Items[r,c]:=0;

    end;

    if assigned(FOnProgress) then
      OnProgress(r, RowCount, Terminated);

    if Terminated then
      Exit;

  end;

  Image.Free;
end;

//-------------------------------------------------------------------
function TImageMatrix.MakeImage (aCenter: TXYPoint; aRadius: double):integer;
//-------------------------------------------------------------------
var r,c: integer; // iCount, iTotal,
    xyPoint : TXYPoint;
    x,y, dist: double;
begin
  Result := 0;

  for r:=0 to RowCount-1 do
  begin
    for c:=0 to ColCount-1 do
    begin
     // inc(iCount);

      x:=TopLeft.X - (r+1/2)*Step;
      y:=TopLeft.Y + (c+1/2)*Step;
      xyPoint.X := x;
      xypoint.Y := y;

      //!!!!!!!!!!!!
      dist := geo_DistanceXY_m(xyPoint, aCenter);
  //    dist:=Sqrt(Sqr(x-aCenter.X)+Sqr(y-aCenter.Y));

      if dist > aRadius then
       Items[r,c]:= 0   else
       begin
         Items[r,c]:= 1;
         Inc(result);
       End;
    end;

    if Assigned(FOnProgress) then
      OnProgress(r*ColCount+c, RowCount*ColCount, Terminated);

    if Terminated then begin
    //  FTerminated := true;
      Exit;
    end;

  end;
end;


end.
