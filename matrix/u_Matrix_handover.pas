unit u_Matrix_handover;

interface
uses Classes,IniFiles,SysUtils,

FileCtrl,
     u_progress,
     u_Matrix,
     u_matrix_base,
     u_geo,
     u_func;

type
  // ������� handoff: ������� ����� 2 ����. ���������

  //--------------------------------------------------
  TMatrixHandover = class(TIntMatrix)
  //--------------------------------------------------
  public
    HandoffDelta : integer;
    constructor Create;
  end;

  //--------------------------------------------------
  TMatrixHandoverCalc = class(TProgress)
  //--------------------------------------------------
  public
    //������������ ������� �� ������ KUP ������
    //-������������ 2 ���� ������ �������

    procedure Execute (aMatrix: TMatrixHandover;
                                  kupMatrixList: TkupMatrixList;
                                  aSensitivity: double
                                  );
  end;


//======================================================
implementation
//======================================================


//--------------------------------------------------
// THandoffMatrix
// ������� handoff: ������� ����� 2 ����. ���������
//--------------------------------------------------
constructor TMatrixHandover.Create;
begin
  inherited;
  HandoffDelta:=10; //by default
end;

//--------------------------------------------------
procedure TMatrixHandoverCalc.Execute (
                aMatrix: TMatrixHandover;
                kupMatrixList: TkupMatrixList;
                aSensitivity: double);
//--------------------------------------------------
//������������ ������� �� ������ KUP ������
//-������������ 2 ���� ������ �������
var
    r,c,i,iValue,iDelta: integer;
    x,y: double;
    arrTop2E: array[0..1] of integer;

    //---------------------------------------------
    procedure DoPush (Value: integer);
    // value is negative number, push value to stack
    //---------------------------------------------
    var i,iMinInd,iMaxInd: integer;
    begin
      if arrTop2E[0]=0 then begin arrTop2E[0]:=Value; Exit; end;
      if arrTop2E[1]=0 then begin arrTop2E[1]:=Value; Exit; end;

      if (arrTop2E[0] <  arrTop2E[1]) then begin iMinInd:=0; iMaxInd:=1; end else
      if (arrTop2E[0] >= arrTop2E[1]) then begin iMinInd:=1; iMaxInd:=0; end;


      if (arrTop2E[iMinInd]<Value) then
        arrTop2E[iMinInd]:=Value;
    end;

begin
  aMatrix.FillBlank();

  with aMatrix do
  for r:=0 to RowCount-1 do
  for c:=0 to ColCount-1 do begin
    x:=TopLeft.X - (r+1/2)*Step;  //take square center
    y:=TopLeft.Y + (c+1/2)*Step;


    FillChar(arrTop2E, SizeOf(arrTop2E), 0);

    with kupMatrixList do
    for i:=0 to Count-1 do begin
      if (Items[i].FindPointXY (x,y,iValue)) and
         (iValue >= aSensitivity)
      then DoPush (iValue);
    end;

    //define delta between 2 max signals
    if (arrTop2E[0]<>0) and (arrTop2E[1]<>0) then begin
      iDelta:=Abs(arrTop2E[0]-arrTop2E[1]);

      if iDelta<=HandoffDelta then
        Items[r,c]:=iDelta;
    end;
  end;

end;



end.


