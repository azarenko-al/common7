unit u_cx_VGrid;

interface
uses
  SysUtils,Forms,Dialogs ,Classes, Controls,  Registry,
  cxVGrid, Variants, cxGraphics, Graphics, cxCheckBox,    cxTextEdit,
  cxDBLookupComboBox,cxButtonEdit, cxControls, cxDBVGrid, cxLookAndFeels,
  cxDropDownEdit,  cxSpinEdit , FileCtrl,

  cxInplaceContainer,  cxShellBrowserDialog,


  u_func,

  PBFolderDialog;


  procedure cx_InitVerticalGrid(Sender: TcxVerticalGrid);
  procedure cx_InitDBVerticalGrid(Sender: TcxDBVerticalGrid);

  procedure cx_BrowseFileFolder(aOwnerForm: TComponent; Sender: TObject);

  function cx_BrowseFile(Sender: TObject; aFilter: String = ''; aDefaultExt:  String = ''): boolean;

  procedure cx_SetButtonEditText(Sender: TObject; aText: string);

//  procedure cx_VerticalGrid_SaveToReg(aVerticalGrid: TcxVerticalGrid; aRegPath: string);
//  procedure cx_VerticalGrid_LoadFromReg___(aVerticalGrid: TcxVerticalGrid; aRegPath: string);

  procedure cx_ColorRow(aSender: TcxEditorRow;
                        aRowArr: array of TcxEditorRow;
                        ACanvas: TcxCanvas;
                        aIsRequiredParam: boolean = false;
                        Color: TColor = clSilver );

//  procedure cx_Dlg_ExportToFile (aGrid: TcxGrid);

//  procedure InitRes;

  procedure cx_SetReadOnly(aRowArray: array of TcxEditorRow; aReadOnly: boolean = true);

// -------------------------
//TcxEditorRow
// -------------------------

function cx_GetRowItemIndex(aEditorRow: TcxEditorRow): Integer;
procedure cx_SetRowItemIndex(aEditorRow: TcxEditorRow; aItemIndex: Integer);
procedure cx_SetRowItemValues(aEditorRow: TcxEditorRow; aStrArr: array of string);

procedure cx_DBVGrid_check(Sender: TcxDBVerticalGrid);

procedure cx_PostEditValue(Sender: TObject);

procedure cx_VerticalGrid_LoadFromReg_(aVerticalGrid: TcxVerticalGrid;  aRegPath: string);


implementation

//---------------------------------------------------------------------------
procedure cx_InitVerticalGrid(Sender: TcxVerticalGrid);
//---------------------------------------------------------------------------
var
  I: Integer;
  oEditRow: TcxEditorRow;
  oProperties: TcxCheckBoxProperties;
  oProperties_Spin: TcxSpinEditProperties;

begin
  Sender.BorderStyle:=cxcbsNone;
//  Sender.LookAndFeel.Kind:=lfStandard;
  Sender.LookAndFeel.Kind:=lfFlat;
  Sender.OptionsView.ShowEditButtons :=  ecsbFocused;
//  Sender.OptionsView.ShowEditButtons :=  ecsbAlways;


 for I := 0 to Sender.Rows.Count - 1 do
    if Sender.Rows[i] is TcxEditorRow then
    begin
        oEditRow:=TcxEditorRow(Sender.Rows[i]);

//        oEditRow.Properties.EditPropertiesClass.ClassName;

        //------------------------------------------------------
        if oEditRow.Properties.EditPropertiesClass = TcxCheckBoxProperties then
        //------------------------------------------------------
        begin
          oProperties:=TcxCheckBoxProperties(oEditRow.Properties.EditProperties);
          oProperties.UseAlignmentWhenInplace := True;

//          oReg.WriteInteger('', oEditRow.Name, IIF(VarIsNull(v), False, v));
        end else

        //------------------------------------------------------
        if oEditRow.Properties.EditPropertiesClass = TcxSpinEditProperties then
        //------------------------------------------------------
        begin
          oProperties_Spin:=TcxSpinEditProperties(oEditRow.Properties.EditProperties);

  //        oSpinEditProperties.Alignment.Horz:=taLeftJustify;
          oProperties_Spin.UseMouseWheel:=False;
          oProperties_Spin.UseCtrlIncrement:=True;
          oProperties_Spin.SpinButtons.ShowFastButtons:=False;

        //  oProperties_Spin.Properties.Options.ShowEditButtons :=eisbNever;

//          oReg.WriteInteger('', oEditRow.Name, IIF(VarIsNull(v), False, v));
        end


    end;

{
oEditRow.Properties.EditPropertiesClass:=TcxSpinEditProperties;

                oSpinEditProperties:=TcxSpinEditProperties(oEditRow.Properties.EditProperties);

                oSpinEditProperties.OnValidate:=DoOnValidate_CX;
                oSpinEditProperties.Alignment.Horz:=taLeftJustify;
                oSpinEditProperties.UseMouseWheel:=False;
                oSpinEditProperties.UseCtrlIncrement:=True;

                if aFieldInfo.Min_Value<>aFieldInfo.Max_Value then
                begin
                  oSpinEditProperties.MinValue:=aFieldInfo.Min_Value ;
                  oSpinEditProperties.MaxValue:=aFieldInfo.Max_Value ;
                end;

                oSpinEditProperties.SpinButtons.ShowFastButtons:=False;

                 case aFieldInfo.FieldType of
                    vftFloat: oSpinEditProperties.ValueType := vtFloat;
                    vftInt  : oSpinEditProperties.ValueType := vtInt;
                 end;

                 oEditRow.Properties.Options.ShowEditButtons :=eisbNever;

               //  oSpinEditProperties.Options.
}


end;


procedure cx_InitDBVerticalGrid(Sender: TcxDBVerticalGrid);
begin
  Sender.BorderStyle:=cxcbsNone;
//  Sender.LookAndFeel.Kind:=lf Standard;
  Sender.LookAndFeel.Kind:=lfFlat;
 // Sender.OptionsView.ShowEditButtons :=  ecsbAlways;

//  lfOffice11
end;


//-------------------------------------------------------------------
procedure cx_BrowseFileFolder(aOwnerForm: TComponent; Sender: TObject);
//-------------------------------------------------------------------
var
  oDialog: TPBFolderDialog;
  oButtonEdit: TcxButtonEdit;
  cxShellBrowserDialog1: TcxShellBrowserDialog;

//  sDir: string;
begin
  Assert(Sender is TcxButtonEdit);
  oButtonEdit :=Sender as TcxButtonEdit;

  {
  sDir:=oButtonEdit.Text;
  if FileCtrl.SelectDirectory('����� �����...','',sDir) then
    cx_SetButtonEditText (oButtonEdit, sDir);

  Exit;
 }
 {
  cxShellBrowserDialog1:=TcxShellBrowserDialog(aOwnerForm);
  cxShellBrowserDialog1.Path:= oButtonEdit.Text;

  if cxShellBrowserDialog1.Execute then
    cx_SetButtonEditText (oButtonEdit, cxShellBrowserDialog1.Path);


  cxShellBrowserDialog1.Free;
   }

  oDialog:=TPBFolderDialog.Create(aOwnerForm);
  oDialog.Folder:=oButtonEdit.Text;

  if oDialog.Execute then
    cx_SetButtonEditText (oButtonEdit, oDialog.Folder);

  oDialog.Free;

end;


//-------------------------------------------------------------------
function cx_BrowseFile(Sender: TObject; aFilter: String = ''; aDefaultExt:
    String = ''): boolean;
//-------------------------------------------------------------------
var
  oDialog: TOpenDialog;
  sFileName: string;
  oButtonEdit: TcxButtonEdit;
begin
  Assert(Sender is TcxButtonEdit);

  oButtonEdit :=Sender as TcxButtonEdit;


  oDialog:=TOpenDialog.Create(Application);
  oDialog.Filter:=aFilter;

  oDialog.FileName:=oButtonEdit.Text;
  oDialog.InitialDir:=ExtractFileDir(oDialog.FileName);

  Result:=oDialog.Execute;

  if Result then
  begin
    sFileName := oDialog.FileName;
    if aDefaultExt<>'' then
      sFileName:=ChangeFileExt(sFileName, aDefaultExt);

    cx_SetButtonEditText (oButtonEdit, sFileName);

  //  oRow.Properties.Value := sFileName;
  //  oRow.VerticalGrid.HideEdit;
  end;

  FreeAndNil(oDialog);
end;


// ---------------------------------------------------------------
function cx_GetRowItemIndex(aEditorRow: TcxEditorRow): Integer;
// ---------------------------------------------------------------
begin
  Assert(aEditorRow.Properties.EditPropertiesClass = TcxComboBoxProperties );


   Result := -1;

   if not VarIsNull(aEditorRow.Properties.Value) then
     if aEditorRow.Properties.EditPropertiesClass = TcxComboBoxProperties then
     begin
       with TcxComboBoxProperties(aEditorRow.Properties.EditProperties) do
         Result := Items.IndexOf(aEditorRow.Properties.Value);
     end;
end;

// ---------------------------------------------------------------
procedure cx_SetRowItemIndex(aEditorRow: TcxEditorRow; aItemIndex: Integer);
// ---------------------------------------------------------------
var
  oProperties: TcxComboBoxProperties;

begin
  Assert(aEditorRow.Properties.EditPropertiesClass = TcxComboBoxProperties );


  if aItemIndex < 0 then
    aItemIndex:=0;


  if aEditorRow.Properties.EditPropertiesClass = TcxComboBoxProperties then
  begin
    aEditorRow.Properties.DataBinding.ValueType:='String';

    oProperties:=TcxComboBoxProperties(aEditorRow.Properties.EditProperties);

  //   with TcxComboBoxProperties(aEditorRow.Properties.EditProperties) do
  //   begin
       Assert(oProperties.Items.Count>0);

    //   oProperties.Items.

       if aItemIndex <= oProperties.Items.Count-1 then
         aEditorRow.Properties.Value :=oProperties.Items[aItemIndex];
   //  end;
  end;

end;


// ---------------------------------------------------------------
procedure cx_SetRowItemValues(aEditorRow: TcxEditorRow; aStrArr: array of string);
// ---------------------------------------------------------------
var
  I: Integer;
begin
  Assert(aEditorRow.Properties.EditPropertiesClass = TcxComboBoxProperties );


   if aEditorRow.Properties.EditPropertiesClass = TcxComboBoxProperties then
   begin
     with TcxComboBoxProperties(aEditorRow.Properties.EditProperties) do
     begin
       Items.Clear;

       for I := 0 to High(aStrArr) do
         Items.Add(aStrArr[i]);
     end;
   end;

end;
     

//--------------------------------------------------------------
procedure cx_SetButtonEditText(Sender: TObject; aText: string);
//--------------------------------------------------------------
var
  bReadOnly : boolean;
begin
  Assert(Sender is TcxButtonEdit);

  bReadOnly:=(Sender as TcxButtonEdit).Properties.ReadOnly;

  (Sender as TcxButtonEdit).Properties.ReadOnly := False;

  (Sender as TcxButtonEdit).Text:=aText;
  (Sender as TcxButtonEdit).PostEditValue;

  (Sender as TcxButtonEdit).Properties.ReadOnly := bReadOnly;
end;



//--------------------------------------------------------------
procedure cx_PostEditValue(Sender: TObject);
//--------------------------------------------------------------
var
  bReadOnly : boolean;
begin
  Assert(Sender is TcxButtonEdit);

//  bReadOnly:=(Sender as TcxButtonEdit).Properties.ReadOnly;

 // (Sender as TcxButtonEdit).Properties.ReadOnly := False;

 // (Sender as TcxButtonEdit).Text:=aText;
  (Sender as TcxButtonEdit).PostEditValue;

 // (Sender as TcxButtonEdit).Properties.ReadOnly := bReadOnly;
end;



//------------------------------------------------------
procedure cx_VerticalGrid_SaveToReg(aVerticalGrid: TcxVerticalGrid; aRegPath:
    string);
//------------------------------------------------------
var
  I: Integer;
  oReg: TRegIniFile;
  v: Variant;
  oEditRow: TcxEditorRow;
begin
  Assert(aRegPath<>'');

  aRegPath:=IncludeTrailingBackslash (aRegPath) + aVerticalGrid.Name;


  oReg:= TRegIniFile.Create(aRegPath);

  for I := 0 to aVerticalGrid.Rows.Count - 1 do
  begin
    if aVerticalGrid.Rows[i] is TcxEditorRow then
    begin
      try
        oEditRow:=TcxEditorRow(aVerticalGrid.Rows[i]);
        v:=oEditRow.Properties.Value;

        oReg.WriteInteger('', oEditRow.Name+'_TAG', oEditRow.Tag);

//        oEditRow.Properties.EditPropertiesClass.ClassName;

        //------------------------------------------------------
        if oEditRow.Properties.EditPropertiesClass = TcxLookupComboBoxProperties then
        begin
          oReg.WriteInteger('', oEditRow.Name, IIF(VarIsNull(v), False, v));
        end else

        //------------------------------------------------------
        if oEditRow.Properties.EditPropertiesClass = TcxCheckBoxProperties then
        begin
          oReg.WriteBool('', oEditRow.Name, IIF(VarIsNull(v), False, v));
        end else

        //------------------------------------------------------
        if oEditRow.Properties.EditPropertiesClass = TcxTextEditProperties then
        begin
          oReg.WriteString('', oEditRow.Name, VarToStr(v));
        end else

        //------------------------------------------------------
        if oEditRow.Properties.EditPropertiesClass = TcxButtonEditProperties then
        begin
          oReg.WriteString ('', oEditRow.Name,        VarToStr(v));
        end else

          oReg.WriteString('', oEditRow.Name, VarToStr(v));

      except
        raise Exception.Create('');
      end;
    end;
  end;

  oReg.Free;

end;

//------------------------------------------------------
procedure cx_VerticalGrid_LoadFromReg_(aVerticalGrid: TcxVerticalGrid;
    aRegPath: string);
//------------------------------------------------------
var
  I: Integer;
  oReg: TRegIniFile;
  v: Variant;
  oEditRow: TcxEditorRow;
begin
  Assert(aRegPath<>'');

  aRegPath:=IncludeTrailingBackslash (aRegPath) + aVerticalGrid.Name;

  oReg:= TRegIniFile.Create(aRegPath);

  for I := 0 to aVerticalGrid.Rows.Count - 1 do
  begin
    if aVerticalGrid.Rows[i] is TcxEditorRow then
    begin
      oEditRow:=TcxEditorRow(aVerticalGrid.Rows[i]);

      v:=oEditRow.Properties.Value;

      oEditRow.Tag:=oReg.ReadInteger('', oEditRow.Name+'_tag', 0);


      //------------------------------------------------------
      if oEditRow.Properties.EditPropertiesClass = TcxLookupComboBoxProperties then
      begin
        v:=oReg.ReadInteger('', oEditRow.Name, IIF(VarIsNull(v), False, v));
      end else

      //------------------------------------------------------
      if oEditRow.Properties.EditPropertiesClass = TcxCheckBoxProperties then
      begin
        v:=oReg.ReadBool('', oEditRow.Name, IIF(VarIsNull(v), False, v));
      end else

      //------------------------------------------------------
      if oEditRow.Properties.EditPropertiesClass = TcxTextEditProperties then
      begin
        v:=oReg.ReadString('', oEditRow.Name, VarToStr(v));
      end else

      //------------------------------------------------------
      if oEditRow.Properties.EditPropertiesClass = TcxButtonEditProperties then
      begin
        oEditRow.Tag:=oReg.ReadInteger('', oEditRow.Name+'_TAG', oEditRow.Tag);
        v :=oReg.ReadString ('', oEditRow.Name, VarToStr(v));

//         oReg.WriteString('', oEditRow.Name, VarToStr(v));
//        oReg.WriteInteger('', oEditRow.Name+'_TAG', oEditRow.Tag);
      end else
 //       raise Exception.Create('cx_VerticalGrid_SaveToReg(: '+ oEditRow.Properties.EditPropertiesClass.ClassName);

       v:=oReg.ReadString('', oEditRow.Name, VarToStr(v));

       oEditRow.Properties.Value:=v;
    end;

  end;

  oReg.Free;

end;

//----------------------------------------------
procedure cx_ColorRow(aSender: TcxEditorRow;
                      aRowArr: array of TcxEditorRow;
                      ACanvas: TcxCanvas;
                      aIsRequiredParam: boolean = false;
                      Color: TColor = clSilver );
//----------------------------------------------
var
  I: Integer;
begin
  for I := 0 to High(aRowArr) do
    if aSender = aRowArr[i] then
    begin
      if aRowArr[i].Properties.Options.Editing=False then
        ACanvas.Brush.Color:=clGrayText
      else

      if (aIsRequiredParam) then
        ACanvas.Brush.Color:=Color;
    end;
end;

//----------------------------------------------------
procedure cx_SetReadOnly(aRowArray: array of TcxEditorRow; aReadOnly: boolean = true);
//----------------------------------------------------
var
  I: Integer;
begin
  for I := 0 to High(aRowArray) do
    aRowArray[i].Properties.EditProperties.ReadOnly:= aReadOnly;
end;


procedure cx_DBVGrid_check(Sender: TcxDBVerticalGrid);
var
  I: Integer;
  oRow: TcxDBEditorRow;
begin
  exit;


  for I := 0 to Sender.Rows.Count - 1 do
  begin
    if Sender.Rows[i] is TcxDBEditorRow then
    begin
      oRow:=Sender.Rows[i] as TcxDBEditorRow;

      assert(oRow.Properties.DataBinding.FieldName<>'', 'procedure cx_DBVGrid_check(Sender: TcxDBVerticalGrid); oRow.Properties.DataBinding.FieldName '+oRow.Name);

//      if oRow.Properties.DataBinding.FieldName='' then
//        ShowMessage('procedure cx_DBVGrid_check(Sender: TcxDBVerticalGrid); oRow.Properties.DataBinding.FieldName '+oRow.Name);
    end;
  end;


end;


end.



{
  procedure cx_ShowColumns(aGrid: TcxGridDBTableView; aTag : Integer);
  begin

  end;


//1 test
/// dffffffffffffffffff
procedure cx_ShowColumnsWithTag(aGrid: TcxGridDBTableView; aTag : Integer);
var
  I: integer;
begin
  for I := 0 to aGrid.ColumnCount - 1 do
    if aGrid.Columns[i].Tag=0 then
      aGrid.Columns[i].Visible := True;

end;}

