unit u_cx_TreeList;

interface

{.$DEFINE test}


uses
  SysUtils, Variants, Classes, Dialogs,
  cxDBTL, cxControls, cxTLData, DB, Menus, ActnList, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, cxCustomData, cxMaskEdit,

  cxTL, cxInplaceContainer,

  u_files,

  u_func;


procedure cx_LoadFromReg_Expanded(aTreeList: TcxCustomTreeList; aColumn:
    TcxTreeListColumn; aFileName: string);

procedure cx_SaveToReg_Expanded(aTreeList: TcxCustomTreeList; aColumn:
    TcxTreeListColumn; aFileName: string);

procedure cx_Validate_Tree(aGrid: TcxDBTreeList);


implementation


const
  DEF_DELIMITER = '/';



//----------------------------------------------------------------------------
procedure cx_Validate_Tree(aGrid: TcxDBTreeList);
//----------------------------------------------------------------------------
var I,j: Integer;
  sFieldName: string;
//  oDataSet: TDataSet;

  oCol: TcxTreeListColumn;
begin
//  oDataSet:=aGrid.DataController.DataSet;

  for I := 0 to aGrid.ColumnCount-1 do
  begin
    oCol:=aGrid.Columns[i];


//    oCol.Properties.get

//
//
//    //------------------------------------------------------
//    if oCol.Properties.get  = TcxButtonEditProperties then
//    //------------------------------------------------------
//    begin
//      oProperties:=TcxCheckBoxProperties(oEditRow.Properties.EditProperties);
//      oProperties.UseAlignmentWhenInplace := True;
//
////          oReg.WriteInteger('', oEditRow.Name, IIF(VarIsNull(v), False, v));
//    end else


//
//    sFieldName:=aGrid.Columns[i].DataBinding.FieldName;
//
//    Assert(Assigned(aGrid.DataController.DataSet));


(*    aGrid.Columns[i].DataBinding.

    if (sFieldName='') or (not Assigned) then


      for j := 0 to High(aFieldNames) do
        if Eq(sFieldName, aFieldNames[j][0]) then
         aGrid.Columns[i].Caption := aFieldNames[j][1]
*)

  end;
end;




//-------------------------------------------------------------------
procedure cx_SaveToReg_Expanded(aTreeList: TcxCustomTreeList; aColumn:
    TcxTreeListColumn; aFileName: string);
//-------------------------------------------------------------------
var
  oStrList: TStringList;

  //---------------------------------------------
  procedure DoProcessExpanded(aNode: TcxTreeListNode; aPath: string);
  //---------------------------------------------
  var
    i: Integer;
    sPath: string;
  begin
    sPath:=aNode.Values[aColumn.ItemIndex];
    if aPath<>'' then
      sPath:=  aPath + DEF_DELIMITER + sPath ;

    oStrList.Add(sPath);

    for i:=0 to aNode.Count-1 do
     if aNode.Items[i].Expanded then
       DoProcessExpanded(aNode.Items[i],  sPath);
  end;
  //---------------------------------------------

var
  i: integer;

begin
  Assert(aFileName<>'');


  oStrList:=TStringList.Create;

  for i:=0 to aTreeList.Count-1 do
    if aTreeList.Items[i].Expanded then
      DoProcessExpanded(aTreeList.Items[i],'');

      
  ForceDirByFileName(aFileName);

  oStrList.SaveToFile(aFileName);

  {$IFDEF test}
  ShowMessage(oStrList.Text);

  {$ENDIF}


 // ShowMessage(oStrList.Text);

  FreeAndNil(oStrList);
end;



//-------------------------------------------------------------------
procedure cx_ExpandNodeByPath(aTreeList: TcxCustomTreeList;  aColumn:
    TcxTreeListColumn;  aStrArray: TStrArray);
//-------------------------------------------------------------------

  //---------------------------------------------
  function DoFindNode(aNode: TcxTreeListNode; aValue: string): TcxTreeListNode;
  //---------------------------------------------
  var
    i: Integer;
    sPath: string;
  begin
    if aNode<>nil then
      aNode.Expand(False);

    Result:=nil;

    if aNode=nil then
    begin
      for i:=0 to aTreeList.Count-1 do
       if Eq( aTreeList.Items[i].Values[aColumn.ItemIndex], aValue)  then
       begin
         Result:=aTreeList.Items[i];
         Exit;
       end
    end
    else
      for i:=0 to aNode.Count-1 do
       if Eq( aNode.Items[i].Values[aColumn.ItemIndex], aValue)  then
       begin
         Result:=aNode.Items[i];
         Exit;
       end;

  end;
  //---------------------------------------------


var
  i: Integer;
  sPath: string;
  oNode: TcxTreeListNode;

begin
  oNode:= nil;

  for I := 0 to High(aStrArray) do
  begin
    if i=0 then
      oNode:=DoFindNode(nil, aStrArray[i])
    else
      oNode:=DoFindNode(oNode, aStrArray[i]);

    if Assigned(oNode) then
      oNode.Expand(False)
    else
      Break;
  end;
end;


//-------------------------------------------------------------------
procedure cx_LoadFromReg_Expanded(aTreeList: TcxCustomTreeList; aColumn:
    TcxTreeListColumn; aFileName: string);
//-------------------------------------------------------------------
var
  i: integer;
  sName: string;
  oStrList: TStringList;
  s: string;
  strArray: TStrArray;
begin
  if not FileExists(aFileName) then
    Exit;

  Assert(FileExists(aFileName));

  aTreeList.BeginUpdate;

  oStrList:=TStringList.Create;
  oStrList.LoadFromFile(aFileName);


  {$IFDEF test}
  ShowMessage(oStrList.Text);

  {$ENDIF}



  for i:=0 to oStrList.Count -1 do
  begin
  //  s:='dasdasd asdasdas asdasdfs asdf3453 ';
  //  strArray:= StringToStrArray_new(s, ';');


    strArray:= StringToStrArray_new(oStrList[i], DEF_DELIMITER);

    cx_ExpandNodeByPath(aTreeList, aColumn, strArray);
  end;

  FreeAndNil(oStrList);

  aTreeList.EndUpdate;

end;



end.
