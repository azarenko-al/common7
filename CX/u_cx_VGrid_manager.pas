unit u_cx_VGrid_manager;

interface
uses
   cxDropDownEdit, cxVGrid
  , Classes;


type
  TcxVGridManager = class(TObject)
  private
    cxVerticalGrid1: TcxVerticalGrid;
  public

    procedure Add;

    function AddCategory(aParentRow: TcxCustomRow; aCaption: string): TcxCustomRow;

    function AddRow(aCaption: string): TcxEditorRow; overload;
    function AddRow(aParentRow: TcxCustomRow; aCaption: string): TcxEditorRow; overload;

    function AddComboBox(aCaption: string): TcxEditorRow;
    procedure AssignComboBoxItems(aRow: TcxEditorRow; aItems: tstrings);

    procedure ClearRows;
    procedure Init(acxVerticalGrid: TcxVerticalGrid);

  end;


implementation



// ---------------------------------------------------------------
function TcxVGridManager.AddCategory(aParentRow: TcxCustomRow; aCaption: string): TcxCustomRow;
// ---------------------------------------------------------------
begin
  if Assigned(aParentRow) then
    Result:=cxVerticalGrid1.AddChild(aParentRow, TcxCategoryRow)
  else
    Result:=cxVerticalGrid1.Add(TcxCategoryRow);

  (Result as TcxCategoryRow).Properties.Caption:=aCaption;

end;

function TcxVGridManager.AddRow(aCaption: string): TcxEditorRow;
begin
  Result := AddRow(nil, aCaption);
end;

// ---------------------------------------------------------------
function TcxVGridManager.AddRow(aParentRow: TcxCustomRow; aCaption: string): TcxEditorRow;
// ---------------------------------------------------------------
var
  oEditRow: TcxEditorRow;
begin
  if Assigned(aParentRow) then
     Result:=cxVerticalGrid1.AddChild(aParentRow, TcxEditorRow) as TcxEditorRow
  else
     Result:=cxVerticalGrid1.Add(TcxEditorRow) as TcxEditorRow;


  oEditRow:=Result as TcxEditorRow;

  oEditRow.Properties.Caption               :=aCaption;// + ' / '+ aFieldInfo.FieldName;
//  oEditRow.Properties.DataBinding.FieldName :=aFieldName;


//  oEditRow:=Result as TcxEditorRow;


end;

(*
  Category


  if bIsCategory then
    oClass:=TcxCategoryRow
  else
    oClass:=TcxDBEditorRow;


  if Assigned(aParentRow) then
     Result:=cxVerticalGrid1.AddChild(aParentRow, oClass)
  else
     Result:=cxVerticalGrid1.Add(oClass);
*)



procedure TcxVGridManager.Add;
(*var
  I: integer;
  sNewFieldName: string;
  bIsCategory: Boolean;
  oEditRow: TcxDBEditorRow;

  oClass: TcxCustomRowClass;
  oButton: TcxEditButton;
*)
begin

end;


procedure TcxVGridManager.AssignComboBoxItems(aRow: TcxEditorRow; aItems: tstrings);
begin
  TcxComboBoxProperties(aRow.Properties.EditProperties).Items.Assign (aItems);

 // cxVerticalGrid1.ClearRows;

//  cxVerticalGrid1: TcxDBVerticalGrid;
end;


// ---------------------------------------------------------------
function TcxVGridManager.AddComboBox(aCaption: string): TcxEditorRow;
// ---------------------------------------------------------------
var
  oEditRow: TcxEditorRow;
begin
  Result := AddRow(nil, aCaption) as TcxEditorRow;

  oEditRow:=Result;// as TcxEditorRow;
  oEditRow.Properties.EditPropertiesClass:=TcxComboBoxProperties;

  with TcxComboBoxProperties(oEditRow.Properties.EditProperties) do
  begin
  //  OnCloseUp     := DoOnPickCloseUp_CX;
    UseMouseWheel := False;
    DropDownRows  :=20;
    DropDownListStyle:=lsFixedList;
  end;

end;

procedure TcxVGridManager.ClearRows;
begin
  cxVerticalGrid1.ClearRows;

//  cxVerticalGrid1: TcxDBVerticalGrid;
end;

procedure TcxVGridManager.Init(acxVerticalGrid: TcxVerticalGrid);
begin
  cxVerticalGrid1 :=acxVerticalGrid;
end;


end.
