

 unit u_cx;

interface

uses
   Windows,SysUtils,Forms ,Classes, Controls,
   cxControls, cxInplaceContainer, cxTL,   cxGridStrs,cxFilterConsts, cxFilterControlStrs,
   DB,   cxPropertiesStore,  cxGridDBBandedTableView,   cxGridLevel, cxClasses,  cxGridCustomView,
   cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,

cxFilter, 

cxImageComboBox,

  Variants,
   cxGridExportLink,

   IniFiles, Dialogs,

//   cxExportGrid4Link,

   u_func,
   u_DB,

   cxDBTL;


  procedure cx_SetGridReadOnly (aGrid: TcxGridDBBandedTableView; aValue: boolean); overload;
  procedure cx_SetGridReadOnly (aGrid: TcxGridDBTableView; aValue: boolean);       overload;

  procedure cx_AddComponentProp(aPropertiesStore: TcxPropertiesStore; aComponent: TComponent; aFieldName: string);

  procedure cx_UpdateSelectedItems(aGrid: TcxGridDBBandedTableView;   aKeyFieldName, aFieldName: string; aValue: Variant); overload;

  procedure cx_UpdateSelectedItems(aGrid: TcxGridDBTableView;  aKeyFieldName, aFieldName: string; aValue: Variant);overload;
                                                             
  function cx_IsCursorInGrid(Sender: TObject): Boolean;

  procedure cx_Post(Sender: TcxGridDBTableView);


  procedure cx_UpdateSelectedItems_Table(aGrid: TcxGridDBTableView; aFieldName: string;   aValue: Variant);

  procedure cx_UpdateSelectedItems_BandedTable(aGrid: TcxGridDBBandedTableView; aFieldName:   string; aValue: Variant);

  procedure cx_GetCheckedItemsToStrList(aGrid: TcxGridDBBandedTableView;
      aOutStrList: TStrings; aKeyFieldName: string = 'id'; aCheckFieldName:   string = 'checked'); overload;

  procedure cx_GetCheckedItemsToStrList(aGrid: TcxGridDBTableView; aOutStrList:
      TStringList; aKeyFieldName: string = 'id'; aCheckFieldName: string =  'checked'); overload;

  function cx_CheckCursorInGrid(Sender: TcxGrid; aGridView: TcxCustomGridView): Boolean;

  procedure cx_GetSelectedList(Sender: TcxDBTreeList; aKeyColumn:   TcxDBTreeListColumn; aList: TList); overload;

  procedure cx_GetSelectedList_(Sender: TcxGridDBBandedTableView; aList: TList); overload;
  procedure cx_GetSelectedList_TableView(Sender: TcxGridDBTableView; aList: TList); overload;


//procedure cx_DBTreeList_Customizing(cxDBTreeList: TcxDBTreeList);


  procedure cx_CheckDataFields(aGrid: TcxGridDBBandedTableView); overload;
  procedure cx_CheckDataFields(aGrid: TcxGridDBTableView); overload;
  procedure cx_CheckDataFields(aTree: TcxDBTreeList); overload;


procedure cx_SetColumnCaptions(aGrid: TcxGridDBTableView; aFieldNames : array
    of string);  overload;

procedure cx_SetColumnCaptions(aGrid: TcxGridDBBandedTableView; aFieldNames : array
    of string);   overload;



//procedure cx_SetColumnCaptions(aGrid: TcxGridDBTableView; aFieldNames : array
//    of Variant); overload;

procedure cx_SetColumnCaptions(aGrid: TcxDBTreeList; aFieldNames : array of
    string); overload;

//procedure cx_SetColumnCaptions(aGrid: TcxGridDBBandedTableView; aFieldNames :
//    array of Variant); overload;

procedure cxGrid_RemoveUnusedColumns(aView: TcxGridDBBandedTableView);

procedure cxGrid_RemoveUnusedColumns_DBTableView(aView: TcxGridDBTableView);

function cx_SetColumnReadOnly(aColumns: array of TcxDBTreeListColumn; aReadOnly
    : boolean): TcxDBTreeListColumn;

procedure cx_ExportGridToExcel(aFileName: string; aGrid: TcxGrid);

function cx_FindNodeByText(aTreeList1: TcxTreeList; aColumn: TcxTreeListColumn; aText: string):
    TcxTreeListNode;

procedure cx_SaveCheckedItemsToIni(aGrid: TcxGridDBTableView; aIniFileName,
    aSection: String);

procedure cx_LoadCheckedItemsFromIni(aGrid: TcxGridDBTableView; aIniFileName,
    aSection: String);

procedure cx_ExportToExcel(aView: TcxGrid);

procedure cx_Search(aGrid: TcxGridTableView; aText: string);




    //procedure cx_Validate_TcxDBTreeList(aGrid: TcxDBTreeList);


 // procedure cx_ShowColumns(aGrid: TcxGridDBTableView; aTag : Integer);


procedure cx_ImgComboBox_Add(aCol: TcxGridColumn; aImgIndex: integer;
          aValue: variant; const aDescription: string);


procedure cx_Init(aGrid: TcxDBTreeList); overload;
procedure cx_Init(aGrid: TcxGrid); overload;

function cx_GetCheckedCount(aGrid: TcxGridDBBandedTableView; aFieldName:
    string): Integer;



//========================================================================
implementation

uses cxGridBandedTableView;

//========================================================================


//--------------------------------------------------------------------------
procedure cx_ImgComboBox_Add(aCol: TcxGridColumn; aImgIndex: integer;
          aValue: variant; const aDescription: string);
//------------------------------------------------------------------------------
var oItem: TcxImageComboBoxItem;
begin
  oItem:= TcxImageComboBoxProperties(aCol.Properties).Items.Add();

  if aImgIndex >= 0 then oItem.ImageIndex:= aImgIndex;
  if aValue <> null then oItem.Value:= aValue;
  if aDescription <> '' then oItem.Description:= aDescription;
end;



function ReplaceStr (Value, OldPattern, NewPattern: string): string;
begin
  Result:=StringReplace (Value, OldPattern, NewPattern, [rfReplaceAll, rfIgnoreCase]);
end;

// ---------------------------------------------------------------
function cx_SetColumnReadOnly(aColumns: array of TcxDBTreeListColumn; aReadOnly
    : boolean): TcxDBTreeListColumn;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  for I := 0 to High(aColumns) - 1 do
   aColumns[i].Options.Editing := not aReadOnly;
end;

//
//
//procedure cx_DBTreeList_Customizing(cxDBTreeList: TcxDBTreeList);
//begin
//  raise Exception.Create('');
////  cxDBTreeList.Customizing.MakeColumnSheetVisible;
//
//end;



//--------------------------------------------------------------------
function cx_FindNodeByText(aTreeList1: TcxTreeList; aColumn: TcxTreeListColumn; aText: string):
    TcxTreeListNode;
//--------------------------------------------------------------------
var
  I: Integer;
  s: string;
begin

  Result :=nil;

  for i:=0 to aTreeList1.AbsoluteCount-1 Do
  begin
    s:= VarToStr (aTreeList1.AbsoluteItems[i].Values[aColumn.ItemIndex]);
//    s:= VarToString( aTreeList1.AbsoluteItems[i].Values[aColumn.ItemIndex]);

//    cxTreeList1.Items[i].Count

    if Eq(s, aText) then
    begin
      Result := aTreeList1.AbsoluteItems[i];
      Break;
    end;
  end;

end;


//-------------------------------------------------------------------
procedure cx_UpdateSelectedItems(aGrid: TcxGridDBBandedTableView;
    aKeyFieldName, aFieldName: string; aValue: Variant);
//-------------------------------------------------------------------
// KeyField for BEELINE TN  - hopsname
// -------------------------------------------------------------------
var i: integer;
  v: Variant;
  s: string;
  oStrList: TStringLIst;
  oDataset: TDataset;
  iKeyColumnInd: Integer;
begin
  oStrList:=TStringList.Create;

  Assert(Assigned(aGrid.GetColumnByFieldName(aKeyFieldName)));
  Assert(Assigned(aGrid.GetColumnByFieldName(aFieldName)));

  iKeyColumnInd  :=aGrid.GetColumnByFieldName(aKeyFieldName).Index;

  Assert(Assigned(aGrid.DataController.DataSet));

  oDataset:=aGrid.DataController.DataSet;
  oDataSet.DisableControls;

  with aGrid.Controller do
    for i := 0 to SelectedRowCount - 1 do
    begin
      v:=SelectedRows[i].Values[iKeyColumnInd];

      try
        s:=SelectedRows[i].Values[iKeyColumnInd];
      except

      end;

      oStrList.Add (s);
    end;


   with aGrid do
     for i := 0 to oStrList.Count - 1 do
      if oDataset.Locate (aKeyFieldName, oStrList[i], []) then
        db_UpdateRecord (oDataset, aFieldName, aValue);

  oStrList.Free;

  oDataSet.EnableControls;

end;




//-------------------------------------------------------------------
procedure cx_GetCheckedItemsToStrList(aGrid: TcxGridDBTableView; aOutStrList:
    TStringList; aKeyFieldName: string = 'id'; aCheckFieldName: string =
    'checked');
//-------------------------------------------------------------------
//  aKeyFieldName - string
//-------------------------------------------------------------------
var i: integer;

  bChecked : boolean;
  sKey : string;

  iKeyFieldInd,iValueFieldInd: integer;
  iCheckColumnInd: integer;

  iKeyColumnInd: Integer;
  iCheckFieldInd: Integer;

  oColumn : TcxGridDBColumn;
  s: string;

begin
  aOutStrList.Clear;


  Assert(aKeyFieldName<>'');
  Assert(aCheckFieldName<>'');


  s:=aGrid.DataController.KeyFieldNames;


  oColumn:=aGrid.GetColumnByFieldName(aKeyFieldName);


  Assert(Assigned(aOutStrList));
  Assert(Assigned(aGrid.GetColumnByFieldName(aKeyFieldName)));
  Assert(Assigned(aGrid.GetColumnByFieldName(aCheckFieldName)));

  iKeyColumnInd  :=aGrid.GetColumnByFieldName(aKeyFieldName).Index;
  iCheckFieldInd :=aGrid.GetColumnByFieldName(aCheckFieldName).Index;

  i:=aGrid.DataController.RecordCount;

  with aGrid.DataController do
    for i := 0 to RecordCount - 1 do
    begin
      bChecked:=Values[i, iCheckFieldInd];
      sKey:=Values[i, iKeyColumnInd];

      if bChecked then
        aOutStrList.Add(sKey);
    end;

end;

//-------------------------------------------------------------------
procedure cx_GetCheckedItemsToStrList(aGrid: TcxGridDBBandedTableView;
    aOutStrList: TStrings; aKeyFieldName: string = 'id'; aCheckFieldName:
    string = 'checked');
//-------------------------------------------------------------------
//  aKeyFieldName - string
//-------------------------------------------------------------------
var i: integer;
 // v: Variant;

  bChecked : boolean;
  sKey : string;

  iKeyFieldInd,iValueFieldInd: integer;
  iCheckColumnInd: integer;

  iKeyColumnInd: Integer;
  iCheckFieldInd: Integer;
  v: Variant;
begin
  aOutStrList.Clear;


  Assert(aKeyFieldName<>'');
  Assert(aCheckFieldName<>'');

  Assert(Assigned(aOutStrList));
  Assert(Assigned(aGrid.GetColumnByFieldName(aKeyFieldName)));
  Assert(Assigned(aGrid.GetColumnByFieldName(aCheckFieldName)));

  iKeyColumnInd  :=aGrid.GetColumnByFieldName(aKeyFieldName).Index;
  iCheckFieldInd :=aGrid.GetColumnByFieldName(aCheckFieldName).Index;

  i:=aGrid.DataController.RecordCount;

  with aGrid.DataController do
    for i := 0 to RecordCount - 1 do
    begin
      v:=Values[i, iCheckFieldInd];
      bChecked:=AsBoolean(v);

      if bChecked then
      begin
        v:=Values[i, iKeyColumnInd];
        sKey    :=AsString(v);

        aOutStrList.Add(sKey);
      end;
    end;

end;


// ---------------------------------------------------------------
procedure cx_SetColumnCaptions(aGrid: TcxDBTreeList; aFieldNames : array of
    string);
// ---------------------------------------------------------------
var I,j: Integer;
  sFieldName: string;

  oColumn: TcxDBTreeListColumn;

begin
  Assert(Length(aFieldNames) mod 2 = 0);


  for I := 0 to aGrid.ColumnCount-1 do
  begin
    oColumn := aGrid.Columns[i] as TcxDBTreeListColumn;

    sFieldName:=oColumn.DataBinding.FieldName ;

    if sFieldName<>'' then
      for j := 0 to (Length(aFieldNames) div 2)-1 do
        if Eq(sFieldName, aFieldNames[j*2]) then
          aGrid.Columns[i].Caption.Text := aFieldNames[j*2+1]
  end;
end;

//----------------------------------------------------------------------------
procedure cx_SetColumnCaptions(aGrid: TcxGridDBTableView; aFieldNames : array
    of string);
//----------------------------------------------------------------------------
var I,j: Integer;
  sFieldName: string;
begin
 Assert(Length(aFieldNames) mod 2 = 0);

  for I := 0 to aGrid.ColumnCount-1 do
  begin
    sFieldName:=aGrid.Columns[i].DataBinding.FieldName;

    if sFieldName<>'' then
      for j := 0 to (Length(aFieldNames) div 2)-1 do
        if Eq(sFieldName, aFieldNames[j*2]) then
         aGrid.Columns[i].Caption := aFieldNames[j*2+1]
  end;
end;



//----------------------------------------------------------------------------
procedure cx_SetColumnCaptions(aGrid: TcxGridDBBandedTableView; aFieldNames : array
    of string);
//----------------------------------------------------------------------------
var I,j: Integer;
  sFieldName: string;
begin
 Assert(Length(aFieldNames) mod 2 = 0);

  for I := 0 to aGrid.ColumnCount-1 do
  begin
    sFieldName:=aGrid.Columns[i].DataBinding.FieldName;

    if sFieldName<>'' then
      for j := 0 to (Length(aFieldNames) div 2)-1 do
        if Eq(sFieldName, aFieldNames[j*2]) then
         aGrid.Columns[i].Caption := aFieldNames[j*2+1]
  end;
end;



//-------------------------------------------------------------------
procedure cx_UpdateSelectedItems(aGrid: TcxGridDBTableView;
    aKeyFieldName, aFieldName: string; aValue: Variant);
//-------------------------------------------------------------------
// KeyField for BEELINE TN  - hopsname
// -------------------------------------------------------------------
var i: integer;
  v: Variant;
  s: string;
  iKeyFieldInd: integer;
  iCheckColumnInd: integer;
  oStrList: TStringLIst;
  oDataset: TDataset;
  iKeyColumnInd: Integer;
  iRecIx: integer;
     r:  TcxCustomGridRecord;
begin
  oStrList:=TStringList.Create;

  Assert(Assigned(aGrid.GetColumnByFieldName(aKeyFieldName)));
  Assert(Assigned(aGrid.GetColumnByFieldName(aFieldName)));

  iKeyColumnInd  :=aGrid.GetColumnByFieldName(aKeyFieldName).Index;
  Assert(iKeyColumnInd>=0, 'Value <0');

  Assert(Assigned(aGrid.DataController.DataSet));

  oDataset:=aGrid.DataController.DataSet;
  oDataSet.DisableControls;

  aGrid.DataController.SelectAll;


  for i := 0 to aGrid.Controller.SelectedRecordCount - 1 do
  begin
    r:=aGrid.Controller.SelectedRecords[I];

    Assert(Assigned(r), 'Value not assigned');
(*
    if not Assigned(r) then
       ShowMessage('');
*)
    Assert(Assigned(aGrid.Controller.SelectedRecords[I]), 'Value not assigned');

    iRecIx := aGrid.Controller.SelectedRecords[I].RecordIndex;

    oStrList.Add (s);
  end;


  for i := 0 to oStrList.Count - 1 do
  begin
    Assert(oStrList[i]<>'');

    if oDataset.Locate (aKeyFieldName, oStrList[i], []) then
       db_UpdateRecord (oDataset, aFieldName, aValue);
  end;

  oStrList.Free;

  oDataSet.EnableControls;

end;



procedure cx_AddComponentProp(aPropertiesStore: TcxPropertiesStore; aComponent: TComponent; aFieldName: string);
var
  oStoreComponent: TcxPropertiesStoreComponent;
begin
  oStoreComponent :=  TcxPropertiesStoreComponent(aPropertiesStore.Components.Add);

  oStoreComponent.Component:=aComponent;
  oStoreComponent.Properties.Add(aFieldName);
end;



//-------------------------------------------------------------------
function cx_IsCursorInGrid(Sender: TObject): Boolean;
//-------------------------------------------------------------------
var
  lHitTest: TcxCustomGridHitTest;
  lPt: TPoint;

  oList: TcxDBTreeList;
  oNode: TcxTreeListNode;

begin
  Result := False;


  if (Sender is TcxDBTreeList) then
  begin
     oList:=TcxDBTreeList(Sender);

//     oList.scr

    lPt := oList.ScreenToClient(Mouse.CursorPos);
  //  lHitTest := oList.ViewInfo.GetHitTest(lPt);


    oNode := oList.FocusedNode;

    if oNode <> oList.HitTest.HitNode then
      Exit;


    Result := True;

   {

Node := cxInvoices.FocusedNode;
 cxInvoices.HitTest.ReCalculate(GetMouseCursorPos);
 if Node <> cxInvoices.HitTest.HitNode then exit;

   }


  end ;



  if (Sender is TcxGridSite) then
  begin
    lPt := TcxGridSite(Sender).ScreenToClient(Mouse.CursorPos);
    lHitTest := TcxGridSite(Sender).ViewInfo.GetHitTest(lPt);


    if (lHitTest.HitTestCode = htCell) and
      (lHitTest is TcxGridRecordCellHitTest) and
      Assigned(TcxGridRecordCellHitTest(lHitTest).GridRecord) and
      TcxGridRecordCellHitTest(lHitTest).GridRecord.IsData then
    begin
      Result := True;
    end;
  end

 // else
  //  Assert(1<>1, 'function cx_IsCursorInGrid(Sender: TObject): Boolean;');




end;

(*
function cx_SetColumnReadOnly(aColumns: array of TcxDBTreeListColumn; aReadOnly
    : boolean): TcxDBTreeListColumn;
var
  I: Integer;
begin
  for I := 0 to High(aColumns) - 1 do
    aColumns[i].Properties.ReadOnly := aReadOnly;
end;
*)


procedure cx_Post(Sender: TcxGridDBTableView);
begin
  if assigned(Sender.DataController.DataSource.DataSet) then
    with Sender.DataController.DataSource.DataSet do
      if State in [dsEdit,dsInsert] then Post;
end;



//------------------------------------------------------------------------------
procedure cx_LocalizeRus();
//------------------------------------------------------------------------------
begin
  cxSetResourceString(@scxGridGroupByBoxCaption, '���������� ���� ��������� ������� ��� ����������� �� ����...');
  cxSetResourceString(@scxGridFilterIsEmpty    , '<������ �� �����>');
  cxSetResourceString(@scxGridFilterCustomizeButtonCaption, '�������������...');
  cxSetResourceString(@scxGridDeletingSelectedConfirmationText, '������� ��� ���������� ������ ?');
  cxSetResourceString(@scxGridDeletingFocusedConfirmationText, '������� ��������� ������ ?');
//  cxSetResourceString(@scxGridDeletingConfirmationCaption, '�����������...');

  cxSetResourceString(@cxSFilterOperatorEqual       ,  '�����'                );
  cxSetResourceString(@cxSFilterOperatorNotEqual    ,  '�� �����'             );
  cxSetResourceString(@cxSFilterOperatorLess        ,  '������'               );
  cxSetResourceString(@cxSFilterOperatorLessEqual   ,  '������ ��� �����'     );
  cxSetResourceString(@cxSFilterOperatorGreater     ,  '������'               );
  cxSetResourceString(@cxSFilterOperatorGreaterEqual,  '������ ��� �����'     );
  cxSetResourceString(@cxSFilterOperatorLike        ,  '������'               );
  cxSetResourceString(@cxSFilterOperatorNotLike     ,  '�� ������'            );
  cxSetResourceString(@cxSFilterOperatorBetween     ,  '�����'                );
  cxSetResourceString(@cxSFilterOperatorNotBetween  ,  '�� �����'             );
  cxSetResourceString(@cxSFilterOperatorInList      ,  '��'                   );
  cxSetResourceString(@cxSFilterOperatorNotInList   ,  '�� ��'                );


  cxSetResourceString(@cxSFilterOperatorYesterday, '�����');
  cxSetResourceString(@cxSFilterOperatorToday, '�������');
  cxSetResourceString(@cxSFilterOperatorTomorrow, '������');

  cxSetResourceString(@cxSFilterOperatorLastWeek, '��������� ������');
  cxSetResourceString(@cxSFilterOperatorLastMonth, '��������� �����');
  cxSetResourceString(@cxSFilterOperatorLastYear, '��������� ���');

  cxSetResourceString(@cxSFilterOperatorThisWeek, '��� ������');
  cxSetResourceString(@cxSFilterOperatorThisMonth, '���� �����');
  cxSetResourceString(@cxSFilterOperatorThisYear, '���� ���');

  cxSetResourceString(@cxSFilterOperatorNextWeek, '��������� ������');
  cxSetResourceString(@cxSFilterOperatorNextMonth, '��������� �����');
  cxSetResourceString(@cxSFilterOperatorNextYear, '��������� ���');


  cxSetResourceString(@cxSFilterOperatorIsNull, '�����');
  cxSetResourceString(@cxSFilterOperatorIsNotNull, '�� �����');
  cxSetResourceString(@cxSFilterOperatorBeginsWith, '���������� �');
  cxSetResourceString(@cxSFilterOperatorDoesNotBeginWith, '�� ���������� �');
  cxSetResourceString(@cxSFilterOperatorEndsWith, '��������� ��');
  cxSetResourceString(@cxSFilterOperatorDoesNotEndWith, '�� ��������� ��');
  cxSetResourceString(@cxSFilterOperatorContains, '��������');
  cxSetResourceString(@cxSFilterOperatorDoesNotContain, '�� ��������');
  cxSetResourceString(@cxSFilterBoxAllCaption, '(���)');
  cxSetResourceString(@cxSFilterBoxCustomCaption, '(���������...)');
  cxSetResourceString(@cxSFilterBoxBlanksCaption, '(������)');
  cxSetResourceString(@cxSFilterBoxNonBlanksCaption, '(��������)');


  cxSetResourceString(@cxSFilterBoolOperatorAnd, '�');  // all
  cxSetResourceString(@cxSFilterBoolOperatorOr, '���');
  cxSetResourceString(@cxSFilterBoolOperatorNotAnd, '� ��'); // not all
  cxSetResourceString(@cxSFilterBoolOperatorNotOr, '��� ��');
  cxSetResourceString(@cxSFilterRootButtonCaption, '������');
  cxSetResourceString(@cxSFilterAddCondition, '�������� �������');
  cxSetResourceString(@cxSFilterAddGroup, '�������� ������');
  cxSetResourceString(@cxSFilterRemoveRow, '������� �������');
  cxSetResourceString(@cxSFilterClearAll, '��������');
  cxSetResourceString(@cxSFilterFooterAddCondition, '������� ������ ����� �������� �������');

  cxSetResourceString(@cxSFilterGroupCaption, '����������� � ��������');
  cxSetResourceString(@cxSFilterRootGroupCaption, '<������>');
  cxSetResourceString(@cxSFilterControlNullString, '<���>');

  cxSetResourceString(@cxSFilterErrorBuilding, 'Can''t build filter from source');

  //FilterDialog
  cxSetResourceString(@cxSFilterDialogCaption, '��������� �������');
  cxSetResourceString(@cxSFilterDialogInvalidValue, '�������� ��������');
  cxSetResourceString(@cxSFilterDialogUse, '������������');
  cxSetResourceString(@cxSFilterDialogSingleCharacter, '��� ������ �������');
  cxSetResourceString(@cxSFilterDialogCharactersSeries, '��� ������ ��������');
  cxSetResourceString(@cxSFilterDialogOperationAnd, '�');
  cxSetResourceString(@cxSFilterDialogOperationOr, '���');
  cxSetResourceString(@cxSFilterDialogRows, '���������� ������, ��� �������:');

  // FilterControlDialog
  cxSetResourceString(@cxSFilterControlDialogCaption, '�������� �������');
  cxSetResourceString(@cxSFilterControlDialogNewFile, '��������.flt');
  cxSetResourceString(@cxSFilterControlDialogOpenDialogCaption, '�������� �������');
  cxSetResourceString(@cxSFilterControlDialogSaveDialogCaption, '���������� �������');
  cxSetResourceString(@cxSFilterControlDialogActionSaveCaption, '���������...');
  cxSetResourceString(@cxSFilterControlDialogActionOpenCaption, '�������...');
  cxSetResourceString(@cxSFilterControlDialogActionApplyCaption, '���������');
  cxSetResourceString(@cxSFilterControlDialogActionOkCaption, '��');
  cxSetResourceString(@cxSFilterControlDialogActionCancelCaption, '������');
  cxSetResourceString(@cxSFilterControlDialogFileExt, 'flt');
  cxSetResourceString(@cxSFilterControlDialogFileFilter, '����� �������� (*.flt)|*.flt');

end;



// ---------------------------------------------------------------
procedure cx_GetSelectedList(Sender: TcxDBTreeList; aKeyColumn:
    TcxDBTreeListColumn; aList: TList);
// ---------------------------------------------------------------
var
  i: Integer;
  iID: Integer;
begin
  aList.Clear;

 // oList :=TList.Create;

  with Sender do
    for i := 0 to SelectionCount - 1 do
    begin
      iID:=Selections[i].Values[aKeyColumn.ItemIndex];
      aList.Add(Pointer(iID));
    end;
end;


// ---------------------------------------------------------------
procedure cx_GetSelectedList_(Sender: TcxGridDBBandedTableView; aList: TList);
// ---------------------------------------------------------------
var
  i: Integer;
  iID: Integer;

  oColumn: TcxGridDBBandedColumn;

begin
  aList.Clear;

  Assert(Sender.DataController.KeyFieldNames<>'');

  oColumn:=Sender.GetColumnByFieldName(Sender.DataController.KeyFieldNames);

  Assert(Assigned(oColumn));


 // oList :=TList.Create;

//   Sender.Controller.SelectedRows

  with Sender.Controller do
    for i := 0 to SelectedRowCount - 1 do
    begin
      iID:=SelectedRows[i].Values[oColumn.Index];
      aList.Add(Pointer(iID));
    end;
end;


// ---------------------------------------------------------------
procedure cx_GetSelectedList_TableView(Sender: TcxGridDBTableView; aList:
    TList);
// ---------------------------------------------------------------
var
  i: Integer;
  iID: Integer;

  oColumn: TcxGridDBColumn;

begin
  aList.Clear;

  Assert(Sender.DataController.KeyFieldNames<>'');

  oColumn:=Sender.GetColumnByFieldName(Sender.DataController.KeyFieldNames);

  Assert(Assigned(oColumn));


 // oList :=TList.Create;

//   Sender.Controller.SelectedRows

  with Sender.Controller do
    for i := 0 to SelectedRowCount - 1 do
    begin
      iID:=SelectedRows[i].Values[oColumn.Index];
      aList.Add(Pointer(iID));
    end;
end;





// ---------------------------------------------------------------
function cx_CheckCursorInGrid(Sender: TcxGrid; aGridView: TcxCustomGridView): Boolean;
// ---------------------------------------------------------------
Var
  P: TPoint;
  a: TcxCustomGridView;
  h: TcxCustomGridHitTest;
//  i: integer;
begin
  P := Sender.ScreenToClient(Mouse.CursorPos);

  h:=aGridView.GetHitTest(P);

//  i:=h.HitTestCode;

  Result:=h.HitTestCode = htRecord or htCell;
end;

// ---------------------------------------------------------------
procedure cxGrid_RemoveUnusedColumns_DBTableView(aView: TcxGridDBTableView);
// ---------------------------------------------------------------
var
  I: Integer;
  s: string;
begin
  for I := aView.ColumnCount - 1  downto 0 do
  begin
    s:=aView.Columns[i].Caption;

    if aView.Columns[i].Caption='' then
      aView.Columns[i].Free;
  end;
end;

// ---------------------------------------------------------------
procedure cx_ExportGridToExcel(aFileName: string; aGrid: TcxGrid);
// ---------------------------------------------------------------
begin
//  raise Exception.Create('');

  cxGridExportLink.ExportGridToExcel (aFileName, aGrid);

  //procedure ExportGridToExcel(const AFileName: string; AGrid: TcxGrid; AExpand: Boolean = True; ASaveAll: Boolean = True; AUseNativeFormat: Boolean = True; const AFileExt: string = �xls�);

//  ExportGrid4ToExcel(aFileName, aGrid);
end;


// ---------------------------------------------------------------
procedure cxGrid_RemoveUnusedColumns(aView: TcxGridDBBandedTableView);
// ---------------------------------------------------------------
var
  I: Integer;
  s: string;
begin
  for I := aView.ColumnCount - 1  downto 0 do
  begin
    s:=aView.Columns[i].Caption;

    if aView.Columns[i].Caption='' then
      aView.Columns[i].Free;
  end;

end;

// ---------------------------------------------------------------
procedure cx_CheckDataFields(aGrid: TcxGridDBBandedTableView);
// ---------------------------------------------------------------
var
  I: Integer;
  oDataSet: TDataSet;
  //s: string;
  sFieldName: string;
begin
  Assert(Assigned(aGrid.DataController.DataSource), 'aGrid.DataController.DataSource not assigned');

  oDataSet:=aGrid.DataController.DataSource.DataSet;

  Assert(Assigned(oDataSet));

 // i:=oDataSet.FieldCount;

//  if oDataSet.Active then

  for I := 0 to aGrid.ColumnCount - 1 do
  begin
    sFieldName:=aGrid.Columns[i].DataBinding.FieldName;

    Assert(sFieldName<>'');

    Assert(Assigned(oDataSet.FindField(sFieldName)), sFieldName);
  end;

 // if aGrid.DataController.DataSource then


  // TODO -cMM: cx_CheckDataFields default body inserted
end;


// ---------------------------------------------------------------
procedure cx_CheckDataFields(aTree: TcxDBTreeList);
// ---------------------------------------------------------------
var
  I: Integer;
  oDataSet: TDataSet;
  oDBTreeListColumn: TcxDBTreeListColumn;
  //s: string;
  sFieldName: string;
begin
  Assert(Assigned(aTree.DataController.DataSource), 'aTree.DataController.DataSource not assigned');

  oDataSet:=aTree.DataController.DataSource.DataSet;

  Assert(aTree.DataController.KeyField<>'');
  Assert(aTree.DataController.ParentField<>'');


  Assert(Assigned(oDataSet));

 // i:=oDataSet.FieldCount;

//  if oDataSet.Active then

  for I := 0 to aTree.ColumnCount - 1 do
  begin
    oDBTreeListColumn := TcxDBTreeListColumn(aTree.Columns[i]);


//    sFieldName:=aTree.Columns[i].DataBinding.FieldName;
    sFieldName:=oDBTreeListColumn.DataBinding.FieldName;

    Assert(sFieldName<>'');

    Assert(Assigned(oDataSet.FindField(sFieldName)), sFieldName);
  end;

 // if aTree.DataController.DataSource then


  // TODO -cMM: cx_CheckDataFields default body inserted
end;


//-------------------------------------------------------------------
procedure cx_SaveCheckedItemsToIni(aGrid: TcxGridDBTableView; aIniFileName,
    aSection: String);
//-------------------------------------------------------------------
//  aKeyFieldName - string
//-------------------------------------------------------------------
var
  i: integer;

  bChecked : boolean;
  sKey : string;

  iKeyFieldInd: integer;
  iCheckColumnInd: integer;

  iKey: Integer;
  sCheckFieldName: string;
  sKeyFieldName: string;
  v: Variant;

  oIni: TIniFile;
  oStrList: TStringList;
  s: string;

//const
 // DEF_SECTION = 'main';

begin

  ForceDirectories(ExtractFileDir(aIniFileName));


  oIni:=TIniFile.create (aIniFileName);
  oIni.EraseSection(aSection);

 // s:=ExtractFileDir(aIniFileName);



//  aOutStrList.Clear;

 // sKeyFieldName  :=FLD_ID;
  sCheckFieldName:=FLD_CHECKED;


  sKeyFieldName:=aGrid.DataController.KeyFieldNames;

  Assert(sKeyFieldName<>'');
//  Assert(aCheckFieldName<>'');

//  Assert(Assigned(aOutStrList));
  Assert(Assigned(aGrid.GetColumnByFieldName(sKeyFieldName)));
  Assert(Assigned(aGrid.GetColumnByFieldName(sCheckFieldName)));

  iKeyFieldInd  :=aGrid.GetColumnByFieldName(sKeyFieldName).Index;
  iCheckColumnInd :=aGrid.GetColumnByFieldName(sCheckFieldName).Index;

  i:=aGrid.DataController.RecordCount;

//  aGrid.DataController.DataSet.Fields


  with aGrid.DataController do
    for i := 0 to RecordCount - 1 do
    begin

      iKey:=Values[i, iKeyFieldInd];
      v   :=Values[i, iCheckColumnInd];

      bChecked:=AsBoolean(v);

      if bChecked then
        oIni.WriteBool(aSection, IntToStr(iKey), True);

    end;


//
{
  oStrList:=TStringList.create;

  oIni.ReadSection(DEF_SECTION, oStrList);

  ShowMessage(oStrList.Text);


  FreeAndNil (oStrList);
}

  FreeAndNil (oIni);

end;


//-------------------------------------------------------------------
procedure cx_LoadCheckedItemsFromIni(aGrid: TcxGridDBTableView; aIniFileName,
    aSection: String);
//-------------------------------------------------------------------
//  aKeyFieldName - string
//-------------------------------------------------------------------
var
  b: Boolean;
  i: integer;

  bChecked : boolean;
  sKey : string;

//  iKeyFieldInd: integer;
 // iCheckColumnInd: integer;

 // iKey: Integer;
  oDataSet: TDataset;
  sCheckFieldName: string;
//  sKeyFieldName: string;
 // v: Variant;

  oIni: TIniFile;
  oStrList: TStringList;
  s: string;

//const
 // DEF_SECTION = 'main';

begin
//  Assert  aGrid.DataController.DataSource

  oDataSet:=aGrid.DataController.DataSet;

  Assert (Assigned (oDataSet));


  if aGrid.DataController.RecordCount=0 then
    exit;


  oDataSet.DisableControls;


 // ForceDirectories(ExtractFileDir(aIniFileName));


  oStrList:=TStringList.create;

  oIni:=TIniFile.create (aIniFileName);


  oIni.ReadSection(aSection, oStrList);


 // s:=ExtractFileDir(aIniFileName);



//  aOutStrList.Clear;

 // sKeyFieldName  :=FLD_ID;
  sCheckFieldName:=FLD_CHECKED;


  for i := 0 to oStrList.Count - 1 do
  begin

    sKey:=oStrList[i];
//      v   :=Values[i, iCheckColumnInd];

    //  sKey:=VarToString(iKey);
    //  b:=oStrList.IndexOf (sKey)>=0 ;

    //  Values[i, iCheckColumnInd]:= b ;


    if oDataset.Locate(FLD_ID, sKey, []) then
      //    if oDataset.Fields[iFieldIndex].Value<>aValue then
            db_UpdateRecord (oDataset, FLD_CHECKED, True);

  end;



   {


  sKeyFieldName:=aGrid.DataController.KeyFieldNames;

  Assert(sKeyFieldName<>'');
//  Assert(aCheckFieldName<>'');

//  Assert(Assigned(aOutStrList));
  Assert(Assigned(aGrid.GetColumnByFieldName(sKeyFieldName)));
  Assert(Assigned(aGrid.GetColumnByFieldName(sCheckFieldName)));

  iKeyFieldInd  :=aGrid.GetColumnByFieldName(sKeyFieldName).Index;
  iCheckColumnInd :=aGrid.GetColumnByFieldName(sCheckFieldName).Index;

//  i:=aGrid.DataController.RecordCount;

//  aGrid.DataController.DataSet.Fields

  i:=aGrid.DataController.RecordCount;


  with aGrid.DataController do
    for i := 0 to RecordCount - 1 do
    begin

      iKey:=Values[i, iKeyFieldInd];
//      v   :=Values[i, iCheckColumnInd];

      sKey:=VarToString(iKey);
      b:=oStrList.IndexOf (sKey)>=0 ;

      Values[i, iCheckColumnInd]:= b ;

   //   Values[i, iCheckColumnInd]:= True ;


//      vKeyValue:=aGrid.DataController.Values[iRecIx, iColIdx];


//      bChecked:=AsBoolean(v);

   //   if bChecked then
    //    oIni.WriteBool(aSection, IntToStr(iKey), True);

    end;

//
   }


//  ShowMessage(oStrList.Text);


   oDataSet.First;
   oDataSet.EnableControls;


  FreeAndNil (oStrList);


  FreeAndNil (oIni);

end;




// ---------------------------------------------------------------
procedure cx_CheckDataFields(aGrid: TcxGridDBTableView);
// ---------------------------------------------------------------
var
  I: Integer;
  oDataSet: TDataSet;
  //s: string;
  sFieldName: string;
begin
  Assert(Assigned(aGrid.DataController.DataSource), 'aGrid.DataController.DataSource not assigned');

  oDataSet:=aGrid.DataController.DataSource.DataSet;

  Assert(Assigned(oDataSet));

 // i:=oDataSet.FieldCount;

//  if oDataSet.Active then

  for I := 0 to aGrid.ColumnCount - 1 do
  begin
    sFieldName:=aGrid.Columns[i].DataBinding.FieldName;

    Assert(sFieldName<>'');

    Assert(Assigned(oDataSet.FindField(sFieldName)));
  end;

 // if aGrid.DataController.DataSource then


  // TODO -cMM: cx_CheckDataFields default body inserted
end;


// ---------------------------------------------------------------
procedure cx_ExportToExcel(aView: TcxGrid);
// ---------------------------------------------------------------
var
  oSaveDialog: TSaveDialog;
begin
 // oSaveDialog:=TSaveDialog.Create(self);

  with TSaveDialog.Create(nil) do
  try
    DefaultExt := 'xls';
    Filter := 'Excel files|*.xls';
    if Execute then
      ExportGridToExcel(FileName, aView, False, True, True);
  finally
    Free;
  end;
end;




// ---------------------------------------------------------------
procedure cx_Search(aGrid: TcxGridTableView; aText: string);
// ---------------------------------------------------------------
var
  I: Integer;
  s: string;
  sFilter: string;

  oFilter: TcxFilterCriteria;

begin
 
  
  aGrid.FilterBox.Visible:=fvNever;// :=false;
  
  
  /////////////////////////////////////////////

  s:=Trim(aText);
  if Length(s)<2 then
  begin
   //  dmMain.Filter.Enabled :=False;
  
    aGrid.DataController.Filter.Active := False; 
    
    Exit;
  end;
  
  
  with aGrid.DataController.Filter do
  try
    BeginUpdate;
    Root.Clear;

    sFilter:= '%'+ s + '%';

    Root.BoolOperatorKind:= fboOr;
               

    for I := 1 to aGrid.ColumnCount-1 do
      Root.AddItem(aGrid.Columns[i], foLike, sFilter, sFilter);

  finally
    EndUpdate;
    Active := true;
  end;

end;



procedure cx_SetGridReadOnly (aGrid: TcxGridDBBandedTableView; aValue: boolean);
begin
  aGrid.OptionsData.Editing:=not aValue;
  aGrid.OptionsData.Appending:=not aValue;
  aGrid.OptionsData.Deleting:=not aValue;
//  aGrid.OptionsSelection.CellSelect:=not aValue;

end;


procedure cx_SetGridReadOnly (aGrid: TcxGridDBTableView; aValue: boolean);
begin
  aGrid.OptionsData.Editing:=not aValue;
  aGrid.OptionsData.Appending:=not aValue;
  aGrid.OptionsData.Deleting:=not aValue;
//  aGrid.OptionsSelection.CellSelect:=not aValue;

end;


//-------------------------------------------------------------------
procedure cx_UpdateSelectedItems_Table(aGrid: TcxGridDBTableView; aFieldName: string;
    aValue: Variant);
//-------------------------------------------------------------------
// KeyField for BEELINE TN  - hopsname
// -------------------------------------------------------------------
var i: integer;
  v: Variant;
  s: string;
  iKeyFieldInd: integer;
  iCheckColumnInd: integer;

  oDataset: TDataset;
  iKeyColumnInd: Integer;
  iRecIx: integer;
  r:  TcxCustomGridRecord;

  bkm_safe, bkm: TBookmarkStr;
  iColIdx: integer;
  vKeyValue: Variant;
  iFieldIndex: integer;
  sKeyFieldNames: string;
begin
  Screen.Cursor:=crHourGlass;
//  Application.ProcessMessages;

  Assert(aGrid.DataController.KeyFieldNames<>'', 'DataController.KeyFieldNames=''''');

  Assert(Assigned(aGrid.GetColumnByFieldName(aFieldName)));
  Assert(Assigned(aGrid.DataController.DataSet));

  oDataSet:=aGrid.DataController.DataSet;
  oDataSet.DisableControls;

  aGrid.BeginUpdate;
  aGrid.DataController.BeginLocate;

  iFieldIndex:=oDataset.FieldList.IndexOf(aFieldName);

  sKeyFieldNames:=aGrid.DataController.KeyFieldNames;


  try
    bkm_safe:=oDataSet.Bookmark;

    for i := 0 to aGrid.Controller.SelectedRecordCount - 1 do
      if aGrid.DataController.DataModeController.GridMode = True then
      begin
        // GridMode= TRUE !!!!!!!!!
        bkm := aGrid.DataController.GetSelectedBookmark(i);

        if oDataSet.BookmarkValid(TBookmark(bkm)) then
        begin
          oDataSet.Bookmark:=bkm;
          if oDataset.Fields[iFieldIndex].Value<>aValue then
            db_UpdateRecord (oDataset, iFieldIndex, aValue);
        end;
      end else
      begin
        iRecIx := aGrid.Controller.SelectedRecords[I].RecordIndex;
        Assert(iRecIx>=0, 'Value <0');

        Assert(Assigned(aGrid.DataController.GetItemByFieldName(sKeyFieldNames)), 'Value not assigned');

        iColIdx := aGrid.DataController.GetItemByFieldName(sKeyFieldNames).Index;
        Assert(iColIdx>=0, 'Value <0');

        // Obtains the value of the required field
       // OutputVal :=
        vKeyValue:=aGrid.DataController.Values[iRecIx, iColIdx];

        if oDataset.Locate(sKeyFieldNames, vKeyValue, []) then
         if oDataset.Fields[iFieldIndex].Value<>aValue then
            db_UpdateRecord (oDataset, iFieldIndex, aValue);
      end;

  finally
    if oDataSet.BookmarkValid(TBookmark(bkm_safe)) then
      oDataSet.Bookmark:=bkm_safe;

    aGrid.DataController.EndLocate;
    aGrid.EndUpdate;
  end;

  oDataSet.EnableControls;

  Screen.Cursor:=crDefault;

end;



//-------------------------------------------------------------------
procedure cx_UpdateSelectedItems_BandedTable(aGrid: TcxGridDBBandedTableView; aFieldName:
    string; aValue: Variant);
//-------------------------------------------------------------------
// KeyField for BEELINE TN  - hopsname
// -------------------------------------------------------------------
var i: integer;
  v: Variant;
  s: string;
  iKeyFieldInd: integer;
  iCheckColumnInd: integer;

  oDataset: TDataset;
  iKeyColumnInd: Integer;
  iRecIx: integer;
  r:  TcxCustomGridRecord;

  bkm_safe, bkm: TBookmarkStr;
  iColIdx: integer;
  vKeyValue: Variant;
  iFieldIndex: Integer;
  oCustomGridTableItem: TcxCustomGridTableItem;
  sKeyFieldNames: string;
begin
 // Application.ProcessMessages;
  Assert(Assigned(aGrid.GetColumnByFieldName(aFieldName)));
  Assert(Assigned(aGrid.DataController.DataSet));

  Assert(aGrid.DataController.KeyFieldNames<>'', 'DataController.KeyFieldNames=''''');

  Screen.Cursor:=crHourGlass;


  oDataSet:=aGrid.DataController.DataSet;
  oDataSet.DisableControls;

  aGrid.BeginUpdate;
  aGrid.DataController.BeginLocate;

   //��� ��������
  iFieldIndex:=oDataset.FieldList.IndexOf(aFieldName);

  sKeyFieldNames:=aGrid.DataController.KeyFieldNames;

  try
    bkm_safe:=oDataSet.Bookmark;

    for i := 0 to aGrid.Controller.SelectedRecordCount - 1 do
      if aGrid.DataController.DataModeController.GridMode = True then
      begin
        // GridMode= TRUE !!!!!!!!!
        bkm := aGrid.DataController.GetSelectedBookmark(i);

        if oDataSet.BookmarkValid(TBookmark(bkm)) then
        begin
          oDataSet.Bookmark:=bkm;
          if oDataset.Fields[iFieldIndex].Value<>aValue then
            db_UpdateRecord (oDataset, iFieldIndex, aValue);

        end;

      end else
      begin
        iRecIx := aGrid.Controller.SelectedRecords[I].RecordIndex;
        Assert(iRecIx>=0, 'Value <0');


      //  Assert(aKeyFieldName<>'', 'KeyFieldName=''''');
//        Assert(Assigned(aGrid.DataController.GetItemByFieldName(aKeyFieldName)), 'Value not assigned');


        oCustomGridTableItem:=aGrid.DataController.GetItemByFieldName(sKeyFieldNames);
        Assert(Assigned(oCustomGridTableItem), 'oCustomGridTableItem not assigned');


        iColIdx := oCustomGridTableItem.Index;

     //   iColIdx := aGrid.DataController.GetItemByFieldName(sKeyFieldNames).Index;
        Assert(iColIdx>=0, 'Value <0');

        // Obtains the value of the required field
       // OutputVal :=
        vKeyValue:=aGrid.DataController.Values[iRecIx, iColIdx];

        if oDataset.Locate(sKeyFieldNames, vKeyValue, []) then
          if oDataset.Fields[iFieldIndex].Value<>aValue then
            db_UpdateRecord (oDataset, iFieldIndex, aValue);
      end;

  finally
    if oDataSet.BookmarkValid(TBookmark(bkm_safe)) then
      oDataSet.Bookmark:=bkm_safe;

    aGrid.DataController.EndLocate;
    aGrid.EndUpdate;
  end;

  oDataSet.EnableControls;

  Screen.Cursor:=crDefault;

end;

//----------------------------------------------------------------
procedure cx_Init(aGrid: TcxDBTreeList);
//----------------------------------------------------------------
begin
  aGrid.LookAndFeel.NativeStyle:=False;

  aGrid.OptionsCustomizing.ColumnMoving:=False;
  aGrid.OptionsCustomizing.ColumnCustomizing:=True;
  aGrid.OptionsCustomizing.ColumnsQuickCustomization:=True;

  aGrid.OptionsCustomizing.StackedColumns:=false;
//  aGrid.OptionsCustomizing.ColumnVertSizing:=False;

end;


procedure cx_Init(aGrid: TcxGrid);
var
  i: Integer;
begin
  aGrid.LookAndFeel.NativeStyle:=False;

  for i:=0 to aGrid.Levels.Count-1 do
    if aGrid.Levels[i].GridView is TcxGridDBBandedTableView then
      with aGrid.Levels[i].GridView as TcxGridDBBandedTableView do
      begin
        OptionsCustomize.ColumnMoving:=False;
//        OptionsCustomize.ColumnCustomizing:=True;
        OptionsCustomize.ColumnsQuickCustomization:=True;

//        OptionsCustomize.StackedColumns:=false;

      end else

    if aGrid.Levels[i].GridView is TcxGridDBTableView then
      with aGrid.Levels[i].GridView as TcxGridDBTableView do
      begin
        OptionsCustomize.ColumnMoving:=False;
//        OptionsCustomize.ColumnCustomizing:=True;
        OptionsCustomize.ColumnsQuickCustomization:=True;

//        OptionsCustomize.StackedColumns:=false;

      end


end;

{

procedure cx_Init(aGrid: TcxGridDBBandedTableView);
begin
//  aGrid.LookAndFeel.NativeStyle:=False;

  aGrid.OptionsCustomizing.ColumnMoving:=False;
  aGrid.OptionsCustomizing.ColumnCustomizing:=True;
  aGrid.OptionsCustomizing.ColumnsQuickCustomization:=True;

  aGrid.OptionsCustomizing.StackedColumns:=false;

  // TODO -cMM: cx_Init default body inserted
end;
}


//-------------------------------------------------------------------
function cx_GetCheckedCount(aGrid: TcxGridDBBandedTableView; aFieldName:
    string): Integer;
//-------------------------------------------------------------------
// KeyField for BEELINE TN  - hopsname
// -------------------------------------------------------------------
var i: integer;
  v: Variant;
//  s: string;
 // oStrList: TStringLIst;
 // oDataset: TDataset;
  iColumnInd: Integer;
begin
 // oStrList:=TStringList.Create;
  Result:=0;

  Assert(Assigned(aGrid.GetColumnByFieldName(aFieldName)));
  iColumnInd  :=aGrid.GetColumnByFieldName(aFieldName).Index;

//  Assert(Assigned(aGrid.DataController.DataSet));

//  oDataset:=aGrid.DataController.DataSet;
 // oDataSet.DisableControls;

  // aGrid.DataController.Values

//  aGrid.DataController.RecordCount

  with aGrid.DataController do
    for i := 0 to RecordCount - 1 do
    begin
      v:=Values[i, iColumnInd];
      
      if v = true then
        Inc (Result);

      try
       // s:=Values[iKeyColumnInd];
      except

      end;

//      oStrList.Add (s);
    end;




end;




begin
  cx_LocalizeRus();
//  InitRes();
 // dx_Init (Application, Mouse);
end.

{


