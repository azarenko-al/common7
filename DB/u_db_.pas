unit u_db_;

interface


uses


//..  OleAuto,
  SysUtils, Classes, DB, Variants,  Forms, Dialogs, StrUtils, Math, cxCustomData, Windows,
  ADODB;




function db_StoredProc_Open(aAdoStoredProc: TAdoStoredProc; aProcName: string;
    aParams: array of Variant): Integer;

function db_StoredProc_Exec(aAdoStoredProc: TAdoStoredProc; aProcName: string;
    aParams: array of Variant): Integer;

type
   TException_DBNETLIB_ConnectionWrite = Class(Exception);


implementation


function db_ExecStoredProc(aAdoStoredProc: TAdoStoredProc; aProcName: string;
    aParams: array of Variant; aIsOpenQuery: boolean = false): Integer; forward;


function db_StoredProc_Open(aAdoStoredProc: TAdoStoredProc; aProcName: string;
    aParams: array of Variant): Integer;
begin
  Result:=db_ExecStoredProc (aAdoStoredProc, aProcName, aParams, true);
end;


function db_StoredProc_Exec(aAdoStoredProc: TAdoStoredProc; aProcName: string;
    aParams: array of Variant): Integer;
begin
  Result:=db_ExecStoredProc (aAdoStoredProc, aProcName, aParams, False);

end;


// ---------------------------------------------------------------
function db_ExecStoredProc(aAdoStoredProc: TAdoStoredProc; aProcName: string;
    aParams: array of Variant; aIsOpenQuery: boolean = false): Integer;
// ---------------------------------------------------------------
var
  b: Boolean;
//  oAddStringsList: TStringList;
  s, sAddString: string;
  oParam: TParameter;
  i: Integer;

  oParameter: TParameter;
  bEqual: boolean;
  k: Integer;
  sFieldName: string;
  sFile: string;
  vValue: Variant;

begin
//TLog.Start ('db_ExecStoredProc - '+ aProcName);

  Assert(Assigned(aAdoStoredProc.Connection), 'aAdoStoredProc.Connection not assigned');

//  Screen.Cursor:=crHourGlass;



  try
   aAdoStoredProc.Close;


 //   aAdoStoredProc.Prepared:= true;
    i := aAdoStoredProc.Parameters.Count;

    bEqual:=aAdoStoredProc.ProcedureName = aProcName;
    aAdoStoredProc.ProcedureName:= aProcName;

    i := aAdoStoredProc.Parameters.Count;

    if (not bEqual) or (aAdoStoredProc.Parameters.Count=0) then
      aAdoStoredProc.Parameters.Refresh;

 //   aAdoStoredProc.ExecProc;


    i := aAdoStoredProc.Parameters.Count;


   s := '';

   for i:=0 to aAdoStoredProc.Parameters.Count-1 do
    begin
      oParameter:=aAdoStoredProc.Parameters[i];
      s := s+ '--'+ aAdoStoredProc.Parameters[i].Name;
    end;


    for i:=0 to (Length(aParams) div 2) -1 do
    begin

      sFieldName:=aParams[i*2];
      vValue    :=aParams[i*2+1];

      oParameter:=aAdoStoredProc.Parameters.FindParam('@'+sFieldName);

      Assert (Assigned(oParameter), sFieldName);

      if Assigned(oParameter) then
      begin
        k:=oParameter.Size;

        {
        if ((oParameter.DataType=ftString) and (oParameter.Size>100000))
          or
           (oParameter.DataType=ftVarBytes)
         }

        if (oParameter.DataType=ftVarBytes) then
        begin
          if FileExists(vValue) then
             oParameter.LoadFromFile(vValue, ftVarBytes);

        end
        else
          oParameter.Value:= vValue

      end
      else
        //g_Log.Error ('db_ExecStoredProc - field not found:'+aProcName+'-'+aParams[i].FieldName);
    end;


 //////////   LogSys('db_ExecStoredProc: '+ aProcName);


   if aIsOpenQuery then
     aAdoStoredProc.Open
   else
     aAdoStoredProc.ExecProc;
     


    oParameter:=aAdoStoredProc.Parameters.FindParam('@RETURN_VALUE');
    if Assigned(oParameter) then
      Result := oParameter.Value
    else
      Result := -9999;


  except
    on E: Exception do
    begin
      s:=E.ClassName;

      b:=E.ClassName='EOleException';
//      b:=E is EOleException;

      if E.ClassName='EOleException' then
      //Pos('ConnectionWrite',E.Message)>0 then
      begin
//          Memo1.Lines.Add( '[DBNETLIB][ConnectionWrite (send()).]');

          raise TException_DBNETLIB_ConnectionWrite.Create(E.Message);
      end;



      s:= aProcName+ ' params: ';

      for i:=0 to (Length(aParams) div 2) -1 do
      begin
        sFieldName:=aParams[i*2];
        vValue    :=aParams[i*2+1];

        s:= s+ sFieldName+' - '+ VarToStr(vValue)+' ; ';

      end;

//      codeSite.SendError(s);
//      db_LogException('u_db.db_ExecStoredProc', E.Message + s);
    end;
  end;

//  Screen.Cursor:=crDefault;

//TLog.Stop ();

end;



end.
