unit u_db_mdb;

interface
uses Windows,Classes,ComObj, ADODB, DB, SysUtils, Dialogs,Variants,

u_db,

     u_Log,
     u_files,
     u_func

     ;


  //---------------------------------------------------------
  // MDB
  //---------------------------------------------------------
  function mdb_OpenConnection(aADOConnection: TADOConnection; aFileName: string): Boolean;

  function  mdb_CreateFile          (aMDBFileName: String): boolean;
  function  mdb_TableExists         (aMDBFileName: string; aTableName: string): boolean;
  procedure mdb_CompactDatabase     (aDatabaseName : String);

  function  mdb_GetTableNameArr     (aMDBFileName: string): TStrArray;
  procedure mdb_GetTableNameList    (aMDBFileName: string; aStrList: TStrings);

  function  mdb_CreateTableFromDataset (aMDBFileName, aTblName: string; aDataSetWithFields: TDataSet): boolean;

  function mdb_MakeCreateSql_From_Dataset(aDataset: TDataset; aTableName:
      string): String;

  function mdb_MakeCreateSQL(aTableName: string; aFieldArr: array of
      TdbFieldRec): String;

function mdb_CreateFileAndOpen(aMDBFileName: String; aADOConnection:
    TADOConnection): boolean;

procedure mdb_CreateTableFromArr(aADOConnection: TADOConnection; aTableName:
    string; aFieldArr: array of TdbFieldRec);


//============================================================================//
implementation
//============================================================================//


//--------------------------------------------------------------------
function mdb_MakeCreateSQL(aTableName: string; aFieldArr: array of
    TdbFieldRec): String;
//--------------------------------------------------------------------
var
  I: Integer;
  s: string;
begin
  s:='';

  for I := 0 to High(aFieldArr) do
  begin
    s:=s+ Format('[%s]', [aFieldArr[i].Name]);

    case aFieldArr[i].Type_ of
      ftAutoInc: s:=s + ' identity';
      ftInteger: s:=s + ' integer';
      ftGraphic: s:=s + ' LONGBINARY';
      ftString : s:=s + ' varchar(' + IntToStr(aFieldArr[i].Size) +')' ;
      ftFloat  : s:=s + ' Float' ;
    else
      raise Exception.Create( '');
    end;

    if i< High(aFieldArr) then
      s:=s+ ',';
  end;

  Result := 'CREATE TABLE ['+ aTableName +'] ('+ s +')';

end;


//--------------------------------------------------------------------
procedure mdb_CreateTableFromArr(aADOConnection: TADOConnection; aTableName:
    string; aFieldArr: array of TdbFieldRec);
//--------------------------------------------------------------------
var
  s: string;
begin
  s:=mdb_MakeCreateSQL (aTableName, aFieldArr);
  aADOConnection.Execute(s);
end;



// ---------------------------------------------------------------
function mdb_GetConnectionString(aFileName: string): string;
// ---------------------------------------------------------------
const
  MDB_CONNECTION_STRING = 'Provider=Microsoft.Jet.OLEDB.4.0;' +
                          'Persist Security Info=False;' +
                          'Data Source=%s;';
begin
  aFileName:=ChangeFileExt (aFileName, '.mdb');

  {
  while (Length(aFileName)>0) and (aFileName[1]='"') do
    System.Delete (aFileName, 1,1);
  }

  //  aFileName:=aFileName;
  Result:=Format (MDB_CONNECTION_STRING, [aFileName]);

end;

// ---------------------------------------------------------------
function mdb_OpenConnection(aADOConnection: TADOConnection; aFileName: string): Boolean;
// ---------------------------------------------------------------
begin
  Result:=False;

  if not FileExists(aFileName) then
  begin
    ShowMessage('File not exists: '+ aFileName);
    Exit;
  end;

  aADOConnection.Close;
  aADOConnection.LoginPrompt := False;

  aADOConnection.ConnectionString:=mdb_GetConnectionString (aFileName);
  aADOConnection.Open;

  Result := aADOConnection.Connected;
end;

//--------------------------------------------------------------------
function mdb_CreateFileAndOpen(aMDBFileName: String; aADOConnection:
    TADOConnection): boolean;
//--------------------------------------------------------------------
begin
  mdb_CreateFile(aMDBFileName);
  mdb_OpenConnection(aADOConnection, aMDBFileName);

end;


//--------------------------------------------------------------------
function mdb_CreateFile(aMDBFileName: String): boolean;
//--------------------------------------------------------------------
var
  vCatalog: Variant;
begin
  Assert(aMDBFileName<>'');

  aMDBFileName:=ChangeFileExt(aMDBFileName, '.mdb');
  ForceDirByFileName (aMDBFileName);

  if FileExists(aMDBFileName) then
    DeleteFile(aMDBFileName);


  Assert(not FileExists(aMDBFileName) , 'not FileExists(aMDBFileName): '+ aMDBFileName);


 // Result:=True;
//  if not FileExists(aMDBFileName) then
//  begin
  try
    vCatalog := CreateOleObject('ADOX.Catalog');
    vCatalog.Create (Format('Provider=Microsoft.Jet.OLEDB.4.0; Data Source="%s"', [aMDBFileName]));

    Result:=FileExists(aMDBFileName);

  except
//      Result:=False;
  end;
// end;


end;


//-------------------------------------------------------------------
function mdb_GetTableNameArr (aMDBFileName: string): TStrArray;
//-------------------------------------------------------------------
var oStrList: TStringList;
  i: integer;
begin
  oStrList:=TStringList.Create;

  mdb_GetTableNameList (aMDBFileName, oStrList);
  SetLength (Result, oStrList.Count);
  for i:=0 to oStrList.Count-1 do
    Result[i]:=oStrList[i];

  oStrList.Free;
end;

//-------------------------------------------------------------------
function mdb_TableExists (aMDBFileName: string; aTableName: string): boolean;
//-------------------------------------------------------------------
var aList: TStringList;
begin
  aList:=TStringList.Create;
  mdb_GetTableNameList (aMDBFileName, aList);
  Result:=(aList.IndexOf (aTableName) >=0 );
  aList.Free;
end;

{
//---------------------------------------------------------------------------
procedure mdb_CompactDatabase_In_Dir(aDir : String);
//---------------------------------------------------------------------------
var
  I: Integer;
  oFiles: TStringList;
begin
  oFiles:=TStringList.create;

  aDir:=IncludeTrailingBackslash(aDir);

  ScanDir1(aDir, '*.mdb', oFiles);

  for I := 0 to oFiles.Count - 1 do    // Iterate
    mdb_CompactDatabase(oFiles[i]);

end;
}

//--------------------------------------------------------------------
procedure mdb_CompactDatabase(aDatabaseName : String);
//--------------------------------------------------------------------
const
  Provider = 'Provider=Microsoft.Jet.OLEDB.4.0;';
  MAX_PATH = 200;
var
  TempName: array[0..MAX_PATH] of Char; // ��� ���������� �����
  sTempPath: string; // ���� �� ����
  sName: string;
  sSrc, sDest: String;
  V: Variant;
begin
  try
    sSrc := Provider + 'Data Source=' + aDatabaseName;
    // �������� ���� ��� ���������� �����
    sTempPath := ExtractFilePath(aDatabaseName);
    if sTempPath = '' then
      sTempPath := GetCurrentDir;
    //�������� ��� ���������� �����
    GetTempFileName(PChar(sTempPath), 'mdb', 0, TempName);
    sName := StrPas(TempName);
    DeleteFile(PChar(sName)); // ����� ����� �� ������ ������������ :))
    sDest := Provider + 'Data Source=' + sName;

    V := CreateOleObject('jro.JetEngine');
    try
      V.CompactDatabase(sSrc, sDest); // �������
    finally
      V := 0;
    end;
      DeleteFile((aDatabaseName)); //  ������� �� ����������� ����

//      DeleteFile(PChar(aDatabaseName)); //  ������� �� ����������� ����
      RenameFile(sName, aDatabaseName); //  � ��������������� ����������� ����
  except
    // ������ ��������� �� �������������� ��������
    on E: Exception do
      ShowMessage(E.message);
  end;
end;

//-------------------------------------------------------------------
procedure mdb_GetTableNameList(aMDBFileName: string; aStrList: TStrings);
//-------------------------------------------------------------------
var oConn: TADOConnection;
begin
  oConn:=TADOConnection.Create(nil);
  oConn.ConnectionString:=mdb_GetConnectionString(aMDBFileName);
  oConn.LoginPrompt:=False;

  oConn.Open;
  oConn.GetTableNames (aStrList, False); //: TStrings; SystemTables: Boolean = False);
//  oConn.Close;
  oConn.Free;
end;

//--------------------------------------------------------------------
function mdb_CreateTableFromDataset (aMDBFileName, aTblName: string; aDataSetWithFields: TDataSet): boolean;
//--------------------------------------------------------------------
const
  MAX_FIELD_LENGTH = 250;
var
  j, iDataSize: Integer;
  oFieldList: TStringList;
  sql: string;
  oFieldType: TFieldType;
  oADOCommand: TADOCommand;
  s: string;
begin
  oFieldList:=     TStringList.Create;
  oADOCommand:=    TADOCommand.Create(nil);

  oADOCommand.ConnectionString:= mdb_GetConnectionString (aMDBFileName);

  aDataSetWithFields.GetFieldNames(oFieldList);

  sql:= 'CREATE TABLE '+ aTblName+ ' (';

  Result:= false;

  for j:= 0 to oFieldList.Count-1 do
  begin
    oFieldType:= aDataSetWithFields.FieldByName(oFieldList[j]).DataType;
    case oFieldType of
      ftString,
      ftWideString:  begin
                        iDataSize:= aDataSetWithFields.FieldByName(oFieldList[j]).DataSize;
                        if iDataSize > MAX_FIELD_LENGTH then
                          iDataSize := MAX_FIELD_LENGTH;
                        s:= '['+oFieldList[j]+']'+' varchar('+AsString(iDataSize)+'),';
                      end;

      ftDateTime:s:= '['+oFieldList[j]+']'+' varchar(250),';
      ftGuid:    s:= '['+oFieldList[j]+']'+' guid,';

      ftMemo,
      ftBlob:    s:= '['+oFieldList[j]+']'+' longbinary,';

      ftAutoInc,
      ftInteger,
      ftSmallInt,
      ftBCD:     s:= '['+oFieldList[j]+']'+' int,';

      ftFloat:   s:= '['+oFieldList[j]+']'+' float,';
      ftBoolean: s:= '['+oFieldList[j]+']'+' bit,';
    end;

    sql:=sql + s;
  end;

  str_DeleteLastChar(sql);
  sql:=sql+ ')';

  try
    oADOCommand.ParamCheck:=  false;
    oADOCommand.CommandText:= sql;
    oADOCommand.Execute;
    Result:= true;
  except
    on e: Exception do begin
    //  gl_DB.ErrorMsg:= E.Message;
      g_Log.AddRecord('u_db', 'mdb_CreateTable', E.Message);
    end;
  end;

  oADOCommand.Free;
  oFieldList.Free;
end;


// ---------------------------------------------------------------
function mdb_MakeCreateSql_From_Dataset(aDataset: TDataset; aTableName:
    string): String;
// ---------------------------------------------------------------

      function db_FieldTypeToString_for_MDB1(aFieldType: TFieldType; aSize: Integer):
          String;
      begin
        case aFieldType of
          ftUnknown :	Result:='';

          ftString	: begin
                        if (aSize>255) and (aSize<1000) then
                          aSize:= 255;

                        Result:='varchar('+AsString(aSize)+')';

                        if aSize>=1000 then
                          Result:='memo';
                      end;

          ftSmallint:	Result:='int null';
          ftInteger :	Result:='int null';
          ftWord    :	Result:='int null';

          ftBoolean : Result:='logical';
          ftFloat   : Result:='float null';
          ftCurrency: Result:='money null';
          ftDate    : Result:='date';
          ftTime    : Result:='date';
          ftDateTime: Result:='date';

          ftMemo    : Result:='memo';
          ftBlob    : Result:='image';

          ftGraphic : Result:='image';

          //ftAutoInc	: Result:='int IDENTITY PRIMARY KEY';
      //    ftAutoInc    :Result:='int PRIMARY KEY';
          ftAutoInc    :Result:='int';

          ftWideString :   begin
                           //   Assert(aSize<>4);

                              if aSize=4 then
                                aSize:=200;

                               Result:='nvarchar('+AsString(aSize)+')';
                           end;

          ftLargeInt	 :Result:='int null';
          ftGuid	     :Result:='varchar(40)';
        else
    //      Result:='';
          raise EClassNotFound.Create('');
        end;
      end;


var
  I: Integer;
  j: Integer;
  sTable: string;
  sFields: string;
  sFld: String;
  sSQL: string;
begin
  // ---------------------------------------------
  Assert(aDataset.FieldCount>0);
  // ---------------------------------------------

  sFields :='';

  for I := 0 to aDataset.FieldCount-1 do
  begin
      with aDataset.Fields.Fields[I] do
      begin
        if Pos('.',FieldName)>0 then
          Continue;


       sFld:=db_FieldTypeToString_for_MDB1(DataType, DataSize);
       sFld:=Format(', [%s] %s', [FieldName, sFld ]);

        sFields:=sFields + sFld;
      end;
  end;

  sFields:=Copy(sFields,2,Length(sFields)-1);

  result:= 'CREATE TABLE '+ aTableName + ' ('+ sFields + ')';

//  Result:=ReplaceStr(Result,'.','_');

end;



end.

