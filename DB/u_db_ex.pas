unit u_db_ex;

interface
uses
   Classes,DB,SysUtils;


procedure db_CreateFieldsForDataset_(aDataSet: TDataSet);

implementation


//--------------------------------------------------------------------
procedure db_CreateFieldsForDataset_(aDataSet: TDataSet);
//--------------------------------------------------------------------
var
  I: Integer;
  bActive: Boolean;
  oField: TField;
  oFieldDef: TFieldDef;
begin
  Assert(Assigned(aDataset));

  bActive:=aDataSet.Active;
  if bActive then
    aDataSet.Close;

  for I := 0 to aDataSet.FieldDefs.Count - 1 do
  begin
    oFieldDef := aDataSet.FieldDefs[i];

    //aDataset.Fields.FindField (aFieldName);

    case aDataSet.FieldDefs[i].DataType of
      ftWideString:begin
                    oField:=TWideStringField.Create(aDataset);
                    oField.Size:=oFieldDef.Size;
                   end;

      ftFixedChar,
      ftString:    begin
                    oField:=TStringField.Create(aDataset);
                    oField.Size:=oFieldDef.Size;
                   end;
    //  :  oField:=TIntegerField.Create(aDataset); //��������� 22-08-06
      ftGuid   :  oField:=TStringField .Create(aDataset); //��������� 22-08-06

      ftAutoInc,
      ftSmallInt,
      ftInteger:  oField:=TIntegerField.Create(aDataset);

      ftBoolean:  oField:=TBooleanField.Create(aDataset);
      ftDateTime: begin
                    oField:=TDateTimeField.Create(aDataset);

                   // TDateTimeField(oField).DisplayFormat:= oFieldDef.
                 //   'dd.mm.yyyy hh:mm';
                  end;
      ftDate:     begin
                    oField:=TDateField.Create(aDataset);
                 //   TDateField(oField).DisplayFormat:='dd.mm.yyyy';
                  end;

      ftFloat:    oField:=TFloatField.Create(aDataset);
      ftMemo:     oField:=TMemoField.Create(aDataset);

      ftGraphic,
      ftBlob:     oField:=TBlobField.Create(aDataset);
    else
      raise Exception.Create('db_CreateField - field type is not defined');
      //Exit;                           //�������������� 23-08-06
      oField:=TStringField.Create(aDataset); //��������� 23-08-06
      oField.Size:=oFieldDef.Size;               //��������� 23-08-06
    end;

   oField.FieldName := oFieldDef.Name;

    oField.DataSet:=aDataset;
  end;


 // if Assigned(aDataSet) then
  if bActive then
    aDataSet.Open;


end;


end.
