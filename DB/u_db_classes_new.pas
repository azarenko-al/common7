unit u_db_classes_new;

interface
uses
  Classes,SysUtils,

  u_func
  ;


type
  // ---------------------------------------------------------------
  TdbRecordList = class(TStringList)
  // ---------------------------------------------------------------
  private
    procedure Show;
  public
    Selection: TStringList;

    constructor Create;
    destructor Destroy; override;


    procedure SetValue(aTableName: string; aSrcKey, aDestKey: variant);
    function GetValue(aTableName: string; aKey: variant): Variant;

    function Exists(aTableName: string; aKey: variant): Boolean;

    function SelectNotAppendedByTableName(aTableName: string): Integer;
  end;


implementation

uses Variants;


constructor TdbRecordList.Create;
begin
  inherited Create;

 // Sorted := True;
  Duplicates := dupIgnore;

  Selection := TStringList.Create();
end;

destructor TdbRecordList.Destroy;
begin
  FreeAndNil(Selection);
  inherited Destroy;
end;

function TdbRecordList.Exists(aTableName: string; aKey: variant): Boolean;
var
  s: string;
begin
  s:=Values[Format('%s:%s',[aTableName, VarToStr(aKey)])];
  Result :=s<>'';
end;

function TdbRecordList.GetValue(aTableName: string; aKey: variant): Variant;
var
  s: string;
begin
  s:=Values[Format('%s:%s',[aTableName, VarToStr(aKey)])];

  if s<>'' then
    Result := s
  else
    Result := null;
end;

// ---------------------------------------------------------------
procedure TdbRecordList.SetValue(aTableName: string; aSrcKey, aDestKey:
    variant);
// ---------------------------------------------------------------
var
  sSrc: string;
  sDest,S: string;
begin
  s :=VarToStr(aSrcKey);
  sDest :=VarToStr(aDestKey);

  if sDest='' then
    sDest :='-';

//    raise Exception.Create('');

  Values[Format('%s:%s',[aTableName, s])] := sDest;
end;

// ---------------------------------------------------------------
function TdbRecordList.SelectNotAppendedByTableName(aTableName: string):
    Integer;
// ---------------------------------------------------------------
var
  I: Integer;
  iPos: Integer;
  s1: string;
  s2: string;
begin
  Selection.Clear;

  for I := 0 to Count - 1 do
  begin
    iPos := Pos(':',Names[i]);
    s1:=Copy(Names[i], 1, iPos-1);
    s2:=Copy(Names[i], iPos+1, 100);

    if Eq(s1,aTableName) then
      if ValueFromIndex[i]='-' then
        Selection.Add(s2);
  end;

  Result := Selection.Count;
end;


procedure TdbRecordList.Show;
begin
 // ShellExec_Notepad_temp(Text);

end;

end.
