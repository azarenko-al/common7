unit u_DEMO_XML;

interface
uses
  u_func,
  u_XML,
  u_GEO;


type
  //----------------------------------------------------
  TcmCell = record
  //----------------------------------------------------
    ID: integer;
    Name: string;
    KupFileName: string;
  end;

  //----------------------------------------------------
  TcmStandardRec = record
  //----------------------------------------------------
    ID  : integer;
    Name: string;

    kupFileName,
    kmoFileName,
    kgrFileName,
    NBestFileName,
    hoffFileName,
    SummaryFileName: string;

    Cells: array of TcmCell;
  end;

  Tdual_C1 = (opBySense,opBySensePlusPorog); //['C1 = P(��)','C2 = C1 + A(��ண)'], [0,1]);

  //----------------------------------------------------
  TcmRegionRec = record
  //----------------------------------------------------
    ID    : integer;
    Name  : string;
    XYRect: TXYRect;
    CalcStep: integer;

    TopLeft_X, TopLeft_Y: double;

    Abonent_Sensitivity: integer;
    RowCount, ColCount: integer;


    Dual: record
      kupFileName: string;
      kgrFileName: string;
      kmoFileName: string;

      dual_C1 : Tdual_C1; //['C1 = P(��)','C2 = C1 + A(��ண)'], [0,1]);
      dual_A  : integer; // ���祭�� ����� A(��ண)[dB]
    end;

    Standards: array of TcmStandardRec;
  end;


  //----------------------------------------------------
  TcmParams = class
  //-----------------------------------------------------
  public
    Options: record  //��ࠬ���� ��� ����
      IsMakeNBestMatrix  : boolean;
      NBestMatrixRecSize: integer;

      IsMakeHandOffMatrix: boolean;
    //  IsUseSenseInKGR : integer;
    end;

    Regions: array of TcmRegionRec;

    procedure LoadFromXML (aFileName: string);
  end;



implementation


{ TcmParams }

//-------------------------------------------------------------------
procedure TcmParams.LoadFromXML(aFileName: string);
//-------------------------------------------------------------------
  //-------------------------------------------------------------------
  procedure DoAssignRegion (vNode: Variant; var aRegionRec: TcmRegionRec);
  //-------------------------------------------------------------------
  var vDual: IXMLNode;
     v: Variant;

  begin
    with aRegionRec do
    begin
      ID  :=xml_GetIntAttr (vNode, 'id'));
      Name:=xml_GetAttr (vNode, 'name');

      XYRect.TopLeft.X :=xml_GetIntAttr (vNode, 'x1'));
      XYRect.TopLeft.Y :=xml_GetIntAttr (vNode, 'y1'));
      XYRect.BottomRight.X :=xml_GetIntAttr (vNode, 'x2'));
      XYRect.BottomRight.Y :=xml_GetIntAttr (vNode, 'y2'));

      TopLeft_X:=XYRect.TopLeft.X;
      TopLeft_Y:=XYRect.TopLeft.Y;

      RowCount:=xml_GetIntAttr (vNode, 'RowCount'));
      ColCount:=xml_GetIntAttr (vNode, 'ColCount'));

      CalcStep:=xml_GetIntAttr (vNode, 'CalcStep'));
      Abonent_Sensitivity:=xml_GetIntAttr (vNode, 'Abonent_Sensitivity'));

      vDual:=xml_GetNodeByPath (vNode, ['Dual']);
      if not VarIsNull (vDual) then begin
        Dual.kupFileName:=xml_GetAttrByPath (vDual, 'kupFileName', 'value');
        Dual.kgrFileName:=xml_GetAttrByPath (vDual, 'kgrFileName', 'value');
        Dual.kmoFileName:=xml_GetAttrByPath (vDual, 'kmoFileName', 'value');

        Dual.dual_C1:=Tdual_C1 (xml_GetIntAttrByPath (vDual, 'dual_C1', 'value')));
        Dual.dual_A :=xml_GetIntAttrByPath (vDual, 'dual_A',  'value'));

      end;


    end;
  end;

  //-------------------------------------------------------------------
  procedure DoAssignStandard (vNode: Variant; var aStandardRec: TcmStandardRec);
  //-------------------------------------------------------------------
  begin
    with aStandardRec do
    begin
      ID  :=xml_GetIntAttr (vNode, 'id'));
      Name:=xml_GetAttr (vNode, 'name');

      kupFileName  :=xml_GetAttrByTagName (vNode, 'kupFileName', 'value');
      kmoFileName  :=xml_GetAttrByTagName (vNode, 'kmoFileName', 'value');
      kgrFileName  :=xml_GetAttrByTagName (vNode, 'kgrFileName', 'value');
      NBestFileName:=xml_GetAttrByTagName (vNode, 'NBestFileName', 'value');
      hoffFileName :=xml_GetAttrByTagName (vNode, 'hoffFileName', 'value');
      SummaryFileName:=xml_GetAttrByTagName (vNode, 'SummaryFileName', 'value');
    end;
  end;

  //-------------------------------------------------------------------
  procedure DoAssignCell (vNode: Variant; var aCellRec: TcmCell);
  //-------------------------------------------------------------------
  begin
    with aCellRec do
    begin
      ID  :=xml_GetIntAttr (vNode, 'id'));
      Name:=xml_GetAttr (vNode, 'name');
      kupFileName:=xml_GetAttr (vNode, 'kupFileName');
    end;
  end;


var
  iCount,iStandard,iCell,iRegion: integer;

  vRoot,vOptions,vRegions,vRegion,vStandards,vStandard,vCells,vCell: Variant;

begin
  gl_XMLDoc.LoadFromFile (aFileName);
  vRoot:=gl_XMLDoc.DocumentElement;

  vOptions:=xml_GetNodeByPath (vRoot, ['Options']);
  if VarIsNull(vOptions) then
    Exit;

  with Options do begin
    IsMakeNBestMatrix  :=AsBoolean(xml_GetAttrByPath(vOptions, 'IsMakeNBestMatrix',   'value'));
    NBestMatrixRecSize :=xml_GetIntAttrByPath(vOptions, 'NBestMatrixRecSize',  'value'));
    IsMakeHandOffMatrix:=AsBoolean(xml_GetAttrByPath(vOptions, 'IsMakeHandOffMatrix', 'value'));
  end;

  {
    Options: record  //��ࠬ���� ��� ����
      IsMakeNBestMatrix  : boolean;
      NBestMatrixRecSize: integer;

      IsMakeHandOffMatrix: boolean;
    //  IsUseSenseInKGR : integer;
    end;
  }
  vRegions:=xml_GetNodeByPath (vRoot, ['Regions']);
  if VarIsNull(vRegions) then
    Exit;

  iCount:=vRegions.ChildNodes.Length;
  SetLength (Regions, iCount);

  for iRegion:=0 to vRegions.ChildNodes.Length-1 do
  begin
    vRegion:=vRegions.ChildNodes.Item(iRegion);
    DoAssignRegion (vRegion, Regions[iRegion]);

    vStandards:=xml_GetNodeByPath (vRegion, ['Standards']);
    if VarIsNull (vStandards) then
      Continue;

    iCount:=vStandards.ChildNodes.Length;
    SetLength (Regions[iRegion].Standards, iCount);


    for iStandard:=0 to vStandards.ChildNodes.Length-1 do
    begin
      vStandard:=vStandards.ChildNodes.Item(iStandard);
      DoAssignStandard (vStandard, Regions[iRegion].Standards[iStandard]);

      vCells:=xml_GetNodeByPath (vStandard, ['CELLS']);
      if VarIsNull (vCells) then
        Continue;

      iCount:=vCells.ChildNodes.Length;
      SetLength (Regions[iRegion].Standards[iStandard].Cells, iCount);


      for iCell:=0 to vCells.ChildNodes.Length-1 do
      begin
        vCell:=vCells.ChildNodes.Item(iCell);
        DoAssignCell (vCell, Regions[iRegion].Standards[iStandard].Cells[iCell]);
      end;
    end;
  end;


end;

end.
