unit Unit8;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls,

  u_files,
  dm_Task_Map_join,

  StdCtrls, cxEdit,
  cxControls, cxContainer, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxShellComboBox, Mask, ToolEdit, Placemnt
  ;

type
  TForm8 = class(TForm)
    Button1: TButton;
    ed_Src: TDirectoryEdit;
    ed_Dest: TDirectoryEdit;
    FormStorage1: TFormStorage;
    procedure Button1Click(Sender: TObject);
  private


  public
    { Public declarations }
  end;

var
  Form8: TForm8;

implementation

{$R *.DFM}

procedure TForm8.Button1Click(Sender: TObject);
begin

  dmTask_Map_join.Params.SrcDir:=ed_Src.Text;
  dmTask_Map_join.Params.DestDir:=ed_Dest.Text;

  dmTask_Map_join.ExecuteDlg();
end;



end.
