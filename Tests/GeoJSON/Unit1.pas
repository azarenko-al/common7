unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,

  u_GeoJSON

  ;

type
  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}


procedure TForm1.FormCreate(Sender: TObject);
var
  oFeatureList: TFeatureCollection;
  oFeature: TFeature;
  oFeature1: TFeature;
  s: string;
begin
  oFeatureList:=TFeatureCollection.Create;

  oFeature:=oFeatureList.AddItem;

  oFeature.Type_:=ftPoint;
  oFeature.SetPoint_LatLot (34,56);

  oFeature.Geometry.Type_:=gtPoint;

  oFeature.Properties.Values['prop1']:='value1';
  oFeature.Properties.Values['prop2']:='value2';
  oFeature.Properties.Values['address']:='����';

  // ---------------------------------------------------------------
  oFeature1:=oFeatureList.AddItem;

  oFeature1.Type_:=ftLineString;
//  oFeature1.SetVector_LatLot (34,56, 33,57 );
  oFeature1.SetRect (LatLon(34,56), LatLon(33,57), LatLon(31,53), LatLon(23,52) );

  oFeature1.Geometry.Type_:=gtLineString;

  oFeature1.Properties.Values['prop1']:='value1';
  oFeature1.Properties.Values['prop2']:='value2';
  oFeature1.Properties.Values['address']:='����';



  s:=oFeatureList.GetJSON;


  FreeAndNil(oFeatureList);

  ShowMessage(s);

end;

end.
