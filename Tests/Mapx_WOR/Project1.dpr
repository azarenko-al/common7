program Project1;

uses
  Forms,
  Unit2 in '..\..\..\RPLS_DB\src\UserSecurity\Unit2.pas' {Form2},
  dm_Security in '..\..\..\RPLS_DB\src\UserSecurity\dm_Security.pas' {dmSecurity: TDataModule},
  Unit6 in '..\..\..\Tests\LeftBar\Unit6.pas' {Form6};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TdmSecurity, dmSecurity);
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TForm6, Form6);
  Application.Run;
end.
