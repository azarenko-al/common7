object Form2: TForm2
  Left = 407
  Top = 200
  Width = 913
  Height = 519
  Caption = 'Form2'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 577
    Height = 492
    ActivePage = TabSheet3
    Align = alLeft
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 377
        Height = 464
        Align = alLeft
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGrid1DBTableView1: TcxGridDBTableView
          DataController.DataSource = DataSource1
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          object cxGrid1DBTableView1id: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Width = 45
          end
          object cxGrid1DBTableView1country: TcxGridDBColumn
            DataBinding.FieldName = 'country'
            Width = 60
          end
          object cxGrid1DBTableView1name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Width = 171
          end
          object cxGrid1DBTableView1code: TcxGridDBColumn
            DataBinding.FieldName = 'code'
            Width = 65
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
      object DBGrid1: TDBGrid
        Left = 377
        Top = 0
        Width = 162
        Height = 464
        Align = alLeft
        DataSource = DataSource2
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
    object ts_Users: TTabSheet
      Caption = 'Users'
      ImageIndex = 1
      object cxGrid3: TcxGrid
        Left = 0
        Top = 0
        Width = 193
        Height = 464
        Align = alLeft
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView1: TcxGridDBTableView
          DataController.DataSource = DataSource1
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          object cxGridDBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Width = 45
          end
          object cxGridDBColumn2: TcxGridDBColumn
            DataBinding.FieldName = 'country'
            Width = 60
          end
          object cxGridDBColumn3: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Width = 171
          end
          object cxGridDBColumn4: TcxGridDBColumn
            DataBinding.FieldName = 'code'
            Width = 65
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
      object DBGrid2: TDBGrid
        Left = 193
        Top = 0
        Width = 280
        Height = 464
        Align = alLeft
        DataSource = ds_Users
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'TabSheet3'
      ImageIndex = 2
      object DBGrid3: TDBGrid
        Left = 0
        Top = 185
        Width = 569
        Height = 161
        Align = alTop
        DataSource = ds_Roles_XREF
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
      object DBGrid4: TDBGrid
        Left = 0
        Top = 0
        Width = 569
        Height = 185
        Align = alTop
        DataSource = ds_Roles1
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 3
      object cxGrid2: TcxGrid
        Left = 16
        Top = 10
        Width = 337
        Height = 391
        TabOrder = 0
        object cxGrid2DBTableView1: TcxGridDBTableView
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
        end
        object cxGrid2Level1: TcxGridLevel
          GridView = cxGrid2DBTableView1
        end
      end
    end
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=onega'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 648
    Top = 8
  end
  object DataSource1: TDataSource
    DataSet = ADOTable1
    Left = 672
    Top = 120
  end
  object ADOTable1: TADOTable
    Connection = ADOConnection1
    CursorType = ctStatic
    Filter = 'country='#39'ru'#39
    Filtered = True
    TableName = 'GeoRegion'
    Left = 672
    Top = 72
    object ADOTable1id: TAutoIncField
      FieldName = 'id'
      ReadOnly = True
    end
    object ADOTable1name: TStringField
      FieldName = 'name'
      ReadOnly = True
      Size = 150
    end
    object ADOTable1code: TIntegerField
      FieldName = 'code'
      ReadOnly = True
    end
    object ADOTable1country: TStringField
      FieldName = 'country'
      ReadOnly = True
      Size = 2
    end
  end
  object ClientDataSet1: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProvider1'
    Left = 680
    Top = 184
    object ClientDataSet1id: TAutoIncField
      DisplayWidth = 7
      FieldName = 'id'
      ReadOnly = True
    end
    object ClientDataSet1name: TStringField
      DisplayWidth = 180
      FieldName = 'name'
      Size = 150
    end
    object ClientDataSet1code: TIntegerField
      DisplayWidth = 6
      FieldName = 'code'
    end
    object ClientDataSet1country: TStringField
      DisplayWidth = 7
      FieldName = 'country'
      Size = 2
    end
  end
  object DataSetProvider1: TDataSetProvider
    DataSet = ADOTable1
    Left = 680
    Top = 240
  end
  object DataSource2: TDataSource
    DataSet = ClientDataSet1
    Left = 768
    Top = 184
  end
  object ds_Roles: TDataSource
    DataSet = tbl_Roles
    Left = 776
    Top = 120
  end
  object tbl_Roles: TADOTable
    Connection = ADOConnection_Security
    CursorType = ctStatic
    Filtered = True
    TableName = 'GeoRegion'
    Left = 776
    Top = 72
    object AutoIncField1: TAutoIncField
      FieldName = 'id'
      ReadOnly = True
    end
    object StringField1: TStringField
      FieldName = 'name'
      ReadOnly = True
      Size = 150
    end
    object IntegerField1: TIntegerField
      FieldName = 'code'
      ReadOnly = True
    end
    object StringField2: TStringField
      FieldName = 'country'
      ReadOnly = True
      Size = 2
    end
  end
  object ADOConnection_Security: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=Security'
    LoginPrompt = False
    Left = 768
    Top = 8
  end
  object ds_Users: TDataSource
    DataSet = tbl_Users
    Left = 848
    Top = 120
  end
  object tbl_Users: TADOTable
    Active = True
    Connection = ADOConnection_Security
    CursorType = ctStatic
    Filtered = True
    TableName = 'Users'
    Left = 848
    Top = 72
    object tbl_UsersUserId: TAutoIncField
      FieldName = 'UserId'
      ReadOnly = True
    end
    object tbl_UsersFirstName: TWideStringField
      FieldName = 'FirstName'
      Size = 50
    end
    object tbl_UsersLastName: TWideStringField
      FieldName = 'LastName'
      Size = 50
    end
    object tbl_UsersEmail: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
    object tbl_UsersLoginId: TWideStringField
      FieldName = 'LoginId'
      Size = 50
    end
    object tbl_UsersPassword: TWideStringField
      FieldName = 'Password'
      Size = 50
    end
    object tbl_UsersWindowsId: TWideStringField
      FieldName = 'WindowsId'
      Size = 500
    end
    object tbl_UsersUseWindowsAuth: TBooleanField
      FieldName = 'UseWindowsAuth'
    end
    object tbl_UsersBuiltInAccount: TBooleanField
      FieldName = 'BuiltInAccount'
    end
    object tbl_UsersSettingsXml: TMemoField
      FieldName = 'SettingsXml'
      BlobType = ftMemo
    end
    object tbl_UsersIsActive: TBooleanField
      FieldName = 'IsActive'
    end
    object tbl_UsersUseWindowsClient: TBooleanField
      FieldName = 'UseWindowsClient'
    end
    object tbl_UsersUseWebClient: TBooleanField
      FieldName = 'UseWebClient'
    end
    object tbl_UsersUseVSClient: TBooleanField
      FieldName = 'UseVSClient'
    end
    object tbl_UsersUseMixedMode: TBooleanField
      FieldName = 'UseMixedMode'
    end
    object tbl_UsersEnterpriseConnectionType: TIntegerField
      FieldName = 'EnterpriseConnectionType'
    end
    object tbl_UsersUseIPhoneClient: TBooleanField
      FieldName = 'UseIPhoneClient'
    end
    object tbl_UsersLastLoginDateTime: TDateTimeField
      FieldName = 'LastLoginDateTime'
    end
    object tbl_UsersReadMessages: TBlobField
      FieldName = 'ReadMessages'
    end
    object tbl_UsersIPhoneSettings: TBlobField
      FieldName = 'IPhoneSettings'
    end
  end
  object qry_Roles: TADOQuery
    Active = True
    Connection = ADOConnection_Security
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    AfterPost = qry_RolesAfterPost
    Parameters = <>
    SQL.Strings = (
      'select * from fn_UserSecurityRoles(1)')
    Left = 664
    Top = 336
  end
  object ds_Roles1: TDataSource
    DataSet = qry_Roles
    Left = 664
    Top = 392
  end
  object qry_Roles_XREF: TADOQuery
    Active = True
    Connection = ADOConnection_Security
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    Parameters = <>
    SQL.Strings = (
      'select * from UserSecurityRoles  where userID=1')
    Left = 768
    Top = 328
  end
  object ds_Roles_XREF: TDataSource
    DataSet = qry_Roles_XREF
    Left = 768
    Top = 392
  end
end
