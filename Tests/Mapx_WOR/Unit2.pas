unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid, Grids, DBGrids, Provider, DBClient, ComCtrls,

  u_db
  ;

type
  TForm2 = class(TForm)
    ADOConnection1: TADOConnection;
    DataSource1: TDataSource;
    ADOTable1: TADOTable;
    ClientDataSet1: TClientDataSet;
    DataSetProvider1: TDataSetProvider;
    DataSource2: TDataSource;
    ADOTable1id: TAutoIncField;
    ADOTable1name: TStringField;
    ADOTable1code: TIntegerField;
    ADOTable1country: TStringField;
    ClientDataSet1id: TAutoIncField;
    ClientDataSet1name: TStringField;
    ClientDataSet1code: TIntegerField;
    ClientDataSet1country: TStringField;
    ds_Roles: TDataSource;
    tbl_Roles: TADOTable;
    AutoIncField1: TAutoIncField;
    StringField1: TStringField;
    IntegerField1: TIntegerField;
    StringField2: TStringField;
    ADOConnection_Security: TADOConnection;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    ts_Users: TTabSheet;
    TabSheet3: TTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1id: TcxGridDBColumn;
    cxGrid1DBTableView1country: TcxGridDBColumn;
    cxGrid1DBTableView1name: TcxGridDBColumn;
    cxGrid1DBTableView1code: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    DBGrid1: TDBGrid;
    cxGrid3: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGridDBColumn4: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    ds_Users: TDataSource;
    tbl_Users: TADOTable;
    DBGrid2: TDBGrid;
    qry_Roles: TADOQuery;
    DBGrid3: TDBGrid;
    ds_Roles1: TDataSource;
    TabSheet2: TTabSheet;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    tbl_UsersUserId: TAutoIncField;
    tbl_UsersFirstName: TWideStringField;
    tbl_UsersLastName: TWideStringField;
    tbl_UsersEmail: TWideStringField;
    tbl_UsersLoginId: TWideStringField;
    tbl_UsersPassword: TWideStringField;
    tbl_UsersWindowsId: TWideStringField;
    tbl_UsersUseWindowsAuth: TBooleanField;
    tbl_UsersBuiltInAccount: TBooleanField;
    tbl_UsersSettingsXml: TMemoField;
    tbl_UsersIsActive: TBooleanField;
    tbl_UsersUseWindowsClient: TBooleanField;
    tbl_UsersUseWebClient: TBooleanField;
    tbl_UsersUseVSClient: TBooleanField;
    tbl_UsersUseMixedMode: TBooleanField;
    tbl_UsersEnterpriseConnectionType: TIntegerField;
    tbl_UsersUseIPhoneClient: TBooleanField;
    tbl_UsersLastLoginDateTime: TDateTimeField;
    tbl_UsersReadMessages: TBlobField;
    tbl_UsersIPhoneSettings: TBlobField;
    DBGrid4: TDBGrid;
    qry_Roles_XREF: TADOQuery;
    ds_Roles_XREF: TDataSource;
    procedure qry_RolesAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

const
  FLD_CHECKED = 'checked';
  FLD_SecurityRoleId = 'SecurityRoleId';
  FLD_UserId = 'UserId';



procedure TForm2.qry_RolesAfterPost(DataSet: TDataSet);
var
  bChecked,bLocated: Boolean;
  iID: Integer;
begin
  iID:=DataSet[FLD_SecurityRoleId];
  bLocated := qry_Roles_XREF.Locate(FLD_SecurityRoleId, iID, []);
  bChecked := DataSet[FLD_CHECKED];

  if (not bChecked) and (bLocated) then
      qry_Roles_XREF.Delete;

  if (bChecked) and (not bLocated) then
     db_AddRecord (qry_Roles_XREF,
                  [db_Par(FLD_UserId, 1),
                   db_Par(FLD_SecurityRoleId, iID) ]);

end;


end.
