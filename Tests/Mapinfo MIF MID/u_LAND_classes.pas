unit u_LAND_classes;

interface
uses SysUtils, classes;

//  u_geo_classes,
 // u_MapInfo_MIF_MID;

type
  TMapInfoRegion =class;
  TMapInfoRegionList =class;

  TXYPointArray = array of record
    X,Y : Double;
  end;


  TMapInfoFile = class
  private
  public
    RegionList: TMapInfoRegionList;

    constructor Create;
    destructor Destroy; override;

    procedure LoadFromFile(aFileName: string);
  end;


  //-----------------------------------------------------
  TMapInfoRegion = class //����������� ��������� � �����
  //-----------------------------------------------------
  private
  public
    XYPoints: TXYPointArray;
  end;


  //-----------------------------------------------------
  TMapInfoRegionList = class(TList)
  //-----------------------------------------------------
  private
    function GetItem(Index: integer): TMapInfoRegion;
  public
   
    procedure Add_(aXYPointArray: TXYPointArray);

    property Items[Index:integer]: TMapInfoRegion read GetItem; default;
  end;



implementation

uses StrUtils;


constructor TMapInfoFile.Create;
begin
  inherited Create;
  RegionList := TMapInfoRegionList.Create();
end;

destructor TMapInfoFile.Destroy;
begin
  FreeAndNil(RegionList);
  inherited Destroy;
end;

// ---------------------------------------------------------------
procedure TMapInfoFile.LoadFromFile(aFileName: string);
// ---------------------------------------------------------------

    // ---------------------------------------------------------------
    procedure DoInsertRegion(aStrList: TStrings);
    var
      I: Integer;
      xy_arr: TXYPointArray;
      e1,e2: double;
      iPos: integer;
      iLen: integer;
      iIndex : Integer;
      s: string;
      iRegionCount: integer;
    begin
      DecimalSeparator :='.';

      if aStrList.Count=0 then
        Exit;

      iIndex :=0;

      for I := 0 to aStrList.Count - 1 do
      begin
        s :=aStrList[i];
        iPos :=Pos(' ',s);

        // ������ ������
    (*    if i=0 then
        begin
          iRegionCount := StrToInt(Copy(s, iPos+1, 10));
        end else
*)
        if (LeftStr(s,2)='  ') and (Copy(s,3,1)<>' ') then
        begin
          iLen := StrToInt(Trim(s));
          SetLength(xy_arr, iLen);
        end else

        if (LeftStr(s,1)<>' ')  then
        begin
          e1:=StrToFloat(Copy(s, 1, iPos));
          e2:=StrToFloat(Copy(s, iPos+1, 10));

          xy_arr[iIndex].X := e1;
          xy_arr[iIndex].Y := e2;
          Inc(iIndex);
        end ;

      end;

      RegionList.Add_(xy_arr);

      aStrList.Clear;
    end;
    // ---------------------------------------------------------------


var
  oStrList: TStringList;
  oStrFile_region: TStringList;

  s: string;
  i: Integer;

  state: (stNone,stRegion);

  s1: string;

begin
  oStrList:=TStringList.Create;
  oStrFile_region:= TStringList.Create;

  oStrList.LoadFromFile(aFileName);


  for i := 0 to oStrList.Count -1 do
  begin
    s := LowerCase(oStrList[i]);

    if Pos('region',s)>0 then
    begin
      state := stRegion;
      if oStrFile_region.Count>0 then
      begin
        s1:=oStrFile_region.Text;
        DoInsertRegion(oStrFile_region);
      end;
    end;

    if state = stRegion then
      oStrFile_region.Add(s);
  end;

  if oStrFile_region.Count>0 then
    DoInsertRegion(oStrFile_region);

  FreeAndNil(oStrList);

end;


//-------------------------------------------
// TMapInfoRegionGroup
//-------------------------------------------


function TMapInfoRegionList.GetItem(Index: integer): TMapInfoRegion;
begin
  Result:=TMapInfoRegion(inherited Items[Index]);
end;


procedure TMapInfoRegionList.Add_(aXYPointArray: TXYPointArray);
var
  obj: TMapInfoRegion;
begin
  obj:=TMapInfoRegion.Create;
  Add(obj);
  obj.XYPoints:=aXYPointArray;
end;



var
  obj: TMapInfoFile;
begin
  obj:=TMapInfoFile.Create;
  obj.LoadFromFile('S:\T_VOL\46.mif');

end.
