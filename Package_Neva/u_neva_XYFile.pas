unit u_neva_XYFile;

interface
uses
  Classes, SysUtils,

  oTypes,
  dmw_use,

  u_Geo;


type
  TrelXYFile = class
    function LoadBoundsFromFile (aFileName: string): TXYRect;
    procedure SaveBoundsToFile   (aFileName: string; aXYBounds: TXYRect);
  end;

implementation

//--------------------------------------------------
// TnevaRelMatrix
//--------------------------------------------------

{function TnevaRelMatrix.GetZoneNum (): integer;
begin
  Result:=geo_Get6ZoneY (XYBounds.TopLeft.Y);
end;
}

//--------------------------------------------------
function TrelXYFile.LoadBoundsFromFile (aFileName: string): TXYRect;
//--------------------------------------------------
var gauss : array[0..3] of tgauss;
    xy: TFileStream;
begin
  Result:=MakeXYRect (0,0,0,0);

  try // ������ ���������
    xy:=TFileStream.Create(aFileName, fmShareDenyNone);
    xy.Read (gauss, SizeOf(gauss));

    with Result do begin
      TopLeft.x:=gauss[0].x;      TopLeft.y:=gauss[0].y;
      BottomRight.x:=gauss[2].x;  BottomRight.y:=gauss[2].y;
    end;
  finally
    xy.Free;
  end;
end;

//--------------------------------------------------
procedure TrelXYFile.SaveBoundsToFile (aFileName: string; aXYBounds: TXYRect);
//--------------------------------------------------
type tgauss = packed record x,y: double; end;
var gauss: array[0..3] of tgauss;   xyFile: TFileStream;
begin
  with aXYBounds do
  begin
    gauss[0].x:=TopLeft.x;
    gauss[0].y:=TopLeft.y;

    gauss[1].x:=TopLeft.x;
    gauss[1].y:=BottomRight.y;

    gauss[2].x:=BottomRight.x;
    gauss[2].y:=BottomRight.y;

    gauss[3].x:=BottomRight.x;
    gauss[3].y:=TopLeft.y;
  end;

  try // ������ ���������
    xyFile:=TFileStream.Create(aFileName, fmCreate);
    xyFile.Write (gauss, SizeOf(gauss));
  finally
    xyFile.Free;
  end;
end;

end.
