unit u_neva_Func;

interface
uses SysUtils,Classes,Math,Graphics,FileCtrl, Variants,

     dmw_use,
     oTypes,
     u_Geo,
     u_func;

type
  // ��� ���������� �����

  TXYiPoint = packed record X,Y: integer; end;
  TXYiRect  = packed record TopLeft,BottomRight: TXYiPoint; end;

  TXYiPointArray = array of TXYiPoint;

  neva_TMapObjectInfo = packed record
    Index       : integer; // ������ ������� �� �����
    Code        : integer; // ��� ������� � ��������������
    Descr       : string;  // �������� �������
    Local       : byte;    // ��������� ����������� (1-����, 2-�����,..)
    LocalStr    : string;  // ��������� ����������� - ������
    ID          : integer; // property 500
  end;

  Tneva_Semantic = record
         Count: integer;
         Items: array[0..50] of record  ID: integer; Caption: string; Value: Variant; end;
       end;

//  neva_TMapFileOpenMode = (fmOpenRead,fmReadWrite);
  neva_TArrayOfXYPointArray = array of TXYPointArray;

  function  neva_Open_Map  (aMapFileName: string; aMode: integer): boolean;
  procedure neva_Close_Map ();

  //
  function neva_Map_BLFrame (aMapFileName: string; var aBLRect: TBLRect): boolean;
  function neva_Map_XYFrame (aMapFileName: string; var aXYRect: TXYRect): boolean;
  function neva_Map_XYiFrame(aMapFileName: string; var aXYiFrame: TXYiRect): boolean;
  function neva_Map_LocalXYFrame (aMapFileName: string; var aXYiRect: TXYiRect): boolean;

  function neva_Map_BLFramePoints (aMapFileName: string; var aBLPoints: TBLPointArray): boolean;

  function  neva_Make_Color (aColor: integer; aBrushStyle: byte=0): integer;

  procedure neva_Create_Map   (aMapFileName,aObjFileName: string; aBounds: TBLRect);

  function  neva_Del_Object (aOffset: integer): boolean;
  function  neva_Get_Object_Info (aMapFileName: string; aObjCode: integer; var aInfo: neva_TMapObjectInfo): boolean;

  function  neva_Get_Object_gPoints (aMapFileName: string;
                                     aOffset: integer;
                                     var aCode,aPropertyID: integer;
                                     var aBLPoints: TBLPointArray
                                     ): boolean;
  // �������� ���������� (����) ����� �������
  // closed map

  function  neva_Goto_Root (): boolean;
  function  neva_Goto_Down (): boolean;
  function  neva_Goto_node (aOffset: integer): boolean;


  function neva_Add_Sign          (aCode: integer; aPoint: TBLPoint; aAsChild:boolean): integer;
  function neva_Insert_Text       (aCode: integer; aText:string; aPoint: TBLPoint; aAsChild:boolean): integer;

  function neva_Insert_Layer      (aCode: integer): integer;

  function neva_Insert_BLPolygone   (aCode:integer; var aBLPoints: TBLPointArray; aColor,aPattern:integer; aAsChild: boolean): integer;
  function neva_Insert_XYPolygone   (aCode:integer; var aXYPoints: TXYPointArray; aColor:integer; aAsChild: boolean): integer;

  function neva_Insert_BLPolyline   (aCode:integer; var aBLPoints:TBLPointArray; aColor:integer; aAsChild:boolean): integer;
  function neva_Insert_XYPolyline   (aCode:integer; var aXYPoints:TXYPointArray; aColor:integer; aAsChild:boolean): integer;

  procedure neva_SetCode (Value: integer);

  function neva_Insert_Vector     (aCode:integer; aPoint1,aPoint2: TBLPoint; aColor:integer; aAsChild:boolean): integer;
  function neva_Insert_Square     (aCode:integer; aPoint: TBLPoint; aSize: integer; aColor:integer; aAsChild:boolean): integer;
  function neva_Insert_Frame      (aCode:integer; aPoint: TBLPoint; aSize: integer): integer;

  function neva_Insert_BLRect     (aCode:integer; aBLRect: TBLRect; aAsChild: boolean): integer;

  procedure neva_DeleteByMask      (aCode:integer; aLocalType:byte; aID:integer);

  function  neva_FindByID          (aObjCode:integer; aObjType:byte; aID:integer): integer; //offset
  function  neva_FindBySem         (aObjCode:integer; aObjType:byte; aSemCode,aSemValue:integer): integer; //offset

  function neva_Find_First_Code (aObjCode:integer; aObjType:byte): integer;
  function neva_Find_Next_Code (aObjCode:integer; aObjType:byte): integer;

  procedure neva_Get_Object_Bounds  (var aXYRect: TXYRect);

  function neva_GetObjectPropertyID (aMapFileName:string; aOffset:integer): integer;

  procedure neva_SetObjectVisible   (aOffset:integer; aVisible: boolean);

  procedure neva_GetObjectBLPoints (var aBLPoints: TBLPointArray);
  procedure neva_GetObjectXYPoints (var aXYPoints: TXYPointArray);
  procedure neva_SetObjectBLPoints (var aBLPoints: TBLPointArray);

  procedure neva_Put_Sem_Value (aSemID: integer; aValue: Variant);
  function  neva_Get_Sem_Value (aSemID: integer; var aValue: Variant; aVarType: integer): boolean;

  procedure neva_SetVisibleLayersForMaps (aMapList: TStringList; aIDs: TIntArray);
  procedure neva_SetVisibleLayers (aMapFileName: string; aIDs: TIntArray);
  function  neva_G_to_L (aMapFileName: string; aBLRect: TBLRect; var aXYiRect: TXYiRect): boolean;

  function neva_GetObjectPointsFromDM (aDmFileName: string; aObjCode: integer): neva_TArrayOfXYPointArray;

  // ������ �� ������ ���� --------------------------


const
  neva_POLY_MAX=60000-2;
  USER_LAYER =100000000;

const
  neva_BS_SOLID  = 1; //0;
  neva_BS_BLANK  = 7;
  neva_BS_POINTS = 15;
  neva_BS_CROSS  = 13;

  NEVA_PATTERN_DEFAULT = -1;
  NEVA_PATTERN_SOLID = 0;
  NEVA_PATTERN_DOTS  = 8;

  NEVA_READ_ONLY = True;

  // �������� ����������� �������
  neva_LOCAL_MENU = 0; // ����
  neva_LOCAL_SIGN = 1; // ����
  neva_LOCAL_LINE = 2; // �����
  neva_LOCAL_AREA = 3; // �������
  neva_LOCAL_TEXT = 4; // �������

  //semantic code for ID
  SEM_NAME      = 9;
  SEM_ID        = 400;
  SEM_PARENT_ID = 401;
  SEM_EXPANDED  = 402;
  SEM_HEIGHT    = 1;
  SEM_EARTH_HEIGHT = 4;
  SEM_ADDRESS   = 9;

type
  neva_TMapLayerRec = record
                         Name    : string;
                         Code    : integer;
                         Visible : boolean;
                      end;
  neva_TMapLayerArr = array of neva_TMapLayerRec;


  function  neva_GetMapLayers (aMapFileName: string): neva_TMapLayerArr;
  procedure neva_SetMapLayers (aMapFileName: string; Values: neva_TMapLayerArr);


//===============================================================
implementation
//===============================================================
type
  neva_TLPoint = packed record x,y: integer end;
  neva_TLPoly  = packed array [0..neva_POLY_MAX] of neva_TLPoint;
  neva_TLLine  = packed record N: SmallInt; Poly: neva_TLPoly end;

  procedure neva_TLine_to_BLPoints (var aLine: neva_TLLine; var aBLPoints: TBLPointArray); forward;
  procedure neva_TLine_to_XYPoints (var aLine: neva_TLLine; var aXYPoints: TXYPointArray); forward;
  procedure neva_BLPoints_to_TLine (var aBLPoints: TBLPointArray; var aLine: neva_TLLine; aIsArea:boolean); forward;
  procedure neva_XYPoints_to_TLine (var aXYPoints: TXYPointArray; var aLine: neva_TLLine; aIsArea:boolean); forward;


  procedure ColorToRGB (Value: TColor; var R,G,B: byte); forward;
  procedure ForceDirByFileName (aFileName: string); forward;


//-------------------------------------------------
function neva_Make_Color (aColor: integer; aBrushStyle: byte=0): integer;
//-------------------------------------------------
var r,g,b: byte;
begin // �������� ������� �� ������ 8 !!!!!!!!!
 { bites   total	description   // ������ ������
  0-4	5  ������� - RED (�� 0 �� 31);
  5-9	5  ������� - GREEN (�� 0 �� 31)
  10-15	6  ����� ���������� (�� 0 �� 62); BrushStyle: 0 - solid;
  16-20	5  ������� - BLUE
  21-23	3  ��� ������� (0-������������ ����������� �����, 8 - �������� ��������)
  }
  ColorToRGB (aColor, r,g,b);
  Result:=(R div 8) + ((G div 8) shl 5) + (aBrushStyle shl 10) +
          ((B div 8) shl 16) + (4 shl 21) ;
end;

//--------------------------------------------------
function neva_Goto_node (aOffset: integer): boolean;
//--------------------------------------------------
begin
  Result:=dm_Goto_Node(aOffset);
end;

//-------------------------------------------------
procedure neva_BLPoints_to_TLine(var aBLPoints: TBLPointArray; var aLine: neva_TLLine;
                                aIsArea: boolean);
//-------------------------------------------------
var i,lx,ly: integer;  blPos: TBLPoint;
begin
  Assert(High(aBLPoints)>0);

  FillChar (aLine, SizeOf(aLine), 0);

  for i:=0 to High(aBLPoints) do
  begin
    blPos:=aBLPoints[i];
    DegToRad2 (blPos);
    dm_R_to_L (blPos.B, blPos.L, lx,ly);

    aLine.Poly[i].x:=lx;
    aLine.Poly[i].y:=ly;
  end;

//  aLine.N:=Length(aBLPoints);
  aLine.N:=High(aBLPoints);

  if aIsArea then
  begin
    aLine.N:=aLine.N+1;// aBLPoints.Count-1+1;
    aLine.Poly[aLine.N].x:=aLine.Poly[0].x;
    aLine.Poly[aLine.N].y:=aLine.Poly[0].y;
  end;
end;

//-------------------------------------------------
procedure neva_XYPoints_to_TLine(var aXYPoints: TXYPointArray; var aLine: neva_TLLine;
                              aIsArea: boolean);
//-------------------------------------------------
var i,lx,ly: integer;
begin
  FillChar (aLine, SizeOf(aLine), 0);

  for i:=0 to High(aXYPoints) do
  begin
    dm_G_to_L (aXYPoints[i].X, aXYPoints[i].Y, lx,ly);

    aLine.Poly[i].x:=lx;
    aLine.Poly[i].y:=ly;
  end;

//  aLine.N:=Length(aXYPoints);
  aLine.N:=High(aXYPoints);

  if aIsArea then
  begin
    aLine.N:=aLine.N+1;
    aLine.Poly[aLine.N].x:=aLine.Poly[0].x;
    aLine.Poly[aLine.N].y:=aLine.Poly[0].y;
  end;
end;


//----------------------------------------------------
procedure neva_Get_Object_Bounds (var aXYRect: TXYRect);
//----------------------------------------------------
var a,b: lpoint;
begin
  dm_Get_Bound (a,b);

  dm_L_to_G (a.x, a.y, aXYRect.TopLeft.X, aXYRect.TopLeft.Y);
  dm_L_to_G (b.x, b.y, aXYRect.BottomRight.X, aXYRect.BottomRight.Y);
end;



//----------------------------------------------------
function neva_Insert_BLRect (aCode:integer; aBLRect: TBLRect; aAsChild:boolean): integer;
//----------------------------------------------------
var
  line: neva_TLLine;
  blPoints: TBLPointArray;
begin
  geo_BLRectToBLPoints (aBLRect, blPoints);

  neva_BLPoints_to_TLine (blPoints, line, true);

  Assert(line.N>0);

  Result := dm_Add_Poly (aCode, NEVA_LOCAL_AREA, 0, @line, true);
end;


//-------------------------------------------------
function neva_Insert_Text (aCode: integer; aText:string; aPoint:TBLPoint; aAsChild:boolean): integer;
//-------------------------------------------------
var lline: neva_TLLine;
    lx1,ly1, lx2,ly2: integer;
begin
    DegToRad2 (aPoint);

    dm_R_to_L (aPoint.B,aPoint.L, lx1,ly1);
    dm_R_to_L (aPoint.B,aPoint.L+0.0004, lx2,ly2);

    FillChar (lline, SizeOf(lline), 0);

    lline.N:=2;
    lline.Poly[0].x:=lx1;  lline.Poly[0].y:=ly1;
    lline.Poly[1].x:=lx2;  lline.Poly[1].y:=ly2;
    lline.Poly[2].x:=lx2;  lline.Poly[2].y:=ly2;

    Result:=dm_Add_Text (aCode, NEVA_LOCAL_TEXT, 0, @lline, PChar(AText), aAsChild);
end;

    //-------------------------------------------------
    function WinToDos (s: string): string;
    //-------------------------------------------------
    var i: integer;  ch: char;
    begin
        for i:=1 to Length(s) do begin
          ch:=s[i];
          if (ch >= '�') and (ch <= '�') then  ch:=chr(ord(ch)-64) else
          if (ch >= '�') and (ch <= '�') then  ch:=chr(ord(ch)-16);
          s[i]:=ch
        end;

        Result:=s
    end;

    //-------------------------------------------------
    function DosToWin (s: string): string;
    //-------------------------------------------------
    var i: Integer; ch: char;
    begin
        for i:=1 to Length(s) do begin
          ch:=s[i];
          if (ch >= #$80) and (ch <= #$AF) then  ch:=chr(ord(ch)+$40) else
          if (ch >= #$E0) and (ch <= #$EF) then  ch:=chr(ord(ch)+$10) else
          if (ch >= #$F0)                  then  ch:=chr(ord(ch)-$50);
          s[i]:=ch
        end;

        Result:=s
    end;

//------------------------------------------------------
procedure neva_Put_Sem_Value (aSemID: integer; aValue: Variant);
//------------------------------------------------------
var str,sValue: string;  sngl: single;  i: integer;
// vValue: Variant;
begin
  case VarType (aValue) of
     varInteger: dm_Put_long (aSemID, aValue);

     varOleStr,
     varString : begin
                   sValue:=aValue;
                   str:=WinToDos(sValue);
                   dm_Put_String (aSemID, PChar(str));
                 end;

     varDouble,
     varSingle : begin
                   sngl:=aValue;

                   // no minus heights
                  // if aSemID=SEM_EARTH_HEIGHT then
                  //   if sngl <= 0 then
                    //   Exit;

                   dm_Put_Real (aSemID, sngl);
                 end;

     varBoolean: begin
                   if aValue=True then i:=1 else i:=0;
                   dm_Put_Long (aSemID, i);
                 end;
  end;
end;


//------------------------------------------------------
function neva_Get_Sem_Value (aSemID: integer; var aValue: Variant; aVarType: integer): boolean;
//------------------------------------------------------
var iValue: integer; sValue:ShortString; fValue:single; // bValue: byte;
begin
  Result:=false;
  case aVarType of
     varInteger: begin Result:=(dm_Get_long (aSemID, 0, iValue));
                      if Result then aValue:=iValue else aValue:=0;
                 end;
     varString : begin
                    Result:=(dm_Get_String (aSemID, 200, sValue));
                    if Result then
                      aValue:=DosToWin(sValue)
                    else aValue:='';
                 end;
     varSingle : begin Result:=(dm_Get_Real (aSemID, 0, fValue));
                       if Result then aValue:=fValue else aValue:=0;
                 end;
     varBoolean: begin Result:=(dm_Get_long (aSemID, 0, iValue));
                       if Result then aValue:=(iValue=1) else aValue:=False;
                 end;

  else
    raise Exception.Create('');
  end;
end;


//----------------------------------------------------
function neva_Insert_BLPolygone(aCode:integer; var aBLPoints: TBLPointArray;
    aColor,aPattern:integer; aAsChild: boolean): integer;
//----------------------------------------------------
var line: neva_TLLine; iDmColor: integer;
begin
  if (aColor=0) and (aPattern <> NEVA_PATTERN_DEFAULT) then
    begin Result:=0; Exit; end;

  neva_BLPoints_to_TLine (aBLPoints, line, true);
  Result := dm_Add_Poly (aCode, NEVA_LOCAL_AREA, 0, @line, aAsChild);

  if (aPattern <> NEVA_PATTERN_DEFAULT) then
  begin
    iDmColor:=neva_Make_Color (aColor, aPattern);
    dm_Set_Graphics(iDmColor);
  end;
end;

//----------------------------------------------------
function neva_Insert_XYPolygone(aCode:integer; var aXYPoints: TXYPointArray;
    aColor:integer; aAsChild: boolean): integer;
//----------------------------------------------------
var line: neva_TLLine;
begin
  neva_XYPoints_to_TLine (aXYPoints, line, true);

  Result := dm_Add_Poly (aCode, NEVA_LOCAL_AREA, 0, @line, aAsChild);
  if (aColor>0) then dm_Set_Graphics(AColor);
end;


//----------------------------------------------------
function neva_Insert_Vector (aCode:integer; aPoint1,aPoint2: TBLPoint; aColor:integer; aAsChild:boolean): integer;
//----------------------------------------------------
var blPoints: TBLPointArray;
begin
  SetLength(blPoints,2);

  blPoints[0]:=aPoint1;
  blPoints[1]:=aPoint2;
  Result:=neva_Insert_BLPolyline (aCode, blPoints, aColor, aAsChild);
end;

//----------------------------------------------------
function neva_Insert_BLPolyline(aCode:integer; var aBLPoints:TBLPointArray;
    aColor:integer; aAsChild:boolean): integer;
//----------------------------------------------------
var line: neva_TLLine;
begin
  neva_BLPoints_to_TLine (aBLPoints, line, false);
  Result := dm_Add_Poly (ACode, neva_LOCAL_LINE, 0, @line, aAsChild);
  if (aColor>0) then dm_Set_Graphics(AColor);
end;

//----------------------------------------------------
function neva_Insert_XYPolyline(aCode:integer; var aXYPoints:TXYPointArray;
    aColor:integer; aAsChild:boolean): integer;
//----------------------------------------------------
var line: neva_TLLine;
begin
  neva_XYPoints_to_TLine (aXYPoints, line, false);
  Result := dm_Add_Poly (ACode, neva_LOCAL_LINE, 0, @line, aAsChild);
  if (aColor>0) then dm_Set_Graphics(AColor);
end;


function neva_Goto_Down(): boolean;
begin
  Result:=dm_Goto_Down;
end;

function neva_Goto_Right(): boolean;
begin
  Result:=dm_Goto_Right;
end;

function neva_Goto_Root(): boolean;
begin
  Result:=dm_Goto_Root > 0;
end;

//-------------------------------------------------
function neva_Add_Sign (aCode:integer; aPoint: TBLPoint; aAsChild:boolean): integer;
//-------------------------------------------------
var a,b: lpoint;
begin
  DegToRad2 (aPoint);

  dm_R_to_L (aPoint.B, aPoint.L, a.x,a.y);
  dm_R_to_L (aPoint.B, aPoint.L, b.x,b.y);

  Result := dm_Add_Sign (aCode, a,b, 0, aAsChild);
end;

//----------------------------------------------------
procedure neva_GetRectForCenterPoint (aPoint: TBLPoint; aSquareSize: integer; var aRect: TBLRect);
//----------------------------------------------------
// ����������� ����� ������� � ������ ������ ����� �������� � ��������
var lx,ly: integer;
    deltaB,deltaL, gx2,gy2: double;
    b2,l2: double;
begin
   DegToRad2(aPoint);

   dm_R_to_L (aPoint.B,aPoint.L, lx,ly);
   dm_L_to_G (lx,ly, gx2,gy2);
   dm_G_to_L (gx2 - aSquareSize,gy2 + aSquareSize, lx,ly);
   dm_L_to_R (lx,ly, b2,l2);

   deltaB:=Abs(aPoint.B-b2);
   deltaL:=Abs(aPoint.L-l2);

   aRect.TopLeft.B:=aPoint.B+ deltaB/2;
   aRect.TopLeft.L:=aPoint.L- deltaL/2;

   aRect.BottomRight.B:=aPoint.B- deltaB/2;
   aRect.BottomRight.L:=aPoint.L+ deltaL/2;

   RadToDeg2(aRect.TopLeft);  RadToDeg2(aRect.BottomRight); 
end;


//----------------------------------------------------
function neva_Insert_Square (aCode:integer; aPoint: TBLPoint; aSize: integer; aColor:integer; aAsChild:boolean): integer;
//----------------------------------------------------
var
  blPoints: TBLPointArray;
  blRect: TBLRect;
begin
  //���������� ������ � ������ � ��������
  blRect:=geo_GetSquareAroundPoint (aPoint, aSize);

  geo_BLRectToBLPoints (blRect, blPoints);

  //   Result:=neva_Insert_BLRect (aCode, blRect, aAsChild);
  //   iDmColor:=neva_Make_Color (aColor, 0);
  Result:=neva_Insert_BLPolygone (aCode, blPoints, aColor, 0, aAsChild);

//   if (aColor>0) then dm_Set_Graphics (aColor);

end;


//----------------------------------------------------
function neva_Insert_Frame (ACode:integer; aPoint: TBLPoint; aSize: integer): integer;
//----------------------------------------------------
var
  blPoints: TBLPointArray;
  line: neva_TLLine;
  blRect: TBLRect;
begin
   //���������� ������ � ������ � ��������
  neva_GetRectForCenterPoint (aPoint, aSize, blRect);

  geo_BLRectToBLPoints (blRect, blPoints);

  neva_BLPoints_to_TLine (blPoints, line, true);

  Assert(line.N>0);

  Result := dm_Add_Poly (ACode, NEVA_LOCAL_LINE, 0, @line, false);
end;

//----------------------------------------------------
procedure neva_GetObjectBLPoints(var aBLPoints: TBLPointArray);
//----------------------------------------------------
var
  line: neva_TLLine;
  iCount: integer;
begin
  FillChar(line, SizeOf(line), 0);
  iCount:=dm_Get_Poly_buf (@line, neva_POLY_MAX);
  // ��� ������ - total=0 ��� 1
  if iCount>=0 then neva_TLine_to_BLPoints (line, aBLPoints)
               else SetLength(aBLPoints,0);// SetLength (Result, 0);
end;


//----------------------------------------------------
function neva_GetObjectXYiPoints(): TXYiPointArray;
//----------------------------------------------------
var line: neva_TLLine;
    i,iCount: integer;
begin
  FillChar(line, SizeOf(line), 0);
  iCount:=dm_Get_Poly_buf (@line, neva_POLY_MAX);

//  iCount:=line.N + 1;
  SetLength (Result, iCount);

  for i:=0 to iCount-1 do
  begin
    Result[i].X:=line.Poly[i].x;
    Result[i].Y:=line.Poly[i].y;
  end;
end;


//----------------------------------------------------
procedure neva_GetObjectXYPoints(var aXYPoints: TXYPointArray);
//----------------------------------------------------
var line: neva_TLLine;  iCount: integer;
begin
  FillChar(line, SizeOf(line), 0);
  iCount:=dm_Get_Poly_buf (@line, neva_POLY_MAX);
  // ��� ������ - total=0 ��� 1

  if iCount>=0 then neva_TLine_to_XYPoints (line, aXYPoints)
               else SetLength(aXYPoints,0);//SetLength (Result, 0);
end;


//----------------------------------------------------
procedure neva_SetObjectBLPoints(var aBLPoints: TBLPointArray);
//----------------------------------------------------
var line: neva_TLLine;  b,l: double;  iCount: integer;
    bIsArea: boolean;
begin
  bIsArea:=(dm_Get_Local = neva_LOCAL_AREA);

  neva_BLPoints_to_TLine (aBLPoints, line, bIsArea);

  iCount:=dm_Set_Poly_buf (@line);
end;


//-------------------------------------------------
procedure neva_TLine_to_BLPoints(var aLine: neva_TLLine; var aBLPoints: TBLPointArray);
//-------------------------------------------------
var i,iCount: integer;
begin
  iCount:=aLine.N + 1;
//  SetLength (aBLPoints, iCount);
  SetLength(aBLPoints,iCount);

//  assert (aBLPoints.Count < High(aBLPoints.Items));

  try
    for i:=0 to iCount-1 do
    begin
      dm_L_to_R (aLine.Poly[i].x, aLine.Poly[i].y,   aBLPoints[i].B, aBLPoints[i].L);
      RadToDeg2 (aBLPoints[i]);
    end;

  except
    raise Exception.Create(IntToStr(aLine.N));
  end;
                                                 
end;

//-------------------------------------------------
procedure neva_TLine_to_XYPoints(var aLine: neva_TLLine; var aXYPoints: TXYPointArray);
//-------------------------------------------------
var i,iCount: integer;
begin
  iCount:=aLine.N + 1;
  SetLength(aXYPoints,iCount);

//  SetLength (Result, iCount);

  for i:=0 to iCount-1 do
  begin
    dm_L_to_G (aLine.Poly[i].x, aLine.Poly[i].y,  aXYPoints[i].X, aXYPoints[i].Y);

    aXYPoints[i].X:=Round(aXYPoints[i].X);
    aXYPoints[i].Y:=Round(aXYPoints[i].Y);
  end;
end;


//----------------------------------------------------
function  neva_Get_Object_GPoints (aMapFileName: string;
                     aOffset:integer;
                     var aCode,aPropertyID: integer;
                     var aBLPoints: TBLPointArray
                     ): boolean;
//----------------------------------------------------
var gX1,gY1:  double;
    id,i,total,max: integer;
    bool: boolean;
    mf: neva_TLLine;
    iValue: integer;

begin
  aCode:=0;
  aPropertyID:=0;

  if (neva_Open_Map (aMapFileName, fmOpenRead)) and
     (dm_Goto_Node(aOffset)=true) and  // ������� �� ������
     (dm_Get_Poly_Count < LPoly_Max) // ���-�� ����� < 127, � �� �� ��������
  then begin
    neva_GetObjectBLPoints(aBLPoints);
    total:=Length(aBLPoints);//.Count;// .aBLPoints.Count-1+1;

 //   if (total>1) and
 //      (Abs(aBLPoints[0].B - aBLPoints[total].B) = 0 {< 0.000001}) and
 //      (Abs(aBLPoints[0].L - aBLPoints[total].L) = 0 {< 0.000001})
 //   then aBLPoints.Count:=total;
 
    aCode:=dm_Get_Code;

    if (true=dm_Get_long (SEM_ID, 0, iValue))
      then aPropertyID:=iValue
      else aPropertyID:=0;

    neva_Close_Map();
  end else
    SetLength(aBLPoints,0);
//    aBLPoints.Count:=0;
//    SetLength(aBLPoints,0);
end;


//----------------------------------------------------
function neva_GetMapLayers (aMapFileName: string): neva_TMapLayerArr;
//----------------------------------------------------
var code,flags: integer;
    obj_name: TShortStr;
begin
  SetLength (Result, 0);

  if neva_Open_Map (aMapFileName, fmOpenRead) then
  begin
    dm_Goto_Root; dm_Goto_down;

    repeat
      code:=dm_Get_Code;
      obj_Get_Name (code, neva_LOCAL_MENU, obj_name);
      flags:=dm_Get_Flags;

      SetLength (Result, High(Result)+1+1);

      Result[High(Result)].Name := obj_name;
      Result[High(Result)].Code := code;
      Result[High(Result)].Visible:=(flags in [0,1]);
    until (not dm_Goto_Right);

    neva_Close_Map();
  end;
end;

//----------------------------------------------------
procedure neva_SetMapLayers (aMapFileName: string; Values: neva_TMapLayerArr);
//----------------------------------------------------
var code,i:integer;
begin
  if neva_Open_Map (aMapFileName, fmOpenWrite) then
  begin
    dm_Goto_Root; dm_Goto_down;

    repeat
      code:=dm_Get_Code;
      for i:=0 to High(Values) do
        if Values[i].Code=code then begin dm_Set_Flags (IIF(Values[i].Visible,0,16)); break; end;

    until (not dm_Goto_Right);
    neva_Close_Map();
  end;
end;


procedure neva_SetVisibleLayersForMaps (aMapList: TStringList; aIDs: TIntArray);
var i: integer;
begin
  for i:=0 to aMapList.Count-1 do
    neva_SetVisibleLayers (aMapList[i], aIDs);
end;


//----------------------------------------------------
procedure neva_SetVisibleLayers (aMapFileName: string; aIDs: TIntArray);
//----------------------------------------------------
var code,iLevel: integer;
begin
  if neva_Open_Map (aMapFileName, fmOpenWrite) then
  begin
    dm_Goto_Root; dm_Goto_down;

    repeat
      code:=dm_Get_Code;
      iLevel:=code div 10000000;

      if iLevel>0 then 
      if FindInIntArray(iLevel, aIDs)>=0
        then dm_Set_Flags (0)
        else dm_Set_Flags (16);

    until (not dm_Goto_Right);

    neva_Close_Map();
  end;
end;

//----------------------------------------------------
function neva_Del_Object (aOffset: integer): boolean;
//----------------------------------------------------
var i:integer;
begin
  i:=dm_Del_Object (0, aOffset);
  Result:=true;
end;


//----------------------------------------------------
function neva_Get_Object_Info (aMapFileName: string; aObjCode: integer; var aInfo: neva_TMapObjectInfo): boolean;
//----------------------------------------------------
var P: PChar;
   bool: boolean;
   i,c: integer;
   obj_name: TShortStr;
   s: string;
   lp: TWLine;
   iValue: integer;
begin
  if (neva_Open_Map (aMapFileName, fmOpenRead)) and
     (neva_Goto_Node(aObjCode)) then
  begin
    aInfo.Index       :=aObjCode;
    aInfo.Code        :=dm_Get_Code;
    aInfo.Local       :=dm_Get_Local;
//    aInfo.LocalStr    := neva_LOCAL_TYPES[aInfo.Local];

    // ��������� �������
    aInfo.Descr:=obj_Get_Name(aInfo.Code, aInfo.Local, obj_name);
//    aInfo.

    if (true=dm_Get_long (SEM_ID, 0, iValue))
      then aInfo.ID:=iValue
      else aInfo.ID:=0;


//  FillChar(lp,sizeof(lp),0);
//    i := obj_Ind_Blank(Info.Code, Info.Local);
//    i := obj_Get_Blank (Info.Code, Info.Local, @lp, SizeOf(TWLine));
//   obj_name:= obj_Get_Object(1, Info.Code, Info.Local, obj_name);
{
    i := dm_Get_hf_Pool (@aPool1, 1024);
}
    neva_Close_Map();
    Result:=true;
  end else
    Result:=false;
end;

//----------------------------------------------------
function neva_Open_Map (aMapFileName: string; aMode: integer): boolean;
//----------------------------------------------------
var fname: TShortStr;
  b: boolean;
begin
  aMapFileName:=Trim(aMapFileName);
  aMapFileName:=ChangeFileExt (aMapFileName, '.dm');

  if not FileExists(aMapFileName) then begin Result:=False; Exit; end;

  try
    b:=(aMode<>fmOpenRead);

    //b:=not aReadOnly

    Result:=dm_Open (StrPCopy(fname, aMapFileName), b) > 0;
    dm_AutoSave (100000);
  except end;
end;


procedure neva_Close_Map ();
begin
  dm_Done;
end;


procedure neva_SetObjectVisible (aOffset:integer; aVisible: boolean);
var bool:boolean;
begin
  bool:=dm_Goto_node (aOffset);
 // dm_Jump_node (aOffset);
  if dm_Object<>aOffset then Exit;
  dm_Set_Flag (fl_draw, not aVisible);
end;

//----------------------------------------------------
procedure neva_Show_Objects (aVisibleID,aInVisibleID: TIntArray);
//----------------------------------------------------
    procedure DoShowObjects (aOffsets: array of integer; aVisible:boolean);
    var i:integer;
    begin
      for i:=0 to High(aOffsets) do begin
        neva_SetObjectVisible (aOffsets[i],aVisible);
      end;
    end;

begin
  if (High(aVisibleID)>=0) or (High(aInVisibleID)>=0) then
  begin
    IntArrayRemoveValues (aInVisibleID, aVisibleID);

    DoShowObjects (aVisibleID,   true);
    DoShowObjects (aInVisibleID, false);
  end;
end;



function neva_GetObjectPropertyID (aMapFileName:string; aOffset:integer): integer;
var iValue:integer;
begin
  if (neva_Open_Map (aMapFileName, fmOpenRead)) and
     (dm_Goto_Node(aOffset)) and
     (dm_Get_long (SEM_ID,0, iValue))
  then Result:=iValue
  else Result:=0
end;

//----------------------------------------------------
function neva_FindByID (aObjCode:integer; aObjType:byte; aID:integer): integer; //offset
//----------------------------------------------------
begin
   Result:=neva_FindBySem (aObjCode, aObjType, SEM_ID, aID);
end;

function neva_Find_First_Code (aObjCode:integer; aObjType:byte): integer;
begin
  Result:=dm_Find_Frst_Code (aObjCode,aObjType);
end;

function neva_Find_Next_Code (aObjCode:integer; aObjType:byte): integer;
begin
  Result:=dm_Find_Next_Code (aObjCode,aObjType);
end;


//----------------------------------------------------
function neva_FindBySem (aObjCode:integer; aObjType:byte; aSemCode,aSemValue:integer): integer; //offset
//----------------------------------------------------
var iOffset: integer;  iValue: integer;  bool: Boolean;
begin
   Result:=-1;
   iOffset:=dm_Find_Frst_Code (aObjCode,aObjType);
   while iOffset>0 do
   begin
     bool:=dm_Get_long (aSemCode, 0, iValue);

     if (bool) and (iValue=aSemValue)
       then begin Result:=iOffset; Exit; end;

     iOffset:=dm_Find_Next_Code (aObjCode,aObjType);
   end;
end;

//----------------------------------------------------
procedure neva_DeleteByMask (aCode:integer; aLocalType:byte; aID:integer);
//----------------------------------------------------
var i,iOffset: integer;
begin
  while true do begin
    iOffset:=neva_FindByID (aCode,aLocalType,aID);
    if iOffset>=0 then
      dm_Del_Object (0, iOffset);
  end;
end;


//----------------------------------------------------
procedure neva_Create_Map (aMapFileName,aObjFileName: string; aBounds: TBLRect);
//----------------------------------------------------
var xmin,ymin,xmax,ymax: double;
   iOffset: integer;
begin
  aMapFileName:=ChangeFileExt (aMapFileName, '.dm');
  ForceDirByFileName (aMapFileName);

  xmin:=DegToRad (aBounds.BottomRight.B);
  xmax:=DegToRad (aBounds.TopLeft.B);
  ymin:=DegToRad (aBounds.TopLeft.L);
  ymax:=DegToRad (aBounds.BottomRight.L);

{ TODO : ???? scale }
  dm_New (PChar(aMapFileName), PChar(aObjFileName),
           1, 1, 1,  0,0,0,  0,0,0,  xmin,ymin,xmax,ymax, 500);

  if (neva_Open_Map (aMapFileName, fmOpenWrite))
    then dm_Done;

end;

//----------------------------------------------------
function neva_Make_BLFrame (aBLPoints: TBLPointArray; var aBLRect: TBLRect): boolean;
//----------------------------------------------------
var i: integer;
begin
  for i:=0 to High(aBLPoints) do //(aBLPoints) do
    if i>0 then begin
      aBLRect.TopLeft.B    :=Max(aBLRect.TopLeft.B,     aBLPoints[i].B);
      aBLRect.TopLeft.L    :=Min(aBLRect.TopLeft.L,     aBLPoints[i].L);
      aBLRect.BottomRight.B:=Min(aBLRect.BottomRight.B, aBLPoints[i].B);
      aBLRect.BottomRight.L:=Max(aBLRect.BottomRight.L, aBLPoints[i].L);
    end else begin
      aBLRect.TopLeft:=aBLPoints[i];
      aBLRect.BottomRight:=aBLPoints[i];
    end;

  Result:=Length(aBLPoints) > 1;// aBLPoints.Count-1 > 1;
end;

//----------------------------------------------------
function neva_Make_XYFrame (aBLPoints: TBLPointArray; var aXYRect: TXYRect): boolean;
//----------------------------------------------------
var i: integer;  xyPoint: TXYPoint;
begin
  for i:=0 to High(aBLPoints) do
  begin
    xyPoint:=geo_BL_to_XY (aBLPoints[i]);

    if i>0 then begin
      aXYRect.TopLeft.X    :=Max(aXYRect.TopLeft.X,     xyPoint.X);
      aXYRect.TopLeft.Y    :=Min(aXYRect.TopLeft.Y,     xyPoint.Y);
      aXYRect.BottomRight.X:=Min(aXYRect.BottomRight.X, xyPoint.X);
      aXYRect.BottomRight.Y:=Max(aXYRect.BottomRight.Y, xyPoint.Y);
    end else begin

      aXYRect.TopLeft:=xyPoint;
      aXYRect.BottomRight:=xyPoint;
    end;
  end;

  Result:=High(aBLPoints) > 1;
end;

//----------------------------------------------------
function neva_Map_BLFramePoints (aMapFileName: string; var aBLPoints: TBLPointArray): boolean;
//----------------------------------------------------
begin
  if neva_Open_Map(aMapFileName, fmOpenRead) then
  begin
    dm_Goto_Root;
    neva_GetObjectBLPoints(aBLPoints);
    neva_Close_Map();
    Result:=true;
  end else
    Result:=false;
end;

//----------------------------------------------------
function neva_Map_XYIFrame (aMapFileName: string; var aXYiFrame: TXYiRect): boolean;
//----------------------------------------------------
var aXYiPoints: TXYiPointArray;
  i: integer;
begin
  if neva_Open_Map(aMapFileName, fmOpenRead) then
  begin
    dm_Goto_Root;
    aXYiPoints:=neva_GetObjectXYiPoints;

    for i:=0 to High(aXYiPoints) do
    begin
      if i=0 then begin
        aXYiFrame.TopLeft:=aXYiPoints[i];
        aXYiFrame.BottomRight:=aXYiPoints[i];
        Continue;
      end;

      aXYiFrame.TopLeft.X:=Min (aXYiFrame.TopLeft.X, aXYiPoints[i].X);
      aXYiFrame.TopLeft.Y:=Min (aXYiFrame.TopLeft.Y, aXYiPoints[i].Y);

      aXYiFrame.BottomRight.X:=Max (aXYiFrame.BottomRight.X, aXYiPoints[i].X);
      aXYiFrame.BottomRight.Y:=Max (aXYiFrame.BottomRight.Y, aXYiPoints[i].Y);

    end;

    neva_Close_Map();
    Result:=true;
  end else
    Result:=false;
end;


//----------------------------------------------------
function neva_Map_BLFrame (aMapFileName: string; var aBLRect: TBLRect): boolean;
//----------------------------------------------------
var blPoints: TBLPointArray;
begin
  Result:=neva_Map_BLFramePoints (aMapFileName, blPoints);
  Result:=Result and neva_Make_BLFrame (blPoints, aBLRect);
end;



//----------------------------------------------------
function neva_Map_XYFrame (aMapFileName: string; var aXYRect: TXYRect): boolean;
//----------------------------------------------------
var blPoints: TBLPointArray;
begin
  Result:=neva_Map_BLFramePoints (aMapFileName, blPoints);
  Result:=Result and neva_Make_XYFrame (blPoints, aXYRect);
end;

//----------------------------------------------------
function neva_G_to_L (aMapFileName: string; aBLRect: TBLRect; var aXYiRect: TXYiRect): boolean;
//----------------------------------------------------
begin
  if neva_Open_Map (aMapFileName, fmOpenRead) then
  begin
    dm_R_to_L (aBLRect.TopLeft.B,     aBLRect.TopLeft.L,      aXYiRect.TopLeft.X,     aXYiRect.TopLeft.Y);
    dm_R_to_L (aBLRect.BottomRight.B, aBLRect.BottomRight.L,  aXYiRect.BottomRight.X, aXYiRect.BottomRight.Y);
    Result:=true;
  end else
    Result:=false;
end;

//----------------------------------------------------
function neva_Map_LocalXYFrame (aMapFileName: string; var aXYiRect: TXYiRect): boolean;
//----------------------------------------------------
var blPoints: TBLPointArray;
   xyRect: TXYRect;
   ix1,iy1,ix2,iy2: integer;
begin
  Result:=neva_Map_BLFramePoints (aMapFileName, blPoints);
  Result:=Result and neva_Make_XYFrame (blPoints, xyRect);

  if Result then
  if neva_Open_Map (aMapFileName, fmOpenRead) then
  begin
    dm_G_to_L (xyRect.TopLeft.X,     xyRect.TopLeft.Y,      ix1, iy1);
    dm_G_to_L (xyRect.BottomRight.X, xyRect.BottomRight.Y,  ix2, iy2);

    aXYiRect.TopLeft.X:=Min (ix1,ix2);
    aXYiRect.TopLeft.Y:=Min (iy1,iy2);
    aXYiRect.BottomRight.X:=Max (ix1,ix2);
    aXYiRect.BottomRight.Y:=Max (iy1,iy2);


    Result:=true;
  end;
end;


//----------------------------------------------------
procedure neva_Get_Object_Semantic (aOffset: integer; var aSemantic: Tneva_Semantic);
//----------------------------------------------------

var
  i,nn,ind,w: word;
  Pool: TPool;

  idt: Id_Tag;
  info: TBytes;
  iCount: integer;

  iValue: integer; sValue: ShortString; fValue: single;  wValue: word; bValue: byte;
  bool: boolean;
  v: Variant;

  pCh: array[0..200] of char;

begin
  aSemantic.Count:=0;

  dm_Goto_Node (aOffset);

  ind:=dm_Get_hf_Pool (@Pool, 1000);
  if ind<0 then Exit;

  ind:=0;
  iCount:=0;

  repeat
    ind:=dm_hf_Pool_Get (@Pool, ind, nn, idt, @info );
    if nn=0 then Break;

    if not (idt in [_string,_int,_float,_real,_word]) then Continue;

    aSemantic.Count:=iCount;
    aSemantic.Items[iCount].ID:=nn;
    v:=null;

    idx_Get_Name(nn, @pCh);
    aSemantic.Items[iCount].Caption:=StrPas (@pCh);

    case idt of
      _string: begin  bool:=(dm_Get_String (nn, 200, sValue));
                      if bool then v:=DosToWin(sValue);
               end;

      _int:    begin  bool:=(dm_Get_long (nn, 0, iValue));
                      if bool then v:=iValue;
               end;
      _word:   begin  bool:=(dm_Get_WORD (nn, 0, wValue));
                      if bool then v:=wValue;
               end;

      _byte:   begin  bool:=(dm_Get_long (nn, 0, iValue));
                      if bool then v:=(iValue=1) else v:=False;
               end;

      _float,
      _real:   begin  bool:=(dm_Get_Real (nn, 0, fValue));
                      if bool then v:=fValue;
               end;

{
  Id_Tag = (_byte,_word,_int,_long,_time,
            _date,_float,_real,_angle,_string,
            _dBase,_enum,_bool,_link,_double,
            _unicode,_list,_text,_label);
 }
    end;
    aSemantic.Items[iCount].Value:=v;

    Inc(iCount);
    if iCount>30 then Break;
  until
    (nn = 0);

end;


//----------------------------------------------------

function neva_Insert_Layer (aCode: integer): integer;
begin
  dm_Goto_Root; dm_Goto_down;  //dm_GOto_last;
  Result:=dm_Add_Layer (aCode,0);
end;

procedure neva_SetCode (Value: integer);
begin
  dm_Set_Code (Value);
end;

//-----------------------------------------------------
function neva_GetObjectPointsFromDM (aDmFileName: string; aObjCode: integer): neva_TArrayOfXYPointArray;
//-----------------------------------------------------
var
  xyPoints: TXYPointArray;
  iOffs,j,iCount: integer;

begin
  neva_Open_Map (aDmFileName, fmOpenRead);

  iCount:=0;
  iOffs:=neva_Find_First_Code (aObjCode, neva_LOCAL_AREA);
  while iOffs>0 do begin
    Inc (iCount);
    iOffs:=neva_Find_Next_Code (aObjCode, neva_LOCAL_AREA);
  end;

  SetLength (Result, iCount);
  j:=0;
  iOffs:=neva_Find_First_Code (aObjCode, neva_LOCAL_AREA);
  while iOffs>0 do begin
    neva_GetObjectXYPoints(Result[j]);

    Inc (j);
    iOffs:=neva_Find_Next_Code (aObjCode, neva_LOCAL_AREA);
  end;

  neva_Close_Map;
end;

procedure ForceDirByFileName (aFileName: string);
begin
  ForceDirectories (ExtractFileDir (aFileName));
end;


//--------------------------------------------------------
procedure ColorToRGB (Value: TColor; var R,G,B: byte);
//--------------------------------------------------------
var cl: packed record case b:byte of
          0: (int: TColor);   1: (RGB: array[0..4] of byte);
        end;
begin
  cl.int:=Value;
  R:=cl.rgb[0];  G:=cl.rgb[1];  B:=cl.rgb[2];
end;


var
  xyBounds: TXYiRect;

  rSemantic: Tneva_Semantic;
  i: integer;

begin
{
  neva_Open_Map ('m:\piter_test.dm', fmOpenRead);

  neva_Get_Object_Semantic (120392, rSemantic);
  i:=0;
}
end.

