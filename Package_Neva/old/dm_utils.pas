unit dm_utils;


interface


 procedure dm_Relief (dmPath,altPath,rlfPath: PChar;
                      x1,y1,x2,y2,ed: Integer;
                      dmw: Boolean);   stdcall;

 function dm_Recolor (dmPath: PChar): longint; stdcall;


{

dmPath  - �����
altPath - ��������� ���� � ������� �������
          �� �������
rlfPath - ������� �����
x1,y1,x2,y2 - ������� � ������� ��������
              �������� �����
ed      - ������ ��������
dmw     - ����� �������
}


implementation

const
 dll_path = 'dm_util.dll';
 dll_path2 = 'dm_recolor.dll';

 procedure dm_Relief (dmPath,altPath,rlfPath: PChar;
                     x1,y1,x2,y2,ed: Integer;
                     dmw: Boolean); external dll_path;

 function dm_Recolor (dmPath: PChar): longint; external dll_path2;


end.

